// To parse this JSON data, do
//
//     final productPagination = productPaginationFromJson(jsonString);

import 'dart:convert';

import 'product_model.dart';

ProductPagination productPaginationFromJson(String str) =>
    ProductPagination.fromJson(json.decode(str));

String productPaginationToJson(ProductPagination data) =>
    json.encode(data.toJson());

class ProductPagination {
  ProductPagination({
    required this.total,
    required this.perPage,
    required this.page,
    required this.lastPage,
    required this.data,
  });

  int total;
  int perPage;
  int page;
  int lastPage;
  List<Product> data;

  factory ProductPagination.fromJson(Map<String, dynamic> json) =>
      ProductPagination(
        total: json["total"],
        perPage: json["perPage"],
        page: json["page"],
        lastPage: json["lastPage"],
        data: List<Product>.from(json["data"].map((x) => Product.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "perPage": perPage,
        "page": page,
        "lastPage": lastPage,
        "data": List<Product>.from(data.map((x) => x.toJson())),
      };
}
