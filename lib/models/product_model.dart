// To parse this JSON data, do
//
//     final product = productFromJson(jsonString);

import 'dart:convert';

import 'variant_model.dart';

Product productFromJson(String str) => Product.fromJson(json.decode(str));

String productToJson(Product data) => json.encode(data.toJson());

class Product {
  Product({
    required this.id,
    required this.createdAt,
    required this.name,
    required this.brandId,
    this.catalogId,
    required this.reviewCount,
    required this.reviewSummation,
    required this.rating,
    required this.featured,
    required this.order,
    required this.active,
    this.code,
    required this.condition,
    required this.description,
    required this.viewCount,
    required this.updatedAt,
    required this.editedBy,
    required this.createdBy,
    required this.slug,
    required this.minPrice,
    required this.variants,
  });

  final int id;
  final DateTime createdAt;
  final String name;
  final int brandId;
  final int? catalogId;
  final int reviewCount;
  final int reviewSummation;
  final int rating;
  final int featured;
  final int order;
  final int active;
  final dynamic code;
  final String condition;
  final String description;
  final int viewCount;
  final DateTime updatedAt;
  final int editedBy;
  final int createdBy;
  final String slug;
  final int minPrice;
  final List<Variant> variants;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"] ?? 0,
        createdAt: json["created_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["created_at"]),
        name: json["name"] ?? '',
        brandId: json["brand_id"] ?? 0,
        catalogId: json["catalog_id"],
        reviewCount: json["review_count"] ?? 0,
        reviewSummation: json["review_summation"] ?? 0,
        rating: json["rating"] ?? 0,
        slug: json["slug"] ?? 0,
        featured: json["featured"] ?? 0,
        order: json["order"] ?? 0,
        active: json["active"] ?? 1,
        code: json["code"],
        condition: json["condition"] ?? '',
        description: json["description"] ?? '',
        viewCount: json["view_count"] ?? 0,
        createdBy: json["created_by"] ?? 0,
        editedBy: json["edited_by"] ?? 0,
        updatedAt: json["updated_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["updated_at"]),
        minPrice: json["min_price"] ?? 0,
        variants: List<Variant>.from(
            json["variants"].map((x) => Variant.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "brand_id": brandId,
        "catalog_id": catalogId,
        "review_count": reviewCount,
        "review_summation": reviewSummation,
        "rating": rating,
        "slug": slug,
        "featured": featured,
        "order": order,
        "active": active,
        "code": code,
        "condition": condition,
        "description": description,
        "view_count": viewCount,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "created_by": createdBy,
        "edited_by": editedBy,
        "min_price": minPrice,
        "variants": List<dynamic>.from(variants.map((x) => x.toJson())),
      };
}
