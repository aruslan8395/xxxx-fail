class FirebaseUser {
  FirebaseUser({
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.uid,
    required this.photoUrl,
    required this.displayName,
  });

  final String firstName;
  final String lastName;
  final String email;
  final String uid;
  final String photoUrl;
  final String displayName;
}
