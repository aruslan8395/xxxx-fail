// To parse this JSON data, do
//
//     final customer = customerFromJson(jsonString);

import 'dart:convert';

Customer customerFromJson(String str) => Customer.fromJson(json.decode(str));

String customerToJson(Customer data) => json.encode(data.toJson());

class Customer {
  Customer({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.displayName,
    required this.phone,
    required this.email,
    this.socialMediaUserId,
    this.socialMediaProvider,
    this.emailVerifiedAt,
    this.billingAddress,
    this.shippingAddress,
  });

  final int id;
  final String firstName;
  final String lastName;
  final String displayName;
  final String phone;
  final String email;
  final dynamic socialMediaUserId;
  final dynamic socialMediaProvider;
  final dynamic emailVerifiedAt;
  final dynamic billingAddress;
  final dynamic shippingAddress;

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        displayName: json["display_name"],
        phone: json["phone"],
        email: json["email"],
        socialMediaUserId: json["social_media_user_id"],
        socialMediaProvider: json["social_media_provider"],
        emailVerifiedAt: json["email_verified_at"],
        billingAddress: json["billing_address"],
        shippingAddress: json["shipping_address"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "last_name": lastName,
        "display_name": displayName,
        "phone": phone,
        "email": email,
        "social_media_user_id": socialMediaUserId,
        "social_media_provider": socialMediaProvider,
        "email_verified_at": emailVerifiedAt,
        "billing_address": billingAddress,
        "shipping_address": shippingAddress,
      };
}
