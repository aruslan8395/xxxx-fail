class Photo {
  Photo({
    required this.id,
    required this.variantId,
    required this.image,
    required this.active,
    required this.createdAt,
    required this.updatedAt,
    required this.createdBy,
    this.editedBy,
    required this.alternativeText,
    required this.order,
    required this.originalLink,
    required this.thumbnailLink,
  });

  final int id;
  final int variantId;
  final String image;
  final int active;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int createdBy;
  final dynamic editedBy;
  final String alternativeText;
  final int order;
  final String originalLink;
  final String thumbnailLink;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"] ?? 0,
        variantId: json["variant_id"] ?? 0,
        image: json["image"] ?? '',
        active: json["active"] ?? 0,
        createdAt: json["created_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["updated_at"]),
        createdBy: json["created_by"] ?? 0,
        editedBy: json["edited_by"],
        alternativeText: json["alternative_text"] ?? '',
        order: json["order"] ?? 0,
        originalLink: json["original_link"] ?? '',
        thumbnailLink: json["thumbnail_link"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "variant_id": variantId,
        "image": image,
        "active": active,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "created_by": createdBy,
        "edited_by": editedBy,
        "alternative_text": alternativeText,
        "order": order,
        "original_link": originalLink,
        "thumbnail_link": thumbnailLink,
      };
}
