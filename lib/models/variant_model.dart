import 'photo_model.dart';

class Variant {
  Variant({
    required this.id,
    required this.productId,
    required this.name,
    required this.slug,
    required this.price,
    this.discount,
    this.stock,
    this.code,
    this.preOrderDay,
    required this.finishing,
    required this.material,
    this.length,
    this.width,
    this.height,
    required this.weight,
    this.icon,
    this.active,
    this.notes,
    this.createdAt,
    this.updatedAt,
    required this.createdBy,
    this.editedBy,
    this.alternativeText,
    this.quota,
    this.percentage,
    this.maxOrderPerInvoice,
    this.startDate,
    this.endDate,
    this.order,
    required this.iconLink,
    required this.photos,
  });

  final int id;
  final int productId;
  final String name;
  final String slug;
  final int price;
  final int? discount;
  final int? stock;
  final String? code;
  final int? preOrderDay;
  final String finishing;
  final String material;
  final String? length;
  final String? width;
  final String? height;
  final int weight;
  final String? icon;
  final int? active;
  final String? notes;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final int createdBy;
  final int? editedBy;
  final String? alternativeText;
  final int? quota;
  final int? percentage;
  final int? maxOrderPerInvoice;
  final String? startDate;
  final String? endDate;
  final int? order;
  final String iconLink;
  final List<Photo> photos;

  factory Variant.fromJson(Map<String, dynamic> json) => Variant(
        id: json["id"],
        productId: json["product_id"],
        name: json["name"],
        slug: json["slug"],
        price: json["price"],
        discount: json["discount"],
        stock: json["stock"] ?? 0,
        code: json["code"],
        preOrderDay: json["pre_order_day"],
        finishing: json["finishing"],
        material: json["material"],
        length: json["length"] ?? '0',
        width: json["width"] ?? '0',
        height: json["height"] ?? '0',
        weight: json["weight"],
        icon: json["icon"],
        active: json["active"] ?? true,
        notes: json["notes"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        createdBy: json["created_by"],
        editedBy: json["edited_by"] ?? 0,
        alternativeText: json["alternative_text"],
        quota: json["quota"],
        percentage: json["percentage"],
        maxOrderPerInvoice: json["max_order_per_invoice"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        order: json["order"] ?? 0,
        iconLink: json["icon_link"] ?? '',
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_id": productId,
        "name": name,
        "slug": slug,
        "price": price,
        "discount": discount,
        "stock": stock,
        "code": code,
        "pre_order_day": preOrderDay,
        "finishing": finishing,
        "material": material,
        "length": length,
        "width": width,
        "height": height,
        "weight": weight,
        "icon": icon,
        "active": active,
        "notes": notes,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "created_by": createdBy,
        "edited_by": editedBy,
        "alternative_text": alternativeText,
        "quota": quota,
        "percentage": percentage,
        "max_order_per_invoice": maxOrderPerInvoice,
        "start_date": startDate,
        "end_date": endDate,
        "order": order,
        "icon_link": iconLink,
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
      };
}
