// To parse this JSON data, do
//
//     final banner = bannerFromJson(jsonString);

import 'dart:convert';

import 'package:equatable/equatable.dart';

// ignore_for_file: argument_type_not_assignable
Banner bannerFromJson(String str) => Banner.fromJson(json.decode(str));

String bannerToJson(Banner data) => json.encode(data.toJson());

class Banner extends Equatable {
  const Banner({
    required this.id,
    this.title,
    this.description,
    this.term,
    required this.url,
    required this.image,
    required this.mobileImage,
    required this.positioning,
    required this.imageLink,
    required this.mobileImageLink,
  });

  final int id;
  final String? title;
  final String? description;
  final String? term;
  final String url;
  final String image;
  final String mobileImage;
  final String? positioning;
  final String imageLink;
  final String mobileImageLink;

  @override
  List<Object> get props =>
      [id, url, image, mobileImage, imageLink, mobileImageLink];

  @override
  String toString() =>
      'Banner {id: $id, title: $title, description: $description, term: $term, url: $url, image: $image, mobileImage: $mobileImage, positioning: $positioning, imageLink: $imageLink, mobileImageLink: $mobileImageLink}';

  factory Banner.fromJson(Map<String, dynamic> json) => Banner(
        id: json["id"] ?? 0,
        title: json["title"] ?? '',
        description: json["description"] ?? '',
        term: json["term"] ?? '',
        url: json["url"] ?? '',
        image: json["image"] ?? '',
        mobileImage: json["mobile_image"] ?? '',
        positioning: json["positioning"] ?? '',
        imageLink: json["image_link"] ?? '',
        mobileImageLink: json["mobile_image_link"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title ?? '',
        "description": description ?? '',
        "term": term ?? '',
        "url": url,
        "image": image,
        "mobile_image": mobileImage,
        "positioning": positioning ?? '',
        "image_link": imageLink,
        "mobile_image_link": mobileImageLink,
      };
}
