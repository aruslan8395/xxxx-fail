import 'package:dio/dio.dart';
import 'package:flutter_unity_widget_example/helpers/constant.dart';
import 'package:flutter_unity_widget_example/models/product_pagination_model.dart';

class ShopService {
  Future<ProductPagination> getShopProductPerPage(int initialPage) async {
    try {
      final String url = '$api/app/products?page=$initialPage';
      final Response response = await Dio().get(url);

      final ProductPagination pagination =
          ProductPagination.fromJson(response.data['data']);

      return pagination;
    } catch (error) {
      throw Exception(error);
    }
  }
}

ShopService shopService = ShopService();
