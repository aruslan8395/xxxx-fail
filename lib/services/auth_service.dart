import 'dart:convert';
import 'dart:math';
import 'package:crypto/crypto.dart';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_unity_widget_example/helpers/constant.dart';
import 'package:flutter_unity_widget_example/models/customer_model.dart';
import 'package:flutter_unity_widget_example/models/firebase_model.dart';
import 'package:flutter_unity_widget_example/models/status_model.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:url_launcher/url_launcher.dart';

class AuthService {
  final Dio _dio = Dio();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<String?> loginWithCredentials(Map<String, dynamic> credentials) async {
    try {
      final String url = '$api/customers/login';

      // Get token
      final Response response = await _dio.post(
        url,
        data: credentials,
        options: Options(contentType: Headers.formUrlEncodedContentType),
      );
      print('response: $response');

      final String token = response.data['data'] as String;

      await GetStorage().write('token', token);
      getCustomer(token);
      return token;
    } catch (error) {
      print(error);
      throw Exception(error);
    }
  }

  String generateNonce([int length = 32]) {
    const charset =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  Future<Customer> getCustomer(String token) async {
    try {
      final String url = '$api/customers';
      final Response response = await _dio.get(url,
          options: Options(
              contentType: Headers.formUrlEncodedContentType,
              headers: {"Authorization": "Bearer $token"}));
      final Customer customer = Customer.fromJson(response.data['data']);
      await GetStorage().write('customer', json.encode(customer.toJson()));

      return customer;
    } catch (error) {
      throw Exception(error);
    }
  }

  Future<String> forgotPassword(String email) async {
    try {
      final String url = '$api/customers/password-reset';
      final Response response = await _dio.post(
        url,
        data: {"email": email},
        options: Options(contentType: Headers.formUrlEncodedContentType),
      );
      final Status status = Status.fromJson(response.data);
      final String message = status.message;

      return message;
    } on DioError catch (error) {
      final Status status = Status.fromJson(error.response!.data);
      final String message = status.message;

      return message;
    }
  }

  Future<bool> logout() async {
    try {
      GetStorage().remove('token');
      GetStorage().remove('customer');
      return true;
    } catch (error) {
      return false;
    }
  }

  Future<Status> register(Map<String, dynamic> data) async {
    try {
      final String url = '$api/customers/register';

      // Get token
      final Response response = await _dio.post(
        url,
        data: data,
        options: Options(contentType: Headers.formUrlEncodedContentType),
      );

      final Status status = Status.fromJson(response.data);

      return status;
    } catch (error) {
      throw Exception(error);
    }
  }

  Future<int> launchUrl(String link) async {
    await canLaunch(link) ? await launch(link) : throw 'Could not launch link';
    return 1;
  }

  Future<FirebaseUser?> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount? googleUser = await googleSignIn.signIn();

    if (googleUser == null) {
      return null;
    }

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    // Create a new credential
    final OAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    // Once signed in, return the UserCredential
    final UserCredential userCreds =
        await FirebaseAuth.instance.signInWithCredential(credential);
    if (userCreds.user != null) {
      return FirebaseUser(
          firstName: userCreds.additionalUserInfo!.profile!['given_name'],
          lastName: userCreds.additionalUserInfo!.profile!['family_name'],
          email: userCreds.user!.email!,
          uid: userCreds.user!.uid,
          photoUrl: userCreds.user!.photoURL!,
          displayName: userCreds.user!.displayName!);
    }
  }

  Future<void> signOut() async {
    await _auth.signOut();
  }

  Future<FirebaseUser?> signInWithFacebook() async {
    try {
      final result = await FacebookAuth.instance.login(
        permissions: [
          'email',
          'public_profile',
        ],
      );

      if (result.status == LoginStatus.success) {
        final userData = await FacebookAuth.instance.getUserData();

        final List<String> userCompleteName =
            userData['name'].toString().split(' ');

        // final AccessToken? accessToken = result.accessToken;

        return FirebaseUser(
            firstName: userCompleteName.first,
            lastName: userCompleteName.last,
            email: userData['email'],
            uid: userData['id'],
            photoUrl: userData['picture']['data']['url'],
            displayName: userData['name']);
      }
      return null;
    } catch (error) {
      throw Exception(error);
    }
  }

  Future<FirebaseUser?> signInWithApple() async {
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    try {
      final appleCredential =
          await SignInWithApple.getAppleIDCredential(scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ], nonce: nonce);

      final oauthCredential = OAuthProvider("apple.com").credential(
        idToken: appleCredential.identityToken,
        rawNonce: rawNonce,
      );

      final authResult = await _auth.signInWithCredential(oauthCredential);

      final firebaseUser = authResult.user;
      if (firebaseUser == null) {
        return null;
      } else {
        final String? displayName = firebaseUser.providerData[0].displayName;
        final String? userEmail = firebaseUser.providerData[0].email;
        final String? uid = firebaseUser.providerData[0].uid;
        final String? photoURL = firebaseUser.providerData[0].photoURL;

        await firebaseUser.updateProfile(displayName: displayName ?? '');
        await firebaseUser.updateEmail(userEmail ?? '');
        final List<String> userCompleteName =
            firebaseUser.displayName.toString().split(' ');
        return FirebaseUser(
          firstName: userCompleteName.first,
          lastName: userCompleteName.last,
          email: userEmail ?? '',
          uid: uid ?? '',
          photoUrl: photoURL ?? '',
          displayName: displayName ?? '',
        );
      }
    } catch (e) {
      throw Exception(e);
    }
  }
}

AuthService authService = AuthService();
