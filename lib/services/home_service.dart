import 'package:dio/dio.dart';
import 'package:flutter_unity_widget_example/helpers/constant.dart';
import 'package:flutter_unity_widget_example/models/banner_model.dart';
import 'package:flutter_unity_widget_example/models/product_model.dart';

class HomeService {
  final Dio _dio = Dio();
  Future<List<Banner>> getAllBanner() async {
    const String url = '$api/banners/featured';
    final List<Banner> banners = [];
    try {
      final Response response = await _dio.get(url);
      for (final element in response.data['data'] as List<dynamic>) {
        banners.add(Banner.fromJson(element));
      }

      return banners;
    } catch (error) {
      if (banners.isEmpty) {
        await Future.delayed(const Duration(seconds: 2), () => getAllBanner());
      }
      throw Exception(error);
    }
  }

  Future<List<Product>> getAllFeaturedProduct() async {
    const String url = '$api/app/products';
    final List<Product> products = [];
    try {
      final Response response = await _dio.get(url);
      for (final element in response.data['data']['data'] as List<dynamic>) {
        products.add(Product.fromJson(element));
      }

      return products;
    } catch (error) {
      if (products.isEmpty) {
        // await Future.delayed(
        //     const Duration(seconds: 2), () => getAllFeaturedProduct());
      }
      throw Exception(error);
    }
  }
}

HomeService homeService = HomeService();
