import 'package:flutter_unity_widget_example/models/banner_model.dart';
import 'package:flutter_unity_widget_example/models/product_model.dart';
import 'package:flutter_unity_widget_example/services/home_service.dart';

import 'package:rxdart/rxdart.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(HomeState initialState) : super(initialState);

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    @override
    // ignore: unused_element
    Stream<Transition<HomeEvent, HomeState>> transformEvent(
        Stream<HomeEvent> events,
        TransitionFunction<HomeEvent, HomeState> transitionFunction) {
      return super.transformEvents(
          events.debounceTime(const Duration(milliseconds: 500)),
          transitionFunction);
    }

    try {
      final List<Banner> banners = await homeService.getAllBanner();
      final List<Product> products = await homeService.getAllFeaturedProduct();

      yield HomeSuccess(
        banners: banners,
        products: products,
      );
    } catch (_) {
      yield HomeFailure();
    }
  }
}
