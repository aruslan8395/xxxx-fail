import 'package:equatable/equatable.dart';
import 'package:flutter_unity_widget_example/models/banner_model.dart';
import 'package:flutter_unity_widget_example/models/product_model.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeFailure extends HomeState {}

class HomeSuccess extends HomeState {
  final List<Product> products;
  final List<Banner> banners;
  const HomeSuccess({required this.banners, required this.products});

  HomeSuccess copyWith({
    required List<Banner> banners,
    required List<Product> products,
  }) {
    return HomeSuccess(banners: banners, products: products);
  }

  @override
  List<Object> get props => [banners, products];

  @override
  String toString() => 'HomeSuccess {banners: $banners, products: $products}';
}
