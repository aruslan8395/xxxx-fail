abstract class LoginEvent {}

class LoginEmailChanged extends LoginEvent {
  final String email;
  LoginEmailChanged({required this.email});
}

class LoginPasswordChanged extends LoginEvent {
  final String password;
  LoginPasswordChanged({required this.password});
}

class LoginWithGoogleSubmitted extends LoginEvent {}

class LoginWithFacebookSubmitted extends LoginEvent {}

class LoginWithAppleSubmitted extends LoginEvent {}

class LoginSubmitted extends LoginEvent {}
