import 'package:flutter_unity_widget_example/models/firebase_model.dart';

import 'form_submission_status.dart';

class LoginState {
  final String email;
  bool get isValidEmail => RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(email);
  final String password;
  bool get isValidPassword => password.length > 6;
  final FirebaseUser? user;
  final FormSubmissionStatus formStatus;

  LoginState({
    this.email = '',
    this.password = '',
    this.user,
    this.formStatus = const InitialFormStatus(),
  });

  LoginState copyWith({
    FirebaseUser? user,
    required String email,
    required String password,
    required FormSubmissionStatus formStatus,
  }) {
    return LoginState(
        email: email, password: password, formStatus: formStatus, user: user);
  }
}
