import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_unity_widget_example/models/firebase_model.dart';
import 'package:flutter_unity_widget_example/services/auth_service.dart';
import 'package:get_storage/get_storage.dart';

import 'form_submission_status.dart';
import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginState());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginEmailChanged) {
      yield state.copyWith(
          email: event.email,
          password: state.password,
          formStatus: state.formStatus);
    } else if (event is LoginPasswordChanged) {
      yield state.copyWith(
        email: state.email,
        password: event.password,
        formStatus: state.formStatus,
      );
    } else if (event is LoginSubmitted) {
      yield state.copyWith(
          email: state.email,
          password: state.password,
          formStatus: FormSubmitting());

      try {
        final String? token = await authService.loginWithCredentials({
          "email": state.email,
          "password": state.password,
        });
        print(token);

        if (token != null) {
          final customer = await authService.getCustomer(token);

          GetStorage().write('token', token);
          GetStorage().write('customer', customer);
          yield state.copyWith(
              email: state.email,
              password: state.password,
              formStatus: SubmissionSuccess());
        } else {
          yield state.copyWith(
              email: state.email,
              password: state.password,
              formStatus: SubmissionFailed(Exception()));
        }
      } catch (error) {
        yield state.copyWith(
            email: state.email,
            password: state.password,
            formStatus: SubmissionFailed(Exception()));
        throw Exception(error);
      }
    } else if (event is LoginWithGoogleSubmitted) {
      yield state.copyWith(
          email: state.email,
          password: state.password,
          formStatus: FormSubmitting());
      FirebaseUser? user;
      try {
        final FirebaseUser? googleUser = await authService.signInWithGoogle();
        if (googleUser != null) {
          user = googleUser;

          final String? token = await authService.loginWithCredentials({
            "email": googleUser.email,
            "password": "Any Password",
            "social_media_provider": "Google",
            "social_media_user_id": googleUser.uid,
          });

          if (token != null) {
            final customer = await authService.getCustomer(token);

            GetStorage().write('token', token);
            GetStorage().write('customer', customer);
            yield state.copyWith(
                email: state.email,
                password: state.password,
                formStatus: SubmissionSuccess());
          }
        }
      } catch (error) {
        yield state.copyWith(
            email: state.email,
            password: state.password,
            user: user,
            formStatus: SubmissionFailed(Exception(error)));
        throw Exception(error);
      }
    } else if (event is LoginWithFacebookSubmitted) {
      FirebaseUser? user;
      try {
        final FirebaseUser? facebookUser =
            await authService.signInWithFacebook();
        if (facebookUser != null) {
          user = facebookUser;

          final String? token = await authService.loginWithCredentials({
            "email": user.email,
            "password": "Any Password",
            "social_media_provider": "Facebook",
            "social_media_user_id": user.uid,
          });

          if (token != null) {
            final customer = await authService.getCustomer(token);

            GetStorage().write('token', token);
            GetStorage().write('customer', customer);
            yield state.copyWith(
                email: state.email,
                password: state.password,
                formStatus: SubmissionSuccess());
          }
        }
      } catch (error) {
        yield state.copyWith(
            email: state.email,
            password: state.password,
            user: user,
            formStatus: SubmissionFailed(Exception(error)));
        throw Exception(error);
      }
    } else if (event is LoginWithAppleSubmitted) {
      FirebaseUser? user;
      try {
        final FirebaseUser? appleUser = await authService.signInWithApple();
        if (appleUser != null) {
          user = appleUser;

          final String? token = await authService.loginWithCredentials({
            "email": user.email,
            "password": "Any Password",
            "social_media_provider": "Apple",
            "social_media_user_id": user.uid,
          });
          if (token != null) {
            final customer = await authService.getCustomer(token);

            GetStorage().write('token', token);
            GetStorage().write('customer', customer);
            yield state.copyWith(
                email: state.email,
                password: state.password,
                formStatus: SubmissionSuccess());
          }
        }
      } catch (error) {
        yield state.copyWith(
            email: state.email,
            password: state.password,
            user: user,
            formStatus: SubmissionFailed(Exception(error)));
        throw Exception(error);
      }
    }
  }
}
