import 'package:equatable/equatable.dart';

abstract class ShopEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RequestProduct extends ShopEvent {}

class DebugProduct extends ShopEvent {}
