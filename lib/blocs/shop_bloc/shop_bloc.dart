import 'package:flutter_unity_widget_example/models/product_pagination_model.dart';
import 'package:flutter_unity_widget_example/services/shop_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'shop_event.dart';
import 'shop_state.dart';

class ShopBloc extends Bloc<ShopEvent, ShopState> {
  ShopBloc()
      : super(
          ShopState(
            pagination: ProductPagination(
                total: 0, perPage: 0, page: 0, lastPage: 0, data: []),
          ),
        );

  @override
  Stream<Transition<ShopEvent, ShopState>> transformEvents(
    Stream<ShopEvent> events,
    TransitionFunction<ShopEvent, ShopState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<ShopState> mapEventToState(ShopEvent event) async* {
    if (event is RequestProduct) {
      yield state.copyWith(
        isFetching: true,
        status: state.status,
        hasReachedMax: state.hasReachedMax,
        pagination: state.pagination,
      );

      yield await _mapShopToState(state);
    }
  }

  Future<ShopState> _mapShopToState(
    ShopState state,
  ) async {
    ProductPagination currentPagination = state.pagination;

    bool hasReachedMax = false;

    try {
      if (state.hasReachedMax) {
        return state.copyWith(
          isFetching: false,
          hasReachedMax: state.hasReachedMax,
          pagination: currentPagination,
          status: state.status,
        );
      }

      final ProductPagination fetchPagination =
          await shopService.getShopProductPerPage(
        state.pagination.page == 0 ? 1 : state.pagination.page + 1,
      );
      final tempData = currentPagination.data;
      currentPagination = fetchPagination;
      if (currentPagination.page != 0) {
        currentPagination.data = List.of(tempData)
          ..addAll(currentPagination.data);
      }
      if (fetchPagination.page == fetchPagination.lastPage) {
        hasReachedMax = true;
      }

      return ShopState(
        pagination: currentPagination,
        hasReachedMax: hasReachedMax,
        status: ShopStatus.success,
      );
    } catch (e) {
      return ShopState(
        pagination: currentPagination,
        status: ShopStatus.failure,
        hasReachedMax: hasReachedMax,
      );
    }
  }
}
