import 'package:equatable/equatable.dart';
import 'package:flutter_unity_widget_example/models/product_pagination_model.dart';

enum ShopStatus { initial, success, failure }

class ShopState extends Equatable {
  const ShopState(
      {this.status = ShopStatus.initial,
      required this.pagination,
      this.isFetching = false,
      this.hasReachedMax = false});

  final ShopStatus status;
  final ProductPagination pagination;

  final bool isFetching;
  final bool hasReachedMax;

  ShopState copyWith(
      {required ShopStatus status,
      required ProductPagination pagination,
      required bool isFetching,
      required bool hasReachedMax}) {
    return ShopState(
        status: status,
        pagination: pagination,
        isFetching: isFetching,
        hasReachedMax: hasReachedMax);
  }

  @override
  List<Object> get props => [status, pagination, isFetching, hasReachedMax];
}
