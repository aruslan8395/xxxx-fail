import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/form_submission_status.dart';

class RegisterState {
  final String firstName;
  bool get isValidFirstName => firstName.isNotEmpty;
  final String lastName;
  bool get isValidLastName => lastName.isNotEmpty;
  final String displayName;
  bool get isValidDisplayName => displayName.isNotEmpty;
  final String phoneNumber;
  bool get isValidPhoneNumber => phoneNumber.length > 9;
  final String email;
  bool get isValidEmail => RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(email);
  final String password;
  bool get isValidPassword => password.length > 6;

  final FormSubmissionStatus formStatus;
  RegisterState({
    this.firstName = '',
    this.lastName = '',
    this.displayName = '',
    this.phoneNumber = '',
    this.email = '',
    this.password = '',
    this.formStatus = const InitialFormStatus(),
  });

  RegisterState copyWith({
    required String firstName,
    required String lastName,
    required String displayName,
    required String phoneNumber,
    required String email,
    required String password,
    required FormSubmissionStatus formStatus,
  }) {
    return RegisterState(
      firstName: firstName,
      lastName: lastName,
      displayName: displayName,
      phoneNumber: phoneNumber,
      email: email,
      password: password,
      formStatus: formStatus,
    );
  }
}
