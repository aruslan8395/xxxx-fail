import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/form_submission_status.dart';
import 'package:flutter_unity_widget_example/models/status_model.dart';
import 'package:flutter_unity_widget_example/services/auth_service.dart';

import 'register_event.dart';
import 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc() : super(RegisterState());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterEmailChanged) {
      yield state.copyWith(
        firstName: state.firstName,
        lastName: state.lastName,
        displayName: state.displayName,
        phoneNumber: state.phoneNumber,
        email: event.email,
        password: state.password,
        formStatus: state.formStatus,
      );
    } else if (event is RegisterPasswordChanged) {
      yield state.copyWith(
        firstName: state.firstName,
        lastName: state.lastName,
        displayName: state.displayName,
        phoneNumber: state.phoneNumber,
        email: state.email,
        password: event.password,
        formStatus: state.formStatus,
      );
    } else if (event is RegisterFirstNameChanged) {
      yield state.copyWith(
        firstName: event.firstName,
        lastName: state.lastName,
        displayName: state.displayName,
        phoneNumber: state.phoneNumber,
        email: state.email,
        password: state.password,
        formStatus: state.formStatus,
      );
    } else if (event is RegisterLastNameChanged) {
      yield state.copyWith(
        firstName: state.firstName,
        lastName: event.lastName,
        displayName: state.displayName,
        phoneNumber: state.phoneNumber,
        email: state.email,
        password: state.password,
        formStatus: state.formStatus,
      );
    } else if (event is RegisterDisplayNameChanged) {
      yield state.copyWith(
        firstName: state.firstName,
        lastName: state.lastName,
        displayName: event.displayName,
        phoneNumber: state.phoneNumber,
        email: state.email,
        password: state.password,
        formStatus: state.formStatus,
      );
    } else if (event is RegisterPhoneNumberChanged) {
      yield state.copyWith(
        firstName: state.firstName,
        lastName: state.lastName,
        displayName: state.displayName,
        phoneNumber: event.phoneNumber,
        email: state.email,
        password: state.password,
        formStatus: state.formStatus,
      );
    } else if (event is RegisterSubmitted) {
      yield state.copyWith(
        firstName: state.firstName,
        lastName: state.lastName,
        displayName: state.displayName,
        phoneNumber: state.phoneNumber,
        email: state.email,
        password: state.password,
        formStatus: FormSubmitting(),
      );
      try {
        final Status status = await authService.register({
          "first_name": state.firstName,
          "last_name": state.lastName,
          "phone": state.phoneNumber,
          "display_name": state.displayName,
          "email": state.email,
          "password": state.password,
        });

        if (status.status == "success") {
          yield state.copyWith(
              email: state.email,
              password: state.password,
              firstName: state.firstName,
              lastName: state.lastName,
              displayName: state.displayName,
              phoneNumber: state.phoneNumber,
              formStatus: SubmissionSuccess());
        }
      } catch (error) {
        yield state.copyWith(
          email: state.email,
          password: state.password,
          firstName: state.firstName,
          lastName: state.lastName,
          displayName: state.displayName,
          phoneNumber: state.phoneNumber,
          formStatus: SubmissionFailed(Exception(error)),
        );

        throw Exception(error);
      }
    }
  }
}
