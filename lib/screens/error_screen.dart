import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'home_screen.dart';

class ErrorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: masterColor('primary'),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: 100,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/gear-image.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(height: 61),
            textWidget(
              text: 'Terjadi Kesalahan',
              fontFamily: 'GT Walsheim Pro',
              size: 16,
              letterSpacing: 1.2,
              color: 'secondary',
            ),
            textWidget(
              text: 'Coba muat ulang halaman ini',
              color: 'secondary',
              weight: 'L',
              fontFamily: 'GT Walsheim Pro',
            ),
            const SizedBox(height: 32),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: masterColor('secondary'),
                padding: EdgeInsets.symmetric(horizontal: 0.1.sw),
              ),
              onPressed: () => pushNewScreen(context, screen: HomeScreen()),
              child: textWidget(
                text: 'MUAT ULANG',
                fontFamily: 'Dosis',
                size: 16,
                weight: 'B',
                color: 'primary',
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: masterColor('secondary'),
                padding: EdgeInsets.symmetric(horizontal: 0.1.sw),
              ),
              onPressed: () => pushNewScreen(context, screen: HomeScreen()),
              child: textWidget(
                text: 'HALAMAN UTAMA',
                fontFamily: 'Dosis',
                size: 16,
                weight: 'B',
                color: 'primary',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
