import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/widgets/forgot_password_screen/forgot_password_form.dart';
import 'package:flutter_unity_widget_example/widgets/shared/dekornata_auth_logo.dart';

class ForgotPasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Stack(
        children: [
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                color: masterColor('primary'),
              ),
            ),
          ),
          Container(
            height: 1.sh,
            width: 1.sw,
            padding: EdgeInsets.fromLTRB(0.11.sw, 0.15.sh, 0.11.sw, 0.02.sh),
            child: Column(
              children: [
                DekornataAuthLogo(),
                textKeyWidget(
                  text: 'forgot',
                  size: 24,
                  color: 'secondary',
                  letterSpacing: 1.2,
                ),
                textKeyWidget(
                  text: 'password-forgot',
                  size: 24,
                  color: 'secondary',
                  weight: 'B',
                  letterSpacing: 1.2,
                ),
                ForgotPasswordForm(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 0.03.sh),
              child: trademarkText(),
            ),
          )
        ],
      ),
    );
  }
}
