import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/constant.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/product_model.dart';
import 'package:flutter_unity_widget_example/widgets/detail_screen.dart/product_description.dart';
import 'package:flutter_unity_widget_example/widgets/detail_screen.dart/product_information.dart';
import 'package:flutter_unity_widget_example/widgets/detail_screen.dart/sliver_header_delegate.dart';
import 'package:get/state_manager.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable
class DetailScreen extends StatefulWidget {
  final Product product;
  const DetailScreen(this.product);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  RxInt index = 0.obs;
  RxInt currentIndex = 0.obs;
  RxInt orderCount = 1.obs;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: textWidget(
            text: widget.product.name,
            fontFamily: 'GT Walsheim Pro',
            weight: 'B',
            color: 'secondary',
            textOverflow: TextOverflow.ellipsis,
            size: 18,
          ),
          backgroundColor: masterColor('primary'),
          automaticallyImplyLeading: true,
        ),
        body: Stack(
          children: [
            CustomScrollView(
              slivers: [
                SliverPersistentHeader(
                    delegate: ProductDetailPhotoCarousel(
                        widget.product, 0.5.sh, currentIndex, index)),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 0.05.sw),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 0.006.sh),
                        Obx(
                          () => Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: widget
                                .product.variants[index.value].photos
                                .map((photo) {
                              final int photoIndex = widget
                                  .product.variants[index.value].photos
                                  .indexOf(photo);
                              return Container(
                                width: 8,
                                height: 8,
                                margin: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 2),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: currentIndex.value == photoIndex
                                      ? masterColor('grey')
                                      : masterColor('secondary'),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                        const SizedBox(height: 10),
                        textWidget(
                          text: 'VARIAN PRODUK',
                          fontFamily: 'GT Walsheim Pro',
                          weight: 'SB',
                          size: 12,
                          color: 'primary',
                        ),
                        const SizedBox(height: 16),
                        SizedBox(
                          height: 0.1.sh,
                          child: ListView(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            children: widget.product.variants
                                .mapIndexed((int i, variant) {
                              return GestureDetector(
                                onTap: () {
                                  index.value = i;
                                },
                                child: Obx(
                                  () => Container(
                                    margin: const EdgeInsets.all(10),
                                    width: 0.4.sw,
                                    height: 0.1.sh,
                                    decoration: BoxDecoration(
                                      border: index.value == i
                                          ? Border.all(color: Colors.amber)
                                          : Border.all(width: 0),
                                      borderRadius: BorderRadius.circular(10),
                                      color: masterColor('disabled'),
                                    ),
                                    child: Row(
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Image.network(
                                            variant.photos[0].thumbnailLink,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        const SizedBox(width: 2),
                                        Flexible(
                                          child: textWidget(
                                              text: variant.name,
                                              textOverflow:
                                                  TextOverflow.ellipsis),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                        Divider(
                          color: masterColor('primary'),
                        ),
                        const SizedBox(height: 10),
                        Obx(
                          () => textWidget(
                            text: widget.product.variants[index.value].name,
                            fontFamily: 'GT Walsheim Pro',
                            letterSpacing: 0.5,
                            color: 'primary',
                            maxLines: 2,
                            textOverflow: TextOverflow.ellipsis,
                          ),
                        ),
                        const SizedBox(height: 2),
                        Obx(
                          () => Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  textWidget(
                                    text:
                                        'IDR ${convertNumberToCurrency(widget.product.variants[index.value].price)}',
                                    fontFamily: 'GT Walsheim Pro',
                                    weight: 'B',
                                    decoration: widget
                                                .product
                                                .variants[index.value]
                                                .discount ==
                                            null
                                        ? TextDecoration.none
                                        : TextDecoration.lineThrough,
                                    color: 'primary',
                                  ),
                                  const SizedBox(width: 15),
                                  if (widget.product.variants[index.value]
                                          .discount ==
                                      null)
                                    const SizedBox()
                                  else
                                    Container(
                                      padding: const EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                          color: masterColor('danger'),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: textWidget(
                                        text:
                                            '${((widget.product.variants[index.value].price - widget.product.variants[index.value].discount!) / widget.product.variants[index.value].price * 100).toInt()} %',
                                        fontFamily: 'GT Walsheim Pro',
                                        size: 10,
                                        color: 'secondary',
                                      ),
                                    )
                                ],
                              ),
                              if (widget
                                      .product.variants[index.value].discount ==
                                  null)
                                const SizedBox()
                              else
                                textWidget(
                                  text:
                                      'IDR ${convertNumberToCurrency(widget.product.variants[index.value].discount!)}',
                                  fontFamily: 'GT Walsheim Pro',
                                  weight: 'B',
                                  size: 18,
                                  color: 'danger',
                                )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 0.03.sh,
                          child: Row(
                            children: [
                              if (widget.product.rating == 0)
                                for (int i = 0; i < 5; i++)
                                  const Icon(
                                    Icons.star_border_outlined,
                                    color: Colors.amber,
                                  )
                              else
                                for (int i = 0; i < widget.product.rating; i++)
                                  const Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                            ],
                          ),
                        ),
                        SizedBox(height: 0.01.sh),
                        // Increment_counter(orderCount: orderCount, widget: widget, index: index),
                        Obx(
                          () => textWidget(
                            text:
                                'Stok ${convertNumberToCurrency(widget.product.variants[index.value].stock!)}',
                            fontFamily: 'GT Walsheim Pro',
                            size: 12,
                            letterSpacing: 0.5,
                            color: 'grey',
                          ),
                        ),
                        SizedBox(height: 0.02.sh),
                        const Divider(thickness: 1),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TabBar(
                              labelColor: masterColor("primary"),
                              controller: tabController,
                              unselectedLabelColor: masterColor("grey"),
                              isScrollable: true,
                              indicatorSize: TabBarIndicatorSize.tab,
                              indicatorColor: masterColor("primary"),
                              tabs: <Widget>[
                                Tab(
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: 0.3.sw,
                                    child: textWidget(
                                      text: 'DESCRIPTIONS',
                                      fontFamily: 'GT Walsheim Pro',
                                      weight: 'SB',
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 0.3.sw,
                                  alignment: Alignment.center,
                                  child: Tab(
                                    child: textWidget(
                                      text: 'INFORMATION',
                                      fontFamily: 'GT Walsheim Pro',
                                      weight: 'SB',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Obx(
                          () => Container(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            height: 0.25.sh,
                            child: ScrollConfiguration(
                              behavior: const ScrollBehavior()
                                ..buildViewportChrome(context, const SizedBox(),
                                    AxisDirection.down),
                              child: TabBarView(
                                controller: tabController,
                                children: [
                                  ProductDescription(
                                      widget.product.description),
                                  SingleChildScrollView(
                                    child: ProductInformation(
                                        widget.product, index.value),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 0.1.sh)
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              left: 0,
              bottom: 0.015.sh,
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: masterColor('primary'),
                ),
                height: 0.07.sh,
                width: 1.sw,
                child: GestureDetector(
                  onTap: () async {
                    await canLaunch('${shop}products/${widget.product.slug}')
                        ? await launch('${shop}products/${widget.product.slug}')
                        : throw 'Could not launch ${'$api/products/${widget.product.slug}'}';
                  },
                  child: textWidget(
                    text: 'BELI LANGSUNG',
                    fontFamily: 'Dosis',
                    weight: 'B',
                    color: 'secondary',
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
