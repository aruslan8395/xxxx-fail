import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/screens/login_screen.dart';

class ProfileScreenNotLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: 100,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/gear-image.png'))),
            ),
            textWidget(
              align: TextAlign.center,
              text: 'HALAMAN INI SEDANG DALAM\nPROSES PEMBUATAN',
              color: 'primary',
              fontFamily: 'Dosis',
              weight: 'B',
              size: 16,
              letterSpacing: 1.2,
              maxLines: 2,
            ),
            const SizedBox(height: 20),
            textWidget(
              text: 'Tunggu update selanjutnya ya!',
              color: 'primary',
              fontFamily: 'GT Walsheim Pro',
            ),
            const SizedBox(height: 32),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: masterColor('primary'),
                  padding: EdgeInsets.symmetric(horizontal: 0.1.sw),
                ),
                onPressed: () {
                  pushNewScreenWithRouteSettings(context,
                      screen: LoginScreen(),
                      settings: const RouteSettings(name: '/login'),
                      withNavBar: false);
                },
                child: textWidget(
                  text: 'LOGIN',
                  fontFamily: 'Dosis',
                  size: 16,
                  weight: 'B',
                  color: 'secondary',
                )),
          ],
        ),
      ),
    );
  }
}
