import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/shop_bloc/shop_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/shop_bloc/shop_event.dart';
import 'package:flutter_unity_widget_example/widgets/shop_screen/shop_screen.dart';

class ShopScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ShopBloc>(
      create: (context) => ShopBloc()
        ..add(
          RequestProduct(),
        ),
      child: ShopMaster(),
    );
  }
}
