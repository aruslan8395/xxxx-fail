import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_unity_widget_example/helpers/dekornata_icon.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_unity_widget_example/services/auth_service.dart';
import 'package:flutter_unity_widget_example/widgets/app_navbar.dart';

import 'package:get_storage/get_storage.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class ProfileScreen extends StatelessWidget {
  final data = GetStorage().read('customer');

  @override
  Widget build(BuildContext context) {
    // Customer customer = Customer.fromJson(data);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: masterColor('primary'),
        title: textWidget(
          text: 'PROFIL',
          fontFamily: 'Dosis',
          weight: 'B',
          color: 'secondary',
          letterSpacing: 1.5,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(15),
            child: Icon(DekornataIcon.profileIcon),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 0.08.sw, vertical: 0.04.sh),
        child: Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  radius: 0.15.sw,
                  backgroundImage: AssetImage(
                    'assets/images/profile-picture.png',
                  ),
                ),
                SizedBox(width: 20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    textWidget(
                      text: data.displayName,
                      fontFamily: 'GT Walsheim Pro',
                      weight: 'B',
                      letterSpacing: 1.2,
                      color: 'primary',
                    ),
                    SizedBox(height: 8),
                    textWidget(
                      text: data.email,
                      fontFamily: 'GT Walsheim Pro',
                      size: 12,
                      color: 'primary',
                    )
                  ],
                )
              ],
            ),
            SizedBox(
              height: 0.04.sh,
            ),
            Divider(),
            SizedBox(height: 10),
            InkWell(
              onTap: () async {
                bool logout = await authService.logout();
                if (logout) {
                  pushNewScreenWithRouteSettings(context,
                      screen: NavBar(),
                      settings: const RouteSettings(name: '/home'),
                      withNavBar: false,
                      pageTransitionAnimation: PageTransitionAnimation.sizeUp);
                }
              },
              child: Ink(
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.grey.shade300,
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset(
                        'assets/icons/svg/logout.svg',
                        height: 26,
                      ),
                    ),
                    SizedBox(width: 0.05.sw),
                    textWidget(
                      text: 'Keluar',
                      fontFamily: 'GT Walsheim Pro',
                      color: 'primary',
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
