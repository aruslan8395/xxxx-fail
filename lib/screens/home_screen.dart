import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/blocs/home_bloc/home_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/home_bloc/home_event.dart';
import 'package:flutter_unity_widget_example/blocs/home_bloc/home_state.dart';
import 'package:flutter_unity_widget_example/widgets/home_screen/home_master.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(HomeInitial())..add(HomeFetched()),
      child: HomeMaster(),
    );
  }
}
