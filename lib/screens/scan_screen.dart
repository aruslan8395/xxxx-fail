import 'package:flutter/material.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/widgets/app_navbar.dart';

class ScanScreen extends StatefulWidget {
  ScanScreen({Key? key}) : super(key: key);

  @override
  _ScanScreenState createState() => _ScanScreenState();
}

class _ScanScreenState extends State<ScanScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    unityWidgetController.unload();
    unityWidgetController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // key: _scaffoldKey,
      body: Stack(
        children: [
          UnityWidget(
            fullscreen: true,
            onUnityCreated: _onUnityCreated,
            onUnityMessage: onUnityMessage,
            onUnitySceneLoaded: onUnitySceneLoaded,
          ),
          SafeArea(
            child: Container(
              padding: EdgeInsets.only(left: 0.05.sw),
              alignment: Alignment.topLeft,
              child: IconButton(
                icon: Icon(Icons.close),
                color: masterColor('primary'),
                onPressed: () {
                  isHide = false;
                  unityWidgetController.pause();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => NavBar()));
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  void setRotationSpeed(String speed) {
    unityWidgetController.postMessage(
      'Cube',
      'SetRotationSpeed',
      speed,
    );
  }

  void onUnityMessage(message) {
    print('Received message from unity: ${message.toString()}');
  }

  void onUnitySceneLoaded(SceneLoaded? scene) {
    print('Received scene loaded from unity: ${scene!.name}');
    print('Received scene loaded from unity buildIndex: ${scene.buildIndex}');
  }

  // Callback that connects the created controller to the unity controller
  void _onUnityCreated(controller) {
    unityWidgetController = controller;
  }
}
