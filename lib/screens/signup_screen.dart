import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/register_bloc/register_bloc.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/firebase_model.dart';
import 'package:flutter_unity_widget_example/widgets/signup_screen.dart/signup_form.dart';

class SignUpScreen extends StatelessWidget {
  final FirebaseUser? user;
  const SignUpScreen({this.user});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Stack(
        children: [
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                color: masterColor('primary'),
              ),
            ),
          ),
          Container(
            height: 1.sh,
            width: 1.sw,
            padding: EdgeInsets.fromLTRB(45, 0.1.sh, 50, 10),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  textKeyWidget(
                    text: 'create-account',
                    size: 24,
                    color: 'secondary',
                    letterSpacing: 1.2,
                  ),
                  MultiBlocProvider(
                    providers: [
                      BlocProvider<RegisterBloc>(
                          create: (context) => RegisterBloc()),
                      BlocProvider<LoginBloc>(create: (context) => LoginBloc())
                    ],
                    child: user == null
                        ? const SignupForm()
                        : SignupForm(user: user),
                  ),
                  SizedBox(height: 0.03.sh),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(top: 0.11.sh, bottom: 0.03.sh),
                      child: trademarkText(),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
