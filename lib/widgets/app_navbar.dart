import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/dekornata_icon.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/screens/category_screen.dart';
import 'package:flutter_unity_widget_example/screens/home_screen.dart';
import 'package:flutter_unity_widget_example/screens/login_screen.dart';
import 'package:flutter_unity_widget_example/screens/order_screen.dart';
import 'package:flutter_unity_widget_example/screens/profile_screen.dart';
import 'package:flutter_unity_widget_example/screens/profilenotscreen_screen.dart';
import 'package:flutter_unity_widget_example/screens/scan_screen.dart';
import 'package:flutter_unity_widget_example/screens/test_screen.dart';
import 'package:get_storage/get_storage.dart';
import 'package:nil/nil.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class NavBar extends StatefulWidget {
  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  late PersistentTabController _persistentTabController;

  @override
  void initState() {
    _persistentTabController = PersistentTabController();

    print(isHide);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PersistentTabView(
        context,
        onItemSelected: (index) {
          if (index == 2 && GetStorage().read('token') == null) {
            setState(() {
              isHide = true;
            });
          }
          if (index == 1) {
            setState(() {
              isHide = true;
            });
            unityWidgetController.resume();
          }
        },
        hideNavigationBar: isHide,
        hideNavigationBarWhenKeyboardShows: true,
        controller: _persistentTabController,
        screens: [
          HomeScreen(),
          ScanScreen(),
          GetStorage().read('token') != null ? ProfileScreen() : LoginScreen()
        ],
        items: [
          PersistentBottomNavBarItem(
            icon: const Icon(DekornataIcon.homeIcon),
            title: "Home",
            activeColorPrimary: masterColor('primary'),
            inactiveColorPrimary: masterColor('grey'),
            iconSize: 30,
            textStyle:
                const TextStyle(fontFamily: 'GT Walsheim Pro', fontSize: 12),
            routeAndNavigatorSettings:
                RouteAndNavigatorSettings(initialRoute: '/', routes: {
              '/test': (context) => TestScreen(),
            }),
          ),
          PersistentBottomNavBarItem(
            icon: Icon(
              Icons.fullscreen,
              color: masterColor('secondary'),
            ),
            title: "Scan",
            activeColorPrimary: masterColor('primary'),
            inactiveColorPrimary: masterColor('grey'),
            iconSize: 30,
            textStyle:
                const TextStyle(fontFamily: 'GT Walsheim Pro', fontSize: 12),
            routeAndNavigatorSettings: RouteAndNavigatorSettings(
              initialRoute: '/scan',
              routes: {
                '/': (context) => HomeScreen(),
                '/category': (context) => CategoryScreen(),
                '/scan': (context) => ScanScreen(),
                '/order': (context) => OrderScreen(),
                '/profile': (context) => ProfileScreenNotLogin(),
              },
            ),
          ),
          PersistentBottomNavBarItem(
            icon: const Icon(DekornataIcon.profileIcon),
            title: "Profile",
            activeColorPrimary: Colors.indigo,
            inactiveColorPrimary: Colors.grey,
            routeAndNavigatorSettings: RouteAndNavigatorSettings(
              initialRoute: '/profile',
              routes: {
                '/': (context) => HomeScreen(),
                '/category': (context) => CategoryScreen(),
                '/scan': (context) => ScanScreen(),
                '/order': (context) => OrderScreen(),
                '/profile': (context) => ProfileScreenNotLogin(),
                '/login': (context) => LoginScreen()
              },
            ),
          ),
        ],
        navBarStyle: NavBarStyle.style15,
        floatingActionButton: const Nil(),
      ),
    );
  }
}
