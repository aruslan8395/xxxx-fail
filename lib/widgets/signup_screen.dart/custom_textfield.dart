import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/firebase_model.dart';

class CustomTextField extends StatelessWidget {
  final FirebaseUser? user;
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;
  final String hintText;
  final TextInputType textInputType;
  const CustomTextField({
    this.user,
    this.onChanged,
    required this.textInputType,
    required this.validator,
    required this.hintText,
  });
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: user == null ? '' : user!.email,
      enabled: user == null,
      onChanged: (value) {},
      textInputAction: TextInputAction.next,
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
      decoration: InputDecoration(
        filled: true,
        fillColor:
            user != null ? masterColor('grey') : masterColor('secondary'),
        hintText: hintText,
        hintStyle: TextStyle(
          color: masterColor('primary'),
          fontFamily: 'GT Walsheim Pro',
          fontSize: 13,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      keyboardType: textInputType,
      validator: validator,
    );
  }
}
