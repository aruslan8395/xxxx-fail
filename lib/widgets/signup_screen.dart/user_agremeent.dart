import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_unity_widget_example/helpers/constant.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/services/auth_service.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:get/state_manager.dart';

class UserAgremeent extends StatelessWidget {
  final RxBool isAgree;
  const UserAgremeent(this.isAgree);
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Row(
        children: [
          SizedBox(
            height: 24,
            width: 24,
            child: Checkbox(
              activeColor: masterColor('primary'),
              onChanged: (value) => isAgree.value = value!,
              value: isAgree.value,
            ),
          ),
          SizedBox(
            width: 0.03.sw,
          ),
          Flexible(
            child: RichText(
              maxLines: 2,
              text: TextSpan(
                children: <TextSpan>[
                  const TextSpan(
                    text: 'Saya menyetujui ',
                    style: TextStyle(
                      fontFamily: 'GT Walsheim Pro',
                      fontSize: 12,
                    ),
                  ),
                  TextSpan(
                      style: const TextStyle(
                        fontFamily: 'GT Walsheim Pro',
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      ),
                      text: 'persyaratan',
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          authService.launchUrl(toslink);
                        }),
                  const TextSpan(
                    text: ' penggunaan dan ',
                    style: TextStyle(
                      fontFamily: 'GT Walsheim Pro',
                      fontSize: 12,
                    ),
                  ),
                  TextSpan(
                      style: const TextStyle(
                        fontFamily: 'GT Walsheim Pro',
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      ),
                      text: 'privasi',
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          authService.launchUrl(privacyLink);
                        }),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
