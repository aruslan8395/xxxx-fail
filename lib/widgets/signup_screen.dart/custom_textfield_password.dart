import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:nil/nil.dart';

class CustomTextFieldPassword extends StatelessWidget {
  final RxBool? hiddenPassword;

  final String? Function(String?)? validator;
  final void Function(String)? onChanged;

  final String hintText;
  const CustomTextFieldPassword({
    required this.validator,
    required this.onChanged,
    required this.hintText,
    this.hiddenPassword,
  });
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => TextFormField(
        // ignore: avoid_bool_literals_in_conditional_expressions
        obscureText: hiddenPassword == null ? false : hiddenPassword!.value,
        validator: validator,
        onChanged: onChanged,
        decoration: InputDecoration(
            filled: true,
            fillColor: masterColor('secondary'),
            hintText: hintText,
            hintStyle: TextStyle(
              color: masterColor('primary'),
              fontFamily: 'GT Walsheim Pro',
              fontSize: 13,
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: masterColor('primary'),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            suffixIcon: hiddenPassword != null
                ? IconButton(
                    splashRadius: 1,
                    focusNode: FocusNode(canRequestFocus: false),
                    onPressed: () {
                      hiddenPassword!.value = !hiddenPassword!.value;
                    },
                    icon: hiddenPassword!.value
                        ? Icon(
                            Icons.visibility_off,
                            color: masterColor("grey"),
                          )
                        : Icon(
                            Icons.visibility,
                            color: masterColor("primary"),
                          ),
                  )
                : const Nil()),
      ),
    );
  }
}
