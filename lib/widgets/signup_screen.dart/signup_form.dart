import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/form_submission_status.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_event.dart';
import 'package:flutter_unity_widget_example/blocs/register_bloc/register_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/register_bloc/register_event.dart';
import 'package:flutter_unity_widget_example/blocs/register_bloc/register_state.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/firebase_model.dart';
import 'package:get/state_manager.dart';

import '../app_navbar.dart';
import 'custom_textfield.dart';
import 'custom_textfield_password.dart';
import 'submit_button.dart';
import 'user_agremeent.dart';

// ignore: must_be_immutable
class SignupForm extends StatefulWidget {
  final FirebaseUser? user;
  const SignupForm({this.user});

  @override
  _SignupFormState createState() => _SignupFormState();
}

class _SignupFormState extends State<SignupForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final RxBool _hiddenPassword = true.obs;

  final RxBool _isAgree = false.obs;

  @override
  void initState() {
    if (widget.user != null) {
      context
          .read<RegisterBloc>()
          .add(RegisterFirstNameChanged(firstName: widget.user!.firstName));
      context
          .read<RegisterBloc>()
          .add(RegisterLastNameChanged(lastName: widget.user!.lastName));
      context.read<RegisterBloc>().add(
          RegisterDisplayNameChanged(displayName: widget.user!.displayName));
      context
          .read<RegisterBloc>()
          .add(RegisterEmailChanged(email: widget.user!.email));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: BlocListener<RegisterBloc, RegisterState>(
        listener: (context, state) {
          if (state.formStatus is SubmissionSuccess) {
            context.read<LoginBloc>().add(LoginSubmitted());
            if (state.formStatus is SubmissionSuccess) {
              isHide = false;
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => NavBar()));
            }
          } else if (state.formStatus is SubmissionFailed) {
            const SnackBar snackBar = SnackBar(content: Text('Gagal Masuk'));
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
        child:
            BlocBuilder<RegisterBloc, RegisterState>(builder: (context, state) {
          return Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 35.38),
                  TextFormField(
                    initialValue:
                        widget.user == null ? '' : widget.user!.firstName,
                    onChanged: (value) {
                      context
                          .read<RegisterBloc>()
                          .add(RegisterFirstNameChanged(firstName: value));
                    },
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () => FocusScope.of(context).nextFocus(),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: masterColor('secondary'),
                      hintText: 'First Name',
                      labelStyle: TextStyle(
                        color: masterColor('primary'),
                        fontFamily: 'GT Walsheim Pro',
                        fontSize: 13,
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) => state.isValidFirstName
                        ? null
                        : 'Nama depan harus diisi',
                  ),
                  const SizedBox(height: 11),
                  TextFormField(
                    initialValue:
                        widget.user == null ? '' : widget.user!.lastName,
                    onChanged: (value) {
                      context
                          .read<RegisterBloc>()
                          .add(RegisterLastNameChanged(lastName: value));
                    },
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () => FocusScope.of(context).nextFocus(),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: masterColor('secondary'),
                      hintText: 'Last Name',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) => state.isValidLastName
                        ? null
                        : 'Nama belakang harus diisi',
                  ),
                  const SizedBox(height: 11),
                  TextFormField(
                    initialValue:
                        widget.user == null ? '' : widget.user!.displayName,
                    onChanged: (value) {
                      context
                          .read<RegisterBloc>()
                          .add(RegisterDisplayNameChanged(displayName: value));
                    },
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () => FocusScope.of(context).nextFocus(),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: masterColor('secondary'),
                      hintText: 'Username',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) => state.isValidDisplayName
                        ? null
                        : 'Display name harus diisi',
                  ),
                  const SizedBox(height: 11),
                  TextFormField(
                    // ignore: avoid_bool_literals_in_conditional_expressions

                    onChanged: (value) {
                      context
                          .read<RegisterBloc>()
                          .add(RegisterPhoneNumberChanged(phoneNumber: value));
                    },
                    textInputAction: TextInputAction.next,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    onEditingComplete: () => FocusScope.of(context).nextFocus(),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: masterColor('secondary'),
                      hintText: 'Phone Number',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    keyboardType: const TextInputType.numberWithOptions(
                        signed: true, decimal: true),
                    validator: (value) => state.isValidPhoneNumber
                        ? null
                        : 'Nomor telpon tidak valid',
                  ),
                  const SizedBox(height: 11),
                  CustomTextField(
                    textInputType: TextInputType.emailAddress,
                    user: widget.user,
                    validator: (value) => state.isValidEmail ? null : null,
                    onChanged: (value) {
                      context
                          .read<RegisterBloc>()
                          .add(RegisterEmailChanged(email: value));
                    },
                    hintText: 'Email',
                  ),
                  const SizedBox(height: 11),
                  CustomTextFieldPassword(
                    validator: (value) => state.isValidPassword
                        ? null
                        : 'Kata Sandi terlalu pendek',
                    onChanged: (value) => context.read<RegisterBloc>().add(
                          RegisterPasswordChanged(password: value),
                        ),
                    hintText: 'Password',
                    hiddenPassword: _hiddenPassword,
                  ),
                  const SizedBox(height: 11),
                  UserAgremeent(_isAgree),
                  const SizedBox(height: 11),
                  if (state.formStatus is FormSubmitting)
                    loadingWidget()
                  else
                    BlocBuilder<RegisterBloc, RegisterState>(
                        builder: (context, state) =>
                            SubmitButton(isAgree: _isAgree, formKey: _formKey))
                ],
              ),
            ],
          );
        }),
      ),
    );
  }
}
