import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_unity_widget_example/blocs/register_bloc/register_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/register_bloc/register_event.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

class SubmitButton extends StatelessWidget {
  final RxBool isAgree;
  final GlobalKey<FormState> formKey;
  const SubmitButton({required this.isAgree, required this.formKey});
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
        minimumSize: Size(1.sw, 44),
        padding: EdgeInsets.symmetric(vertical: 0.02.sh),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      ),
      onPressed: () {
        if (isAgree.value == true) {
          if (formKey.currentState!.validate()) {
            context.read<RegisterBloc>().add(RegisterSubmitted());
          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text(
                  'Anda belum menyetujui persyaratan penggunaan dan privasi'),
            ),
          );
        }
      },
      child: textKeyWidget(
        text: 'sign-up',
        color: 'primary',
        weight: 'B',
        size: 16,
      ),
    );
  }
}
