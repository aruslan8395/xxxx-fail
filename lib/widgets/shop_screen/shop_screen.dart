import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_unity_widget_example/blocs/shop_bloc/shop_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/shop_bloc/shop_event.dart';
import 'package:flutter_unity_widget_example/blocs/shop_bloc/shop_state.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/product_pagination_model.dart';
import 'package:flutter_unity_widget_example/screens/error_screen.dart';
import 'package:flutter_unity_widget_example/widgets/shared/product_card.dart';
import 'package:nil/nil.dart';

class ShopMaster extends StatefulWidget {
  @override
  _ShopMasterState createState() => _ShopMasterState();
}

class _ShopMasterState extends State<ShopMaster> {
  final _scrollController = ScrollController();
  final _scrollControllerInner = ScrollController();
  final _scrollThreshold = 50;
  late ShopState localState;
  ProductPagination _products = ProductPagination(
    total: 0,
    perPage: 0,
    page: 0,
    lastPage: 0,
    data: [],
  );
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      BlocProvider.of<ShopBloc>(context).add(
        RequestProduct(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: masterColor('primary'),
        title: textWidget(
          text: 'PRODUK REKOMENDASI',
          fontFamily: 'Dosis',
          weight: 'B',
          color: 'secondary',
          letterSpacing: 1.2,
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          _products = ProductPagination(
            total: 0,
            perPage: 0,
            page: 0,
            lastPage: 0,
            data: [],
          );
          BlocProvider.of<ShopBloc>(context).add(RequestProduct());
        },
        child: BlocListener<ShopBloc, ShopState>(
          listener: (context, state) {
            if (state.status == ShopStatus.success) {
              _products = state.pagination;
            }
          },
          child: BlocBuilder<ShopBloc, ShopState>(
            builder: (context, state) {
              localState = state;
              switch (state.status) {
                case ShopStatus.initial:
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: masterColor('primary'),
                    ),
                  );
                case ShopStatus.failure:
                  return ErrorScreen();
                case ShopStatus.success:
                  return Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: ListView(
                      controller: _scrollController,
                      children: [
                        StaggeredGridView.countBuilder(
                            crossAxisCount: 4,
                            itemCount: _products.data.length,
                            shrinkWrap: true,
                            primary: false,
                            mainAxisSpacing: 8,
                            crossAxisSpacing: 8,
                            controller: _scrollControllerInner,
                            itemBuilder: (context, index) {
                              return ProductCard(
                                  product: _products.data[index]);
                            },
                            staggeredTileBuilder: (int index) {
                              return const StaggeredTile.fit(2);
                            }),
                        if (state.hasReachedMax)
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Center(
                              child: textWidget(
                                text: "end-of-page",
                                weight: "L",
                                fontFamily: "Montserrat",
                                size: 13,
                                letterSpacing: 1.2,
                              ),
                            ),
                          )
                        else
                          state.isFetching
                              ? const Padding(
                                  padding: EdgeInsets.only(top: 20),
                                  child: Center(
                                      child: CircularProgressIndicator()),
                                )
                              : const Nil(),
                      ],
                    ),
                  );
              }
            },
          ),
        ),
      ),
    );
  }
}
