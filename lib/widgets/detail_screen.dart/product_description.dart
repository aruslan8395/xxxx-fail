import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';

class ProductDescription extends StatelessWidget {
  final String productDescription;
  const ProductDescription(this.productDescription);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: textWidget(
        text: productDescription,
        fontFamily: 'GT Walsheim Pro',
        maxLines: 6,
        textOverflow: TextOverflow.ellipsis,
      ),
    );
  }
}
