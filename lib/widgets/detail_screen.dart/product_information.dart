import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/product_model.dart';

class ProductInformation extends StatelessWidget {
  final int index;
  final Product product;
  const ProductInformation(this.product, this.index);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textWidget(
                text: 'Dimension',
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 0.5,
              ),
              textWidget(
                text:
                    '${product.variants[index].length}*${product.variants[index].width}*${product.variants[index].height} cm',
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 0.5,
              )
            ],
          ),
          SizedBox(height: 0.005.sh),
          const Divider(
            thickness: 1,
            color: Colors.amber,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textWidget(
                text: 'Weight',
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 0.5,
              ),
              textWidget(
                text: '${product.variants[index].weight / 1000} kilogram',
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 0.5,
              )
            ],
          ),
          SizedBox(height: 0.005.sh),
          const Divider(
            thickness: 1,
            color: Colors.amber,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textWidget(
                text: 'Material',
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 0.5,
              ),
              textWidget(
                text: product.variants[index].material,
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 0.5,
              )
            ],
          ),
          SizedBox(height: 0.005.sh),
          const Divider(
            thickness: 1,
            color: Colors.amber,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textWidget(
                text: 'Finishing',
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 0.5,
              ),
              textWidget(
                text: product.variants[index].finishing,
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 0.5,
              )
            ],
          ),
          SizedBox(height: 0.005.sh),
          const Divider(
            thickness: 1,
            color: Colors.amber,
          ),
        ],
      ),
    );
  }
}
