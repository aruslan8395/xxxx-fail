import 'package:carousel_slider/carousel_slider.dart';

import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/product_model.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class ProductDetailPhotoCarousel extends SliverPersistentHeaderDelegate {
  final RxInt productIndex;
  final RxInt currentIndex;
  final double expandedHeight;
  final Product product;
  ProductDetailPhotoCarousel(
      this.product, this.expandedHeight, this.currentIndex, this.productIndex);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Obx(
      () => CarouselSlider(
        options: CarouselOptions(
            height: expandedHeight,
            // aspectRatio: 1.873,
            viewportFraction: 1,
            autoPlay: product.variants[productIndex.value].photos.length == 1
                ? product.variants[productIndex.value].photos.length != 1
                : product.variants[productIndex.value].photos.length == 1,
            onPageChanged: (index, reason) {
              currentIndex.value = index;
            }),
        items: product.variants[productIndex.value].photos.map((photo) {
          return Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: masterColor('primary'),
            ),
            child: Image.network(
              photo.originalLink,
              fit: BoxFit.cover,
            ),
          );
        }).toList(),
      ),
    );
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => 0;
}
