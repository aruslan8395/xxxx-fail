import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/product_model.dart';
import 'package:flutter_unity_widget_example/screens/detail_screen.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class ProductCard extends StatelessWidget {
  final Product product;
  const ProductCard({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        pushNewScreen(context,
            screen: DetailScreen(product), withNavBar: isHide);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 1.sw,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                child: Image(
                  image:
                      NetworkImage(product.variants[0].photos[0].thumbnailLink),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  textWidget(
                    text: product.name,
                    maxLines: 2,
                    fontFamily: 'GT Walsheim Pro',
                    size: 13,
                    letterSpacing: 1.2,
                  ),
                  textWidget(
                    text:
                        'IDR ${convertNumberToCurrency(product.variants[0].price)}',
                    decoration: product.variants[0].discount == null
                        ? TextDecoration.none
                        : TextDecoration.lineThrough,
                    fontFamily: 'GT Walsheim Pro',
                    weight: 'B',
                    letterSpacing: 1.2,
                    color: 'primary',
                  ),
                  if (product.variants[0].discount == null)
                    const SizedBox()
                  else
                    textWidget(
                      text:
                          'IDR ${convertNumberToCurrency(product.variants[0].discount!)}',
                      fontFamily: 'GT Walsheim Pro',
                      weight: 'B',
                      letterSpacing: 1.2,
                      color: 'danger',
                    ),
                  textWidget(
                    text:
                        'Stock ${convertNumberToCurrency(product.variants[0].stock!)}',
                    fontFamily: 'GT Walsheim Pro',
                    letterSpacing: 1,
                    weight: 'L',
                    color: 'primary',
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
