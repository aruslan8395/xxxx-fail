import 'package:flutter/material.dart';

class DekornataAuthLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 40),
      height: 45.08,
      width: 45.56,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/dekornata-logo.png'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
