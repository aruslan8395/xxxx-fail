import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/services/auth_service.dart';
import 'package:get/state_manager.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// ignore: must_be_immutable
class ForgotPasswordForm extends StatelessWidget {
  RxString email = ''.obs;
  // ignore: prefer_final_fields

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 35.38),
        TextFormField(
          onChanged: (value) {
            email.value = value;
          },
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            filled: true,
            fillColor: masterColor('secondary'),
            labelText: 'Email',
            labelStyle: TextStyle(
              color: masterColor('primary'),
              fontFamily: 'GT Walsheim Pro',
              fontSize: 13,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          keyboardType: TextInputType.emailAddress,
          validator: (val) {
            if (val == null) {
              return 'Email cannot be empty';
            } else {
              final bool emailValid = RegExp(
                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                  .hasMatch(val);
              if (!emailValid) {
                return 'Please enter a valid email';
              }
              return null;
            }
          },
        ),
        const SizedBox(height: 11),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.white,
            minimumSize: Size(1.sw, 44),
            padding: EdgeInsets.symmetric(vertical: 0.02.sh),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          ),
          onPressed: () async {
            final String message =
                await authService.forgotPassword(email.value);

            FocusScope.of(context).requestFocus(FocusNode());
            showSnackBar(context, message);
            Future.delayed(const Duration(seconds: 2),
                () => Navigator.pushNamed(context, '/'));
          },
          child: textKeyWidget(
            text: 'send',
            color: 'primary',
            weight: 'B',
            size: 16,
          ),
        ),
      ],
    );
  }
}
