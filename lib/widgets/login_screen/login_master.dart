import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_event.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_state.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/screens/signup_screen.dart';
import 'package:flutter_unity_widget_example/widgets/app_navbar.dart';
import 'package:flutter_unity_widget_example/widgets/shared/dekornata_auth_logo.dart';
import 'package:get/state_manager.dart';

import 'custom_underline_button.dart';
import 'firebase_login_button.dart';
import 'login_form.dart';

class LoginMaster extends StatelessWidget {
  final RxBool isSubmitted = false.obs;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Stack(
            children: [
              Positioned.fill(
                child: Container(
                  decoration: BoxDecoration(
                    color: masterColor('primary'),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(45, 0.1.sh, 50, 10),
                height: 1.sh,
                width: 1.sw,
                child: Column(
                  children: [
                    DekornataAuthLogo(),
                    textKeyWidget(
                      text: 'dekornata',
                      size: 24,
                      color: 'secondary',
                      letterSpacing: 1.2,
                    ),
                    BlocProvider<LoginBloc>(
                        create: (context) => LoginBloc(), child: LoginForm()),
                    CustomUnderlineButton(
                      text: 'forgot-password',
                      onPressed: () => Navigator.pushNamed(context, '/forgot'),
                      color: 'secondary',
                    ),
                    SizedBox(height: 0.03.sh),
                    textKeyWidget(
                      text: 'or-signin',
                      fontFamily: 'GT Walsheim Pro',
                      weight: 'M',
                      color: 'secondary',
                    ),
                    SizedBox(height: 0.02.sh),
                    FirebaseLoginButton(),
                    SizedBox(height: 0.005.sh),
                    CustomUnderlineButton(
                      text: 'didnt-have',
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SignUpScreen())),
                      color: 'secondary',
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: EdgeInsets.only(top: 0.16.sh, bottom: 0.03.sh),
                        child: trademarkText(),
                      ),
                    ),
                  ],
                ),
              ),
              if (state.formStatus is LoginSubmitted)
                SizedBox(
                  height: 1.sh,
                  width: 1.sw,
                )
              else
                Container(),
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                    padding: EdgeInsets.only(top: 0.07.sh, left: 0.03.sw),
                    icon: Icon(Icons.close),
                    color: masterColor('secondary'),
                    onPressed: () {
                      isHide = false;
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => NavBar()));
                    }),
              ),
            ],
          );
        },
      ),
    );
  }
}
