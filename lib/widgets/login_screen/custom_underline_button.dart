import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';

class CustomUnderlineButton extends StatelessWidget {
  final ButtonStyle? style;
  final String text;
  final void Function()? onPressed;
  final String color;
  const CustomUnderlineButton(
      {required this.text,
      required this.onPressed,
      required this.color,
      this.style});
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: style,
      onPressed: onPressed,
      child: textKeyWidget(
        text: text,
        decoration: TextDecoration.underline,
        fontFamily: 'GT Walsheim Pro',
        weight: 'SB',
        size: 12,
        color: color,
      ),
    );
  }
}
