import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/form_submission_status.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_event.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_state.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:get/state_manager.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// ignore: must_be_immutable
class LoginForm extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  // ignore: prefer_final_fields
  RxBool _hiddenPassword = true.obs;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          const SizedBox(height: 35.38),
          BlocListener<LoginBloc, LoginState>(
            listener: (context, state) {
              if (state.formStatus is SubmissionSuccess) {
                Navigator.pushReplacementNamed(context, '/navbar');
              }
            },
            child:
                BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
              return TextFormField(
                onChanged: (value) {
                  context
                      .read<LoginBloc>()
                      .add(LoginEmailChanged(email: value));
                },
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: masterColor('secondary'),
                  hintText: 'Email',
                  hintStyle: TextStyle(
                    color: masterColor('primary'),
                    fontFamily: 'GT Walsheim Pro',
                    fontSize: 13,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                keyboardType: TextInputType.emailAddress,
                validator: (value) =>
                    state.isValidEmail ? null : 'Masukan email yang valid',
              );
            }),
          ),
          const SizedBox(height: 11),
          BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
            return Obx(
              () => TextFormField(
                validator: (value) =>
                    state.isValidPassword ? null : 'Kata sandi terlalu pendek',
                obscureText: _hiddenPassword.value,
                onChanged: (value) => context.read<LoginBloc>().add(
                      LoginPasswordChanged(password: value),
                    ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: masterColor('secondary'),
                  hintText: 'Password',
                  hintStyle: TextStyle(
                    color: masterColor('primary'),
                    fontFamily: 'GT Walsheim Pro',
                    fontSize: 13,
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: masterColor('primary'),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  suffixIcon: IconButton(
                    splashRadius: 1,
                    focusNode: FocusNode(canRequestFocus: false),
                    onPressed: () {
                      _hiddenPassword.toggle();
                    },
                    icon: _hiddenPassword.value
                        ? Icon(
                            Icons.visibility_off,
                            color: masterColor("grey"),
                          )
                        : Icon(
                            Icons.visibility,
                            color: masterColor("primary"),
                          ),
                  ),
                ),
              ),
            );
          }),
          const SizedBox(height: 11),
          BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
            return state.formStatus is FormSubmitting
                ? CircularProgressIndicator(
                    backgroundColor: masterColor('primary'),
                  )
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        minimumSize: Size(1.sw, 44),
                        padding: EdgeInsets.symmetric(vertical: 0.02.sh),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10))),
                    onPressed: () {
                      if (_formKey.currentState == null) {
                      } else if (_formKey.currentState!.validate()) {
                        context.read<LoginBloc>().add(LoginSubmitted());
                        if (state.formStatus is SubmissionSuccess) {
                          Navigator.pushReplacementNamed(context, '/navbar');
                        } else if (state.formStatus is SubmissionFailed) {
                          showSnackBar(context, 'Gagal Masuk');
                        }
                      }
                    },
                    child: textKeyWidget(
                      text: 'sign-in',
                      color: 'primary',
                      weight: 'B',
                      size: 16,
                    ),
                  );
          }),
          SizedBox(height: 0.001.sh),
        ],
      ),
    );
  }
}
