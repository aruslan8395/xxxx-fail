import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/form_submission_status.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_event.dart';
import 'package:flutter_unity_widget_example/blocs/login_bloc.dart/login_state.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/firebase_model.dart';
import 'package:flutter_unity_widget_example/screens/signup_screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../app_navbar.dart';

class FirebaseLoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.formStatus is SubmissionSuccess) {
          isHide = false;
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => NavBar()));
        }
        if (state.formStatus is SubmissionFailed) {
          if (state.user != null) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SignUpScreen(
                  user: FirebaseUser(
                    displayName: state.user!.displayName,
                    uid: state.user!.uid,
                    email: state.user!.email,
                    photoUrl: state.user!.photoUrl,
                    lastName: state.user!.lastName,
                    firstName: state.user!.firstName,
                  ),
                ),
              ),
            );
          } else {
            showSnackBar(context, 'Gagal Masuk');
          }
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                onPressed: () async {
                  context.read<LoginBloc>().add(LoginWithGoogleSubmitted());
                },
                child: const Icon(FontAwesomeIcons.google),
              ),
              if (Platform.isAndroid)
                const SizedBox()
              else
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: masterColor('apple'),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {
                    context.read<LoginBloc>().add(LoginWithAppleSubmitted());
                  },
                  child: const Icon(FontAwesomeIcons.apple),
                ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: masterColor('facebook'),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                onPressed: () {
                  context.read<LoginBloc>().add(LoginWithFacebookSubmitted());
                },
                child: const Icon(FontAwesomeIcons.facebookF),
              ),
            ],
          );
        },
      ),
    );
  }
}
