import 'package:flutter_unity_widget_example/blocs/home_bloc/home_bloc.dart';
import 'package:flutter_unity_widget_example/blocs/home_bloc/home_event.dart';
import 'package:flutter_unity_widget_example/blocs/home_bloc/home_state.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/models/banner_model.dart'
    as banner;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_unity_widget_example/models/product_model.dart';
import 'package:flutter_unity_widget_example/screens/error_screen.dart';
import 'package:flutter_unity_widget_example/screens/shop_screen.dart';
import 'package:flutter_unity_widget_example/services/home_service.dart';
import 'package:flutter_unity_widget_example/widgets/shared/product_card.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'header_search.dart';
import 'image_slider.dart';
import 'product_button.dart';

class HomeMaster extends StatefulWidget {
  @override
  _HomeMasterState createState() => _HomeMasterState();
}

class _HomeMasterState extends State<HomeMaster> {
  late TrackingScrollController _scrollController;

  @override
  void initState() {
    _scrollController = TrackingScrollController();
    homeService.getAllFeaturedProduct();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  List<banner.Banner> _banners = [];
  List<Product> _products = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RefreshIndicator(
      onRefresh: () async {
        _banners = [];
        _products = [];
        BlocProvider.of<HomeBloc>(context).add(HomeFetched());
      },
      child: SizedBox(
        height: 1.sh,
        width: 1.sh,
        child: Stack(
          children: [
            SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: 0.068.sh),
                    BlocListener<HomeBloc, HomeState>(
                      listener: (context, state) {
                        if (state is HomeSuccess) {
                          _banners.addAll(state.banners);
                          _products.addAll(state.products);
                        }
                      },
                      child: BlocBuilder<HomeBloc, HomeState>(
                          builder: (context, state) {
                        if (state is HomeInitial) {
                          return SizedBox(
                            width: 1.sw,
                            height: 0.5.sh,
                            child: Center(
                              child: CircularProgressIndicator(
                                backgroundColor: masterColor('primary'),
                              ),
                            ),
                          );
                        } else if (state is HomeSuccess) {
                          return Column(
                            children: [
                              ImageSlider(_banners),
                              // ExploreCategoriesSection(),
                              Container(
                                padding:
                                    EdgeInsets.symmetric(horizontal: 0.04.sw),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    textWidget(
                                      text: 'PRODUK REKOMENDASI',
                                      fontFamily: 'Dosis',
                                      weight: 'B',
                                      letterSpacing: 1.2,
                                      color: 'primary',
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        pushNewScreen(context,
                                            screen: ShopScreen(),
                                            withNavBar: false);
                                      },
                                      child: textWidget(
                                        text: 'Lihat Semua',
                                        fontFamily: 'GT Walsheim Pro',
                                        size: 10,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.symmetric(horizontal: 0.04.sw),
                                child: StaggeredGridView.countBuilder(
                                  crossAxisCount: 4,
                                  crossAxisSpacing: 8,
                                  mainAxisSpacing: 8,
                                  itemCount: _products.length,
                                  shrinkWrap: true,
                                  primary: false,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) =>
                                      ProductCard(product: _products[index]),
                                  staggeredTileBuilder: (index) =>
                                      const StaggeredTile.fit(2),
                                  // mainAxisSpacing: 1.0,
                                  // crossAxisSpacing: 1.0,
                                ),
                              ),
                              SizedBox(height: 0.01.sh),
                              const ProductButton(),
                              SizedBox(height: 0.1.sh)
                            ],
                          );
                        } else if (state is HomeFailure) {
                          return ErrorScreen();
                        }
                        return ErrorScreen();
                      }),
                    )
                  ],
                ),
              ),
            ),
            GestureDetector(
                onTap: () => pushNewScreen(
                      context,
                      screen: ErrorScreen(),
                    ),
                child: HeaderSearch(_scrollController)),
          ],
        ),
      ),
    ));
  }
}
