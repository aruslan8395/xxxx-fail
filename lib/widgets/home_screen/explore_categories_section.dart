import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/screens/comingsoon_screen.dart';
import 'package:flutter_unity_widget_example/screens/shop_screen.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class ExploreCategoriesSection extends StatelessWidget {
  final List<String> categoryTitle = [
    'Belanja',
    'Grafir',
    'Quotation',
    'Mitra',
    'Poin',
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0.02.sh, horizontal: 0.04.sw),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(right: 0.015.sw),
                  child: Divider(
                    color: masterColor('grey'),
                    thickness: 1,
                  ),
                ),
              ),
              textWidget(
                text: 'Explore Categories',
                fontFamily: 'GT Walsheim Pro',
                color: 'primary',
                letterSpacing: 1.2,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 0.015.sw),
                  child: Divider(
                    color: masterColor('grey'),
                    thickness: 1,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 0.01.sh),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              for (int i = 0; i < 5; i++)
                Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        if (i == 0) {
                          pushNewScreen(context,
                              screen: ShopScreen(), withNavBar: false);
                        } else {
                          pushNewScreen(context,
                              screen: const ComingSoonScreen(
                                flag: true,
                              ),
                              withNavBar: false);
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.all(5),
                        height: 48,
                        width: 48,
                        decoration: BoxDecoration(
                          border: Border.all(color: masterColor('grey')),
                          shape: BoxShape.circle,
                        ),
                        child: SvgPicture.asset(
                          'assets/icons/svg/explore-categories-$i.svg',
                          height: 20,
                          width: 20,
                          color: masterColor('primary'),
                        ),
                      ),
                    ),
                    textWidget(
                      text: categoryTitle[i],
                      fontFamily: 'GT Walsheim Pro',
                      size: 10,
                      color: 'primary',
                    )
                  ],
                )
            ],
          )
        ],
      ),
    );
  }
}
