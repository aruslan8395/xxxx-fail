import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';
import 'package:flutter_unity_widget_example/screens/shop_screen.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class ProductButton extends StatelessWidget {
  const ProductButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(width: 0.9.sw, height: 0.05.sh),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: masterColor('primary'),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        onPressed: () {
          pushNewScreen(context, screen: ShopScreen(), withNavBar: false);
        },
        child: textWidget(
          text: 'Lihat Semua',
          fontFamily: 'GT Walsheim Pro',
          color: 'secondary',
        ),
      ),
    );
  }
}
