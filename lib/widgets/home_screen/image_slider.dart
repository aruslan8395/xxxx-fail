import 'package:carousel_slider/carousel_slider.dart';

import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/models/banner_model.dart'
    as banner;
import 'package:flutter_unity_widget_example/helpers/widget.dart';

class ImageSlider extends StatefulWidget {
  final List<banner.Banner> banners;
  const ImageSlider(this.banners);
  @override
  _ImageSliderState createState() => _ImageSliderState();
}

class _ImageSliderState extends State<ImageSlider> {
  late int _currentIndex;

  @override
  void initState() {
    _currentIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CarouselSlider(
          options: CarouselOptions(
              aspectRatio: 1.873,
              viewportFraction: 1,
              autoPlay: true,
              onPageChanged: (index, reason) {
                setState(() {
                  _currentIndex = index;
                });
              }),
          items: widget.banners.map((banner) {
            return Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: masterColor('primary'),
              ),
              child: Image.network(
                'https://res.cloudinary.com/dk6bmnbp7/image/upload/f_auto,q_50/v1/${banner.mobileImageLink}',
                fit: BoxFit.cover,
              ),
            );
          }).toList(),
        ),
        Positioned(
          left: MediaQuery.of(context).size.width * 0.44,
          bottom: MediaQuery.of(context).size.height * 0.008,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: widget.banners.map((banner) {
              final int index = widget.banners.indexOf(banner);
              return Container(
                width: 8,
                height: 8,
                margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 2),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _currentIndex == index
                      ? masterColor('grey')
                      : masterColor('secondary'),
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }
}
