import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_unity_widget_example/helpers/widget.dart';

class HeaderSearch extends StatefulWidget {
  final TrackingScrollController scrollController;
  const HeaderSearch(this.scrollController);
  @override
  _HeaderSearchState createState() => _HeaderSearchState();
}

class _HeaderSearchState extends State<HeaderSearch> {
  late Color _backgroundColor;
  late Color _backgroundColorSearch;
  // ignore_for_file: unused_field
  late Color _colorIcon;
  late double _opacity;
  late double _offset;
  final _opacityMax = 0.01;

  @override
  void initState() {
    _backgroundColor = masterColor('primary');
    _backgroundColorSearch = Colors.white;
    _colorIcon = Colors.white;
    _opacity = 0;
    _offset = 0;
    widget.scrollController.addListener(() {
      final scrollOffset = widget.scrollController.offset;
      if (scrollOffset >= _offset && scrollOffset > 5) {
        _opacity = double.parse((_opacity + _opacityMax).toStringAsFixed(2));
        if (_opacity >= 1) _opacity = 1;
      } else if (scrollOffset < 100) {
        _opacity = double.parse((_opacity - _opacityMax).toStringAsFixed(2));
        if (_opacity <= 1) _opacity = 0;
      }
      setState(() {
        if (scrollOffset <= 0) {
          _backgroundColorSearch = Colors.white;
          _colorIcon = Colors.white;
          _opacity = 0;
          _offset = 0;
        } else {
          _backgroundColorSearch = Colors.grey[200]!;
          _colorIcon = Colors.deepOrange;
        }
        _backgroundColor = Colors.white.withOpacity(_opacity);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      color: _backgroundColor,
      padding: const EdgeInsets.all(15),
      child: Row(
        children: [
          SvgPicture.asset(
            'assets/icons/svg/dekornata-logo.svg',
            width: 10,
            height: 30,
            color: masterColor('secondary'),
          )
          // SearchBar(_backgroundColorSearch),
          // const SizedBox(width: 5),
          // Expanded(
          //   flex: 3,
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //     children: [
          //       HeaderIcon(
          //         iconColor: _colorIcon,
          //         icon: SvgPicture.asset(
          //             'assets/icons/svg/notification-icon.svg'),
          //         badgeNotificationCount: 35,
          //         onPressed: () {},
          //       ),
          //       HeaderIcon(
          //         iconColor: _colorIcon,
          //         icon: SvgPicture.asset('assets/icons/svg/cart-icon.svg'),
          //         badgeNotificationCount: 35,
          //         onPressed: () {},
          //       ),
          //     ],
          //   ),
          // ),
        ],
      ),
    ));
  }
}
