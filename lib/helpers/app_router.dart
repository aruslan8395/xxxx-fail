import 'package:flutter/material.dart';
import 'package:flutter_unity_widget_example/screens/category_screen.dart';
import 'package:flutter_unity_widget_example/screens/forgot_password_screen.dart';
import 'package:flutter_unity_widget_example/screens/home_screen.dart';
import 'package:flutter_unity_widget_example/screens/login_screen.dart';
import 'package:flutter_unity_widget_example/screens/order_screen.dart';
import 'package:flutter_unity_widget_example/screens/profilenotscreen_screen.dart';
import 'package:flutter_unity_widget_example/screens/scan_screen.dart';
import 'package:flutter_unity_widget_example/screens/signup_screen.dart';
import 'package:flutter_unity_widget_example/screens/test_screen.dart';
import 'package:flutter_unity_widget_example/widgets/app_navbar.dart';
import 'package:nil/nil.dart';

class AppRouter {
  Route onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => NavBar()); //LoginScreen()
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case '/navbar':
        return MaterialPageRoute(builder: (_) => NavBar());
      case '/test':
        return MaterialPageRoute(builder: (_) => TestScreen());
      case '/home':
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case '/category':
        return MaterialPageRoute(builder: (_) => CategoryScreen());
      case '/scan':
        return MaterialPageRoute(
          builder: (_) => ScanScreen(),
        );
      case '/order':
        return MaterialPageRoute(builder: (_) => OrderScreen());
      case '/profile':
        return MaterialPageRoute(builder: (_) => ProfileScreenNotLogin());
      case '/signup':
        return MaterialPageRoute(builder: (_) => const SignUpScreen());
      case '/forgot':
        return MaterialPageRoute(builder: (_) => ForgotPasswordScreen());
      default:
        return MaterialPageRoute(builder: (_) => const Nil());
    }
  }
}

class DetailArguments {
  final String product;
  DetailArguments(this.product);
}
