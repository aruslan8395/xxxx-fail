import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';

Text textWidget(
    {required String text,
    Key? key,
    String color = "default",
    int maxLines = 1,
    double size = 14,
    FontStyle fontStyle = FontStyle.normal,
    String weight = "N",
    String fontFamily = "",
    double? height,
    TextDecoration decoration = TextDecoration.none,
    TextAlign align = TextAlign.start,
    TextOverflow textOverflow = TextOverflow.visible,
    double letterSpacing = 0}) {
  return Text(
    text,
    textAlign: align,
    overflow: textOverflow,
    maxLines: maxLines,
    style: TextStyle(
      height: height,
      fontStyle: fontStyle,
      fontFamily: fontFamily.isNotEmpty ? fontFamily : "Dosis",
      letterSpacing: letterSpacing,
      decoration: decoration,
      decorationColor: masterColor(color),
      color: masterColor(color),
      fontSize: size,
      fontWeight: masterFontWeight(weight),
    ),
  );
}

FontWeight masterFontWeight([String weight = 'N']) {
  late FontWeight returnedFW;
  switch (weight) {
    case 'T':
      returnedFW = FontWeight.w100;
      break;
    case 'XL':
      returnedFW = FontWeight.w200;
      break;
    case 'L': // Light
      returnedFW = FontWeight.w300;
      break;
    case 'N': // Normal
      returnedFW = FontWeight.w400;
      break;
    case 'M': // Medium
      returnedFW = FontWeight.w500;
      break;
    case 'SB': // Bold
      returnedFW = FontWeight.w600;
      break;
    case 'B':
      returnedFW = FontWeight.w700;
      break;
    case 'XB':
      returnedFW = FontWeight.w800;
      break;
    case 'TB':
      returnedFW = FontWeight.w900;
      break;
  }
  return returnedFW;
}

Color masterColor([String color = "default", double opacity = 1]) {
  Color returnedColor;
  color.toLowerCase();
  switch (color) {
    case "primary":
      returnedColor = const Color(0xFF354C89);
      break;
    case "secondary":
      returnedColor = Colors.white;
      break;
    case "danger":
      returnedColor = const Color(0xFFFF605D);
      break;
    case "apple":
      returnedColor = const Color(0xFF414042);
      break;
    case "facebook":
      returnedColor = const Color(0xFF4363A9);
      break;
    case "grey":
      returnedColor = Colors.grey;
      break;
    case "disabled":
      returnedColor = const Color(0xFFF0F0F0);
      break;
    default:
      returnedColor = Color.fromRGBO(0, 0, 0, opacity);
      break;
  }
  return returnedColor;
}

Text textKeyWidget(
    {required String text,
    String color = "default",
    double size = 14,
    FontStyle fontStyle = FontStyle.normal,
    String weight = "N",
    double? height,
    String fontFamily = "",
    TextDecoration decoration = TextDecoration.none,
    TextAlign align = TextAlign.start,
    TextOverflow textOverflow = TextOverflow.visible,
    double letterSpacing = 0}) {
  return Text(
    text.tr().toString(),
    textAlign: align,
    overflow: textOverflow,
    style: TextStyle(
      fontFamily: fontFamily.isNotEmpty ? fontFamily : "Dosis",
      height: height,
      fontStyle: fontStyle,
      letterSpacing: letterSpacing,
      decoration: decoration,
      decorationColor: masterColor(color),
      color: masterColor(color),
      fontSize: size,
      fontWeight: masterFontWeight(weight),
    ),
  );
}

Text trademarkText() {
  return textWidget(
    color: 'secondary',
    text: '\u00a9 2021 Dekornata All Right Reserved',
    fontFamily: 'GT Walsheim Pro',
    size: 8,
  );
}

Widget loadingWidget() {
  return Center(
    child: CircularProgressIndicator(
      backgroundColor: masterColor('primary'),
    ),
  );
}

void showSnackBar(BuildContext context, String message) {
  final SnackBar snackBar = SnackBar(content: Text(message));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

bool isHide = false;
late UnityWidgetController unityWidgetController;

String convertNumberToCurrency(int number) {
  if (number == 0) return "0";

  String currency = '';
  final String reversedNumber = number.toString().split('').reversed.join();

  for (var i = 0; i < reversedNumber.length; i++) {
    int grabString = i + 3;

    if (grabString > reversedNumber.length) grabString = reversedNumber.length;

    if (i % 3 == 0) currency += '${reversedNumber.substring(i, grabString)}.';
  }

  final String formatted =
      currency.split('').reversed.join().substring(1, currency.length);

  return formatted.isNotEmpty ? formatted : '0';
}
