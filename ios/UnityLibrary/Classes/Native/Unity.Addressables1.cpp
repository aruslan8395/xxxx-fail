﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// DelegateList`1<System.Single>
struct DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5;
// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF;
// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean>>
struct DelegateList_1_t52A8167A6C97544426B924CCF66D6B38876C448E;
// ListWithEvents`1<UnityEngine.ResourceManagement.IUpdateReceiver>
struct ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A;
// ListWithEvents`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider>
struct ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE;
// PackedPlayModeBuildLogs
struct PackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23;
// PackedPlayModeBuildLogs/RuntimeBuildLog[]
struct RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B;
// System.Action`1<System.Single>
struct Action_1_t298B565CF49E0C72E4377AA3B30170F58D9F64FB;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>>
struct Action_1_tC7396721A16D97E46049382833189660B6FEA123;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>>
struct Action_1_t5FA24278A896AF4BD19B76F343A99FF5B92377B6;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>
struct Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403;
// System.Action`1<UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext>
struct Action_1_t7E2E77D49E6327A95CD26DD6B7B15371F40B3879;
// System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception>
struct Action_2_t13B49A7C8A6F369FD014EC82284A2788B38CD2AF;
// System.Action`4<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.Object>
struct Action_4_t00BC12A3A8BE88ECE96C624C5BF21F6134B4CC9D;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>
struct Dictionary_2_t0103DA6FE6ABFBFF0AAD96D7DD6D056FE201283A;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider>
struct Dictionary_2_tE62CF6EF7A0490AEC063959914E542596A7AAA2E;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct Dictionary_2_t2F95F954FC957D3411922E726F65355AB595789D;
// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct Dictionary_2_t1A2F98797A66B85EE82F7EF18DA6EC46172488E1;
// System.Collections.Generic.HashSet`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct HashSet_1_t12733814D81222896EFB6827B32A20B16A4B7D54;
// System.Collections.Generic.HashSet`1<UnityEngine.ResourceManagement.ResourceManager/InstanceOperation>
struct HashSet_1_t23D49383C703F59347C8C5D8BD437FA27F32141C;
// System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct IList_1_tA839EBE831AA697F04E70F745B9C7A5C4E3BAB04;
// System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog>
struct List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0;
// System.Collections.Generic.List`1<UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo>
struct List_1_t7C552F8C2E6EB7B78898EE4BBBC60F321F37CFE5;
// System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData>
struct List_1_t6A3E8E12BEA59AD97A4CCE0ABC71286067B7B157;
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778;
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>
struct List_1_tABAB852F391465C5F53EBD6DB0F50766773D82CD;
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.IUpdateReceiver>
struct List_1_tEDDE2A2CDB994C781B024A7ED67DFB45B834542E;
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>
struct List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>
struct Func_2_tB62D6B6E70CB1678DFC96AC52A112445C6292995;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AddressableAssets.AddressablesImpl
struct AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F;
// UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData
struct ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48;
// UnityEngine.AssetReferenceUILabelRestriction
struct AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC;
// UnityEngine.AssetReferenceUIRestriction
struct AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>
struct AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>
struct AsyncOperationBase_1_t982094A83C74A4F62EE507ADD2A4673A39CA4D39;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Collections.Generic.List`1<System.String>>
struct AsyncOperationBase_1_tF75817EFF39CF1858B6F34C0D7FCF8E85C949067;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>>
struct AsyncOperationBase_1_t323AC5CFDD6557DB9E7052A1F0986FF49B791931;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Object>
struct AsyncOperationBase_1_tAAF61937FB6883D39394C70C89821ECB0B49D8E5;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>
struct AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>
struct AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle[]
struct AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456;
// UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation
struct IAsyncOperation_t95BFE028EE1B4B3BF86C0A386CB3FE057D6E4104;
// UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation
struct InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D;
// UnityEngine.ResourceManagement.ResourceManager
struct ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99;
// UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider
struct IInstanceProvider_t77C8CD099706D5B4D157104EB25570F4E87D9148;
// UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider
struct ISceneProvider_tA53FD0CACA45676436E0C56C2A1CFDB59807CBB2;
// UnityEngine.ResourceManagement.Util.IAllocationStrategy
struct IAllocationStrategy_t5E79614EE5CAB3506B0EB1D43D1A062FE5F67CE5;
// UnityEngine.ResourceManagement.Util.ObjectInitializationData[]
struct ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tC7396721A16D97E46049382833189660B6FEA123_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Addressables_t8AD39E84886280F198451740C05CA7E8AF2C977D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AsyncOperationStatus_t598BB2ACEBD2067A004500935C76AA6D845EE005_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral3C1B63DEBF79CE695956E89613C7F2BEAFC4DEBC;
IL2CPP_EXTERN_C String_t* _stringLiteral4A5A52C51940D8628AA7982A664B485789A9EF88;
IL2CPP_EXTERN_C String_t* _stringLiteral60C190087C7D3A3C22FFF66FC21F515D7AD5572E;
IL2CPP_EXTERN_C String_t* _stringLiteralA92DB0F71F769FFBF78C8E2664FA6CBB7A5E3387;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralECEC38FACF03D69014C82B8F5C23E6D265A14DCF;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m192B30F1BE49B025A7E9942E52258DEF2B7DBDA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AddressablesImpl_Release_TisIList_1_tA839EBE831AA697F04E70F745B9C7A5C4E3BAB04_m20D71A2F51CF9A928F32F0DA21B864A948FD37E5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationBase_1_Complete_m4DA85FC017FAB08C949D44BC24D6288684163447_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationBase_1__ctor_mCFF2B72AFE0F6AA1A554A1EC61672D880386CCAE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_add_Completed_mD28383C96433437034C7700C534C8A5374DEE6D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_get_DebugName_mB9D572DD987A5AFC28F4E6398305FAA51BE680AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_get_Result_m4AD1404D8FBA522A55420CEE072E5CBF2FEB58F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_get_Result_m50166C1809868992DA99771109671F872426DE6F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_get_Status_mB0A602F07AAA2FD4462731CD0F2E92ED9130393F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m2329B6D018D3E680CF80034B110AAD82890A3A57_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m0BAB547D027E681F5AC5FBC80BACA58C958138EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mA9B5E64E1849F2AC2065781986A55CCD9DC9A50F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m5481EE2DB20534CFF47F7BF2ABDA5E44D1692443_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m801F71E2980D37299592A623FB92A141F866225A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InitalizationObjectsOperation_U3CExecuteU3Eb__7_0_mDDBD37CA57E5F64767F1A289DA18292ACF0F939C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* JsonUtility_FromJson_TisPackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23_m3679717B41751ED5E1FCE12D32AD9838229179FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m391465EE5D1E99359D7FE152BDCA406567D461F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m4C6EBC1F113010F19C7D7803D372E64478CF02E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mAC57F8B0BFC67D139F9FEF652DA4CCEB9A60E968_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m7304DC41D77EB6CBB389ACB5225B632038E33694_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AssetReferenceUILabelRestriction_ToString_m610DD1BF67CD7978120DE81D61CBC05EFAF6FA68_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InitalizationObjectsOperation_Execute_m9936831C2834EB8F0549D62194578368C376F85F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InitalizationObjectsOperation_LogRuntimeWarnings_m4FA0B605859EFF68C1730534149964CA532B47F4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InitalizationObjectsOperation_U3CExecuteU3Eb__7_0_mDDBD37CA57E5F64767F1A289DA18292ACF0F939C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InitalizationObjectsOperation__ctor_mEBF54CDAB8D99111796BAA542124CB6C91452123_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InitalizationObjectsOperation_get_DebugName_m0BB1AD62757F2E6F1784ECEF82D0A3B4031EDBD5_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// PackedPlayModeBuildLogs
struct PackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog> PackedPlayModeBuildLogs::m_RuntimeBuildLogs
	List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * ___m_RuntimeBuildLogs_0;

public:
	inline static int32_t get_offset_of_m_RuntimeBuildLogs_0() { return static_cast<int32_t>(offsetof(PackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23, ___m_RuntimeBuildLogs_0)); }
	inline List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * get_m_RuntimeBuildLogs_0() const { return ___m_RuntimeBuildLogs_0; }
	inline List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 ** get_address_of_m_RuntimeBuildLogs_0() { return &___m_RuntimeBuildLogs_0; }
	inline void set_m_RuntimeBuildLogs_0(List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * value)
	{
		___m_RuntimeBuildLogs_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RuntimeBuildLogs_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog>
struct List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0, ____items_1)); }
	inline RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B* get__items_1() const { return ____items_1; }
	inline RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0_StaticFields, ____emptyArray_5)); }
	inline RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B* get__emptyArray_5() const { return ____emptyArray_5; }
	inline RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(RuntimeBuildLogU5BU5D_t82EAE0263667D8AABA4F63BFE0F4ADE5218AF31B* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778, ____items_1)); }
	inline AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* get__items_1() const { return ____items_1; }
	inline AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778_StaticFields, ____emptyArray_5)); }
	inline AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* get__emptyArray_5() const { return ____emptyArray_5; }
	inline AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>
struct List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A, ____items_1)); }
	inline ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* get__items_1() const { return ____items_1; }
	inline ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A_StaticFields, ____emptyArray_5)); }
	inline ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.StringBuilder
struct StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.ResourceManagement.ResourceManager
struct ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.ResourceManagement.ResourceManager::postProfilerEvents
	bool ___postProfilerEvents_0;
	// System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.ResourceManagement.ResourceManager::<InternalIdTransformFunc>k__BackingField
	Func_2_tB62D6B6E70CB1678DFC96AC52A112445C6292995 * ___U3CInternalIdTransformFuncU3Ek__BackingField_2;
	// System.Boolean UnityEngine.ResourceManagement.ResourceManager::CallbackHooksEnabled
	bool ___CallbackHooksEnabled_3;
	// ListWithEvents`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::m_ResourceProviders
	ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE * ___m_ResourceProviders_4;
	// UnityEngine.ResourceManagement.Util.IAllocationStrategy UnityEngine.ResourceManagement.ResourceManager::m_allocator
	RuntimeObject* ___m_allocator_5;
	// ListWithEvents`1<UnityEngine.ResourceManagement.IUpdateReceiver> UnityEngine.ResourceManagement.ResourceManager::m_UpdateReceivers
	ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A * ___m_UpdateReceivers_6;
	// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.IUpdateReceiver> UnityEngine.ResourceManagement.ResourceManager::m_UpdateReceiversToRemove
	List_1_tEDDE2A2CDB994C781B024A7ED67DFB45B834542E * ___m_UpdateReceiversToRemove_7;
	// System.Boolean UnityEngine.ResourceManagement.ResourceManager::m_UpdatingReceivers
	bool ___m_UpdatingReceivers_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::m_providerMap
	Dictionary_2_tE62CF6EF7A0490AEC063959914E542596A7AAA2E * ___m_providerMap_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_AssetOperationCache
	Dictionary_2_t0103DA6FE6ABFBFF0AAD96D7DD6D056FE201283A * ___m_AssetOperationCache_10;
	// System.Collections.Generic.HashSet`1<UnityEngine.ResourceManagement.ResourceManager/InstanceOperation> UnityEngine.ResourceManagement.ResourceManager::m_TrackedInstanceOperations
	HashSet_1_t23D49383C703F59347C8C5D8BD437FA27F32141C * ___m_TrackedInstanceOperations_11;
	// DelegateList`1<System.Single> UnityEngine.ResourceManagement.ResourceManager::m_UpdateCallbacks
	DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5 * ___m_UpdateCallbacks_12;
	// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_DeferredCompleteCallbacks
	List_1_tABAB852F391465C5F53EBD6DB0F50766773D82CD * ___m_DeferredCompleteCallbacks_13;
	// System.Action`4<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.Object> UnityEngine.ResourceManagement.ResourceManager::m_obsoleteDiagnosticsHandler
	Action_4_t00BC12A3A8BE88ECE96C624C5BF21F6134B4CC9D * ___m_obsoleteDiagnosticsHandler_14;
	// System.Action`1<UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext> UnityEngine.ResourceManagement.ResourceManager::m_diagnosticsHandler
	Action_1_t7E2E77D49E6327A95CD26DD6B7B15371F40B3879 * ___m_diagnosticsHandler_15;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_ReleaseOpNonCached
	Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * ___m_ReleaseOpNonCached_16;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_ReleaseOpCached
	Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * ___m_ReleaseOpCached_17;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_ReleaseInstanceOp
	Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * ___m_ReleaseInstanceOp_18;
	// UnityEngine.Networking.CertificateHandler UnityEngine.ResourceManagement.ResourceManager::<CertificateHandlerInstance>k__BackingField
	CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * ___U3CCertificateHandlerInstanceU3Ek__BackingField_21;
	// System.Boolean UnityEngine.ResourceManagement.ResourceManager::m_RegisteredForCallbacks
	bool ___m_RegisteredForCallbacks_22;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> UnityEngine.ResourceManagement.ResourceManager::m_ProviderOperationTypeCache
	Dictionary_2_t1A2F98797A66B85EE82F7EF18DA6EC46172488E1 * ___m_ProviderOperationTypeCache_23;

public:
	inline static int32_t get_offset_of_postProfilerEvents_0() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___postProfilerEvents_0)); }
	inline bool get_postProfilerEvents_0() const { return ___postProfilerEvents_0; }
	inline bool* get_address_of_postProfilerEvents_0() { return &___postProfilerEvents_0; }
	inline void set_postProfilerEvents_0(bool value)
	{
		___postProfilerEvents_0 = value;
	}

	inline static int32_t get_offset_of_U3CInternalIdTransformFuncU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___U3CInternalIdTransformFuncU3Ek__BackingField_2)); }
	inline Func_2_tB62D6B6E70CB1678DFC96AC52A112445C6292995 * get_U3CInternalIdTransformFuncU3Ek__BackingField_2() const { return ___U3CInternalIdTransformFuncU3Ek__BackingField_2; }
	inline Func_2_tB62D6B6E70CB1678DFC96AC52A112445C6292995 ** get_address_of_U3CInternalIdTransformFuncU3Ek__BackingField_2() { return &___U3CInternalIdTransformFuncU3Ek__BackingField_2; }
	inline void set_U3CInternalIdTransformFuncU3Ek__BackingField_2(Func_2_tB62D6B6E70CB1678DFC96AC52A112445C6292995 * value)
	{
		___U3CInternalIdTransformFuncU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInternalIdTransformFuncU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_CallbackHooksEnabled_3() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___CallbackHooksEnabled_3)); }
	inline bool get_CallbackHooksEnabled_3() const { return ___CallbackHooksEnabled_3; }
	inline bool* get_address_of_CallbackHooksEnabled_3() { return &___CallbackHooksEnabled_3; }
	inline void set_CallbackHooksEnabled_3(bool value)
	{
		___CallbackHooksEnabled_3 = value;
	}

	inline static int32_t get_offset_of_m_ResourceProviders_4() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ResourceProviders_4)); }
	inline ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE * get_m_ResourceProviders_4() const { return ___m_ResourceProviders_4; }
	inline ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE ** get_address_of_m_ResourceProviders_4() { return &___m_ResourceProviders_4; }
	inline void set_m_ResourceProviders_4(ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE * value)
	{
		___m_ResourceProviders_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ResourceProviders_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_allocator_5() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_allocator_5)); }
	inline RuntimeObject* get_m_allocator_5() const { return ___m_allocator_5; }
	inline RuntimeObject** get_address_of_m_allocator_5() { return &___m_allocator_5; }
	inline void set_m_allocator_5(RuntimeObject* value)
	{
		___m_allocator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_allocator_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateReceivers_6() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_UpdateReceivers_6)); }
	inline ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A * get_m_UpdateReceivers_6() const { return ___m_UpdateReceivers_6; }
	inline ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A ** get_address_of_m_UpdateReceivers_6() { return &___m_UpdateReceivers_6; }
	inline void set_m_UpdateReceivers_6(ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A * value)
	{
		___m_UpdateReceivers_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateReceivers_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateReceiversToRemove_7() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_UpdateReceiversToRemove_7)); }
	inline List_1_tEDDE2A2CDB994C781B024A7ED67DFB45B834542E * get_m_UpdateReceiversToRemove_7() const { return ___m_UpdateReceiversToRemove_7; }
	inline List_1_tEDDE2A2CDB994C781B024A7ED67DFB45B834542E ** get_address_of_m_UpdateReceiversToRemove_7() { return &___m_UpdateReceiversToRemove_7; }
	inline void set_m_UpdateReceiversToRemove_7(List_1_tEDDE2A2CDB994C781B024A7ED67DFB45B834542E * value)
	{
		___m_UpdateReceiversToRemove_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateReceiversToRemove_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdatingReceivers_8() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_UpdatingReceivers_8)); }
	inline bool get_m_UpdatingReceivers_8() const { return ___m_UpdatingReceivers_8; }
	inline bool* get_address_of_m_UpdatingReceivers_8() { return &___m_UpdatingReceivers_8; }
	inline void set_m_UpdatingReceivers_8(bool value)
	{
		___m_UpdatingReceivers_8 = value;
	}

	inline static int32_t get_offset_of_m_providerMap_9() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_providerMap_9)); }
	inline Dictionary_2_tE62CF6EF7A0490AEC063959914E542596A7AAA2E * get_m_providerMap_9() const { return ___m_providerMap_9; }
	inline Dictionary_2_tE62CF6EF7A0490AEC063959914E542596A7AAA2E ** get_address_of_m_providerMap_9() { return &___m_providerMap_9; }
	inline void set_m_providerMap_9(Dictionary_2_tE62CF6EF7A0490AEC063959914E542596A7AAA2E * value)
	{
		___m_providerMap_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_providerMap_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_AssetOperationCache_10() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_AssetOperationCache_10)); }
	inline Dictionary_2_t0103DA6FE6ABFBFF0AAD96D7DD6D056FE201283A * get_m_AssetOperationCache_10() const { return ___m_AssetOperationCache_10; }
	inline Dictionary_2_t0103DA6FE6ABFBFF0AAD96D7DD6D056FE201283A ** get_address_of_m_AssetOperationCache_10() { return &___m_AssetOperationCache_10; }
	inline void set_m_AssetOperationCache_10(Dictionary_2_t0103DA6FE6ABFBFF0AAD96D7DD6D056FE201283A * value)
	{
		___m_AssetOperationCache_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AssetOperationCache_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedInstanceOperations_11() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_TrackedInstanceOperations_11)); }
	inline HashSet_1_t23D49383C703F59347C8C5D8BD437FA27F32141C * get_m_TrackedInstanceOperations_11() const { return ___m_TrackedInstanceOperations_11; }
	inline HashSet_1_t23D49383C703F59347C8C5D8BD437FA27F32141C ** get_address_of_m_TrackedInstanceOperations_11() { return &___m_TrackedInstanceOperations_11; }
	inline void set_m_TrackedInstanceOperations_11(HashSet_1_t23D49383C703F59347C8C5D8BD437FA27F32141C * value)
	{
		___m_TrackedInstanceOperations_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedInstanceOperations_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateCallbacks_12() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_UpdateCallbacks_12)); }
	inline DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5 * get_m_UpdateCallbacks_12() const { return ___m_UpdateCallbacks_12; }
	inline DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5 ** get_address_of_m_UpdateCallbacks_12() { return &___m_UpdateCallbacks_12; }
	inline void set_m_UpdateCallbacks_12(DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5 * value)
	{
		___m_UpdateCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateCallbacks_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeferredCompleteCallbacks_13() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_DeferredCompleteCallbacks_13)); }
	inline List_1_tABAB852F391465C5F53EBD6DB0F50766773D82CD * get_m_DeferredCompleteCallbacks_13() const { return ___m_DeferredCompleteCallbacks_13; }
	inline List_1_tABAB852F391465C5F53EBD6DB0F50766773D82CD ** get_address_of_m_DeferredCompleteCallbacks_13() { return &___m_DeferredCompleteCallbacks_13; }
	inline void set_m_DeferredCompleteCallbacks_13(List_1_tABAB852F391465C5F53EBD6DB0F50766773D82CD * value)
	{
		___m_DeferredCompleteCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeferredCompleteCallbacks_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_obsoleteDiagnosticsHandler_14() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_obsoleteDiagnosticsHandler_14)); }
	inline Action_4_t00BC12A3A8BE88ECE96C624C5BF21F6134B4CC9D * get_m_obsoleteDiagnosticsHandler_14() const { return ___m_obsoleteDiagnosticsHandler_14; }
	inline Action_4_t00BC12A3A8BE88ECE96C624C5BF21F6134B4CC9D ** get_address_of_m_obsoleteDiagnosticsHandler_14() { return &___m_obsoleteDiagnosticsHandler_14; }
	inline void set_m_obsoleteDiagnosticsHandler_14(Action_4_t00BC12A3A8BE88ECE96C624C5BF21F6134B4CC9D * value)
	{
		___m_obsoleteDiagnosticsHandler_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_obsoleteDiagnosticsHandler_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_diagnosticsHandler_15() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_diagnosticsHandler_15)); }
	inline Action_1_t7E2E77D49E6327A95CD26DD6B7B15371F40B3879 * get_m_diagnosticsHandler_15() const { return ___m_diagnosticsHandler_15; }
	inline Action_1_t7E2E77D49E6327A95CD26DD6B7B15371F40B3879 ** get_address_of_m_diagnosticsHandler_15() { return &___m_diagnosticsHandler_15; }
	inline void set_m_diagnosticsHandler_15(Action_1_t7E2E77D49E6327A95CD26DD6B7B15371F40B3879 * value)
	{
		___m_diagnosticsHandler_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_diagnosticsHandler_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReleaseOpNonCached_16() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ReleaseOpNonCached_16)); }
	inline Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * get_m_ReleaseOpNonCached_16() const { return ___m_ReleaseOpNonCached_16; }
	inline Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 ** get_address_of_m_ReleaseOpNonCached_16() { return &___m_ReleaseOpNonCached_16; }
	inline void set_m_ReleaseOpNonCached_16(Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * value)
	{
		___m_ReleaseOpNonCached_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReleaseOpNonCached_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReleaseOpCached_17() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ReleaseOpCached_17)); }
	inline Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * get_m_ReleaseOpCached_17() const { return ___m_ReleaseOpCached_17; }
	inline Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 ** get_address_of_m_ReleaseOpCached_17() { return &___m_ReleaseOpCached_17; }
	inline void set_m_ReleaseOpCached_17(Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * value)
	{
		___m_ReleaseOpCached_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReleaseOpCached_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReleaseInstanceOp_18() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ReleaseInstanceOp_18)); }
	inline Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * get_m_ReleaseInstanceOp_18() const { return ___m_ReleaseInstanceOp_18; }
	inline Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 ** get_address_of_m_ReleaseInstanceOp_18() { return &___m_ReleaseInstanceOp_18; }
	inline void set_m_ReleaseInstanceOp_18(Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * value)
	{
		___m_ReleaseInstanceOp_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReleaseInstanceOp_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCertificateHandlerInstanceU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___U3CCertificateHandlerInstanceU3Ek__BackingField_21)); }
	inline CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * get_U3CCertificateHandlerInstanceU3Ek__BackingField_21() const { return ___U3CCertificateHandlerInstanceU3Ek__BackingField_21; }
	inline CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 ** get_address_of_U3CCertificateHandlerInstanceU3Ek__BackingField_21() { return &___U3CCertificateHandlerInstanceU3Ek__BackingField_21; }
	inline void set_U3CCertificateHandlerInstanceU3Ek__BackingField_21(CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * value)
	{
		___U3CCertificateHandlerInstanceU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCertificateHandlerInstanceU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_RegisteredForCallbacks_22() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_RegisteredForCallbacks_22)); }
	inline bool get_m_RegisteredForCallbacks_22() const { return ___m_RegisteredForCallbacks_22; }
	inline bool* get_address_of_m_RegisteredForCallbacks_22() { return &___m_RegisteredForCallbacks_22; }
	inline void set_m_RegisteredForCallbacks_22(bool value)
	{
		___m_RegisteredForCallbacks_22 = value;
	}

	inline static int32_t get_offset_of_m_ProviderOperationTypeCache_23() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ProviderOperationTypeCache_23)); }
	inline Dictionary_2_t1A2F98797A66B85EE82F7EF18DA6EC46172488E1 * get_m_ProviderOperationTypeCache_23() const { return ___m_ProviderOperationTypeCache_23; }
	inline Dictionary_2_t1A2F98797A66B85EE82F7EF18DA6EC46172488E1 ** get_address_of_m_ProviderOperationTypeCache_23() { return &___m_ProviderOperationTypeCache_23; }
	inline void set_m_ProviderOperationTypeCache_23(Dictionary_2_t1A2F98797A66B85EE82F7EF18DA6EC46172488E1 * value)
	{
		___m_ProviderOperationTypeCache_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ProviderOperationTypeCache_23), (void*)value);
	}
};

struct ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99_StaticFields
{
public:
	// System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception> UnityEngine.ResourceManagement.ResourceManager::<ExceptionHandler>k__BackingField
	Action_2_t13B49A7C8A6F369FD014EC82284A2788B38CD2AF * ___U3CExceptionHandlerU3Ek__BackingField_1;
	// System.Int32 UnityEngine.ResourceManagement.ResourceManager::s_GroupOperationTypeHash
	int32_t ___s_GroupOperationTypeHash_19;
	// System.Int32 UnityEngine.ResourceManagement.ResourceManager::s_InstanceOperationTypeHash
	int32_t ___s_InstanceOperationTypeHash_20;

public:
	inline static int32_t get_offset_of_U3CExceptionHandlerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99_StaticFields, ___U3CExceptionHandlerU3Ek__BackingField_1)); }
	inline Action_2_t13B49A7C8A6F369FD014EC82284A2788B38CD2AF * get_U3CExceptionHandlerU3Ek__BackingField_1() const { return ___U3CExceptionHandlerU3Ek__BackingField_1; }
	inline Action_2_t13B49A7C8A6F369FD014EC82284A2788B38CD2AF ** get_address_of_U3CExceptionHandlerU3Ek__BackingField_1() { return &___U3CExceptionHandlerU3Ek__BackingField_1; }
	inline void set_U3CExceptionHandlerU3Ek__BackingField_1(Action_2_t13B49A7C8A6F369FD014EC82284A2788B38CD2AF * value)
	{
		___U3CExceptionHandlerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExceptionHandlerU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_s_GroupOperationTypeHash_19() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99_StaticFields, ___s_GroupOperationTypeHash_19)); }
	inline int32_t get_s_GroupOperationTypeHash_19() const { return ___s_GroupOperationTypeHash_19; }
	inline int32_t* get_address_of_s_GroupOperationTypeHash_19() { return &___s_GroupOperationTypeHash_19; }
	inline void set_s_GroupOperationTypeHash_19(int32_t value)
	{
		___s_GroupOperationTypeHash_19 = value;
	}

	inline static int32_t get_offset_of_s_InstanceOperationTypeHash_20() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99_StaticFields, ___s_InstanceOperationTypeHash_20)); }
	inline int32_t get_s_InstanceOperationTypeHash_20() const { return ___s_InstanceOperationTypeHash_20; }
	inline int32_t* get_address_of_s_InstanceOperationTypeHash_20() { return &___s_InstanceOperationTypeHash_20; }
	inline void set_s_InstanceOperationTypeHash_20(int32_t value)
	{
		___s_InstanceOperationTypeHash_20 = value;
	}
};


// System.Boolean
struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.AssetReferenceUIRestriction
struct AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle
struct AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::m_InternalOp
	RuntimeObject* ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::m_Version
	int32_t ___m_Version_1;
	// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::m_LocationName
	String_t* ___m_LocationName_2;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185, ___m_InternalOp_0)); }
	inline RuntimeObject* get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline RuntimeObject** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(RuntimeObject* value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}

	inline static int32_t get_offset_of_m_LocationName_2() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185, ___m_LocationName_2)); }
	inline String_t* get_m_LocationName_2() const { return ___m_LocationName_2; }
	inline String_t** get_address_of_m_LocationName_2() { return &___m_LocationName_2; }
	inline void set_m_LocationName_2(String_t* value)
	{
		___m_LocationName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocationName_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle
struct AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185_marshaled_pinvoke
{
	RuntimeObject* ___m_InternalOp_0;
	int32_t ___m_Version_1;
	char* ___m_LocationName_2;
};
// Native definition for COM marshalling of UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle
struct AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185_marshaled_com
{
	RuntimeObject* ___m_InternalOp_0;
	int32_t ___m_Version_1;
	Il2CppChar* ___m_LocationName_2;
};

// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>
struct AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_t982094A83C74A4F62EE507ADD2A4673A39CA4D39 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;
	// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_LocationName
	String_t* ___m_LocationName_2;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_t982094A83C74A4F62EE507ADD2A4673A39CA4D39 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_t982094A83C74A4F62EE507ADD2A4673A39CA4D39 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_t982094A83C74A4F62EE507ADD2A4673A39CA4D39 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}

	inline static int32_t get_offset_of_m_LocationName_2() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED, ___m_LocationName_2)); }
	inline String_t* get_m_LocationName_2() const { return ___m_LocationName_2; }
	inline String_t** get_address_of_m_LocationName_2() { return &___m_LocationName_2; }
	inline void set_m_LocationName_2(String_t* value)
	{
		___m_LocationName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocationName_2), (void*)value);
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>>
struct AsyncOperationHandle_1_t95C8103BAFB5BEBF279E1F28B0DD9DA099EE9562 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_tF75817EFF39CF1858B6F34C0D7FCF8E85C949067 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;
	// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_LocationName
	String_t* ___m_LocationName_2;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t95C8103BAFB5BEBF279E1F28B0DD9DA099EE9562, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_tF75817EFF39CF1858B6F34C0D7FCF8E85C949067 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_tF75817EFF39CF1858B6F34C0D7FCF8E85C949067 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_tF75817EFF39CF1858B6F34C0D7FCF8E85C949067 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t95C8103BAFB5BEBF279E1F28B0DD9DA099EE9562, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}

	inline static int32_t get_offset_of_m_LocationName_2() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t95C8103BAFB5BEBF279E1F28B0DD9DA099EE9562, ___m_LocationName_2)); }
	inline String_t* get_m_LocationName_2() const { return ___m_LocationName_2; }
	inline String_t** get_address_of_m_LocationName_2() { return &___m_LocationName_2; }
	inline void set_m_LocationName_2(String_t* value)
	{
		___m_LocationName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocationName_2), (void*)value);
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>>
struct AsyncOperationHandle_1_t16C2B4678E91F85C98BD21DE4C5E30BFE2821EC5 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_t323AC5CFDD6557DB9E7052A1F0986FF49B791931 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;
	// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_LocationName
	String_t* ___m_LocationName_2;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t16C2B4678E91F85C98BD21DE4C5E30BFE2821EC5, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_t323AC5CFDD6557DB9E7052A1F0986FF49B791931 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_t323AC5CFDD6557DB9E7052A1F0986FF49B791931 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_t323AC5CFDD6557DB9E7052A1F0986FF49B791931 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t16C2B4678E91F85C98BD21DE4C5E30BFE2821EC5, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}

	inline static int32_t get_offset_of_m_LocationName_2() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t16C2B4678E91F85C98BD21DE4C5E30BFE2821EC5, ___m_LocationName_2)); }
	inline String_t* get_m_LocationName_2() const { return ___m_LocationName_2; }
	inline String_t** get_address_of_m_LocationName_2() { return &___m_LocationName_2; }
	inline void set_m_LocationName_2(String_t* value)
	{
		___m_LocationName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocationName_2), (void*)value);
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>
struct AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_tAAF61937FB6883D39394C70C89821ECB0B49D8E5 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;
	// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_LocationName
	String_t* ___m_LocationName_2;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_tAAF61937FB6883D39394C70C89821ECB0B49D8E5 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_tAAF61937FB6883D39394C70C89821ECB0B49D8E5 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_tAAF61937FB6883D39394C70C89821ECB0B49D8E5 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}

	inline static int32_t get_offset_of_m_LocationName_2() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457, ___m_LocationName_2)); }
	inline String_t* get_m_LocationName_2() const { return ___m_LocationName_2; }
	inline String_t** get_address_of_m_LocationName_2() { return &___m_LocationName_2; }
	inline void set_m_LocationName_2(String_t* value)
	{
		___m_LocationName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocationName_2), (void*)value);
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>
struct AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;
	// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_LocationName
	String_t* ___m_LocationName_2;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}

	inline static int32_t get_offset_of_m_LocationName_2() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB, ___m_LocationName_2)); }
	inline String_t* get_m_LocationName_2() const { return ___m_LocationName_2; }
	inline String_t** get_address_of_m_LocationName_2() { return &___m_LocationName_2; }
	inline void set_m_LocationName_2(String_t* value)
	{
		___m_LocationName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocationName_2), (void*)value);
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>
struct AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;
	// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_LocationName
	String_t* ___m_LocationName_2;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}

	inline static int32_t get_offset_of_m_LocationName_2() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89, ___m_LocationName_2)); }
	inline String_t* get_m_LocationName_2() const { return ___m_LocationName_2; }
	inline String_t** get_address_of_m_LocationName_2() { return &___m_LocationName_2; }
	inline void set_m_LocationName_2(String_t* value)
	{
		___m_LocationName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocationName_2), (void*)value);
	}
};


// UnityEngine.ResourceManagement.Util.SerializedType
struct SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 
{
public:
	// System.String UnityEngine.ResourceManagement.Util.SerializedType::m_AssemblyName
	String_t* ___m_AssemblyName_0;
	// System.String UnityEngine.ResourceManagement.Util.SerializedType::m_ClassName
	String_t* ___m_ClassName_1;
	// System.Type UnityEngine.ResourceManagement.Util.SerializedType::m_CachedType
	Type_t * ___m_CachedType_2;
	// System.Boolean UnityEngine.ResourceManagement.Util.SerializedType::<ValueChanged>k__BackingField
	bool ___U3CValueChangedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_AssemblyName_0() { return static_cast<int32_t>(offsetof(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84, ___m_AssemblyName_0)); }
	inline String_t* get_m_AssemblyName_0() const { return ___m_AssemblyName_0; }
	inline String_t** get_address_of_m_AssemblyName_0() { return &___m_AssemblyName_0; }
	inline void set_m_AssemblyName_0(String_t* value)
	{
		___m_AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AssemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ClassName_1() { return static_cast<int32_t>(offsetof(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84, ___m_ClassName_1)); }
	inline String_t* get_m_ClassName_1() const { return ___m_ClassName_1; }
	inline String_t** get_address_of_m_ClassName_1() { return &___m_ClassName_1; }
	inline void set_m_ClassName_1(String_t* value)
	{
		___m_ClassName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ClassName_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedType_2() { return static_cast<int32_t>(offsetof(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84, ___m_CachedType_2)); }
	inline Type_t * get_m_CachedType_2() const { return ___m_CachedType_2; }
	inline Type_t ** get_address_of_m_CachedType_2() { return &___m_CachedType_2; }
	inline void set_m_CachedType_2(Type_t * value)
	{
		___m_CachedType_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedType_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueChangedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84, ___U3CValueChangedU3Ek__BackingField_3)); }
	inline bool get_U3CValueChangedU3Ek__BackingField_3() const { return ___U3CValueChangedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CValueChangedU3Ek__BackingField_3() { return &___U3CValueChangedU3Ek__BackingField_3; }
	inline void set_U3CValueChangedU3Ek__BackingField_3(bool value)
	{
		___U3CValueChangedU3Ek__BackingField_3 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ResourceManagement.Util.SerializedType
struct SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84_marshaled_pinvoke
{
	char* ___m_AssemblyName_0;
	char* ___m_ClassName_1;
	Type_t * ___m_CachedType_2;
	int32_t ___U3CValueChangedU3Ek__BackingField_3;
};
// Native definition for COM marshalling of UnityEngine.ResourceManagement.Util.SerializedType
struct SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84_marshaled_com
{
	Il2CppChar* ___m_AssemblyName_0;
	Il2CppChar* ___m_ClassName_1;
	Type_t * ___m_CachedType_2;
	int32_t ___U3CValueChangedU3Ek__BackingField_3;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Reflection.BindingFlags
struct BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.AddressableAssets.AddressablesImpl
struct AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F  : public RuntimeObject
{
public:
	// UnityEngine.ResourceManagement.ResourceManager UnityEngine.AddressableAssets.AddressablesImpl::m_ResourceManager
	ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * ___m_ResourceManager_0;
	// UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider UnityEngine.AddressableAssets.AddressablesImpl::m_InstanceProvider
	RuntimeObject* ___m_InstanceProvider_1;
	// UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider UnityEngine.AddressableAssets.AddressablesImpl::SceneProvider
	RuntimeObject* ___SceneProvider_3;
	// System.Collections.Generic.List`1<UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo> UnityEngine.AddressableAssets.AddressablesImpl::m_ResourceLocators
	List_1_t7C552F8C2E6EB7B78898EE4BBBC60F321F37CFE5 * ___m_ResourceLocators_4;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::m_InitializationOperation
	AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89  ___m_InitializationOperation_5;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>> UnityEngine.AddressableAssets.AddressablesImpl::m_ActiveCheckUpdateOperation
	AsyncOperationHandle_1_t95C8103BAFB5BEBF279E1F28B0DD9DA099EE9562  ___m_ActiveCheckUpdateOperation_6;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.AddressablesImpl::m_ActiveUpdateOperation
	AsyncOperationHandle_1_t16C2B4678E91F85C98BD21DE4C5E30BFE2821EC5  ___m_ActiveUpdateOperation_7;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.AddressableAssets.AddressablesImpl::m_OnHandleCompleteAction
	Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * ___m_OnHandleCompleteAction_8;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.AddressableAssets.AddressablesImpl::m_OnSceneHandleCompleteAction
	Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * ___m_OnSceneHandleCompleteAction_9;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.AddressableAssets.AddressablesImpl::m_OnHandleDestroyedAction
	Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * ___m_OnHandleDestroyedAction_10;
	// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.AddressableAssets.AddressablesImpl::m_resultToHandle
	Dictionary_2_t2F95F954FC957D3411922E726F65355AB595789D * ___m_resultToHandle_11;
	// System.Collections.Generic.HashSet`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.AddressableAssets.AddressablesImpl::m_SceneInstances
	HashSet_1_t12733814D81222896EFB6827B32A20B16A4B7D54 * ___m_SceneInstances_12;
	// System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::hasStartedInitialization
	bool ___hasStartedInitialization_13;

public:
	inline static int32_t get_offset_of_m_ResourceManager_0() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_ResourceManager_0)); }
	inline ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * get_m_ResourceManager_0() const { return ___m_ResourceManager_0; }
	inline ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 ** get_address_of_m_ResourceManager_0() { return &___m_ResourceManager_0; }
	inline void set_m_ResourceManager_0(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * value)
	{
		___m_ResourceManager_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ResourceManager_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_InstanceProvider_1() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_InstanceProvider_1)); }
	inline RuntimeObject* get_m_InstanceProvider_1() const { return ___m_InstanceProvider_1; }
	inline RuntimeObject** get_address_of_m_InstanceProvider_1() { return &___m_InstanceProvider_1; }
	inline void set_m_InstanceProvider_1(RuntimeObject* value)
	{
		___m_InstanceProvider_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InstanceProvider_1), (void*)value);
	}

	inline static int32_t get_offset_of_SceneProvider_3() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___SceneProvider_3)); }
	inline RuntimeObject* get_SceneProvider_3() const { return ___SceneProvider_3; }
	inline RuntimeObject** get_address_of_SceneProvider_3() { return &___SceneProvider_3; }
	inline void set_SceneProvider_3(RuntimeObject* value)
	{
		___SceneProvider_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SceneProvider_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ResourceLocators_4() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_ResourceLocators_4)); }
	inline List_1_t7C552F8C2E6EB7B78898EE4BBBC60F321F37CFE5 * get_m_ResourceLocators_4() const { return ___m_ResourceLocators_4; }
	inline List_1_t7C552F8C2E6EB7B78898EE4BBBC60F321F37CFE5 ** get_address_of_m_ResourceLocators_4() { return &___m_ResourceLocators_4; }
	inline void set_m_ResourceLocators_4(List_1_t7C552F8C2E6EB7B78898EE4BBBC60F321F37CFE5 * value)
	{
		___m_ResourceLocators_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ResourceLocators_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_InitializationOperation_5() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_InitializationOperation_5)); }
	inline AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89  get_m_InitializationOperation_5() const { return ___m_InitializationOperation_5; }
	inline AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89 * get_address_of_m_InitializationOperation_5() { return &___m_InitializationOperation_5; }
	inline void set_m_InitializationOperation_5(AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89  value)
	{
		___m_InitializationOperation_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_InitializationOperation_5))->___m_InternalOp_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_InitializationOperation_5))->___m_LocationName_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_ActiveCheckUpdateOperation_6() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_ActiveCheckUpdateOperation_6)); }
	inline AsyncOperationHandle_1_t95C8103BAFB5BEBF279E1F28B0DD9DA099EE9562  get_m_ActiveCheckUpdateOperation_6() const { return ___m_ActiveCheckUpdateOperation_6; }
	inline AsyncOperationHandle_1_t95C8103BAFB5BEBF279E1F28B0DD9DA099EE9562 * get_address_of_m_ActiveCheckUpdateOperation_6() { return &___m_ActiveCheckUpdateOperation_6; }
	inline void set_m_ActiveCheckUpdateOperation_6(AsyncOperationHandle_1_t95C8103BAFB5BEBF279E1F28B0DD9DA099EE9562  value)
	{
		___m_ActiveCheckUpdateOperation_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ActiveCheckUpdateOperation_6))->___m_InternalOp_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ActiveCheckUpdateOperation_6))->___m_LocationName_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_ActiveUpdateOperation_7() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_ActiveUpdateOperation_7)); }
	inline AsyncOperationHandle_1_t16C2B4678E91F85C98BD21DE4C5E30BFE2821EC5  get_m_ActiveUpdateOperation_7() const { return ___m_ActiveUpdateOperation_7; }
	inline AsyncOperationHandle_1_t16C2B4678E91F85C98BD21DE4C5E30BFE2821EC5 * get_address_of_m_ActiveUpdateOperation_7() { return &___m_ActiveUpdateOperation_7; }
	inline void set_m_ActiveUpdateOperation_7(AsyncOperationHandle_1_t16C2B4678E91F85C98BD21DE4C5E30BFE2821EC5  value)
	{
		___m_ActiveUpdateOperation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ActiveUpdateOperation_7))->___m_InternalOp_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ActiveUpdateOperation_7))->___m_LocationName_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_OnHandleCompleteAction_8() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_OnHandleCompleteAction_8)); }
	inline Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * get_m_OnHandleCompleteAction_8() const { return ___m_OnHandleCompleteAction_8; }
	inline Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA ** get_address_of_m_OnHandleCompleteAction_8() { return &___m_OnHandleCompleteAction_8; }
	inline void set_m_OnHandleCompleteAction_8(Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * value)
	{
		___m_OnHandleCompleteAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHandleCompleteAction_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSceneHandleCompleteAction_9() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_OnSceneHandleCompleteAction_9)); }
	inline Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * get_m_OnSceneHandleCompleteAction_9() const { return ___m_OnSceneHandleCompleteAction_9; }
	inline Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA ** get_address_of_m_OnSceneHandleCompleteAction_9() { return &___m_OnSceneHandleCompleteAction_9; }
	inline void set_m_OnSceneHandleCompleteAction_9(Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * value)
	{
		___m_OnSceneHandleCompleteAction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSceneHandleCompleteAction_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnHandleDestroyedAction_10() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_OnHandleDestroyedAction_10)); }
	inline Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * get_m_OnHandleDestroyedAction_10() const { return ___m_OnHandleDestroyedAction_10; }
	inline Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA ** get_address_of_m_OnHandleDestroyedAction_10() { return &___m_OnHandleDestroyedAction_10; }
	inline void set_m_OnHandleDestroyedAction_10(Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * value)
	{
		___m_OnHandleDestroyedAction_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHandleDestroyedAction_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_resultToHandle_11() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_resultToHandle_11)); }
	inline Dictionary_2_t2F95F954FC957D3411922E726F65355AB595789D * get_m_resultToHandle_11() const { return ___m_resultToHandle_11; }
	inline Dictionary_2_t2F95F954FC957D3411922E726F65355AB595789D ** get_address_of_m_resultToHandle_11() { return &___m_resultToHandle_11; }
	inline void set_m_resultToHandle_11(Dictionary_2_t2F95F954FC957D3411922E726F65355AB595789D * value)
	{
		___m_resultToHandle_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_resultToHandle_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_SceneInstances_12() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_SceneInstances_12)); }
	inline HashSet_1_t12733814D81222896EFB6827B32A20B16A4B7D54 * get_m_SceneInstances_12() const { return ___m_SceneInstances_12; }
	inline HashSet_1_t12733814D81222896EFB6827B32A20B16A4B7D54 ** get_address_of_m_SceneInstances_12() { return &___m_SceneInstances_12; }
	inline void set_m_SceneInstances_12(HashSet_1_t12733814D81222896EFB6827B32A20B16A4B7D54 * value)
	{
		___m_SceneInstances_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SceneInstances_12), (void*)value);
	}

	inline static int32_t get_offset_of_hasStartedInitialization_13() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___hasStartedInitialization_13)); }
	inline bool get_hasStartedInitialization_13() const { return ___hasStartedInitialization_13; }
	inline bool* get_address_of_hasStartedInitialization_13() { return &___hasStartedInitialization_13; }
	inline void set_hasStartedInitialization_13(bool value)
	{
		___hasStartedInitialization_13 = value;
	}
};


// UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData
struct ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48  : public RuntimeObject
{
public:
	// System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_buildTarget
	String_t* ___m_buildTarget_1;
	// System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_SettingsHash
	String_t* ___m_SettingsHash_2;
	// System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_CatalogLocations
	List_1_t6A3E8E12BEA59AD97A4CCE0ABC71286067B7B157 * ___m_CatalogLocations_3;
	// System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_ProfileEvents
	bool ___m_ProfileEvents_4;
	// System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_LogResourceManagerExceptions
	bool ___m_LogResourceManagerExceptions_5;
	// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_ExtraInitializationData
	List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * ___m_ExtraInitializationData_6;
	// System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_DisableCatalogUpdateOnStart
	bool ___m_DisableCatalogUpdateOnStart_7;
	// System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_IsLocalCatalogInBundle
	bool ___m_IsLocalCatalogInBundle_8;
	// UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_CertificateHandlerType
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  ___m_CertificateHandlerType_9;
	// System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_AddressablesVersion
	String_t* ___m_AddressablesVersion_10;
	// System.Int32 UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_maxConcurrentWebRequests
	int32_t ___m_maxConcurrentWebRequests_11;

public:
	inline static int32_t get_offset_of_m_buildTarget_1() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_buildTarget_1)); }
	inline String_t* get_m_buildTarget_1() const { return ___m_buildTarget_1; }
	inline String_t** get_address_of_m_buildTarget_1() { return &___m_buildTarget_1; }
	inline void set_m_buildTarget_1(String_t* value)
	{
		___m_buildTarget_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_buildTarget_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SettingsHash_2() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_SettingsHash_2)); }
	inline String_t* get_m_SettingsHash_2() const { return ___m_SettingsHash_2; }
	inline String_t** get_address_of_m_SettingsHash_2() { return &___m_SettingsHash_2; }
	inline void set_m_SettingsHash_2(String_t* value)
	{
		___m_SettingsHash_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SettingsHash_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CatalogLocations_3() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_CatalogLocations_3)); }
	inline List_1_t6A3E8E12BEA59AD97A4CCE0ABC71286067B7B157 * get_m_CatalogLocations_3() const { return ___m_CatalogLocations_3; }
	inline List_1_t6A3E8E12BEA59AD97A4CCE0ABC71286067B7B157 ** get_address_of_m_CatalogLocations_3() { return &___m_CatalogLocations_3; }
	inline void set_m_CatalogLocations_3(List_1_t6A3E8E12BEA59AD97A4CCE0ABC71286067B7B157 * value)
	{
		___m_CatalogLocations_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CatalogLocations_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ProfileEvents_4() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_ProfileEvents_4)); }
	inline bool get_m_ProfileEvents_4() const { return ___m_ProfileEvents_4; }
	inline bool* get_address_of_m_ProfileEvents_4() { return &___m_ProfileEvents_4; }
	inline void set_m_ProfileEvents_4(bool value)
	{
		___m_ProfileEvents_4 = value;
	}

	inline static int32_t get_offset_of_m_LogResourceManagerExceptions_5() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_LogResourceManagerExceptions_5)); }
	inline bool get_m_LogResourceManagerExceptions_5() const { return ___m_LogResourceManagerExceptions_5; }
	inline bool* get_address_of_m_LogResourceManagerExceptions_5() { return &___m_LogResourceManagerExceptions_5; }
	inline void set_m_LogResourceManagerExceptions_5(bool value)
	{
		___m_LogResourceManagerExceptions_5 = value;
	}

	inline static int32_t get_offset_of_m_ExtraInitializationData_6() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_ExtraInitializationData_6)); }
	inline List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * get_m_ExtraInitializationData_6() const { return ___m_ExtraInitializationData_6; }
	inline List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A ** get_address_of_m_ExtraInitializationData_6() { return &___m_ExtraInitializationData_6; }
	inline void set_m_ExtraInitializationData_6(List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * value)
	{
		___m_ExtraInitializationData_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExtraInitializationData_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableCatalogUpdateOnStart_7() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_DisableCatalogUpdateOnStart_7)); }
	inline bool get_m_DisableCatalogUpdateOnStart_7() const { return ___m_DisableCatalogUpdateOnStart_7; }
	inline bool* get_address_of_m_DisableCatalogUpdateOnStart_7() { return &___m_DisableCatalogUpdateOnStart_7; }
	inline void set_m_DisableCatalogUpdateOnStart_7(bool value)
	{
		___m_DisableCatalogUpdateOnStart_7 = value;
	}

	inline static int32_t get_offset_of_m_IsLocalCatalogInBundle_8() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_IsLocalCatalogInBundle_8)); }
	inline bool get_m_IsLocalCatalogInBundle_8() const { return ___m_IsLocalCatalogInBundle_8; }
	inline bool* get_address_of_m_IsLocalCatalogInBundle_8() { return &___m_IsLocalCatalogInBundle_8; }
	inline void set_m_IsLocalCatalogInBundle_8(bool value)
	{
		___m_IsLocalCatalogInBundle_8 = value;
	}

	inline static int32_t get_offset_of_m_CertificateHandlerType_9() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_CertificateHandlerType_9)); }
	inline SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  get_m_CertificateHandlerType_9() const { return ___m_CertificateHandlerType_9; }
	inline SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 * get_address_of_m_CertificateHandlerType_9() { return &___m_CertificateHandlerType_9; }
	inline void set_m_CertificateHandlerType_9(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  value)
	{
		___m_CertificateHandlerType_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CertificateHandlerType_9))->___m_AssemblyName_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CertificateHandlerType_9))->___m_ClassName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CertificateHandlerType_9))->___m_CachedType_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AddressablesVersion_10() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_AddressablesVersion_10)); }
	inline String_t* get_m_AddressablesVersion_10() const { return ___m_AddressablesVersion_10; }
	inline String_t** get_address_of_m_AddressablesVersion_10() { return &___m_AddressablesVersion_10; }
	inline void set_m_AddressablesVersion_10(String_t* value)
	{
		___m_AddressablesVersion_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddressablesVersion_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_maxConcurrentWebRequests_11() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_maxConcurrentWebRequests_11)); }
	inline int32_t get_m_maxConcurrentWebRequests_11() const { return ___m_maxConcurrentWebRequests_11; }
	inline int32_t* get_address_of_m_maxConcurrentWebRequests_11() { return &___m_maxConcurrentWebRequests_11; }
	inline void set_m_maxConcurrentWebRequests_11(int32_t value)
	{
		___m_maxConcurrentWebRequests_11 = value;
	}
};


// UnityEngine.AddressableAssets.Utility.SerializationUtilities/ObjectType
struct ObjectType_t9E5ED83BD39951E53AF5C9055468CCCFB52A28F7 
{
public:
	// System.Int32 UnityEngine.AddressableAssets.Utility.SerializationUtilities/ObjectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectType_t9E5ED83BD39951E53AF5C9055468CCCFB52A28F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AssetReferenceUILabelRestriction
struct AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC  : public AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372
{
public:
	// System.String[] UnityEngine.AssetReferenceUILabelRestriction::m_AllowedLabels
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_AllowedLabels_0;
	// System.String UnityEngine.AssetReferenceUILabelRestriction::m_CachedToString
	String_t* ___m_CachedToString_1;

public:
	inline static int32_t get_offset_of_m_AllowedLabels_0() { return static_cast<int32_t>(offsetof(AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC, ___m_AllowedLabels_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_AllowedLabels_0() const { return ___m_AllowedLabels_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_AllowedLabels_0() { return &___m_AllowedLabels_0; }
	inline void set_m_AllowedLabels_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_AllowedLabels_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AllowedLabels_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedToString_1() { return static_cast<int32_t>(offsetof(AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC, ___m_CachedToString_1)); }
	inline String_t* get_m_CachedToString_1() const { return ___m_CachedToString_1; }
	inline String_t** get_address_of_m_CachedToString_1() { return &___m_CachedToString_1; }
	inline void set_m_CachedToString_1(String_t* value)
	{
		___m_CachedToString_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedToString_1), (void*)value);
	}
};


// UnityEngine.LogType
struct LogType_t6B6C6234E8B44B73937581ACFBE15DE28227849D 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogType_t6B6C6234E8B44B73937581ACFBE15DE28227849D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus
struct AsyncOperationStatus_t598BB2ACEBD2067A004500935C76AA6D845EE005 
{
public:
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsyncOperationStatus_t598BB2ACEBD2067A004500935C76AA6D845EE005, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ResourceManagement.Util.ObjectInitializationData
struct ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 
{
public:
	// System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::m_Id
	String_t* ___m_Id_0;
	// UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::m_ObjectType
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  ___m_ObjectType_1;
	// System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::m_Data
	String_t* ___m_Data_2;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42, ___m_Id_0)); }
	inline String_t* get_m_Id_0() const { return ___m_Id_0; }
	inline String_t** get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(String_t* value)
	{
		___m_Id_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ObjectType_1() { return static_cast<int32_t>(offsetof(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42, ___m_ObjectType_1)); }
	inline SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  get_m_ObjectType_1() const { return ___m_ObjectType_1; }
	inline SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 * get_address_of_m_ObjectType_1() { return &___m_ObjectType_1; }
	inline void set_m_ObjectType_1(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  value)
	{
		___m_ObjectType_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ObjectType_1))->___m_AssemblyName_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ObjectType_1))->___m_ClassName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ObjectType_1))->___m_CachedType_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Data_2() { return static_cast<int32_t>(offsetof(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42, ___m_Data_2)); }
	inline String_t* get_m_Data_2() const { return ___m_Data_2; }
	inline String_t** get_address_of_m_Data_2() { return &___m_Data_2; }
	inline void set_m_Data_2(String_t* value)
	{
		___m_Data_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Data_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ResourceManagement.Util.ObjectInitializationData
struct ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42_marshaled_pinvoke
{
	char* ___m_Id_0;
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84_marshaled_pinvoke ___m_ObjectType_1;
	char* ___m_Data_2;
};
// Native definition for COM marshalling of UnityEngine.ResourceManagement.Util.ObjectInitializationData
struct ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42_marshaled_com
{
	Il2CppChar* ___m_Id_0;
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84_marshaled_com ___m_ObjectType_1;
	Il2CppChar* ___m_Data_2;
};

// PackedPlayModeBuildLogs/RuntimeBuildLog
struct RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44 
{
public:
	// UnityEngine.LogType PackedPlayModeBuildLogs/RuntimeBuildLog::Type
	int32_t ___Type_0;
	// System.String PackedPlayModeBuildLogs/RuntimeBuildLog::Message
	String_t* ___Message_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Message_1() { return static_cast<int32_t>(offsetof(RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44, ___Message_1)); }
	inline String_t* get_Message_1() const { return ___Message_1; }
	inline String_t** get_address_of_Message_1() { return &___Message_1; }
	inline void set_Message_1(String_t* value)
	{
		___Message_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Message_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of PackedPlayModeBuildLogs/RuntimeBuildLog
struct RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44_marshaled_pinvoke
{
	int32_t ___Type_0;
	char* ___Message_1;
};
// Native definition for COM marshalling of PackedPlayModeBuildLogs/RuntimeBuildLog
struct RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44_marshaled_com
{
	int32_t ___Type_0;
	Il2CppChar* ___Message_1;
};

// System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>
struct Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9, ___list_0)); }
	inline List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * get_list_0() const { return ___list_0; }
	inline List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9, ___current_3)); }
	inline ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  get_current_3() const { return ___current_3; }
	inline ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_Id_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___m_ObjectType_1))->___m_AssemblyName_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___m_ObjectType_1))->___m_ClassName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___m_ObjectType_1))->___m_CachedType_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_Data_2), (void*)NULL);
		#endif
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>
struct AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D  : public RuntimeObject
{
public:
	// TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::<Result>k__BackingField
	bool ___U3CResultU3Ek__BackingField_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_referenceCount
	int32_t ___m_referenceCount_1;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_Status
	int32_t ___m_Status_2;
	// System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_Error
	Exception_t * ___m_Error_3;
	// UnityEngine.ResourceManagement.ResourceManager UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_RM
	ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * ___m_RM_4;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_Version
	int32_t ___m_Version_5;
	// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_DestroyedAction
	DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * ___m_DestroyedAction_6;
	// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_CompletedActionT
	DelegateList_1_t52A8167A6C97544426B924CCF66D6B38876C448E * ___m_CompletedActionT_7;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_OnDestroyAction
	Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * ___m_OnDestroyAction_8;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_dependencyCompleteAction
	Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * ___m_dependencyCompleteAction_9;
	// System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_10;
	// System.Threading.EventWaitHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_waitHandle
	EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 * ___m_waitHandle_11;
	// System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_InDeferredCallbackQueue
	bool ___m_InDeferredCallbackQueue_12;
	// DelegateList`1<System.Single> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_UpdateCallbacks
	DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5 * ___m_UpdateCallbacks_13;
	// System.Action`1<System.Single> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_UpdateCallback
	Action_1_t298B565CF49E0C72E4377AA3B30170F58D9F64FB * ___m_UpdateCallback_14;

public:
	inline static int32_t get_offset_of_U3CResultU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___U3CResultU3Ek__BackingField_0)); }
	inline bool get_U3CResultU3Ek__BackingField_0() const { return ___U3CResultU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CResultU3Ek__BackingField_0() { return &___U3CResultU3Ek__BackingField_0; }
	inline void set_U3CResultU3Ek__BackingField_0(bool value)
	{
		___U3CResultU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_referenceCount_1() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_referenceCount_1)); }
	inline int32_t get_m_referenceCount_1() const { return ___m_referenceCount_1; }
	inline int32_t* get_address_of_m_referenceCount_1() { return &___m_referenceCount_1; }
	inline void set_m_referenceCount_1(int32_t value)
	{
		___m_referenceCount_1 = value;
	}

	inline static int32_t get_offset_of_m_Status_2() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_Status_2)); }
	inline int32_t get_m_Status_2() const { return ___m_Status_2; }
	inline int32_t* get_address_of_m_Status_2() { return &___m_Status_2; }
	inline void set_m_Status_2(int32_t value)
	{
		___m_Status_2 = value;
	}

	inline static int32_t get_offset_of_m_Error_3() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_Error_3)); }
	inline Exception_t * get_m_Error_3() const { return ___m_Error_3; }
	inline Exception_t ** get_address_of_m_Error_3() { return &___m_Error_3; }
	inline void set_m_Error_3(Exception_t * value)
	{
		___m_Error_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Error_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_RM_4() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_RM_4)); }
	inline ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * get_m_RM_4() const { return ___m_RM_4; }
	inline ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 ** get_address_of_m_RM_4() { return &___m_RM_4; }
	inline void set_m_RM_4(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * value)
	{
		___m_RM_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RM_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_5() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_Version_5)); }
	inline int32_t get_m_Version_5() const { return ___m_Version_5; }
	inline int32_t* get_address_of_m_Version_5() { return &___m_Version_5; }
	inline void set_m_Version_5(int32_t value)
	{
		___m_Version_5 = value;
	}

	inline static int32_t get_offset_of_m_DestroyedAction_6() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_DestroyedAction_6)); }
	inline DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * get_m_DestroyedAction_6() const { return ___m_DestroyedAction_6; }
	inline DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF ** get_address_of_m_DestroyedAction_6() { return &___m_DestroyedAction_6; }
	inline void set_m_DestroyedAction_6(DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * value)
	{
		___m_DestroyedAction_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DestroyedAction_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_CompletedActionT_7() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_CompletedActionT_7)); }
	inline DelegateList_1_t52A8167A6C97544426B924CCF66D6B38876C448E * get_m_CompletedActionT_7() const { return ___m_CompletedActionT_7; }
	inline DelegateList_1_t52A8167A6C97544426B924CCF66D6B38876C448E ** get_address_of_m_CompletedActionT_7() { return &___m_CompletedActionT_7; }
	inline void set_m_CompletedActionT_7(DelegateList_1_t52A8167A6C97544426B924CCF66D6B38876C448E * value)
	{
		___m_CompletedActionT_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CompletedActionT_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDestroyAction_8() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_OnDestroyAction_8)); }
	inline Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * get_m_OnDestroyAction_8() const { return ___m_OnDestroyAction_8; }
	inline Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 ** get_address_of_m_OnDestroyAction_8() { return &___m_OnDestroyAction_8; }
	inline void set_m_OnDestroyAction_8(Action_1_t5BBBF57E27DCFC921E5FA8AF1D36A52DB03B8403 * value)
	{
		___m_OnDestroyAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDestroyAction_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_dependencyCompleteAction_9() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_dependencyCompleteAction_9)); }
	inline Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * get_m_dependencyCompleteAction_9() const { return ___m_dependencyCompleteAction_9; }
	inline Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA ** get_address_of_m_dependencyCompleteAction_9() { return &___m_dependencyCompleteAction_9; }
	inline void set_m_dependencyCompleteAction_9(Action_1_t019575D6E69B846A491C3246223C80D04F9D1EEA * value)
	{
		___m_dependencyCompleteAction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_dependencyCompleteAction_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___U3CIsRunningU3Ek__BackingField_10)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_10() const { return ___U3CIsRunningU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_10() { return &___U3CIsRunningU3Ek__BackingField_10; }
	inline void set_U3CIsRunningU3Ek__BackingField_10(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_m_waitHandle_11() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_waitHandle_11)); }
	inline EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 * get_m_waitHandle_11() const { return ___m_waitHandle_11; }
	inline EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 ** get_address_of_m_waitHandle_11() { return &___m_waitHandle_11; }
	inline void set_m_waitHandle_11(EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 * value)
	{
		___m_waitHandle_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_waitHandle_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_InDeferredCallbackQueue_12() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_InDeferredCallbackQueue_12)); }
	inline bool get_m_InDeferredCallbackQueue_12() const { return ___m_InDeferredCallbackQueue_12; }
	inline bool* get_address_of_m_InDeferredCallbackQueue_12() { return &___m_InDeferredCallbackQueue_12; }
	inline void set_m_InDeferredCallbackQueue_12(bool value)
	{
		___m_InDeferredCallbackQueue_12 = value;
	}

	inline static int32_t get_offset_of_m_UpdateCallbacks_13() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_UpdateCallbacks_13)); }
	inline DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5 * get_m_UpdateCallbacks_13() const { return ___m_UpdateCallbacks_13; }
	inline DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5 ** get_address_of_m_UpdateCallbacks_13() { return &___m_UpdateCallbacks_13; }
	inline void set_m_UpdateCallbacks_13(DelegateList_1_t706C5131B25748EEAC82932F52CCF1AF4B4CCBE5 * value)
	{
		___m_UpdateCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateCallbacks_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateCallback_14() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D, ___m_UpdateCallback_14)); }
	inline Action_1_t298B565CF49E0C72E4377AA3B30170F58D9F64FB * get_m_UpdateCallback_14() const { return ___m_UpdateCallback_14; }
	inline Action_1_t298B565CF49E0C72E4377AA3B30170F58D9F64FB ** get_address_of_m_UpdateCallback_14() { return &___m_UpdateCallback_14; }
	inline void set_m_UpdateCallback_14(Action_1_t298B565CF49E0C72E4377AA3B30170F58D9F64FB * value)
	{
		___m_UpdateCallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateCallback_14), (void*)value);
	}
};


// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>>
struct Action_1_tC7396721A16D97E46049382833189660B6FEA123  : public MulticastDelegate_t
{
public:

public:
};


// System.Collections.Generic.List`1/Enumerator<PackedPlayModeBuildLogs/RuntimeBuildLog>
struct Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80, ___list_0)); }
	inline List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * get_list_0() const { return ___list_0; }
	inline List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80, ___current_3)); }
	inline RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  get_current_3() const { return ___current_3; }
	inline RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___Message_1), (void*)NULL);
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation
struct InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D  : public AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData> UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::m_RtdOp
	AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  ___m_RtdOp_15;
	// UnityEngine.AddressableAssets.AddressablesImpl UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::m_Addressables
	AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * ___m_Addressables_16;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::m_DepOp
	AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED  ___m_DepOp_17;

public:
	inline static int32_t get_offset_of_m_RtdOp_15() { return static_cast<int32_t>(offsetof(InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D, ___m_RtdOp_15)); }
	inline AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  get_m_RtdOp_15() const { return ___m_RtdOp_15; }
	inline AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB * get_address_of_m_RtdOp_15() { return &___m_RtdOp_15; }
	inline void set_m_RtdOp_15(AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  value)
	{
		___m_RtdOp_15 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RtdOp_15))->___m_InternalOp_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RtdOp_15))->___m_LocationName_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Addressables_16() { return static_cast<int32_t>(offsetof(InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D, ___m_Addressables_16)); }
	inline AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * get_m_Addressables_16() const { return ___m_Addressables_16; }
	inline AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F ** get_address_of_m_Addressables_16() { return &___m_Addressables_16; }
	inline void set_m_Addressables_16(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * value)
	{
		___m_Addressables_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Addressables_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_DepOp_17() { return static_cast<int32_t>(offsetof(InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D, ___m_DepOp_17)); }
	inline AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED  get_m_DepOp_17() const { return ___m_DepOp_17; }
	inline AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED * get_address_of_m_DepOp_17() { return &___m_DepOp_17; }
	inline void set_m_DepOp_17(AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED  value)
	{
		___m_DepOp_17 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DepOp_17))->___m_InternalOp_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DepOp_17))->___m_LocationName_2), (void*)NULL);
		#endif
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * JsonUtility_FromJson_TisRuntimeObject_m38BFFDE45556DDE756B8EDCA5E0775A8320E1296_gshared (String_t* ___json0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80  List_1_GetEnumerator_mAC57F8B0BFC67D139F9FEF652DA4CCEB9A60E968_gshared (List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<PackedPlayModeBuildLogs/RuntimeBuildLog>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  Enumerator_get_Current_m801F71E2980D37299592A623FB92A141F866225A_gshared_inline (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<PackedPlayModeBuildLogs/RuntimeBuildLog>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mA9B5E64E1849F2AC2065781986A55CCD9DC9A50F_gshared (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<PackedPlayModeBuildLogs/RuntimeBuildLog>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m2329B6D018D3E680CF80034B110AAD82890A3A57_gshared (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 * __this, const RuntimeMethod* method);
// !0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>::get_Result()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AsyncOperationHandle_1_get_Result_m985ECC2026619BAD74B746928322E2CE10ED76AC_gshared (AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>::Complete(!0,System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncOperationBase_1_Complete_m4DA85FC017FAB08C949D44BC24D6288684163447_gshared (AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D * __this, bool ___result0, bool ___success1, String_t* ___errorMsg2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7304DC41D77EB6CBB389ACB5225B632038E33694_gshared (List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9  List_1_GetEnumerator_m4C6EBC1F113010F19C7D7803D372E64478CF02E8_gshared (List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  Enumerator_get_Current_m5481EE2DB20534CFF47F7BF2ABDA5E44D1692443_gshared_inline (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m391465EE5D1E99359D7FE152BDCA406567D461F8_gshared (List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * __this, AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m0BAB547D027E681F5AC5FBC80BACA58C958138EC_gshared (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7_gshared (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mCC0DF94996ADFDA0FEA83B0C1515A0B200C782F3_gshared (Action_1_t5FA24278A896AF4BD19B76F343A99FF5B92377B6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<!0>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncOperationHandle_1_add_Completed_m488120E83467FD28902227B07715F5C9C196768D_gshared (AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457 * __this, Action_1_t5FA24278A896AF4BD19B76F343A99FF5B92377B6 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncOperationBase_1__ctor_mCFF2B72AFE0F6AA1A554A1EC61672D880386CCAE_gshared (AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D * __this, const RuntimeMethod* method);
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>::get_Status()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AsyncOperationHandle_1_get_Status_m6265AEFF3018A04E1BB3C9F5C20357DB6C6E482C_gshared (AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457 * __this, const RuntimeMethod* method);
// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>::get_DebugName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AsyncOperationHandle_1_get_DebugName_m19C298AC80FE2528E176B1D01095502FA4D5E884_gshared (AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release<System.Object>(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddressablesImpl_Release_TisRuntimeObject_m01AA831E29E78BFF6D2D5AB9A79643C0CB81F535_gshared (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * __this, AsyncOperationHandle_1_t190F093DB12FE8A4AA7C3AB0C53005A615046457  ___handle0, const RuntimeMethod* method);

// System.Void UnityEngine.AssetReferenceUIRestriction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetReferenceUIRestriction__ctor_m66132256B7F090205A884805AF37E18ACBA905B9 (AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372 * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m05C12F58ADC2D807613A9301DF438CB3CD09B75A (StringBuilder_t * __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0 (Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * __this, const RuntimeMethod* method);
// UnityEngine.ResourceManagement.ResourceManager UnityEngine.AddressableAssets.AddressablesImpl::get_ResourceManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForCallbacks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResourceManager_RegisterForCallbacks_mD497DF3FD15D025444B1AB3F33542977EA1DEC11 (ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * __this, const RuntimeMethod* method);
// System.Boolean System.IO.File::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB (String_t* ___path0, const RuntimeMethod* method);
// System.String System.IO.File::ReadAllText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* File_ReadAllText_m404A1BE4C87AC3C7B9C0B07469CDC44DE52817FF (String_t* ___path0, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<PackedPlayModeBuildLogs>(System.String)
inline PackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23 * JsonUtility_FromJson_TisPackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23_m3679717B41751ED5E1FCE12D32AD9838229179FD (String_t* ___json0, const RuntimeMethod* method)
{
	return ((  PackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23 * (*) (String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisRuntimeObject_m38BFFDE45556DDE756B8EDCA5E0775A8320E1296_gshared)(___json0, method);
}
// System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog> PackedPlayModeBuildLogs::get_RuntimeBuildLogs()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * PackedPlayModeBuildLogs_get_RuntimeBuildLogs_mFC37B4B555D8F01D81B994F1BDCFA2EBA5029725_inline (PackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog>::GetEnumerator()
inline Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80  List_1_GetEnumerator_mAC57F8B0BFC67D139F9FEF652DA4CCEB9A60E968 (List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80  (*) (List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 *, const RuntimeMethod*))List_1_GetEnumerator_mAC57F8B0BFC67D139F9FEF652DA4CCEB9A60E968_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<PackedPlayModeBuildLogs/RuntimeBuildLog>::get_Current()
inline RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  Enumerator_get_Current_m801F71E2980D37299592A623FB92A141F866225A_inline (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  (*) (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 *, const RuntimeMethod*))Enumerator_get_Current_m801F71E2980D37299592A623FB92A141F866225A_gshared_inline)(__this, method);
}
// System.Void UnityEngine.AddressableAssets.Addressables::LogWarning(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Addressables_LogWarning_m04F34DFAEED10EC01D34840BACCA3EC826F6296B (String_t* ___msg0, const RuntimeMethod* method);
// System.Void UnityEngine.AddressableAssets.Addressables::LogError(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Addressables_LogError_mC09A67786B75C90DC3260646B3B67FB4A79AE712 (String_t* ___msg0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<PackedPlayModeBuildLogs/RuntimeBuildLog>::MoveNext()
inline bool Enumerator_MoveNext_mA9B5E64E1849F2AC2065781986A55CCD9DC9A50F (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 *, const RuntimeMethod*))Enumerator_MoveNext_mA9B5E64E1849F2AC2065781986A55CCD9DC9A50F_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PackedPlayModeBuildLogs/RuntimeBuildLog>::Dispose()
inline void Enumerator_Dispose_m2329B6D018D3E680CF80034B110AAD82890A3A57 (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 *, const RuntimeMethod*))Enumerator_Dispose_m2329B6D018D3E680CF80034B110AAD82890A3A57_gshared)(__this, method);
}
// !0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>::get_Result()
inline ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * AsyncOperationHandle_1_get_Result_m4AD1404D8FBA522A55420CEE072E5CBF2FEB58F9 (AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB * __this, const RuntimeMethod* method)
{
	return ((  ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * (*) (AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB *, const RuntimeMethod*))AsyncOperationHandle_1_get_Result_m985ECC2026619BAD74B746928322E2CE10ED76AC_gshared)(__this, method);
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>::Complete(!0,System.Boolean,System.String)
inline void AsyncOperationBase_1_Complete_m4DA85FC017FAB08C949D44BC24D6288684163447 (AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D * __this, bool ___result0, bool ___success1, String_t* ___errorMsg2, const RuntimeMethod* method)
{
	((  void (*) (AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D *, bool, bool, String_t*, const RuntimeMethod*))AsyncOperationBase_1_Complete_m4DA85FC017FAB08C949D44BC24D6288684163447_gshared)(__this, ___result0, ___success1, ___errorMsg2, method);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlayerPrefs_GetString_m3031AD2D5DEAB97677A9EF629618541437F079F1 (String_t* ___key0, const RuntimeMethod* method);
// System.String UnityEngine.AddressableAssets.AddressablesImpl::ResolveInternalId(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AddressablesImpl_ResolveInternalId_mA46547D161E2499742CD72810D596DFEA1ED4D77 (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * __this, String_t* ___id0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::LogRuntimeWarnings(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InitalizationObjectsOperation_LogRuntimeWarnings_m4FA0B605859EFF68C1730534149964CA532B47F4 (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, String_t* ___pathToBuildLogs0, const RuntimeMethod* method);
// System.Void System.IO.File::Delete(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_Delete_mBE814E569EAB07FAD140C6DCDB957F1CB8C85DE2 (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>::.ctor()
inline void List_1__ctor_m7304DC41D77EB6CBB389ACB5225B632038E33694 (List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 *, const RuntimeMethod*))List_1__ctor_m7304DC41D77EB6CBB389ACB5225B632038E33694_gshared)(__this, method);
}
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_InitializationObjects()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * ResourceManagerRuntimeData_get_InitializationObjects_m66450D298C2EA3FC6937F1CDCA3FD45D1EC6419C_inline (ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::GetEnumerator()
inline Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9  List_1_GetEnumerator_m4C6EBC1F113010F19C7D7803D372E64478CF02E8 (List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9  (*) (List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A *, const RuntimeMethod*))List_1_GetEnumerator_m4C6EBC1F113010F19C7D7803D372E64478CF02E8_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::get_Current()
inline ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  Enumerator_get_Current_m5481EE2DB20534CFF47F7BF2ABDA5E44D1692443_inline (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 * __this, const RuntimeMethod* method)
{
	return ((  ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  (*) (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 *, const RuntimeMethod*))Enumerator_get_Current_m5481EE2DB20534CFF47F7BF2ABDA5E44D1692443_gshared_inline)(__this, method);
}
// UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_ObjectType()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_inline (ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 * __this, const RuntimeMethod* method);
// System.Type UnityEngine.ResourceManagement.Util.SerializedType::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * SerializedType_get_Value_mA75D04829016315F5CB5EBD10CD4A44C0754012C (SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 * __this, const RuntimeMethod* method);
// System.Boolean System.Type::op_Equality(System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Type_op_Equality_m7040622C9E1037EFC73E1F0EDB1DD241282BE3D8 (Type_t * ___left0, Type_t * ___right1, const RuntimeMethod* method);
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.Util.ObjectInitializationData::GetAsyncInitHandle(UnityEngine.ResourceManagement.ResourceManager,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  ObjectInitializationData_GetAsyncInitHandle_m0186D6321B14C9AC25E3F8196220ABBE96A965F0 (ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 * __this, ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * ___rm0, String_t* ___idOverride1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>::Add(!0)
inline void List_1_Add_m391465EE5D1E99359D7FE152BDCA406567D461F8 (List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * __this, AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 *, AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185 , const RuntimeMethod*))List_1_Add_m391465EE5D1E99359D7FE152BDCA406567D461F8_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.AddressableAssets.Addressables::LogErrorFormat(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Addressables_LogErrorFormat_m4032E8F90EA251E3D3125E891EAC9034084D7158 (String_t* ___format0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::MoveNext()
inline bool Enumerator_MoveNext_m0BAB547D027E681F5AC5FBC80BACA58C958138EC (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 *, const RuntimeMethod*))Enumerator_MoveNext_m0BAB547D027E681F5AC5FBC80BACA58C958138EC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::Dispose()
inline void Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7 (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 *, const RuntimeMethod*))Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7_gshared)(__this, method);
}
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGenericGroupOperation(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED  ResourceManager_CreateGenericGroupOperation_mB729E00A7923C66BD7F66AF04C49785E3348E947 (ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * __this, List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * ___operations0, bool ___releasedCachedOpOnComplete1, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m192B30F1BE49B025A7E9942E52258DEF2B7DBDA1 (Action_1_tC7396721A16D97E46049382833189660B6FEA123 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC7396721A16D97E46049382833189660B6FEA123 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mCC0DF94996ADFDA0FEA83B0C1515A0B200C782F3_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<!0>>)
inline void AsyncOperationHandle_1_add_Completed_mD28383C96433437034C7700C534C8A5374DEE6D8 (AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED * __this, Action_1_tC7396721A16D97E46049382833189660B6FEA123 * ___value0, const RuntimeMethod* method)
{
	((  void (*) (AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *, Action_1_tC7396721A16D97E46049382833189660B6FEA123 *, const RuntimeMethod*))AsyncOperationHandle_1_add_Completed_m488120E83467FD28902227B07715F5C9C196768D_gshared)(__this, ___value0, method);
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>::.ctor()
inline void AsyncOperationBase_1__ctor_mCFF2B72AFE0F6AA1A554A1EC61672D880386CCAE (AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D * __this, const RuntimeMethod* method)
{
	((  void (*) (AsyncOperationBase_1_t4DFB292934D68823D4A0EBDCF9E086B4B3330F8D *, const RuntimeMethod*))AsyncOperationBase_1__ctor_mCFF2B72AFE0F6AA1A554A1EC61672D880386CCAE_gshared)(__this, method);
}
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>::get_Status()
inline int32_t AsyncOperationHandle_1_get_Status_mB0A602F07AAA2FD4462731CD0F2E92ED9130393F (AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *, const RuntimeMethod*))AsyncOperationHandle_1_get_Status_m6265AEFF3018A04E1BB3C9F5C20357DB6C6E482C_gshared)(__this, method);
}
// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>::get_DebugName()
inline String_t* AsyncOperationHandle_1_get_DebugName_mB9D572DD987A5AFC28F4E6398305FAA51BE680AC (AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *, const RuntimeMethod*))AsyncOperationHandle_1_get_DebugName_m19C298AC80FE2528E176B1D01095502FA4D5E884_gshared)(__this, method);
}
// !0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>::get_Result()
inline RuntimeObject* AsyncOperationHandle_1_get_Result_m50166C1809868992DA99771109671F872426DE6F (AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *, const RuntimeMethod*))AsyncOperationHandle_1_get_Result_m985ECC2026619BAD74B746928322E2CE10ED76AC_gshared)(__this, method);
}
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99 (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, RuntimeObject * ___arg23, const RuntimeMethod* method);
// System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
inline void AddressablesImpl_Release_TisIList_1_tA839EBE831AA697F04E70F745B9C7A5C4E3BAB04_m20D71A2F51CF9A928F32F0DA21B864A948FD37E5 (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * __this, AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED  ___handle0, const RuntimeMethod* method)
{
	((  void (*) (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F *, AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED , const RuntimeMethod*))AddressablesImpl_Release_TisRuntimeObject_m01AA831E29E78BFF6D2D5AB9A79643C0CB81F535_gshared)(__this, ___handle0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AssetReferenceUILabelRestriction::.ctor(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetReferenceUILabelRestriction__ctor_m5C11CFCCA34855EF6B79A42D1DB94B6465C5EF10 (AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC * __this, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___allowedLabels0, const RuntimeMethod* method)
{
	{
		// public AssetReferenceUILabelRestriction(params string[] allowedLabels)
		AssetReferenceUIRestriction__ctor_m66132256B7F090205A884805AF37E18ACBA905B9(__this, /*hidden argument*/NULL);
		// m_AllowedLabels = allowedLabels;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = ___allowedLabels0;
		__this->set_m_AllowedLabels_0(L_0);
		// }
		return;
	}
}
// System.Boolean UnityEngine.AssetReferenceUILabelRestriction::ValidateAsset(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AssetReferenceUILabelRestriction_ValidateAsset_m0F0986CED44CE89DC1A6030BC5DA515440329E6E (AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC * __this, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method)
{
	{
		// return true;
		return (bool)1;
	}
}
// System.Boolean UnityEngine.AssetReferenceUILabelRestriction::ValidateAsset(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AssetReferenceUILabelRestriction_ValidateAsset_m7FEA8893F130FF8EA21721DCDFD2B754D9638751 (AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC * __this, String_t* ___path0, const RuntimeMethod* method)
{
	{
		// return true;
		return (bool)1;
	}
}
// System.String UnityEngine.AssetReferenceUILabelRestriction::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AssetReferenceUILabelRestriction_ToString_m610DD1BF67CD7978120DE81D61CBC05EFAF6FA68 (AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetReferenceUILabelRestriction_ToString_m610DD1BF67CD7978120DE81D61CBC05EFAF6FA68_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	bool V_1 = false;
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	{
		// if (m_CachedToString == null)
		String_t* L_0 = __this->get_m_CachedToString_1();
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		// StringBuilder sb = new StringBuilder();
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		// bool first = true;
		V_1 = (bool)1;
		// foreach (var t in m_AllowedLabels)
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = __this->get_m_AllowedLabels_0();
		V_2 = L_2;
		V_3 = 0;
		goto IL_003b;
	}

IL_001b:
	{
		// foreach (var t in m_AllowedLabels)
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		String_t* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_4 = L_6;
		// if (!first)
		bool L_7 = V_1;
		if (L_7)
		{
			goto IL_002c;
		}
	}
	{
		// sb.Append(',');
		StringBuilder_t * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_Append_m05C12F58ADC2D807613A9301DF438CB3CD09B75A(L_8, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_002c:
	{
		// first = false;
		V_1 = (bool)0;
		// sb.Append(t);
		StringBuilder_t * L_9 = V_0;
		String_t* L_10 = V_4;
		NullCheck(L_9);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_003b:
	{
		// foreach (var t in m_AllowedLabels)
		int32_t L_12 = V_3;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		// m_CachedToString = sb.ToString();
		StringBuilder_t * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		__this->set_m_CachedToString_1(L_15);
	}

IL_004d:
	{
		// return m_CachedToString;
		String_t* L_16 = __this->get_m_CachedToString_1();
		return L_16;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.AssetReferenceUIRestriction::ValidateAsset(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AssetReferenceUIRestriction_ValidateAsset_mD920DB6EAA0CB521F83F4BCDDBA24817BD983624 (AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372 * __this, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method)
{
	{
		// return true;
		return (bool)1;
	}
}
// System.Boolean UnityEngine.AssetReferenceUIRestriction::ValidateAsset(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AssetReferenceUIRestriction_ValidateAsset_m5703AC82584DFFA2651A6BFE3E1D1456FE7799C4 (AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	{
		// return true;
		return (bool)1;
	}
}
// System.Void UnityEngine.AssetReferenceUIRestriction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetReferenceUIRestriction__ctor_m66132256B7F090205A884805AF37E18ACBA905B9 (AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>,UnityEngine.AddressableAssets.AddressablesImpl)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitalizationObjectsOperation_Init_m52566D080315AD808CFDFA8DBB44C37451550234 (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  ___rtdOp0, AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * ___addressables1, const RuntimeMethod* method)
{
	{
		// m_RtdOp = rtdOp;
		AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  L_0 = ___rtdOp0;
		__this->set_m_RtdOp_15(L_0);
		// m_Addressables = addressables;
		AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_1 = ___addressables1;
		__this->set_m_Addressables_16(L_1);
		// m_Addressables.ResourceManager.RegisterForCallbacks();
		AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_2 = __this->get_m_Addressables_16();
		NullCheck(L_2);
		ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * L_3 = AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		ResourceManager_RegisterForCallbacks_mD497DF3FD15D025444B1AB3F33542977EA1DEC11(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::get_DebugName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InitalizationObjectsOperation_get_DebugName_m0BB1AD62757F2E6F1784ECEF82D0A3B4031EDBD5 (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitalizationObjectsOperation_get_DebugName_m0BB1AD62757F2E6F1784ECEF82D0A3B4031EDBD5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return "InitializationObjectsOperation"; }
		return _stringLiteral4A5A52C51940D8628AA7982A664B485789A9EF88;
	}
}
// System.Boolean UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::LogRuntimeWarnings(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InitalizationObjectsOperation_LogRuntimeWarnings_m4FA0B605859EFF68C1730534149964CA532B47F4 (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, String_t* ___pathToBuildLogs0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitalizationObjectsOperation_LogRuntimeWarnings_m4FA0B605859EFF68C1730534149964CA532B47F4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80  V_1;
	memset((&V_1), 0, sizeof(V_1));
	RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (!File.Exists(pathToBuildLogs))
		String_t* L_0 = ___pathToBuildLogs0;
		bool L_1 = File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_000a:
	{
		// PackedPlayModeBuildLogs runtimeBuildLogs = JsonUtility.FromJson<PackedPlayModeBuildLogs>(File.ReadAllText(pathToBuildLogs));
		String_t* L_2 = ___pathToBuildLogs0;
		String_t* L_3 = File_ReadAllText_m404A1BE4C87AC3C7B9C0B07469CDC44DE52817FF(L_2, /*hidden argument*/NULL);
		PackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23 * L_4 = JsonUtility_FromJson_TisPackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23_m3679717B41751ED5E1FCE12D32AD9838229179FD(L_3, /*hidden argument*/JsonUtility_FromJson_TisPackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23_m3679717B41751ED5E1FCE12D32AD9838229179FD_RuntimeMethod_var);
		// bool messageLogged = false;
		V_0 = (bool)0;
		// foreach (var log in runtimeBuildLogs.RuntimeBuildLogs)
		NullCheck(L_4);
		List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * L_5 = PackedPlayModeBuildLogs_get_RuntimeBuildLogs_mFC37B4B555D8F01D81B994F1BDCFA2EBA5029725_inline(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80  L_6 = List_1_GetEnumerator_mAC57F8B0BFC67D139F9FEF652DA4CCEB9A60E968(L_5, /*hidden argument*/List_1_GetEnumerator_mAC57F8B0BFC67D139F9FEF652DA4CCEB9A60E968_RuntimeMethod_var);
		V_1 = L_6;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0065;
		}

IL_0024:
		{
			// foreach (var log in runtimeBuildLogs.RuntimeBuildLogs)
			RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  L_7 = Enumerator_get_Current_m801F71E2980D37299592A623FB92A141F866225A_inline((Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m801F71E2980D37299592A623FB92A141F866225A_RuntimeMethod_var);
			V_2 = L_7;
			// messageLogged = true;
			V_0 = (bool)1;
			// switch (log.Type)
			RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  L_8 = V_2;
			int32_t L_9 = L_8.get_Type_0();
			V_3 = L_9;
			int32_t L_10 = V_3;
			switch (L_10)
			{
				case 0:
				{
					goto IL_005a;
				}
				case 1:
				{
					goto IL_0065;
				}
				case 2:
				{
					goto IL_004d;
				}
				case 3:
				{
					goto IL_0065;
				}
			}
		}

IL_004b:
		{
			goto IL_0065;
		}

IL_004d:
		{
			// Addressables.LogWarning(log.Message);
			RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  L_11 = V_2;
			String_t* L_12 = L_11.get_Message_1();
			IL2CPP_RUNTIME_CLASS_INIT(Addressables_t8AD39E84886280F198451740C05CA7E8AF2C977D_il2cpp_TypeInfo_var);
			Addressables_LogWarning_m04F34DFAEED10EC01D34840BACCA3EC826F6296B(L_12, /*hidden argument*/NULL);
			// break;
			goto IL_0065;
		}

IL_005a:
		{
			// Addressables.LogError(log.Message);
			RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  L_13 = V_2;
			String_t* L_14 = L_13.get_Message_1();
			IL2CPP_RUNTIME_CLASS_INIT(Addressables_t8AD39E84886280F198451740C05CA7E8AF2C977D_il2cpp_TypeInfo_var);
			Addressables_LogError_mC09A67786B75C90DC3260646B3B67FB4A79AE712(L_14, /*hidden argument*/NULL);
		}

IL_0065:
		{
			// foreach (var log in runtimeBuildLogs.RuntimeBuildLogs)
			bool L_15 = Enumerator_MoveNext_mA9B5E64E1849F2AC2065781986A55CCD9DC9A50F((Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_mA9B5E64E1849F2AC2065781986A55CCD9DC9A50F_RuntimeMethod_var);
			if (L_15)
			{
				goto IL_0024;
			}
		}

IL_006e:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_0070);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0070;
	}

FINALLY_0070:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2329B6D018D3E680CF80034B110AAD82890A3A57((Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m2329B6D018D3E680CF80034B110AAD82890A3A57_RuntimeMethod_var);
		IL2CPP_END_FINALLY(112)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(112)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
	}

IL_007e:
	{
		// return messageLogged;
		bool L_16 = V_0;
		return L_16;
	}
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::Execute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitalizationObjectsOperation_Execute_m9936831C2834EB8F0549D62194578368C376F85F (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitalizationObjectsOperation_Execute_m9936831C2834EB8F0549D62194578368C376F85F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * V_0 = NULL;
	String_t* V_1 = NULL;
	List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * V_2 = NULL;
	Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9  V_3;
	memset((&V_3), 0, sizeof(V_3));
	ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  V_4;
	memset((&V_4), 0, sizeof(V_4));
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  V_5;
	memset((&V_5), 0, sizeof(V_5));
	AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Exception_t * V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var rtd = m_RtdOp.Result;
		AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB * L_0 = __this->get_address_of_m_RtdOp_15();
		ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * L_1 = AsyncOperationHandle_1_get_Result_m4AD1404D8FBA522A55420CEE072E5CBF2FEB58F9((AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB *)L_0, /*hidden argument*/AsyncOperationHandle_1_get_Result_m4AD1404D8FBA522A55420CEE072E5CBF2FEB58F9_RuntimeMethod_var);
		V_0 = L_1;
		// if (rtd == null)
		ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		// Addressables.LogError("RuntimeData is null.  Please ensure you have built the correct Player Content.");
		IL2CPP_RUNTIME_CLASS_INIT(Addressables_t8AD39E84886280F198451740C05CA7E8AF2C977D_il2cpp_TypeInfo_var);
		Addressables_LogError_mC09A67786B75C90DC3260646B3B67FB4A79AE712(_stringLiteralECEC38FACF03D69014C82B8F5C23E6D265A14DCF, /*hidden argument*/NULL);
		// Complete(true, true, "");
		AsyncOperationBase_1_Complete_m4DA85FC017FAB08C949D44BC24D6288684163447(__this, (bool)1, (bool)1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/AsyncOperationBase_1_Complete_m4DA85FC017FAB08C949D44BC24D6288684163447_RuntimeMethod_var);
		// return;
		return;
	}

IL_0027:
	{
		// string buildLogsPath = m_Addressables.ResolveInternalId(PlayerPrefs.GetString(Addressables.kAddressablesRuntimeBuildLogPath));
		AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_3 = __this->get_m_Addressables_16();
		String_t* L_4 = PlayerPrefs_GetString_m3031AD2D5DEAB97677A9EF629618541437F079F1(_stringLiteralA92DB0F71F769FFBF78C8E2664FA6CBB7A5E3387, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_5 = AddressablesImpl_ResolveInternalId_mA46547D161E2499742CD72810D596DFEA1ED4D77(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		// if (LogRuntimeWarnings(buildLogsPath))
		String_t* L_6 = V_1;
		bool L_7 = InitalizationObjectsOperation_LogRuntimeWarnings_m4FA0B605859EFF68C1730534149964CA532B47F4(__this, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004c;
		}
	}
	{
		// File.Delete(buildLogsPath);
		String_t* L_8 = V_1;
		File_Delete_mBE814E569EAB07FAD140C6DCDB957F1CB8C85DE2(L_8, /*hidden argument*/NULL);
	}

IL_004c:
	{
		// List<AsyncOperationHandle> initOperations = new List<AsyncOperationHandle>();
		List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * L_9 = (List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 *)il2cpp_codegen_object_new(List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778_il2cpp_TypeInfo_var);
		List_1__ctor_m7304DC41D77EB6CBB389ACB5225B632038E33694(L_9, /*hidden argument*/List_1__ctor_m7304DC41D77EB6CBB389ACB5225B632038E33694_RuntimeMethod_var);
		V_2 = L_9;
		// foreach (var i in rtd.InitializationObjects)
		ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * L_10 = V_0;
		NullCheck(L_10);
		List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * L_11 = ResourceManagerRuntimeData_get_InitializationObjects_m66450D298C2EA3FC6937F1CDCA3FD45D1EC6419C_inline(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9  L_12 = List_1_GetEnumerator_m4C6EBC1F113010F19C7D7803D372E64478CF02E8(L_11, /*hidden argument*/List_1_GetEnumerator_m4C6EBC1F113010F19C7D7803D372E64478CF02E8_RuntimeMethod_var);
		V_3 = L_12;
	}

IL_005e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c9;
		}

IL_0060:
		{
			// foreach (var i in rtd.InitializationObjects)
			ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  L_13 = Enumerator_get_Current_m5481EE2DB20534CFF47F7BF2ABDA5E44D1692443_inline((Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m5481EE2DB20534CFF47F7BF2ABDA5E44D1692443_RuntimeMethod_var);
			V_4 = L_13;
			// if (i.ObjectType.Value == null)
			SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  L_14 = ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_inline((ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 *)(&V_4), /*hidden argument*/NULL);
			V_5 = L_14;
			Type_t * L_15 = SerializedType_get_Value_mA75D04829016315F5CB5EBD10CD4A44C0754012C((SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 *)(&V_5), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			bool L_16 = Type_op_Equality_m7040622C9E1037EFC73E1F0EDB1DD241282BE3D8(L_15, (Type_t *)NULL, /*hidden argument*/NULL);
			if (L_16)
			{
				goto IL_00c9;
			}
		}

IL_0081:
		{
		}

IL_0082:
		try
		{ // begin try (depth: 2)
			// var o = i.GetAsyncInitHandle(m_Addressables.ResourceManager);
			AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_17 = __this->get_m_Addressables_16();
			NullCheck(L_17);
			ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * L_18 = AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF(L_17, /*hidden argument*/NULL);
			AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  L_19 = ObjectInitializationData_GetAsyncInitHandle_m0186D6321B14C9AC25E3F8196220ABBE96A965F0((ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 *)(&V_4), L_18, (String_t*)NULL, /*hidden argument*/NULL);
			V_6 = L_19;
			// initOperations.Add(o);
			List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * L_20 = V_2;
			AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  L_21 = V_6;
			NullCheck(L_20);
			List_1_Add_m391465EE5D1E99359D7FE152BDCA406567D461F8(L_20, L_21, /*hidden argument*/List_1_Add_m391465EE5D1E99359D7FE152BDCA406567D461F8_RuntimeMethod_var);
			// }
			goto IL_00c9;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
				goto CATCH_00a1;
			throw e;
		}

CATCH_00a1:
		{ // begin catch(System.Exception)
			// catch (Exception ex)
			V_7 = ((Exception_t *)__exception_local);
			// Addressables.LogErrorFormat("Exception thrown during initialization of object {0}: {1}", i,
			//     ex.ToString());
			ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_22 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
			ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_23 = L_22;
			ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  L_24 = V_4;
			ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  L_25 = L_24;
			RuntimeObject * L_26 = Box(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42_il2cpp_TypeInfo_var, &L_25);
			NullCheck(L_23);
			ArrayElementTypeCheck (L_23, L_26);
			(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_26);
			ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_27 = L_23;
			Exception_t * L_28 = V_7;
			NullCheck(L_28);
			String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_28);
			NullCheck(L_27);
			ArrayElementTypeCheck (L_27, L_29);
			(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_29);
			IL2CPP_RUNTIME_CLASS_INIT(Addressables_t8AD39E84886280F198451740C05CA7E8AF2C977D_il2cpp_TypeInfo_var);
			Addressables_LogErrorFormat_m4032E8F90EA251E3D3125E891EAC9034084D7158(_stringLiteral3C1B63DEBF79CE695956E89613C7F2BEAFC4DEBC, L_27, /*hidden argument*/NULL);
			// }
			goto IL_00c9;
		} // end catch (depth: 2)

IL_00c9:
		{
			// foreach (var i in rtd.InitializationObjects)
			bool L_30 = Enumerator_MoveNext_m0BAB547D027E681F5AC5FBC80BACA58C958138EC((Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m0BAB547D027E681F5AC5FBC80BACA58C958138EC_RuntimeMethod_var);
			if (L_30)
			{
				goto IL_0060;
			}
		}

IL_00d2:
		{
			IL2CPP_LEAVE(0xE2, FINALLY_00d4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00d4;
	}

FINALLY_00d4:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7((Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7_RuntimeMethod_var);
		IL2CPP_END_FINALLY(212)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(212)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xE2, IL_00e2)
	}

IL_00e2:
	{
		// m_DepOp = m_Addressables.ResourceManager.CreateGenericGroupOperation(initOperations, true);
		AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_31 = __this->get_m_Addressables_16();
		NullCheck(L_31);
		ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * L_32 = AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF(L_31, /*hidden argument*/NULL);
		List_1_t8B3E12469FEEE45F172B5CE4C5F4B264C6CFF778 * L_33 = V_2;
		NullCheck(L_32);
		AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED  L_34 = ResourceManager_CreateGenericGroupOperation_mB729E00A7923C66BD7F66AF04C49785E3348E947(L_32, L_33, (bool)1, /*hidden argument*/NULL);
		__this->set_m_DepOp_17(L_34);
		// m_DepOp.Completed += (obj) =>
		// {
		//     bool success = obj.Status == AsyncOperationStatus.Succeeded;
		//     Complete(true, success, success ? "" : $"{obj.DebugName}, status={obj.Status}, result={obj.Result} failed initialization.");
		//     m_Addressables.Release(m_DepOp);
		// };
		AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED * L_35 = __this->get_address_of_m_DepOp_17();
		Action_1_tC7396721A16D97E46049382833189660B6FEA123 * L_36 = (Action_1_tC7396721A16D97E46049382833189660B6FEA123 *)il2cpp_codegen_object_new(Action_1_tC7396721A16D97E46049382833189660B6FEA123_il2cpp_TypeInfo_var);
		Action_1__ctor_m192B30F1BE49B025A7E9942E52258DEF2B7DBDA1(L_36, __this, (intptr_t)((intptr_t)InitalizationObjectsOperation_U3CExecuteU3Eb__7_0_mDDBD37CA57E5F64767F1A289DA18292ACF0F939C_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m192B30F1BE49B025A7E9942E52258DEF2B7DBDA1_RuntimeMethod_var);
		AsyncOperationHandle_1_add_Completed_mD28383C96433437034C7700C534C8A5374DEE6D8((AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *)L_35, L_36, /*hidden argument*/AsyncOperationHandle_1_add_Completed_mD28383C96433437034C7700C534C8A5374DEE6D8_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitalizationObjectsOperation__ctor_mEBF54CDAB8D99111796BAA542124CB6C91452123 (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitalizationObjectsOperation__ctor_mEBF54CDAB8D99111796BAA542124CB6C91452123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncOperationBase_1__ctor_mCFF2B72AFE0F6AA1A554A1EC61672D880386CCAE(__this, /*hidden argument*/AsyncOperationBase_1__ctor_mCFF2B72AFE0F6AA1A554A1EC61672D880386CCAE_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::<Execute>b__7_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitalizationObjectsOperation_U3CExecuteU3Eb__7_0_mDDBD37CA57E5F64767F1A289DA18292ACF0F939C (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED  ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitalizationObjectsOperation_U3CExecuteU3Eb__7_0_mDDBD37CA57E5F64767F1A289DA18292ACF0F939C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool G_B2_0 = false;
	int32_t G_B2_1 = 0;
	InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * G_B2_2 = NULL;
	bool G_B1_0 = false;
	int32_t G_B1_1 = 0;
	InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	bool G_B3_1 = false;
	int32_t G_B3_2 = 0;
	InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * G_B3_3 = NULL;
	{
		// bool success = obj.Status == AsyncOperationStatus.Succeeded;
		int32_t L_0 = AsyncOperationHandle_1_get_Status_mB0A602F07AAA2FD4462731CD0F2E92ED9130393F((AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *)(&___obj0), /*hidden argument*/AsyncOperationHandle_1_get_Status_mB0A602F07AAA2FD4462731CD0F2E92ED9130393F_RuntimeMethod_var);
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
		// Complete(true, success, success ? "" : $"{obj.DebugName}, status={obj.Status}, result={obj.Result} failed initialization.");
		bool L_1 = V_0;
		bool L_2 = V_0;
		G_B1_0 = L_1;
		G_B1_1 = 1;
		G_B1_2 = __this;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = 1;
			G_B2_2 = __this;
			goto IL_0037;
		}
	}
	{
		String_t* L_3 = AsyncOperationHandle_1_get_DebugName_mB9D572DD987A5AFC28F4E6398305FAA51BE680AC((AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *)(&___obj0), /*hidden argument*/AsyncOperationHandle_1_get_DebugName_mB9D572DD987A5AFC28F4E6398305FAA51BE680AC_RuntimeMethod_var);
		int32_t L_4 = AsyncOperationHandle_1_get_Status_mB0A602F07AAA2FD4462731CD0F2E92ED9130393F((AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *)(&___obj0), /*hidden argument*/AsyncOperationHandle_1_get_Status_mB0A602F07AAA2FD4462731CD0F2E92ED9130393F_RuntimeMethod_var);
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(AsyncOperationStatus_t598BB2ACEBD2067A004500935C76AA6D845EE005_il2cpp_TypeInfo_var, &L_5);
		RuntimeObject* L_7 = AsyncOperationHandle_1_get_Result_m50166C1809868992DA99771109671F872426DE6F((AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED *)(&___obj0), /*hidden argument*/AsyncOperationHandle_1_get_Result_m50166C1809868992DA99771109671F872426DE6F_RuntimeMethod_var);
		String_t* L_8 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteral60C190087C7D3A3C22FFF66FC21F515D7AD5572E, L_3, L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003c;
	}

IL_0037:
	{
		G_B3_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003c:
	{
		NullCheck(G_B3_3);
		AsyncOperationBase_1_Complete_m4DA85FC017FAB08C949D44BC24D6288684163447(G_B3_3, (bool)G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/AsyncOperationBase_1_Complete_m4DA85FC017FAB08C949D44BC24D6288684163447_RuntimeMethod_var);
		// m_Addressables.Release(m_DepOp);
		AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_9 = __this->get_m_Addressables_16();
		AsyncOperationHandle_1_t68B104C5410359700E7F5A66BC4D9952AAA3F7ED  L_10 = __this->get_m_DepOp_17();
		NullCheck(L_9);
		AddressablesImpl_Release_TisIList_1_tA839EBE831AA697F04E70F745B9C7A5C4E3BAB04_m20D71A2F51CF9A928F32F0DA21B864A948FD37E5(L_9, L_10, /*hidden argument*/AddressablesImpl_Release_TisIList_1_tA839EBE831AA697F04E70F745B9C7A5C4E3BAB04_m20D71A2F51CF9A928F32F0DA21B864A948FD37E5_RuntimeMethod_var);
		// };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * PackedPlayModeBuildLogs_get_RuntimeBuildLogs_mFC37B4B555D8F01D81B994F1BDCFA2EBA5029725_inline (PackedPlayModeBuildLogs_tBEE7F60E510087873AE78578DF5FAFF6D578FF23 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_RuntimeBuildLogs; }
		List_1_t6A59BD3A75F41F2F1CD64353A401F856411254B0 * L_0 = __this->get_m_RuntimeBuildLogs_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * ResourceManagerRuntimeData_get_InitializationObjects_m66450D298C2EA3FC6937F1CDCA3FD45D1EC6419C_inline (ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * __this, const RuntimeMethod* method)
{
	{
		// public List<ObjectInitializationData> InitializationObjects { get { return m_ExtraInitializationData; } }
		List_1_t55FD2028A9666E6769EE6436BCBE3C70DFADB56A * L_0 = __this->get_m_ExtraInitializationData_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_inline (ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 * __this, const RuntimeMethod* method)
{
	{
		// public SerializedType ObjectType { get { return m_ObjectType; } }
		SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  L_0 = __this->get_m_ObjectType_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  Enumerator_get_Current_m801F71E2980D37299592A623FB92A141F866225A_gshared_inline (Enumerator_tDA8044DC2E775CE4AD028682DE55B1EB861DCA80 * __this, const RuntimeMethod* method)
{
	{
		RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44  L_0 = (RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44 )__this->get_current_3();
		return (RuntimeBuildLog_t1E879632D1AAA1D6C22018CCD41812929740BB44 )L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  Enumerator_get_Current_m5481EE2DB20534CFF47F7BF2ABDA5E44D1692443_gshared_inline (Enumerator_tF2594BFC36C019257D61DA46A9D7FC0A23F1A2D9 * __this, const RuntimeMethod* method)
{
	{
		ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  L_0 = (ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 )__this->get_current_3();
		return (ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 )L_0;
	}
}
