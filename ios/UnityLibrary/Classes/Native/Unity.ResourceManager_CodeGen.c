﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void DelegateList`1::.ctor(System.Func`2<System.Action`1<T>,System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>,System.Action`1<System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>)
// 0x00000002 System.Int32 DelegateList`1::get_Count()
// 0x00000003 System.Void DelegateList`1::Add(System.Action`1<T>)
// 0x00000004 System.Void DelegateList`1::Remove(System.Action`1<T>)
// 0x00000005 System.Void DelegateList`1::Invoke(T)
// 0x00000006 System.Void DelegateList`1::Clear()
// 0x00000007 DelegateList`1<T> DelegateList`1::CreateWithGlobalCache()
// 0x00000008 System.Void ListWithEvents`1::add_OnElementAdded(System.Action`1<T>)
// 0x00000009 System.Void ListWithEvents`1::remove_OnElementAdded(System.Action`1<T>)
// 0x0000000A System.Void ListWithEvents`1::add_OnElementRemoved(System.Action`1<T>)
// 0x0000000B System.Void ListWithEvents`1::remove_OnElementRemoved(System.Action`1<T>)
// 0x0000000C System.Void ListWithEvents`1::InvokeAdded(T)
// 0x0000000D System.Void ListWithEvents`1::InvokeRemoved(T)
// 0x0000000E T ListWithEvents`1::get_Item(System.Int32)
// 0x0000000F System.Void ListWithEvents`1::set_Item(System.Int32,T)
// 0x00000010 System.Int32 ListWithEvents`1::get_Count()
// 0x00000011 System.Boolean ListWithEvents`1::get_IsReadOnly()
// 0x00000012 System.Void ListWithEvents`1::Add(T)
// 0x00000013 System.Void ListWithEvents`1::Clear()
// 0x00000014 System.Boolean ListWithEvents`1::Contains(T)
// 0x00000015 System.Void ListWithEvents`1::CopyTo(T[],System.Int32)
// 0x00000016 System.Collections.Generic.IEnumerator`1<T> ListWithEvents`1::GetEnumerator()
// 0x00000017 System.Int32 ListWithEvents`1::IndexOf(T)
// 0x00000018 System.Void ListWithEvents`1::Insert(System.Int32,T)
// 0x00000019 System.Boolean ListWithEvents`1::Remove(T)
// 0x0000001A System.Void ListWithEvents`1::RemoveAt(System.Int32)
// 0x0000001B System.Collections.IEnumerator ListWithEvents`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001C System.Void ListWithEvents`1::.ctor()
// 0x0000001D System.Void MonoBehaviourCallbackHooks::add_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_add_OnUpdateDelegate_m87AED51B4FE2D79C20DAC3B8830776BB97F803D5 (void);
// 0x0000001E System.Void MonoBehaviourCallbackHooks::remove_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m01C7ECAD1953ED33B92C8FF5DE8A026A8CE3AE71 (void);
// 0x0000001F System.String MonoBehaviourCallbackHooks::GetGameObjectName()
extern void MonoBehaviourCallbackHooks_GetGameObjectName_m055C72395F199B2BBF29A85EBDD88070A5A79561 (void);
// 0x00000020 System.Void MonoBehaviourCallbackHooks::Update()
extern void MonoBehaviourCallbackHooks_Update_mF37044CFC87FEC864949100ACD8DC11DD5740E38 (void);
// 0x00000021 System.Void MonoBehaviourCallbackHooks::.ctor()
extern void MonoBehaviourCallbackHooks__ctor_mE5DBDE8B892D936E025E60EDDB435273BC905333 (void);
// 0x00000022 System.Void UnityEngine.ResourceManagement.ChainOperation`2::.ctor()
// 0x00000023 System.String UnityEngine.ResourceManagement.ChainOperation`2::get_DebugName()
// 0x00000024 System.Void UnityEngine.ResourceManagement.ChainOperation`2::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000025 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>,System.Boolean)
// 0x00000026 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Execute()
// 0x00000027 System.Void UnityEngine.ResourceManagement.ChainOperation`2::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000028 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Destroy()
// 0x00000029 System.Void UnityEngine.ResourceManagement.ChainOperation`2::ReleaseDependencies()
// 0x0000002A UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ChainOperation`2::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x0000002B System.Single UnityEngine.ResourceManagement.ChainOperation`2::get_Progress()
// 0x0000002C System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::.ctor()
// 0x0000002D System.String UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_DebugName()
// 0x0000002E System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000002F System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>,System.Boolean)
// 0x00000030 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Execute()
// 0x00000031 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000032 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Destroy()
// 0x00000033 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::ReleaseDependencies()
// 0x00000034 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000035 System.Single UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_Progress()
// 0x00000036 System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception> UnityEngine.ResourceManagement.ResourceManager::get_ExceptionHandler()
extern void ResourceManager_get_ExceptionHandler_mE9398578AC49B638C0ADC70144676C30591BDED1 (void);
// 0x00000037 System.Void UnityEngine.ResourceManagement.ResourceManager::set_ExceptionHandler(System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception>)
extern void ResourceManager_set_ExceptionHandler_m93B2193AA6515D32E5BDAD7C5E9BA7A7E4049F14 (void);
// 0x00000038 System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.ResourceManagement.ResourceManager::get_InternalIdTransformFunc()
extern void ResourceManager_get_InternalIdTransformFunc_m3E8CBA63BA66D29B6893294B2DF8FD9EF0BD3D8A (void);
// 0x00000039 System.Void UnityEngine.ResourceManagement.ResourceManager::set_InternalIdTransformFunc(System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>)
extern void ResourceManager_set_InternalIdTransformFunc_m24175C47AA27AEFDF392CC49749B25D8A01D071E (void);
// 0x0000003A System.String UnityEngine.ResourceManagement.ResourceManager::TransformInternalId(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_TransformInternalId_m117A1A664540BC9546EC5CDBD1B36A25FE0BC889 (void);
// 0x0000003B System.Int32 UnityEngine.ResourceManagement.ResourceManager::get_OperationCacheCount()
extern void ResourceManager_get_OperationCacheCount_m8BCBBD8633D220D4F1B281562211C26653C37951 (void);
// 0x0000003C System.Int32 UnityEngine.ResourceManagement.ResourceManager::get_InstanceOperationCount()
extern void ResourceManager_get_InstanceOperationCount_m57CEED0C028F8D06B5A9E05AB6AAE01FB5038D3F (void);
// 0x0000003D System.Void UnityEngine.ResourceManagement.ResourceManager::AddUpdateReceiver(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_AddUpdateReceiver_m950EAA79EC44CCF02632A799050761670DEC319A (void);
// 0x0000003E System.Void UnityEngine.ResourceManagement.ResourceManager::RemoveUpdateReciever(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_RemoveUpdateReciever_m9FD7EF0CEE2313A78484B81FE0EEF97A7BE0B502 (void);
// 0x0000003F UnityEngine.ResourceManagement.Util.IAllocationStrategy UnityEngine.ResourceManagement.ResourceManager::get_Allocator()
extern void ResourceManager_get_Allocator_m617C32C69B3AF34BA88127E5EE31B8B2731AE685 (void);
// 0x00000040 System.Void UnityEngine.ResourceManagement.ResourceManager::set_Allocator(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager_set_Allocator_m76760FFBBACB5A3C422D88498A57E2688D44DCB8 (void);
// 0x00000041 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::get_ResourceProviders()
extern void ResourceManager_get_ResourceProviders_m66EA708F4B1478C79D4C90E53F68C8A3DC0FB5DA (void);
// 0x00000042 UnityEngine.Networking.CertificateHandler UnityEngine.ResourceManagement.ResourceManager::get_CertificateHandlerInstance()
extern void ResourceManager_get_CertificateHandlerInstance_m2E6389B8CA321D6850C1EF792F7F30CB85E6AD53 (void);
// 0x00000043 System.Void UnityEngine.ResourceManagement.ResourceManager::set_CertificateHandlerInstance(UnityEngine.Networking.CertificateHandler)
extern void ResourceManager_set_CertificateHandlerInstance_m40FDEA12E0566292641236BD511236193B31BB0E (void);
// 0x00000044 System.Void UnityEngine.ResourceManagement.ResourceManager::.ctor(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager__ctor_m72F4278E1B2667B12CA84961A22A799356B2C6A5 (void);
// 0x00000045 System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectAdded(System.Object)
extern void ResourceManager_OnObjectAdded_m09BD1FC6D74DA0D0DEE4FC4E7734DC38BAE43CA3 (void);
// 0x00000046 System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectRemoved(System.Object)
extern void ResourceManager_OnObjectRemoved_m7C0E125D80AF720707652982FF4E63E40DE8539E (void);
// 0x00000047 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForCallbacks()
extern void ResourceManager_RegisterForCallbacks_mD497DF3FD15D025444B1AB3F33542977EA1DEC11 (void);
// 0x00000048 System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticsCallback()
extern void ResourceManager_ClearDiagnosticsCallback_m55CA640ED4CDFEB2ED720BC81402A3855BB44E71 (void);
// 0x00000049 System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticCallbacks()
extern void ResourceManager_ClearDiagnosticCallbacks_mAF83E29B383519A87759E03665B5458D43CD36A0 (void);
// 0x0000004A System.Void UnityEngine.ResourceManagement.ResourceManager::UnregisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext>)
extern void ResourceManager_UnregisterDiagnosticCallback_mAD93A86A5BB291A0A2EAC951730BBFE56E9AA640 (void);
// 0x0000004B System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`4<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.Object>)
extern void ResourceManager_RegisterDiagnosticCallback_m47485FFB63AA58A3659A97DA75191B47022659E9 (void);
// 0x0000004C System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext>)
extern void ResourceManager_RegisterDiagnosticCallback_mDE7E58521FD6B46F0472B310BE672C7364184448 (void);
// 0x0000004D System.Void UnityEngine.ResourceManagement.ResourceManager::PostDiagnosticEvent(UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext)
extern void ResourceManager_PostDiagnosticEvent_m30A67E5310B6A55D34BD8BAB50365A63BB4C2800 (void);
// 0x0000004E UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider UnityEngine.ResourceManagement.ResourceManager::GetResourceProvider(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetResourceProvider_m5F732649805DCCFC152AE89945CCEEB8D4876B5A (void);
// 0x0000004F System.Type UnityEngine.ResourceManagement.ResourceManager::GetDefaultTypeForLocation(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetDefaultTypeForLocation_m4FDDE992FD2950D5BDBDB4AB34E5B150D37760C4 (void);
// 0x00000050 System.Int32 UnityEngine.ResourceManagement.ResourceManager::CalculateLocationsHash(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Type)
extern void ResourceManager_CalculateLocationsHash_m66A19883C1E4299C05C0C997B461C1DA9D3A3377 (void);
// 0x00000051 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Type,System.Boolean)
extern void ResourceManager_ProvideResource_mC630BFAED57BCFA4BC40EE626FD2760772C24D6F (void);
// 0x00000052 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000053 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000054 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_StartOperation_m5F3A0530300FE62FCCA0AA55ADB4AF825AC0E630 (void);
// 0x00000055 System.Void UnityEngine.ResourceManagement.ResourceManager::OnInstanceOperationDestroy(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnInstanceOperationDestroy_m11E9CAD345CBBD8DCC5C984C91BB228CBFD2177F (void);
// 0x00000056 System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyNonCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyNonCached_m8DD16F401831146BAF28C5295853D02075C4B922 (void);
// 0x00000057 System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyCached_m1AC04D6B5F3E74D19F6A1CC34613431727E974A8 (void);
// 0x00000058 T UnityEngine.ResourceManagement.ResourceManager::CreateOperation(System.Type,System.Int32,System.Int32,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x00000059 System.Void UnityEngine.ResourceManagement.ResourceManager::AddOperationToCache(System.Int32,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_AddOperationToCache_m17A4FF6A869D9ED103A4169B9C168CEBFFC835DC (void);
// 0x0000005A System.Boolean UnityEngine.ResourceManagement.ResourceManager::RemoveOperationFromCache(System.Int32)
extern void ResourceManager_RemoveOperationFromCache_mE0343EF5C4A23A28D4A145E9DB20FE7463436768 (void);
// 0x0000005B System.Boolean UnityEngine.ResourceManagement.ResourceManager::IsOperationCached(System.Int32)
extern void ResourceManager_IsOperationCached_mBDB18814D56394E9909A4404DB14C549F60E8AA0 (void);
// 0x0000005C System.Int32 UnityEngine.ResourceManagement.ResourceManager::CachedOperationCount()
extern void ResourceManager_CachedOperationCount_mADC8BA688795B826520977167476B649BD26291D (void);
// 0x0000005D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperation(TObject,System.String)
// 0x0000005E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperationInternal(TObject,System.Boolean,System.String,System.Boolean)
// 0x0000005F System.Void UnityEngine.ResourceManagement.ResourceManager::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Release_mE62F1F2457C5B85A8D41ACA9872D0F5CEE50EE03 (void);
// 0x00000060 System.Void UnityEngine.ResourceManagement.ResourceManager::Acquire(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Acquire_mC0B02D11591D5E0B4AE5826ED6A345D48AF89043 (void);
// 0x00000061 UnityEngine.ResourceManagement.AsyncOperations.GroupOperation UnityEngine.ResourceManagement.ResourceManager::AcquireGroupOpFromCache(System.Int32)
extern void ResourceManager_AcquireGroupOpFromCache_m1D1F014837755ACC0F1E1D99487608E454F007A0 (void);
// 0x00000062 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGroupOperation(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
// 0x00000063 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGroupOperation(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
// 0x00000064 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGenericGroupOperation(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void ResourceManager_CreateGenericGroupOperation_mB729E00A7923C66BD7F66AF04C49785E3348E947 (void);
// 0x00000065 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::ProvideResourceGroupCached(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Int32,System.Type,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void ResourceManager_ProvideResourceGroupCached_mA9021EA82D52FE9FE1589DBF3F3D169C57B4ECDC (void);
// 0x00000066 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager::ProvideResources(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>)
// 0x00000067 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager::ProvideResources(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean,System.Action`1<TObject>)
// 0x00000068 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000069 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x0000006A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperationInternal(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>,System.Boolean)
// 0x0000006B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperationInternal(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>,System.Boolean)
// 0x0000006C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ProvideScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void ResourceManager_ProvideScene_mAF97EC94C8AD654C10DA6D905110B21F640BDCA5 (void);
// 0x0000006D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ReleaseScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void ResourceManager_ReleaseScene_m459CB8CAAB851FA3495F881AEFAD1C3FF29106FA (void);
// 0x0000006E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.ResourceManagement.ResourceManager::ProvideInstance(UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
extern void ResourceManager_ProvideInstance_m9C1635807FC0ED58FB88BEF2B803BC9C1AAD14F3 (void);
// 0x0000006F System.Void UnityEngine.ResourceManagement.ResourceManager::CleanupSceneInstances(UnityEngine.SceneManagement.Scene)
extern void ResourceManager_CleanupSceneInstances_m9BD22D197EE156F52DD1D5FAB1282D4E2113400D (void);
// 0x00000070 System.Void UnityEngine.ResourceManagement.ResourceManager::ExecuteDeferredCallbacks()
extern void ResourceManager_ExecuteDeferredCallbacks_m7839A017C2185C347BB428B84D89D08DC91B3D64 (void);
// 0x00000071 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForDeferredCallback(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Boolean)
extern void ResourceManager_RegisterForDeferredCallback_m63949A3E890C4387FE17491AD5B54AF35ED02E8F (void);
// 0x00000072 System.Void UnityEngine.ResourceManagement.ResourceManager::Update(System.Single)
extern void ResourceManager_Update_m08AF2DABA8576AAA841CCBFDC7A025C00836E33C (void);
// 0x00000073 System.Void UnityEngine.ResourceManagement.ResourceManager::Dispose()
extern void ResourceManager_Dispose_mEFE92B8273D202FCF7A4DAF39D8039257B7AD0B1 (void);
// 0x00000074 System.Void UnityEngine.ResourceManagement.ResourceManager::.cctor()
extern void ResourceManager__cctor_mA7B6D4B17DACE5BD1CCAA35357E9F1EBB3CC9000 (void);
// 0x00000075 System.Void UnityEngine.ResourceManagement.ResourceManager::<.ctor>b__45_0(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_U3C_ctorU3Eb__45_0_m8DAE9BBA3A515ACAB8A018265F816E4F45C5B3C7 (void);
// 0x00000076 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_OperationHandle()
extern void DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD (void);
// 0x00000077 UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_Type()
extern void DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E (void);
// 0x00000078 System.Int32 UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_EventValue()
extern void DiagnosticEventContext_get_EventValue_m47DD1F4C4E14042B9B710D82AE04092C1F184F35 (void);
// 0x00000079 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_Location()
extern void DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7 (void);
// 0x0000007A System.Object UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_Context()
extern void DiagnosticEventContext_get_Context_mE8E291342E3B9293E5AEDE07D44F3BC9E0CB7840 (void);
// 0x0000007B System.String UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::get_Error()
extern void DiagnosticEventContext_get_Error_mE66E65391D117531702CD7EBAE58AD10A9C61B1F (void);
// 0x0000007C System.Void UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.String,System.Object)
extern void DiagnosticEventContext__ctor_m0B5C04CEA5ACB1D89AD8A3005963A97DECC84979 (void);
// 0x0000007D System.Void UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::.ctor()
// 0x0000007E System.Void UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::Init(TObject,System.Boolean,System.String,System.Boolean)
// 0x0000007F System.String UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::get_DebugName()
// 0x00000080 System.Void UnityEngine.ResourceManagement.ResourceManager/CompletedOperation`1::Execute()
// 0x00000081 System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void InstanceOperation_Init_m8F3175B8D48645B5B1B5A7F5680EB2B42AEEE0AF (void);
// 0x00000082 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void InstanceOperation_GetDownloadStatus_mA34529318D816B4BC07F0A35EFC4A572D317233C (void);
// 0x00000083 System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void InstanceOperation_GetDependencies_m25FF504226BFEE33AA8C25FBDFC49A69DB2FFE82 (void);
// 0x00000084 System.String UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::get_DebugName()
extern void InstanceOperation_get_DebugName_mB7397923A863B8762D7EB956FAFCA3D5E7A509F6 (void);
// 0x00000085 UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::InstanceScene()
extern void InstanceOperation_InstanceScene_m6AF4F1151492C24B07C214A98DBAB5CBE64E3DD6 (void);
// 0x00000086 System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::Destroy()
extern void InstanceOperation_Destroy_m97EA48B0B2D6D19E20628CB60B6B9CE2A77C428A (void);
// 0x00000087 System.Single UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::get_Progress()
extern void InstanceOperation_get_Progress_m7F8FB815DC11AFD0B8DA7F73E36F80696DF15DF8 (void);
// 0x00000088 System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::Execute()
extern void InstanceOperation_Execute_m4BFCA35557963C12E209810A6EE278BD2E1117FA (void);
// 0x00000089 System.Void UnityEngine.ResourceManagement.ResourceManager/InstanceOperation::.ctor()
extern void InstanceOperation__ctor_mEF3B832F2785E2DA33B74AA45E69DE4C423F0433 (void);
// 0x0000008A System.Void UnityEngine.ResourceManagement.ResourceManager/<>c__DisplayClass83_0`1::.ctor()
// 0x0000008B System.Void UnityEngine.ResourceManagement.ResourceManager/<>c__DisplayClass83_0`1::<ProvideResources>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x0000008C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager/<>c__DisplayClass83_0`1::<ProvideResources>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x0000008D System.Void UnityEngine.ResourceManagement.IUpdateReceiver::Update(System.Single)
// 0x0000008E System.Boolean UnityEngine.ResourceManagement.WebRequestQueueOperation::get_IsDone()
extern void WebRequestQueueOperation_get_IsDone_mC3E8299EB4F2137D60A4C2A5F33392D9D545BAAA (void);
// 0x0000008F System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueueOperation__ctor_mEEB70D014E81E5CE7294B0813DC576E94BB25C55 (void);
// 0x00000090 System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::Complete(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void WebRequestQueueOperation_Complete_m6D2F3AA5740D67CAE8C6DB4FE4BA542FFA451650 (void);
// 0x00000091 System.Void UnityEngine.ResourceManagement.WebRequestQueue::SetMaxConcurrentRequests(System.Int32)
extern void WebRequestQueue_SetMaxConcurrentRequests_mEB8F9C16DEC65629AE432FC0561478514185BF6D (void);
// 0x00000092 UnityEngine.ResourceManagement.WebRequestQueueOperation UnityEngine.ResourceManagement.WebRequestQueue::QueueRequest(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueue_QueueRequest_m54F1A56E5082365292141AC67CEFEBEEA52F04AE (void);
// 0x00000093 System.Void UnityEngine.ResourceManagement.WebRequestQueue::OnWebAsyncOpComplete(UnityEngine.AsyncOperation)
extern void WebRequestQueue_OnWebAsyncOpComplete_m17C30059501F106DF7BFB9F227F7E5DAD800900D (void);
// 0x00000094 System.Void UnityEngine.ResourceManagement.WebRequestQueue::.cctor()
extern void WebRequestQueue__cctor_mD23AA2F5F8B4D8BF629334F3C3A7B8E9715F3D1B (void);
// 0x00000095 System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor()
extern void ResourceManagerException__ctor_m01391C459974ED2BC4FC9D49A5EA5F1E43D31AD6 (void);
// 0x00000096 System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.String)
extern void ResourceManagerException__ctor_m8169F1FC9FA779C72056110DE66646F51B9CFDC8 (void);
// 0x00000097 System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.String,System.Exception)
extern void ResourceManagerException__ctor_m86948C7C5AE7D082B0656AA751D9CAA7BEBDAA6C (void);
// 0x00000098 System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ResourceManagerException__ctor_m521E0FC24584DE7F32D8E8E52525A18C70EC1EAC (void);
// 0x00000099 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Location()
extern void UnknownResourceProviderException_get_Location_mC37FB4E0339053A4F3C5DFDEFAC15F8B4E1A7CA6 (void);
// 0x0000009A System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::set_Location(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException_set_Location_mC68D88A08BAD8EC90EC965C7DF5B0D019AA8855D (void);
// 0x0000009B System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException__ctor_mE46E123397D4F657424D013093385B7BA9611815 (void);
// 0x0000009C System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor()
extern void UnknownResourceProviderException__ctor_m69A301E51BA3A08D097A5B7570AB1B9BEDEBB205 (void);
// 0x0000009D System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.String)
extern void UnknownResourceProviderException__ctor_mE26E8FC70B59AF0F9A9E8A8249B3CDF0E794A9FB (void);
// 0x0000009E System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.String,System.Exception)
extern void UnknownResourceProviderException__ctor_m9A851459EA94ED9A7F8E1F4522C4E6470949D37D (void);
// 0x0000009F System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UnknownResourceProviderException__ctor_m018E494ED54533C65EC6461C5BFA1DC26557D4A7 (void);
// 0x000000A0 System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Message()
extern void UnknownResourceProviderException_get_Message_mEDAFB46A1DAED56B03BD9AC94CE53E9052DE0E41 (void);
// 0x000000A1 System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::ToString()
extern void UnknownResourceProviderException_ToString_mEF4042034C559688A612FF63DC72B5E0450A6CEF (void);
// 0x000000A2 System.Boolean UnityEngine.ResourceManagement.Util.ComponentSingleton`1::get_Exists()
// 0x000000A3 T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::get_Instance()
// 0x000000A4 T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::FindInstance()
// 0x000000A5 System.String UnityEngine.ResourceManagement.Util.ComponentSingleton`1::GetGameObjectName()
// 0x000000A6 T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::CreateNewSingleton()
// 0x000000A7 System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::Awake()
// 0x000000A8 System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::DestroySingleton()
// 0x000000A9 System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::.ctor()
// 0x000000AA System.Collections.Generic.LinkedListNode`1<UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo> UnityEngine.ResourceManagement.Util.DelayedActionManager::GetNode(UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo&)
extern void DelayedActionManager_GetNode_m6E89B600D763BA70B4DB26528AF8D170B234FB8A (void);
// 0x000000AB System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::Clear()
extern void DelayedActionManager_Clear_m2027FDCBDC891F654C104C98EC1991C92BFF9C3E (void);
// 0x000000AC System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::DestroyWhenComplete()
extern void DelayedActionManager_DestroyWhenComplete_m012CE6FCBDD8B238DEA779002FA400006B6F378F (void);
// 0x000000AD System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::AddAction(System.Delegate,System.Single,System.Object[])
extern void DelayedActionManager_AddAction_mF8F65EE6C2F4EACC027E1E1FB5D7FE00E5639CF5 (void);
// 0x000000AE System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::AddActionInternal(System.Delegate,System.Single,System.Object[])
extern void DelayedActionManager_AddActionInternal_m152580083979E12ADC9F1F2D6628D4A20D373420 (void);
// 0x000000AF System.Boolean UnityEngine.ResourceManagement.Util.DelayedActionManager::get_IsActive()
extern void DelayedActionManager_get_IsActive_mD85795ACE4EDF386CBC7A5A11EE2EC80FF4D9584 (void);
// 0x000000B0 System.Boolean UnityEngine.ResourceManagement.Util.DelayedActionManager::Wait(System.Single,System.Single)
extern void DelayedActionManager_Wait_mB71946F645806E69AF4348ED0104AF0BC92E7730 (void);
// 0x000000B1 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::LateUpdate()
extern void DelayedActionManager_LateUpdate_m17A22132BABB6873D12730B51D08EEEED122335F (void);
// 0x000000B2 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::InternalLateUpdate(System.Single)
extern void DelayedActionManager_InternalLateUpdate_mE1B94E403B75E83D07D5A537C43BB582D45314C0 (void);
// 0x000000B3 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::OnApplicationQuit()
extern void DelayedActionManager_OnApplicationQuit_m650F69C5F7F85477DFB8931DBD91589266709EE6 (void);
// 0x000000B4 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::.ctor()
extern void DelayedActionManager__ctor_m1992ABABC7393554D05674AFAE25D8BA1BD56A9E (void);
// 0x000000B5 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::.ctor(System.Delegate,System.Single,System.Object[])
extern void DelegateInfo__ctor_mD79A58037BB825D5FA49F0D35C072FC85EA6EDCF (void);
// 0x000000B6 System.Single UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::get_InvocationTime()
extern void DelegateInfo_get_InvocationTime_m2DFFB1182B78A216D6210CB939DA4DBC59E7DF59 (void);
// 0x000000B7 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::set_InvocationTime(System.Single)
extern void DelegateInfo_set_InvocationTime_m3472B095E8176F30FCED254739CCF797E8121E46 (void);
// 0x000000B8 System.String UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::ToString()
extern void DelegateInfo_ToString_m0FC72F240C31351642A1401F70AEC75127BB0635 (void);
// 0x000000B9 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager/DelegateInfo::Invoke()
extern void DelegateInfo_Invoke_m9853B80CDAD912DC9E644027D0B8B8AA03B362E9 (void);
// 0x000000BA System.Boolean UnityEngine.ResourceManagement.Util.IInitializableObject::Initialize(System.String,System.String)
// 0x000000BB UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.Util.IInitializableObject::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
// 0x000000BC System.String UnityEngine.ResourceManagement.Util.IObjectInitializationDataProvider::get_Name()
// 0x000000BD UnityEngine.ResourceManagement.Util.ObjectInitializationData UnityEngine.ResourceManagement.Util.IObjectInitializationDataProvider::CreateObjectInitializationData()
// 0x000000BE System.Object UnityEngine.ResourceManagement.Util.IAllocationStrategy::New(System.Type,System.Int32)
// 0x000000BF System.Void UnityEngine.ResourceManagement.Util.IAllocationStrategy::Release(System.Int32,System.Object)
// 0x000000C0 System.Object UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::New(System.Type,System.Int32)
extern void DefaultAllocationStrategy_New_mC842EAFAF088441610BB59880015DA917A407F65 (void);
// 0x000000C1 System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::Release(System.Int32,System.Object)
extern void DefaultAllocationStrategy_Release_mF5CF80B8C407DAB00382F1249DA26132925766F0 (void);
// 0x000000C2 System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::.ctor()
extern void DefaultAllocationStrategy__ctor_mD43247AE516C9B2D7C8B3ED2F31399537E468014 (void);
// 0x000000C3 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void LRUCacheAllocationStrategy__ctor_mE0BA2D685813D042B3515DF86B5ED5566652861F (void);
// 0x000000C4 System.Collections.Generic.List`1<System.Object> UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::GetPool()
extern void LRUCacheAllocationStrategy_GetPool_mD773863C92E277581464529F161B016D042733BD (void);
// 0x000000C5 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::ReleasePool(System.Collections.Generic.List`1<System.Object>)
extern void LRUCacheAllocationStrategy_ReleasePool_mB58B3D361C98E1E4C6043F73661F624F3005F1C8 (void);
// 0x000000C6 System.Object UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::New(System.Type,System.Int32)
extern void LRUCacheAllocationStrategy_New_m061E645DCA19415B1289FE2259DCBD404F1BF0AF (void);
// 0x000000C7 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::Release(System.Int32,System.Object)
extern void LRUCacheAllocationStrategy_Release_m2530B50A0F910642E5F147686EEFD90ECA268255 (void);
// 0x000000C8 System.Void UnityEngine.ResourceManagement.Util.SerializedTypeRestrictionAttribute::.ctor()
extern void SerializedTypeRestrictionAttribute__ctor_m1A85236760A4446B2E8916F9B83BA86C9E4483BE (void);
// 0x000000C9 System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Acquire(T)
// 0x000000CA System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x000000CB System.Int32 UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::get_CreatedNodeCount()
// 0x000000CC System.Int32 UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::get_CachedNodeCount()
// 0x000000CD System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::.ctor()
// 0x000000CE System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Acquire(T)
// 0x000000CF System.Void UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x000000D0 System.String UnityEngine.ResourceManagement.Util.SerializedType::get_AssemblyName()
extern void SerializedType_get_AssemblyName_mB4D8E282E3B22D60B88D0B612E1DD2706450C6FA (void);
// 0x000000D1 System.String UnityEngine.ResourceManagement.Util.SerializedType::get_ClassName()
extern void SerializedType_get_ClassName_m58CBCA1680BC7B435A66D60D6E49FBE3E2FF03FD (void);
// 0x000000D2 System.String UnityEngine.ResourceManagement.Util.SerializedType::ToString()
extern void SerializedType_ToString_mE7F91178BCF528D8D66A51CAED1BE91B046C4486 (void);
// 0x000000D3 System.Type UnityEngine.ResourceManagement.Util.SerializedType::get_Value()
extern void SerializedType_get_Value_mA75D04829016315F5CB5EBD10CD4A44C0754012C (void);
// 0x000000D4 System.Void UnityEngine.ResourceManagement.Util.SerializedType::set_Value(System.Type)
extern void SerializedType_set_Value_m45104AAC40C5238AF758D2D552B92E52FF705092 (void);
// 0x000000D5 System.Boolean UnityEngine.ResourceManagement.Util.SerializedType::get_ValueChanged()
extern void SerializedType_get_ValueChanged_mA3E9901574336A20A27D2A0DED75B476C02E261E (void);
// 0x000000D6 System.Void UnityEngine.ResourceManagement.Util.SerializedType::set_ValueChanged(System.Boolean)
extern void SerializedType_set_ValueChanged_mFEA6D354142B82599C513F737E679AB4A8C08E9F (void);
// 0x000000D7 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Id()
extern void ObjectInitializationData_get_Id_m769EFD3C4475326DDFC86C9E9255973AB4B84561 (void);
// 0x000000D8 UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_ObjectType()
extern void ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532 (void);
// 0x000000D9 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Data()
extern void ObjectInitializationData_get_Data_mA167B62E4FB7ADE8409B4AB74D534EE18BBDA7FC (void);
// 0x000000DA System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::ToString()
extern void ObjectInitializationData_ToString_m31940BC41095BD7336F705D0BF27B827BE91BBBC (void);
// 0x000000DB TObject UnityEngine.ResourceManagement.Util.ObjectInitializationData::CreateInstance(System.String)
// 0x000000DC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.Util.ObjectInitializationData::GetAsyncInitHandle(UnityEngine.ResourceManagement.ResourceManager,System.String)
extern void ObjectInitializationData_GetAsyncInitHandle_m0186D6321B14C9AC25E3F8196220ABBE96A965F0 (void);
// 0x000000DD System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ExtractKeyAndSubKey(System.Object,System.String&,System.String&)
extern void ResourceManagerConfig_ExtractKeyAndSubKey_mB1352EBE9B7434F2F0F8A092CFDF64C162FFD57C (void);
// 0x000000DE System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::IsPathRemote(System.String)
extern void ResourceManagerConfig_IsPathRemote_m2C5EA552B346A09472FD4D9A797E1A2C80F1DBAA (void);
// 0x000000DF System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ShouldPathUseWebRequest(System.String)
extern void ResourceManagerConfig_ShouldPathUseWebRequest_m965D90CDBCB1B2960449029203B6CD7B1F787060 (void);
// 0x000000E0 System.Array UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateArrayResult(System.Type,UnityEngine.Object[])
extern void ResourceManagerConfig_CreateArrayResult_m4915DA72A71401B4AA6B23DABEE79F5EB87E5115 (void);
// 0x000000E1 TObject UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateArrayResult(UnityEngine.Object[])
// 0x000000E2 System.Collections.IList UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateListResult(System.Type,UnityEngine.Object[])
extern void ResourceManagerConfig_CreateListResult_m1D92E3CF8E1C7D105A18BDAC71FCFC8EEDF7D03D (void);
// 0x000000E3 TObject UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateListResult(UnityEngine.Object[])
// 0x000000E4 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::IsInstance()
// 0x000000E5 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource::GetAssetBundle()
// 0x000000E6 System.String UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Hash()
extern void AssetBundleRequestOptions_get_Hash_m4E09B8CEBD441A4BBCB5E938C506581B5B21B9E9 (void);
// 0x000000E7 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Hash(System.String)
extern void AssetBundleRequestOptions_set_Hash_m316DB9107A1B7D38F665646C8A6393F907A30578 (void);
// 0x000000E8 System.UInt32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Crc()
extern void AssetBundleRequestOptions_get_Crc_m2045B390D2F29C985C4B6AA1C7B62BC8F412BE70 (void);
// 0x000000E9 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Crc(System.UInt32)
extern void AssetBundleRequestOptions_set_Crc_mC97B6149AA2CF428CCB89E81C50DB28D25BF4A28 (void);
// 0x000000EA System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Timeout()
extern void AssetBundleRequestOptions_get_Timeout_m316EA8799F388E2CD6B4D8B7EFE1FB0644E27482 (void);
// 0x000000EB System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Timeout(System.Int32)
extern void AssetBundleRequestOptions_set_Timeout_m629613CA143FED7DD52CD18D5B5D136C31854369 (void);
// 0x000000EC System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_ChunkedTransfer()
extern void AssetBundleRequestOptions_get_ChunkedTransfer_m01C2B585734CDE057E74C134A3A5B88719BBCFA6 (void);
// 0x000000ED System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_ChunkedTransfer(System.Boolean)
extern void AssetBundleRequestOptions_set_ChunkedTransfer_m38F1FE80CB53AED2187F9BED913EB53DA6D3C929 (void);
// 0x000000EE System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_RedirectLimit()
extern void AssetBundleRequestOptions_get_RedirectLimit_m8C4C7E38FF49194DA5A40D1FB1C4BB1618FFA7D3 (void);
// 0x000000EF System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_RedirectLimit(System.Int32)
extern void AssetBundleRequestOptions_set_RedirectLimit_mCF37BDC8B611CF76BD33E38E7C6A41009793B4DB (void);
// 0x000000F0 System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_RetryCount()
extern void AssetBundleRequestOptions_get_RetryCount_m50E1494EB797D875FAEAB3AED7D2B29AEAF63382 (void);
// 0x000000F1 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_RetryCount(System.Int32)
extern void AssetBundleRequestOptions_set_RetryCount_m9ABB7EB9BC0D7511685BFC05B24DC2F0A0CA57B2 (void);
// 0x000000F2 System.String UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_BundleName()
extern void AssetBundleRequestOptions_get_BundleName_mC59B8E79B08BB3408A763C9B8D8B968D19E71FAA (void);
// 0x000000F3 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_BundleName(System.String)
extern void AssetBundleRequestOptions_set_BundleName_m38C6621464EB9D69CA387959C68C899AF38FB315 (void);
// 0x000000F4 System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_BundleSize()
extern void AssetBundleRequestOptions_get_BundleSize_m837CABABA41D15168137DDB77C235D1A5036B6A3 (void);
// 0x000000F5 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_BundleSize(System.Int64)
extern void AssetBundleRequestOptions_set_BundleSize_m2A790CEA18CA25BECAFEE784991F8AB66FEBC901 (void);
// 0x000000F6 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_UseCrcForCachedBundle()
extern void AssetBundleRequestOptions_get_UseCrcForCachedBundle_m1C00D926365DF41F20DC6F2908F0128EDE6888A1 (void);
// 0x000000F7 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_UseCrcForCachedBundle(System.Boolean)
extern void AssetBundleRequestOptions_set_UseCrcForCachedBundle_mDD1ECC570ED4D82E4F44AFBE2B3EF958BDDA3F7F (void);
// 0x000000F8 System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::ComputeSize(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager)
extern void AssetBundleRequestOptions_ComputeSize_m090B8ECB2F40F173679FC235A9C3DA6915427D6E (void);
// 0x000000F9 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::.ctor()
extern void AssetBundleRequestOptions__ctor_mECFC54613FCFAD2997F2616562DA9EAA796A2199 (void);
// 0x000000FA UnityEngine.Networking.UnityWebRequest UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::CreateWebRequest(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AssetBundleResource_CreateWebRequest_m008113D1236E539144C558988C2D6F9D71881962 (void);
// 0x000000FB System.Single UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::PercentComplete()
extern void AssetBundleResource_PercentComplete_mED6167B98761953F9D21BCCCBD2B49296FA69D67 (void);
// 0x000000FC UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetDownloadStatus()
extern void AssetBundleResource_GetDownloadStatus_mD477BF4665169B01680B78FCB57CE9095BEF6F13 (void);
// 0x000000FD UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetAssetBundle()
extern void AssetBundleResource_GetAssetBundle_mEDCC866A7AF1A532F40B40B821DD457AC73FB8D0 (void);
// 0x000000FE System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AssetBundleResource_Start_mC19F8F354525DC25A262C5054BB68380BAA7A7C0 (void);
// 0x000000FF System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::BeginOperation()
extern void AssetBundleResource_BeginOperation_mB48A167F91987D1255038260B8D95381D9CE3EBB (void);
// 0x00000100 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::LocalRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void AssetBundleResource_LocalRequestOperationCompleted_m9CD3F70C0A35A95F298C4ACE444EDF6D1BBFFDBC (void);
// 0x00000101 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::WebRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void AssetBundleResource_WebRequestOperationCompleted_m9F2F0EC5AE2BFA16CF34C1CA1407E7C4E04A9B9F (void);
// 0x00000102 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Unload()
extern void AssetBundleResource_Unload_m8A7BDC23B2F5D403F5856771C45A56FC94EFB80F (void);
// 0x00000103 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::.ctor()
extern void AssetBundleResource__ctor_m252A70EBBA6B3827A634F8621F302960820B752F (void);
// 0x00000104 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::<BeginOperation>b__13_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void AssetBundleResource_U3CBeginOperationU3Eb__13_0_m3526234E064ACDB8E8B9896754280EB0B4ADA028 (void);
// 0x00000105 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AssetBundleProvider_Provide_m7261CC3C80ECC32D8356DEF62234754764DF96B3 (void);
// 0x00000106 System.Type UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AssetBundleProvider_GetDefaultType_mCDB0B3B3E180FE1873A48A781F879AFB4DD97531 (void);
// 0x00000107 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void AssetBundleProvider_Release_m1D7D55505161B85DE77D4BFE84E9D94B2F7EE332 (void);
// 0x00000108 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::.ctor()
extern void AssetBundleProvider__ctor_m317B2BC098303C8CBAB7760BE13A78F526AA24A9 (void);
// 0x00000109 System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AtlasSpriteProvider_Provide_m5C5B89EFDB74D296ADEA5C901306AAE25D593213 (void);
// 0x0000010A System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::.ctor()
extern void AtlasSpriteProvider__ctor_m33DD706E56EC96EF28DF76C13C034DD5A6C34F1C (void);
// 0x0000010B System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void BundledAssetProvider_Provide_mAE2649FB77C2D9450265EF65772E4483039F2F4D (void);
// 0x0000010C System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider::.ctor()
extern void BundledAssetProvider__ctor_mDC8402C44066C088B1D8496D5C758434CF5FF003 (void);
// 0x0000010D UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::LoadBundleFromDependecies(System.Collections.Generic.IList`1<System.Object>)
extern void InternalOp_LoadBundleFromDependecies_m21F705B1F2719B8F2FAA8974B0179EA4093F2DBA (void);
// 0x0000010E System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void InternalOp_Start_m89777E539E2163EEA568A672200C2BD3F5419926 (void);
// 0x0000010F System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::ActionComplete(UnityEngine.AsyncOperation)
extern void InternalOp_ActionComplete_m3B9C2BACE76D3A41B6DEE145A42EC3548BC8B0D3 (void);
// 0x00000110 System.Single UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::ProgressCallback()
extern void InternalOp_ProgressCallback_mCBD860D0D7B1E49FB2297546D7D66C6A51BB8012 (void);
// 0x00000111 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider/InternalOp::.ctor()
extern void InternalOp__ctor_mFC858704B9747EEFA53E021392B2900BCFCD27D6 (void);
// 0x00000112 UnityEngine.Vector3 UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Position()
extern void InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC (void);
// 0x00000113 UnityEngine.Quaternion UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Rotation()
extern void InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012 (void);
// 0x00000114 UnityEngine.Transform UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Parent()
extern void InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621 (void);
// 0x00000115 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_InstantiateInWorldPosition()
extern void InstantiationParameters_get_InstantiateInWorldPosition_mD022156583A3F9F0DCFF0DAEA75F571E4BBE25B8 (void);
// 0x00000116 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_SetPositionRotation()
extern void InstantiationParameters_get_SetPositionRotation_m1A4A574A3025F0DCE609D8F49F6071FA57904F9C (void);
// 0x00000117 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::.ctor(UnityEngine.Transform,System.Boolean)
extern void InstantiationParameters__ctor_m32A54AA524BFE9CCF5D2EA04CD665297FFA1C0E6 (void);
// 0x00000118 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void InstantiationParameters__ctor_mDCF85728CF9BB36A6EE0930C8B68C10810FE60AA (void);
// 0x00000119 TObject UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::Instantiate(TObject)
// 0x0000011A UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
// 0x0000011B System.Void UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ReleaseInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.GameObject)
// 0x0000011C System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::.ctor(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation)
extern void ProvideHandle__ctor_mEBB5A836A4CDB0B4FB938F85050D865695516097 (void);
// 0x0000011D UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_InternalOp()
extern void ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F (void);
// 0x0000011E UnityEngine.ResourceManagement.ResourceManager UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_ResourceManager()
extern void ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76 (void);
// 0x0000011F System.Type UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Type()
extern void ProvideHandle_get_Type_m52E919AFDB11C39D84549BA4E05F730F14BC528B (void);
// 0x00000120 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Location()
extern void ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978 (void);
// 0x00000121 System.Int32 UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_DependencyCount()
extern void ProvideHandle_get_DependencyCount_m1C44CB1B419CE21A00D6E4275B01147F978FC1AA (void);
// 0x00000122 TDepObject UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependency(System.Int32)
// 0x00000123 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
extern void ProvideHandle_GetDependencies_mF82C08A32FF506D98985684CF1E7E42EF4326653 (void);
// 0x00000124 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::SetProgressCallback(System.Func`1<System.Single>)
extern void ProvideHandle_SetProgressCallback_m762D5F97C1CAAC2D0431DE3C8C22F6FB405FC77E (void);
// 0x00000125 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::SetDownloadProgressCallbacks(System.Func`1<UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus>)
extern void ProvideHandle_SetDownloadProgressCallbacks_mA4D6FFA06016A567D962BE5AF19F1CAF3C5B5C7C (void);
// 0x00000126 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::Complete(T,System.Boolean,System.Exception)
// 0x00000127 System.String UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_ProviderId()
// 0x00000128 System.Type UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000129 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x0000012A System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x0000012B System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
// 0x0000012C UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_BehaviourFlags()
// 0x0000012D UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::get_Scene()
extern void SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3 (void);
// 0x0000012E System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::set_Scene(UnityEngine.SceneManagement.Scene)
extern void SceneInstance_set_Scene_m9B163169E348E68FF15DFDEF76E25815AF6D063D (void);
// 0x0000012F System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Activate()
extern void SceneInstance_Activate_mA85392F02BF420CD5D4ACD8BA84CCE4A577F7466 (void);
// 0x00000130 UnityEngine.AsyncOperation UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::ActivateAsync()
extern void SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3 (void);
// 0x00000131 System.Int32 UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::GetHashCode()
extern void SceneInstance_GetHashCode_mFB6154BAA8F8283352C273727C0611405EDC963D (void);
// 0x00000132 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Equals(System.Object)
extern void SceneInstance_Equals_mF184704D845C4F37D594CE251E7DBD449B5E7F38 (void);
// 0x00000133 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ProvideScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
// 0x00000134 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
// 0x00000135 UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
extern void InstanceProvider_ProvideInstance_m530D2258769CC2B97647ECFBCF34659BD93C2C6B (void);
// 0x00000136 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::ReleaseInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.GameObject)
extern void InstanceProvider_ReleaseInstance_m28F42006EDEA00DCAF391F17303EDB0EE83199E6 (void);
// 0x00000137 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::.ctor()
extern void InstanceProvider__ctor_m1D163567C544AA99FE68262DEBAC0FD3EE7A9B69 (void);
// 0x00000138 System.Object UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::Convert(System.Type,System.String)
extern void JsonAssetProvider_Convert_m5E81612E9A2074C28456F014F751C07715CB7424 (void);
// 0x00000139 System.Void UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::.ctor()
extern void JsonAssetProvider__ctor_m414B5D8DE089729BB8C2E80849FB109D17462448 (void);
// 0x0000013A System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void LegacyResourcesProvider_Provide_m971E9C71D39B404FFDAA4C6F9B9487A590609211 (void);
// 0x0000013B System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void LegacyResourcesProvider_Release_mB20852B4A94D65940F18484F00767408CD88FE03 (void);
// 0x0000013C System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::.ctor()
extern void LegacyResourcesProvider__ctor_mCC36862065E722B44749F3856D01F90CDB83F72C (void);
// 0x0000013D System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider/InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void InternalOp_Start_mDB6E979C502EFB3276D89619217754D57AC48805 (void);
// 0x0000013E System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider/InternalOp::AsyncOperationCompleted(UnityEngine.AsyncOperation)
extern void InternalOp_AsyncOperationCompleted_m30C8DF6B9601A1511465548C371CDA2E2FB3F33C (void);
// 0x0000013F System.Single UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider/InternalOp::PercentComplete()
extern void InternalOp_PercentComplete_mFBA923ACE0098674D6F6C9AD7A68640180572DF3 (void);
// 0x00000140 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider/InternalOp::.ctor()
extern void InternalOp__ctor_m098F6C379EFFB213B8ADC3B5CE14A63AB7A37539 (void);
// 0x00000141 System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::get_ProviderId()
extern void ResourceProviderBase_get_ProviderId_m6184BE49E537EF0650F299F642B1E1C0E0D00996 (void);
// 0x00000142 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Initialize(System.String,System.String)
extern void ResourceProviderBase_Initialize_mA8285F2F9D6AB90FBE07229C852D29D12259694D (void);
// 0x00000143 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_CanProvide_m76642A78907B6CD37706CA93E660ABCEF9F87C85 (void);
// 0x00000144 System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::ToString()
extern void ResourceProviderBase_ToString_mD650C694955FB3B6635377B8DFF5E4D707B2E342 (void);
// 0x00000145 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void ResourceProviderBase_Release_m46D9D39E8F99B2A9D8FA478D1BA6D051B169EBA1 (void);
// 0x00000146 System.Type UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_GetDefaultType_mBA1FAA374F596F96BCD4A6F1C76839356A3A17F0 (void);
// 0x00000147 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x00000148 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
extern void ResourceProviderBase_InitializeAsync_m4B3BF412B1D41A03BA8D30918FE7A7C4006E4F1F (void);
// 0x00000149 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider.get_BehaviourFlags()
extern void ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mFDEC1DF913DFD712F248FAD2A36DB610F7580F25 (void);
// 0x0000014A System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::.ctor()
extern void ResourceProviderBase__ctor_mC9B337B8D57A9C39902D92A9F8B5A06057C340F7 (void);
// 0x0000014B System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/BaseInitAsyncOp::Init(System.Func`1<System.Boolean>)
extern void BaseInitAsyncOp_Init_m124642C8428A2986C50FF8B9CD624012F4F29938 (void);
// 0x0000014C System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/BaseInitAsyncOp::Execute()
extern void BaseInitAsyncOp_Execute_m4C62815A957FE145FFB26B6F0B7B9AC734D133A9 (void);
// 0x0000014D System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/BaseInitAsyncOp::.ctor()
extern void BaseInitAsyncOp__ctor_mFE9165E83F0C5435643766CFAEAB0AA631FF495B (void);
// 0x0000014E System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m9A7D2B62835C38A11D92570665937E0C5854BE52 (void);
// 0x0000014F System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase/<>c__DisplayClass10_0::<InitializeAsync>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m1B9EB201931A439A29FD64339511621980DBF9C0 (void);
// 0x00000150 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::ProvideScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void SceneProvider_ProvideScene_m4F70BD611D12B7822B5770066AF144FB577C5583 (void);
// 0x00000151 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void SceneProvider_ReleaseScene_m1A7A7B799DFEF64C68E7101546579CC907E0048A (void);
// 0x00000152 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::.ctor()
extern void SceneProvider__ctor_mADE65924761AD6CA8132FA0980EEB3480701A225 (void);
// 0x00000153 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::.ctor(UnityEngine.ResourceManagement.ResourceManager)
extern void SceneOp__ctor_m9D33D33E529B1290A79E3C11BF62B7348BB09D36 (void);
// 0x00000154 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void SceneOp_GetDownloadStatus_m1D5B073EE18406450DA223BBDF851D2728162138 (void);
// 0x00000155 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::Init(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
extern void SceneOp_Init_m51770548CC4207ED9FFBA41BF1179D9DB11A0571 (void);
// 0x00000156 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void SceneOp_GetDependencies_m505E528E6A74BE2234E9835AE451F59689BDAF3D (void);
// 0x00000157 System.String UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::get_DebugName()
extern void SceneOp_get_DebugName_m9317CCE448A96B710DF79D7722989A51D7802FA5 (void);
// 0x00000158 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::Execute()
extern void SceneOp_Execute_mAC2090B2EA33B97E5AAD2E44F3880E7B21A9199C (void);
// 0x00000159 UnityEngine.ResourceManagement.ResourceProviders.SceneInstance UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::InternalLoadScene(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Boolean,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void SceneOp_InternalLoadScene_m2CAA571373803372A9429801BE7E78BE193A06BF (void);
// 0x0000015A UnityEngine.AsyncOperation UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::InternalLoad(System.String,System.Boolean,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneOp_InternalLoad_mA7B170134422CBCD64923BCE1CCDDA7F98B7C610 (void);
// 0x0000015B System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::Destroy()
extern void SceneOp_Destroy_mE8547B5022F4751624DE1C743F8D3B36B9A0CEDB (void);
// 0x0000015C System.Single UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::get_Progress()
extern void SceneOp_get_Progress_m80BCE300B854BAD74C7AA5AD5248D18E72C05B91 (void);
// 0x0000015D System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/SceneOp::UnityEngine.ResourceManagement.IUpdateReceiver.Update(System.Single)
extern void SceneOp_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m6639AFC516698E0D44E6F996112F711DAA446B0C (void);
// 0x0000015E System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void UnloadSceneOp_Init_mFFCFC26F6DA81BCA74A3BBA3C3ECF2935D3837F5 (void);
// 0x0000015F System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::Execute()
extern void UnloadSceneOp_Execute_m7BFBA4CC6B880638D3A7C50842B736EBD2156A18 (void);
// 0x00000160 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::UnloadSceneCompleted(UnityEngine.AsyncOperation)
extern void UnloadSceneOp_UnloadSceneCompleted_m10F816AE19F3266AC6AC5C80B8169FACA8E272BA (void);
// 0x00000161 System.Single UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::get_Progress()
extern void UnloadSceneOp_get_Progress_m73550503E47D08122BD59EDAAA4F89B9768C7C34 (void);
// 0x00000162 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider/UnloadSceneOp::.ctor()
extern void UnloadSceneOp__ctor_m9FFCFD1D98D7DDCE47CC1E1390B6D2306919C699 (void);
// 0x00000163 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::get_IgnoreFailures()
extern void TextDataProvider_get_IgnoreFailures_m9B008F37CE1A3B2AC0A7A9B929684D0F2CB761DD (void);
// 0x00000164 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::set_IgnoreFailures(System.Boolean)
extern void TextDataProvider_set_IgnoreFailures_mC87BF0A8E1FD80D131A49F6F36946DD1BB28F703 (void);
// 0x00000165 System.Object UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Convert(System.Type,System.String)
extern void TextDataProvider_Convert_mE6997E7A73F9D4D4B1D479E112D6D567356A51C8 (void);
// 0x00000166 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void TextDataProvider_Provide_m6EBF00ECB1FC60B196D4810B7397FE57CC95B302 (void);
// 0x00000167 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::.ctor()
extern void TextDataProvider__ctor_m2139CF91C3D15A52F07BF06758DAE372B6E869C8 (void);
// 0x00000168 System.Single UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::GetPercentComplete()
extern void InternalOp_GetPercentComplete_mCBA1C79FA03221A9719DE8F2D167E601ED38F48F (void);
// 0x00000169 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle,UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider,System.Boolean)
extern void InternalOp_Start_m451A235CFF51EC2F1A4572380C16F99186F78C62 (void);
// 0x0000016A System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::RequestOperation_completed(UnityEngine.AsyncOperation)
extern void InternalOp_RequestOperation_completed_mE369E59BFAD959075E24A08B2C8BC161AB00FCDA (void);
// 0x0000016B System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::.ctor()
extern void InternalOp__ctor_m89FAB3AFC1702B9DE88A8CC8DF9BFA0640EFCA5D (void);
// 0x0000016C System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider/InternalOp::<Start>b__6_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void InternalOp_U3CStartU3Eb__6_0_mA6BFCF95D75BB17BFFB8E7AE463DB6897F6B5D3F (void);
// 0x0000016D System.Int64 UnityEngine.ResourceManagement.ResourceLocations.ILocationSizeData::ComputeSize(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager)
// 0x0000016E System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_InternalId()
// 0x0000016F System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ProviderId()
// 0x00000170 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Dependencies()
// 0x00000171 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::Hash(System.Type)
// 0x00000172 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_DependencyHashCode()
// 0x00000173 System.Boolean UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_HasDependencies()
// 0x00000174 System.Object UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Data()
// 0x00000175 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_PrimaryKey()
// 0x00000176 System.Type UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ResourceType()
// 0x00000177 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_InternalId()
extern void ResourceLocationBase_get_InternalId_m48AE4FE26C6020D0379C5EA2D0C78BE65EB5321A (void);
// 0x00000178 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ProviderId()
extern void ResourceLocationBase_get_ProviderId_m0FE503A1563C2043448041496BFE6C8DEB0CD17A (void);
// 0x00000179 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Dependencies()
extern void ResourceLocationBase_get_Dependencies_mD2A7F2814C27932160FC498D65D01061DC68D337 (void);
// 0x0000017A System.Boolean UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_HasDependencies()
extern void ResourceLocationBase_get_HasDependencies_m0CA2517DEE801CD55836C826FEC9909524DD0078 (void);
// 0x0000017B System.Object UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Data()
extern void ResourceLocationBase_get_Data_m90FD96E80811C52DDEFA865509295A956D75434F (void);
// 0x0000017C System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::set_Data(System.Object)
extern void ResourceLocationBase_set_Data_mED732C047B318CB886E722162EDB8822BBC84856 (void);
// 0x0000017D System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_PrimaryKey()
extern void ResourceLocationBase_get_PrimaryKey_m8E372A4ED7EF146A50E89A31ACCCBCC05327BF20 (void);
// 0x0000017E System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::set_PrimaryKey(System.String)
extern void ResourceLocationBase_set_PrimaryKey_m2F567328A25C460BC21855189974DE7DA1DA237E (void);
// 0x0000017F System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_DependencyHashCode()
extern void ResourceLocationBase_get_DependencyHashCode_mFB10FA3994BC92F90B34324C2E4CB71C5CF39D59 (void);
// 0x00000180 System.Type UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ResourceType()
extern void ResourceLocationBase_get_ResourceType_m7E0EC38ED4AB5FBCDDBA1AAAFEDB171EB06750D0 (void);
// 0x00000181 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::Hash(System.Type)
extern void ResourceLocationBase_Hash_m0B322CEB2CFAEE38C6991C97E2BD25897FB4A8AB (void);
// 0x00000182 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ToString()
extern void ResourceLocationBase_ToString_m8E238CA6431C9203C33F9DBB343C8FDFF75BF70C (void);
// 0x00000183 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::.ctor(System.String,System.String,System.String,System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation[])
extern void ResourceLocationBase__ctor_m39F2DD0986F808B99DB0034D3A7AA092CD61254F (void);
// 0x00000184 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ComputeDependencyHash()
extern void ResourceLocationBase_ComputeDependencyHash_mBD90CCAA4E286E789D843FA8A66D0E262B95F78F (void);
// 0x00000185 System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Graph()
extern void DiagnosticEvent_get_Graph_mCF17340DDBF35945343484DC79AA2D2D8A1AFCE3 (void);
// 0x00000186 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_ObjectId()
extern void DiagnosticEvent_get_ObjectId_m1333C6072B25E6A8047548B8A6501753C00BB282 (void);
// 0x00000187 System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_DisplayName()
extern void DiagnosticEvent_get_DisplayName_m3FC7DFC84D87E9797E61B679F6A91F61ED70320E (void);
// 0x00000188 System.Int32[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Dependencies()
extern void DiagnosticEvent_get_Dependencies_m5986D447D6C4B69FC83EFD4A9FA0533BCA254146 (void);
// 0x00000189 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Stream()
extern void DiagnosticEvent_get_Stream_mD67FE4C4C2240D064E2B7AB436388ED88FAF7EF8 (void);
// 0x0000018A System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Frame()
extern void DiagnosticEvent_get_Frame_mDA581C601CD74A5DB69B9601753E95B57DB516F7 (void);
// 0x0000018B System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Value()
extern void DiagnosticEvent_get_Value_mC9E381EEF0DD9BE55F1BDD56E8E9218EE5C9890A (void);
// 0x0000018C System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::.ctor(System.String,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void DiagnosticEvent__ctor_m39F2B2E3A31C9446E2F78E0ECEED641B7DA639B5 (void);
// 0x0000018D System.Byte[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Serialize()
extern void DiagnosticEvent_Serialize_mBFF1FE437AD85BDD0EB4530553B99CE46D655572 (void);
// 0x0000018E UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Deserialize(System.Byte[])
extern void DiagnosticEvent_Deserialize_m9ECB9B4281433F281C33ED3A7F46428B7127D29C (void);
// 0x0000018F System.Guid UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::get_PlayerConnectionGuid()
extern void DiagnosticEventCollectorSingleton_get_PlayerConnectionGuid_m1799DCC055E7993B6660BD897E3C529BAF9B4072 (void);
// 0x00000190 System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::GetGameObjectName()
extern void DiagnosticEventCollectorSingleton_GetGameObjectName_m182214CC37DFC410484A87F8A1010EE1B01ED3AC (void);
// 0x00000191 System.Boolean UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>,System.Boolean,System.Boolean)
extern void DiagnosticEventCollectorSingleton_RegisterEventHandler_m497A7D9C92D97E582B77AE99B8CFA4BBD4FE6E78 (void);
// 0x00000192 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollectorSingleton_RegisterEventHandler_mF058DAC7FDFA8764E8AAA070E24F7E0B9501FAB7 (void);
// 0x00000193 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::UnregisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollectorSingleton_UnregisterEventHandler_m271A76C77416EEF9C2687CD362FC318ABE1520BF (void);
// 0x00000194 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::PostEvent(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void DiagnosticEventCollectorSingleton_PostEvent_m8732893450E280DD70C22DBB0D7AC90F33E7820E (void);
// 0x00000195 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::Awake()
extern void DiagnosticEventCollectorSingleton_Awake_mA48C7FD054BC6C4A667EA731E6DCDB8023BB2DFE (void);
// 0x00000196 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::Update()
extern void DiagnosticEventCollectorSingleton_Update_mCC134B87BA4296B108C26D3F711759BDAEF238DC (void);
// 0x00000197 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::.ctor()
extern void DiagnosticEventCollectorSingleton__ctor_m5387CDDF73C7688E6BDA1010756BA2DD5790180C (void);
// 0x00000198 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton/<>c::.cctor()
extern void U3CU3Ec__cctor_m997FEEEF0466EF45F4241809D0A1E1E9A68DAD28 (void);
// 0x00000199 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton/<>c::.ctor()
extern void U3CU3Ec__ctor_mD78635A4C3F2DB70C1EC0B802FDA20912E22AF80 (void);
// 0x0000019A System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton/<>c::<RegisterEventHandler>b__8_0(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void U3CU3Ec_U3CRegisterEventHandlerU3Eb__8_0_mC8EA0E9F53319A2AF5D2347FEDA49C5D13060559 (void);
// 0x0000019B System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton/<>c::<Awake>b__11_0(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void U3CU3Ec_U3CAwakeU3Eb__11_0_mFAB321847E83F9F545317E54A070B21A05F252F0 (void);
// 0x0000019C System.Guid UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::get_PlayerConnectionGuid()
extern void DiagnosticEventCollector_get_PlayerConnectionGuid_mF58D6D0EFEB25249DDABB744E0F4F2BC6CACB085 (void);
// 0x0000019D UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::FindOrCreateGlobalInstance()
extern void DiagnosticEventCollector_FindOrCreateGlobalInstance_m606D0B93253933F64A4F126E952F4AACAD95F1A6 (void);
// 0x0000019E System.Boolean UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>,System.Boolean,System.Boolean)
extern void DiagnosticEventCollector_RegisterEventHandler_m7D8FE6ABC6C4E0ACA3760759B97555003246BAC3 (void);
// 0x0000019F System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::UnregisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollector_UnregisterEventHandler_m9E008CDA2BC88C4CCB3210AD0A35C10CFD457824 (void);
// 0x000001A0 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::PostEvent(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void DiagnosticEventCollector_PostEvent_mF5A13EE761B20F591FCF3F60E0845778142B0F80 (void);
// 0x000001A1 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::.ctor()
extern void DiagnosticEventCollector__ctor_mE4B01B8115DD645B51E76F3F5679CB1BD4B1D219 (void);
// 0x000001A2 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ICachable::get_Hash()
// 0x000001A3 System.Void UnityEngine.ResourceManagement.AsyncOperations.ICachable::set_Hash(System.Int32)
// 0x000001A4 System.Object UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetResultAsObject()
// 0x000001A5 System.Type UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_ResultType()
// 0x000001A6 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Version()
// 0x000001A7 System.String UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_DebugName()
// 0x000001A8 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::DecrementReferenceCount()
// 0x000001A9 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::IncrementReferenceCount()
// 0x000001AA System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_ReferenceCount()
// 0x000001AB System.Single UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_PercentComplete()
// 0x000001AC UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x000001AD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Status()
// 0x000001AE System.Exception UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_OperationException()
// 0x000001AF System.Boolean UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_IsDone()
// 0x000001B0 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x000001B1 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B2 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_IsRunning()
// 0x000001B3 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B4 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B5 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B6 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B7 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::InvokeCompletionEvent()
// 0x000001B8 System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Task()
// 0x000001B9 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x000001BA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Handle()
// 0x000001BB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Execute()
// 0x000001BC System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Destroy()
// 0x000001BD System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Progress()
// 0x000001BE System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DebugName()
// 0x000001BF System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C0 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Result()
// 0x000001C1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_Result(TObject)
// 0x000001C2 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Version()
// 0x000001C3 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_CompletedEventHasListeners()
// 0x000001C4 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DestroyedEventHasListeners()
// 0x000001C5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x000001C6 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_ReferenceCount()
// 0x000001C7 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_IsRunning()
// 0x000001C8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_IsRunning(System.Boolean)
// 0x000001C9 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::.ctor()
// 0x000001CA System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ShortenPath(System.String,System.Boolean)
// 0x000001CB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::IncrementReferenceCount()
// 0x000001CC System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::DecrementReferenceCount()
// 0x000001CD System.Threading.WaitHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_WaitHandle()
// 0x000001CE System.Threading.Tasks.Task`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Task()
// 0x000001CF System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Task()
// 0x000001D0 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ToString()
// 0x000001D1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::RegisterForDeferredCallbackEvent(System.Boolean)
// 0x000001D2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001D3 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001D4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001D5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001D6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001D7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001D8 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Status()
// 0x000001D9 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_OperationException()
// 0x000001DA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OperationException(System.Exception)
// 0x000001DB System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::MoveNext()
// 0x000001DC System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Reset()
// 0x000001DD System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Current()
// 0x000001DE System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_IsDone()
// 0x000001DF System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_PercentComplete()
// 0x000001E0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeCompletionEvent()
// 0x000001E1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Handle()
// 0x000001E2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UpdateCallback(System.Single)
// 0x000001E3 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.String)
// 0x000001E4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.String,System.Boolean)
// 0x000001E5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x000001E6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeExecute()
// 0x000001E7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E9 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001EA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001EB System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Version()
// 0x000001EC System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_ReferenceCount()
// 0x000001ED System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_PercentComplete()
// 0x000001EE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Status()
// 0x000001EF System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_OperationException()
// 0x000001F0 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_IsDone()
// 0x000001F1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Handle()
// 0x000001F2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x000001F3 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_DebugName()
// 0x000001F4 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetResultAsObject()
// 0x000001F5 System.Type UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_ResultType()
// 0x000001F6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001F7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.DecrementReferenceCount()
// 0x000001F8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.IncrementReferenceCount()
// 0x000001F9 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.InvokeCompletionEvent()
// 0x000001FA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x000001FB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ReleaseDependencies()
// 0x000001FC UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x000001FD UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x000001FE System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::<.ctor>b__34_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x000001FF System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass42_0::.ctor()
// 0x00000200 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass42_0::<get_Task>b__0(System.Object)
// 0x00000201 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass44_0::.ctor()
// 0x00000202 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass44_0::<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Task>b__0(System.Object)
// 0x00000203 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass55_0::.ctor()
// 0x00000204 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass55_0::<add_CompletedTypeless>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000205 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass56_0::.ctor()
// 0x00000206 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1/<>c__DisplayClass56_0::<remove_CompletedTypeless>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000207 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_LocationName()
// 0x00000208 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::set_LocationName(System.String)
// 0x00000209 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::op_Implicit(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x0000020A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>)
// 0x0000020B UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::GetDownloadStatus()
// 0x0000020C UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::InternalGetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x0000020D System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
// 0x0000020E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32)
// 0x0000020F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.String)
// 0x00000210 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32,System.String)
// 0x00000211 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Acquire()
// 0x00000212 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000213 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000214 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000215 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000216 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_DebugName()
// 0x00000217 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000218 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000219 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Equals(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x0000021A System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::GetHashCode()
// 0x0000021B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_InternalOp()
// 0x0000021C System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_IsDone()
// 0x0000021D System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::IsValid()
// 0x0000021E System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_OperationException()
// 0x0000021F System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_PercentComplete()
// 0x00000220 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_ReferenceCount()
// 0x00000221 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Release()
// 0x00000222 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Result()
// 0x00000223 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Status()
// 0x00000224 System.Threading.Tasks.Task`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Task()
// 0x00000225 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.get_Current()
// 0x00000226 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.MoveNext()
// 0x00000227 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.Reset()
// 0x00000228 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_LocationName()
extern void AsyncOperationHandle_get_LocationName_m28C16FB5752DBA9E9BC3481A2FEEEE8631A0879C (void);
// 0x00000229 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::set_LocationName(System.String)
extern void AsyncOperationHandle_set_LocationName_m84D55A8B0E3620FEBFDF4EF68681135CF54C9C3D (void);
// 0x0000022A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void AsyncOperationHandle__ctor_m0B9C35256EB8B705BC512EF9821F0734A318E4F7 (void);
// 0x0000022B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32)
extern void AsyncOperationHandle__ctor_mC21DA18C142F32C56786DBBC82CA86A4CA032701 (void);
// 0x0000022C System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.String)
extern void AsyncOperationHandle__ctor_m0D8F822B4B8C250F8AC60E230B2F6DCC1E4BB398 (void);
// 0x0000022D System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32,System.String)
extern void AsyncOperationHandle__ctor_mFDC8FCA6F2C2FA687BD56CE9544ED582D475D132 (void);
// 0x0000022E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Acquire()
extern void AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707 (void);
// 0x0000022F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Completed_m736B8F270CA7BD0D742477D59F5882D179E0E0C6 (void);
// 0x00000230 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Completed_m7EEA0C4ED68EB2E2A66F642882DDD0DD7A348F23 (void);
// 0x00000231 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Convert()
// 0x00000232 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Equals(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AsyncOperationHandle_Equals_mCAE66BFB37D7B0B71417D5895C9352F57D35C5E0 (void);
// 0x00000233 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_DebugName()
extern void AsyncOperationHandle_get_DebugName_m0698FEE2DD4D7EBDF815D04ED9EAFE02A6E4E5D6 (void);
// 0x00000234 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Destroyed_m13BB1F42E96BDACF63489DF8710CABE3E36107EC (void);
// 0x00000235 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Destroyed_mD2240BD43BE2C73A7865D3BDCC79431934026D83 (void);
// 0x00000236 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_GetDependencies_mFBA207AD01B44C329D78DD30530F755ED6D4B52B (void);
// 0x00000237 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetHashCode()
extern void AsyncOperationHandle_GetHashCode_m83FA3D51E02578AA34AAA4C08286A06332EE73DA (void);
// 0x00000238 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_InternalOp()
extern void AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B (void);
// 0x00000239 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_IsDone()
extern void AsyncOperationHandle_get_IsDone_m6A2917A1C796F97B37F12FAC994BD7F2A2103C74 (void);
// 0x0000023A System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::IsValid()
extern void AsyncOperationHandle_IsValid_mD2FD09414056E2CC3570BCFB1E4E676CFE3A6D5E (void);
// 0x0000023B System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_OperationException()
extern void AsyncOperationHandle_get_OperationException_mEE9849546A200CCFE5BEFE90DB9CC89743511006 (void);
// 0x0000023C System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_PercentComplete()
extern void AsyncOperationHandle_get_PercentComplete_m61CD06AEC4F584971D3C4B00523DF60C49BB370F (void);
// 0x0000023D UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetDownloadStatus()
extern void AsyncOperationHandle_GetDownloadStatus_m0C0F49BC92003068201C090DF8E7EB6B2E7167D4 (void);
// 0x0000023E UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::InternalGetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void AsyncOperationHandle_InternalGetDownloadStatus_m3AA15862A8CC2F4485FD157F5D64C1F96617963C (void);
// 0x0000023F System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_ReferenceCount()
extern void AsyncOperationHandle_get_ReferenceCount_mB2BA57B7F73269EF4AB3294EB6C86FBD03339731 (void);
// 0x00000240 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Release()
extern void AsyncOperationHandle_Release_m628A81301374CB506296D33A6A00B5DEBB5CCCDF (void);
// 0x00000241 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Result()
extern void AsyncOperationHandle_get_Result_m9371F734DC95198D64F6743BBC680B3F2EB02772 (void);
// 0x00000242 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Status()
extern void AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458 (void);
// 0x00000243 System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Task()
extern void AsyncOperationHandle_get_Task_m6E5707186EBDF86F40768E00B1DCD9E1B267109E (void);
// 0x00000244 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.get_Current()
extern void AsyncOperationHandle_System_Collections_IEnumerator_get_Current_mA090660989881BBBD052B6371961C96DDE2F0AF9 (void);
// 0x00000245 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.MoveNext()
extern void AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m826BA946D6D215742C9F83C8B99A63974E7B1CCF (void);
// 0x00000246 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.Reset()
extern void AsyncOperationHandle_System_Collections_IEnumerator_Reset_mF9FA74B53FBD69373E94B1FD2E41D6A1B4C06F11 (void);
// 0x00000247 System.Single UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus::get_Percent()
extern void DownloadStatus_get_Percent_mFDB729839F011232B236F7958E37953E2454BA0F (void);
// 0x00000248 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::.ctor()
extern void GroupOperation__ctor_m3EAB15B15FF218854F45B5F0174C2FCA75C62247 (void);
// 0x00000249 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::UnityEngine.ResourceManagement.AsyncOperations.ICachable.get_Hash()
extern void GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_get_Hash_mABCC641018173E2BBE94EC4A7DB5F8692612505B (void);
// 0x0000024A System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::UnityEngine.ResourceManagement.AsyncOperations.ICachable.set_Hash(System.Int32)
extern void GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_set_Hash_m05B9F21A48E3CD7C13A7B913D7BFD85591D47F70 (void);
// 0x0000024B System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependentOps()
extern void GroupOperation_GetDependentOps_m7C2A8933BD9562184400AF82518F54AA1B4C5C42 (void);
// 0x0000024C System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupOperation_GetDependencies_m1CF7564360E96CAE8CF86E07823C32DC2AE87CB4 (void);
// 0x0000024D System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::ReleaseDependencies()
extern void GroupOperation_ReleaseDependencies_m66F1D2CBDDDCA26C8512718341F4B12309D6A08D (void);
// 0x0000024E UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void GroupOperation_GetDownloadStatus_m4E6E076A47189294A6AA032C19B632BE24F2CE37 (void);
// 0x0000024F System.Boolean UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::DependenciesAreUnchanged(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupOperation_DependenciesAreUnchanged_m0022B84628A6480B5EA850C7389C96909FF49FF5 (void);
// 0x00000250 System.String UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::get_DebugName()
extern void GroupOperation_get_DebugName_m8B848F52E178DB9DAF8FC66537B8CB01611B156A (void);
// 0x00000251 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Execute()
extern void GroupOperation_Execute_m0E4CD4E4679BD341333E6A2A8AE0BC3C7D23344F (void);
// 0x00000252 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::CompleteIfDependenciesComplete()
extern void GroupOperation_CompleteIfDependenciesComplete_m77AD5B785A95F917B86D5C244D2193797EE888EA (void);
// 0x00000253 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Destroy()
extern void GroupOperation_Destroy_m9D51D92A5FF17ADF9C1BBB7EC8126B3326251E2D (void);
// 0x00000254 System.Single UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::get_Progress()
extern void GroupOperation_get_Progress_m9C3CD3DF3228BA94E03C198AD5CE9FC091EC6B73 (void);
// 0x00000255 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Init(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean,System.Boolean)
extern void GroupOperation_Init_m9125BD292475CA5A2F335C66EFAC4DBDD6BFC996 (void);
// 0x00000256 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Init(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,UnityEngine.ResourceManagement.AsyncOperations.GroupOperation/GroupOperationSettings)
extern void GroupOperation_Init_m43D4E19CB88D75CF0BA203C00CF6BD005DC4A043 (void);
// 0x00000257 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::OnOperationCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void GroupOperation_OnOperationCompleted_m14BC186F8D434432960795EF635F8A67A3A5780E (void);
// 0x00000258 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x00000259 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>,System.Boolean)
// 0x0000025A System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_ProvideHandleVersion()
// 0x0000025B UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_Location()
// 0x0000025C System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_DependencyCount()
// 0x0000025D System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x0000025E TDepObject UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependency(System.Int32)
// 0x0000025F System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::SetProgressCallback(System.Func`1<System.Single>)
// 0x00000260 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x00000261 System.Type UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_RequestedType()
// 0x00000262 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::SetDownloadProgressCallback(System.Func`1<UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus>)
// 0x00000263 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::UnityEngine.ResourceManagement.AsyncOperations.ICachable.get_Hash()
// 0x00000264 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::UnityEngine.ResourceManagement.AsyncOperations.ICachable.set_Hash(System.Int32)
// 0x00000265 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_ProvideHandleVersion()
// 0x00000266 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_Location()
// 0x00000267 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::SetDownloadProgressCallback(System.Func`1<UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus>)
// 0x00000268 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000269 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::.ctor()
// 0x0000026A System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000026B System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::ReleaseDependencies()
// 0x0000026C System.String UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_DebugName()
// 0x0000026D System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x0000026E System.Type UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_RequestedType()
// 0x0000026F System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_DependencyCount()
// 0x00000270 TDepObject UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependency(System.Int32)
// 0x00000271 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::SetProgressCallback(System.Func`1<System.Single>)
// 0x00000272 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x00000273 System.Single UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_Progress()
// 0x00000274 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Execute()
// 0x00000275 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x00000276 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>,System.Boolean)
// 0x00000277 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Destroy()
static Il2CppMethodPointer s_methodPointers[631] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoBehaviourCallbackHooks_add_OnUpdateDelegate_m87AED51B4FE2D79C20DAC3B8830776BB97F803D5,
	MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m01C7ECAD1953ED33B92C8FF5DE8A026A8CE3AE71,
	MonoBehaviourCallbackHooks_GetGameObjectName_m055C72395F199B2BBF29A85EBDD88070A5A79561,
	MonoBehaviourCallbackHooks_Update_mF37044CFC87FEC864949100ACD8DC11DD5740E38,
	MonoBehaviourCallbackHooks__ctor_mE5DBDE8B892D936E025E60EDDB435273BC905333,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceManager_get_ExceptionHandler_mE9398578AC49B638C0ADC70144676C30591BDED1,
	ResourceManager_set_ExceptionHandler_m93B2193AA6515D32E5BDAD7C5E9BA7A7E4049F14,
	ResourceManager_get_InternalIdTransformFunc_m3E8CBA63BA66D29B6893294B2DF8FD9EF0BD3D8A,
	ResourceManager_set_InternalIdTransformFunc_m24175C47AA27AEFDF392CC49749B25D8A01D071E,
	ResourceManager_TransformInternalId_m117A1A664540BC9546EC5CDBD1B36A25FE0BC889,
	ResourceManager_get_OperationCacheCount_m8BCBBD8633D220D4F1B281562211C26653C37951,
	ResourceManager_get_InstanceOperationCount_m57CEED0C028F8D06B5A9E05AB6AAE01FB5038D3F,
	ResourceManager_AddUpdateReceiver_m950EAA79EC44CCF02632A799050761670DEC319A,
	ResourceManager_RemoveUpdateReciever_m9FD7EF0CEE2313A78484B81FE0EEF97A7BE0B502,
	ResourceManager_get_Allocator_m617C32C69B3AF34BA88127E5EE31B8B2731AE685,
	ResourceManager_set_Allocator_m76760FFBBACB5A3C422D88498A57E2688D44DCB8,
	ResourceManager_get_ResourceProviders_m66EA708F4B1478C79D4C90E53F68C8A3DC0FB5DA,
	ResourceManager_get_CertificateHandlerInstance_m2E6389B8CA321D6850C1EF792F7F30CB85E6AD53,
	ResourceManager_set_CertificateHandlerInstance_m40FDEA12E0566292641236BD511236193B31BB0E,
	ResourceManager__ctor_m72F4278E1B2667B12CA84961A22A799356B2C6A5,
	ResourceManager_OnObjectAdded_m09BD1FC6D74DA0D0DEE4FC4E7734DC38BAE43CA3,
	ResourceManager_OnObjectRemoved_m7C0E125D80AF720707652982FF4E63E40DE8539E,
	ResourceManager_RegisterForCallbacks_mD497DF3FD15D025444B1AB3F33542977EA1DEC11,
	ResourceManager_ClearDiagnosticsCallback_m55CA640ED4CDFEB2ED720BC81402A3855BB44E71,
	ResourceManager_ClearDiagnosticCallbacks_mAF83E29B383519A87759E03665B5458D43CD36A0,
	ResourceManager_UnregisterDiagnosticCallback_mAD93A86A5BB291A0A2EAC951730BBFE56E9AA640,
	ResourceManager_RegisterDiagnosticCallback_m47485FFB63AA58A3659A97DA75191B47022659E9,
	ResourceManager_RegisterDiagnosticCallback_mDE7E58521FD6B46F0472B310BE672C7364184448,
	ResourceManager_PostDiagnosticEvent_m30A67E5310B6A55D34BD8BAB50365A63BB4C2800,
	ResourceManager_GetResourceProvider_m5F732649805DCCFC152AE89945CCEEB8D4876B5A,
	ResourceManager_GetDefaultTypeForLocation_m4FDDE992FD2950D5BDBDB4AB34E5B150D37760C4,
	ResourceManager_CalculateLocationsHash_m66A19883C1E4299C05C0C997B461C1DA9D3A3377,
	ResourceManager_ProvideResource_mC630BFAED57BCFA4BC40EE626FD2760772C24D6F,
	NULL,
	NULL,
	ResourceManager_StartOperation_m5F3A0530300FE62FCCA0AA55ADB4AF825AC0E630,
	ResourceManager_OnInstanceOperationDestroy_m11E9CAD345CBBD8DCC5C984C91BB228CBFD2177F,
	ResourceManager_OnOperationDestroyNonCached_m8DD16F401831146BAF28C5295853D02075C4B922,
	ResourceManager_OnOperationDestroyCached_m1AC04D6B5F3E74D19F6A1CC34613431727E974A8,
	NULL,
	ResourceManager_AddOperationToCache_m17A4FF6A869D9ED103A4169B9C168CEBFFC835DC,
	ResourceManager_RemoveOperationFromCache_mE0343EF5C4A23A28D4A145E9DB20FE7463436768,
	ResourceManager_IsOperationCached_mBDB18814D56394E9909A4404DB14C549F60E8AA0,
	ResourceManager_CachedOperationCount_mADC8BA688795B826520977167476B649BD26291D,
	NULL,
	NULL,
	ResourceManager_Release_mE62F1F2457C5B85A8D41ACA9872D0F5CEE50EE03,
	ResourceManager_Acquire_mC0B02D11591D5E0B4AE5826ED6A345D48AF89043,
	ResourceManager_AcquireGroupOpFromCache_m1D1F014837755ACC0F1E1D99487608E454F007A0,
	NULL,
	NULL,
	ResourceManager_CreateGenericGroupOperation_mB729E00A7923C66BD7F66AF04C49785E3348E947,
	ResourceManager_ProvideResourceGroupCached_mA9021EA82D52FE9FE1589DBF3F3D169C57B4ECDC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceManager_ProvideScene_mAF97EC94C8AD654C10DA6D905110B21F640BDCA5,
	ResourceManager_ReleaseScene_m459CB8CAAB851FA3495F881AEFAD1C3FF29106FA,
	ResourceManager_ProvideInstance_m9C1635807FC0ED58FB88BEF2B803BC9C1AAD14F3,
	ResourceManager_CleanupSceneInstances_m9BD22D197EE156F52DD1D5FAB1282D4E2113400D,
	ResourceManager_ExecuteDeferredCallbacks_m7839A017C2185C347BB428B84D89D08DC91B3D64,
	ResourceManager_RegisterForDeferredCallback_m63949A3E890C4387FE17491AD5B54AF35ED02E8F,
	ResourceManager_Update_m08AF2DABA8576AAA841CCBFDC7A025C00836E33C,
	ResourceManager_Dispose_mEFE92B8273D202FCF7A4DAF39D8039257B7AD0B1,
	ResourceManager__cctor_mA7B6D4B17DACE5BD1CCAA35357E9F1EBB3CC9000,
	ResourceManager_U3C_ctorU3Eb__45_0_m8DAE9BBA3A515ACAB8A018265F816E4F45C5B3C7,
	DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD,
	DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E,
	DiagnosticEventContext_get_EventValue_m47DD1F4C4E14042B9B710D82AE04092C1F184F35,
	DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7,
	DiagnosticEventContext_get_Context_mE8E291342E3B9293E5AEDE07D44F3BC9E0CB7840,
	DiagnosticEventContext_get_Error_mE66E65391D117531702CD7EBAE58AD10A9C61B1F,
	DiagnosticEventContext__ctor_m0B5C04CEA5ACB1D89AD8A3005963A97DECC84979,
	NULL,
	NULL,
	NULL,
	NULL,
	InstanceOperation_Init_m8F3175B8D48645B5B1B5A7F5680EB2B42AEEE0AF,
	InstanceOperation_GetDownloadStatus_mA34529318D816B4BC07F0A35EFC4A572D317233C,
	InstanceOperation_GetDependencies_m25FF504226BFEE33AA8C25FBDFC49A69DB2FFE82,
	InstanceOperation_get_DebugName_mB7397923A863B8762D7EB956FAFCA3D5E7A509F6,
	InstanceOperation_InstanceScene_m6AF4F1151492C24B07C214A98DBAB5CBE64E3DD6,
	InstanceOperation_Destroy_m97EA48B0B2D6D19E20628CB60B6B9CE2A77C428A,
	InstanceOperation_get_Progress_m7F8FB815DC11AFD0B8DA7F73E36F80696DF15DF8,
	InstanceOperation_Execute_m4BFCA35557963C12E209810A6EE278BD2E1117FA,
	InstanceOperation__ctor_mEF3B832F2785E2DA33B74AA45E69DE4C423F0433,
	NULL,
	NULL,
	NULL,
	NULL,
	WebRequestQueueOperation_get_IsDone_mC3E8299EB4F2137D60A4C2A5F33392D9D545BAAA,
	WebRequestQueueOperation__ctor_mEEB70D014E81E5CE7294B0813DC576E94BB25C55,
	WebRequestQueueOperation_Complete_m6D2F3AA5740D67CAE8C6DB4FE4BA542FFA451650,
	WebRequestQueue_SetMaxConcurrentRequests_mEB8F9C16DEC65629AE432FC0561478514185BF6D,
	WebRequestQueue_QueueRequest_m54F1A56E5082365292141AC67CEFEBEEA52F04AE,
	WebRequestQueue_OnWebAsyncOpComplete_m17C30059501F106DF7BFB9F227F7E5DAD800900D,
	WebRequestQueue__cctor_mD23AA2F5F8B4D8BF629334F3C3A7B8E9715F3D1B,
	ResourceManagerException__ctor_m01391C459974ED2BC4FC9D49A5EA5F1E43D31AD6,
	ResourceManagerException__ctor_m8169F1FC9FA779C72056110DE66646F51B9CFDC8,
	ResourceManagerException__ctor_m86948C7C5AE7D082B0656AA751D9CAA7BEBDAA6C,
	ResourceManagerException__ctor_m521E0FC24584DE7F32D8E8E52525A18C70EC1EAC,
	UnknownResourceProviderException_get_Location_mC37FB4E0339053A4F3C5DFDEFAC15F8B4E1A7CA6,
	UnknownResourceProviderException_set_Location_mC68D88A08BAD8EC90EC965C7DF5B0D019AA8855D,
	UnknownResourceProviderException__ctor_mE46E123397D4F657424D013093385B7BA9611815,
	UnknownResourceProviderException__ctor_m69A301E51BA3A08D097A5B7570AB1B9BEDEBB205,
	UnknownResourceProviderException__ctor_mE26E8FC70B59AF0F9A9E8A8249B3CDF0E794A9FB,
	UnknownResourceProviderException__ctor_m9A851459EA94ED9A7F8E1F4522C4E6470949D37D,
	UnknownResourceProviderException__ctor_m018E494ED54533C65EC6461C5BFA1DC26557D4A7,
	UnknownResourceProviderException_get_Message_mEDAFB46A1DAED56B03BD9AC94CE53E9052DE0E41,
	UnknownResourceProviderException_ToString_mEF4042034C559688A612FF63DC72B5E0450A6CEF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DelayedActionManager_GetNode_m6E89B600D763BA70B4DB26528AF8D170B234FB8A,
	DelayedActionManager_Clear_m2027FDCBDC891F654C104C98EC1991C92BFF9C3E,
	DelayedActionManager_DestroyWhenComplete_m012CE6FCBDD8B238DEA779002FA400006B6F378F,
	DelayedActionManager_AddAction_mF8F65EE6C2F4EACC027E1E1FB5D7FE00E5639CF5,
	DelayedActionManager_AddActionInternal_m152580083979E12ADC9F1F2D6628D4A20D373420,
	DelayedActionManager_get_IsActive_mD85795ACE4EDF386CBC7A5A11EE2EC80FF4D9584,
	DelayedActionManager_Wait_mB71946F645806E69AF4348ED0104AF0BC92E7730,
	DelayedActionManager_LateUpdate_m17A22132BABB6873D12730B51D08EEEED122335F,
	DelayedActionManager_InternalLateUpdate_mE1B94E403B75E83D07D5A537C43BB582D45314C0,
	DelayedActionManager_OnApplicationQuit_m650F69C5F7F85477DFB8931DBD91589266709EE6,
	DelayedActionManager__ctor_m1992ABABC7393554D05674AFAE25D8BA1BD56A9E,
	DelegateInfo__ctor_mD79A58037BB825D5FA49F0D35C072FC85EA6EDCF,
	DelegateInfo_get_InvocationTime_m2DFFB1182B78A216D6210CB939DA4DBC59E7DF59,
	DelegateInfo_set_InvocationTime_m3472B095E8176F30FCED254739CCF797E8121E46,
	DelegateInfo_ToString_m0FC72F240C31351642A1401F70AEC75127BB0635,
	DelegateInfo_Invoke_m9853B80CDAD912DC9E644027D0B8B8AA03B362E9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DefaultAllocationStrategy_New_mC842EAFAF088441610BB59880015DA917A407F65,
	DefaultAllocationStrategy_Release_mF5CF80B8C407DAB00382F1249DA26132925766F0,
	DefaultAllocationStrategy__ctor_mD43247AE516C9B2D7C8B3ED2F31399537E468014,
	LRUCacheAllocationStrategy__ctor_mE0BA2D685813D042B3515DF86B5ED5566652861F,
	LRUCacheAllocationStrategy_GetPool_mD773863C92E277581464529F161B016D042733BD,
	LRUCacheAllocationStrategy_ReleasePool_mB58B3D361C98E1E4C6043F73661F624F3005F1C8,
	LRUCacheAllocationStrategy_New_m061E645DCA19415B1289FE2259DCBD404F1BF0AF,
	LRUCacheAllocationStrategy_Release_m2530B50A0F910642E5F147686EEFD90ECA268255,
	SerializedTypeRestrictionAttribute__ctor_m1A85236760A4446B2E8916F9B83BA86C9E4483BE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializedType_get_AssemblyName_mB4D8E282E3B22D60B88D0B612E1DD2706450C6FA,
	SerializedType_get_ClassName_m58CBCA1680BC7B435A66D60D6E49FBE3E2FF03FD,
	SerializedType_ToString_mE7F91178BCF528D8D66A51CAED1BE91B046C4486,
	SerializedType_get_Value_mA75D04829016315F5CB5EBD10CD4A44C0754012C,
	SerializedType_set_Value_m45104AAC40C5238AF758D2D552B92E52FF705092,
	SerializedType_get_ValueChanged_mA3E9901574336A20A27D2A0DED75B476C02E261E,
	SerializedType_set_ValueChanged_mFEA6D354142B82599C513F737E679AB4A8C08E9F,
	ObjectInitializationData_get_Id_m769EFD3C4475326DDFC86C9E9255973AB4B84561,
	ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532,
	ObjectInitializationData_get_Data_mA167B62E4FB7ADE8409B4AB74D534EE18BBDA7FC,
	ObjectInitializationData_ToString_m31940BC41095BD7336F705D0BF27B827BE91BBBC,
	NULL,
	ObjectInitializationData_GetAsyncInitHandle_m0186D6321B14C9AC25E3F8196220ABBE96A965F0,
	ResourceManagerConfig_ExtractKeyAndSubKey_mB1352EBE9B7434F2F0F8A092CFDF64C162FFD57C,
	ResourceManagerConfig_IsPathRemote_m2C5EA552B346A09472FD4D9A797E1A2C80F1DBAA,
	ResourceManagerConfig_ShouldPathUseWebRequest_m965D90CDBCB1B2960449029203B6CD7B1F787060,
	ResourceManagerConfig_CreateArrayResult_m4915DA72A71401B4AA6B23DABEE79F5EB87E5115,
	NULL,
	ResourceManagerConfig_CreateListResult_m1D92E3CF8E1C7D105A18BDAC71FCFC8EEDF7D03D,
	NULL,
	NULL,
	NULL,
	AssetBundleRequestOptions_get_Hash_m4E09B8CEBD441A4BBCB5E938C506581B5B21B9E9,
	AssetBundleRequestOptions_set_Hash_m316DB9107A1B7D38F665646C8A6393F907A30578,
	AssetBundleRequestOptions_get_Crc_m2045B390D2F29C985C4B6AA1C7B62BC8F412BE70,
	AssetBundleRequestOptions_set_Crc_mC97B6149AA2CF428CCB89E81C50DB28D25BF4A28,
	AssetBundleRequestOptions_get_Timeout_m316EA8799F388E2CD6B4D8B7EFE1FB0644E27482,
	AssetBundleRequestOptions_set_Timeout_m629613CA143FED7DD52CD18D5B5D136C31854369,
	AssetBundleRequestOptions_get_ChunkedTransfer_m01C2B585734CDE057E74C134A3A5B88719BBCFA6,
	AssetBundleRequestOptions_set_ChunkedTransfer_m38F1FE80CB53AED2187F9BED913EB53DA6D3C929,
	AssetBundleRequestOptions_get_RedirectLimit_m8C4C7E38FF49194DA5A40D1FB1C4BB1618FFA7D3,
	AssetBundleRequestOptions_set_RedirectLimit_mCF37BDC8B611CF76BD33E38E7C6A41009793B4DB,
	AssetBundleRequestOptions_get_RetryCount_m50E1494EB797D875FAEAB3AED7D2B29AEAF63382,
	AssetBundleRequestOptions_set_RetryCount_m9ABB7EB9BC0D7511685BFC05B24DC2F0A0CA57B2,
	AssetBundleRequestOptions_get_BundleName_mC59B8E79B08BB3408A763C9B8D8B968D19E71FAA,
	AssetBundleRequestOptions_set_BundleName_m38C6621464EB9D69CA387959C68C899AF38FB315,
	AssetBundleRequestOptions_get_BundleSize_m837CABABA41D15168137DDB77C235D1A5036B6A3,
	AssetBundleRequestOptions_set_BundleSize_m2A790CEA18CA25BECAFEE784991F8AB66FEBC901,
	AssetBundleRequestOptions_get_UseCrcForCachedBundle_m1C00D926365DF41F20DC6F2908F0128EDE6888A1,
	AssetBundleRequestOptions_set_UseCrcForCachedBundle_mDD1ECC570ED4D82E4F44AFBE2B3EF958BDDA3F7F,
	AssetBundleRequestOptions_ComputeSize_m090B8ECB2F40F173679FC235A9C3DA6915427D6E,
	AssetBundleRequestOptions__ctor_mECFC54613FCFAD2997F2616562DA9EAA796A2199,
	AssetBundleResource_CreateWebRequest_m008113D1236E539144C558988C2D6F9D71881962,
	AssetBundleResource_PercentComplete_mED6167B98761953F9D21BCCCBD2B49296FA69D67,
	AssetBundleResource_GetDownloadStatus_mD477BF4665169B01680B78FCB57CE9095BEF6F13,
	AssetBundleResource_GetAssetBundle_mEDCC866A7AF1A532F40B40B821DD457AC73FB8D0,
	AssetBundleResource_Start_mC19F8F354525DC25A262C5054BB68380BAA7A7C0,
	AssetBundleResource_BeginOperation_mB48A167F91987D1255038260B8D95381D9CE3EBB,
	AssetBundleResource_LocalRequestOperationCompleted_m9CD3F70C0A35A95F298C4ACE444EDF6D1BBFFDBC,
	AssetBundleResource_WebRequestOperationCompleted_m9F2F0EC5AE2BFA16CF34C1CA1407E7C4E04A9B9F,
	AssetBundleResource_Unload_m8A7BDC23B2F5D403F5856771C45A56FC94EFB80F,
	AssetBundleResource__ctor_m252A70EBBA6B3827A634F8621F302960820B752F,
	AssetBundleResource_U3CBeginOperationU3Eb__13_0_m3526234E064ACDB8E8B9896754280EB0B4ADA028,
	AssetBundleProvider_Provide_m7261CC3C80ECC32D8356DEF62234754764DF96B3,
	AssetBundleProvider_GetDefaultType_mCDB0B3B3E180FE1873A48A781F879AFB4DD97531,
	AssetBundleProvider_Release_m1D7D55505161B85DE77D4BFE84E9D94B2F7EE332,
	AssetBundleProvider__ctor_m317B2BC098303C8CBAB7760BE13A78F526AA24A9,
	AtlasSpriteProvider_Provide_m5C5B89EFDB74D296ADEA5C901306AAE25D593213,
	AtlasSpriteProvider__ctor_m33DD706E56EC96EF28DF76C13C034DD5A6C34F1C,
	BundledAssetProvider_Provide_mAE2649FB77C2D9450265EF65772E4483039F2F4D,
	BundledAssetProvider__ctor_mDC8402C44066C088B1D8496D5C758434CF5FF003,
	InternalOp_LoadBundleFromDependecies_m21F705B1F2719B8F2FAA8974B0179EA4093F2DBA,
	InternalOp_Start_m89777E539E2163EEA568A672200C2BD3F5419926,
	InternalOp_ActionComplete_m3B9C2BACE76D3A41B6DEE145A42EC3548BC8B0D3,
	InternalOp_ProgressCallback_mCBD860D0D7B1E49FB2297546D7D66C6A51BB8012,
	InternalOp__ctor_mFC858704B9747EEFA53E021392B2900BCFCD27D6,
	InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC,
	InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012,
	InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621,
	InstantiationParameters_get_InstantiateInWorldPosition_mD022156583A3F9F0DCFF0DAEA75F571E4BBE25B8,
	InstantiationParameters_get_SetPositionRotation_m1A4A574A3025F0DCE609D8F49F6071FA57904F9C,
	InstantiationParameters__ctor_m32A54AA524BFE9CCF5D2EA04CD665297FFA1C0E6,
	InstantiationParameters__ctor_mDCF85728CF9BB36A6EE0930C8B68C10810FE60AA,
	NULL,
	NULL,
	NULL,
	ProvideHandle__ctor_mEBB5A836A4CDB0B4FB938F85050D865695516097,
	ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F,
	ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76,
	ProvideHandle_get_Type_m52E919AFDB11C39D84549BA4E05F730F14BC528B,
	ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978,
	ProvideHandle_get_DependencyCount_m1C44CB1B419CE21A00D6E4275B01147F978FC1AA,
	NULL,
	ProvideHandle_GetDependencies_mF82C08A32FF506D98985684CF1E7E42EF4326653,
	ProvideHandle_SetProgressCallback_m762D5F97C1CAAC2D0431DE3C8C22F6FB405FC77E,
	ProvideHandle_SetDownloadProgressCallbacks_mA4D6FFA06016A567D962BE5AF19F1CAF3C5B5C7C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3,
	SceneInstance_set_Scene_m9B163169E348E68FF15DFDEF76E25815AF6D063D,
	SceneInstance_Activate_mA85392F02BF420CD5D4ACD8BA84CCE4A577F7466,
	SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3,
	SceneInstance_GetHashCode_mFB6154BAA8F8283352C273727C0611405EDC963D,
	SceneInstance_Equals_mF184704D845C4F37D594CE251E7DBD449B5E7F38,
	NULL,
	NULL,
	InstanceProvider_ProvideInstance_m530D2258769CC2B97647ECFBCF34659BD93C2C6B,
	InstanceProvider_ReleaseInstance_m28F42006EDEA00DCAF391F17303EDB0EE83199E6,
	InstanceProvider__ctor_m1D163567C544AA99FE68262DEBAC0FD3EE7A9B69,
	JsonAssetProvider_Convert_m5E81612E9A2074C28456F014F751C07715CB7424,
	JsonAssetProvider__ctor_m414B5D8DE089729BB8C2E80849FB109D17462448,
	LegacyResourcesProvider_Provide_m971E9C71D39B404FFDAA4C6F9B9487A590609211,
	LegacyResourcesProvider_Release_mB20852B4A94D65940F18484F00767408CD88FE03,
	LegacyResourcesProvider__ctor_mCC36862065E722B44749F3856D01F90CDB83F72C,
	InternalOp_Start_mDB6E979C502EFB3276D89619217754D57AC48805,
	InternalOp_AsyncOperationCompleted_m30C8DF6B9601A1511465548C371CDA2E2FB3F33C,
	InternalOp_PercentComplete_mFBA923ACE0098674D6F6C9AD7A68640180572DF3,
	InternalOp__ctor_m098F6C379EFFB213B8ADC3B5CE14A63AB7A37539,
	ResourceProviderBase_get_ProviderId_m6184BE49E537EF0650F299F642B1E1C0E0D00996,
	ResourceProviderBase_Initialize_mA8285F2F9D6AB90FBE07229C852D29D12259694D,
	ResourceProviderBase_CanProvide_m76642A78907B6CD37706CA93E660ABCEF9F87C85,
	ResourceProviderBase_ToString_mD650C694955FB3B6635377B8DFF5E4D707B2E342,
	ResourceProviderBase_Release_m46D9D39E8F99B2A9D8FA478D1BA6D051B169EBA1,
	ResourceProviderBase_GetDefaultType_mBA1FAA374F596F96BCD4A6F1C76839356A3A17F0,
	NULL,
	ResourceProviderBase_InitializeAsync_m4B3BF412B1D41A03BA8D30918FE7A7C4006E4F1F,
	ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mFDEC1DF913DFD712F248FAD2A36DB610F7580F25,
	ResourceProviderBase__ctor_mC9B337B8D57A9C39902D92A9F8B5A06057C340F7,
	BaseInitAsyncOp_Init_m124642C8428A2986C50FF8B9CD624012F4F29938,
	BaseInitAsyncOp_Execute_m4C62815A957FE145FFB26B6F0B7B9AC734D133A9,
	BaseInitAsyncOp__ctor_mFE9165E83F0C5435643766CFAEAB0AA631FF495B,
	U3CU3Ec__DisplayClass10_0__ctor_m9A7D2B62835C38A11D92570665937E0C5854BE52,
	U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m1B9EB201931A439A29FD64339511621980DBF9C0,
	SceneProvider_ProvideScene_m4F70BD611D12B7822B5770066AF144FB577C5583,
	SceneProvider_ReleaseScene_m1A7A7B799DFEF64C68E7101546579CC907E0048A,
	SceneProvider__ctor_mADE65924761AD6CA8132FA0980EEB3480701A225,
	SceneOp__ctor_m9D33D33E529B1290A79E3C11BF62B7348BB09D36,
	SceneOp_GetDownloadStatus_m1D5B073EE18406450DA223BBDF851D2728162138,
	SceneOp_Init_m51770548CC4207ED9FFBA41BF1179D9DB11A0571,
	SceneOp_GetDependencies_m505E528E6A74BE2234E9835AE451F59689BDAF3D,
	SceneOp_get_DebugName_m9317CCE448A96B710DF79D7722989A51D7802FA5,
	SceneOp_Execute_mAC2090B2EA33B97E5AAD2E44F3880E7B21A9199C,
	SceneOp_InternalLoadScene_m2CAA571373803372A9429801BE7E78BE193A06BF,
	SceneOp_InternalLoad_mA7B170134422CBCD64923BCE1CCDDA7F98B7C610,
	SceneOp_Destroy_mE8547B5022F4751624DE1C743F8D3B36B9A0CEDB,
	SceneOp_get_Progress_m80BCE300B854BAD74C7AA5AD5248D18E72C05B91,
	SceneOp_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m6639AFC516698E0D44E6F996112F711DAA446B0C,
	UnloadSceneOp_Init_mFFCFC26F6DA81BCA74A3BBA3C3ECF2935D3837F5,
	UnloadSceneOp_Execute_m7BFBA4CC6B880638D3A7C50842B736EBD2156A18,
	UnloadSceneOp_UnloadSceneCompleted_m10F816AE19F3266AC6AC5C80B8169FACA8E272BA,
	UnloadSceneOp_get_Progress_m73550503E47D08122BD59EDAAA4F89B9768C7C34,
	UnloadSceneOp__ctor_m9FFCFD1D98D7DDCE47CC1E1390B6D2306919C699,
	TextDataProvider_get_IgnoreFailures_m9B008F37CE1A3B2AC0A7A9B929684D0F2CB761DD,
	TextDataProvider_set_IgnoreFailures_mC87BF0A8E1FD80D131A49F6F36946DD1BB28F703,
	TextDataProvider_Convert_mE6997E7A73F9D4D4B1D479E112D6D567356A51C8,
	TextDataProvider_Provide_m6EBF00ECB1FC60B196D4810B7397FE57CC95B302,
	TextDataProvider__ctor_m2139CF91C3D15A52F07BF06758DAE372B6E869C8,
	InternalOp_GetPercentComplete_mCBA1C79FA03221A9719DE8F2D167E601ED38F48F,
	InternalOp_Start_m451A235CFF51EC2F1A4572380C16F99186F78C62,
	InternalOp_RequestOperation_completed_mE369E59BFAD959075E24A08B2C8BC161AB00FCDA,
	InternalOp__ctor_m89FAB3AFC1702B9DE88A8CC8DF9BFA0640EFCA5D,
	InternalOp_U3CStartU3Eb__6_0_mA6BFCF95D75BB17BFFB8E7AE463DB6897F6B5D3F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceLocationBase_get_InternalId_m48AE4FE26C6020D0379C5EA2D0C78BE65EB5321A,
	ResourceLocationBase_get_ProviderId_m0FE503A1563C2043448041496BFE6C8DEB0CD17A,
	ResourceLocationBase_get_Dependencies_mD2A7F2814C27932160FC498D65D01061DC68D337,
	ResourceLocationBase_get_HasDependencies_m0CA2517DEE801CD55836C826FEC9909524DD0078,
	ResourceLocationBase_get_Data_m90FD96E80811C52DDEFA865509295A956D75434F,
	ResourceLocationBase_set_Data_mED732C047B318CB886E722162EDB8822BBC84856,
	ResourceLocationBase_get_PrimaryKey_m8E372A4ED7EF146A50E89A31ACCCBCC05327BF20,
	ResourceLocationBase_set_PrimaryKey_m2F567328A25C460BC21855189974DE7DA1DA237E,
	ResourceLocationBase_get_DependencyHashCode_mFB10FA3994BC92F90B34324C2E4CB71C5CF39D59,
	ResourceLocationBase_get_ResourceType_m7E0EC38ED4AB5FBCDDBA1AAAFEDB171EB06750D0,
	ResourceLocationBase_Hash_m0B322CEB2CFAEE38C6991C97E2BD25897FB4A8AB,
	ResourceLocationBase_ToString_m8E238CA6431C9203C33F9DBB343C8FDFF75BF70C,
	ResourceLocationBase__ctor_m39F2DD0986F808B99DB0034D3A7AA092CD61254F,
	ResourceLocationBase_ComputeDependencyHash_mBD90CCAA4E286E789D843FA8A66D0E262B95F78F,
	DiagnosticEvent_get_Graph_mCF17340DDBF35945343484DC79AA2D2D8A1AFCE3,
	DiagnosticEvent_get_ObjectId_m1333C6072B25E6A8047548B8A6501753C00BB282,
	DiagnosticEvent_get_DisplayName_m3FC7DFC84D87E9797E61B679F6A91F61ED70320E,
	DiagnosticEvent_get_Dependencies_m5986D447D6C4B69FC83EFD4A9FA0533BCA254146,
	DiagnosticEvent_get_Stream_mD67FE4C4C2240D064E2B7AB436388ED88FAF7EF8,
	DiagnosticEvent_get_Frame_mDA581C601CD74A5DB69B9601753E95B57DB516F7,
	DiagnosticEvent_get_Value_mC9E381EEF0DD9BE55F1BDD56E8E9218EE5C9890A,
	DiagnosticEvent__ctor_m39F2B2E3A31C9446E2F78E0ECEED641B7DA639B5,
	DiagnosticEvent_Serialize_mBFF1FE437AD85BDD0EB4530553B99CE46D655572,
	DiagnosticEvent_Deserialize_m9ECB9B4281433F281C33ED3A7F46428B7127D29C,
	DiagnosticEventCollectorSingleton_get_PlayerConnectionGuid_m1799DCC055E7993B6660BD897E3C529BAF9B4072,
	DiagnosticEventCollectorSingleton_GetGameObjectName_m182214CC37DFC410484A87F8A1010EE1B01ED3AC,
	DiagnosticEventCollectorSingleton_RegisterEventHandler_m497A7D9C92D97E582B77AE99B8CFA4BBD4FE6E78,
	DiagnosticEventCollectorSingleton_RegisterEventHandler_mF058DAC7FDFA8764E8AAA070E24F7E0B9501FAB7,
	DiagnosticEventCollectorSingleton_UnregisterEventHandler_m271A76C77416EEF9C2687CD362FC318ABE1520BF,
	DiagnosticEventCollectorSingleton_PostEvent_m8732893450E280DD70C22DBB0D7AC90F33E7820E,
	DiagnosticEventCollectorSingleton_Awake_mA48C7FD054BC6C4A667EA731E6DCDB8023BB2DFE,
	DiagnosticEventCollectorSingleton_Update_mCC134B87BA4296B108C26D3F711759BDAEF238DC,
	DiagnosticEventCollectorSingleton__ctor_m5387CDDF73C7688E6BDA1010756BA2DD5790180C,
	U3CU3Ec__cctor_m997FEEEF0466EF45F4241809D0A1E1E9A68DAD28,
	U3CU3Ec__ctor_mD78635A4C3F2DB70C1EC0B802FDA20912E22AF80,
	U3CU3Ec_U3CRegisterEventHandlerU3Eb__8_0_mC8EA0E9F53319A2AF5D2347FEDA49C5D13060559,
	U3CU3Ec_U3CAwakeU3Eb__11_0_mFAB321847E83F9F545317E54A070B21A05F252F0,
	DiagnosticEventCollector_get_PlayerConnectionGuid_mF58D6D0EFEB25249DDABB744E0F4F2BC6CACB085,
	DiagnosticEventCollector_FindOrCreateGlobalInstance_m606D0B93253933F64A4F126E952F4AACAD95F1A6,
	DiagnosticEventCollector_RegisterEventHandler_m7D8FE6ABC6C4E0ACA3760759B97555003246BAC3,
	DiagnosticEventCollector_UnregisterEventHandler_m9E008CDA2BC88C4CCB3210AD0A35C10CFD457824,
	DiagnosticEventCollector_PostEvent_mF5A13EE761B20F591FCF3F60E0845778142B0F80,
	DiagnosticEventCollector__ctor_mE4B01B8115DD645B51E76F3F5679CB1BD4B1D219,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AsyncOperationHandle_get_LocationName_m28C16FB5752DBA9E9BC3481A2FEEEE8631A0879C,
	AsyncOperationHandle_set_LocationName_m84D55A8B0E3620FEBFDF4EF68681135CF54C9C3D,
	AsyncOperationHandle__ctor_m0B9C35256EB8B705BC512EF9821F0734A318E4F7,
	AsyncOperationHandle__ctor_mC21DA18C142F32C56786DBBC82CA86A4CA032701,
	AsyncOperationHandle__ctor_m0D8F822B4B8C250F8AC60E230B2F6DCC1E4BB398,
	AsyncOperationHandle__ctor_mFDC8FCA6F2C2FA687BD56CE9544ED582D475D132,
	AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707,
	AsyncOperationHandle_add_Completed_m736B8F270CA7BD0D742477D59F5882D179E0E0C6,
	AsyncOperationHandle_remove_Completed_m7EEA0C4ED68EB2E2A66F642882DDD0DD7A348F23,
	NULL,
	AsyncOperationHandle_Equals_mCAE66BFB37D7B0B71417D5895C9352F57D35C5E0,
	AsyncOperationHandle_get_DebugName_m0698FEE2DD4D7EBDF815D04ED9EAFE02A6E4E5D6,
	AsyncOperationHandle_add_Destroyed_m13BB1F42E96BDACF63489DF8710CABE3E36107EC,
	AsyncOperationHandle_remove_Destroyed_mD2240BD43BE2C73A7865D3BDCC79431934026D83,
	AsyncOperationHandle_GetDependencies_mFBA207AD01B44C329D78DD30530F755ED6D4B52B,
	AsyncOperationHandle_GetHashCode_m83FA3D51E02578AA34AAA4C08286A06332EE73DA,
	AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B,
	AsyncOperationHandle_get_IsDone_m6A2917A1C796F97B37F12FAC994BD7F2A2103C74,
	AsyncOperationHandle_IsValid_mD2FD09414056E2CC3570BCFB1E4E676CFE3A6D5E,
	AsyncOperationHandle_get_OperationException_mEE9849546A200CCFE5BEFE90DB9CC89743511006,
	AsyncOperationHandle_get_PercentComplete_m61CD06AEC4F584971D3C4B00523DF60C49BB370F,
	AsyncOperationHandle_GetDownloadStatus_m0C0F49BC92003068201C090DF8E7EB6B2E7167D4,
	AsyncOperationHandle_InternalGetDownloadStatus_m3AA15862A8CC2F4485FD157F5D64C1F96617963C,
	AsyncOperationHandle_get_ReferenceCount_mB2BA57B7F73269EF4AB3294EB6C86FBD03339731,
	AsyncOperationHandle_Release_m628A81301374CB506296D33A6A00B5DEBB5CCCDF,
	AsyncOperationHandle_get_Result_m9371F734DC95198D64F6743BBC680B3F2EB02772,
	AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458,
	AsyncOperationHandle_get_Task_m6E5707186EBDF86F40768E00B1DCD9E1B267109E,
	AsyncOperationHandle_System_Collections_IEnumerator_get_Current_mA090660989881BBBD052B6371961C96DDE2F0AF9,
	AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m826BA946D6D215742C9F83C8B99A63974E7B1CCF,
	AsyncOperationHandle_System_Collections_IEnumerator_Reset_mF9FA74B53FBD69373E94B1FD2E41D6A1B4C06F11,
	DownloadStatus_get_Percent_mFDB729839F011232B236F7958E37953E2454BA0F,
	GroupOperation__ctor_m3EAB15B15FF218854F45B5F0174C2FCA75C62247,
	GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_get_Hash_mABCC641018173E2BBE94EC4A7DB5F8692612505B,
	GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_set_Hash_m05B9F21A48E3CD7C13A7B913D7BFD85591D47F70,
	GroupOperation_GetDependentOps_m7C2A8933BD9562184400AF82518F54AA1B4C5C42,
	GroupOperation_GetDependencies_m1CF7564360E96CAE8CF86E07823C32DC2AE87CB4,
	GroupOperation_ReleaseDependencies_m66F1D2CBDDDCA26C8512718341F4B12309D6A08D,
	GroupOperation_GetDownloadStatus_m4E6E076A47189294A6AA032C19B632BE24F2CE37,
	GroupOperation_DependenciesAreUnchanged_m0022B84628A6480B5EA850C7389C96909FF49FF5,
	GroupOperation_get_DebugName_m8B848F52E178DB9DAF8FC66537B8CB01611B156A,
	GroupOperation_Execute_m0E4CD4E4679BD341333E6A2A8AE0BC3C7D23344F,
	GroupOperation_CompleteIfDependenciesComplete_m77AD5B785A95F917B86D5C244D2193797EE888EA,
	GroupOperation_Destroy_m9D51D92A5FF17ADF9C1BBB7EC8126B3326251E2D,
	GroupOperation_get_Progress_m9C3CD3DF3228BA94E03C198AD5CE9FC091EC6B73,
	GroupOperation_Init_m9125BD292475CA5A2F335C66EFAC4DBDD6BFC996,
	GroupOperation_Init_m43D4E19CB88D75CF0BA203C00CF6BD005DC4A043,
	GroupOperation_OnOperationCompleted_m14BC186F8D434432960795EF635F8A67A3A5780E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD_AdjustorThunk (void);
extern void DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E_AdjustorThunk (void);
extern void DiagnosticEventContext_get_EventValue_m47DD1F4C4E14042B9B710D82AE04092C1F184F35_AdjustorThunk (void);
extern void DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7_AdjustorThunk (void);
extern void DiagnosticEventContext_get_Context_mE8E291342E3B9293E5AEDE07D44F3BC9E0CB7840_AdjustorThunk (void);
extern void DiagnosticEventContext_get_Error_mE66E65391D117531702CD7EBAE58AD10A9C61B1F_AdjustorThunk (void);
extern void DiagnosticEventContext__ctor_m0B5C04CEA5ACB1D89AD8A3005963A97DECC84979_AdjustorThunk (void);
extern void DelegateInfo__ctor_mD79A58037BB825D5FA49F0D35C072FC85EA6EDCF_AdjustorThunk (void);
extern void DelegateInfo_get_InvocationTime_m2DFFB1182B78A216D6210CB939DA4DBC59E7DF59_AdjustorThunk (void);
extern void DelegateInfo_set_InvocationTime_m3472B095E8176F30FCED254739CCF797E8121E46_AdjustorThunk (void);
extern void DelegateInfo_ToString_m0FC72F240C31351642A1401F70AEC75127BB0635_AdjustorThunk (void);
extern void DelegateInfo_Invoke_m9853B80CDAD912DC9E644027D0B8B8AA03B362E9_AdjustorThunk (void);
extern void SerializedType_get_AssemblyName_mB4D8E282E3B22D60B88D0B612E1DD2706450C6FA_AdjustorThunk (void);
extern void SerializedType_get_ClassName_m58CBCA1680BC7B435A66D60D6E49FBE3E2FF03FD_AdjustorThunk (void);
extern void SerializedType_ToString_mE7F91178BCF528D8D66A51CAED1BE91B046C4486_AdjustorThunk (void);
extern void SerializedType_get_Value_mA75D04829016315F5CB5EBD10CD4A44C0754012C_AdjustorThunk (void);
extern void SerializedType_set_Value_m45104AAC40C5238AF758D2D552B92E52FF705092_AdjustorThunk (void);
extern void SerializedType_get_ValueChanged_mA3E9901574336A20A27D2A0DED75B476C02E261E_AdjustorThunk (void);
extern void SerializedType_set_ValueChanged_mFEA6D354142B82599C513F737E679AB4A8C08E9F_AdjustorThunk (void);
extern void ObjectInitializationData_get_Id_m769EFD3C4475326DDFC86C9E9255973AB4B84561_AdjustorThunk (void);
extern void ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_AdjustorThunk (void);
extern void ObjectInitializationData_get_Data_mA167B62E4FB7ADE8409B4AB74D534EE18BBDA7FC_AdjustorThunk (void);
extern void ObjectInitializationData_ToString_m31940BC41095BD7336F705D0BF27B827BE91BBBC_AdjustorThunk (void);
extern void ObjectInitializationData_GetAsyncInitHandle_m0186D6321B14C9AC25E3F8196220ABBE96A965F0_AdjustorThunk (void);
extern void InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC_AdjustorThunk (void);
extern void InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012_AdjustorThunk (void);
extern void InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621_AdjustorThunk (void);
extern void InstantiationParameters_get_InstantiateInWorldPosition_mD022156583A3F9F0DCFF0DAEA75F571E4BBE25B8_AdjustorThunk (void);
extern void InstantiationParameters_get_SetPositionRotation_m1A4A574A3025F0DCE609D8F49F6071FA57904F9C_AdjustorThunk (void);
extern void InstantiationParameters__ctor_m32A54AA524BFE9CCF5D2EA04CD665297FFA1C0E6_AdjustorThunk (void);
extern void InstantiationParameters__ctor_mDCF85728CF9BB36A6EE0930C8B68C10810FE60AA_AdjustorThunk (void);
extern void ProvideHandle__ctor_mEBB5A836A4CDB0B4FB938F85050D865695516097_AdjustorThunk (void);
extern void ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F_AdjustorThunk (void);
extern void ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76_AdjustorThunk (void);
extern void ProvideHandle_get_Type_m52E919AFDB11C39D84549BA4E05F730F14BC528B_AdjustorThunk (void);
extern void ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978_AdjustorThunk (void);
extern void ProvideHandle_get_DependencyCount_m1C44CB1B419CE21A00D6E4275B01147F978FC1AA_AdjustorThunk (void);
extern void ProvideHandle_GetDependencies_mF82C08A32FF506D98985684CF1E7E42EF4326653_AdjustorThunk (void);
extern void ProvideHandle_SetProgressCallback_m762D5F97C1CAAC2D0431DE3C8C22F6FB405FC77E_AdjustorThunk (void);
extern void ProvideHandle_SetDownloadProgressCallbacks_mA4D6FFA06016A567D962BE5AF19F1CAF3C5B5C7C_AdjustorThunk (void);
extern void SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3_AdjustorThunk (void);
extern void SceneInstance_set_Scene_m9B163169E348E68FF15DFDEF76E25815AF6D063D_AdjustorThunk (void);
extern void SceneInstance_Activate_mA85392F02BF420CD5D4ACD8BA84CCE4A577F7466_AdjustorThunk (void);
extern void SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3_AdjustorThunk (void);
extern void SceneInstance_GetHashCode_mFB6154BAA8F8283352C273727C0611405EDC963D_AdjustorThunk (void);
extern void SceneInstance_Equals_mF184704D845C4F37D594CE251E7DBD449B5E7F38_AdjustorThunk (void);
extern void DiagnosticEvent_get_Graph_mCF17340DDBF35945343484DC79AA2D2D8A1AFCE3_AdjustorThunk (void);
extern void DiagnosticEvent_get_ObjectId_m1333C6072B25E6A8047548B8A6501753C00BB282_AdjustorThunk (void);
extern void DiagnosticEvent_get_DisplayName_m3FC7DFC84D87E9797E61B679F6A91F61ED70320E_AdjustorThunk (void);
extern void DiagnosticEvent_get_Dependencies_m5986D447D6C4B69FC83EFD4A9FA0533BCA254146_AdjustorThunk (void);
extern void DiagnosticEvent_get_Stream_mD67FE4C4C2240D064E2B7AB436388ED88FAF7EF8_AdjustorThunk (void);
extern void DiagnosticEvent_get_Frame_mDA581C601CD74A5DB69B9601753E95B57DB516F7_AdjustorThunk (void);
extern void DiagnosticEvent_get_Value_mC9E381EEF0DD9BE55F1BDD56E8E9218EE5C9890A_AdjustorThunk (void);
extern void DiagnosticEvent__ctor_m39F2B2E3A31C9446E2F78E0ECEED641B7DA639B5_AdjustorThunk (void);
extern void DiagnosticEvent_Serialize_mBFF1FE437AD85BDD0EB4530553B99CE46D655572_AdjustorThunk (void);
extern void AsyncOperationHandle_get_LocationName_m28C16FB5752DBA9E9BC3481A2FEEEE8631A0879C_AdjustorThunk (void);
extern void AsyncOperationHandle_set_LocationName_m84D55A8B0E3620FEBFDF4EF68681135CF54C9C3D_AdjustorThunk (void);
extern void AsyncOperationHandle__ctor_m0B9C35256EB8B705BC512EF9821F0734A318E4F7_AdjustorThunk (void);
extern void AsyncOperationHandle__ctor_mC21DA18C142F32C56786DBBC82CA86A4CA032701_AdjustorThunk (void);
extern void AsyncOperationHandle__ctor_m0D8F822B4B8C250F8AC60E230B2F6DCC1E4BB398_AdjustorThunk (void);
extern void AsyncOperationHandle__ctor_mFDC8FCA6F2C2FA687BD56CE9544ED582D475D132_AdjustorThunk (void);
extern void AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707_AdjustorThunk (void);
extern void AsyncOperationHandle_add_Completed_m736B8F270CA7BD0D742477D59F5882D179E0E0C6_AdjustorThunk (void);
extern void AsyncOperationHandle_remove_Completed_m7EEA0C4ED68EB2E2A66F642882DDD0DD7A348F23_AdjustorThunk (void);
extern void AsyncOperationHandle_Equals_mCAE66BFB37D7B0B71417D5895C9352F57D35C5E0_AdjustorThunk (void);
extern void AsyncOperationHandle_get_DebugName_m0698FEE2DD4D7EBDF815D04ED9EAFE02A6E4E5D6_AdjustorThunk (void);
extern void AsyncOperationHandle_add_Destroyed_m13BB1F42E96BDACF63489DF8710CABE3E36107EC_AdjustorThunk (void);
extern void AsyncOperationHandle_remove_Destroyed_mD2240BD43BE2C73A7865D3BDCC79431934026D83_AdjustorThunk (void);
extern void AsyncOperationHandle_GetDependencies_mFBA207AD01B44C329D78DD30530F755ED6D4B52B_AdjustorThunk (void);
extern void AsyncOperationHandle_GetHashCode_m83FA3D51E02578AA34AAA4C08286A06332EE73DA_AdjustorThunk (void);
extern void AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B_AdjustorThunk (void);
extern void AsyncOperationHandle_get_IsDone_m6A2917A1C796F97B37F12FAC994BD7F2A2103C74_AdjustorThunk (void);
extern void AsyncOperationHandle_IsValid_mD2FD09414056E2CC3570BCFB1E4E676CFE3A6D5E_AdjustorThunk (void);
extern void AsyncOperationHandle_get_OperationException_mEE9849546A200CCFE5BEFE90DB9CC89743511006_AdjustorThunk (void);
extern void AsyncOperationHandle_get_PercentComplete_m61CD06AEC4F584971D3C4B00523DF60C49BB370F_AdjustorThunk (void);
extern void AsyncOperationHandle_GetDownloadStatus_m0C0F49BC92003068201C090DF8E7EB6B2E7167D4_AdjustorThunk (void);
extern void AsyncOperationHandle_InternalGetDownloadStatus_m3AA15862A8CC2F4485FD157F5D64C1F96617963C_AdjustorThunk (void);
extern void AsyncOperationHandle_get_ReferenceCount_mB2BA57B7F73269EF4AB3294EB6C86FBD03339731_AdjustorThunk (void);
extern void AsyncOperationHandle_Release_m628A81301374CB506296D33A6A00B5DEBB5CCCDF_AdjustorThunk (void);
extern void AsyncOperationHandle_get_Result_m9371F734DC95198D64F6743BBC680B3F2EB02772_AdjustorThunk (void);
extern void AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458_AdjustorThunk (void);
extern void AsyncOperationHandle_get_Task_m6E5707186EBDF86F40768E00B1DCD9E1B267109E_AdjustorThunk (void);
extern void AsyncOperationHandle_System_Collections_IEnumerator_get_Current_mA090660989881BBBD052B6371961C96DDE2F0AF9_AdjustorThunk (void);
extern void AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m826BA946D6D215742C9F83C8B99A63974E7B1CCF_AdjustorThunk (void);
extern void AsyncOperationHandle_System_Collections_IEnumerator_Reset_mF9FA74B53FBD69373E94B1FD2E41D6A1B4C06F11_AdjustorThunk (void);
extern void DownloadStatus_get_Percent_mFDB729839F011232B236F7958E37953E2454BA0F_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[86] = 
{
	{ 0x06000076, DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD_AdjustorThunk },
	{ 0x06000077, DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E_AdjustorThunk },
	{ 0x06000078, DiagnosticEventContext_get_EventValue_m47DD1F4C4E14042B9B710D82AE04092C1F184F35_AdjustorThunk },
	{ 0x06000079, DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7_AdjustorThunk },
	{ 0x0600007A, DiagnosticEventContext_get_Context_mE8E291342E3B9293E5AEDE07D44F3BC9E0CB7840_AdjustorThunk },
	{ 0x0600007B, DiagnosticEventContext_get_Error_mE66E65391D117531702CD7EBAE58AD10A9C61B1F_AdjustorThunk },
	{ 0x0600007C, DiagnosticEventContext__ctor_m0B5C04CEA5ACB1D89AD8A3005963A97DECC84979_AdjustorThunk },
	{ 0x060000B5, DelegateInfo__ctor_mD79A58037BB825D5FA49F0D35C072FC85EA6EDCF_AdjustorThunk },
	{ 0x060000B6, DelegateInfo_get_InvocationTime_m2DFFB1182B78A216D6210CB939DA4DBC59E7DF59_AdjustorThunk },
	{ 0x060000B7, DelegateInfo_set_InvocationTime_m3472B095E8176F30FCED254739CCF797E8121E46_AdjustorThunk },
	{ 0x060000B8, DelegateInfo_ToString_m0FC72F240C31351642A1401F70AEC75127BB0635_AdjustorThunk },
	{ 0x060000B9, DelegateInfo_Invoke_m9853B80CDAD912DC9E644027D0B8B8AA03B362E9_AdjustorThunk },
	{ 0x060000D0, SerializedType_get_AssemblyName_mB4D8E282E3B22D60B88D0B612E1DD2706450C6FA_AdjustorThunk },
	{ 0x060000D1, SerializedType_get_ClassName_m58CBCA1680BC7B435A66D60D6E49FBE3E2FF03FD_AdjustorThunk },
	{ 0x060000D2, SerializedType_ToString_mE7F91178BCF528D8D66A51CAED1BE91B046C4486_AdjustorThunk },
	{ 0x060000D3, SerializedType_get_Value_mA75D04829016315F5CB5EBD10CD4A44C0754012C_AdjustorThunk },
	{ 0x060000D4, SerializedType_set_Value_m45104AAC40C5238AF758D2D552B92E52FF705092_AdjustorThunk },
	{ 0x060000D5, SerializedType_get_ValueChanged_mA3E9901574336A20A27D2A0DED75B476C02E261E_AdjustorThunk },
	{ 0x060000D6, SerializedType_set_ValueChanged_mFEA6D354142B82599C513F737E679AB4A8C08E9F_AdjustorThunk },
	{ 0x060000D7, ObjectInitializationData_get_Id_m769EFD3C4475326DDFC86C9E9255973AB4B84561_AdjustorThunk },
	{ 0x060000D8, ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_AdjustorThunk },
	{ 0x060000D9, ObjectInitializationData_get_Data_mA167B62E4FB7ADE8409B4AB74D534EE18BBDA7FC_AdjustorThunk },
	{ 0x060000DA, ObjectInitializationData_ToString_m31940BC41095BD7336F705D0BF27B827BE91BBBC_AdjustorThunk },
	{ 0x060000DC, ObjectInitializationData_GetAsyncInitHandle_m0186D6321B14C9AC25E3F8196220ABBE96A965F0_AdjustorThunk },
	{ 0x06000112, InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC_AdjustorThunk },
	{ 0x06000113, InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012_AdjustorThunk },
	{ 0x06000114, InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621_AdjustorThunk },
	{ 0x06000115, InstantiationParameters_get_InstantiateInWorldPosition_mD022156583A3F9F0DCFF0DAEA75F571E4BBE25B8_AdjustorThunk },
	{ 0x06000116, InstantiationParameters_get_SetPositionRotation_m1A4A574A3025F0DCE609D8F49F6071FA57904F9C_AdjustorThunk },
	{ 0x06000117, InstantiationParameters__ctor_m32A54AA524BFE9CCF5D2EA04CD665297FFA1C0E6_AdjustorThunk },
	{ 0x06000118, InstantiationParameters__ctor_mDCF85728CF9BB36A6EE0930C8B68C10810FE60AA_AdjustorThunk },
	{ 0x0600011C, ProvideHandle__ctor_mEBB5A836A4CDB0B4FB938F85050D865695516097_AdjustorThunk },
	{ 0x0600011D, ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F_AdjustorThunk },
	{ 0x0600011E, ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76_AdjustorThunk },
	{ 0x0600011F, ProvideHandle_get_Type_m52E919AFDB11C39D84549BA4E05F730F14BC528B_AdjustorThunk },
	{ 0x06000120, ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978_AdjustorThunk },
	{ 0x06000121, ProvideHandle_get_DependencyCount_m1C44CB1B419CE21A00D6E4275B01147F978FC1AA_AdjustorThunk },
	{ 0x06000123, ProvideHandle_GetDependencies_mF82C08A32FF506D98985684CF1E7E42EF4326653_AdjustorThunk },
	{ 0x06000124, ProvideHandle_SetProgressCallback_m762D5F97C1CAAC2D0431DE3C8C22F6FB405FC77E_AdjustorThunk },
	{ 0x06000125, ProvideHandle_SetDownloadProgressCallbacks_mA4D6FFA06016A567D962BE5AF19F1CAF3C5B5C7C_AdjustorThunk },
	{ 0x0600012D, SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3_AdjustorThunk },
	{ 0x0600012E, SceneInstance_set_Scene_m9B163169E348E68FF15DFDEF76E25815AF6D063D_AdjustorThunk },
	{ 0x0600012F, SceneInstance_Activate_mA85392F02BF420CD5D4ACD8BA84CCE4A577F7466_AdjustorThunk },
	{ 0x06000130, SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3_AdjustorThunk },
	{ 0x06000131, SceneInstance_GetHashCode_mFB6154BAA8F8283352C273727C0611405EDC963D_AdjustorThunk },
	{ 0x06000132, SceneInstance_Equals_mF184704D845C4F37D594CE251E7DBD449B5E7F38_AdjustorThunk },
	{ 0x06000185, DiagnosticEvent_get_Graph_mCF17340DDBF35945343484DC79AA2D2D8A1AFCE3_AdjustorThunk },
	{ 0x06000186, DiagnosticEvent_get_ObjectId_m1333C6072B25E6A8047548B8A6501753C00BB282_AdjustorThunk },
	{ 0x06000187, DiagnosticEvent_get_DisplayName_m3FC7DFC84D87E9797E61B679F6A91F61ED70320E_AdjustorThunk },
	{ 0x06000188, DiagnosticEvent_get_Dependencies_m5986D447D6C4B69FC83EFD4A9FA0533BCA254146_AdjustorThunk },
	{ 0x06000189, DiagnosticEvent_get_Stream_mD67FE4C4C2240D064E2B7AB436388ED88FAF7EF8_AdjustorThunk },
	{ 0x0600018A, DiagnosticEvent_get_Frame_mDA581C601CD74A5DB69B9601753E95B57DB516F7_AdjustorThunk },
	{ 0x0600018B, DiagnosticEvent_get_Value_mC9E381EEF0DD9BE55F1BDD56E8E9218EE5C9890A_AdjustorThunk },
	{ 0x0600018C, DiagnosticEvent__ctor_m39F2B2E3A31C9446E2F78E0ECEED641B7DA639B5_AdjustorThunk },
	{ 0x0600018D, DiagnosticEvent_Serialize_mBFF1FE437AD85BDD0EB4530553B99CE46D655572_AdjustorThunk },
	{ 0x06000228, AsyncOperationHandle_get_LocationName_m28C16FB5752DBA9E9BC3481A2FEEEE8631A0879C_AdjustorThunk },
	{ 0x06000229, AsyncOperationHandle_set_LocationName_m84D55A8B0E3620FEBFDF4EF68681135CF54C9C3D_AdjustorThunk },
	{ 0x0600022A, AsyncOperationHandle__ctor_m0B9C35256EB8B705BC512EF9821F0734A318E4F7_AdjustorThunk },
	{ 0x0600022B, AsyncOperationHandle__ctor_mC21DA18C142F32C56786DBBC82CA86A4CA032701_AdjustorThunk },
	{ 0x0600022C, AsyncOperationHandle__ctor_m0D8F822B4B8C250F8AC60E230B2F6DCC1E4BB398_AdjustorThunk },
	{ 0x0600022D, AsyncOperationHandle__ctor_mFDC8FCA6F2C2FA687BD56CE9544ED582D475D132_AdjustorThunk },
	{ 0x0600022E, AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707_AdjustorThunk },
	{ 0x0600022F, AsyncOperationHandle_add_Completed_m736B8F270CA7BD0D742477D59F5882D179E0E0C6_AdjustorThunk },
	{ 0x06000230, AsyncOperationHandle_remove_Completed_m7EEA0C4ED68EB2E2A66F642882DDD0DD7A348F23_AdjustorThunk },
	{ 0x06000232, AsyncOperationHandle_Equals_mCAE66BFB37D7B0B71417D5895C9352F57D35C5E0_AdjustorThunk },
	{ 0x06000233, AsyncOperationHandle_get_DebugName_m0698FEE2DD4D7EBDF815D04ED9EAFE02A6E4E5D6_AdjustorThunk },
	{ 0x06000234, AsyncOperationHandle_add_Destroyed_m13BB1F42E96BDACF63489DF8710CABE3E36107EC_AdjustorThunk },
	{ 0x06000235, AsyncOperationHandle_remove_Destroyed_mD2240BD43BE2C73A7865D3BDCC79431934026D83_AdjustorThunk },
	{ 0x06000236, AsyncOperationHandle_GetDependencies_mFBA207AD01B44C329D78DD30530F755ED6D4B52B_AdjustorThunk },
	{ 0x06000237, AsyncOperationHandle_GetHashCode_m83FA3D51E02578AA34AAA4C08286A06332EE73DA_AdjustorThunk },
	{ 0x06000238, AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B_AdjustorThunk },
	{ 0x06000239, AsyncOperationHandle_get_IsDone_m6A2917A1C796F97B37F12FAC994BD7F2A2103C74_AdjustorThunk },
	{ 0x0600023A, AsyncOperationHandle_IsValid_mD2FD09414056E2CC3570BCFB1E4E676CFE3A6D5E_AdjustorThunk },
	{ 0x0600023B, AsyncOperationHandle_get_OperationException_mEE9849546A200CCFE5BEFE90DB9CC89743511006_AdjustorThunk },
	{ 0x0600023C, AsyncOperationHandle_get_PercentComplete_m61CD06AEC4F584971D3C4B00523DF60C49BB370F_AdjustorThunk },
	{ 0x0600023D, AsyncOperationHandle_GetDownloadStatus_m0C0F49BC92003068201C090DF8E7EB6B2E7167D4_AdjustorThunk },
	{ 0x0600023E, AsyncOperationHandle_InternalGetDownloadStatus_m3AA15862A8CC2F4485FD157F5D64C1F96617963C_AdjustorThunk },
	{ 0x0600023F, AsyncOperationHandle_get_ReferenceCount_mB2BA57B7F73269EF4AB3294EB6C86FBD03339731_AdjustorThunk },
	{ 0x06000240, AsyncOperationHandle_Release_m628A81301374CB506296D33A6A00B5DEBB5CCCDF_AdjustorThunk },
	{ 0x06000241, AsyncOperationHandle_get_Result_m9371F734DC95198D64F6743BBC680B3F2EB02772_AdjustorThunk },
	{ 0x06000242, AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458_AdjustorThunk },
	{ 0x06000243, AsyncOperationHandle_get_Task_m6E5707186EBDF86F40768E00B1DCD9E1B267109E_AdjustorThunk },
	{ 0x06000244, AsyncOperationHandle_System_Collections_IEnumerator_get_Current_mA090660989881BBBD052B6371961C96DDE2F0AF9_AdjustorThunk },
	{ 0x06000245, AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m826BA946D6D215742C9F83C8B99A63974E7B1CCF_AdjustorThunk },
	{ 0x06000246, AsyncOperationHandle_System_Collections_IEnumerator_Reset_mF9FA74B53FBD69373E94B1FD2E41D6A1B4C06F11_AdjustorThunk },
	{ 0x06000247, DownloadStatus_get_Percent_mFDB729839F011232B236F7958E37953E2454BA0F_AdjustorThunk },
};
static const int32_t s_InvokerIndices[631] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	14,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	154,
	14,
	26,
	28,
	10,
	10,
	26,
	26,
	14,
	26,
	14,
	14,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	2071,
	105,
	28,
	41,
	2072,
	-1,
	-1,
	2073,
	26,
	26,
	26,
	-1,
	62,
	30,
	30,
	10,
	-1,
	-1,
	2074,
	2074,
	34,
	-1,
	-1,
	2075,
	2076,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2077,
	2078,
	2079,
	2080,
	23,
	456,
	335,
	23,
	3,
	26,
	2081,
	10,
	10,
	14,
	14,
	14,
	2082,
	-1,
	-1,
	-1,
	-1,
	2083,
	2084,
	26,
	14,
	1647,
	23,
	727,
	23,
	23,
	-1,
	-1,
	-1,
	335,
	89,
	26,
	26,
	164,
	0,
	154,
	3,
	23,
	26,
	27,
	111,
	14,
	26,
	26,
	23,
	26,
	27,
	111,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	500,
	3,
	23,
	2085,
	2086,
	49,
	1619,
	23,
	335,
	23,
	23,
	2086,
	727,
	335,
	14,
	23,
	90,
	2087,
	14,
	2088,
	58,
	62,
	58,
	62,
	23,
	339,
	14,
	26,
	58,
	62,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	14,
	26,
	89,
	31,
	14,
	2089,
	14,
	14,
	-1,
	2090,
	365,
	114,
	114,
	1,
	-1,
	1,
	-1,
	-1,
	14,
	14,
	26,
	10,
	32,
	10,
	32,
	89,
	31,
	10,
	32,
	10,
	32,
	14,
	26,
	172,
	200,
	89,
	31,
	2091,
	23,
	28,
	727,
	2092,
	14,
	2093,
	23,
	26,
	26,
	23,
	23,
	26,
	2093,
	28,
	27,
	23,
	2093,
	23,
	2093,
	23,
	0,
	2093,
	26,
	727,
	23,
	1423,
	1670,
	14,
	89,
	89,
	456,
	2094,
	-1,
	2095,
	27,
	27,
	14,
	14,
	14,
	14,
	10,
	-1,
	26,
	26,
	26,
	-1,
	14,
	28,
	90,
	2093,
	27,
	10,
	1647,
	2080,
	23,
	14,
	10,
	9,
	2077,
	2078,
	2095,
	27,
	23,
	105,
	23,
	2093,
	27,
	23,
	2093,
	26,
	727,
	23,
	14,
	90,
	90,
	14,
	27,
	28,
	2093,
	2087,
	10,
	23,
	26,
	23,
	23,
	23,
	89,
	2077,
	2078,
	23,
	26,
	2084,
	2096,
	26,
	14,
	23,
	2097,
	2098,
	23,
	727,
	335,
	2099,
	23,
	26,
	727,
	23,
	89,
	31,
	105,
	2093,
	23,
	727,
	2100,
	26,
	23,
	26,
	2091,
	14,
	14,
	14,
	112,
	10,
	89,
	14,
	14,
	14,
	14,
	14,
	14,
	89,
	14,
	26,
	14,
	26,
	10,
	14,
	112,
	14,
	822,
	23,
	14,
	10,
	14,
	14,
	10,
	10,
	10,
	2101,
	14,
	2102,
	433,
	14,
	2103,
	26,
	26,
	2104,
	23,
	23,
	23,
	3,
	23,
	2105,
	2104,
	433,
	4,
	2103,
	26,
	2104,
	23,
	10,
	32,
	14,
	14,
	10,
	14,
	23,
	23,
	10,
	727,
	2084,
	10,
	14,
	89,
	26,
	26,
	89,
	26,
	26,
	26,
	26,
	23,
	14,
	2106,
	2081,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	26,
	26,
	130,
	27,
	107,
	2081,
	26,
	26,
	-1,
	2107,
	14,
	26,
	26,
	26,
	10,
	14,
	89,
	89,
	14,
	727,
	2092,
	2084,
	10,
	23,
	14,
	10,
	14,
	14,
	89,
	23,
	727,
	23,
	10,
	32,
	14,
	26,
	23,
	2084,
	9,
	14,
	23,
	23,
	23,
	727,
	807,
	130,
	2074,
	2108,
	2109,
	10,
	14,
	10,
	26,
	-1,
	26,
	-1,
	14,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[39] = 
{
	{ 0x02000002, { 0, 20 } },
	{ 0x02000003, { 20, 23 } },
	{ 0x02000005, { 43, 27 } },
	{ 0x02000006, { 70, 18 } },
	{ 0x0200000A, { 134, 5 } },
	{ 0x0200000C, { 139, 8 } },
	{ 0x02000012, { 147, 10 } },
	{ 0x0200001B, { 157, 9 } },
	{ 0x0200001C, { 166, 5 } },
	{ 0x02000043, { 184, 66 } },
	{ 0x02000044, { 250, 2 } },
	{ 0x02000045, { 252, 3 } },
	{ 0x02000046, { 255, 2 } },
	{ 0x02000047, { 257, 2 } },
	{ 0x02000048, { 259, 24 } },
	{ 0x0200004F, { 285, 11 } },
	{ 0x06000052, { 88, 2 } },
	{ 0x06000053, { 90, 2 } },
	{ 0x06000058, { 92, 1 } },
	{ 0x0600005D, { 93, 1 } },
	{ 0x0600005E, { 94, 4 } },
	{ 0x06000062, { 98, 3 } },
	{ 0x06000063, { 101, 3 } },
	{ 0x06000066, { 104, 1 } },
	{ 0x06000067, { 105, 9 } },
	{ 0x06000068, { 114, 6 } },
	{ 0x06000069, { 120, 4 } },
	{ 0x0600006A, { 124, 6 } },
	{ 0x0600006B, { 130, 4 } },
	{ 0x060000DB, { 171, 1 } },
	{ 0x060000E1, { 172, 2 } },
	{ 0x060000E3, { 174, 2 } },
	{ 0x060000E4, { 176, 2 } },
	{ 0x06000119, { 178, 4 } },
	{ 0x06000122, { 182, 1 } },
	{ 0x06000126, { 183, 1 } },
	{ 0x06000231, { 283, 2 } },
	{ 0x06000270, { 296, 1 } },
	{ 0x06000272, { 297, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[301] = 
{
	{ (Il2CppRGCTXDataType)3, 21687 },
	{ (Il2CppRGCTXDataType)3, 21688 },
	{ (Il2CppRGCTXDataType)2, 27907 },
	{ (Il2CppRGCTXDataType)3, 21689 },
	{ (Il2CppRGCTXDataType)3, 21690 },
	{ (Il2CppRGCTXDataType)3, 21691 },
	{ (Il2CppRGCTXDataType)3, 21692 },
	{ (Il2CppRGCTXDataType)3, 21693 },
	{ (Il2CppRGCTXDataType)3, 21694 },
	{ (Il2CppRGCTXDataType)3, 21695 },
	{ (Il2CppRGCTXDataType)3, 21696 },
	{ (Il2CppRGCTXDataType)3, 21697 },
	{ (Il2CppRGCTXDataType)3, 21698 },
	{ (Il2CppRGCTXDataType)2, 24195 },
	{ (Il2CppRGCTXDataType)3, 21699 },
	{ (Il2CppRGCTXDataType)3, 21700 },
	{ (Il2CppRGCTXDataType)2, 24199 },
	{ (Il2CppRGCTXDataType)3, 21701 },
	{ (Il2CppRGCTXDataType)2, 24200 },
	{ (Il2CppRGCTXDataType)3, 21702 },
	{ (Il2CppRGCTXDataType)2, 24206 },
	{ (Il2CppRGCTXDataType)3, 21703 },
	{ (Il2CppRGCTXDataType)3, 21704 },
	{ (Il2CppRGCTXDataType)3, 21705 },
	{ (Il2CppRGCTXDataType)3, 21706 },
	{ (Il2CppRGCTXDataType)3, 21707 },
	{ (Il2CppRGCTXDataType)3, 21708 },
	{ (Il2CppRGCTXDataType)3, 21709 },
	{ (Il2CppRGCTXDataType)2, 27668 },
	{ (Il2CppRGCTXDataType)3, 21710 },
	{ (Il2CppRGCTXDataType)3, 21711 },
	{ (Il2CppRGCTXDataType)3, 21712 },
	{ (Il2CppRGCTXDataType)3, 21713 },
	{ (Il2CppRGCTXDataType)2, 27908 },
	{ (Il2CppRGCTXDataType)3, 21714 },
	{ (Il2CppRGCTXDataType)3, 21715 },
	{ (Il2CppRGCTXDataType)3, 21716 },
	{ (Il2CppRGCTXDataType)3, 21717 },
	{ (Il2CppRGCTXDataType)3, 21718 },
	{ (Il2CppRGCTXDataType)3, 21719 },
	{ (Il2CppRGCTXDataType)3, 21720 },
	{ (Il2CppRGCTXDataType)2, 27909 },
	{ (Il2CppRGCTXDataType)3, 21721 },
	{ (Il2CppRGCTXDataType)3, 21722 },
	{ (Il2CppRGCTXDataType)2, 24219 },
	{ (Il2CppRGCTXDataType)3, 21723 },
	{ (Il2CppRGCTXDataType)2, 27910 },
	{ (Il2CppRGCTXDataType)3, 21724 },
	{ (Il2CppRGCTXDataType)1, 24220 },
	{ (Il2CppRGCTXDataType)1, 24222 },
	{ (Il2CppRGCTXDataType)3, 21725 },
	{ (Il2CppRGCTXDataType)3, 21726 },
	{ (Il2CppRGCTXDataType)3, 21727 },
	{ (Il2CppRGCTXDataType)2, 24221 },
	{ (Il2CppRGCTXDataType)3, 21728 },
	{ (Il2CppRGCTXDataType)3, 21729 },
	{ (Il2CppRGCTXDataType)3, 21730 },
	{ (Il2CppRGCTXDataType)3, 21731 },
	{ (Il2CppRGCTXDataType)3, 21732 },
	{ (Il2CppRGCTXDataType)3, 21733 },
	{ (Il2CppRGCTXDataType)3, 21734 },
	{ (Il2CppRGCTXDataType)3, 21735 },
	{ (Il2CppRGCTXDataType)3, 21736 },
	{ (Il2CppRGCTXDataType)3, 21737 },
	{ (Il2CppRGCTXDataType)3, 21738 },
	{ (Il2CppRGCTXDataType)3, 21739 },
	{ (Il2CppRGCTXDataType)3, 21740 },
	{ (Il2CppRGCTXDataType)3, 21741 },
	{ (Il2CppRGCTXDataType)3, 21742 },
	{ (Il2CppRGCTXDataType)3, 21743 },
	{ (Il2CppRGCTXDataType)3, 21744 },
	{ (Il2CppRGCTXDataType)2, 24231 },
	{ (Il2CppRGCTXDataType)3, 21745 },
	{ (Il2CppRGCTXDataType)2, 27911 },
	{ (Il2CppRGCTXDataType)3, 21746 },
	{ (Il2CppRGCTXDataType)1, 24232 },
	{ (Il2CppRGCTXDataType)3, 21747 },
	{ (Il2CppRGCTXDataType)3, 21748 },
	{ (Il2CppRGCTXDataType)3, 21749 },
	{ (Il2CppRGCTXDataType)3, 21750 },
	{ (Il2CppRGCTXDataType)3, 21751 },
	{ (Il2CppRGCTXDataType)3, 21752 },
	{ (Il2CppRGCTXDataType)3, 21753 },
	{ (Il2CppRGCTXDataType)3, 21754 },
	{ (Il2CppRGCTXDataType)3, 21755 },
	{ (Il2CppRGCTXDataType)3, 21756 },
	{ (Il2CppRGCTXDataType)3, 21757 },
	{ (Il2CppRGCTXDataType)3, 21758 },
	{ (Il2CppRGCTXDataType)1, 24245 },
	{ (Il2CppRGCTXDataType)3, 21759 },
	{ (Il2CppRGCTXDataType)3, 21760 },
	{ (Il2CppRGCTXDataType)3, 21761 },
	{ (Il2CppRGCTXDataType)2, 24249 },
	{ (Il2CppRGCTXDataType)3, 21762 },
	{ (Il2CppRGCTXDataType)1, 27912 },
	{ (Il2CppRGCTXDataType)3, 21763 },
	{ (Il2CppRGCTXDataType)3, 21764 },
	{ (Il2CppRGCTXDataType)3, 21765 },
	{ (Il2CppRGCTXDataType)3, 21766 },
	{ (Il2CppRGCTXDataType)3, 21767 },
	{ (Il2CppRGCTXDataType)2, 27914 },
	{ (Il2CppRGCTXDataType)3, 21768 },
	{ (Il2CppRGCTXDataType)3, 21769 },
	{ (Il2CppRGCTXDataType)2, 27916 },
	{ (Il2CppRGCTXDataType)3, 21770 },
	{ (Il2CppRGCTXDataType)2, 27917 },
	{ (Il2CppRGCTXDataType)3, 21771 },
	{ (Il2CppRGCTXDataType)3, 21772 },
	{ (Il2CppRGCTXDataType)3, 21773 },
	{ (Il2CppRGCTXDataType)1, 24260 },
	{ (Il2CppRGCTXDataType)3, 21774 },
	{ (Il2CppRGCTXDataType)2, 27918 },
	{ (Il2CppRGCTXDataType)3, 21775 },
	{ (Il2CppRGCTXDataType)3, 21776 },
	{ (Il2CppRGCTXDataType)1, 27919 },
	{ (Il2CppRGCTXDataType)3, 21777 },
	{ (Il2CppRGCTXDataType)3, 21778 },
	{ (Il2CppRGCTXDataType)3, 21779 },
	{ (Il2CppRGCTXDataType)2, 24264 },
	{ (Il2CppRGCTXDataType)3, 21780 },
	{ (Il2CppRGCTXDataType)2, 27920 },
	{ (Il2CppRGCTXDataType)3, 21781 },
	{ (Il2CppRGCTXDataType)3, 21782 },
	{ (Il2CppRGCTXDataType)3, 21783 },
	{ (Il2CppRGCTXDataType)1, 27921 },
	{ (Il2CppRGCTXDataType)3, 21784 },
	{ (Il2CppRGCTXDataType)3, 21785 },
	{ (Il2CppRGCTXDataType)3, 21786 },
	{ (Il2CppRGCTXDataType)2, 24272 },
	{ (Il2CppRGCTXDataType)3, 21787 },
	{ (Il2CppRGCTXDataType)2, 27922 },
	{ (Il2CppRGCTXDataType)3, 21788 },
	{ (Il2CppRGCTXDataType)3, 21789 },
	{ (Il2CppRGCTXDataType)3, 21790 },
	{ (Il2CppRGCTXDataType)3, 21791 },
	{ (Il2CppRGCTXDataType)2, 24305 },
	{ (Il2CppRGCTXDataType)3, 21792 },
	{ (Il2CppRGCTXDataType)3, 21793 },
	{ (Il2CppRGCTXDataType)3, 21794 },
	{ (Il2CppRGCTXDataType)2, 24316 },
	{ (Il2CppRGCTXDataType)3, 21795 },
	{ (Il2CppRGCTXDataType)2, 27923 },
	{ (Il2CppRGCTXDataType)3, 21796 },
	{ (Il2CppRGCTXDataType)3, 21797 },
	{ (Il2CppRGCTXDataType)3, 21798 },
	{ (Il2CppRGCTXDataType)3, 21799 },
	{ (Il2CppRGCTXDataType)3, 21800 },
	{ (Il2CppRGCTXDataType)2, 24337 },
	{ (Il2CppRGCTXDataType)2, 24335 },
	{ (Il2CppRGCTXDataType)3, 21801 },
	{ (Il2CppRGCTXDataType)3, 21802 },
	{ (Il2CppRGCTXDataType)3, 21803 },
	{ (Il2CppRGCTXDataType)1, 24335 },
	{ (Il2CppRGCTXDataType)3, 21804 },
	{ (Il2CppRGCTXDataType)3, 21805 },
	{ (Il2CppRGCTXDataType)3, 21806 },
	{ (Il2CppRGCTXDataType)3, 21807 },
	{ (Il2CppRGCTXDataType)3, 21808 },
	{ (Il2CppRGCTXDataType)3, 21809 },
	{ (Il2CppRGCTXDataType)3, 21810 },
	{ (Il2CppRGCTXDataType)2, 24359 },
	{ (Il2CppRGCTXDataType)3, 21811 },
	{ (Il2CppRGCTXDataType)2, 27924 },
	{ (Il2CppRGCTXDataType)3, 21812 },
	{ (Il2CppRGCTXDataType)3, 21813 },
	{ (Il2CppRGCTXDataType)3, 21814 },
	{ (Il2CppRGCTXDataType)2, 27925 },
	{ (Il2CppRGCTXDataType)2, 27926 },
	{ (Il2CppRGCTXDataType)3, 21815 },
	{ (Il2CppRGCTXDataType)3, 21816 },
	{ (Il2CppRGCTXDataType)3, 21817 },
	{ (Il2CppRGCTXDataType)2, 24368 },
	{ (Il2CppRGCTXDataType)1, 24372 },
	{ (Il2CppRGCTXDataType)2, 24372 },
	{ (Il2CppRGCTXDataType)1, 24373 },
	{ (Il2CppRGCTXDataType)2, 24373 },
	{ (Il2CppRGCTXDataType)1, 27927 },
	{ (Il2CppRGCTXDataType)1, 27928 },
	{ (Il2CppRGCTXDataType)3, 21818 },
	{ (Il2CppRGCTXDataType)3, 21819 },
	{ (Il2CppRGCTXDataType)3, 21820 },
	{ (Il2CppRGCTXDataType)3, 21821 },
	{ (Il2CppRGCTXDataType)3, 21822 },
	{ (Il2CppRGCTXDataType)3, 21823 },
	{ (Il2CppRGCTXDataType)3, 21824 },
	{ (Il2CppRGCTXDataType)3, 21825 },
	{ (Il2CppRGCTXDataType)3, 21826 },
	{ (Il2CppRGCTXDataType)2, 24469 },
	{ (Il2CppRGCTXDataType)3, 21827 },
	{ (Il2CppRGCTXDataType)3, 21828 },
	{ (Il2CppRGCTXDataType)2, 24469 },
	{ (Il2CppRGCTXDataType)3, 21829 },
	{ (Il2CppRGCTXDataType)3, 21830 },
	{ (Il2CppRGCTXDataType)2, 27929 },
	{ (Il2CppRGCTXDataType)3, 21831 },
	{ (Il2CppRGCTXDataType)3, 21832 },
	{ (Il2CppRGCTXDataType)3, 21833 },
	{ (Il2CppRGCTXDataType)3, 21834 },
	{ (Il2CppRGCTXDataType)3, 21835 },
	{ (Il2CppRGCTXDataType)3, 21836 },
	{ (Il2CppRGCTXDataType)2, 27930 },
	{ (Il2CppRGCTXDataType)3, 21837 },
	{ (Il2CppRGCTXDataType)3, 21838 },
	{ (Il2CppRGCTXDataType)2, 27931 },
	{ (Il2CppRGCTXDataType)3, 21839 },
	{ (Il2CppRGCTXDataType)2, 24466 },
	{ (Il2CppRGCTXDataType)3, 21840 },
	{ (Il2CppRGCTXDataType)3, 21841 },
	{ (Il2CppRGCTXDataType)3, 21842 },
	{ (Il2CppRGCTXDataType)2, 27932 },
	{ (Il2CppRGCTXDataType)3, 21843 },
	{ (Il2CppRGCTXDataType)3, 21844 },
	{ (Il2CppRGCTXDataType)3, 21845 },
	{ (Il2CppRGCTXDataType)2, 27933 },
	{ (Il2CppRGCTXDataType)3, 21846 },
	{ (Il2CppRGCTXDataType)3, 21847 },
	{ (Il2CppRGCTXDataType)2, 24468 },
	{ (Il2CppRGCTXDataType)3, 21848 },
	{ (Il2CppRGCTXDataType)3, 21849 },
	{ (Il2CppRGCTXDataType)2, 27934 },
	{ (Il2CppRGCTXDataType)3, 21850 },
	{ (Il2CppRGCTXDataType)3, 21851 },
	{ (Il2CppRGCTXDataType)3, 21852 },
	{ (Il2CppRGCTXDataType)3, 21853 },
	{ (Il2CppRGCTXDataType)3, 21854 },
	{ (Il2CppRGCTXDataType)3, 21855 },
	{ (Il2CppRGCTXDataType)3, 21856 },
	{ (Il2CppRGCTXDataType)3, 21857 },
	{ (Il2CppRGCTXDataType)3, 21858 },
	{ (Il2CppRGCTXDataType)3, 21859 },
	{ (Il2CppRGCTXDataType)3, 21860 },
	{ (Il2CppRGCTXDataType)3, 21861 },
	{ (Il2CppRGCTXDataType)3, 21862 },
	{ (Il2CppRGCTXDataType)3, 21863 },
	{ (Il2CppRGCTXDataType)3, 21864 },
	{ (Il2CppRGCTXDataType)3, 21865 },
	{ (Il2CppRGCTXDataType)3, 21866 },
	{ (Il2CppRGCTXDataType)3, 21867 },
	{ (Il2CppRGCTXDataType)3, 21868 },
	{ (Il2CppRGCTXDataType)3, 21869 },
	{ (Il2CppRGCTXDataType)3, 21870 },
	{ (Il2CppRGCTXDataType)3, 21871 },
	{ (Il2CppRGCTXDataType)3, 21872 },
	{ (Il2CppRGCTXDataType)3, 21873 },
	{ (Il2CppRGCTXDataType)3, 21874 },
	{ (Il2CppRGCTXDataType)3, 21875 },
	{ (Il2CppRGCTXDataType)1, 24466 },
	{ (Il2CppRGCTXDataType)3, 21876 },
	{ (Il2CppRGCTXDataType)3, 21877 },
	{ (Il2CppRGCTXDataType)3, 21878 },
	{ (Il2CppRGCTXDataType)2, 27935 },
	{ (Il2CppRGCTXDataType)3, 21879 },
	{ (Il2CppRGCTXDataType)2, 27936 },
	{ (Il2CppRGCTXDataType)3, 21880 },
	{ (Il2CppRGCTXDataType)2, 27937 },
	{ (Il2CppRGCTXDataType)3, 21881 },
	{ (Il2CppRGCTXDataType)2, 24486 },
	{ (Il2CppRGCTXDataType)3, 21882 },
	{ (Il2CppRGCTXDataType)2, 24491 },
	{ (Il2CppRGCTXDataType)3, 21883 },
	{ (Il2CppRGCTXDataType)3, 21884 },
	{ (Il2CppRGCTXDataType)3, 21885 },
	{ (Il2CppRGCTXDataType)3, 21886 },
	{ (Il2CppRGCTXDataType)3, 21887 },
	{ (Il2CppRGCTXDataType)2, 24497 },
	{ (Il2CppRGCTXDataType)3, 21888 },
	{ (Il2CppRGCTXDataType)3, 21889 },
	{ (Il2CppRGCTXDataType)3, 21890 },
	{ (Il2CppRGCTXDataType)3, 21891 },
	{ (Il2CppRGCTXDataType)3, 21892 },
	{ (Il2CppRGCTXDataType)3, 21893 },
	{ (Il2CppRGCTXDataType)3, 21894 },
	{ (Il2CppRGCTXDataType)3, 21895 },
	{ (Il2CppRGCTXDataType)3, 21896 },
	{ (Il2CppRGCTXDataType)3, 21897 },
	{ (Il2CppRGCTXDataType)3, 21898 },
	{ (Il2CppRGCTXDataType)3, 21899 },
	{ (Il2CppRGCTXDataType)3, 21900 },
	{ (Il2CppRGCTXDataType)3, 21901 },
	{ (Il2CppRGCTXDataType)3, 21902 },
	{ (Il2CppRGCTXDataType)3, 21903 },
	{ (Il2CppRGCTXDataType)3, 21904 },
	{ (Il2CppRGCTXDataType)2, 24496 },
	{ (Il2CppRGCTXDataType)2, 24503 },
	{ (Il2CppRGCTXDataType)3, 21905 },
	{ (Il2CppRGCTXDataType)3, 21906 },
	{ (Il2CppRGCTXDataType)3, 21907 },
	{ (Il2CppRGCTXDataType)2, 24519 },
	{ (Il2CppRGCTXDataType)1, 24520 },
	{ (Il2CppRGCTXDataType)3, 21908 },
	{ (Il2CppRGCTXDataType)2, 24520 },
	{ (Il2CppRGCTXDataType)3, 21909 },
	{ (Il2CppRGCTXDataType)3, 21910 },
	{ (Il2CppRGCTXDataType)3, 21911 },
	{ (Il2CppRGCTXDataType)3, 21912 },
	{ (Il2CppRGCTXDataType)3, 21913 },
	{ (Il2CppRGCTXDataType)2, 24521 },
	{ (Il2CppRGCTXDataType)2, 27938 },
	{ (Il2CppRGCTXDataType)3, 21914 },
	{ (Il2CppRGCTXDataType)2, 24522 },
	{ (Il2CppRGCTXDataType)1, 24522 },
};
extern const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule;
const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule = 
{
	"Unity.ResourceManager.dll",
	631,
	s_methodPointers,
	86,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	39,
	s_rgctxIndices,
	301,
	s_rgctxValues,
	NULL,
};
