﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Lean.Common.LeanLoadScene::Load()
extern void LeanLoadScene_Load_mFA7580E440FBB98C9BE5641B51D3E73CCE8674E8 (void);
// 0x00000002 System.Void Lean.Common.LeanLoadScene::Load(System.String)
extern void LeanLoadScene_Load_mD7A63B8F1C4BCA4307ED7398AB0B532532F7F5CA (void);
// 0x00000003 System.Void Lean.Common.LeanLoadScene::.ctor()
extern void LeanLoadScene__ctor_m61A66416EBAE0BBDDC054CD49B00F9C03B3C045F (void);
// 0x00000004 System.Void Lean.Common.LeanPongBall::Awake()
extern void LeanPongBall_Awake_m62EAA2BE6E8749D7A189E9DF2D20F340E56AD110 (void);
// 0x00000005 System.Void Lean.Common.LeanPongBall::FixedUpdate()
extern void LeanPongBall_FixedUpdate_m4A4C5D1F906DD834210284CDC230A6A083D54B43 (void);
// 0x00000006 System.Void Lean.Common.LeanPongBall::ResetPositionAndVelocity()
extern void LeanPongBall_ResetPositionAndVelocity_mE9BEBDB8E4BA3BFD5FDD329FBFB4A526B5DB7F91 (void);
// 0x00000007 System.Void Lean.Common.LeanPongBall::.ctor()
extern void LeanPongBall__ctor_m671168DAB452268E3BFFB73305F53E6AA07FB9B8 (void);
// 0x00000008 System.Void Lean.Common.LeanChase::set_Destination(UnityEngine.Transform)
extern void LeanChase_set_Destination_mEBF64D402A6859947D4BEEBC7FE4C3DF3A49C64B (void);
// 0x00000009 UnityEngine.Transform Lean.Common.LeanChase::get_Destination()
extern void LeanChase_get_Destination_mDB53AC14115CDC221A48489879682FDEE9D93486 (void);
// 0x0000000A System.Void Lean.Common.LeanChase::SetPosition(UnityEngine.Vector3)
extern void LeanChase_SetPosition_mDE262AE936B73C26496E1E77AC3D633E74B74052 (void);
// 0x0000000B System.Void Lean.Common.LeanChase::SnapToPosition()
extern void LeanChase_SnapToPosition_m1FD14D68B0EF6735EDE1084FFE3F54B966F45709 (void);
// 0x0000000C System.Void Lean.Common.LeanChase::Start()
extern void LeanChase_Start_mABB43A0B840BA0414367B51398947F2757B16417 (void);
// 0x0000000D System.Void Lean.Common.LeanChase::Update()
extern void LeanChase_Update_mAE95DBCBBC17FDBDCF63165CFAE82B1CD2CE2EA3 (void);
// 0x0000000E System.Void Lean.Common.LeanChase::UpdatePosition(System.Single,System.Single)
extern void LeanChase_UpdatePosition_m3154D14EB40356F07A765FBD4C9D60D446876DF0 (void);
// 0x0000000F System.Void Lean.Common.LeanChase::.ctor()
extern void LeanChase__ctor_m50678584D570650D3689A4D91E142FC24DA47A13 (void);
// 0x00000010 System.Void Lean.Common.LeanChaseRigidbody::SetPosition(UnityEngine.Vector3)
extern void LeanChaseRigidbody_SetPosition_m3A1C01577CEDADC4C39AA25E938F2328CC4C7371 (void);
// 0x00000011 System.Void Lean.Common.LeanChaseRigidbody::OnEnable()
extern void LeanChaseRigidbody_OnEnable_mCAB62D9D7089FDACBD33012117E15B952FD330D0 (void);
// 0x00000012 System.Void Lean.Common.LeanChaseRigidbody::UpdatePosition(System.Single,System.Single)
extern void LeanChaseRigidbody_UpdatePosition_m8FF4B220D3CD24F62AE03435921CADC1A516D7F1 (void);
// 0x00000013 System.Void Lean.Common.LeanChaseRigidbody::LateUpdate()
extern void LeanChaseRigidbody_LateUpdate_m19F33310246D29308CFC283036631DBE1CEB3E35 (void);
// 0x00000014 System.Void Lean.Common.LeanChaseRigidbody::.ctor()
extern void LeanChaseRigidbody__ctor_m0B40186BA5BC09987E7AA28A3105DD6710509040 (void);
// 0x00000015 System.Void Lean.Common.LeanChaseRigidbody2D::set_Axis(Lean.Common.LeanChaseRigidbody2D/AxisType)
extern void LeanChaseRigidbody2D_set_Axis_m037D56BED6B81664334C3DC98AF678196BA63F57 (void);
// 0x00000016 Lean.Common.LeanChaseRigidbody2D/AxisType Lean.Common.LeanChaseRigidbody2D::get_Axis()
extern void LeanChaseRigidbody2D_get_Axis_mD8129F28DC62EB81CC8035934D2A0248356F5DA9 (void);
// 0x00000017 System.Void Lean.Common.LeanChaseRigidbody2D::SetPosition(UnityEngine.Vector3)
extern void LeanChaseRigidbody2D_SetPosition_m1BEE8ABC2307E528A19B28E7DA3FB401869E11D8 (void);
// 0x00000018 System.Void Lean.Common.LeanChaseRigidbody2D::OnEnable()
extern void LeanChaseRigidbody2D_OnEnable_mF17D06984F5209BA95E667A81188888EA9D3AD0F (void);
// 0x00000019 System.Void Lean.Common.LeanChaseRigidbody2D::UpdatePosition(System.Single,System.Single)
extern void LeanChaseRigidbody2D_UpdatePosition_mE2DBE19FEC8B32AA213963ED3CD20EF67D20582D (void);
// 0x0000001A System.Void Lean.Common.LeanChaseRigidbody2D::LateUpdate()
extern void LeanChaseRigidbody2D_LateUpdate_mF5C5AD625EF02A31CC4561DA20BD94BAE9D49B5B (void);
// 0x0000001B System.Void Lean.Common.LeanChaseRigidbody2D::.ctor()
extern void LeanChaseRigidbody2D__ctor_mA5A046402F971F70874D026F6BB74EBC4351408D (void);
// 0x0000001C System.Void Lean.Common.LeanConstrainLocalPosition::LateUpdate()
extern void LeanConstrainLocalPosition_LateUpdate_mBB2C7839929CEA77E5AE2E42FB3683D6614A790E (void);
// 0x0000001D System.Boolean Lean.Common.LeanConstrainLocalPosition::DoClamp(UnityEngine.Vector3&)
extern void LeanConstrainLocalPosition_DoClamp_mEF9DCF72ECCFF0D67458EF7FE6E89CAE2D709F1D (void);
// 0x0000001E System.Void Lean.Common.LeanConstrainLocalPosition::.ctor()
extern void LeanConstrainLocalPosition__ctor_m0C0B19A2832F74267F6C3EAF4B98E3AC1DBFDE19 (void);
// 0x0000001F System.Void Lean.Common.LeanConstrainScale::LateUpdate()
extern void LeanConstrainScale_LateUpdate_mCFD3A827D999951920F34A557A1692E7DF3781C3 (void);
// 0x00000020 System.Void Lean.Common.LeanConstrainScale::.ctor()
extern void LeanConstrainScale__ctor_m54DD63544BAAE8FD9CBD79C2DDFBEE515F1615F9 (void);
// 0x00000021 System.Void Lean.Common.LeanConstrainToAxis::LateUpdate()
extern void LeanConstrainToAxis_LateUpdate_m080C72F196EBCE7ECD741A9A8760A5EC69EA388F (void);
// 0x00000022 System.Void Lean.Common.LeanConstrainToAxis::.ctor()
extern void LeanConstrainToAxis__ctor_m761669EBC80EB87DA6C87B254C7EEFF1FAAE56AC (void);
// 0x00000023 System.Void Lean.Common.LeanConstrainToBox::LateUpdate()
extern void LeanConstrainToBox_LateUpdate_mD22F30B8BDDE6E3CDAA7852401A2D1D4405C0E83 (void);
// 0x00000024 System.Void Lean.Common.LeanConstrainToBox::OnDrawGizmosSelected()
extern void LeanConstrainToBox_OnDrawGizmosSelected_m579ABD51B3FC0C67415ED9F85175ED966A0BB663 (void);
// 0x00000025 System.Void Lean.Common.LeanConstrainToBox::.ctor()
extern void LeanConstrainToBox__ctor_m306BCB35A8AFD0349212BCF0052090845A84C0B4 (void);
// 0x00000026 System.Void Lean.Common.LeanConstrainToCollider::LateUpdate()
extern void LeanConstrainToCollider_LateUpdate_mB831BFC2A94E68CAD46885533E212A1A0AA4941C (void);
// 0x00000027 System.Void Lean.Common.LeanConstrainToCollider::.ctor()
extern void LeanConstrainToCollider__ctor_m49574AB33FE30FCF224EF078626DAA839F83E1CE (void);
// 0x00000028 System.Void Lean.Common.LeanConstrainToColliders::LateUpdate()
extern void LeanConstrainToColliders_LateUpdate_m7B4353C3FF44E9280896EDBC7A722C584368D2BA (void);
// 0x00000029 System.Void Lean.Common.LeanConstrainToColliders::.ctor()
extern void LeanConstrainToColliders__ctor_m0A01C293101707C13D7684DBC4E01FE52F4FE611 (void);
// 0x0000002A System.Void Lean.Common.LeanConstrainToDirection::LateUpdate()
extern void LeanConstrainToDirection_LateUpdate_m03411D8CF13318DFD2624CB9EC6605314BFF9DBF (void);
// 0x0000002B System.Void Lean.Common.LeanConstrainToDirection::.ctor()
extern void LeanConstrainToDirection__ctor_m6A02D6FF85792F40D27CF76EF5FDCD204E0F2417 (void);
// 0x0000002C System.Void Lean.Common.LeanConstrainToOrthographic::LateUpdate()
extern void LeanConstrainToOrthographic_LateUpdate_m7288138E2504E24CBA5E844373BE2991781817A2 (void);
// 0x0000002D System.Void Lean.Common.LeanConstrainToOrthographic::.ctor()
extern void LeanConstrainToOrthographic__ctor_m7DE32DDAAB5C64BEC610D0759EC41AC28AA2946E (void);
// 0x0000002E Lean.Common.LeanDelayedValue/FloatEvent Lean.Common.LeanDelayedValue::get_OnValueX()
extern void LeanDelayedValue_get_OnValueX_m06943E5747EFBB4D7D32EFFA8337AECB879D346E (void);
// 0x0000002F Lean.Common.LeanDelayedValue/FloatEvent Lean.Common.LeanDelayedValue::get_OnValueY()
extern void LeanDelayedValue_get_OnValueY_mE0A2E9779E6AB1B27E533889D70902E277FF95D4 (void);
// 0x00000030 Lean.Common.LeanDelayedValue/FloatEvent Lean.Common.LeanDelayedValue::get_OnValueZ()
extern void LeanDelayedValue_get_OnValueZ_m0E189F5998BC682FEAE6EED82811599E2848CA24 (void);
// 0x00000031 Lean.Common.LeanDelayedValue/Vector2Event Lean.Common.LeanDelayedValue::get_OnValueXY()
extern void LeanDelayedValue_get_OnValueXY_m4A9C91314DE14D8D0EBB01C0BA12C80A551EDE8C (void);
// 0x00000032 Lean.Common.LeanDelayedValue/Vector3Event Lean.Common.LeanDelayedValue::get_OnValueXYZ()
extern void LeanDelayedValue_get_OnValueXYZ_m57FC20493817B44F5305128E74D1EBE943A3A81F (void);
// 0x00000033 System.Void Lean.Common.LeanDelayedValue::SetX(System.Single)
extern void LeanDelayedValue_SetX_mB91ED053708ADBE6363B4E443B89182E34081C95 (void);
// 0x00000034 System.Void Lean.Common.LeanDelayedValue::SetY(System.Single)
extern void LeanDelayedValue_SetY_m3442668008B862B1DFE1154101CFF0E9A8D93258 (void);
// 0x00000035 System.Void Lean.Common.LeanDelayedValue::SetZ(System.Single)
extern void LeanDelayedValue_SetZ_mA12164EBB7FEA25A818C7567064BED59367FFC35 (void);
// 0x00000036 System.Void Lean.Common.LeanDelayedValue::SetXY(UnityEngine.Vector2)
extern void LeanDelayedValue_SetXY_m23F4493FB9B5B3E31027CBDB30F0E146383CB2A2 (void);
// 0x00000037 System.Void Lean.Common.LeanDelayedValue::SetXYZ(UnityEngine.Vector3)
extern void LeanDelayedValue_SetXYZ_mFBB5B0F9ED1E19F51DEC49E4A57C7F6C32713ABD (void);
// 0x00000038 System.Void Lean.Common.LeanDelayedValue::Clear()
extern void LeanDelayedValue_Clear_mBE1871969AA68803F0952823BD0C83E561E8BE11 (void);
// 0x00000039 System.Void Lean.Common.LeanDelayedValue::Update()
extern void LeanDelayedValue_Update_m08D7F2DABAD5D2E499A6C60C9F2CA2992EE165CB (void);
// 0x0000003A System.Void Lean.Common.LeanDelayedValue::Submit(UnityEngine.Vector3)
extern void LeanDelayedValue_Submit_mEE0E7B78BE0F8A472080EA262BD5EFADBE986131 (void);
// 0x0000003B System.Void Lean.Common.LeanDelayedValue::.ctor()
extern void LeanDelayedValue__ctor_mDE609DEF833BE6A5B0C890DBC127ED8F0483A2A5 (void);
// 0x0000003C UnityEngine.Events.UnityEvent Lean.Common.LeanFollow::get_OnReachedDestination()
extern void LeanFollow_get_OnReachedDestination_m9DC4AB7707BCE9E77E71B1F72D174086D12B87C3 (void);
// 0x0000003D System.Void Lean.Common.LeanFollow::ClearPositions()
extern void LeanFollow_ClearPositions_m882DF3EEC091B4475EF7D8E9245580E751A1DED5 (void);
// 0x0000003E System.Void Lean.Common.LeanFollow::SnapToNextPosition()
extern void LeanFollow_SnapToNextPosition_m7E40642AA78352FBD4E6E2CE185EAD9672A89ACF (void);
// 0x0000003F System.Void Lean.Common.LeanFollow::AddPosition(UnityEngine.Vector3)
extern void LeanFollow_AddPosition_m6FDAAC51971A43482EDADFB929366BDBB12A1715 (void);
// 0x00000040 System.Void Lean.Common.LeanFollow::Update()
extern void LeanFollow_Update_mE4FC7633F28DDE13FDC841881C16A5B86528812F (void);
// 0x00000041 System.Void Lean.Common.LeanFollow::TrimPositions()
extern void LeanFollow_TrimPositions_m6DE85761C9A8132F2423A8059B4DD01245B0AA3A (void);
// 0x00000042 System.Void Lean.Common.LeanFollow::.ctor()
extern void LeanFollow__ctor_mCC1968F35CB97AF0E472FF78F6664F9D162B8B80 (void);
// 0x00000043 System.Void Lean.Common.LeanMaintainDistance::AddDistance(System.Single)
extern void LeanMaintainDistance_AddDistance_m203DF6386DFD4FFD87210D24716AA56E2452AB79 (void);
// 0x00000044 System.Void Lean.Common.LeanMaintainDistance::MultiplyDistance(System.Single)
extern void LeanMaintainDistance_MultiplyDistance_mAACD05031247D21B269B2350F422A8365891D4AB (void);
// 0x00000045 System.Void Lean.Common.LeanMaintainDistance::Start()
extern void LeanMaintainDistance_Start_mE9F1B93D23661F4D9F026EF49AB93262C6C438F4 (void);
// 0x00000046 System.Void Lean.Common.LeanMaintainDistance::LateUpdate()
extern void LeanMaintainDistance_LateUpdate_m309EAD4F87D5DBD0DEA1E1D8A3E725C4979071FF (void);
// 0x00000047 System.Void Lean.Common.LeanMaintainDistance::.ctor()
extern void LeanMaintainDistance__ctor_m2FDE93F5292651228FF0CF139DCFD0958CB84448 (void);
// 0x00000048 System.Void Lean.Common.LeanManualRescale::set_Target(UnityEngine.GameObject)
extern void LeanManualRescale_set_Target_mB08A2074D51B43EEDD7F42FAF1FF70243244793A (void);
// 0x00000049 UnityEngine.GameObject Lean.Common.LeanManualRescale::get_Target()
extern void LeanManualRescale_get_Target_m3D0871EA0E644E026402F0ACA998619D53FA15D0 (void);
// 0x0000004A System.Void Lean.Common.LeanManualRescale::set_AxesA(UnityEngine.Vector3)
extern void LeanManualRescale_set_AxesA_m1473D459B9A279E3775758587717989DBC31BD9F (void);
// 0x0000004B UnityEngine.Vector3 Lean.Common.LeanManualRescale::get_AxesA()
extern void LeanManualRescale_get_AxesA_mDF5403217B9C2F6C00F8C0D5AF15F2E4EEEFCA1E (void);
// 0x0000004C System.Void Lean.Common.LeanManualRescale::set_AxesB(UnityEngine.Vector3)
extern void LeanManualRescale_set_AxesB_m811EF5522A2136C5DC290A9BD85DCE21FC1F434A (void);
// 0x0000004D UnityEngine.Vector3 Lean.Common.LeanManualRescale::get_AxesB()
extern void LeanManualRescale_get_AxesB_mE03F25569F1B7DBD0A1A97470E1CC38B53D6DB34 (void);
// 0x0000004E System.Void Lean.Common.LeanManualRescale::set_Multiplier(System.Single)
extern void LeanManualRescale_set_Multiplier_m82EC8D0980FCA8254780B33248E711BFBEDC21C4 (void);
// 0x0000004F System.Single Lean.Common.LeanManualRescale::get_Multiplier()
extern void LeanManualRescale_get_Multiplier_m0CE271A918F279418A64DBC2BD51CBD7D84C066D (void);
// 0x00000050 System.Void Lean.Common.LeanManualRescale::set_Damping(System.Single)
extern void LeanManualRescale_set_Damping_m2ED3EC22F2F715ACD171E5CD383DE20BA6CCFFA8 (void);
// 0x00000051 System.Single Lean.Common.LeanManualRescale::get_Damping()
extern void LeanManualRescale_get_Damping_mB79803A4C9977FA9CDE5E881F351758A903AFD12 (void);
// 0x00000052 System.Void Lean.Common.LeanManualRescale::set_ScaleByTime(System.Boolean)
extern void LeanManualRescale_set_ScaleByTime_m47D047762D08EEF29F6AF293F715BB86D73B5D26 (void);
// 0x00000053 System.Boolean Lean.Common.LeanManualRescale::get_ScaleByTime()
extern void LeanManualRescale_get_ScaleByTime_m2F309AE9C5EAA8F690B7E5813DD75A67D2479950 (void);
// 0x00000054 System.Void Lean.Common.LeanManualRescale::set_DefaultScale(UnityEngine.Vector3)
extern void LeanManualRescale_set_DefaultScale_mB41AA8F18F0E838C9AC5349D639EDCB432763991 (void);
// 0x00000055 UnityEngine.Vector3 Lean.Common.LeanManualRescale::get_DefaultScale()
extern void LeanManualRescale_get_DefaultScale_m5CB56F293FEDD76511F25ECF31190FADB3928535 (void);
// 0x00000056 System.Void Lean.Common.LeanManualRescale::ResetScale()
extern void LeanManualRescale_ResetScale_mE640226DC12DD0EFB1737A97BA576ED82F633371 (void);
// 0x00000057 System.Void Lean.Common.LeanManualRescale::SnapToTarget()
extern void LeanManualRescale_SnapToTarget_m10627A7E29ADA90CAE7504887A7D9A524F314E69 (void);
// 0x00000058 System.Void Lean.Common.LeanManualRescale::AddScaleA(System.Single)
extern void LeanManualRescale_AddScaleA_m28BEED55982C9FB11994764946F48EE2A7FC4009 (void);
// 0x00000059 System.Void Lean.Common.LeanManualRescale::AddScaleB(System.Single)
extern void LeanManualRescale_AddScaleB_m13D3F59ED04E1C83B33333AD77E0E3ADE48DB598 (void);
// 0x0000005A System.Void Lean.Common.LeanManualRescale::AddScaleAB(UnityEngine.Vector2)
extern void LeanManualRescale_AddScaleAB_m3BE77A9AE20EB496D174FFEDE58D60C13A5C4064 (void);
// 0x0000005B System.Void Lean.Common.LeanManualRescale::AddScale(UnityEngine.Vector3)
extern void LeanManualRescale_AddScale_m2C411006B42ECE60594C471C65832FB1200D46BE (void);
// 0x0000005C System.Void Lean.Common.LeanManualRescale::Update()
extern void LeanManualRescale_Update_mCB916F67EFEA1896F27EC8DB972F805F6DFCF8AC (void);
// 0x0000005D System.Void Lean.Common.LeanManualRescale::UpdateScale(System.Single)
extern void LeanManualRescale_UpdateScale_m8612B3083C3C09446055A5F378B77C712EA06C65 (void);
// 0x0000005E System.Void Lean.Common.LeanManualRescale::.ctor()
extern void LeanManualRescale__ctor_mC9A21E420A9537BCB11CAA1C903C6C5A150E5A3A (void);
// 0x0000005F System.Void Lean.Common.LeanManualRotate::set_Target(UnityEngine.GameObject)
extern void LeanManualRotate_set_Target_mB885E481B82A633FA09B2B68A57E2F781E147819 (void);
// 0x00000060 UnityEngine.GameObject Lean.Common.LeanManualRotate::get_Target()
extern void LeanManualRotate_get_Target_m09CD3F3A57F6D43F474938620EA5294F47C7567C (void);
// 0x00000061 System.Void Lean.Common.LeanManualRotate::set_Space(UnityEngine.Space)
extern void LeanManualRotate_set_Space_mE650F021EB03B99D69A6EEF1484DBFF446E01C2E (void);
// 0x00000062 UnityEngine.Space Lean.Common.LeanManualRotate::get_Space()
extern void LeanManualRotate_get_Space_m129BC692FE14560824C11C8E04F8334E6AFF0E5D (void);
// 0x00000063 System.Void Lean.Common.LeanManualRotate::set_AxisA(UnityEngine.Vector3)
extern void LeanManualRotate_set_AxisA_mE009D2CD80B7EBF811A48E82844EEFCF0DE7DFDE (void);
// 0x00000064 UnityEngine.Vector3 Lean.Common.LeanManualRotate::get_AxisA()
extern void LeanManualRotate_get_AxisA_m1D8FE0FA9438E39FB7111D7E1D6990613670AB71 (void);
// 0x00000065 System.Void Lean.Common.LeanManualRotate::set_AxisB(UnityEngine.Vector3)
extern void LeanManualRotate_set_AxisB_m6A01BFAB9B3688FF8ACD16C90255B01C0A269D78 (void);
// 0x00000066 UnityEngine.Vector3 Lean.Common.LeanManualRotate::get_AxisB()
extern void LeanManualRotate_get_AxisB_m6435E35A62C75AF7FAB2DCDB8C5B1C6CCF3E165D (void);
// 0x00000067 System.Void Lean.Common.LeanManualRotate::set_Multiplier(System.Single)
extern void LeanManualRotate_set_Multiplier_m3F2669DA6B225A006E77A81800370DDC5F2BED86 (void);
// 0x00000068 System.Single Lean.Common.LeanManualRotate::get_Multiplier()
extern void LeanManualRotate_get_Multiplier_mDB26646EAE2A22C7929C59E77A2B37A956FAD9C4 (void);
// 0x00000069 System.Void Lean.Common.LeanManualRotate::set_Damping(System.Single)
extern void LeanManualRotate_set_Damping_m088AA0C448C2C0EF59AA4A96FC8ADAB967715239 (void);
// 0x0000006A System.Single Lean.Common.LeanManualRotate::get_Damping()
extern void LeanManualRotate_get_Damping_mD689BDD320E1DEB6C02355040B836ED4C3FAB5DC (void);
// 0x0000006B System.Void Lean.Common.LeanManualRotate::set_ScaleByTime(System.Boolean)
extern void LeanManualRotate_set_ScaleByTime_m0D8331675DFF4789D49E031D82D9FCF38DB5764D (void);
// 0x0000006C System.Boolean Lean.Common.LeanManualRotate::get_ScaleByTime()
extern void LeanManualRotate_get_ScaleByTime_m49AF5494293F148543355119313F19A2B717D270 (void);
// 0x0000006D System.Void Lean.Common.LeanManualRotate::set_DefaultRotation(UnityEngine.Vector3)
extern void LeanManualRotate_set_DefaultRotation_m8531F09038BAB8CD13BB918F4BBDE3E890C74307 (void);
// 0x0000006E UnityEngine.Vector3 Lean.Common.LeanManualRotate::get_DefaultRotation()
extern void LeanManualRotate_get_DefaultRotation_m1107C2E86F159C8559FC48EECA501D6C08A55E03 (void);
// 0x0000006F System.Void Lean.Common.LeanManualRotate::ResetRotation()
extern void LeanManualRotate_ResetRotation_mC4100756507BC34F7C385E4E42B5D27B50EC3DFD (void);
// 0x00000070 System.Void Lean.Common.LeanManualRotate::SnapToTarget()
extern void LeanManualRotate_SnapToTarget_m7140EE483EB5A0385A34B4D3715A53D03F6C28D5 (void);
// 0x00000071 System.Void Lean.Common.LeanManualRotate::StopRotation()
extern void LeanManualRotate_StopRotation_mD836D0C7866EB8C9AF753463BF7CEB815B8E7F8E (void);
// 0x00000072 System.Void Lean.Common.LeanManualRotate::RotateA(System.Single)
extern void LeanManualRotate_RotateA_m5A8F339954C4022E609A779F79355BF19EF9AE2B (void);
// 0x00000073 System.Void Lean.Common.LeanManualRotate::RotateB(System.Single)
extern void LeanManualRotate_RotateB_m319F085726465850A8630CBB905A08364D8B4531 (void);
// 0x00000074 System.Void Lean.Common.LeanManualRotate::RotateAB(UnityEngine.Vector2)
extern void LeanManualRotate_RotateAB_m991ED3255F451E259B07B56038DEFFB35D8E137C (void);
// 0x00000075 System.Void Lean.Common.LeanManualRotate::Update()
extern void LeanManualRotate_Update_mBD7135B81C77797CE92FD98E8F3C46808514515A (void);
// 0x00000076 System.Void Lean.Common.LeanManualRotate::UpdateRotation(System.Single)
extern void LeanManualRotate_UpdateRotation_mDBB0A9176861D205C92E8B128DA3E3B023C31801 (void);
// 0x00000077 System.Void Lean.Common.LeanManualRotate::.ctor()
extern void LeanManualRotate__ctor_m3EB072B30C78F33EBF97151573C01F749866A4C3 (void);
// 0x00000078 System.Void Lean.Common.LeanManualTorque::AddTorqueA(System.Single)
extern void LeanManualTorque_AddTorqueA_mB4E1BE12462FC636F6C1F9A175DB65FA99E6558F (void);
// 0x00000079 System.Void Lean.Common.LeanManualTorque::AddTorqueB(System.Single)
extern void LeanManualTorque_AddTorqueB_mA9190C1CD08B97827294906BA561F037BB5458FB (void);
// 0x0000007A System.Void Lean.Common.LeanManualTorque::AddTorqueAB(UnityEngine.Vector2)
extern void LeanManualTorque_AddTorqueAB_mDE13792337D1F79DC1D51F1384EC5FE8FA5E07A6 (void);
// 0x0000007B System.Void Lean.Common.LeanManualTorque::AddTorqueFromTo(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanManualTorque_AddTorqueFromTo_m8DD178C9B02EB6E858C97C93A8A0FF892AF471B0 (void);
// 0x0000007C System.Void Lean.Common.LeanManualTorque::AddTorque(UnityEngine.Vector3)
extern void LeanManualTorque_AddTorque_m90271F96C1CC8C23B5C16F955401962427F305B9 (void);
// 0x0000007D System.Void Lean.Common.LeanManualTorque::.ctor()
extern void LeanManualTorque__ctor_m8B585E82E523982E489B6A317A5142ED099E20A3 (void);
// 0x0000007E System.Void Lean.Common.LeanManualTranslate::set_Target(UnityEngine.GameObject)
extern void LeanManualTranslate_set_Target_m57FDF437EEEE95649893635FDF0C6293D4CC862B (void);
// 0x0000007F UnityEngine.GameObject Lean.Common.LeanManualTranslate::get_Target()
extern void LeanManualTranslate_get_Target_m8FDE72E6486B26FC083A5EC251BF971CE7351517 (void);
// 0x00000080 System.Void Lean.Common.LeanManualTranslate::set_Space(UnityEngine.Space)
extern void LeanManualTranslate_set_Space_mAB93D871FAE6584B77C8C68F35892EAE39DA5532 (void);
// 0x00000081 UnityEngine.Space Lean.Common.LeanManualTranslate::get_Space()
extern void LeanManualTranslate_get_Space_mF46BEA76A855B3B7F24FE100A3F9E0E9E98B39B5 (void);
// 0x00000082 System.Void Lean.Common.LeanManualTranslate::set_DirectionA(UnityEngine.Vector3)
extern void LeanManualTranslate_set_DirectionA_m3F210C270534CA943C4442298B276BB9F7AE11A8 (void);
// 0x00000083 UnityEngine.Vector3 Lean.Common.LeanManualTranslate::get_DirectionA()
extern void LeanManualTranslate_get_DirectionA_m91037C51E60504C546273AB45024E0ED261537F7 (void);
// 0x00000084 System.Void Lean.Common.LeanManualTranslate::set_DirectionB(UnityEngine.Vector3)
extern void LeanManualTranslate_set_DirectionB_m9082631EA0581E4B1BC95228A7366718E4D143E1 (void);
// 0x00000085 UnityEngine.Vector3 Lean.Common.LeanManualTranslate::get_DirectionB()
extern void LeanManualTranslate_get_DirectionB_m6F1FA204FFE559DD574261D36D5A5EA723839E1A (void);
// 0x00000086 System.Void Lean.Common.LeanManualTranslate::set_Multiplier(System.Single)
extern void LeanManualTranslate_set_Multiplier_mFE34753057EAB34DF5AB909625980FDFD4D79058 (void);
// 0x00000087 System.Single Lean.Common.LeanManualTranslate::get_Multiplier()
extern void LeanManualTranslate_get_Multiplier_m8A44B4B85BD8AC3516373CE40D321C5F0726B54B (void);
// 0x00000088 System.Void Lean.Common.LeanManualTranslate::set_Damping(System.Single)
extern void LeanManualTranslate_set_Damping_m179FB6114BF2A71F7604BA1CA017EF26891757B0 (void);
// 0x00000089 System.Single Lean.Common.LeanManualTranslate::get_Damping()
extern void LeanManualTranslate_get_Damping_m90BA63E70ED1329310F6FB3066AD5D8588C9141C (void);
// 0x0000008A System.Void Lean.Common.LeanManualTranslate::set_ScaleByTime(System.Boolean)
extern void LeanManualTranslate_set_ScaleByTime_mE8E12040FD556308FF55DB96C5268628C3CB8310 (void);
// 0x0000008B System.Boolean Lean.Common.LeanManualTranslate::get_ScaleByTime()
extern void LeanManualTranslate_get_ScaleByTime_m6337B6A654D1C26CD12BC75C030AEC8A47AB78E0 (void);
// 0x0000008C System.Void Lean.Common.LeanManualTranslate::set_DefaultPosition(UnityEngine.Vector3)
extern void LeanManualTranslate_set_DefaultPosition_m3474CA88DE1870D2FE325EFBA2A6EEF796E7A266 (void);
// 0x0000008D UnityEngine.Vector3 Lean.Common.LeanManualTranslate::get_DefaultPosition()
extern void LeanManualTranslate_get_DefaultPosition_m382699B209CA6AC2EFF769BE5A76B8E439E24666 (void);
// 0x0000008E System.Void Lean.Common.LeanManualTranslate::ResetPosition()
extern void LeanManualTranslate_ResetPosition_mE8090325C2223064B3DF3A7BDB32DFF926449A6F (void);
// 0x0000008F System.Void Lean.Common.LeanManualTranslate::SnapToTarget()
extern void LeanManualTranslate_SnapToTarget_mFFBD6EA73CD0EF67BC6F9EA431A5D3C84F7AAA7D (void);
// 0x00000090 System.Void Lean.Common.LeanManualTranslate::TranslateA(System.Single)
extern void LeanManualTranslate_TranslateA_m59274DF458CFF99559987823746B4225637279E1 (void);
// 0x00000091 System.Void Lean.Common.LeanManualTranslate::TranslateB(System.Single)
extern void LeanManualTranslate_TranslateB_mE9924B7FAB2566AF5C51E92110D4BB3D6AA35048 (void);
// 0x00000092 System.Void Lean.Common.LeanManualTranslate::TranslateAB(UnityEngine.Vector2)
extern void LeanManualTranslate_TranslateAB_mA833B6AE51514DE1811BEAF6AC9BE20705F70198 (void);
// 0x00000093 System.Void Lean.Common.LeanManualTranslate::Translate(UnityEngine.Vector3)
extern void LeanManualTranslate_Translate_mA2CB0F9252C319E05F261140F7E3A91C42162E46 (void);
// 0x00000094 System.Void Lean.Common.LeanManualTranslate::TranslateWorld(UnityEngine.Vector3)
extern void LeanManualTranslate_TranslateWorld_mC26815602C794D26B8E32A04A6AD6C2F0329EF33 (void);
// 0x00000095 System.Void Lean.Common.LeanManualTranslate::Update()
extern void LeanManualTranslate_Update_mCEC613DDF7A3BD1D850B1FD1613FBE9F6330C35A (void);
// 0x00000096 System.Void Lean.Common.LeanManualTranslate::UpdatePosition(System.Single)
extern void LeanManualTranslate_UpdatePosition_m05D76DC74F3EA85D07696607E1FC35226AAD5916 (void);
// 0x00000097 System.Void Lean.Common.LeanManualTranslate::.ctor()
extern void LeanManualTranslate__ctor_m265081E9C0596939EB1A9D613909320368103E8C (void);
// 0x00000098 System.Void Lean.Common.LeanManualTranslateRigidbody::TranslateA(System.Single)
extern void LeanManualTranslateRigidbody_TranslateA_mBA0B202F5C27D66F2EB7222BF642CFA8D789011B (void);
// 0x00000099 System.Void Lean.Common.LeanManualTranslateRigidbody::TranslateB(System.Single)
extern void LeanManualTranslateRigidbody_TranslateB_m4E3D275BD568613065D2E998EDB81F517553380E (void);
// 0x0000009A System.Void Lean.Common.LeanManualTranslateRigidbody::TranslateAB(UnityEngine.Vector2)
extern void LeanManualTranslateRigidbody_TranslateAB_m025CB60090C18EC9AE4751A9A15263DF8BE9A4C7 (void);
// 0x0000009B System.Void Lean.Common.LeanManualTranslateRigidbody::Translate(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody_Translate_mE8CA00F0910E9DCC0CF3206DF0462EDEA432B70D (void);
// 0x0000009C System.Void Lean.Common.LeanManualTranslateRigidbody::TranslateWorld(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody_TranslateWorld_mB94CB1BD22350D0B52F538F68BAE2EDFE57BCCD5 (void);
// 0x0000009D System.Void Lean.Common.LeanManualTranslateRigidbody::Update()
extern void LeanManualTranslateRigidbody_Update_mB3AE43AF3E8A8C050AF22F6E5C422573929B8FA5 (void);
// 0x0000009E System.Void Lean.Common.LeanManualTranslateRigidbody::.ctor()
extern void LeanManualTranslateRigidbody__ctor_m20FDA772AF498C5572CE6445927CEBEFD09E061A (void);
// 0x0000009F System.Void Lean.Common.LeanManualTranslateRigidbody2D::TranslateA(System.Single)
extern void LeanManualTranslateRigidbody2D_TranslateA_m6D4F70FB2D30C9A989CF7082B4A4D28DD3998D79 (void);
// 0x000000A0 System.Void Lean.Common.LeanManualTranslateRigidbody2D::TranslateB(System.Single)
extern void LeanManualTranslateRigidbody2D_TranslateB_mADB8A4F07DA0B4BB19AF4039118C40C7E8F7C995 (void);
// 0x000000A1 System.Void Lean.Common.LeanManualTranslateRigidbody2D::TranslateAB(UnityEngine.Vector2)
extern void LeanManualTranslateRigidbody2D_TranslateAB_m9F867F5F11E532263AEA8F0035D7346E5B76D001 (void);
// 0x000000A2 System.Void Lean.Common.LeanManualTranslateRigidbody2D::Translate(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody2D_Translate_mE111E55D49E155538E4782D2EA20F9EC9DB52275 (void);
// 0x000000A3 System.Void Lean.Common.LeanManualTranslateRigidbody2D::TranslateWorld(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody2D_TranslateWorld_mE17AAB380DA913AEA08CACFEFF97D27FDCE5D02A (void);
// 0x000000A4 System.Void Lean.Common.LeanManualTranslateRigidbody2D::FixedUpdate()
extern void LeanManualTranslateRigidbody2D_FixedUpdate_m8276A252FA5D24EBD48504E835AFAD19C1F67213 (void);
// 0x000000A5 System.Void Lean.Common.LeanManualTranslateRigidbody2D::Update()
extern void LeanManualTranslateRigidbody2D_Update_m762124D0AE451F813007300A654E42FFCDAAFA41 (void);
// 0x000000A6 System.Void Lean.Common.LeanManualTranslateRigidbody2D::.ctor()
extern void LeanManualTranslateRigidbody2D__ctor_mD9CB477D8FC5BE2048903BFA2C7FB1DDA93CB54D (void);
// 0x000000A7 System.Void Lean.Common.LeanOrbit::Rotate(UnityEngine.Vector2)
extern void LeanOrbit_Rotate_m2978C60AFC28CCB0E0F602C3E2E283C2ECCC6331 (void);
// 0x000000A8 System.Void Lean.Common.LeanOrbit::RotatePitch(System.Single)
extern void LeanOrbit_RotatePitch_m171D3F078CC522A2D69DD3F1CB4A55169D689C53 (void);
// 0x000000A9 System.Void Lean.Common.LeanOrbit::RotateYaw(System.Single)
extern void LeanOrbit_RotateYaw_mE3A55FDE0FF18849953516D687ABA80BD1A8D151 (void);
// 0x000000AA System.Void Lean.Common.LeanOrbit::Start()
extern void LeanOrbit_Start_mD2AA0DA08032B63E998EF0743D959C8BE1BF52CE (void);
// 0x000000AB System.Void Lean.Common.LeanOrbit::LateUpdate()
extern void LeanOrbit_LateUpdate_mE401BE25E27951AF94482F3A4C46E649699F5AE8 (void);
// 0x000000AC System.Single Lean.Common.LeanOrbit::GetSensitivity()
extern void LeanOrbit_GetSensitivity_mE55D03EEAB010940503367634E1F3720D87FEDAF (void);
// 0x000000AD System.Void Lean.Common.LeanOrbit::.ctor()
extern void LeanOrbit__ctor_mB3024B182676E90C121AA88262FC90455F75E753 (void);
// 0x000000AE System.Void Lean.Common.LeanPitchYaw::ResetRotation()
extern void LeanPitchYaw_ResetRotation_m13CF6D58ABEB93F446EAFFE29CC4D97313345EEB (void);
// 0x000000AF System.Void Lean.Common.LeanPitchYaw::RotateToPosition(UnityEngine.Vector3)
extern void LeanPitchYaw_RotateToPosition_mE09CF6CB904116706B24A68AFFBEC6F992A80627 (void);
// 0x000000B0 System.Void Lean.Common.LeanPitchYaw::RotateToDirection(UnityEngine.Vector3)
extern void LeanPitchYaw_RotateToDirection_mFAF6EF14A236AE4C61FE6BF078D24613ED60BDA7 (void);
// 0x000000B1 System.Void Lean.Common.LeanPitchYaw::SetPitch(System.Single)
extern void LeanPitchYaw_SetPitch_m8A53BFF6FCF4C34818A1F711EFD7A657E786C014 (void);
// 0x000000B2 System.Void Lean.Common.LeanPitchYaw::SetYaw(System.Single)
extern void LeanPitchYaw_SetYaw_m5503ED71BBC7D5B4CC5F846F1611212640789C11 (void);
// 0x000000B3 System.Void Lean.Common.LeanPitchYaw::RotateToScreenPosition(UnityEngine.Vector2)
extern void LeanPitchYaw_RotateToScreenPosition_m6DDAA3FC33B9729A3A17D39A94A2A3852422F15A (void);
// 0x000000B4 System.Void Lean.Common.LeanPitchYaw::Rotate(UnityEngine.Vector2)
extern void LeanPitchYaw_Rotate_m6F20D1A4C3E1E24D4F08DE2AD69895152833CED6 (void);
// 0x000000B5 System.Void Lean.Common.LeanPitchYaw::RotatePitch(System.Single)
extern void LeanPitchYaw_RotatePitch_m605338F4EA223C332A76833DF8D93AD90DFD0999 (void);
// 0x000000B6 System.Void Lean.Common.LeanPitchYaw::RotateYaw(System.Single)
extern void LeanPitchYaw_RotateYaw_mCABB4BC8E2AC83B0EB05BE9F5AFC5D2F58D230CD (void);
// 0x000000B7 System.Void Lean.Common.LeanPitchYaw::Start()
extern void LeanPitchYaw_Start_mD8D640E75C536EF037700CABB040ED72F71E0F4E (void);
// 0x000000B8 System.Void Lean.Common.LeanPitchYaw::LateUpdate()
extern void LeanPitchYaw_LateUpdate_mD4ABC222F1F05DCFD444D5C1D6F58A91847A3242 (void);
// 0x000000B9 System.Single Lean.Common.LeanPitchYaw::GetSensitivity()
extern void LeanPitchYaw_GetSensitivity_m67CB21E47092B27B53761158B6BBAEC2CE82E0DE (void);
// 0x000000BA System.Void Lean.Common.LeanPitchYaw::.ctor()
extern void LeanPitchYaw__ctor_mD6C4406B081FBA2E6119AB06F24F85D987EC2BC1 (void);
// 0x000000BB System.Void Lean.Common.LeanPitchYawAutoRotate::OnEnable()
extern void LeanPitchYawAutoRotate_OnEnable_m3983342BA9B67602E32F2D75013AAA6121EFCA18 (void);
// 0x000000BC System.Void Lean.Common.LeanPitchYawAutoRotate::LateUpdate()
extern void LeanPitchYawAutoRotate_LateUpdate_m372767D5F2FCE1700373D03218808B5AF94B1C44 (void);
// 0x000000BD System.Void Lean.Common.LeanPitchYawAutoRotate::.ctor()
extern void LeanPitchYawAutoRotate__ctor_m9488F8B9FBB7EA008A3ABE0C30F35323133C02DF (void);
// 0x000000BE System.Void Lean.Common.LeanRevertTransform::Start()
extern void LeanRevertTransform_Start_m10EA69D75DC0898D55B85C71DA455B4AFD54DCF7 (void);
// 0x000000BF System.Void Lean.Common.LeanRevertTransform::Revert()
extern void LeanRevertTransform_Revert_mCEC35C80F80A3E1E3816C57A5714E289CE89B61F (void);
// 0x000000C0 System.Void Lean.Common.LeanRevertTransform::StopRevert()
extern void LeanRevertTransform_StopRevert_m21B74CCEBFC897C98091567F071A726F40A67D38 (void);
// 0x000000C1 System.Void Lean.Common.LeanRevertTransform::RecordTransform()
extern void LeanRevertTransform_RecordTransform_m641F557BB14F4AD624EF6947C8EFDEB1FD0A6478 (void);
// 0x000000C2 System.Void Lean.Common.LeanRevertTransform::Update()
extern void LeanRevertTransform_Update_m8B7B02EC480655272682ADBCCB7F868B15415710 (void);
// 0x000000C3 System.Boolean Lean.Common.LeanRevertTransform::ReachedTarget()
extern void LeanRevertTransform_ReachedTarget_m8F3AE66D8E5EA517930336E51D44C97B34EC91C5 (void);
// 0x000000C4 System.Void Lean.Common.LeanRevertTransform::.ctor()
extern void LeanRevertTransform__ctor_m628FED4F7DA09B4807CF3125C513A149A1D6D6A7 (void);
// 0x000000C5 UnityEngine.Transform Lean.Common.LeanRotateToPosition::get_FinalTransform()
extern void LeanRotateToPosition_get_FinalTransform_mAB675B2F22F390B3D2842FA5272F70861D5B2DEE (void);
// 0x000000C6 System.Void Lean.Common.LeanRotateToPosition::SetPosition(UnityEngine.Vector3)
extern void LeanRotateToPosition_SetPosition_m65321E7AA33648A6B1B57DB1D0681D13D7286A6D (void);
// 0x000000C7 System.Void Lean.Common.LeanRotateToPosition::SetDelta(UnityEngine.Vector3)
extern void LeanRotateToPosition_SetDelta_mD8192586859342AC7BEC63619CB888D8F24333C9 (void);
// 0x000000C8 System.Void Lean.Common.LeanRotateToPosition::ResetPosition()
extern void LeanRotateToPosition_ResetPosition_m6FD2DA2BFD3E065D7FD42E78D5025B5E399A5ED1 (void);
// 0x000000C9 System.Void Lean.Common.LeanRotateToPosition::Start()
extern void LeanRotateToPosition_Start_m0C8ADED3E536D212FC8A274B63F24F97C96ACC75 (void);
// 0x000000CA System.Void Lean.Common.LeanRotateToPosition::OnEnable()
extern void LeanRotateToPosition_OnEnable_m7E19CCC4F668BD6C5E7A03921892FB80275C7E12 (void);
// 0x000000CB System.Void Lean.Common.LeanRotateToPosition::LateUpdate()
extern void LeanRotateToPosition_LateUpdate_m95EC07C3E76716E188088B6D3AA6F8BE02E2AA4D (void);
// 0x000000CC System.Void Lean.Common.LeanRotateToPosition::UpdateRotation(UnityEngine.Transform,UnityEngine.Vector3)
extern void LeanRotateToPosition_UpdateRotation_mA2423BE6A2DF7952514FF308BB65FBC93A8EF0C9 (void);
// 0x000000CD System.Void Lean.Common.LeanRotateToPosition::.ctor()
extern void LeanRotateToPosition__ctor_m35D6F36810CC6B7C905F10470B8AF4BEDA636384 (void);
// 0x000000CE System.Void Lean.Common.LeanRotateToRigidbody2D::OnEnable()
extern void LeanRotateToRigidbody2D_OnEnable_mADF072537833661B7199C59E3482066AD72AE0F6 (void);
// 0x000000CF System.Void Lean.Common.LeanRotateToRigidbody2D::Start()
extern void LeanRotateToRigidbody2D_Start_m87D8831571BE92B1D1CBA87A59B68BB99880A128 (void);
// 0x000000D0 System.Void Lean.Common.LeanRotateToRigidbody2D::LateUpdate()
extern void LeanRotateToRigidbody2D_LateUpdate_m07797EDF26F34CCF79C8D323C272ACD26A46AA51 (void);
// 0x000000D1 System.Void Lean.Common.LeanRotateToRigidbody2D::.ctor()
extern void LeanRotateToRigidbody2D__ctor_mE03111FE29A90A38F08457449B18DDB64E7E8F7C (void);
// 0x000000D2 Lean.Common.LeanSmoothedValue/FloatEvent Lean.Common.LeanSmoothedValue::get_OnValueX()
extern void LeanSmoothedValue_get_OnValueX_m48F8BE42CACED3665054D9FA9D5115CD3CC17AA8 (void);
// 0x000000D3 Lean.Common.LeanSmoothedValue/FloatEvent Lean.Common.LeanSmoothedValue::get_OnValueY()
extern void LeanSmoothedValue_get_OnValueY_m0C50AC2FD067D255F75F46A76E813C8F98A9F7A1 (void);
// 0x000000D4 Lean.Common.LeanSmoothedValue/FloatEvent Lean.Common.LeanSmoothedValue::get_OnValueZ()
extern void LeanSmoothedValue_get_OnValueZ_m749217C6FE670753ED3F4AA0F29752C4A806822B (void);
// 0x000000D5 Lean.Common.LeanSmoothedValue/Vector2Event Lean.Common.LeanSmoothedValue::get_OnValueXY()
extern void LeanSmoothedValue_get_OnValueXY_m0E8E886063D6C0A8D2B9590DC04497EAC2D356F5 (void);
// 0x000000D6 Lean.Common.LeanSmoothedValue/Vector3Event Lean.Common.LeanSmoothedValue::get_OnValueXYZ()
extern void LeanSmoothedValue_get_OnValueXYZ_mDCA2262167A27F0A5C2185DBBF392B318CC68C4C (void);
// 0x000000D7 System.Void Lean.Common.LeanSmoothedValue::SetX(System.Single)
extern void LeanSmoothedValue_SetX_mA85882DC41C7DC971A65C681E4EE351349D38348 (void);
// 0x000000D8 System.Void Lean.Common.LeanSmoothedValue::SetY(System.Single)
extern void LeanSmoothedValue_SetY_mF06EEC70FAB07165DA5D34F5AEB343FFE160D578 (void);
// 0x000000D9 System.Void Lean.Common.LeanSmoothedValue::SetZ(System.Single)
extern void LeanSmoothedValue_SetZ_m0214415B5619350F8F2F7B9B5418D12921282428 (void);
// 0x000000DA System.Void Lean.Common.LeanSmoothedValue::SetXY(UnityEngine.Vector2)
extern void LeanSmoothedValue_SetXY_m0358D08B89BE7E44C33583110C2468D779D1F736 (void);
// 0x000000DB System.Void Lean.Common.LeanSmoothedValue::SetXYZ(UnityEngine.Vector3)
extern void LeanSmoothedValue_SetXYZ_m7024326070ECC67F4B2812E2CCA1DE6204C2536D (void);
// 0x000000DC System.Void Lean.Common.LeanSmoothedValue::SnapToTarget()
extern void LeanSmoothedValue_SnapToTarget_m938E84C230A60B8A82364857151770B1827A846A (void);
// 0x000000DD System.Void Lean.Common.LeanSmoothedValue::Stop()
extern void LeanSmoothedValue_Stop_mBE412AC72823BE3FCE8372380D02CAE790C7F993 (void);
// 0x000000DE System.Void Lean.Common.LeanSmoothedValue::Update()
extern void LeanSmoothedValue_Update_m030E03BB1548A1A4E78CAE781B302407D17B874D (void);
// 0x000000DF System.Void Lean.Common.LeanSmoothedValue::Submit(UnityEngine.Vector3)
extern void LeanSmoothedValue_Submit_m195C4D7091E141CD43AF302796AC2C74F0E7C716 (void);
// 0x000000E0 System.Void Lean.Common.LeanSmoothedValue::.ctor()
extern void LeanSmoothedValue__ctor_m1DA1E026A727C7FE802D21F053F8FA5025A43815 (void);
// 0x000000E1 System.Void Lean.Common.LeanSpawnBetween::Spawn(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanSpawnBetween_Spawn_m3D1C633BD8C22BB23CAF5C49CA3CE95E1323412E (void);
// 0x000000E2 System.Void Lean.Common.LeanSpawnBetween::.ctor()
extern void LeanSpawnBetween__ctor_m853DC15F75A5282C7BAC22A9851C88CF87BD6175 (void);
// 0x000000E3 System.Void Lean.Common.LeanSwap::UpdateSwap()
extern void LeanSwap_UpdateSwap_m8775432B9932F36F5BA81D1AF725AFC55CAF8AA3 (void);
// 0x000000E4 System.Void Lean.Common.LeanSwap::SwapTo(System.Int32)
extern void LeanSwap_SwapTo_mD9F490AC477280019B645CDFF4CF7142C0ECA2FF (void);
// 0x000000E5 System.Void Lean.Common.LeanSwap::SwapToPrevious()
extern void LeanSwap_SwapToPrevious_m922F0A3BE491764218F008C05769361B32962ED3 (void);
// 0x000000E6 System.Void Lean.Common.LeanSwap::SwapToNext()
extern void LeanSwap_SwapToNext_m74CEC15055B51912B786213F1FCF03B62CEE2705 (void);
// 0x000000E7 UnityEngine.Transform Lean.Common.LeanSwap::GetPrefab()
extern void LeanSwap_GetPrefab_m9936DACFCD30D9042F53D1A83B78DBC66FE070AD (void);
// 0x000000E8 System.Void Lean.Common.LeanSwap::.ctor()
extern void LeanSwap__ctor_m60DA7812034657247A220259D1B4615D5BA34D3E (void);
// 0x000000E9 Lean.Common.LeanThresholdDelta/FloatEvent Lean.Common.LeanThresholdDelta::get_OnDeltaX()
extern void LeanThresholdDelta_get_OnDeltaX_mDE54FE4E639D516FF715AA82F2D208058EB361D0 (void);
// 0x000000EA Lean.Common.LeanThresholdDelta/FloatEvent Lean.Common.LeanThresholdDelta::get_OnDeltaY()
extern void LeanThresholdDelta_get_OnDeltaY_m4397E2D65003FBEF2D2A4A7D025C32088B2A5067 (void);
// 0x000000EB Lean.Common.LeanThresholdDelta/FloatEvent Lean.Common.LeanThresholdDelta::get_OnDeltaZ()
extern void LeanThresholdDelta_get_OnDeltaZ_m9FC9A71FFAB85055855FCD64F48EE2912D80E9A3 (void);
// 0x000000EC Lean.Common.LeanThresholdDelta/Vector2Event Lean.Common.LeanThresholdDelta::get_OnDeltaXY()
extern void LeanThresholdDelta_get_OnDeltaXY_mA2C666259942A53B2F2F3C8C446571FA083156FF (void);
// 0x000000ED Lean.Common.LeanThresholdDelta/Vector3Event Lean.Common.LeanThresholdDelta::get_OnDeltaXYZ()
extern void LeanThresholdDelta_get_OnDeltaXYZ_mEEF8A96AF9BBA26B3EFA8AB408FA2448B1CF5929 (void);
// 0x000000EE System.Void Lean.Common.LeanThresholdDelta::AddXY(UnityEngine.Vector2)
extern void LeanThresholdDelta_AddXY_m7C92EE6DDB7604485B331447FB8B86037F8F79C9 (void);
// 0x000000EF System.Void Lean.Common.LeanThresholdDelta::AddXYZ(UnityEngine.Vector3)
extern void LeanThresholdDelta_AddXYZ_m3459C0789546C3C98AA62E7DCA3B61244225DE0E (void);
// 0x000000F0 System.Void Lean.Common.LeanThresholdDelta::AddX(System.Single)
extern void LeanThresholdDelta_AddX_m84433F17454CFA99903A349635A893CB0BC957C9 (void);
// 0x000000F1 System.Void Lean.Common.LeanThresholdDelta::AddY(System.Single)
extern void LeanThresholdDelta_AddY_mF07895A3BD45E1244045D82A64C601C962C0FE59 (void);
// 0x000000F2 System.Void Lean.Common.LeanThresholdDelta::AddZ(System.Single)
extern void LeanThresholdDelta_AddZ_mDC7ED0BE94565E0A27D0C08621AC67A792F507A0 (void);
// 0x000000F3 System.Void Lean.Common.LeanThresholdDelta::Update()
extern void LeanThresholdDelta_Update_m824E3651FFADE99F09AC41607EE59B228C580291 (void);
// 0x000000F4 System.Void Lean.Common.LeanThresholdDelta::.ctor()
extern void LeanThresholdDelta__ctor_m33F98962F123D4472B403F32462F7E10E14AFA32 (void);
// 0x000000F5 Lean.Common.LeanThresholdPosition/FloatEvent Lean.Common.LeanThresholdPosition::get_OnPositionX()
extern void LeanThresholdPosition_get_OnPositionX_m260F8DAF211804D1BAEA355B29256CE43D2CAF8F (void);
// 0x000000F6 Lean.Common.LeanThresholdPosition/FloatEvent Lean.Common.LeanThresholdPosition::get_OnPositionY()
extern void LeanThresholdPosition_get_OnPositionY_mE5B80D4008420ED4F58DB2544E76EE284641190B (void);
// 0x000000F7 Lean.Common.LeanThresholdPosition/FloatEvent Lean.Common.LeanThresholdPosition::get_OnPositionZ()
extern void LeanThresholdPosition_get_OnPositionZ_m6237867CBA886ABD202B2F44038AB1173CB9ECD0 (void);
// 0x000000F8 Lean.Common.LeanThresholdPosition/Vector2Event Lean.Common.LeanThresholdPosition::get_OnPositionXY()
extern void LeanThresholdPosition_get_OnPositionXY_m23577E64474D5AEC8EE4D6E37E0FA1D1F6AA4774 (void);
// 0x000000F9 Lean.Common.LeanThresholdPosition/Vector3Event Lean.Common.LeanThresholdPosition::get_OnPositionXYZ()
extern void LeanThresholdPosition_get_OnPositionXYZ_m6626B35F585ADE91DBE506FDFBC0AFAA9A1804E6 (void);
// 0x000000FA System.Void Lean.Common.LeanThresholdPosition::AddXY(UnityEngine.Vector2)
extern void LeanThresholdPosition_AddXY_mAA3C1F480BA8494220FAFE251F26C0080D80DA5C (void);
// 0x000000FB System.Void Lean.Common.LeanThresholdPosition::AddXYZ(UnityEngine.Vector3)
extern void LeanThresholdPosition_AddXYZ_m9798D69833641EC17C4FB81AE4F9F0CFD572A7C8 (void);
// 0x000000FC System.Void Lean.Common.LeanThresholdPosition::AddX(System.Single)
extern void LeanThresholdPosition_AddX_mA0A351B6F0997D920802377156AFE602BF32D898 (void);
// 0x000000FD System.Void Lean.Common.LeanThresholdPosition::AddY(System.Single)
extern void LeanThresholdPosition_AddY_mD68F489C04324123F20373441E19B741BAB9E129 (void);
// 0x000000FE System.Void Lean.Common.LeanThresholdPosition::AddZ(System.Single)
extern void LeanThresholdPosition_AddZ_mDCB09A571756D2C1430BC06C7B77B81074D66B67 (void);
// 0x000000FF System.Void Lean.Common.LeanThresholdPosition::SetXY(UnityEngine.Vector2)
extern void LeanThresholdPosition_SetXY_m4D78BB4110EC376A15EB02B67A7E0D7B8E42EDF2 (void);
// 0x00000100 System.Void Lean.Common.LeanThresholdPosition::SetXYZ(UnityEngine.Vector3)
extern void LeanThresholdPosition_SetXYZ_mE541C1AA0627213FF9E7A05A23F388E20AB5C817 (void);
// 0x00000101 System.Void Lean.Common.LeanThresholdPosition::Update()
extern void LeanThresholdPosition_Update_m32DB07B5B1FB84F5442A8683A12FA1A52BF9EB2D (void);
// 0x00000102 System.Void Lean.Common.LeanThresholdPosition::.ctor()
extern void LeanThresholdPosition__ctor_m643369A9E09C0B91EB979F71C5A62D2855D4E8D7 (void);
// 0x00000103 System.Void Lean.Common.LeanDelayedValue/FloatEvent::.ctor()
extern void FloatEvent__ctor_m46B54DF2F7D74451F1A3FA6A1E74FD32DF9B9FE7 (void);
// 0x00000104 System.Void Lean.Common.LeanDelayedValue/Vector2Event::.ctor()
extern void Vector2Event__ctor_mB9621DEBD410A8711EA0FA708ADE2EF5C2C1DD8B (void);
// 0x00000105 System.Void Lean.Common.LeanDelayedValue/Vector3Event::.ctor()
extern void Vector3Event__ctor_m2CF6053FB19E82B9538312F20D93E5E28F273F37 (void);
// 0x00000106 System.Void Lean.Common.LeanSmoothedValue/FloatEvent::.ctor()
extern void FloatEvent__ctor_mBB3739A987620B7DA9CD0BBD921F7FC006E338B0 (void);
// 0x00000107 System.Void Lean.Common.LeanSmoothedValue/Vector2Event::.ctor()
extern void Vector2Event__ctor_m0C625665519D0EA94B47DE01FC060C12AF60076F (void);
// 0x00000108 System.Void Lean.Common.LeanSmoothedValue/Vector3Event::.ctor()
extern void Vector3Event__ctor_mEBABDC63340C816C36A505E3F3B5086900FA79FB (void);
// 0x00000109 System.Void Lean.Common.LeanThresholdDelta/FloatEvent::.ctor()
extern void FloatEvent__ctor_mDD395F8982FF3E66E647401079FCB257D44FCD1F (void);
// 0x0000010A System.Void Lean.Common.LeanThresholdDelta/Vector2Event::.ctor()
extern void Vector2Event__ctor_mB013CAE99521DAB140B8FF8221C82CC60117EF8D (void);
// 0x0000010B System.Void Lean.Common.LeanThresholdDelta/Vector3Event::.ctor()
extern void Vector3Event__ctor_m1B9144A410EF0B372CD181E8C3AD2A0B6CB23FD1 (void);
// 0x0000010C System.Void Lean.Common.LeanThresholdPosition/FloatEvent::.ctor()
extern void FloatEvent__ctor_m90623FFEEC7FB615B12C4F30880FF29001BE9FBC (void);
// 0x0000010D System.Void Lean.Common.LeanThresholdPosition/Vector2Event::.ctor()
extern void Vector2Event__ctor_m9755CC8802E359EA2A6133CB802E8F52E175AD2A (void);
// 0x0000010E System.Void Lean.Common.LeanThresholdPosition/Vector3Event::.ctor()
extern void Vector3Event__ctor_m94D524EB2935C114C47592E9C61366062A20164B (void);
static Il2CppMethodPointer s_methodPointers[270] = 
{
	LeanLoadScene_Load_mFA7580E440FBB98C9BE5641B51D3E73CCE8674E8,
	LeanLoadScene_Load_mD7A63B8F1C4BCA4307ED7398AB0B532532F7F5CA,
	LeanLoadScene__ctor_m61A66416EBAE0BBDDC054CD49B00F9C03B3C045F,
	LeanPongBall_Awake_m62EAA2BE6E8749D7A189E9DF2D20F340E56AD110,
	LeanPongBall_FixedUpdate_m4A4C5D1F906DD834210284CDC230A6A083D54B43,
	LeanPongBall_ResetPositionAndVelocity_mE9BEBDB8E4BA3BFD5FDD329FBFB4A526B5DB7F91,
	LeanPongBall__ctor_m671168DAB452268E3BFFB73305F53E6AA07FB9B8,
	LeanChase_set_Destination_mEBF64D402A6859947D4BEEBC7FE4C3DF3A49C64B,
	LeanChase_get_Destination_mDB53AC14115CDC221A48489879682FDEE9D93486,
	LeanChase_SetPosition_mDE262AE936B73C26496E1E77AC3D633E74B74052,
	LeanChase_SnapToPosition_m1FD14D68B0EF6735EDE1084FFE3F54B966F45709,
	LeanChase_Start_mABB43A0B840BA0414367B51398947F2757B16417,
	LeanChase_Update_mAE95DBCBBC17FDBDCF63165CFAE82B1CD2CE2EA3,
	LeanChase_UpdatePosition_m3154D14EB40356F07A765FBD4C9D60D446876DF0,
	LeanChase__ctor_m50678584D570650D3689A4D91E142FC24DA47A13,
	LeanChaseRigidbody_SetPosition_m3A1C01577CEDADC4C39AA25E938F2328CC4C7371,
	LeanChaseRigidbody_OnEnable_mCAB62D9D7089FDACBD33012117E15B952FD330D0,
	LeanChaseRigidbody_UpdatePosition_m8FF4B220D3CD24F62AE03435921CADC1A516D7F1,
	LeanChaseRigidbody_LateUpdate_m19F33310246D29308CFC283036631DBE1CEB3E35,
	LeanChaseRigidbody__ctor_m0B40186BA5BC09987E7AA28A3105DD6710509040,
	LeanChaseRigidbody2D_set_Axis_m037D56BED6B81664334C3DC98AF678196BA63F57,
	LeanChaseRigidbody2D_get_Axis_mD8129F28DC62EB81CC8035934D2A0248356F5DA9,
	LeanChaseRigidbody2D_SetPosition_m1BEE8ABC2307E528A19B28E7DA3FB401869E11D8,
	LeanChaseRigidbody2D_OnEnable_mF17D06984F5209BA95E667A81188888EA9D3AD0F,
	LeanChaseRigidbody2D_UpdatePosition_mE2DBE19FEC8B32AA213963ED3CD20EF67D20582D,
	LeanChaseRigidbody2D_LateUpdate_mF5C5AD625EF02A31CC4561DA20BD94BAE9D49B5B,
	LeanChaseRigidbody2D__ctor_mA5A046402F971F70874D026F6BB74EBC4351408D,
	LeanConstrainLocalPosition_LateUpdate_mBB2C7839929CEA77E5AE2E42FB3683D6614A790E,
	LeanConstrainLocalPosition_DoClamp_mEF9DCF72ECCFF0D67458EF7FE6E89CAE2D709F1D,
	LeanConstrainLocalPosition__ctor_m0C0B19A2832F74267F6C3EAF4B98E3AC1DBFDE19,
	LeanConstrainScale_LateUpdate_mCFD3A827D999951920F34A557A1692E7DF3781C3,
	LeanConstrainScale__ctor_m54DD63544BAAE8FD9CBD79C2DDFBEE515F1615F9,
	LeanConstrainToAxis_LateUpdate_m080C72F196EBCE7ECD741A9A8760A5EC69EA388F,
	LeanConstrainToAxis__ctor_m761669EBC80EB87DA6C87B254C7EEFF1FAAE56AC,
	LeanConstrainToBox_LateUpdate_mD22F30B8BDDE6E3CDAA7852401A2D1D4405C0E83,
	LeanConstrainToBox_OnDrawGizmosSelected_m579ABD51B3FC0C67415ED9F85175ED966A0BB663,
	LeanConstrainToBox__ctor_m306BCB35A8AFD0349212BCF0052090845A84C0B4,
	LeanConstrainToCollider_LateUpdate_mB831BFC2A94E68CAD46885533E212A1A0AA4941C,
	LeanConstrainToCollider__ctor_m49574AB33FE30FCF224EF078626DAA839F83E1CE,
	LeanConstrainToColliders_LateUpdate_m7B4353C3FF44E9280896EDBC7A722C584368D2BA,
	LeanConstrainToColliders__ctor_m0A01C293101707C13D7684DBC4E01FE52F4FE611,
	LeanConstrainToDirection_LateUpdate_m03411D8CF13318DFD2624CB9EC6605314BFF9DBF,
	LeanConstrainToDirection__ctor_m6A02D6FF85792F40D27CF76EF5FDCD204E0F2417,
	LeanConstrainToOrthographic_LateUpdate_m7288138E2504E24CBA5E844373BE2991781817A2,
	LeanConstrainToOrthographic__ctor_m7DE32DDAAB5C64BEC610D0759EC41AC28AA2946E,
	LeanDelayedValue_get_OnValueX_m06943E5747EFBB4D7D32EFFA8337AECB879D346E,
	LeanDelayedValue_get_OnValueY_mE0A2E9779E6AB1B27E533889D70902E277FF95D4,
	LeanDelayedValue_get_OnValueZ_m0E189F5998BC682FEAE6EED82811599E2848CA24,
	LeanDelayedValue_get_OnValueXY_m4A9C91314DE14D8D0EBB01C0BA12C80A551EDE8C,
	LeanDelayedValue_get_OnValueXYZ_m57FC20493817B44F5305128E74D1EBE943A3A81F,
	LeanDelayedValue_SetX_mB91ED053708ADBE6363B4E443B89182E34081C95,
	LeanDelayedValue_SetY_m3442668008B862B1DFE1154101CFF0E9A8D93258,
	LeanDelayedValue_SetZ_mA12164EBB7FEA25A818C7567064BED59367FFC35,
	LeanDelayedValue_SetXY_m23F4493FB9B5B3E31027CBDB30F0E146383CB2A2,
	LeanDelayedValue_SetXYZ_mFBB5B0F9ED1E19F51DEC49E4A57C7F6C32713ABD,
	LeanDelayedValue_Clear_mBE1871969AA68803F0952823BD0C83E561E8BE11,
	LeanDelayedValue_Update_m08D7F2DABAD5D2E499A6C60C9F2CA2992EE165CB,
	LeanDelayedValue_Submit_mEE0E7B78BE0F8A472080EA262BD5EFADBE986131,
	LeanDelayedValue__ctor_mDE609DEF833BE6A5B0C890DBC127ED8F0483A2A5,
	LeanFollow_get_OnReachedDestination_m9DC4AB7707BCE9E77E71B1F72D174086D12B87C3,
	LeanFollow_ClearPositions_m882DF3EEC091B4475EF7D8E9245580E751A1DED5,
	LeanFollow_SnapToNextPosition_m7E40642AA78352FBD4E6E2CE185EAD9672A89ACF,
	LeanFollow_AddPosition_m6FDAAC51971A43482EDADFB929366BDBB12A1715,
	LeanFollow_Update_mE4FC7633F28DDE13FDC841881C16A5B86528812F,
	LeanFollow_TrimPositions_m6DE85761C9A8132F2423A8059B4DD01245B0AA3A,
	LeanFollow__ctor_mCC1968F35CB97AF0E472FF78F6664F9D162B8B80,
	LeanMaintainDistance_AddDistance_m203DF6386DFD4FFD87210D24716AA56E2452AB79,
	LeanMaintainDistance_MultiplyDistance_mAACD05031247D21B269B2350F422A8365891D4AB,
	LeanMaintainDistance_Start_mE9F1B93D23661F4D9F026EF49AB93262C6C438F4,
	LeanMaintainDistance_LateUpdate_m309EAD4F87D5DBD0DEA1E1D8A3E725C4979071FF,
	LeanMaintainDistance__ctor_m2FDE93F5292651228FF0CF139DCFD0958CB84448,
	LeanManualRescale_set_Target_mB08A2074D51B43EEDD7F42FAF1FF70243244793A,
	LeanManualRescale_get_Target_m3D0871EA0E644E026402F0ACA998619D53FA15D0,
	LeanManualRescale_set_AxesA_m1473D459B9A279E3775758587717989DBC31BD9F,
	LeanManualRescale_get_AxesA_mDF5403217B9C2F6C00F8C0D5AF15F2E4EEEFCA1E,
	LeanManualRescale_set_AxesB_m811EF5522A2136C5DC290A9BD85DCE21FC1F434A,
	LeanManualRescale_get_AxesB_mE03F25569F1B7DBD0A1A97470E1CC38B53D6DB34,
	LeanManualRescale_set_Multiplier_m82EC8D0980FCA8254780B33248E711BFBEDC21C4,
	LeanManualRescale_get_Multiplier_m0CE271A918F279418A64DBC2BD51CBD7D84C066D,
	LeanManualRescale_set_Damping_m2ED3EC22F2F715ACD171E5CD383DE20BA6CCFFA8,
	LeanManualRescale_get_Damping_mB79803A4C9977FA9CDE5E881F351758A903AFD12,
	LeanManualRescale_set_ScaleByTime_m47D047762D08EEF29F6AF293F715BB86D73B5D26,
	LeanManualRescale_get_ScaleByTime_m2F309AE9C5EAA8F690B7E5813DD75A67D2479950,
	LeanManualRescale_set_DefaultScale_mB41AA8F18F0E838C9AC5349D639EDCB432763991,
	LeanManualRescale_get_DefaultScale_m5CB56F293FEDD76511F25ECF31190FADB3928535,
	LeanManualRescale_ResetScale_mE640226DC12DD0EFB1737A97BA576ED82F633371,
	LeanManualRescale_SnapToTarget_m10627A7E29ADA90CAE7504887A7D9A524F314E69,
	LeanManualRescale_AddScaleA_m28BEED55982C9FB11994764946F48EE2A7FC4009,
	LeanManualRescale_AddScaleB_m13D3F59ED04E1C83B33333AD77E0E3ADE48DB598,
	LeanManualRescale_AddScaleAB_m3BE77A9AE20EB496D174FFEDE58D60C13A5C4064,
	LeanManualRescale_AddScale_m2C411006B42ECE60594C471C65832FB1200D46BE,
	LeanManualRescale_Update_mCB916F67EFEA1896F27EC8DB972F805F6DFCF8AC,
	LeanManualRescale_UpdateScale_m8612B3083C3C09446055A5F378B77C712EA06C65,
	LeanManualRescale__ctor_mC9A21E420A9537BCB11CAA1C903C6C5A150E5A3A,
	LeanManualRotate_set_Target_mB885E481B82A633FA09B2B68A57E2F781E147819,
	LeanManualRotate_get_Target_m09CD3F3A57F6D43F474938620EA5294F47C7567C,
	LeanManualRotate_set_Space_mE650F021EB03B99D69A6EEF1484DBFF446E01C2E,
	LeanManualRotate_get_Space_m129BC692FE14560824C11C8E04F8334E6AFF0E5D,
	LeanManualRotate_set_AxisA_mE009D2CD80B7EBF811A48E82844EEFCF0DE7DFDE,
	LeanManualRotate_get_AxisA_m1D8FE0FA9438E39FB7111D7E1D6990613670AB71,
	LeanManualRotate_set_AxisB_m6A01BFAB9B3688FF8ACD16C90255B01C0A269D78,
	LeanManualRotate_get_AxisB_m6435E35A62C75AF7FAB2DCDB8C5B1C6CCF3E165D,
	LeanManualRotate_set_Multiplier_m3F2669DA6B225A006E77A81800370DDC5F2BED86,
	LeanManualRotate_get_Multiplier_mDB26646EAE2A22C7929C59E77A2B37A956FAD9C4,
	LeanManualRotate_set_Damping_m088AA0C448C2C0EF59AA4A96FC8ADAB967715239,
	LeanManualRotate_get_Damping_mD689BDD320E1DEB6C02355040B836ED4C3FAB5DC,
	LeanManualRotate_set_ScaleByTime_m0D8331675DFF4789D49E031D82D9FCF38DB5764D,
	LeanManualRotate_get_ScaleByTime_m49AF5494293F148543355119313F19A2B717D270,
	LeanManualRotate_set_DefaultRotation_m8531F09038BAB8CD13BB918F4BBDE3E890C74307,
	LeanManualRotate_get_DefaultRotation_m1107C2E86F159C8559FC48EECA501D6C08A55E03,
	LeanManualRotate_ResetRotation_mC4100756507BC34F7C385E4E42B5D27B50EC3DFD,
	LeanManualRotate_SnapToTarget_m7140EE483EB5A0385A34B4D3715A53D03F6C28D5,
	LeanManualRotate_StopRotation_mD836D0C7866EB8C9AF753463BF7CEB815B8E7F8E,
	LeanManualRotate_RotateA_m5A8F339954C4022E609A779F79355BF19EF9AE2B,
	LeanManualRotate_RotateB_m319F085726465850A8630CBB905A08364D8B4531,
	LeanManualRotate_RotateAB_m991ED3255F451E259B07B56038DEFFB35D8E137C,
	LeanManualRotate_Update_mBD7135B81C77797CE92FD98E8F3C46808514515A,
	LeanManualRotate_UpdateRotation_mDBB0A9176861D205C92E8B128DA3E3B023C31801,
	LeanManualRotate__ctor_m3EB072B30C78F33EBF97151573C01F749866A4C3,
	LeanManualTorque_AddTorqueA_mB4E1BE12462FC636F6C1F9A175DB65FA99E6558F,
	LeanManualTorque_AddTorqueB_mA9190C1CD08B97827294906BA561F037BB5458FB,
	LeanManualTorque_AddTorqueAB_mDE13792337D1F79DC1D51F1384EC5FE8FA5E07A6,
	LeanManualTorque_AddTorqueFromTo_m8DD178C9B02EB6E858C97C93A8A0FF892AF471B0,
	LeanManualTorque_AddTorque_m90271F96C1CC8C23B5C16F955401962427F305B9,
	LeanManualTorque__ctor_m8B585E82E523982E489B6A317A5142ED099E20A3,
	LeanManualTranslate_set_Target_m57FDF437EEEE95649893635FDF0C6293D4CC862B,
	LeanManualTranslate_get_Target_m8FDE72E6486B26FC083A5EC251BF971CE7351517,
	LeanManualTranslate_set_Space_mAB93D871FAE6584B77C8C68F35892EAE39DA5532,
	LeanManualTranslate_get_Space_mF46BEA76A855B3B7F24FE100A3F9E0E9E98B39B5,
	LeanManualTranslate_set_DirectionA_m3F210C270534CA943C4442298B276BB9F7AE11A8,
	LeanManualTranslate_get_DirectionA_m91037C51E60504C546273AB45024E0ED261537F7,
	LeanManualTranslate_set_DirectionB_m9082631EA0581E4B1BC95228A7366718E4D143E1,
	LeanManualTranslate_get_DirectionB_m6F1FA204FFE559DD574261D36D5A5EA723839E1A,
	LeanManualTranslate_set_Multiplier_mFE34753057EAB34DF5AB909625980FDFD4D79058,
	LeanManualTranslate_get_Multiplier_m8A44B4B85BD8AC3516373CE40D321C5F0726B54B,
	LeanManualTranslate_set_Damping_m179FB6114BF2A71F7604BA1CA017EF26891757B0,
	LeanManualTranslate_get_Damping_m90BA63E70ED1329310F6FB3066AD5D8588C9141C,
	LeanManualTranslate_set_ScaleByTime_mE8E12040FD556308FF55DB96C5268628C3CB8310,
	LeanManualTranslate_get_ScaleByTime_m6337B6A654D1C26CD12BC75C030AEC8A47AB78E0,
	LeanManualTranslate_set_DefaultPosition_m3474CA88DE1870D2FE325EFBA2A6EEF796E7A266,
	LeanManualTranslate_get_DefaultPosition_m382699B209CA6AC2EFF769BE5A76B8E439E24666,
	LeanManualTranslate_ResetPosition_mE8090325C2223064B3DF3A7BDB32DFF926449A6F,
	LeanManualTranslate_SnapToTarget_mFFBD6EA73CD0EF67BC6F9EA431A5D3C84F7AAA7D,
	LeanManualTranslate_TranslateA_m59274DF458CFF99559987823746B4225637279E1,
	LeanManualTranslate_TranslateB_mE9924B7FAB2566AF5C51E92110D4BB3D6AA35048,
	LeanManualTranslate_TranslateAB_mA833B6AE51514DE1811BEAF6AC9BE20705F70198,
	LeanManualTranslate_Translate_mA2CB0F9252C319E05F261140F7E3A91C42162E46,
	LeanManualTranslate_TranslateWorld_mC26815602C794D26B8E32A04A6AD6C2F0329EF33,
	LeanManualTranslate_Update_mCEC613DDF7A3BD1D850B1FD1613FBE9F6330C35A,
	LeanManualTranslate_UpdatePosition_m05D76DC74F3EA85D07696607E1FC35226AAD5916,
	LeanManualTranslate__ctor_m265081E9C0596939EB1A9D613909320368103E8C,
	LeanManualTranslateRigidbody_TranslateA_mBA0B202F5C27D66F2EB7222BF642CFA8D789011B,
	LeanManualTranslateRigidbody_TranslateB_m4E3D275BD568613065D2E998EDB81F517553380E,
	LeanManualTranslateRigidbody_TranslateAB_m025CB60090C18EC9AE4751A9A15263DF8BE9A4C7,
	LeanManualTranslateRigidbody_Translate_mE8CA00F0910E9DCC0CF3206DF0462EDEA432B70D,
	LeanManualTranslateRigidbody_TranslateWorld_mB94CB1BD22350D0B52F538F68BAE2EDFE57BCCD5,
	LeanManualTranslateRigidbody_Update_mB3AE43AF3E8A8C050AF22F6E5C422573929B8FA5,
	LeanManualTranslateRigidbody__ctor_m20FDA772AF498C5572CE6445927CEBEFD09E061A,
	LeanManualTranslateRigidbody2D_TranslateA_m6D4F70FB2D30C9A989CF7082B4A4D28DD3998D79,
	LeanManualTranslateRigidbody2D_TranslateB_mADB8A4F07DA0B4BB19AF4039118C40C7E8F7C995,
	LeanManualTranslateRigidbody2D_TranslateAB_m9F867F5F11E532263AEA8F0035D7346E5B76D001,
	LeanManualTranslateRigidbody2D_Translate_mE111E55D49E155538E4782D2EA20F9EC9DB52275,
	LeanManualTranslateRigidbody2D_TranslateWorld_mE17AAB380DA913AEA08CACFEFF97D27FDCE5D02A,
	LeanManualTranslateRigidbody2D_FixedUpdate_m8276A252FA5D24EBD48504E835AFAD19C1F67213,
	LeanManualTranslateRigidbody2D_Update_m762124D0AE451F813007300A654E42FFCDAAFA41,
	LeanManualTranslateRigidbody2D__ctor_mD9CB477D8FC5BE2048903BFA2C7FB1DDA93CB54D,
	LeanOrbit_Rotate_m2978C60AFC28CCB0E0F602C3E2E283C2ECCC6331,
	LeanOrbit_RotatePitch_m171D3F078CC522A2D69DD3F1CB4A55169D689C53,
	LeanOrbit_RotateYaw_mE3A55FDE0FF18849953516D687ABA80BD1A8D151,
	LeanOrbit_Start_mD2AA0DA08032B63E998EF0743D959C8BE1BF52CE,
	LeanOrbit_LateUpdate_mE401BE25E27951AF94482F3A4C46E649699F5AE8,
	LeanOrbit_GetSensitivity_mE55D03EEAB010940503367634E1F3720D87FEDAF,
	LeanOrbit__ctor_mB3024B182676E90C121AA88262FC90455F75E753,
	LeanPitchYaw_ResetRotation_m13CF6D58ABEB93F446EAFFE29CC4D97313345EEB,
	LeanPitchYaw_RotateToPosition_mE09CF6CB904116706B24A68AFFBEC6F992A80627,
	LeanPitchYaw_RotateToDirection_mFAF6EF14A236AE4C61FE6BF078D24613ED60BDA7,
	LeanPitchYaw_SetPitch_m8A53BFF6FCF4C34818A1F711EFD7A657E786C014,
	LeanPitchYaw_SetYaw_m5503ED71BBC7D5B4CC5F846F1611212640789C11,
	LeanPitchYaw_RotateToScreenPosition_m6DDAA3FC33B9729A3A17D39A94A2A3852422F15A,
	LeanPitchYaw_Rotate_m6F20D1A4C3E1E24D4F08DE2AD69895152833CED6,
	LeanPitchYaw_RotatePitch_m605338F4EA223C332A76833DF8D93AD90DFD0999,
	LeanPitchYaw_RotateYaw_mCABB4BC8E2AC83B0EB05BE9F5AFC5D2F58D230CD,
	LeanPitchYaw_Start_mD8D640E75C536EF037700CABB040ED72F71E0F4E,
	LeanPitchYaw_LateUpdate_mD4ABC222F1F05DCFD444D5C1D6F58A91847A3242,
	LeanPitchYaw_GetSensitivity_m67CB21E47092B27B53761158B6BBAEC2CE82E0DE,
	LeanPitchYaw__ctor_mD6C4406B081FBA2E6119AB06F24F85D987EC2BC1,
	LeanPitchYawAutoRotate_OnEnable_m3983342BA9B67602E32F2D75013AAA6121EFCA18,
	LeanPitchYawAutoRotate_LateUpdate_m372767D5F2FCE1700373D03218808B5AF94B1C44,
	LeanPitchYawAutoRotate__ctor_m9488F8B9FBB7EA008A3ABE0C30F35323133C02DF,
	LeanRevertTransform_Start_m10EA69D75DC0898D55B85C71DA455B4AFD54DCF7,
	LeanRevertTransform_Revert_mCEC35C80F80A3E1E3816C57A5714E289CE89B61F,
	LeanRevertTransform_StopRevert_m21B74CCEBFC897C98091567F071A726F40A67D38,
	LeanRevertTransform_RecordTransform_m641F557BB14F4AD624EF6947C8EFDEB1FD0A6478,
	LeanRevertTransform_Update_m8B7B02EC480655272682ADBCCB7F868B15415710,
	LeanRevertTransform_ReachedTarget_m8F3AE66D8E5EA517930336E51D44C97B34EC91C5,
	LeanRevertTransform__ctor_m628FED4F7DA09B4807CF3125C513A149A1D6D6A7,
	LeanRotateToPosition_get_FinalTransform_mAB675B2F22F390B3D2842FA5272F70861D5B2DEE,
	LeanRotateToPosition_SetPosition_m65321E7AA33648A6B1B57DB1D0681D13D7286A6D,
	LeanRotateToPosition_SetDelta_mD8192586859342AC7BEC63619CB888D8F24333C9,
	LeanRotateToPosition_ResetPosition_m6FD2DA2BFD3E065D7FD42E78D5025B5E399A5ED1,
	LeanRotateToPosition_Start_m0C8ADED3E536D212FC8A274B63F24F97C96ACC75,
	LeanRotateToPosition_OnEnable_m7E19CCC4F668BD6C5E7A03921892FB80275C7E12,
	LeanRotateToPosition_LateUpdate_m95EC07C3E76716E188088B6D3AA6F8BE02E2AA4D,
	LeanRotateToPosition_UpdateRotation_mA2423BE6A2DF7952514FF308BB65FBC93A8EF0C9,
	LeanRotateToPosition__ctor_m35D6F36810CC6B7C905F10470B8AF4BEDA636384,
	LeanRotateToRigidbody2D_OnEnable_mADF072537833661B7199C59E3482066AD72AE0F6,
	LeanRotateToRigidbody2D_Start_m87D8831571BE92B1D1CBA87A59B68BB99880A128,
	LeanRotateToRigidbody2D_LateUpdate_m07797EDF26F34CCF79C8D323C272ACD26A46AA51,
	LeanRotateToRigidbody2D__ctor_mE03111FE29A90A38F08457449B18DDB64E7E8F7C,
	LeanSmoothedValue_get_OnValueX_m48F8BE42CACED3665054D9FA9D5115CD3CC17AA8,
	LeanSmoothedValue_get_OnValueY_m0C50AC2FD067D255F75F46A76E813C8F98A9F7A1,
	LeanSmoothedValue_get_OnValueZ_m749217C6FE670753ED3F4AA0F29752C4A806822B,
	LeanSmoothedValue_get_OnValueXY_m0E8E886063D6C0A8D2B9590DC04497EAC2D356F5,
	LeanSmoothedValue_get_OnValueXYZ_mDCA2262167A27F0A5C2185DBBF392B318CC68C4C,
	LeanSmoothedValue_SetX_mA85882DC41C7DC971A65C681E4EE351349D38348,
	LeanSmoothedValue_SetY_mF06EEC70FAB07165DA5D34F5AEB343FFE160D578,
	LeanSmoothedValue_SetZ_m0214415B5619350F8F2F7B9B5418D12921282428,
	LeanSmoothedValue_SetXY_m0358D08B89BE7E44C33583110C2468D779D1F736,
	LeanSmoothedValue_SetXYZ_m7024326070ECC67F4B2812E2CCA1DE6204C2536D,
	LeanSmoothedValue_SnapToTarget_m938E84C230A60B8A82364857151770B1827A846A,
	LeanSmoothedValue_Stop_mBE412AC72823BE3FCE8372380D02CAE790C7F993,
	LeanSmoothedValue_Update_m030E03BB1548A1A4E78CAE781B302407D17B874D,
	LeanSmoothedValue_Submit_m195C4D7091E141CD43AF302796AC2C74F0E7C716,
	LeanSmoothedValue__ctor_m1DA1E026A727C7FE802D21F053F8FA5025A43815,
	LeanSpawnBetween_Spawn_m3D1C633BD8C22BB23CAF5C49CA3CE95E1323412E,
	LeanSpawnBetween__ctor_m853DC15F75A5282C7BAC22A9851C88CF87BD6175,
	LeanSwap_UpdateSwap_m8775432B9932F36F5BA81D1AF725AFC55CAF8AA3,
	LeanSwap_SwapTo_mD9F490AC477280019B645CDFF4CF7142C0ECA2FF,
	LeanSwap_SwapToPrevious_m922F0A3BE491764218F008C05769361B32962ED3,
	LeanSwap_SwapToNext_m74CEC15055B51912B786213F1FCF03B62CEE2705,
	LeanSwap_GetPrefab_m9936DACFCD30D9042F53D1A83B78DBC66FE070AD,
	LeanSwap__ctor_m60DA7812034657247A220259D1B4615D5BA34D3E,
	LeanThresholdDelta_get_OnDeltaX_mDE54FE4E639D516FF715AA82F2D208058EB361D0,
	LeanThresholdDelta_get_OnDeltaY_m4397E2D65003FBEF2D2A4A7D025C32088B2A5067,
	LeanThresholdDelta_get_OnDeltaZ_m9FC9A71FFAB85055855FCD64F48EE2912D80E9A3,
	LeanThresholdDelta_get_OnDeltaXY_mA2C666259942A53B2F2F3C8C446571FA083156FF,
	LeanThresholdDelta_get_OnDeltaXYZ_mEEF8A96AF9BBA26B3EFA8AB408FA2448B1CF5929,
	LeanThresholdDelta_AddXY_m7C92EE6DDB7604485B331447FB8B86037F8F79C9,
	LeanThresholdDelta_AddXYZ_m3459C0789546C3C98AA62E7DCA3B61244225DE0E,
	LeanThresholdDelta_AddX_m84433F17454CFA99903A349635A893CB0BC957C9,
	LeanThresholdDelta_AddY_mF07895A3BD45E1244045D82A64C601C962C0FE59,
	LeanThresholdDelta_AddZ_mDC7ED0BE94565E0A27D0C08621AC67A792F507A0,
	LeanThresholdDelta_Update_m824E3651FFADE99F09AC41607EE59B228C580291,
	LeanThresholdDelta__ctor_m33F98962F123D4472B403F32462F7E10E14AFA32,
	LeanThresholdPosition_get_OnPositionX_m260F8DAF211804D1BAEA355B29256CE43D2CAF8F,
	LeanThresholdPosition_get_OnPositionY_mE5B80D4008420ED4F58DB2544E76EE284641190B,
	LeanThresholdPosition_get_OnPositionZ_m6237867CBA886ABD202B2F44038AB1173CB9ECD0,
	LeanThresholdPosition_get_OnPositionXY_m23577E64474D5AEC8EE4D6E37E0FA1D1F6AA4774,
	LeanThresholdPosition_get_OnPositionXYZ_m6626B35F585ADE91DBE506FDFBC0AFAA9A1804E6,
	LeanThresholdPosition_AddXY_mAA3C1F480BA8494220FAFE251F26C0080D80DA5C,
	LeanThresholdPosition_AddXYZ_m9798D69833641EC17C4FB81AE4F9F0CFD572A7C8,
	LeanThresholdPosition_AddX_mA0A351B6F0997D920802377156AFE602BF32D898,
	LeanThresholdPosition_AddY_mD68F489C04324123F20373441E19B741BAB9E129,
	LeanThresholdPosition_AddZ_mDCB09A571756D2C1430BC06C7B77B81074D66B67,
	LeanThresholdPosition_SetXY_m4D78BB4110EC376A15EB02B67A7E0D7B8E42EDF2,
	LeanThresholdPosition_SetXYZ_mE541C1AA0627213FF9E7A05A23F388E20AB5C817,
	LeanThresholdPosition_Update_m32DB07B5B1FB84F5442A8683A12FA1A52BF9EB2D,
	LeanThresholdPosition__ctor_m643369A9E09C0B91EB979F71C5A62D2855D4E8D7,
	FloatEvent__ctor_m46B54DF2F7D74451F1A3FA6A1E74FD32DF9B9FE7,
	Vector2Event__ctor_mB9621DEBD410A8711EA0FA708ADE2EF5C2C1DD8B,
	Vector3Event__ctor_m2CF6053FB19E82B9538312F20D93E5E28F273F37,
	FloatEvent__ctor_mBB3739A987620B7DA9CD0BBD921F7FC006E338B0,
	Vector2Event__ctor_m0C625665519D0EA94B47DE01FC060C12AF60076F,
	Vector3Event__ctor_mEBABDC63340C816C36A505E3F3B5086900FA79FB,
	FloatEvent__ctor_mDD395F8982FF3E66E647401079FCB257D44FCD1F,
	Vector2Event__ctor_mB013CAE99521DAB140B8FF8221C82CC60117EF8D,
	Vector3Event__ctor_m1B9144A410EF0B372CD181E8C3AD2A0B6CB23FD1,
	FloatEvent__ctor_m90623FFEEC7FB615B12C4F30880FF29001BE9FBC,
	Vector2Event__ctor_m9755CC8802E359EA2A6133CB802E8F52E175AD2A,
	Vector3Event__ctor_m94D524EB2935C114C47592E9C61366062A20164B,
};
static const int32_t s_InvokerIndices[270] = 
{
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	1424,
	23,
	23,
	23,
	1595,
	23,
	1424,
	23,
	1595,
	23,
	23,
	32,
	10,
	1424,
	23,
	1595,
	23,
	23,
	23,
	845,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	14,
	335,
	335,
	335,
	1435,
	1424,
	23,
	23,
	1424,
	23,
	14,
	23,
	23,
	1424,
	23,
	23,
	23,
	335,
	335,
	23,
	23,
	23,
	26,
	14,
	1424,
	1423,
	1424,
	1423,
	335,
	727,
	335,
	727,
	31,
	89,
	1424,
	1423,
	23,
	23,
	335,
	335,
	1435,
	1424,
	23,
	335,
	23,
	26,
	14,
	32,
	10,
	1424,
	1423,
	1424,
	1423,
	335,
	727,
	335,
	727,
	31,
	89,
	1424,
	1423,
	23,
	23,
	23,
	335,
	335,
	1435,
	23,
	335,
	23,
	335,
	335,
	1435,
	1426,
	1424,
	23,
	26,
	14,
	32,
	10,
	1424,
	1423,
	1424,
	1423,
	335,
	727,
	335,
	727,
	31,
	89,
	1424,
	1423,
	23,
	23,
	335,
	335,
	1435,
	1424,
	1424,
	23,
	335,
	23,
	335,
	335,
	1435,
	1424,
	1424,
	23,
	23,
	335,
	335,
	1435,
	1424,
	1424,
	23,
	23,
	23,
	1435,
	335,
	335,
	23,
	23,
	727,
	23,
	23,
	1424,
	1424,
	335,
	335,
	1435,
	1435,
	335,
	335,
	23,
	23,
	727,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	23,
	14,
	1424,
	1424,
	23,
	23,
	23,
	23,
	1678,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	14,
	335,
	335,
	335,
	1435,
	1424,
	23,
	23,
	23,
	1424,
	23,
	1426,
	23,
	23,
	32,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	14,
	14,
	1435,
	1424,
	335,
	335,
	335,
	23,
	23,
	14,
	14,
	14,
	14,
	14,
	1435,
	1424,
	335,
	335,
	335,
	1435,
	1424,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_LeanCommonPlusCodeGenModule;
const Il2CppCodeGenModule g_LeanCommonPlusCodeGenModule = 
{
	"LeanCommonPlus.dll",
	270,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
