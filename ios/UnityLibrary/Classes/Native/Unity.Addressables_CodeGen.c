﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog> PackedPlayModeBuildLogs::get_RuntimeBuildLogs()
extern void PackedPlayModeBuildLogs_get_RuntimeBuildLogs_mFC37B4B555D8F01D81B994F1BDCFA2EBA5029725 (void);
// 0x00000002 System.Void PackedPlayModeBuildLogs::set_RuntimeBuildLogs(System.Collections.Generic.List`1<PackedPlayModeBuildLogs/RuntimeBuildLog>)
extern void PackedPlayModeBuildLogs_set_RuntimeBuildLogs_mA67ED4CF1CDF896CDC50BBC7E6773D64B6680DB6 (void);
// 0x00000003 System.Void PackedPlayModeBuildLogs::.ctor()
extern void PackedPlayModeBuildLogs__ctor_m67AD0142BD6DC97F8CCD1C6E86A98CC95D35288E (void);
// 0x00000004 System.Void PackedPlayModeBuildLogs/RuntimeBuildLog::.ctor(UnityEngine.LogType,System.String)
extern void RuntimeBuildLog__ctor_m88375AA8CAFE39FD77175C963991221231AAE038 (void);
// 0x00000005 System.Boolean UnityEngine.AssetReferenceUIRestriction::ValidateAsset(UnityEngine.Object)
extern void AssetReferenceUIRestriction_ValidateAsset_mD920DB6EAA0CB521F83F4BCDDBA24817BD983624 (void);
// 0x00000006 System.Boolean UnityEngine.AssetReferenceUIRestriction::ValidateAsset(System.String)
extern void AssetReferenceUIRestriction_ValidateAsset_m5703AC82584DFFA2651A6BFE3E1D1456FE7799C4 (void);
// 0x00000007 System.Void UnityEngine.AssetReferenceUIRestriction::.ctor()
extern void AssetReferenceUIRestriction__ctor_m66132256B7F090205A884805AF37E18ACBA905B9 (void);
// 0x00000008 System.Void UnityEngine.AssetReferenceUILabelRestriction::.ctor(System.String[])
extern void AssetReferenceUILabelRestriction__ctor_m5C11CFCCA34855EF6B79A42D1DB94B6465C5EF10 (void);
// 0x00000009 System.Boolean UnityEngine.AssetReferenceUILabelRestriction::ValidateAsset(UnityEngine.Object)
extern void AssetReferenceUILabelRestriction_ValidateAsset_m0F0986CED44CE89DC1A6030BC5DA515440329E6E (void);
// 0x0000000A System.Boolean UnityEngine.AssetReferenceUILabelRestriction::ValidateAsset(System.String)
extern void AssetReferenceUILabelRestriction_ValidateAsset_m7FEA8893F130FF8EA21721DCDFD2B754D9638751 (void);
// 0x0000000B System.String UnityEngine.AssetReferenceUILabelRestriction::ToString()
extern void AssetReferenceUILabelRestriction_ToString_m610DD1BF67CD7978120DE81D61CBC05EFAF6FA68 (void);
// 0x0000000C System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>,UnityEngine.AddressableAssets.AddressablesImpl)
extern void InitalizationObjectsOperation_Init_m52566D080315AD808CFDFA8DBB44C37451550234 (void);
// 0x0000000D System.String UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::get_DebugName()
extern void InitalizationObjectsOperation_get_DebugName_m0BB1AD62757F2E6F1784ECEF82D0A3B4031EDBD5 (void);
// 0x0000000E System.Boolean UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::LogRuntimeWarnings(System.String)
extern void InitalizationObjectsOperation_LogRuntimeWarnings_m4FA0B605859EFF68C1730534149964CA532B47F4 (void);
// 0x0000000F System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::Execute()
extern void InitalizationObjectsOperation_Execute_m9936831C2834EB8F0549D62194578368C376F85F (void);
// 0x00000010 System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::.ctor()
extern void InitalizationObjectsOperation__ctor_mEBF54CDAB8D99111796BAA542124CB6C91452123 (void);
// 0x00000011 System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::<Execute>b__7_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
extern void InitalizationObjectsOperation_U3CExecuteU3Eb__7_0_mDDBD37CA57E5F64767F1A289DA18292ACF0F939C (void);
// 0x00000012 System.Object UnityEngine.AddressableAssets.InvalidKeyException::get_Key()
extern void InvalidKeyException_get_Key_m1E827CD10FE8F9121523C55323BFB74E317FB8C3 (void);
// 0x00000013 System.Void UnityEngine.AddressableAssets.InvalidKeyException::set_Key(System.Object)
extern void InvalidKeyException_set_Key_m2DFB687B239948C7109FE19A7A3929829E474D07 (void);
// 0x00000014 System.Type UnityEngine.AddressableAssets.InvalidKeyException::get_Type()
extern void InvalidKeyException_get_Type_m5925DED206520BC63D995CE46BB8E94B36783D9F (void);
// 0x00000015 System.Void UnityEngine.AddressableAssets.InvalidKeyException::set_Type(System.Type)
extern void InvalidKeyException_set_Type_m7B2B3DA88C6503E74497F859DFFC419B0407ED9C (void);
// 0x00000016 System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.Object)
extern void InvalidKeyException__ctor_m2C677DE6F5C9F5147694794B724B73984E389BAC (void);
// 0x00000017 System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.Object,System.Type)
extern void InvalidKeyException__ctor_mBF47DDCC7F5E32F0AE59484A9AD3E9E67837A3D3 (void);
// 0x00000018 System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor()
extern void InvalidKeyException__ctor_mBF7BF36DF3B90DF445FF838B8E7A313F6805E50A (void);
// 0x00000019 System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.String)
extern void InvalidKeyException__ctor_m6234249EF148914FE104EC6410D84189D1297612 (void);
// 0x0000001A System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.String,System.Exception)
extern void InvalidKeyException__ctor_m164FFF9386B875FB19AE242E431A8121F8424D00 (void);
// 0x0000001B System.Void UnityEngine.AddressableAssets.InvalidKeyException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void InvalidKeyException__ctor_mD6FF0F2CBFB4CB9FE246A054DE267BC49815C86E (void);
// 0x0000001C System.String UnityEngine.AddressableAssets.InvalidKeyException::get_Message()
extern void InvalidKeyException_get_Message_m33BE20761F0CDD13983BA12B6B78F1458DDD5593 (void);
// 0x0000001D UnityEngine.AddressableAssets.AddressablesImpl UnityEngine.AddressableAssets.Addressables::get_m_Addressables()
extern void Addressables_get_m_Addressables_m4B5B344E036E113B2320ED1C55C9BC30DFE5DF03 (void);
// 0x0000001E UnityEngine.ResourceManagement.ResourceManager UnityEngine.AddressableAssets.Addressables::get_ResourceManager()
extern void Addressables_get_ResourceManager_m559415A56917C68B7B442D4CDBBC7D7793380CCA (void);
// 0x0000001F UnityEngine.AddressableAssets.AddressablesImpl UnityEngine.AddressableAssets.Addressables::get_Instance()
extern void Addressables_get_Instance_m79F2DCCE86CBD9DC0A7664684D60BFE110E2C4CB (void);
// 0x00000020 UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider UnityEngine.AddressableAssets.Addressables::get_InstanceProvider()
extern void Addressables_get_InstanceProvider_m596D5E60D0D76D85163E04F5E09D91A9CEACF85F (void);
// 0x00000021 System.String UnityEngine.AddressableAssets.Addressables::ResolveInternalId(System.String)
extern void Addressables_ResolveInternalId_m9CA9B4DB03E1FB4336119C24F7ACEE4D689FBF73 (void);
// 0x00000022 System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.AddressableAssets.Addressables::get_InternalIdTransformFunc()
extern void Addressables_get_InternalIdTransformFunc_m086E306F157AD6C14CDC1DACD7C926E814CAE84F (void);
// 0x00000023 System.Void UnityEngine.AddressableAssets.Addressables::set_InternalIdTransformFunc(System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>)
extern void Addressables_set_InternalIdTransformFunc_m49DF9488BEAAC7F8F4F6AE26BCFADC411DE22B5B (void);
// 0x00000024 System.String UnityEngine.AddressableAssets.Addressables::get_StreamingAssetsSubFolder()
extern void Addressables_get_StreamingAssetsSubFolder_m7C00B95B7D348FD20C32854C15511E0D6938031B (void);
// 0x00000025 System.String UnityEngine.AddressableAssets.Addressables::get_BuildPath()
extern void Addressables_get_BuildPath_m342677BBCB1F3D60F97124F42969C024AF34E373 (void);
// 0x00000026 System.String UnityEngine.AddressableAssets.Addressables::get_PlayerBuildDataPath()
extern void Addressables_get_PlayerBuildDataPath_m11B91766AA948854507DA3F25F35ABC2FABE26CB (void);
// 0x00000027 System.String UnityEngine.AddressableAssets.Addressables::get_RuntimePath()
extern void Addressables_get_RuntimePath_mF43F0428A035E595492659C7BBBCEE12A1AB4EC9 (void);
// 0x00000028 System.Collections.Generic.IEnumerable`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::get_ResourceLocators()
extern void Addressables_get_ResourceLocators_m8366B40D38FC8C4DC67F0C27718AC0AF98DF4BC4 (void);
// 0x00000029 System.Void UnityEngine.AddressableAssets.Addressables::InternalSafeSerializationLog(System.String,UnityEngine.LogType)
extern void Addressables_InternalSafeSerializationLog_mBDD38AF01A76BE7997AD1CE7552B10D6892FD482 (void);
// 0x0000002A System.Void UnityEngine.AddressableAssets.Addressables::InternalSafeSerializationLogFormat(System.String,UnityEngine.LogType,System.Object[])
extern void Addressables_InternalSafeSerializationLogFormat_m94E66F0F85E2611139034B4228ABB240B1F97062 (void);
// 0x0000002B System.Void UnityEngine.AddressableAssets.Addressables::Log(System.String)
extern void Addressables_Log_mAB235422F98E3AE80C43106A1DD5C32A65EE96D5 (void);
// 0x0000002C System.Void UnityEngine.AddressableAssets.Addressables::LogFormat(System.String,System.Object[])
extern void Addressables_LogFormat_m2B7DD20142AFE6172A35A0492B369B3B438E6A44 (void);
// 0x0000002D System.Void UnityEngine.AddressableAssets.Addressables::LogWarning(System.String)
extern void Addressables_LogWarning_m04F34DFAEED10EC01D34840BACCA3EC826F6296B (void);
// 0x0000002E System.Void UnityEngine.AddressableAssets.Addressables::LogWarningFormat(System.String,System.Object[])
extern void Addressables_LogWarningFormat_mFBF7F6170CB35FC36604D2AC9E2ADA30493994DF (void);
// 0x0000002F System.Void UnityEngine.AddressableAssets.Addressables::LogError(System.String)
extern void Addressables_LogError_mC09A67786B75C90DC3260646B3B67FB4A79AE712 (void);
// 0x00000030 System.Void UnityEngine.AddressableAssets.Addressables::LogException(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception)
extern void Addressables_LogException_mFCD7800E34C948276DF5555F6A26CA2F56AB046A (void);
// 0x00000031 System.Void UnityEngine.AddressableAssets.Addressables::LogErrorFormat(System.String,System.Object[])
extern void Addressables_LogErrorFormat_m4032E8F90EA251E3D3125E891EAC9034084D7158 (void);
// 0x00000032 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::Initialize()
extern void Addressables_Initialize_mCC26D4A61B062FB38D22A1D52693E701F76FCE5C (void);
// 0x00000033 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::InitializeAsync()
extern void Addressables_InitializeAsync_mF5F1D985415748D2FBB8B6A7968172231D28EB4B (void);
// 0x00000034 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::LoadContentCatalog(System.String,System.String)
extern void Addressables_LoadContentCatalog_m40B518F060D33E36B587B1DE0881A3AD4A587E8F (void);
// 0x00000035 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::LoadContentCatalogAsync(System.String,System.String)
extern void Addressables_LoadContentCatalogAsync_m823DD22FA0B1BC2756333E60648BAC4302539E5C (void);
// 0x00000036 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::LoadContentCatalogAsync(System.String,System.Boolean,System.String)
extern void Addressables_LoadContentCatalogAsync_mB985959DEF1FA1885462E1CF71F7F9F772C80128 (void);
// 0x00000037 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Addressables::get_InitializationOperation()
extern void Addressables_get_InitializationOperation_mEFB504954577C24D51998F7B1E09FCE4211BD114 (void);
// 0x00000038 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.Addressables::LoadAsset(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000039 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.Addressables::LoadAsset(System.Object)
// 0x0000003A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.Addressables::LoadAssetAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x0000003B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.Addressables::LoadAssetAsync(System.Object)
// 0x0000003C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocations(System.Collections.Generic.IList`1<System.Object>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void Addressables_LoadResourceLocations_m4F23AE2D71F14D498D4E938B70E5166B67B8CCE8 (void);
// 0x0000003D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocationsAsync(System.Collections.Generic.IList`1<System.Object>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void Addressables_LoadResourceLocationsAsync_m167D968E945FAB7D8608AB25FA77F4CB4AF02B72 (void);
// 0x0000003E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocationsAsync(System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void Addressables_LoadResourceLocationsAsync_m988E10578219A4DA61CAFEFF1C80923E2ACAA69E (void);
// 0x0000003F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocations(System.Object,System.Type)
extern void Addressables_LoadResourceLocations_mA1218B08662D2327E6E6F613D8DF8153330290DB (void);
// 0x00000040 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.Addressables::LoadResourceLocationsAsync(System.Object,System.Type)
extern void Addressables_LoadResourceLocationsAsync_m5EE5F1D2A4F6A6E91E0BFDD5BE57B02DA645C64D (void);
// 0x00000041 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssets(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>)
// 0x00000042 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>)
// 0x00000043 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>,System.Boolean)
// 0x00000044 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssets(System.Collections.Generic.IList`1<System.Object>,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode)
// 0x00000045 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.Generic.IList`1<System.Object>,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode)
// 0x00000046 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.IEnumerable,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode)
// 0x00000047 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.Generic.IList`1<System.Object>,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
// 0x00000048 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Collections.IEnumerable,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
// 0x00000049 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssets(System.Object,System.Action`1<TObject>)
// 0x0000004A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Object,System.Action`1<TObject>)
// 0x0000004B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.Addressables::LoadAssetsAsync(System.Object,System.Action`1<TObject>,System.Boolean)
// 0x0000004C System.Void UnityEngine.AddressableAssets.Addressables::Release(TObject)
// 0x0000004D System.Void UnityEngine.AddressableAssets.Addressables::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x0000004E System.Void UnityEngine.AddressableAssets.Addressables::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void Addressables_Release_m95B8EDD8FDFE027D6D9DE975FB74118A9C2EE0AD (void);
// 0x0000004F System.Boolean UnityEngine.AddressableAssets.Addressables::ReleaseInstance(UnityEngine.GameObject)
extern void Addressables_ReleaseInstance_m2B0F5ED8BC8D25EBCDE33235922FF9C848B84A00 (void);
// 0x00000050 System.Boolean UnityEngine.AddressableAssets.Addressables::ReleaseInstance(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void Addressables_ReleaseInstance_mAC8E6467B5682E089AFA799C97EC8B62F342FB57 (void);
// 0x00000051 System.Boolean UnityEngine.AddressableAssets.Addressables::ReleaseInstance(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void Addressables_ReleaseInstance_m1DAD05443A51FC063DB44BF6A229A76EA8626BC1 (void);
// 0x00000052 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSize(System.Object)
extern void Addressables_GetDownloadSize_mCEE3C8647DEB61B6018E05D5F6BF9FB6863478C4 (void);
// 0x00000053 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSizeAsync(System.Object)
extern void Addressables_GetDownloadSizeAsync_m18791D84E25476A7831A3F9F8FF0CB6F9589E79B (void);
// 0x00000054 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSizeAsync(System.String)
extern void Addressables_GetDownloadSizeAsync_mC7A9C8312AEA48EB740B199332980F4C145A05A9 (void);
// 0x00000055 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSizeAsync(System.Collections.Generic.IList`1<System.Object>)
extern void Addressables_GetDownloadSizeAsync_mE6A41E54B87DB8F14A36E0CFE8E61E86A31B2746 (void);
// 0x00000056 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.Addressables::GetDownloadSizeAsync(System.Collections.IEnumerable)
extern void Addressables_GetDownloadSizeAsync_mC6D17C32745ED075C6CFE5BE1FF207838EBCEE13 (void);
// 0x00000057 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependencies(System.Object)
extern void Addressables_DownloadDependencies_mA0CFE31F8C239215BB9076054942BDDE346208FB (void);
// 0x00000058 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependenciesAsync(System.Object,System.Boolean)
extern void Addressables_DownloadDependenciesAsync_m4A68D9E8BD7BBB31654BE831E8C36DD8107B4D39 (void);
// 0x00000059 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependenciesAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void Addressables_DownloadDependenciesAsync_mD24CD3D5D47EEF5169FAA59D3F522E1E3B794AA5 (void);
// 0x0000005A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependenciesAsync(System.Collections.Generic.IList`1<System.Object>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
extern void Addressables_DownloadDependenciesAsync_m44208F2E4FD30FC9BCA6EF3EBD52830973624F64 (void);
// 0x0000005B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.Addressables::DownloadDependenciesAsync(System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
extern void Addressables_DownloadDependenciesAsync_m0FDFC253AFA3BADED90B7B1D181A25B260942823 (void);
// 0x0000005C System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Object)
extern void Addressables_ClearDependencyCacheAsync_m9B071B3672AD19314118690C7E6F750FBA074A39 (void);
// 0x0000005D System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
extern void Addressables_ClearDependencyCacheAsync_mE5B52A7D5BEFF0DCE9AC6F3CE3EDD7BEEDC91EE0 (void);
// 0x0000005E System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<System.Object>)
extern void Addressables_ClearDependencyCacheAsync_mE9F398C1526A1BAD1C605E71094B6E6D38FF66FF (void);
// 0x0000005F System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.IEnumerable)
extern void Addressables_ClearDependencyCacheAsync_m78B63E2087AE06479BABED465CEE03BC4ADAA04E (void);
// 0x00000060 System.Void UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.String)
extern void Addressables_ClearDependencyCacheAsync_mDF6D31502222A4F44C90C74616B71BEE089850A6 (void);
// 0x00000061 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Object,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m4CB974BBB18D2B1DC77EB9E7FAA4325311775B18 (void);
// 0x00000062 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m972F65BF2ECACC6943626939835133DE2B72B57F (void);
// 0x00000063 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<System.Object>,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m272EFC570CC0870D38005E977AEC51B3764E754F (void);
// 0x00000064 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.Collections.IEnumerable,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_mC0587D5C3AB91DC6347652C01FE1DD117739E0BC (void);
// 0x00000065 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Addressables::ClearDependencyCacheAsync(System.String,System.Boolean)
extern void Addressables_ClearDependencyCacheAsync_m1D2D4FCE1F33EAF638559D3D88E30C933505C895 (void);
// 0x00000066 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void Addressables_Instantiate_m38F3A3466690CEC825E332AE18253A747548B2C6 (void);
// 0x00000067 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void Addressables_Instantiate_mCBB986921A7C47C43F5714F536C1D1391D1DA55B (void);
// 0x00000068 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(System.Object,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void Addressables_Instantiate_m8FB224FEE45D90CBE659A111642AEEAC01B84345 (void);
// 0x00000069 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(System.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void Addressables_Instantiate_m10EFD35BA331D9429E1D8971990A6FD982C66790 (void);
// 0x0000006A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(System.Object,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void Addressables_Instantiate_mB059AF3EC4DC874A04E3A57982A1279CD88CB247 (void);
// 0x0000006B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::Instantiate(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void Addressables_Instantiate_m9F22F2DA820853C9721CBD2833582AA39C5649E3 (void);
// 0x0000006C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void Addressables_InstantiateAsync_m3E95E3A1BC501C77305391BB8035C4C5C76428A5 (void);
// 0x0000006D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void Addressables_InstantiateAsync_mCE7A7244B6C59CCE618ADCCB85DB6A282D536924 (void);
// 0x0000006E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(System.Object,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void Addressables_InstantiateAsync_m90F4701421C9E1FBE2B6568794BFD94D207F34C8 (void);
// 0x0000006F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(System.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void Addressables_InstantiateAsync_mC9BD2F5EE1D01BA0D9B6D96D64282E781B800E60 (void);
// 0x00000070 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(System.Object,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void Addressables_InstantiateAsync_m723CB32260997B45B177B590803483ED4088054A (void);
// 0x00000071 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.Addressables::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void Addressables_InstantiateAsync_m08CB7D015F2A0848BF35698C798C5FAFA7252B51 (void);
// 0x00000072 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::LoadScene(System.Object,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void Addressables_LoadScene_m42FBDD40F07302289F125FDDED1147F8C701115E (void);
// 0x00000073 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::LoadScene(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void Addressables_LoadScene_m4984981A1B86802A49A78EB59C4130C30B0A47EE (void);
// 0x00000074 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::LoadSceneAsync(System.Object,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void Addressables_LoadSceneAsync_m35D5003C7E5D2FB53721FA4A0E69111E4AA475D5 (void);
// 0x00000075 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::LoadSceneAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void Addressables_LoadSceneAsync_m20E4FC964C7FF2E0DA55DAD0EFA2F3369179A7F5 (void);
// 0x00000076 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadScene(UnityEngine.ResourceManagement.ResourceProviders.SceneInstance,System.Boolean)
extern void Addressables_UnloadScene_m0179B0B6AEC1BC991DF71EBD15DFE0D0520E1FAD (void);
// 0x00000077 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadScene(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Boolean)
extern void Addressables_UnloadScene_mFC5B53937A992A13AE0756B52AA662538B32D479 (void);
// 0x00000078 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadScene(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,System.Boolean)
extern void Addressables_UnloadScene_m36D212716A1F7B063B641F38DF62E913DE267554 (void);
// 0x00000079 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadSceneAsync(UnityEngine.ResourceManagement.ResourceProviders.SceneInstance,System.Boolean)
extern void Addressables_UnloadSceneAsync_m82B29BAEC5AC73EEA98775C17C59A7B78E77A2FB (void);
// 0x0000007A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Boolean)
extern void Addressables_UnloadSceneAsync_m6285B0374E4437B9488BB967DF83AC4EEFFCEB06 (void);
// 0x0000007B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.Addressables::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,System.Boolean)
extern void Addressables_UnloadSceneAsync_m77BBD190C06DF29ADF371BBAB8A74A1AB86FE6C8 (void);
// 0x0000007C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>> UnityEngine.AddressableAssets.Addressables::CheckForCatalogUpdates(System.Boolean)
extern void Addressables_CheckForCatalogUpdates_mDC8FC8CDCFDBBF65815D9D753A55A54E122025F3 (void);
// 0x0000007D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.Addressables::UpdateCatalogs(System.Collections.Generic.IEnumerable`1<System.String>,System.Boolean)
extern void Addressables_UpdateCatalogs_m1276D61515680CEDD07F816D68C70E59355BCFBE (void);
// 0x0000007E System.Void UnityEngine.AddressableAssets.Addressables::AddResourceLocator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void Addressables_AddResourceLocator_m2EC9EE727EBC05B0F6AD2220297518D7D86BA293 (void);
// 0x0000007F System.Void UnityEngine.AddressableAssets.Addressables::RemoveResourceLocator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator)
extern void Addressables_RemoveResourceLocator_m528825E9A799673C6DCD692977556D73F310EB80 (void);
// 0x00000080 System.Void UnityEngine.AddressableAssets.Addressables::ClearResourceLocators()
extern void Addressables_ClearResourceLocators_m17063021B75F324686A0B5DB018670D6FA300480 (void);
// 0x00000081 System.Void UnityEngine.AddressableAssets.Addressables::.cctor()
extern void Addressables__cctor_mAD235DF28DD797B1194041717B18F6252FE35138 (void);
// 0x00000082 UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider UnityEngine.AddressableAssets.AddressablesImpl::get_InstanceProvider()
extern void AddressablesImpl_get_InstanceProvider_m60B8111043831A5C192F0E2E793C464437F8AF05 (void);
// 0x00000083 System.Void UnityEngine.AddressableAssets.AddressablesImpl::set_InstanceProvider(UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider)
extern void AddressablesImpl_set_InstanceProvider_m7CF3BD592A3B0F2BFD48334BAFCA7EF33ABA1A56 (void);
// 0x00000084 UnityEngine.ResourceManagement.ResourceManager UnityEngine.AddressableAssets.AddressablesImpl::get_ResourceManager()
extern void AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF (void);
// 0x00000085 System.Int32 UnityEngine.AddressableAssets.AddressablesImpl::get_SceneOperationCount()
extern void AddressablesImpl_get_SceneOperationCount_m3E682904BF0072F1F9230E2E422B99941DE840A9 (void);
// 0x00000086 System.Int32 UnityEngine.AddressableAssets.AddressablesImpl::get_TrackedHandleCount()
extern void AddressablesImpl_get_TrackedHandleCount_m85F986C30B8B04621F1D6FB92321F01983808F04 (void);
// 0x00000087 System.Void UnityEngine.AddressableAssets.AddressablesImpl::.ctor(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void AddressablesImpl__ctor_mFB97DC608C727A38F66BE2A6DA8FC808F44255EE (void);
// 0x00000088 System.Void UnityEngine.AddressableAssets.AddressablesImpl::ReleaseSceneManagerOperation()
extern void AddressablesImpl_ReleaseSceneManagerOperation_m822C1DCE0B11759298D8BD726819B856586B62BD (void);
// 0x00000089 System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.AddressableAssets.AddressablesImpl::get_InternalIdTransformFunc()
extern void AddressablesImpl_get_InternalIdTransformFunc_m12919FE9A72016A569E96FA767BBA217E3FB07B1 (void);
// 0x0000008A System.Void UnityEngine.AddressableAssets.AddressablesImpl::set_InternalIdTransformFunc(System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>)
extern void AddressablesImpl_set_InternalIdTransformFunc_m12AF3ED9B26345126CDEFB7BCCA747DC5DD13973 (void);
// 0x0000008B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::get_ChainOperation()
extern void AddressablesImpl_get_ChainOperation_mC1FCCFDA2FC6033081B38EDB8BC4FE11298E2FA6 (void);
// 0x0000008C System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::get_ShouldChainRequest()
extern void AddressablesImpl_get_ShouldChainRequest_m394E11B8082EFA65D02B6A38001DC6EA5E122B2B (void);
// 0x0000008D System.Void UnityEngine.AddressableAssets.AddressablesImpl::OnSceneUnloaded(UnityEngine.SceneManagement.Scene)
extern void AddressablesImpl_OnSceneUnloaded_mF08FCC9FC559EAD70688D3F7EF63E38FC2C7DF88 (void);
// 0x0000008E System.String UnityEngine.AddressableAssets.AddressablesImpl::get_StreamingAssetsSubFolder()
extern void AddressablesImpl_get_StreamingAssetsSubFolder_mB50C0351EB64D4B92DC719D6AB6072ACA86E8BB9 (void);
// 0x0000008F System.String UnityEngine.AddressableAssets.AddressablesImpl::get_BuildPath()
extern void AddressablesImpl_get_BuildPath_mD149F3594AD078E5D674C0AA0848F7328A63C9D7 (void);
// 0x00000090 System.String UnityEngine.AddressableAssets.AddressablesImpl::get_PlayerBuildDataPath()
extern void AddressablesImpl_get_PlayerBuildDataPath_m4F535DC68E6773D2854E6945F8D318E1183BEF68 (void);
// 0x00000091 System.String UnityEngine.AddressableAssets.AddressablesImpl::get_RuntimePath()
extern void AddressablesImpl_get_RuntimePath_m708E3330D275A0B6757B7ACD331F8B144C4186D4 (void);
// 0x00000092 System.Void UnityEngine.AddressableAssets.AddressablesImpl::Log(System.String)
extern void AddressablesImpl_Log_mA095E88894C5379A25E4C82E22AAA1B9E9AD4BCE (void);
// 0x00000093 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogFormat(System.String,System.Object[])
extern void AddressablesImpl_LogFormat_m4565DCEA2F86CF81839D8B341D3FA5D7169818F7 (void);
// 0x00000094 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogWarning(System.String)
extern void AddressablesImpl_LogWarning_m2B5474CC29323911DF941FF0074CF749A5E1795B (void);
// 0x00000095 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogWarningFormat(System.String,System.Object[])
extern void AddressablesImpl_LogWarningFormat_m0EFB836035CE1A546271053410B37209E33FD6BD (void);
// 0x00000096 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogError(System.String)
extern void AddressablesImpl_LogError_m97D4A1E15A5E702D10738E4EC683C376CB72844C (void);
// 0x00000097 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogException(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception)
extern void AddressablesImpl_LogException_m0499403B8FA853BB874A4ED46C423BC638191F4E (void);
// 0x00000098 System.Void UnityEngine.AddressableAssets.AddressablesImpl::LogErrorFormat(System.String,System.Object[])
extern void AddressablesImpl_LogErrorFormat_m948EDC09AF660C88E82BA2369001C21A92E44C15 (void);
// 0x00000099 System.String UnityEngine.AddressableAssets.AddressablesImpl::ResolveInternalId(System.String)
extern void AddressablesImpl_ResolveInternalId_mA46547D161E2499742CD72810D596DFEA1ED4D77 (void);
// 0x0000009A System.Collections.Generic.IEnumerable`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::get_ResourceLocators()
extern void AddressablesImpl_get_ResourceLocators_m8069917B2A391A881797D8F6BFCEC2243515A4C5 (void);
// 0x0000009B System.Void UnityEngine.AddressableAssets.AddressablesImpl::AddResourceLocator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AddressablesImpl_AddResourceLocator_mE2F2A1011D79783D5D94A1F1AE953321C120CABC (void);
// 0x0000009C System.Void UnityEngine.AddressableAssets.AddressablesImpl::RemoveResourceLocator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator)
extern void AddressablesImpl_RemoveResourceLocator_mD45770B3A83ADFA2AB555FB31A9D7D61879C7CBF (void);
// 0x0000009D System.Void UnityEngine.AddressableAssets.AddressablesImpl::ClearResourceLocators()
extern void AddressablesImpl_ClearResourceLocators_mF82ADF623A74B6B751A3AD1791FA4CA7D06B5AC8 (void);
// 0x0000009E System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::GetResourceLocations(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void AddressablesImpl_GetResourceLocations_m1CA1288F2C67A631AE464BAAD55F6007B5D102AE (void);
// 0x0000009F System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::GetResourceLocations(System.Collections.IEnumerable,System.Type,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void AddressablesImpl_GetResourceLocations_m945F0FD282F02D92CA593CDCF2F24FA8BAFEE5B9 (void);
// 0x000000A0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::InitializeAsync(System.String,System.String,System.Boolean)
extern void AddressablesImpl_InitializeAsync_m11D3E10EC93C8F0F417EA23EF7D58CFF78BBAD87 (void);
// 0x000000A1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::InitializeAsync()
extern void AddressablesImpl_InitializeAsync_m4A4F934AF1E0BD4A41E309E55BF6EFBE6CED9E2C (void);
// 0x000000A2 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase UnityEngine.AddressableAssets.AddressablesImpl::CreateCatalogLocationWithHashDependencies(System.String,System.String)
extern void AddressablesImpl_CreateCatalogLocationWithHashDependencies_m0A9E7AA50E452C39C6C780A1D64EE7D5B3B911E5 (void);
// 0x000000A3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::LoadContentCatalogAsync(System.String,System.Boolean,System.String)
extern void AddressablesImpl_LoadContentCatalogAsync_m8265B5A15EE5743FA30BD942BBED95066E0B131B (void);
// 0x000000A4 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::TrackHandle(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void AddressablesImpl_TrackHandle_m0EB206D85412C5EC04C3E119DCFA2C357C09B504 (void);
// 0x000000A5 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl::TrackHandle(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000000A6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::TrackHandle(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_TrackHandle_m87874D354073A48745A80A9857A523BF9C0E0560 (void);
// 0x000000A7 System.Void UnityEngine.AddressableAssets.AddressablesImpl::ClearTrackHandles()
extern void AddressablesImpl_ClearTrackHandles_m315C8135F0E63956226A2580CCE39283B31E42E5 (void);
// 0x000000A8 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x000000A9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object)
// 0x000000AA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetAsync(System.Object)
// 0x000000AB UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl::LoadResourceLocationsWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void AddressablesImpl_LoadResourceLocationsWithChain_mD6BE697B071B0E474E53830A901C768EB735D379 (void);
// 0x000000AC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl::LoadResourceLocationsAsync(System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Type)
extern void AddressablesImpl_LoadResourceLocationsAsync_mA686A69DC2D3A4DDEA6221F43376FC800EBCFCB3 (void);
// 0x000000AD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl::LoadResourceLocationsWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,System.Type)
extern void AddressablesImpl_LoadResourceLocationsWithChain_mEB368DB6C3986A7BF1F229F787CC25B19A4BC678 (void);
// 0x000000AE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl::LoadResourceLocationsAsync(System.Object,System.Type)
extern void AddressablesImpl_LoadResourceLocationsAsync_mB2FA2FF787A72F08D4DDA0BAAD287AD53C6B6CCB (void);
// 0x000000AF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>,System.Boolean)
// 0x000000B0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.IEnumerable,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
// 0x000000B1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsAsync(System.Collections.IEnumerable,System.Action`1<TObject>,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
// 0x000000B2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,System.Action`1<TObject>,System.Boolean)
// 0x000000B3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl::LoadAssetsAsync(System.Object,System.Action`1<TObject>,System.Boolean)
// 0x000000B4 System.Void UnityEngine.AddressableAssets.AddressablesImpl::OnHandleDestroyed(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_OnHandleDestroyed_mD1203A8A5BFA64AA3406C7FC2EBCF4AF1E474475 (void);
// 0x000000B5 System.Void UnityEngine.AddressableAssets.AddressablesImpl::OnSceneHandleCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_OnSceneHandleCompleted_mC09A18EB2F08488681D7BC7D70CDA308076C14A7 (void);
// 0x000000B6 System.Void UnityEngine.AddressableAssets.AddressablesImpl::OnHandleCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_OnHandleCompleted_m810CE6BD36FAC94433B0319ADE294772EF24FBE5 (void);
// 0x000000B7 System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release(TObject)
// 0x000000B8 System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000000B9 System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_Release_m71C27A8288DB3BF05910279F11CDD69404692D75 (void);
// 0x000000BA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl::GetDownloadSizeWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object)
extern void AddressablesImpl_GetDownloadSizeWithChain_m1B972DA152368F3A4BF01589DB98F516E8F41ED8 (void);
// 0x000000BB UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl::GetDownloadSizeWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.IEnumerable)
extern void AddressablesImpl_GetDownloadSizeWithChain_mF6EAD3FC240411A213E218883273FA269C4FEA56 (void);
// 0x000000BC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl::GetDownloadSizeAsync(System.Object)
extern void AddressablesImpl_GetDownloadSizeAsync_mEB67C85DC464F53629760BFA18A2DADA28D80F7C (void);
// 0x000000BD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl::GetDownloadSizeAsync(System.Collections.IEnumerable)
extern void AddressablesImpl_GetDownloadSizeAsync_m2EABA472ED94651C65DB04B4FE9A7D7D17AD6668 (void);
// 0x000000BE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsyncWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsyncWithChain_m1D6B91A163CF6BDDF4C7196BB7DC711A8D4502F5 (void);
// 0x000000BF System.Collections.Generic.List`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.AddressableAssets.AddressablesImpl::GatherDependenciesFromLocations(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
extern void AddressablesImpl_GatherDependenciesFromLocations_m01332E9D4D5B131709B0E1F7B50C043376FC58F4 (void);
// 0x000000C0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsync(System.Object,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsync_mEF8B90CAF2BEB8C10015775D391E76DD2D3FEA18 (void);
// 0x000000C1 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsyncWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsyncWithChain_m81B6C58EE2A51073096FD95D411A3D968D67BEEB (void);
// 0x000000C2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsync_m8E7A915B1F80C32A4BA8179025FF06F0E4F3D936 (void);
// 0x000000C3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsyncWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsyncWithChain_m064DD963263B0B0380D07679A6AA6067C5CD75FB (void);
// 0x000000C4 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AddressablesImpl::DownloadDependenciesAsync(System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode,System.Boolean)
extern void AddressablesImpl_DownloadDependenciesAsync_m6414EF4D5FC0F831981FC9AFFCC9348940D97457 (void);
// 0x000000C5 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::ClearDependencyCacheForKey(System.Object)
extern void AddressablesImpl_ClearDependencyCacheForKey_m0E986D879EF307BE0D4A466EF65DED6328034815 (void);
// 0x000000C6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl::ClearDependencyCacheAsync(System.Object,System.Boolean)
extern void AddressablesImpl_ClearDependencyCacheAsync_mAFA6D29AF413CEBAB3EBD7391F1E43EC0E4FC80B (void);
// 0x000000C7 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl::ClearDependencyCacheAsync(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean)
extern void AddressablesImpl_ClearDependencyCacheAsync_mF3E0167748EA49B66A21EA5A0F45A90AAC91088A (void);
// 0x000000C8 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl::ClearDependencyCacheAsync(System.Collections.IEnumerable,System.Boolean)
extern void AddressablesImpl_ClearDependencyCacheAsync_m84F1D5ADFB5C0BDCD7E89F4C01B76FD69863679F (void);
// 0x000000C9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m686908BECA518774981EBF6E5B13EFAA9CB71D73 (void);
// 0x000000CA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m2093DEB4C20FB8A9EDAC7E3F9B0E5F51957D04B0 (void);
// 0x000000CB UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(System.Object,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_mEC48526760B336BBF3DD968EF0C88A4FB37ED100 (void);
// 0x000000CC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(System.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m38C438BEE7E7F2B9B913A6BC27C22E7363FD73CA (void);
// 0x000000CD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void AddressablesImpl_InstantiateWithChain_m97786DAC24258ACC5E42543D8E520BF2492EFA41 (void);
// 0x000000CE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(System.Object,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_m21DB723FD9048B8D1E1039C457FB407DE2F0E357 (void);
// 0x000000CF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void AddressablesImpl_InstantiateWithChain_m0884A4716451E99D465F4ACF941A5EE8CBFD3FD7 (void);
// 0x000000D0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl::InstantiateAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,System.Boolean)
extern void AddressablesImpl_InstantiateAsync_mA10679617DC0E9EF2ED3325BEE4F1C6A5B3F3F94 (void);
// 0x000000D1 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::ReleaseInstance(UnityEngine.GameObject)
extern void AddressablesImpl_ReleaseInstance_m972660B8DBA7CF0C191F8EBE28311B4C0C22319C (void);
// 0x000000D2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::LoadSceneWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Object,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void AddressablesImpl_LoadSceneWithChain_m5A2178CC3FC09CF8C9BE9B1F3704278F5A71AFFB (void);
// 0x000000D3 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::LoadSceneAsync(System.Object,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32,System.Boolean)
extern void AddressablesImpl_LoadSceneAsync_m5D6A565C43447859C4F2946EB573421997CDE32E (void);
// 0x000000D4 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::LoadSceneAsync(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32,System.Boolean)
extern void AddressablesImpl_LoadSceneAsync_m9C39609CD09828421355A2D9E767BB38235B1665 (void);
// 0x000000D5 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::UnloadSceneAsync(UnityEngine.ResourceManagement.ResourceProviders.SceneInstance,System.Boolean)
extern void AddressablesImpl_UnloadSceneAsync_m36D752A57D4E88D29B2CB2D9613CDE5C8ED8FB28 (void);
// 0x000000D6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Boolean)
extern void AddressablesImpl_UnloadSceneAsync_m85DABF4E58EE6ACFA26B1DF63A315951609EAF20 (void);
// 0x000000D7 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::UnloadSceneAsync(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,System.Boolean)
extern void AddressablesImpl_UnloadSceneAsync_m6CCDCF5F164CC2D71D8E82B81C114ED37F5F6DA6 (void);
// 0x000000D8 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::CreateUnloadSceneWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Boolean)
extern void AddressablesImpl_CreateUnloadSceneWithChain_m968F189581465B4EAF5A26F68652E4462C559554 (void);
// 0x000000D9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::CreateUnloadSceneWithChain(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,System.Boolean)
extern void AddressablesImpl_CreateUnloadSceneWithChain_m524E0316B96655A3F6235A1B658D1B8A5622B823 (void);
// 0x000000DA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl::InternalUnloadScene(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>,System.Boolean)
extern void AddressablesImpl_InternalUnloadScene_m50518F7D3C6E69F07DAA545D6862EB89C5EA9714 (void);
// 0x000000DB System.Object UnityEngine.AddressableAssets.AddressablesImpl::EvaluateKey(System.Object)
extern void AddressablesImpl_EvaluateKey_mC24716AC53F37CA42CDA773EA7B9FBA88A364314 (void);
// 0x000000DC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>> UnityEngine.AddressableAssets.AddressablesImpl::CheckForCatalogUpdates(System.Boolean)
extern void AddressablesImpl_CheckForCatalogUpdates_m38DC71A358BB49922778118BBD146612E5FAA15E (void);
// 0x000000DD UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo UnityEngine.AddressableAssets.AddressablesImpl::GetLocatorInfo(System.String)
extern void AddressablesImpl_GetLocatorInfo_m1DEA14E46D54B4C09FE8FBF8AFACC78162949FEB (void);
// 0x000000DE System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.AddressableAssets.AddressablesImpl::get_CatalogsWithAvailableUpdates()
extern void AddressablesImpl_get_CatalogsWithAvailableUpdates_m19302FD4FAFD70B8AD82A2343CCD0057EB7005F3 (void);
// 0x000000DF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.AddressablesImpl::UpdateCatalogs(System.Collections.Generic.IEnumerable`1<System.String>,System.Boolean)
extern void AddressablesImpl_UpdateCatalogs_mF7C9B9975D87E1BC2834DD876D95E3788FDD21A2 (void);
// 0x000000E0 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::Equals(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AddressablesImpl_Equals_mCADA4AC0A1F2ED550ACC39DC8A5F1E214AB80CEB (void);
// 0x000000E1 System.Int32 UnityEngine.AddressableAssets.AddressablesImpl::GetHashCode(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AddressablesImpl_GetHashCode_m03A282AEA329A55DBFBD4F3BFBD506D832ADCC01 (void);
// 0x000000E2 System.Void UnityEngine.AddressableAssets.AddressablesImpl::<InitializeAsync>b__57_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>)
extern void AddressablesImpl_U3CInitializeAsyncU3Eb__57_0_m26FC116FE12B1C257AA9C1FDB4887A4063697F31 (void);
// 0x000000E3 System.Void UnityEngine.AddressableAssets.AddressablesImpl::<TrackHandle>b__61_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void AddressablesImpl_U3CTrackHandleU3Eb__61_0_m5551E5AB9746D40C6F672ED2CBAF9E406E23BE77 (void);
// 0x000000E4 System.Void UnityEngine.AddressableAssets.AddressablesImpl::<DownloadDependenciesAsync>b__91_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>>)
extern void AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__91_0_mB51027D69506A97CCCA45286AC90CBE95AD63BBD (void);
// 0x000000E5 System.Void UnityEngine.AddressableAssets.AddressablesImpl::<DownloadDependenciesAsync>b__91_1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>>)
extern void AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__91_1_m94862209DDB1CF58A6412E71B931FE18D1414207 (void);
// 0x000000E6 System.Void UnityEngine.AddressableAssets.AddressablesImpl::<DownloadDependenciesAsync>b__93_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>>)
extern void AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__93_0_m1124D80525DEB05AB5EE561A90BEAB43901EA3DA (void);
// 0x000000E7 System.Void UnityEngine.AddressableAssets.AddressablesImpl::<DownloadDependenciesAsync>b__95_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>>)
extern void AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__95_0_m639480CBE7F6C2F4DD335FB73623F575074F775D (void);
// 0x000000E8 System.Void UnityEngine.AddressableAssets.AddressablesImpl::<DownloadDependenciesAsync>b__95_1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>>)
extern void AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__95_1_mE4CF7E0CC979594BF8D5A139E5E4C0B9548E1776 (void);
// 0x000000E9 System.Void UnityEngine.AddressableAssets.AddressablesImpl::<InternalUnloadScene>b__117_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void AddressablesImpl_U3CInternalUnloadSceneU3Eb__117_0_mE8A86C2E2CA77B10A74F3BC325279CF5434CC1F8 (void);
// 0x000000EA System.Void UnityEngine.AddressableAssets.AddressablesImpl::<CheckForCatalogUpdates>b__119_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void AddressablesImpl_U3CCheckForCatalogUpdatesU3Eb__119_0_m195B08AE7FC5EF4594288EB4F896883A103414AB (void);
// 0x000000EB UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_Locator()
extern void ResourceLocatorInfo_get_Locator_m730E272473D7C040F4F841E8551408D7CD7972BC (void);
// 0x000000EC System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::set_Locator(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator)
extern void ResourceLocatorInfo_set_Locator_mE580D784A4F673DDDB4294D954F3318962C22F7B (void);
// 0x000000ED System.String UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_LocalHash()
extern void ResourceLocatorInfo_get_LocalHash_mE42295051FB10834E554BA358EF27797DEF02FA6 (void);
// 0x000000EE System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::set_LocalHash(System.String)
extern void ResourceLocatorInfo_set_LocalHash_mBE8A94DBDA82AFA8664B367A8AB5C075260AD851 (void);
// 0x000000EF UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_CatalogLocation()
extern void ResourceLocatorInfo_get_CatalogLocation_mD0A97F02CAD8D1108EE0DD40C801EC3146EE8CE9 (void);
// 0x000000F0 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::set_CatalogLocation(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceLocatorInfo_set_CatalogLocation_m03220A441E317EA295005B990404BDE74EAA9B44 (void);
// 0x000000F1 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_ContentUpdateAvailable()
extern void ResourceLocatorInfo_get_ContentUpdateAvailable_m3DFA6F817B37BB2C44EFD73F343B32C7FB239C7D (void);
// 0x000000F2 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::set_ContentUpdateAvailable(System.Boolean)
extern void ResourceLocatorInfo_set_ContentUpdateAvailable_mD5DB2475409DC347D899089950F14953CB2C8B34 (void);
// 0x000000F3 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::.ctor(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceLocatorInfo__ctor_m771FFCECED08BC5F23E420C74B5AAC65971B9EAB (void);
// 0x000000F4 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_HashLocation()
extern void ResourceLocatorInfo_get_HashLocation_m06E961C27F73FFF9147F8A2E683BBDED55111B10 (void);
// 0x000000F5 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::get_CanUpdateContent()
extern void ResourceLocatorInfo_get_CanUpdateContent_mB3AFEF8C7E62E0F4043B68CD8641D8056B5B3991 (void);
// 0x000000F6 System.Void UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo::UpdateContent(UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceLocatorInfo_UpdateContent_m12E2AE38A5B71B3EC19D3B20847DCDBB86B03167 (void);
// 0x000000F7 System.String UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::get_DebugName()
extern void LoadResourceLocationKeyOp_get_DebugName_m0FE13637334C8510C330B99DF4DA803723B2C903 (void);
// 0x000000F8 System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::Init(UnityEngine.AddressableAssets.AddressablesImpl,System.Type,System.Object)
extern void LoadResourceLocationKeyOp_Init_mAAEE48C857897F6D6055E520BFF1D39D89EC72CC (void);
// 0x000000F9 System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::Execute()
extern void LoadResourceLocationKeyOp_Execute_m9DC1F3186E5476DC77A1229E304FA6AA01DF4D5F (void);
// 0x000000FA System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeyOp::.ctor()
extern void LoadResourceLocationKeyOp__ctor_mC4FC39260004C27AD6697FF8C79392617F6A87DC (void);
// 0x000000FB System.String UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::get_DebugName()
extern void LoadResourceLocationKeysOp_get_DebugName_m5F40DAD3471E05B60D117EF3CFBD59B663BFF23D (void);
// 0x000000FC System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::Init(UnityEngine.AddressableAssets.AddressablesImpl,System.Type,System.Collections.IEnumerable,UnityEngine.AddressableAssets.Addressables/MergeMode)
extern void LoadResourceLocationKeysOp_Init_m7B85866D25E6F935F469136DE5EEF45010A039E3 (void);
// 0x000000FD System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::Execute()
extern void LoadResourceLocationKeysOp_Execute_mA58F78F7D010586D72640A82798AB736FB9B7BF5 (void);
// 0x000000FE System.Void UnityEngine.AddressableAssets.AddressablesImpl/LoadResourceLocationKeysOp::.ctor()
extern void LoadResourceLocationKeysOp__ctor_mE892F37F59EE3746012541B7B69928477F770313 (void);
// 0x000000FF System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mAC90BA4B3DD4EC74A8AD2C293A31AB8C1288B289 (void);
// 0x00000100 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass33_0::<OnSceneUnloaded>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void U3CU3Ec__DisplayClass33_0_U3COnSceneUnloadedU3Eb__0_m7FF18F1F05E2417815B0C44CC705937F52ABE452 (void);
// 0x00000101 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c::.cctor()
extern void U3CU3Ec__cctor_mE4D426958DC06AA4379154B639D4A5AD8E737EFB (void);
// 0x00000102 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c::.ctor()
extern void U3CU3Ec__ctor_mF80895DD032961F149235217FCD848988CE11F9C (void);
// 0x00000103 UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator UnityEngine.AddressableAssets.AddressablesImpl/<>c::<get_ResourceLocators>b__51_0(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec_U3Cget_ResourceLocatorsU3Eb__51_0_mBBC7E4C00C5B24BD50F4E4C8D5476E696F6814FC (void);
// 0x00000104 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/<>c::<get_CatalogsWithAvailableUpdates>b__122_0(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec_U3Cget_CatalogsWithAvailableUpdatesU3Eb__122_0_mB2DA8D81C64536BDF6985D7CC0E3CC984667ABDB (void);
// 0x00000105 System.String UnityEngine.AddressableAssets.AddressablesImpl/<>c::<get_CatalogsWithAvailableUpdates>b__122_1(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec_U3Cget_CatalogsWithAvailableUpdatesU3Eb__122_1_m6B6239FB63EC8387B9142BFFA0B54FC95BE4DE9B (void);
// 0x00000106 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_mEEF2D488E3840C320349B1637D7949BDC46E6C16 (void);
// 0x00000107 System.Boolean UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass53_0::<RemoveResourceLocator>b__0(UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo)
extern void U3CU3Ec__DisplayClass53_0_U3CRemoveResourceLocatorU3Eb__0_m276C46AFC10A31F94DC0CC2F2DDE9AD9E209BFDD (void);
// 0x00000108 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m1A9D8A8D34D2F41BD1EB8E7FDC1196FD3BCA7051 (void);
// 0x00000109 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass60_0::<LoadContentCatalogAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass60_0_U3CLoadContentCatalogAsyncU3Eb__0_m28C5F432D5D23E56E5713B4BF806C8008F7CF0B7 (void);
// 0x0000010A System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass60_0::<LoadContentCatalogAsync>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>)
extern void U3CU3Ec__DisplayClass60_0_U3CLoadContentCatalogAsyncU3Eb__1_m1B0A1DAAD02E67D79E7CE9F39DA6A8B13835FEB5 (void);
// 0x0000010B System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass66_0`1::.ctor()
// 0x0000010C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass66_0`1::<LoadAssetWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x0000010D System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_mAA76CEE7F2EB371FB268C00868EEA1149B183844 (void);
// 0x0000010E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass70_0::<LoadResourceLocationsWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass70_0_U3CLoadResourceLocationsWithChainU3Eb__0_m8301D18E79C1115DF44E90E69C20DBC51B5EF4D4 (void);
// 0x0000010F System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass72_0::.ctor()
extern void U3CU3Ec__DisplayClass72_0__ctor_m22FF4B277F2C39CAE3441AA4394309D6C03890D6 (void);
// 0x00000110 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass72_0::<LoadResourceLocationsWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass72_0_U3CLoadResourceLocationsWithChainU3Eb__0_mE29B861ADAA7EF7D2B0D3EB16BAE65ACD8AFD3B9 (void);
// 0x00000111 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass75_0`1::.ctor()
// 0x00000112 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass75_0`1::<LoadAssetsWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000113 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass77_0`1::.ctor()
// 0x00000114 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass77_0`1::<LoadAssetsWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000115 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass85_0::.ctor()
extern void U3CU3Ec__DisplayClass85_0__ctor_m6C493BD183735DFD8F187810C30A3C35D70ABC24 (void);
// 0x00000116 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass85_0::<GetDownloadSizeWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass85_0_U3CGetDownloadSizeWithChainU3Eb__0_mCCB904A48B29CF6D927A5E4070D2F10B59939ED9 (void);
// 0x00000117 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass86_0::.ctor()
extern void U3CU3Ec__DisplayClass86_0__ctor_mAAAAF10FB90900E31FE9A79E69731F491E635137 (void);
// 0x00000118 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Int64> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass86_0::<GetDownloadSizeWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass86_0_U3CGetDownloadSizeWithChainU3Eb__0_mCB5A4ABC8FEC8FDE68C54A8F3ECDE0A73B48CAE6 (void);
// 0x00000119 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass89_0::.ctor()
extern void U3CU3Ec__DisplayClass89_0__ctor_mF5D6A8604A5B04D424202916A0F6DCCC4026CEB3 (void);
// 0x0000011A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass89_0::<DownloadDependenciesAsyncWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass89_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_m379801DD72BAE2FF2BB383BC6001CCFF840D6C7F (void);
// 0x0000011B System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass89_0::<DownloadDependenciesAsyncWithChain>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>>)
extern void U3CU3Ec__DisplayClass89_0_U3CDownloadDependenciesAsyncWithChainU3Eb__1_m30D531506E5B0911F6D5F9493E79ADF7E7473AB1 (void);
// 0x0000011C System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass92_0::.ctor()
extern void U3CU3Ec__DisplayClass92_0__ctor_m87E39DFE717183553547BE00A74C4E75F0B1C5D3 (void);
// 0x0000011D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass92_0::<DownloadDependenciesAsyncWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass92_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_m3C508242DFA3C5F9E33D1F22BE38F6017DC595B7 (void);
// 0x0000011E System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass92_0::<DownloadDependenciesAsyncWithChain>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>>)
extern void U3CU3Ec__DisplayClass92_0_U3CDownloadDependenciesAsyncWithChainU3Eb__1_mF939C952742E569E1BB45CEE1B94E520C9C33CF9 (void);
// 0x0000011F System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass94_0::.ctor()
extern void U3CU3Ec__DisplayClass94_0__ctor_m3A5E4400DEB224FA1F67EAE4D99687910F5F552F (void);
// 0x00000120 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass94_0::<DownloadDependenciesAsyncWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass94_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_mC4366D6C7E90D135017029573B24F977DA2BE921 (void);
// 0x00000121 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass94_0::<DownloadDependenciesAsyncWithChain>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource>>)
extern void U3CU3Ec__DisplayClass94_0_U3CDownloadDependenciesAsyncWithChainU3Eb__1_m7FAC15C4293A43668DF7698AF7B1BD7EC352379D (void);
// 0x00000122 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass97_0::.ctor()
extern void U3CU3Ec__DisplayClass97_0__ctor_m32ADA65CFAEDBB2E8B48E0F31FC4042D481A930E (void);
// 0x00000123 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass97_0::<ClearDependencyCacheAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass97_0_U3CClearDependencyCacheAsyncU3Eb__0_mC4DDDF0AF4EC0F28DD0BD09DC610EF31D4C5EC22 (void);
// 0x00000124 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass98_0::.ctor()
extern void U3CU3Ec__DisplayClass98_0__ctor_m172896A28667FCC4A09F93487DA41B2F2C5ADAF7 (void);
// 0x00000125 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass98_0::<ClearDependencyCacheAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass98_0_U3CClearDependencyCacheAsyncU3Eb__0_m555A4E75D9EE591E16768FFABABEB3A170FA862B (void);
// 0x00000126 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass99_0::.ctor()
extern void U3CU3Ec__DisplayClass99_0__ctor_mFEBE0247BA067F0D735346050F3E12B5E34E8961 (void);
// 0x00000127 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass99_0::<ClearDependencyCacheAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass99_0_U3CClearDependencyCacheAsyncU3Eb__0_m3FBE206E8F99096801FFB71D72BB95360A57ED8D (void);
// 0x00000128 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass104_0::.ctor()
extern void U3CU3Ec__DisplayClass104_0__ctor_m52D0EF2F2A23858ECB5248941D1D7DD9A32354D8 (void);
// 0x00000129 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass104_0::<InstantiateWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass104_0_U3CInstantiateWithChainU3Eb__0_mC6C1C332F5E7626A6960764AC94483677448018D (void);
// 0x0000012A System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass106_0::.ctor()
extern void U3CU3Ec__DisplayClass106_0__ctor_m14F475158A764AAA956F68944993B1B46F0BB097 (void);
// 0x0000012B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass106_0::<InstantiateWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass106_0_U3CInstantiateWithChainU3Eb__0_m0E209C9091E1142E757F3E1782EBCFB955CA781B (void);
// 0x0000012C System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass109_0::.ctor()
extern void U3CU3Ec__DisplayClass109_0__ctor_m7239ED44514B79B2D019D10AFA76A8E7E42A2D21 (void);
// 0x0000012D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass109_0::<LoadSceneWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass109_0_U3CLoadSceneWithChainU3Eb__0_m87230ADE3583440F51FE7BE4809CC67C16068BB7 (void);
// 0x0000012E System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass115_0::.ctor()
extern void U3CU3Ec__DisplayClass115_0__ctor_m06B200FEA4A019D4D9FBE76F0BAE0C00CF6FA1CA (void);
// 0x0000012F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass115_0::<CreateUnloadSceneWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass115_0_U3CCreateUnloadSceneWithChainU3Eb__0_m9D73EA0D587AA8141168413581BA0A5A98D73055 (void);
// 0x00000130 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass116_0::.ctor()
extern void U3CU3Ec__DisplayClass116_0__ctor_m608071B61C8B59CF8484E46BCB39366C5B4B1F02 (void);
// 0x00000131 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass116_0::<CreateUnloadSceneWithChain>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void U3CU3Ec__DisplayClass116_0_U3CCreateUnloadSceneWithChainU3Eb__0_mCB75653E581B2AB7A6F1013ABF742CD5046A4604 (void);
// 0x00000132 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass123_0::.ctor()
extern void U3CU3Ec__DisplayClass123_0__ctor_m0CAC1B0511D449C5963E1A18E7CE6523237A7DD0 (void);
// 0x00000133 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass123_0::<UpdateCatalogs>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>>)
extern void U3CU3Ec__DisplayClass123_0_U3CUpdateCatalogsU3Eb__0_mB0E9D34ACEBDBAAD2C2D58569BC45EC05D744589 (void);
// 0x00000134 System.Void UnityEngine.AddressableAssets.AddressablesImpl/<>c__DisplayClass123_0::<UpdateCatalogs>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void U3CU3Ec__DisplayClass123_0_U3CUpdateCatalogsU3Eb__1_m73E2B35B4C2B6F9543F693CB5F86F0D36AF89A7F (void);
// 0x00000135 System.String UnityEngine.AddressableAssets.AssetLabelReference::get_labelString()
extern void AssetLabelReference_get_labelString_mD59B72403465A4860CCF78FB546FE69CFFFC5622 (void);
// 0x00000136 System.Void UnityEngine.AddressableAssets.AssetLabelReference::set_labelString(System.String)
extern void AssetLabelReference_set_labelString_m882B1996DCA9934C1FB2B5DA165B6F20B536EDEE (void);
// 0x00000137 System.Object UnityEngine.AddressableAssets.AssetLabelReference::get_RuntimeKey()
extern void AssetLabelReference_get_RuntimeKey_mA4EDB42A283468905F1818D537491032E154BA7E (void);
// 0x00000138 System.Boolean UnityEngine.AddressableAssets.AssetLabelReference::RuntimeKeyIsValid()
extern void AssetLabelReference_RuntimeKeyIsValid_m4BFF940547119F1F6B086FE4E84328A8596E747E (void);
// 0x00000139 System.Int32 UnityEngine.AddressableAssets.AssetLabelReference::GetHashCode()
extern void AssetLabelReference_GetHashCode_m15FC2ADC5A186D4A6F2D4F018C8BBFE7CCB777F0 (void);
// 0x0000013A System.Void UnityEngine.AddressableAssets.AssetLabelReference::.ctor()
extern void AssetLabelReference__ctor_mFA533405E7B5DE7AF46E044BB5BE3622BEEB54D1 (void);
// 0x0000013B System.Void UnityEngine.AddressableAssets.AssetReferenceT`1::.ctor(System.String)
// 0x0000013C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AssetReferenceT`1::LoadAsset()
// 0x0000013D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AssetReferenceT`1::LoadAssetAsync()
// 0x0000013E System.Boolean UnityEngine.AddressableAssets.AssetReferenceT`1::ValidateAsset(UnityEngine.Object)
// 0x0000013F System.Boolean UnityEngine.AddressableAssets.AssetReferenceT`1::ValidateAsset(System.String)
// 0x00000140 System.Void UnityEngine.AddressableAssets.AssetReferenceGameObject::.ctor(System.String)
extern void AssetReferenceGameObject__ctor_m2224F8AF5D9FD66A0061B04B8D5F5FF0C54AAFA0 (void);
// 0x00000141 System.Void UnityEngine.AddressableAssets.AssetReferenceTexture::.ctor(System.String)
extern void AssetReferenceTexture__ctor_mA0C17276E8543A776744C24E79B28A97DD8EE86A (void);
// 0x00000142 System.Void UnityEngine.AddressableAssets.AssetReferenceTexture2D::.ctor(System.String)
extern void AssetReferenceTexture2D__ctor_m6073FEF7EB30A8201C97BC93BE51C3271C45045C (void);
// 0x00000143 System.Void UnityEngine.AddressableAssets.AssetReferenceTexture3D::.ctor(System.String)
extern void AssetReferenceTexture3D__ctor_mE95547D963764AEB3E6E4D79035968B134B4197A (void);
// 0x00000144 System.Void UnityEngine.AddressableAssets.AssetReferenceSprite::.ctor(System.String)
extern void AssetReferenceSprite__ctor_mD6A5EE1BBEB557114F8C168813DA7FB78ACE1BC9 (void);
// 0x00000145 System.Boolean UnityEngine.AddressableAssets.AssetReferenceSprite::ValidateAsset(System.String)
extern void AssetReferenceSprite_ValidateAsset_m5C656AD60659BE608B5C63431BD83A132CFAB69A (void);
// 0x00000146 System.Void UnityEngine.AddressableAssets.AssetReferenceAtlasedSprite::.ctor(System.String)
extern void AssetReferenceAtlasedSprite__ctor_m1761A66E91F0AD050D9DC8D5D9EFDEC62E826BB5 (void);
// 0x00000147 System.Boolean UnityEngine.AddressableAssets.AssetReferenceAtlasedSprite::ValidateAsset(System.String)
extern void AssetReferenceAtlasedSprite_ValidateAsset_m9DB5B0519722A771CA3B82A163AE5033EE7A17B0 (void);
// 0x00000148 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.AddressableAssets.AssetReference::get_OperationHandle()
extern void AssetReference_get_OperationHandle_mB4BAAB57C9A519BDDA6E2308BA157FF8929728AF (void);
// 0x00000149 System.Object UnityEngine.AddressableAssets.AssetReference::get_RuntimeKey()
extern void AssetReference_get_RuntimeKey_m53C038FD2A954CE6382C683D6FB72A7049D91684 (void);
// 0x0000014A System.String UnityEngine.AddressableAssets.AssetReference::get_AssetGUID()
extern void AssetReference_get_AssetGUID_m3B9DC49BB25B4E5EEED574C33FB6D0E372B97C8B (void);
// 0x0000014B System.String UnityEngine.AddressableAssets.AssetReference::get_SubObjectName()
extern void AssetReference_get_SubObjectName_mB053749C44EB7B019BDE026DDC1830589BD93966 (void);
// 0x0000014C System.Void UnityEngine.AddressableAssets.AssetReference::set_SubObjectName(System.String)
extern void AssetReference_set_SubObjectName_m3263E26040C68E634FA1348E2A73B9B3D0E1762D (void);
// 0x0000014D System.Type UnityEngine.AddressableAssets.AssetReference::get_SubOjbectType()
extern void AssetReference_get_SubOjbectType_m59C79C425452536B2548FB958E96DB67BFB1EC84 (void);
// 0x0000014E System.Boolean UnityEngine.AddressableAssets.AssetReference::IsValid()
extern void AssetReference_IsValid_m00FA122AB61CE13B8559471526AB1D57CD3648BF (void);
// 0x0000014F System.Boolean UnityEngine.AddressableAssets.AssetReference::get_IsDone()
extern void AssetReference_get_IsDone_m1DB636F5D907D7D792B43FA2CD477A86E55F9681 (void);
// 0x00000150 System.Void UnityEngine.AddressableAssets.AssetReference::.ctor()
extern void AssetReference__ctor_m46BDFDC071BFC1D60C817A1C5B41A16CBA1F1C66 (void);
// 0x00000151 System.Void UnityEngine.AddressableAssets.AssetReference::.ctor(System.String)
extern void AssetReference__ctor_mE6785CAB054C044B348B64250D9904D1C6A3B61E (void);
// 0x00000152 UnityEngine.Object UnityEngine.AddressableAssets.AssetReference::get_Asset()
extern void AssetReference_get_Asset_mF316D0153DAB80E3593997C854E96A21B80D092C (void);
// 0x00000153 System.String UnityEngine.AddressableAssets.AssetReference::ToString()
extern void AssetReference_ToString_mD0986FBA89F75545C76909CD3B7D56CC0696C1F5 (void);
// 0x00000154 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T> UnityEngine.AddressableAssets.AssetReference::CreateFailedOperation()
// 0x00000155 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AssetReference::LoadAsset()
// 0x00000156 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AssetReference::LoadScene()
extern void AssetReference_LoadScene_m6AFBBDBF59A4BA4DC807CA75047119D9693C1D6F (void);
// 0x00000157 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AssetReference::Instantiate(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void AssetReference_Instantiate_mC320C78B3EB3684BE0CF806FDAF041174F77E11F (void);
// 0x00000158 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AssetReference::Instantiate(UnityEngine.Transform,System.Boolean)
extern void AssetReference_Instantiate_m1FB73D6398004633844103B443BBA8E7A423738F (void);
// 0x00000159 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.AddressableAssets.AssetReference::LoadAssetAsync()
// 0x0000015A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AssetReference::LoadSceneAsync(UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void AssetReference_LoadSceneAsync_mE7EBA1B3BD636157883FC6579B43C4C56D1DD7B5 (void);
// 0x0000015B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.AddressableAssets.AssetReference::UnLoadScene()
extern void AssetReference_UnLoadScene_m5D3FC5DD78B5533A4D10EA16436914A105C4C1F8 (void);
// 0x0000015C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AssetReference::InstantiateAsync(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void AssetReference_InstantiateAsync_m639FCE542FB03D1F21D46380FAB6D15E5F2D5AF7 (void);
// 0x0000015D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.AddressableAssets.AssetReference::InstantiateAsync(UnityEngine.Transform,System.Boolean)
extern void AssetReference_InstantiateAsync_mE50F81174A8DA19F2348C760B61DA80AEE04CD93 (void);
// 0x0000015E System.Boolean UnityEngine.AddressableAssets.AssetReference::RuntimeKeyIsValid()
extern void AssetReference_RuntimeKeyIsValid_m3E9D167E60996FF8D81F580A80CB8BA4DAF9E4FC (void);
// 0x0000015F System.Void UnityEngine.AddressableAssets.AssetReference::ReleaseAsset()
extern void AssetReference_ReleaseAsset_m431802AD4EF73EE220B506308D7A3EC47DC34851 (void);
// 0x00000160 System.Void UnityEngine.AddressableAssets.AssetReference::ReleaseInstance(UnityEngine.GameObject)
extern void AssetReference_ReleaseInstance_m6FB49CF792970765BF400B237C1187884052FB87 (void);
// 0x00000161 System.Boolean UnityEngine.AddressableAssets.AssetReference::ValidateAsset(UnityEngine.Object)
extern void AssetReference_ValidateAsset_mDE6F7EF70C3C45F0D5C40D175AE29F3FB87B0A9E (void);
// 0x00000162 System.Boolean UnityEngine.AddressableAssets.AssetReference::ValidateAsset(System.String)
extern void AssetReference_ValidateAsset_m77B9DDB2DCF842315D6B1008C54A41553FDEE86A (void);
// 0x00000163 System.Object UnityEngine.AddressableAssets.IKeyEvaluator::get_RuntimeKey()
// 0x00000164 System.Boolean UnityEngine.AddressableAssets.IKeyEvaluator::RuntimeKeyIsValid()
// 0x00000165 System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void CheckCatalogsOperation__ctor_m001A86CDA6AF9A0FAE3AB17B875941BB4BB372B7 (void);
// 0x00000166 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>> UnityEngine.AddressableAssets.CheckCatalogsOperation::Start(System.Collections.Generic.List`1<UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo>)
extern void CheckCatalogsOperation_Start_mC606668524E71A54954A02D6D8223584A6F7859C (void);
// 0x00000167 System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation::Destroy()
extern void CheckCatalogsOperation_Destroy_mB9F8845351DFDB4F148C18B2A8EA63B398FC5FA9 (void);
// 0x00000168 System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void CheckCatalogsOperation_GetDependencies_m94B28B65B6C1EDB35C6197D98B777B65CECE8F0B (void);
// 0x00000169 System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation::Execute()
extern void CheckCatalogsOperation_Execute_mEF08CD547922397D9DE1D386BEA57D3BD4C1CDCB (void);
// 0x0000016A System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation/<>c::.cctor()
extern void U3CU3Ec__cctor_m763929708DCF74CD18989AFEE9D2A3380E7D449F (void);
// 0x0000016B System.Void UnityEngine.AddressableAssets.CheckCatalogsOperation/<>c::.ctor()
extern void U3CU3Ec__ctor_m3A8EB8F91F6762E238F130CE3378A49352E47215 (void);
// 0x0000016C System.Boolean UnityEngine.AddressableAssets.CheckCatalogsOperation/<>c::<Start>b__5_0(UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider)
extern void U3CU3Ec_U3CStartU3Eb__5_0_mBD33EDAE0E282645311F1FBDB63BE37B23F7FB76 (void);
// 0x0000016D System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void UpdateCatalogsOperation__ctor_mAEA17D1BDDD7BD34200E355988A3D65217272D8B (void);
// 0x0000016E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.UpdateCatalogsOperation::Start(System.Collections.Generic.IEnumerable`1<System.String>)
extern void UpdateCatalogsOperation_Start_m6E88A765FFD8B4711796B5712C0CD31A2A100499 (void);
// 0x0000016F System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::Destroy()
extern void UpdateCatalogsOperation_Destroy_m1F3A40E0AF0A3F3FF8207C65523FA0591E5C65CA (void);
// 0x00000170 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void UpdateCatalogsOperation_GetDependencies_m02994A4CDFB683FE17233F87DAEC58F294BAD6EE (void);
// 0x00000171 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation::Execute()
extern void UpdateCatalogsOperation_Execute_mB21E9B8BFB6FE2B37A2DD5F9DA31D5F6A6EF3DF6 (void);
// 0x00000172 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation/<>c::.cctor()
extern void U3CU3Ec__cctor_m857753E805839903F2BC7F7AEBCE16E4DAE18536 (void);
// 0x00000173 System.Void UnityEngine.AddressableAssets.UpdateCatalogsOperation/<>c::.ctor()
extern void U3CU3Ec__ctor_m01C579E9C120F21F7E733812D6AD04C44762CE4B (void);
// 0x00000174 System.Boolean UnityEngine.AddressableAssets.UpdateCatalogsOperation/<>c::<Start>b__4_0(UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider)
extern void U3CU3Ec_U3CStartU3Eb__4_0_m97D1006FD50716A4F919D9B3FF2E95FC4F9064F9 (void);
// 0x00000175 System.String UnityEngine.AddressableAssets.DynamicResourceLocator::get_LocatorId()
extern void DynamicResourceLocator_get_LocatorId_m6377861BDFB96A3D7537138F8B3CC92C2EC44BF9 (void);
// 0x00000176 System.Collections.Generic.IEnumerable`1<System.Object> UnityEngine.AddressableAssets.DynamicResourceLocator::get_Keys()
extern void DynamicResourceLocator_get_Keys_mADFD7AAEE7E7B4FE7499A754B0D219AE03A0DD6F (void);
// 0x00000177 System.Void UnityEngine.AddressableAssets.DynamicResourceLocator::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void DynamicResourceLocator__ctor_m3E4F35E7D895831EE76A3FEE63921169C3CC2781 (void);
// 0x00000178 System.Boolean UnityEngine.AddressableAssets.DynamicResourceLocator::Locate(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void DynamicResourceLocator_Locate_mC48F124C4425265E6E42632408DC313533723E02 (void);
// 0x00000179 System.Void UnityEngine.AddressableAssets.DynamicResourceLocator::CreateDynamicLocations(System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.String,System.String,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void DynamicResourceLocator_CreateDynamicLocations_m8D88AEB304D7601E15F999845CD9A7C576216F4A (void);
// 0x0000017A UnityEngine.AddressableAssets.AddressablesPlatform UnityEngine.AddressableAssets.PlatformMappingService::GetAddressablesPlatformInternal(UnityEngine.RuntimePlatform)
extern void PlatformMappingService_GetAddressablesPlatformInternal_m97FC778BFAA18E1D123FDC738A0C717531F908E2 (void);
// 0x0000017B UnityEngine.AddressableAssets.AddressablesPlatform UnityEngine.AddressableAssets.PlatformMappingService::GetPlatform()
extern void PlatformMappingService_GetPlatform_m640E7D9426E0615A3DC3D5436A0CDBB6DBDDFB50 (void);
// 0x0000017C System.Void UnityEngine.AddressableAssets.PlatformMappingService::.ctor()
extern void PlatformMappingService__ctor_m4176958626D49CDF1D0A7F163284C0785991E95F (void);
// 0x0000017D System.Void UnityEngine.AddressableAssets.PlatformMappingService::.cctor()
extern void PlatformMappingService__cctor_m61A0B8E6F8CD5732F4241A3B57EBAF1750565E04 (void);
// 0x0000017E UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent UnityEngine.AddressableAssets.Utility.DiagnosticInfo::CreateEvent(System.String,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.Int32)
extern void DiagnosticInfo_CreateEvent_m7BC23DB8C1758E94200B9A5A71161BD2847C22D3 (void);
// 0x0000017F System.Void UnityEngine.AddressableAssets.Utility.DiagnosticInfo::.ctor()
extern void DiagnosticInfo__ctor_mC37E66C328AB9D0CC49A5E7CD3DA46325CCD13BE (void);
// 0x00000180 System.Void UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::.ctor(UnityEngine.ResourceManagement.ResourceManager)
extern void ResourceManagerDiagnostics__ctor_m2A3BA0AAE462F5CA7197D640C7ABBBBAE511232D (void);
// 0x00000181 System.Int32 UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::SumDependencyNameHashCodes(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManagerDiagnostics_SumDependencyNameHashCodes_m0DE26E26EBD9761F9266508CC40639A28F9AD115 (void);
// 0x00000182 System.Int32 UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::CalculateHashCode(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManagerDiagnostics_CalculateHashCode_mCFCD981FBB120165EC6BFED2FD3124E50DDEEC31 (void);
// 0x00000183 System.Void UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::OnResourceManagerDiagnosticEvent(UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext)
extern void ResourceManagerDiagnostics_OnResourceManagerDiagnosticEvent_m1743AD94BFC8DF4B39ADE60E520BD59421189E91 (void);
// 0x00000184 System.Void UnityEngine.AddressableAssets.Utility.ResourceManagerDiagnostics::Dispose()
extern void ResourceManagerDiagnostics_Dispose_m085D3FBBE22EA40BD062CF038800E20A86A00D6F (void);
// 0x00000185 System.Int32 UnityEngine.AddressableAssets.Utility.SerializationUtilities::ReadInt32FromByteArray(System.Byte[],System.Int32)
extern void SerializationUtilities_ReadInt32FromByteArray_m4A998891901FC83A215146AEF87B175D128869FC (void);
// 0x00000186 System.Int32 UnityEngine.AddressableAssets.Utility.SerializationUtilities::WriteInt32ToByteArray(System.Byte[],System.Int32,System.Int32)
extern void SerializationUtilities_WriteInt32ToByteArray_m7C3F1DF7E37CD23D76931AE5B6369A959EF8BA86 (void);
// 0x00000187 System.Object UnityEngine.AddressableAssets.Utility.SerializationUtilities::ReadObjectFromByteArray(System.Byte[],System.Int32)
extern void SerializationUtilities_ReadObjectFromByteArray_mAC53DA56CEB59215F9CA0B44D9E0B406B23AB733 (void);
// 0x00000188 System.Int32 UnityEngine.AddressableAssets.Utility.SerializationUtilities::WriteObjectToByteList(System.Object,System.Collections.Generic.List`1<System.Byte>)
extern void SerializationUtilities_WriteObjectToByteList_m3C9303097352A716AA3FBCBA5BC3B5CB90C9824D (void);
// 0x00000189 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider::.ctor(UnityEngine.ResourceManagement.ResourceManager)
extern void ContentCatalogProvider__ctor_m4A8C166F0598190FB083D72299620C0CE2A12AA6 (void);
// 0x0000018A System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void ContentCatalogProvider_Release_m0F1F55F8205680BAAF6BA6BAD5D9B3808867CB67 (void);
// 0x0000018B System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void ContentCatalogProvider_Provide_m342DAE0BBCE88808163642D8D0A36005B8D4FC30 (void);
// 0x0000018C System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle,System.Boolean,System.Boolean)
extern void InternalOp_Start_m32B3A0491CEA49C08B83AD65526A9FCDA63130E4 (void);
// 0x0000018D System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::Release()
extern void InternalOp_Release_m6A48F23B0BF67A54472E72A19C6B6ABF6B52D2DD (void);
// 0x0000018E System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::LoadCatalog(System.String,System.Boolean,System.Boolean)
extern void InternalOp_LoadCatalog_mC080EB46BDACBCEA41F2F7C668AAAB1376DCAE0B (void);
// 0x0000018F System.String UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::GetTransformedInternalId(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void InternalOp_GetTransformedInternalId_mFA529AEC6097418B89A9647A6A308B48B58D3EF8 (void);
// 0x00000190 System.String UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::DetermineIdToLoad(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Collections.Generic.IList`1<System.Object>,System.Boolean)
extern void InternalOp_DetermineIdToLoad_mCE7D096E09A898F73672207445CBBED926B7EE73 (void);
// 0x00000191 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::OnCatalogLoaded(UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData)
extern void InternalOp_OnCatalogLoaded_m32E0C229A2C69C41BCF6FB5E4759F54D1A7EA7AF (void);
// 0x00000192 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::.ctor()
extern void InternalOp__ctor_m00A19525A60DBF2EF367EF517DA10C4624018CA5 (void);
// 0x00000193 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::<LoadCatalog>b__7_0(UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData)
extern void InternalOp_U3CLoadCatalogU3Eb__7_0_mE2044A89530400AB20A702228205F845B0909D17 (void);
// 0x00000194 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp::<LoadCatalog>b__7_1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>)
extern void InternalOp_U3CLoadCatalogU3Eb__7_1_mF11E9BFFB660F9158273D073CAB608F514333AD8 (void);
// 0x00000195 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::add_OnLoaded(System.Action`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>)
extern void BundledCatalog_add_OnLoaded_m08D1DC44179DEA7AC6533698998B35D0A2F7E843 (void);
// 0x00000196 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::remove_OnLoaded(System.Action`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>)
extern void BundledCatalog_remove_OnLoaded_mEFC260E4F15C57F7E7FA144AC70A522DE1047B43 (void);
// 0x00000197 System.Boolean UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::get_OpInProgress()
extern void BundledCatalog_get_OpInProgress_m28D11B2D0119E8815165C869EE6F8C05E378332B (void);
// 0x00000198 System.Boolean UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::get_OpIsSuccess()
extern void BundledCatalog_get_OpIsSuccess_mE7C858B1C1E424D63C6063B27E4A7A0D2FF7D1D4 (void);
// 0x00000199 System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::.ctor(System.String)
extern void BundledCatalog__ctor_m9818983D8EA29EBBDFC2BC7EB0D66DC93FE85078 (void);
// 0x0000019A System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::Finalize()
extern void BundledCatalog_Finalize_m998C5533738BE626BB99B3FC97E9DC5573DEA0A1 (void);
// 0x0000019B System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::Unload()
extern void BundledCatalog_Unload_mBBF241B2FA8ED9B62552C0A8A93098611F4E9CF1 (void);
// 0x0000019C System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::LoadCatalogFromBundleAsync()
extern void BundledCatalog_LoadCatalogFromBundleAsync_m87EC9B066E4E9145FA22DE481C354F69EF197F39 (void);
// 0x0000019D System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::<LoadCatalogFromBundleAsync>b__16_0(UnityEngine.AsyncOperation)
extern void BundledCatalog_U3CLoadCatalogFromBundleAsyncU3Eb__16_0_m2B9D73C323D8EECD0F5A8E54F7FB5C72B521C032 (void);
// 0x0000019E System.Void UnityEngine.AddressableAssets.ResourceProviders.ContentCatalogProvider/InternalOp/BundledCatalog::<LoadCatalogFromBundleAsync>b__16_1(UnityEngine.AsyncOperation)
extern void BundledCatalog_U3CLoadCatalogFromBundleAsyncU3Eb__16_1_mB79EE7829A6B850788D4D24ED7A84E362DB168AE (void);
// 0x0000019F System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_InternalId()
extern void ContentCatalogDataEntry_get_InternalId_mFB37FEB717C2B5D130971F8053BA9DA25B0C4368 (void);
// 0x000001A0 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_InternalId(System.String)
extern void ContentCatalogDataEntry_set_InternalId_m87C09E2098D5A89572421AA1538826C73CE9BDCB (void);
// 0x000001A1 System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_Provider()
extern void ContentCatalogDataEntry_get_Provider_m2E5B4395011ED9C3BA10C5288B94F6A52347B522 (void);
// 0x000001A2 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_Provider(System.String)
extern void ContentCatalogDataEntry_set_Provider_m57E6C75DB0E0AFDB10242312F2ED439C764DFB32 (void);
// 0x000001A3 System.Collections.Generic.List`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_Keys()
extern void ContentCatalogDataEntry_get_Keys_mBEA0556C9890073F8B56DB307FFCD277421977E7 (void);
// 0x000001A4 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_Keys(System.Collections.Generic.List`1<System.Object>)
extern void ContentCatalogDataEntry_set_Keys_m15E40907903F27A494730DBC44917B2F8E58B033 (void);
// 0x000001A5 System.Collections.Generic.List`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_Dependencies()
extern void ContentCatalogDataEntry_get_Dependencies_m5C1F0348EADF4890D1FB030613E4C1B5183A720D (void);
// 0x000001A6 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_Dependencies(System.Collections.Generic.List`1<System.Object>)
extern void ContentCatalogDataEntry_set_Dependencies_m3FA6E60076D1F9B9851E24458567F6BAED1B2932 (void);
// 0x000001A7 System.Object UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_Data()
extern void ContentCatalogDataEntry_get_Data_mC70E2483CD068C901C5FC29D4152818AC509C5DF (void);
// 0x000001A8 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_Data(System.Object)
extern void ContentCatalogDataEntry_set_Data_mFECE94F66B67ED56BF63B1AB0030344418917675 (void);
// 0x000001A9 System.Type UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::get_ResourceType()
extern void ContentCatalogDataEntry_get_ResourceType_m721C512ACB304B0B75C755EEFFB8D58655138E46 (void);
// 0x000001AA System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::set_ResourceType(System.Type)
extern void ContentCatalogDataEntry_set_ResourceType_m51903647B9C10D88AFB570267E2BD5383D2A2B83 (void);
// 0x000001AB System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogDataEntry::.ctor(System.Type,System.String,System.String,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Object)
extern void ContentCatalogDataEntry__ctor_m35D853FE4BD8A796E2A36FF9C7D481DC746C75A9 (void);
// 0x000001AC System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_ProviderId()
extern void ContentCatalogData_get_ProviderId_mD08675BA7BD77473D1406C154BDC3243608EE17C (void);
// 0x000001AD System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::set_ProviderId(System.String)
extern void ContentCatalogData_set_ProviderId_mFEBBA31FD47C3FF8226C5BEFF0CFB0C99581CEFA (void);
// 0x000001AE UnityEngine.ResourceManagement.Util.ObjectInitializationData UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_InstanceProviderData()
extern void ContentCatalogData_get_InstanceProviderData_m1C8F75FC83CA8BCAAD687DCC3DA7A03D27C1181C (void);
// 0x000001AF System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::set_InstanceProviderData(UnityEngine.ResourceManagement.Util.ObjectInitializationData)
extern void ContentCatalogData_set_InstanceProviderData_m82A56B7162300C82F0007E3A8A09C7E168CE07E8 (void);
// 0x000001B0 UnityEngine.ResourceManagement.Util.ObjectInitializationData UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_SceneProviderData()
extern void ContentCatalogData_get_SceneProviderData_m5BC5272A27857E6603B0EA4D2901BBE3BA6CEDBA (void);
// 0x000001B1 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::set_SceneProviderData(UnityEngine.ResourceManagement.Util.ObjectInitializationData)
extern void ContentCatalogData_set_SceneProviderData_m4F12291B570D89F56271B2985DA37A3887D08569 (void);
// 0x000001B2 System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData> UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_ResourceProviderData()
extern void ContentCatalogData_get_ResourceProviderData_mB27A713ABE5105A00386E830FFAA63EF2A735611 (void);
// 0x000001B3 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::set_ResourceProviderData(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>)
extern void ContentCatalogData_set_ResourceProviderData_m35399808222F501DC47652DD8A3E22EDC9A7E230 (void);
// 0x000001B4 System.String[] UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_ProviderIds()
extern void ContentCatalogData_get_ProviderIds_m6EF0BF07042821BE2211AFF7D002078C76140D76 (void);
// 0x000001B5 System.String[] UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::get_InternalIds()
extern void ContentCatalogData_get_InternalIds_mEB3D0FAE03880663E7A1552FC62B8DFC68A65F1C (void);
// 0x000001B6 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::CleanData()
extern void ContentCatalogData_CleanData_mAED0C34FF6ECC590082A97E2CE9027AE3DA58541 (void);
// 0x000001B7 UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::CreateCustomLocator(System.String,System.String)
extern void ContentCatalogData_CreateCustomLocator_m99BA0D17CC77D91BC5FB307AB5F6B0CBC7A11731 (void);
// 0x000001B8 UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::CreateLocator(System.String)
extern void ContentCatalogData_CreateLocator_m14684F03CDD2C36D4294456F49D6E62EE2BCDB36 (void);
// 0x000001B9 System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::ExpandInternalId(System.String[],System.String)
extern void ContentCatalogData_ExpandInternalId_mE1B7E65B5BC2F697F3CD44315E8AAB8A2D53C919 (void);
// 0x000001BA System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData::.ctor()
extern void ContentCatalogData__ctor_m6E6EBB5EB01F469B6C1DFAA00BBE3E418746E4CD (void);
// 0x000001BB System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_InternalId()
extern void CompactLocation_get_InternalId_m924673579F96D30A92FC3F6D1412B04C34B46ADE (void);
// 0x000001BC System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_ProviderId()
extern void CompactLocation_get_ProviderId_m8984A92DD78F5178E404801F86B4E9861126A946 (void);
// 0x000001BD System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_Dependencies()
extern void CompactLocation_get_Dependencies_m00D2B27CCF44BEE6756E4B494C1B5ABFC0B5EE2C (void);
// 0x000001BE System.Boolean UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_HasDependencies()
extern void CompactLocation_get_HasDependencies_mBAFBB58126FAAAF5DC02B20061254967BA3B7650 (void);
// 0x000001BF System.Int32 UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_DependencyHashCode()
extern void CompactLocation_get_DependencyHashCode_mC7BA696649520BEE3BE793A0E4FBFC0908174216 (void);
// 0x000001C0 System.Object UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_Data()
extern void CompactLocation_get_Data_m0AAE40A976FFFFE28B454CF6FA547F2C6C81343A (void);
// 0x000001C1 System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_PrimaryKey()
extern void CompactLocation_get_PrimaryKey_m7619D46570D27544D9ADFFD67C71B76AF79FC0FD (void);
// 0x000001C2 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::set_PrimaryKey(System.String)
extern void CompactLocation_set_PrimaryKey_mAA96AF8389B0DD1BE342795386F4DBE8436C6363 (void);
// 0x000001C3 System.Type UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::get_ResourceType()
extern void CompactLocation_get_ResourceType_m3B3467CC3D01D5B81B75F56C2114818959B92B95 (void);
// 0x000001C4 System.String UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::ToString()
extern void CompactLocation_ToString_m9C014084C5019F97F65E5D091BDB2AC991E962E3 (void);
// 0x000001C5 System.Int32 UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::Hash(System.Type)
extern void CompactLocation_Hash_m8202FDA5FAFFB40D562CE01C8D6DEB51906F1401 (void);
// 0x000001C6 System.Void UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData/CompactLocation::.ctor(UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap,System.String,System.String,System.Object,System.Object,System.Int32,System.String,System.Type)
extern void CompactLocation__ctor_mECD42CDE7A9468E1205608E1DED10726E42094E1 (void);
// 0x000001C7 System.String UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator::get_LocatorId()
// 0x000001C8 System.Collections.Generic.IEnumerable`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator::get_Keys()
// 0x000001C9 System.Boolean UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator::Locate(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
// 0x000001CA System.Boolean UnityEngine.AddressableAssets.ResourceLocators.LegacyResourcesLocator::Locate(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void LegacyResourcesLocator_Locate_mC0DE4EA48FD0727C27881E9505ED9042860012DA (void);
// 0x000001CB System.Collections.Generic.IEnumerable`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.LegacyResourcesLocator::get_Keys()
extern void LegacyResourcesLocator_get_Keys_m7EAECDF4CF0FA60E95E02590252A80BA8654411E (void);
// 0x000001CC System.String UnityEngine.AddressableAssets.ResourceLocators.LegacyResourcesLocator::get_LocatorId()
extern void LegacyResourcesLocator_get_LocatorId_m9FF301DE2F149DBE70FA215322AE551F6787FC62 (void);
// 0x000001CD System.Void UnityEngine.AddressableAssets.ResourceLocators.LegacyResourcesLocator::.ctor()
extern void LegacyResourcesLocator__ctor_m57A60CBED17827A53BBDAC21801238BB7E524AB3 (void);
// 0x000001CE System.String[] UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_Keys()
extern void ResourceLocationData_get_Keys_m95E3FE87FAD4116ED7FCA22944798A556B56E072 (void);
// 0x000001CF System.String UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_InternalId()
extern void ResourceLocationData_get_InternalId_m7B07A2EC474A0768E5AABEF5F5C0E791732E8027 (void);
// 0x000001D0 System.String UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_Provider()
extern void ResourceLocationData_get_Provider_mAA09EA5D941C8C38A267FDE28153F3FD491A3CEC (void);
// 0x000001D1 System.String[] UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_Dependencies()
extern void ResourceLocationData_get_Dependencies_mB34D1E9CBA0A627B5425AD1EAF5A88C7B95B5BD5 (void);
// 0x000001D2 System.Type UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::get_ResourceType()
extern void ResourceLocationData_get_ResourceType_mFD9450F8EA61975402324BB8F2FF3D619F06CAD3 (void);
// 0x000001D3 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData::.ctor(System.String[],System.String,System.Type,System.Type,System.String[])
extern void ResourceLocationData__ctor_m0E7FD144A514493E811C7164D5B584A461B191A9 (void);
// 0x000001D4 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::.ctor(System.String,System.Int32)
extern void ResourceLocationMap__ctor_mC813DB53B5E8FFE370668D269514DF3C560ECF6B (void);
// 0x000001D5 System.String UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::get_LocatorId()
extern void ResourceLocationMap_get_LocatorId_m4B732D9F2E7C5D28E56761A9D894DED62D9D5DEE (void);
// 0x000001D6 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::set_LocatorId(System.String)
extern void ResourceLocationMap_set_LocatorId_m9E78B67DDCEE65AADB7564874C0E3DF2BC2A16E5 (void);
// 0x000001D7 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::.ctor(System.String,System.Collections.Generic.IList`1<UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData>)
extern void ResourceLocationMap__ctor_m261636B722FC0BE9E8FDBAD4A82BAAA6E576E3DD (void);
// 0x000001D8 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>> UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::get_Locations()
extern void ResourceLocationMap_get_Locations_m053D3170A234FD827295742240A5AB4ED8C94C80 (void);
// 0x000001D9 System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::set_Locations(System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>>)
extern void ResourceLocationMap_set_Locations_mF5479BA16A1818C48CB358E4584B503D25600431 (void);
// 0x000001DA System.Collections.Generic.IEnumerable`1<System.Object> UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::get_Keys()
extern void ResourceLocationMap_get_Keys_m69A47B7B44545D92BCE439B17DA5EFF8CE65262E (void);
// 0x000001DB System.Boolean UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::Locate(System.Object,System.Type,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>&)
extern void ResourceLocationMap_Locate_mECDC782C947D84FAF8039E47163E941278DA2E60 (void);
// 0x000001DC System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::Add(System.Object,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceLocationMap_Add_mADA3092718E8E26CC47C9D3BC4FF46355BBE662A (void);
// 0x000001DD System.Void UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap::Add(System.Object,System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
extern void ResourceLocationMap_Add_mB9919BC6957DA163FB74329612D856FBA7473796 (void);
// 0x000001DE System.Reflection.Assembly[] UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::GetAssemblies()
extern void AddressablesRuntimeProperties_GetAssemblies_m09AECD629BFC348E1A6757DA45B4FAE49323A632 (void);
// 0x000001DF System.Int32 UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::GetCachedValueCount()
extern void AddressablesRuntimeProperties_GetCachedValueCount_m2BD6E72F2379C60A57DC55C862047E3EABD1E6EB (void);
// 0x000001E0 System.Void UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::SetPropertyValue(System.String,System.String)
extern void AddressablesRuntimeProperties_SetPropertyValue_m29212BFCE259D325CC6F2CCDAC33E5D06F1FFA19 (void);
// 0x000001E1 System.Void UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::ClearCachedPropertyValues()
extern void AddressablesRuntimeProperties_ClearCachedPropertyValues_m60331DB73383166637F6803E65EE4E9F92F0FEC3 (void);
// 0x000001E2 System.String UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::EvaluateProperty(System.String)
extern void AddressablesRuntimeProperties_EvaluateProperty_mD99449A4FDD26E48DC0F72D873D565E36DFF178E (void);
// 0x000001E3 System.String UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::EvaluateString(System.String)
extern void AddressablesRuntimeProperties_EvaluateString_mDC0EDF6BE5F3D71A86D9368D65808341B33977F9 (void);
// 0x000001E4 System.String UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::EvaluateString(System.String,System.Char,System.Char,System.Func`2<System.String,System.String>)
extern void AddressablesRuntimeProperties_EvaluateString_mBE78D9C49C00AA51BAC9EFDDC2891D3896ED759B (void);
// 0x000001E5 System.Void UnityEngine.AddressableAssets.Initialization.AddressablesRuntimeProperties::.cctor()
extern void AddressablesRuntimeProperties__cctor_m72344F3970961EE4B6E857FDBAF9FC9601D03249 (void);
// 0x000001E6 System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitialization::Initialize(System.String,System.String)
extern void CacheInitialization_Initialize_m84C4CA343614C58EA62339A112936B5D2D82F991 (void);
// 0x000001E7 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.AddressableAssets.Initialization.CacheInitialization::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
extern void CacheInitialization_InitializeAsync_m88DBBF94C7F371BDDC6D83593561A55425121D75 (void);
// 0x000001E8 System.String UnityEngine.AddressableAssets.Initialization.CacheInitialization::get_RootPath()
extern void CacheInitialization_get_RootPath_mCA040C83CD18A5FFEE86DB278DC27BF96AC4EFA2 (void);
// 0x000001E9 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization::.ctor()
extern void CacheInitialization__ctor_m6D7F13CE6DF84327B832FE5DA423EB5FF3A4A68E (void);
// 0x000001EA System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::Init(System.Func`1<System.Boolean>)
extern void CacheInitOp_Init_m85BC46318C85BF88E32AE33E20EF6FBCC86B5A41 (void);
// 0x000001EB System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::Update(System.Single)
extern void CacheInitOp_Update_m0DC3663F60875894337799B6788618E87E9F9FAF (void);
// 0x000001EC System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::Execute()
extern void CacheInitOp_Execute_m4C7CC0D4553AA5CE6B2BFA8CC69F1CD06E676EA4 (void);
// 0x000001ED System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/CacheInitOp::.ctor()
extern void CacheInitOp__ctor_m444BACF330047EFB701B77CB3D2B9B39560E0EDA (void);
// 0x000001EE System.Void UnityEngine.AddressableAssets.Initialization.CacheInitialization/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mD5860E8EF9BE05A794379F42531D4079C8AACFAB (void);
// 0x000001EF System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitialization/<>c__DisplayClass1_0::<InitializeAsync>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CInitializeAsyncU3Eb__0_m2538DB099B7B6B28F096C7B427EE119B5B432EB9 (void);
// 0x000001F0 System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_CompressionEnabled()
extern void CacheInitializationData_get_CompressionEnabled_m1A6D2EE22AC36E83DFD290029B49A0FA646A4E1B (void);
// 0x000001F1 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_CompressionEnabled(System.Boolean)
extern void CacheInitializationData_set_CompressionEnabled_m049A27E23EB72E7F8F417BDF761FC1B62B307780 (void);
// 0x000001F2 System.String UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_CacheDirectoryOverride()
extern void CacheInitializationData_get_CacheDirectoryOverride_mE1EF452D155E7AA280A2DD8E39FC6CA5C2B95F07 (void);
// 0x000001F3 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_CacheDirectoryOverride(System.String)
extern void CacheInitializationData_set_CacheDirectoryOverride_mBB16148369183D82249B0FFA31A15561379123FA (void);
// 0x000001F4 System.Int32 UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_ExpirationDelay()
extern void CacheInitializationData_get_ExpirationDelay_m47D69B354674B647094AE9D6350073891D084E2C (void);
// 0x000001F5 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_ExpirationDelay(System.Int32)
extern void CacheInitializationData_set_ExpirationDelay_m128AB5CF5FBCC522C405CE9E06ED166012C420F2 (void);
// 0x000001F6 System.Boolean UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_LimitCacheSize()
extern void CacheInitializationData_get_LimitCacheSize_m74556C6B8530AC825FA4F1F90480C6DF5BB0EDBA (void);
// 0x000001F7 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_LimitCacheSize(System.Boolean)
extern void CacheInitializationData_set_LimitCacheSize_mDF1410AD0211710BD6EEF0D1DFCBB45322AAA1A1 (void);
// 0x000001F8 System.Int64 UnityEngine.AddressableAssets.Initialization.CacheInitializationData::get_MaximumCacheSize()
extern void CacheInitializationData_get_MaximumCacheSize_mA1DF53979B2B8FB504BD7199E274DE47FDACDAA9 (void);
// 0x000001F9 System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::set_MaximumCacheSize(System.Int64)
extern void CacheInitializationData_set_MaximumCacheSize_mADE0EA2E6EE5D93C35FF33A1B799647646498BE7 (void);
// 0x000001FA System.Void UnityEngine.AddressableAssets.Initialization.CacheInitializationData::.ctor()
extern void CacheInitializationData__ctor_m1D05D1B29EF2177B68B7436A14ECE60482C5ED8C (void);
// 0x000001FB System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation::.ctor(UnityEngine.AddressableAssets.AddressablesImpl)
extern void InitializationOperation__ctor_m50D593F5AB884ECCA24A06110BC70AC6E18B4C0D (void);
// 0x000001FC System.Single UnityEngine.AddressableAssets.Initialization.InitializationOperation::get_Progress()
extern void InitializationOperation_get_Progress_m19B5B699271709F8C49060D72BD3C4CCF9392D2B (void);
// 0x000001FD System.String UnityEngine.AddressableAssets.Initialization.InitializationOperation::get_DebugName()
extern void InitializationOperation_get_DebugName_m30EF56B368268A0A53AB09DAFE7FE9E61C5148EE (void);
// 0x000001FE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::CreateInitializationOperation(UnityEngine.AddressableAssets.AddressablesImpl,System.String,System.String)
extern void InitializationOperation_CreateInitializationOperation_m0A06B94DC1C0E72AF6A76829341EAB8BA3B53475 (void);
// 0x000001FF System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation::Execute()
extern void InitializationOperation_Execute_m73765CDF2ABBF0C5B42B3EEEE27D478C6E120621 (void);
// 0x00000200 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadProvider(UnityEngine.AddressableAssets.AddressablesImpl,UnityEngine.ResourceManagement.Util.ObjectInitializationData,System.String)
extern void InitializationOperation_LoadProvider_mBD87B26C88D9C7A665915F4022EA3A40C6EB68A3 (void);
// 0x00000201 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::OnCatalogDataLoaded(UnityEngine.AddressableAssets.AddressablesImpl,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>,System.String)
extern void InitializationOperation_OnCatalogDataLoaded_m68939EADAC84D1E9B945D1334F093546CACE2C34 (void);
// 0x00000202 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadContentCatalog(UnityEngine.AddressableAssets.AddressablesImpl,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String)
extern void InitializationOperation_LoadContentCatalog_m73FE2779679B767FC83B5FCD809ECC8F9BE66375 (void);
// 0x00000203 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadContentCatalog(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String)
extern void InitializationOperation_LoadContentCatalog_mB6F8DBE2F391B2BC6DCC5F10B718B7AEF67F2AAF (void);
// 0x00000204 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation::LoadContentCatalogInternal(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Int32,UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationMap)
extern void InitializationOperation_LoadContentCatalogInternal_mB56B5D92CE721F32017E15E29A304AEE408AA543 (void);
// 0x00000205 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c::.cctor()
extern void U3CU3Ec__cctor_m79CEA4105B6CB03F698DEE3211F212C5340FAD32 (void);
// 0x00000206 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c::.ctor()
extern void U3CU3Ec__ctor_m16C874F7A1A6028AB4ADA575E33B175649B20220 (void);
// 0x00000207 System.Boolean UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c::<Execute>b__11_0(UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider)
extern void U3CU3Ec_U3CExecuteU3Eb__11_0_m4447D36E6E5526706AFF1CCD1C5DCDD945AE4AEB (void);
// 0x00000208 System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mCD083B2540766C586BB16BDD77B124544584BAB2 (void);
// 0x00000209 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c__DisplayClass14_0::<LoadContentCatalog>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.ContentCatalogData>)
extern void U3CU3Ec__DisplayClass14_0_U3CLoadContentCatalogU3Eb__0_m71905AFDA419CB108FAABEBCDDFCC2AB18627959 (void);
// 0x0000020A System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mD50EAF1D4DA9BBDC92F9727BE8F20C9D9680539E (void);
// 0x0000020B System.Void UnityEngine.AddressableAssets.Initialization.InitializationOperation/<>c__DisplayClass16_0::<LoadContentCatalogInternal>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>)
extern void U3CU3Ec__DisplayClass16_0_U3CLoadContentCatalogInternalU3Eb__0_mC5EFF2C227A1A0C6C68C4CE4B2E5AE8EE7A6046E (void);
// 0x0000020C System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_BuildTarget()
extern void ResourceManagerRuntimeData_get_BuildTarget_m700564BBBBEE5DAE2784A7003B94BEF0F4E0E576 (void);
// 0x0000020D System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_BuildTarget(System.String)
extern void ResourceManagerRuntimeData_set_BuildTarget_m223B97D38F0ABF30350CC0D97AC1309C52B90D55 (void);
// 0x0000020E System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_SettingsHash()
extern void ResourceManagerRuntimeData_get_SettingsHash_m4C901062D59D872BFA86421F17B776DD80E6510B (void);
// 0x0000020F System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_SettingsHash(System.String)
extern void ResourceManagerRuntimeData_set_SettingsHash_mFE24A6F9127F319C958B42302CB8533711C67963 (void);
// 0x00000210 System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_CatalogLocations()
extern void ResourceManagerRuntimeData_get_CatalogLocations_mA1502016990897BC29281693E311DA7DDCD04F3D (void);
// 0x00000211 System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_ProfileEvents()
extern void ResourceManagerRuntimeData_get_ProfileEvents_m79993B4A71501B47BF3E582BE71379A01D553D56 (void);
// 0x00000212 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_ProfileEvents(System.Boolean)
extern void ResourceManagerRuntimeData_set_ProfileEvents_m2864B9E3A67779664D86D4BCDC3D0D7D03A2FEA5 (void);
// 0x00000213 System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_LogResourceManagerExceptions()
extern void ResourceManagerRuntimeData_get_LogResourceManagerExceptions_m248B241CAE48E87A91C751C872E66F670F1CFC96 (void);
// 0x00000214 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_LogResourceManagerExceptions(System.Boolean)
extern void ResourceManagerRuntimeData_set_LogResourceManagerExceptions_mD3D941287A8566DC7D22D44F644607EA2523BD99 (void);
// 0x00000215 System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_InitializationObjects()
extern void ResourceManagerRuntimeData_get_InitializationObjects_m66450D298C2EA3FC6937F1CDCA3FD45D1EC6419C (void);
// 0x00000216 System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_DisableCatalogUpdateOnStartup()
extern void ResourceManagerRuntimeData_get_DisableCatalogUpdateOnStartup_m1C644ADE8967D7C6B084C9A7B0B093C089E153C0 (void);
// 0x00000217 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_DisableCatalogUpdateOnStartup(System.Boolean)
extern void ResourceManagerRuntimeData_set_DisableCatalogUpdateOnStartup_mBF675BA49554986D73793A129FABB219A810904D (void);
// 0x00000218 System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_IsLocalCatalogInBundle()
extern void ResourceManagerRuntimeData_get_IsLocalCatalogInBundle_m8E4E402623F2AA36C604F3612BB19F2AF70C36C4 (void);
// 0x00000219 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_IsLocalCatalogInBundle(System.Boolean)
extern void ResourceManagerRuntimeData_set_IsLocalCatalogInBundle_m365491D5CDD96252F9F694DD4334160B6B096CE5 (void);
// 0x0000021A System.Type UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_CertificateHandlerType()
extern void ResourceManagerRuntimeData_get_CertificateHandlerType_m197BBAAAA1560C291E4E3104D4E368AC292EAAF1 (void);
// 0x0000021B System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_CertificateHandlerType(System.Type)
extern void ResourceManagerRuntimeData_set_CertificateHandlerType_m14EA2E7F6F89B5642110A46719CCEF6EE9E14D03 (void);
// 0x0000021C System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_AddressablesVersion()
extern void ResourceManagerRuntimeData_get_AddressablesVersion_m3B27B471EA8E85668FC89FD378F5FDC666DBCE0F (void);
// 0x0000021D System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_AddressablesVersion(System.String)
extern void ResourceManagerRuntimeData_set_AddressablesVersion_m715CCA401537DD86A16E29E65F0FE5B914BAB5F4 (void);
// 0x0000021E System.Int32 UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_MaxConcurrentWebRequests()
extern void ResourceManagerRuntimeData_get_MaxConcurrentWebRequests_mAFA6F5BF14AB593C0B9E86296CA60B1F7CB98B20 (void);
// 0x0000021F System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::set_MaxConcurrentWebRequests(System.Int32)
extern void ResourceManagerRuntimeData_set_MaxConcurrentWebRequests_m5E25EAB615AEEC0C14A2975321F2C197B7E05449 (void);
// 0x00000220 System.Void UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::.ctor()
extern void ResourceManagerRuntimeData__ctor_m9A7AE4DAC2751378AC656DA5A60ACC16230C3CD1 (void);
static Il2CppMethodPointer s_methodPointers[544] = 
{
	PackedPlayModeBuildLogs_get_RuntimeBuildLogs_mFC37B4B555D8F01D81B994F1BDCFA2EBA5029725,
	PackedPlayModeBuildLogs_set_RuntimeBuildLogs_mA67ED4CF1CDF896CDC50BBC7E6773D64B6680DB6,
	PackedPlayModeBuildLogs__ctor_m67AD0142BD6DC97F8CCD1C6E86A98CC95D35288E,
	RuntimeBuildLog__ctor_m88375AA8CAFE39FD77175C963991221231AAE038,
	AssetReferenceUIRestriction_ValidateAsset_mD920DB6EAA0CB521F83F4BCDDBA24817BD983624,
	AssetReferenceUIRestriction_ValidateAsset_m5703AC82584DFFA2651A6BFE3E1D1456FE7799C4,
	AssetReferenceUIRestriction__ctor_m66132256B7F090205A884805AF37E18ACBA905B9,
	AssetReferenceUILabelRestriction__ctor_m5C11CFCCA34855EF6B79A42D1DB94B6465C5EF10,
	AssetReferenceUILabelRestriction_ValidateAsset_m0F0986CED44CE89DC1A6030BC5DA515440329E6E,
	AssetReferenceUILabelRestriction_ValidateAsset_m7FEA8893F130FF8EA21721DCDFD2B754D9638751,
	AssetReferenceUILabelRestriction_ToString_m610DD1BF67CD7978120DE81D61CBC05EFAF6FA68,
	InitalizationObjectsOperation_Init_m52566D080315AD808CFDFA8DBB44C37451550234,
	InitalizationObjectsOperation_get_DebugName_m0BB1AD62757F2E6F1784ECEF82D0A3B4031EDBD5,
	InitalizationObjectsOperation_LogRuntimeWarnings_m4FA0B605859EFF68C1730534149964CA532B47F4,
	InitalizationObjectsOperation_Execute_m9936831C2834EB8F0549D62194578368C376F85F,
	InitalizationObjectsOperation__ctor_mEBF54CDAB8D99111796BAA542124CB6C91452123,
	InitalizationObjectsOperation_U3CExecuteU3Eb__7_0_mDDBD37CA57E5F64767F1A289DA18292ACF0F939C,
	InvalidKeyException_get_Key_m1E827CD10FE8F9121523C55323BFB74E317FB8C3,
	InvalidKeyException_set_Key_m2DFB687B239948C7109FE19A7A3929829E474D07,
	InvalidKeyException_get_Type_m5925DED206520BC63D995CE46BB8E94B36783D9F,
	InvalidKeyException_set_Type_m7B2B3DA88C6503E74497F859DFFC419B0407ED9C,
	InvalidKeyException__ctor_m2C677DE6F5C9F5147694794B724B73984E389BAC,
	InvalidKeyException__ctor_mBF47DDCC7F5E32F0AE59484A9AD3E9E67837A3D3,
	InvalidKeyException__ctor_mBF7BF36DF3B90DF445FF838B8E7A313F6805E50A,
	InvalidKeyException__ctor_m6234249EF148914FE104EC6410D84189D1297612,
	InvalidKeyException__ctor_m164FFF9386B875FB19AE242E431A8121F8424D00,
	InvalidKeyException__ctor_mD6FF0F2CBFB4CB9FE246A054DE267BC49815C86E,
	InvalidKeyException_get_Message_m33BE20761F0CDD13983BA12B6B78F1458DDD5593,
	Addressables_get_m_Addressables_m4B5B344E036E113B2320ED1C55C9BC30DFE5DF03,
	Addressables_get_ResourceManager_m559415A56917C68B7B442D4CDBBC7D7793380CCA,
	Addressables_get_Instance_m79F2DCCE86CBD9DC0A7664684D60BFE110E2C4CB,
	Addressables_get_InstanceProvider_m596D5E60D0D76D85163E04F5E09D91A9CEACF85F,
	Addressables_ResolveInternalId_m9CA9B4DB03E1FB4336119C24F7ACEE4D689FBF73,
	Addressables_get_InternalIdTransformFunc_m086E306F157AD6C14CDC1DACD7C926E814CAE84F,
	Addressables_set_InternalIdTransformFunc_m49DF9488BEAAC7F8F4F6AE26BCFADC411DE22B5B,
	Addressables_get_StreamingAssetsSubFolder_m7C00B95B7D348FD20C32854C15511E0D6938031B,
	Addressables_get_BuildPath_m342677BBCB1F3D60F97124F42969C024AF34E373,
	Addressables_get_PlayerBuildDataPath_m11B91766AA948854507DA3F25F35ABC2FABE26CB,
	Addressables_get_RuntimePath_mF43F0428A035E595492659C7BBBCEE12A1AB4EC9,
	Addressables_get_ResourceLocators_m8366B40D38FC8C4DC67F0C27718AC0AF98DF4BC4,
	Addressables_InternalSafeSerializationLog_mBDD38AF01A76BE7997AD1CE7552B10D6892FD482,
	Addressables_InternalSafeSerializationLogFormat_m94E66F0F85E2611139034B4228ABB240B1F97062,
	Addressables_Log_mAB235422F98E3AE80C43106A1DD5C32A65EE96D5,
	Addressables_LogFormat_m2B7DD20142AFE6172A35A0492B369B3B438E6A44,
	Addressables_LogWarning_m04F34DFAEED10EC01D34840BACCA3EC826F6296B,
	Addressables_LogWarningFormat_mFBF7F6170CB35FC36604D2AC9E2ADA30493994DF,
	Addressables_LogError_mC09A67786B75C90DC3260646B3B67FB4A79AE712,
	Addressables_LogException_mFCD7800E34C948276DF5555F6A26CA2F56AB046A,
	Addressables_LogErrorFormat_m4032E8F90EA251E3D3125E891EAC9034084D7158,
	Addressables_Initialize_mCC26D4A61B062FB38D22A1D52693E701F76FCE5C,
	Addressables_InitializeAsync_mF5F1D985415748D2FBB8B6A7968172231D28EB4B,
	Addressables_LoadContentCatalog_m40B518F060D33E36B587B1DE0881A3AD4A587E8F,
	Addressables_LoadContentCatalogAsync_m823DD22FA0B1BC2756333E60648BAC4302539E5C,
	Addressables_LoadContentCatalogAsync_mB985959DEF1FA1885462E1CF71F7F9F772C80128,
	Addressables_get_InitializationOperation_mEFB504954577C24D51998F7B1E09FCE4211BD114,
	NULL,
	NULL,
	NULL,
	NULL,
	Addressables_LoadResourceLocations_m4F23AE2D71F14D498D4E938B70E5166B67B8CCE8,
	Addressables_LoadResourceLocationsAsync_m167D968E945FAB7D8608AB25FA77F4CB4AF02B72,
	Addressables_LoadResourceLocationsAsync_m988E10578219A4DA61CAFEFF1C80923E2ACAA69E,
	Addressables_LoadResourceLocations_mA1218B08662D2327E6E6F613D8DF8153330290DB,
	Addressables_LoadResourceLocationsAsync_m5EE5F1D2A4F6A6E91E0BFDD5BE57B02DA645C64D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Addressables_Release_m95B8EDD8FDFE027D6D9DE975FB74118A9C2EE0AD,
	Addressables_ReleaseInstance_m2B0F5ED8BC8D25EBCDE33235922FF9C848B84A00,
	Addressables_ReleaseInstance_mAC8E6467B5682E089AFA799C97EC8B62F342FB57,
	Addressables_ReleaseInstance_m1DAD05443A51FC063DB44BF6A229A76EA8626BC1,
	Addressables_GetDownloadSize_mCEE3C8647DEB61B6018E05D5F6BF9FB6863478C4,
	Addressables_GetDownloadSizeAsync_m18791D84E25476A7831A3F9F8FF0CB6F9589E79B,
	Addressables_GetDownloadSizeAsync_mC7A9C8312AEA48EB740B199332980F4C145A05A9,
	Addressables_GetDownloadSizeAsync_mE6A41E54B87DB8F14A36E0CFE8E61E86A31B2746,
	Addressables_GetDownloadSizeAsync_mC6D17C32745ED075C6CFE5BE1FF207838EBCEE13,
	Addressables_DownloadDependencies_mA0CFE31F8C239215BB9076054942BDDE346208FB,
	Addressables_DownloadDependenciesAsync_m4A68D9E8BD7BBB31654BE831E8C36DD8107B4D39,
	Addressables_DownloadDependenciesAsync_mD24CD3D5D47EEF5169FAA59D3F522E1E3B794AA5,
	Addressables_DownloadDependenciesAsync_m44208F2E4FD30FC9BCA6EF3EBD52830973624F64,
	Addressables_DownloadDependenciesAsync_m0FDFC253AFA3BADED90B7B1D181A25B260942823,
	Addressables_ClearDependencyCacheAsync_m9B071B3672AD19314118690C7E6F750FBA074A39,
	Addressables_ClearDependencyCacheAsync_mE5B52A7D5BEFF0DCE9AC6F3CE3EDD7BEEDC91EE0,
	Addressables_ClearDependencyCacheAsync_mE9F398C1526A1BAD1C605E71094B6E6D38FF66FF,
	Addressables_ClearDependencyCacheAsync_m78B63E2087AE06479BABED465CEE03BC4ADAA04E,
	Addressables_ClearDependencyCacheAsync_mDF6D31502222A4F44C90C74616B71BEE089850A6,
	Addressables_ClearDependencyCacheAsync_m4CB974BBB18D2B1DC77EB9E7FAA4325311775B18,
	Addressables_ClearDependencyCacheAsync_m972F65BF2ECACC6943626939835133DE2B72B57F,
	Addressables_ClearDependencyCacheAsync_m272EFC570CC0870D38005E977AEC51B3764E754F,
	Addressables_ClearDependencyCacheAsync_mC0587D5C3AB91DC6347652C01FE1DD117739E0BC,
	Addressables_ClearDependencyCacheAsync_m1D2D4FCE1F33EAF638559D3D88E30C933505C895,
	Addressables_Instantiate_m38F3A3466690CEC825E332AE18253A747548B2C6,
	Addressables_Instantiate_mCBB986921A7C47C43F5714F536C1D1391D1DA55B,
	Addressables_Instantiate_m8FB224FEE45D90CBE659A111642AEEAC01B84345,
	Addressables_Instantiate_m10EFD35BA331D9429E1D8971990A6FD982C66790,
	Addressables_Instantiate_mB059AF3EC4DC874A04E3A57982A1279CD88CB247,
	Addressables_Instantiate_m9F22F2DA820853C9721CBD2833582AA39C5649E3,
	Addressables_InstantiateAsync_m3E95E3A1BC501C77305391BB8035C4C5C76428A5,
	Addressables_InstantiateAsync_mCE7A7244B6C59CCE618ADCCB85DB6A282D536924,
	Addressables_InstantiateAsync_m90F4701421C9E1FBE2B6568794BFD94D207F34C8,
	Addressables_InstantiateAsync_mC9BD2F5EE1D01BA0D9B6D96D64282E781B800E60,
	Addressables_InstantiateAsync_m723CB32260997B45B177B590803483ED4088054A,
	Addressables_InstantiateAsync_m08CB7D015F2A0848BF35698C798C5FAFA7252B51,
	Addressables_LoadScene_m42FBDD40F07302289F125FDDED1147F8C701115E,
	Addressables_LoadScene_m4984981A1B86802A49A78EB59C4130C30B0A47EE,
	Addressables_LoadSceneAsync_m35D5003C7E5D2FB53721FA4A0E69111E4AA475D5,
	Addressables_LoadSceneAsync_m20E4FC964C7FF2E0DA55DAD0EFA2F3369179A7F5,
	Addressables_UnloadScene_m0179B0B6AEC1BC991DF71EBD15DFE0D0520E1FAD,
	Addressables_UnloadScene_mFC5B53937A992A13AE0756B52AA662538B32D479,
	Addressables_UnloadScene_m36D212716A1F7B063B641F38DF62E913DE267554,
	Addressables_UnloadSceneAsync_m82B29BAEC5AC73EEA98775C17C59A7B78E77A2FB,
	Addressables_UnloadSceneAsync_m6285B0374E4437B9488BB967DF83AC4EEFFCEB06,
	Addressables_UnloadSceneAsync_m77BBD190C06DF29ADF371BBAB8A74A1AB86FE6C8,
	Addressables_CheckForCatalogUpdates_mDC8FC8CDCFDBBF65815D9D753A55A54E122025F3,
	Addressables_UpdateCatalogs_m1276D61515680CEDD07F816D68C70E59355BCFBE,
	Addressables_AddResourceLocator_m2EC9EE727EBC05B0F6AD2220297518D7D86BA293,
	Addressables_RemoveResourceLocator_m528825E9A799673C6DCD692977556D73F310EB80,
	Addressables_ClearResourceLocators_m17063021B75F324686A0B5DB018670D6FA300480,
	Addressables__cctor_mAD235DF28DD797B1194041717B18F6252FE35138,
	AddressablesImpl_get_InstanceProvider_m60B8111043831A5C192F0E2E793C464437F8AF05,
	AddressablesImpl_set_InstanceProvider_m7CF3BD592A3B0F2BFD48334BAFCA7EF33ABA1A56,
	AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF,
	AddressablesImpl_get_SceneOperationCount_m3E682904BF0072F1F9230E2E422B99941DE840A9,
	AddressablesImpl_get_TrackedHandleCount_m85F986C30B8B04621F1D6FB92321F01983808F04,
	AddressablesImpl__ctor_mFB97DC608C727A38F66BE2A6DA8FC808F44255EE,
	AddressablesImpl_ReleaseSceneManagerOperation_m822C1DCE0B11759298D8BD726819B856586B62BD,
	AddressablesImpl_get_InternalIdTransformFunc_m12919FE9A72016A569E96FA767BBA217E3FB07B1,
	AddressablesImpl_set_InternalIdTransformFunc_m12AF3ED9B26345126CDEFB7BCCA747DC5DD13973,
	AddressablesImpl_get_ChainOperation_mC1FCCFDA2FC6033081B38EDB8BC4FE11298E2FA6,
	AddressablesImpl_get_ShouldChainRequest_m394E11B8082EFA65D02B6A38001DC6EA5E122B2B,
	AddressablesImpl_OnSceneUnloaded_mF08FCC9FC559EAD70688D3F7EF63E38FC2C7DF88,
	AddressablesImpl_get_StreamingAssetsSubFolder_mB50C0351EB64D4B92DC719D6AB6072ACA86E8BB9,
	AddressablesImpl_get_BuildPath_mD149F3594AD078E5D674C0AA0848F7328A63C9D7,
	AddressablesImpl_get_PlayerBuildDataPath_m4F535DC68E6773D2854E6945F8D318E1183BEF68,
	AddressablesImpl_get_RuntimePath_m708E3330D275A0B6757B7ACD331F8B144C4186D4,
	AddressablesImpl_Log_mA095E88894C5379A25E4C82E22AAA1B9E9AD4BCE,
	AddressablesImpl_LogFormat_m4565DCEA2F86CF81839D8B341D3FA5D7169818F7,
	AddressablesImpl_LogWarning_m2B5474CC29323911DF941FF0074CF749A5E1795B,
	AddressablesImpl_LogWarningFormat_m0EFB836035CE1A546271053410B37209E33FD6BD,
	AddressablesImpl_LogError_m97D4A1E15A5E702D10738E4EC683C376CB72844C,
	AddressablesImpl_LogException_m0499403B8FA853BB874A4ED46C423BC638191F4E,
	AddressablesImpl_LogErrorFormat_m948EDC09AF660C88E82BA2369001C21A92E44C15,
	AddressablesImpl_ResolveInternalId_mA46547D161E2499742CD72810D596DFEA1ED4D77,
	AddressablesImpl_get_ResourceLocators_m8069917B2A391A881797D8F6BFCEC2243515A4C5,
	AddressablesImpl_AddResourceLocator_mE2F2A1011D79783D5D94A1F1AE953321C120CABC,
	AddressablesImpl_RemoveResourceLocator_mD45770B3A83ADFA2AB555FB31A9D7D61879C7CBF,
	AddressablesImpl_ClearResourceLocators_mF82ADF623A74B6B751A3AD1791FA4CA7D06B5AC8,
	AddressablesImpl_GetResourceLocations_m1CA1288F2C67A631AE464BAAD55F6007B5D102AE,
	AddressablesImpl_GetResourceLocations_m945F0FD282F02D92CA593CDCF2F24FA8BAFEE5B9,
	AddressablesImpl_InitializeAsync_m11D3E10EC93C8F0F417EA23EF7D58CFF78BBAD87,
	AddressablesImpl_InitializeAsync_m4A4F934AF1E0BD4A41E309E55BF6EFBE6CED9E2C,
	AddressablesImpl_CreateCatalogLocationWithHashDependencies_m0A9E7AA50E452C39C6C780A1D64EE7D5B3B911E5,
	AddressablesImpl_LoadContentCatalogAsync_m8265B5A15EE5743FA30BD942BBED95066E0B131B,
	AddressablesImpl_TrackHandle_m0EB206D85412C5EC04C3E119DCFA2C357C09B504,
	NULL,
	AddressablesImpl_TrackHandle_m87874D354073A48745A80A9857A523BF9C0E0560,
	AddressablesImpl_ClearTrackHandles_m315C8135F0E63956226A2580CCE39283B31E42E5,
	NULL,
	NULL,
	NULL,
	AddressablesImpl_LoadResourceLocationsWithChain_mD6BE697B071B0E474E53830A901C768EB735D379,
	AddressablesImpl_LoadResourceLocationsAsync_mA686A69DC2D3A4DDEA6221F43376FC800EBCFCB3,
	AddressablesImpl_LoadResourceLocationsWithChain_mEB368DB6C3986A7BF1F229F787CC25B19A4BC678,
	AddressablesImpl_LoadResourceLocationsAsync_mB2FA2FF787A72F08D4DDA0BAAD287AD53C6B6CCB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AddressablesImpl_OnHandleDestroyed_mD1203A8A5BFA64AA3406C7FC2EBCF4AF1E474475,
	AddressablesImpl_OnSceneHandleCompleted_mC09A18EB2F08488681D7BC7D70CDA308076C14A7,
	AddressablesImpl_OnHandleCompleted_m810CE6BD36FAC94433B0319ADE294772EF24FBE5,
	NULL,
	NULL,
	AddressablesImpl_Release_m71C27A8288DB3BF05910279F11CDD69404692D75,
	AddressablesImpl_GetDownloadSizeWithChain_m1B972DA152368F3A4BF01589DB98F516E8F41ED8,
	AddressablesImpl_GetDownloadSizeWithChain_mF6EAD3FC240411A213E218883273FA269C4FEA56,
	AddressablesImpl_GetDownloadSizeAsync_mEB67C85DC464F53629760BFA18A2DADA28D80F7C,
	AddressablesImpl_GetDownloadSizeAsync_m2EABA472ED94651C65DB04B4FE9A7D7D17AD6668,
	AddressablesImpl_DownloadDependenciesAsyncWithChain_m1D6B91A163CF6BDDF4C7196BB7DC711A8D4502F5,
	AddressablesImpl_GatherDependenciesFromLocations_m01332E9D4D5B131709B0E1F7B50C043376FC58F4,
	AddressablesImpl_DownloadDependenciesAsync_mEF8B90CAF2BEB8C10015775D391E76DD2D3FEA18,
	AddressablesImpl_DownloadDependenciesAsyncWithChain_m81B6C58EE2A51073096FD95D411A3D968D67BEEB,
	AddressablesImpl_DownloadDependenciesAsync_m8E7A915B1F80C32A4BA8179025FF06F0E4F3D936,
	AddressablesImpl_DownloadDependenciesAsyncWithChain_m064DD963263B0B0380D07679A6AA6067C5CD75FB,
	AddressablesImpl_DownloadDependenciesAsync_m6414EF4D5FC0F831981FC9AFFCC9348940D97457,
	AddressablesImpl_ClearDependencyCacheForKey_m0E986D879EF307BE0D4A466EF65DED6328034815,
	AddressablesImpl_ClearDependencyCacheAsync_mAFA6D29AF413CEBAB3EBD7391F1E43EC0E4FC80B,
	AddressablesImpl_ClearDependencyCacheAsync_mF3E0167748EA49B66A21EA5A0F45A90AAC91088A,
	AddressablesImpl_ClearDependencyCacheAsync_m84F1D5ADFB5C0BDCD7E89F4C01B76FD69863679F,
	AddressablesImpl_InstantiateAsync_m686908BECA518774981EBF6E5B13EFAA9CB71D73,
	AddressablesImpl_InstantiateAsync_m2093DEB4C20FB8A9EDAC7E3F9B0E5F51957D04B0,
	AddressablesImpl_InstantiateAsync_mEC48526760B336BBF3DD968EF0C88A4FB37ED100,
	AddressablesImpl_InstantiateAsync_m38C438BEE7E7F2B9B913A6BC27C22E7363FD73CA,
	AddressablesImpl_InstantiateWithChain_m97786DAC24258ACC5E42543D8E520BF2492EFA41,
	AddressablesImpl_InstantiateAsync_m21DB723FD9048B8D1E1039C457FB407DE2F0E357,
	AddressablesImpl_InstantiateWithChain_m0884A4716451E99D465F4ACF941A5EE8CBFD3FD7,
	AddressablesImpl_InstantiateAsync_mA10679617DC0E9EF2ED3325BEE4F1C6A5B3F3F94,
	AddressablesImpl_ReleaseInstance_m972660B8DBA7CF0C191F8EBE28311B4C0C22319C,
	AddressablesImpl_LoadSceneWithChain_m5A2178CC3FC09CF8C9BE9B1F3704278F5A71AFFB,
	AddressablesImpl_LoadSceneAsync_m5D6A565C43447859C4F2946EB573421997CDE32E,
	AddressablesImpl_LoadSceneAsync_m9C39609CD09828421355A2D9E767BB38235B1665,
	AddressablesImpl_UnloadSceneAsync_m36D752A57D4E88D29B2CB2D9613CDE5C8ED8FB28,
	AddressablesImpl_UnloadSceneAsync_m85DABF4E58EE6ACFA26B1DF63A315951609EAF20,
	AddressablesImpl_UnloadSceneAsync_m6CCDCF5F164CC2D71D8E82B81C114ED37F5F6DA6,
	AddressablesImpl_CreateUnloadSceneWithChain_m968F189581465B4EAF5A26F68652E4462C559554,
	AddressablesImpl_CreateUnloadSceneWithChain_m524E0316B96655A3F6235A1B658D1B8A5622B823,
	AddressablesImpl_InternalUnloadScene_m50518F7D3C6E69F07DAA545D6862EB89C5EA9714,
	AddressablesImpl_EvaluateKey_mC24716AC53F37CA42CDA773EA7B9FBA88A364314,
	AddressablesImpl_CheckForCatalogUpdates_m38DC71A358BB49922778118BBD146612E5FAA15E,
	AddressablesImpl_GetLocatorInfo_m1DEA14E46D54B4C09FE8FBF8AFACC78162949FEB,
	AddressablesImpl_get_CatalogsWithAvailableUpdates_m19302FD4FAFD70B8AD82A2343CCD0057EB7005F3,
	AddressablesImpl_UpdateCatalogs_mF7C9B9975D87E1BC2834DD876D95E3788FDD21A2,
	AddressablesImpl_Equals_mCADA4AC0A1F2ED550ACC39DC8A5F1E214AB80CEB,
	AddressablesImpl_GetHashCode_m03A282AEA329A55DBFBD4F3BFBD506D832ADCC01,
	AddressablesImpl_U3CInitializeAsyncU3Eb__57_0_m26FC116FE12B1C257AA9C1FDB4887A4063697F31,
	AddressablesImpl_U3CTrackHandleU3Eb__61_0_m5551E5AB9746D40C6F672ED2CBAF9E406E23BE77,
	AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__91_0_mB51027D69506A97CCCA45286AC90CBE95AD63BBD,
	AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__91_1_m94862209DDB1CF58A6412E71B931FE18D1414207,
	AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__93_0_m1124D80525DEB05AB5EE561A90BEAB43901EA3DA,
	AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__95_0_m639480CBE7F6C2F4DD335FB73623F575074F775D,
	AddressablesImpl_U3CDownloadDependenciesAsyncU3Eb__95_1_mE4CF7E0CC979594BF8D5A139E5E4C0B9548E1776,
	AddressablesImpl_U3CInternalUnloadSceneU3Eb__117_0_mE8A86C2E2CA77B10A74F3BC325279CF5434CC1F8,
	AddressablesImpl_U3CCheckForCatalogUpdatesU3Eb__119_0_m195B08AE7FC5EF4594288EB4F896883A103414AB,
	ResourceLocatorInfo_get_Locator_m730E272473D7C040F4F841E8551408D7CD7972BC,
	ResourceLocatorInfo_set_Locator_mE580D784A4F673DDDB4294D954F3318962C22F7B,
	ResourceLocatorInfo_get_LocalHash_mE42295051FB10834E554BA358EF27797DEF02FA6,
	ResourceLocatorInfo_set_LocalHash_mBE8A94DBDA82AFA8664B367A8AB5C075260AD851,
	ResourceLocatorInfo_get_CatalogLocation_mD0A97F02CAD8D1108EE0DD40C801EC3146EE8CE9,
	ResourceLocatorInfo_set_CatalogLocation_m03220A441E317EA295005B990404BDE74EAA9B44,
	ResourceLocatorInfo_get_ContentUpdateAvailable_m3DFA6F817B37BB2C44EFD73F343B32C7FB239C7D,
	ResourceLocatorInfo_set_ContentUpdateAvailable_mD5DB2475409DC347D899089950F14953CB2C8B34,
	ResourceLocatorInfo__ctor_m771FFCECED08BC5F23E420C74B5AAC65971B9EAB,
	ResourceLocatorInfo_get_HashLocation_m06E961C27F73FFF9147F8A2E683BBDED55111B10,
	ResourceLocatorInfo_get_CanUpdateContent_mB3AFEF8C7E62E0F4043B68CD8641D8056B5B3991,
	ResourceLocatorInfo_UpdateContent_m12E2AE38A5B71B3EC19D3B20847DCDBB86B03167,
	LoadResourceLocationKeyOp_get_DebugName_m0FE13637334C8510C330B99DF4DA803723B2C903,
	LoadResourceLocationKeyOp_Init_mAAEE48C857897F6D6055E520BFF1D39D89EC72CC,
	LoadResourceLocationKeyOp_Execute_m9DC1F3186E5476DC77A1229E304FA6AA01DF4D5F,
	LoadResourceLocationKeyOp__ctor_mC4FC39260004C27AD6697FF8C79392617F6A87DC,
	LoadResourceLocationKeysOp_get_DebugName_m5F40DAD3471E05B60D117EF3CFBD59B663BFF23D,
	LoadResourceLocationKeysOp_Init_m7B85866D25E6F935F469136DE5EEF45010A039E3,
	LoadResourceLocationKeysOp_Execute_mA58F78F7D010586D72640A82798AB736FB9B7BF5,
	LoadResourceLocationKeysOp__ctor_mE892F37F59EE3746012541B7B69928477F770313,
	U3CU3Ec__DisplayClass33_0__ctor_mAC90BA4B3DD4EC74A8AD2C293A31AB8C1288B289,
	U3CU3Ec__DisplayClass33_0_U3COnSceneUnloadedU3Eb__0_m7FF18F1F05E2417815B0C44CC705937F52ABE452,
	U3CU3Ec__cctor_mE4D426958DC06AA4379154B639D4A5AD8E737EFB,
	U3CU3Ec__ctor_mF80895DD032961F149235217FCD848988CE11F9C,
	U3CU3Ec_U3Cget_ResourceLocatorsU3Eb__51_0_mBBC7E4C00C5B24BD50F4E4C8D5476E696F6814FC,
	U3CU3Ec_U3Cget_CatalogsWithAvailableUpdatesU3Eb__122_0_mB2DA8D81C64536BDF6985D7CC0E3CC984667ABDB,
	U3CU3Ec_U3Cget_CatalogsWithAvailableUpdatesU3Eb__122_1_m6B6239FB63EC8387B9142BFFA0B54FC95BE4DE9B,
	U3CU3Ec__DisplayClass53_0__ctor_mEEF2D488E3840C320349B1637D7949BDC46E6C16,
	U3CU3Ec__DisplayClass53_0_U3CRemoveResourceLocatorU3Eb__0_m276C46AFC10A31F94DC0CC2F2DDE9AD9E209BFDD,
	U3CU3Ec__DisplayClass60_0__ctor_m1A9D8A8D34D2F41BD1EB8E7FDC1196FD3BCA7051,
	U3CU3Ec__DisplayClass60_0_U3CLoadContentCatalogAsyncU3Eb__0_m28C5F432D5D23E56E5713B4BF806C8008F7CF0B7,
	U3CU3Ec__DisplayClass60_0_U3CLoadContentCatalogAsyncU3Eb__1_m1B0A1DAAD02E67D79E7CE9F39DA6A8B13835FEB5,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass70_0__ctor_mAA76CEE7F2EB371FB268C00868EEA1149B183844,
	U3CU3Ec__DisplayClass70_0_U3CLoadResourceLocationsWithChainU3Eb__0_m8301D18E79C1115DF44E90E69C20DBC51B5EF4D4,
	U3CU3Ec__DisplayClass72_0__ctor_m22FF4B277F2C39CAE3441AA4394309D6C03890D6,
	U3CU3Ec__DisplayClass72_0_U3CLoadResourceLocationsWithChainU3Eb__0_mE29B861ADAA7EF7D2B0D3EB16BAE65ACD8AFD3B9,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass85_0__ctor_m6C493BD183735DFD8F187810C30A3C35D70ABC24,
	U3CU3Ec__DisplayClass85_0_U3CGetDownloadSizeWithChainU3Eb__0_mCCB904A48B29CF6D927A5E4070D2F10B59939ED9,
	U3CU3Ec__DisplayClass86_0__ctor_mAAAAF10FB90900E31FE9A79E69731F491E635137,
	U3CU3Ec__DisplayClass86_0_U3CGetDownloadSizeWithChainU3Eb__0_mCB5A4ABC8FEC8FDE68C54A8F3ECDE0A73B48CAE6,
	U3CU3Ec__DisplayClass89_0__ctor_mF5D6A8604A5B04D424202916A0F6DCCC4026CEB3,
	U3CU3Ec__DisplayClass89_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_m379801DD72BAE2FF2BB383BC6001CCFF840D6C7F,
	U3CU3Ec__DisplayClass89_0_U3CDownloadDependenciesAsyncWithChainU3Eb__1_m30D531506E5B0911F6D5F9493E79ADF7E7473AB1,
	U3CU3Ec__DisplayClass92_0__ctor_m87E39DFE717183553547BE00A74C4E75F0B1C5D3,
	U3CU3Ec__DisplayClass92_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_m3C508242DFA3C5F9E33D1F22BE38F6017DC595B7,
	U3CU3Ec__DisplayClass92_0_U3CDownloadDependenciesAsyncWithChainU3Eb__1_mF939C952742E569E1BB45CEE1B94E520C9C33CF9,
	U3CU3Ec__DisplayClass94_0__ctor_m3A5E4400DEB224FA1F67EAE4D99687910F5F552F,
	U3CU3Ec__DisplayClass94_0_U3CDownloadDependenciesAsyncWithChainU3Eb__0_mC4366D6C7E90D135017029573B24F977DA2BE921,
	U3CU3Ec__DisplayClass94_0_U3CDownloadDependenciesAsyncWithChainU3Eb__1_m7FAC15C4293A43668DF7698AF7B1BD7EC352379D,
	U3CU3Ec__DisplayClass97_0__ctor_m32ADA65CFAEDBB2E8B48E0F31FC4042D481A930E,
	U3CU3Ec__DisplayClass97_0_U3CClearDependencyCacheAsyncU3Eb__0_mC4DDDF0AF4EC0F28DD0BD09DC610EF31D4C5EC22,
	U3CU3Ec__DisplayClass98_0__ctor_m172896A28667FCC4A09F93487DA41B2F2C5ADAF7,
	U3CU3Ec__DisplayClass98_0_U3CClearDependencyCacheAsyncU3Eb__0_m555A4E75D9EE591E16768FFABABEB3A170FA862B,
	U3CU3Ec__DisplayClass99_0__ctor_mFEBE0247BA067F0D735346050F3E12B5E34E8961,
	U3CU3Ec__DisplayClass99_0_U3CClearDependencyCacheAsyncU3Eb__0_m3FBE206E8F99096801FFB71D72BB95360A57ED8D,
	U3CU3Ec__DisplayClass104_0__ctor_m52D0EF2F2A23858ECB5248941D1D7DD9A32354D8,
	U3CU3Ec__DisplayClass104_0_U3CInstantiateWithChainU3Eb__0_mC6C1C332F5E7626A6960764AC94483677448018D,
	U3CU3Ec__DisplayClass106_0__ctor_m14F475158A764AAA956F68944993B1B46F0BB097,
	U3CU3Ec__DisplayClass106_0_U3CInstantiateWithChainU3Eb__0_m0E209C9091E1142E757F3E1782EBCFB955CA781B,
	U3CU3Ec__DisplayClass109_0__ctor_m7239ED44514B79B2D019D10AFA76A8E7E42A2D21,
	U3CU3Ec__DisplayClass109_0_U3CLoadSceneWithChainU3Eb__0_m87230ADE3583440F51FE7BE4809CC67C16068BB7,
	U3CU3Ec__DisplayClass115_0__ctor_m06B200FEA4A019D4D9FBE76F0BAE0C00CF6FA1CA,
	U3CU3Ec__DisplayClass115_0_U3CCreateUnloadSceneWithChainU3Eb__0_m9D73EA0D587AA8141168413581BA0A5A98D73055,
	U3CU3Ec__DisplayClass116_0__ctor_m608071B61C8B59CF8484E46BCB39366C5B4B1F02,
	U3CU3Ec__DisplayClass116_0_U3CCreateUnloadSceneWithChainU3Eb__0_mCB75653E581B2AB7A6F1013ABF742CD5046A4604,
	U3CU3Ec__DisplayClass123_0__ctor_m0CAC1B0511D449C5963E1A18E7CE6523237A7DD0,
	U3CU3Ec__DisplayClass123_0_U3CUpdateCatalogsU3Eb__0_mB0E9D34ACEBDBAAD2C2D58569BC45EC05D744589,
	U3CU3Ec__DisplayClass123_0_U3CUpdateCatalogsU3Eb__1_m73E2B35B4C2B6F9543F693CB5F86F0D36AF89A7F,
	AssetLabelReference_get_labelString_mD59B72403465A4860CCF78FB546FE69CFFFC5622,
	AssetLabelReference_set_labelString_m882B1996DCA9934C1FB2B5DA165B6F20B536EDEE,
	AssetLabelReference_get_RuntimeKey_mA4EDB42A283468905F1818D537491032E154BA7E,
	AssetLabelReference_RuntimeKeyIsValid_m4BFF940547119F1F6B086FE4E84328A8596E747E,
	AssetLabelReference_GetHashCode_m15FC2ADC5A186D4A6F2D4F018C8BBFE7CCB777F0,
	AssetLabelReference__ctor_mFA533405E7B5DE7AF46E044BB5BE3622BEEB54D1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AssetReferenceGameObject__ctor_m2224F8AF5D9FD66A0061B04B8D5F5FF0C54AAFA0,
	AssetReferenceTexture__ctor_mA0C17276E8543A776744C24E79B28A97DD8EE86A,
	AssetReferenceTexture2D__ctor_m6073FEF7EB30A8201C97BC93BE51C3271C45045C,
	AssetReferenceTexture3D__ctor_mE95547D963764AEB3E6E4D79035968B134B4197A,
	AssetReferenceSprite__ctor_mD6A5EE1BBEB557114F8C168813DA7FB78ACE1BC9,
	AssetReferenceSprite_ValidateAsset_m5C656AD60659BE608B5C63431BD83A132CFAB69A,
	AssetReferenceAtlasedSprite__ctor_m1761A66E91F0AD050D9DC8D5D9EFDEC62E826BB5,
	AssetReferenceAtlasedSprite_ValidateAsset_m9DB5B0519722A771CA3B82A163AE5033EE7A17B0,
	AssetReference_get_OperationHandle_mB4BAAB57C9A519BDDA6E2308BA157FF8929728AF,
	AssetReference_get_RuntimeKey_m53C038FD2A954CE6382C683D6FB72A7049D91684,
	AssetReference_get_AssetGUID_m3B9DC49BB25B4E5EEED574C33FB6D0E372B97C8B,
	AssetReference_get_SubObjectName_mB053749C44EB7B019BDE026DDC1830589BD93966,
	AssetReference_set_SubObjectName_m3263E26040C68E634FA1348E2A73B9B3D0E1762D,
	AssetReference_get_SubOjbectType_m59C79C425452536B2548FB958E96DB67BFB1EC84,
	AssetReference_IsValid_m00FA122AB61CE13B8559471526AB1D57CD3648BF,
	AssetReference_get_IsDone_m1DB636F5D907D7D792B43FA2CD477A86E55F9681,
	AssetReference__ctor_m46BDFDC071BFC1D60C817A1C5B41A16CBA1F1C66,
	AssetReference__ctor_mE6785CAB054C044B348B64250D9904D1C6A3B61E,
	AssetReference_get_Asset_mF316D0153DAB80E3593997C854E96A21B80D092C,
	AssetReference_ToString_mD0986FBA89F75545C76909CD3B7D56CC0696C1F5,
	NULL,
	NULL,
	AssetReference_LoadScene_m6AFBBDBF59A4BA4DC807CA75047119D9693C1D6F,
	AssetReference_Instantiate_mC320C78B3EB3684BE0CF806FDAF041174F77E11F,
	AssetReference_Instantiate_m1FB73D6398004633844103B443BBA8E7A423738F,
	NULL,
	AssetReference_LoadSceneAsync_mE7EBA1B3BD636157883FC6579B43C4C56D1DD7B5,
	AssetReference_UnLoadScene_m5D3FC5DD78B5533A4D10EA16436914A105C4C1F8,
	AssetReference_InstantiateAsync_m639FCE542FB03D1F21D46380FAB6D15E5F2D5AF7,
	AssetReference_InstantiateAsync_mE50F81174A8DA19F2348C760B61DA80AEE04CD93,
	AssetReference_RuntimeKeyIsValid_m3E9D167E60996FF8D81F580A80CB8BA4DAF9E4FC,
	AssetReference_ReleaseAsset_m431802AD4EF73EE220B506308D7A3EC47DC34851,
	AssetReference_ReleaseInstance_m6FB49CF792970765BF400B237C1187884052FB87,
	AssetReference_ValidateAsset_mDE6F7EF70C3C45F0D5C40D175AE29F3FB87B0A9E,
	AssetReference_ValidateAsset_m77B9DDB2DCF842315D6B1008C54A41553FDEE86A,
	NULL,
	NULL,
	CheckCatalogsOperation__ctor_m001A86CDA6AF9A0FAE3AB17B875941BB4BB372B7,
	CheckCatalogsOperation_Start_mC606668524E71A54954A02D6D8223584A6F7859C,
	CheckCatalogsOperation_Destroy_mB9F8845351DFDB4F148C18B2A8EA63B398FC5FA9,
	CheckCatalogsOperation_GetDependencies_m94B28B65B6C1EDB35C6197D98B777B65CECE8F0B,
	CheckCatalogsOperation_Execute_mEF08CD547922397D9DE1D386BEA57D3BD4C1CDCB,
	U3CU3Ec__cctor_m763929708DCF74CD18989AFEE9D2A3380E7D449F,
	U3CU3Ec__ctor_m3A8EB8F91F6762E238F130CE3378A49352E47215,
	U3CU3Ec_U3CStartU3Eb__5_0_mBD33EDAE0E282645311F1FBDB63BE37B23F7FB76,
	UpdateCatalogsOperation__ctor_mAEA17D1BDDD7BD34200E355988A3D65217272D8B,
	UpdateCatalogsOperation_Start_m6E88A765FFD8B4711796B5712C0CD31A2A100499,
	UpdateCatalogsOperation_Destroy_m1F3A40E0AF0A3F3FF8207C65523FA0591E5C65CA,
	UpdateCatalogsOperation_GetDependencies_m02994A4CDFB683FE17233F87DAEC58F294BAD6EE,
	UpdateCatalogsOperation_Execute_mB21E9B8BFB6FE2B37A2DD5F9DA31D5F6A6EF3DF6,
	U3CU3Ec__cctor_m857753E805839903F2BC7F7AEBCE16E4DAE18536,
	U3CU3Ec__ctor_m01C579E9C120F21F7E733812D6AD04C44762CE4B,
	U3CU3Ec_U3CStartU3Eb__4_0_m97D1006FD50716A4F919D9B3FF2E95FC4F9064F9,
	DynamicResourceLocator_get_LocatorId_m6377861BDFB96A3D7537138F8B3CC92C2EC44BF9,
	DynamicResourceLocator_get_Keys_mADFD7AAEE7E7B4FE7499A754B0D219AE03A0DD6F,
	DynamicResourceLocator__ctor_m3E4F35E7D895831EE76A3FEE63921169C3CC2781,
	DynamicResourceLocator_Locate_mC48F124C4425265E6E42632408DC313533723E02,
	DynamicResourceLocator_CreateDynamicLocations_m8D88AEB304D7601E15F999845CD9A7C576216F4A,
	PlatformMappingService_GetAddressablesPlatformInternal_m97FC778BFAA18E1D123FDC738A0C717531F908E2,
	PlatformMappingService_GetPlatform_m640E7D9426E0615A3DC3D5436A0CDBB6DBDDFB50,
	PlatformMappingService__ctor_m4176958626D49CDF1D0A7F163284C0785991E95F,
	PlatformMappingService__cctor_m61A0B8E6F8CD5732F4241A3B57EBAF1750565E04,
	DiagnosticInfo_CreateEvent_m7BC23DB8C1758E94200B9A5A71161BD2847C22D3,
	DiagnosticInfo__ctor_mC37E66C328AB9D0CC49A5E7CD3DA46325CCD13BE,
	ResourceManagerDiagnostics__ctor_m2A3BA0AAE462F5CA7197D640C7ABBBBAE511232D,
	ResourceManagerDiagnostics_SumDependencyNameHashCodes_m0DE26E26EBD9761F9266508CC40639A28F9AD115,
	ResourceManagerDiagnostics_CalculateHashCode_mCFCD981FBB120165EC6BFED2FD3124E50DDEEC31,
	ResourceManagerDiagnostics_OnResourceManagerDiagnosticEvent_m1743AD94BFC8DF4B39ADE60E520BD59421189E91,
	ResourceManagerDiagnostics_Dispose_m085D3FBBE22EA40BD062CF038800E20A86A00D6F,
	SerializationUtilities_ReadInt32FromByteArray_m4A998891901FC83A215146AEF87B175D128869FC,
	SerializationUtilities_WriteInt32ToByteArray_m7C3F1DF7E37CD23D76931AE5B6369A959EF8BA86,
	SerializationUtilities_ReadObjectFromByteArray_mAC53DA56CEB59215F9CA0B44D9E0B406B23AB733,
	SerializationUtilities_WriteObjectToByteList_m3C9303097352A716AA3FBCBA5BC3B5CB90C9824D,
	ContentCatalogProvider__ctor_m4A8C166F0598190FB083D72299620C0CE2A12AA6,
	ContentCatalogProvider_Release_m0F1F55F8205680BAAF6BA6BAD5D9B3808867CB67,
	ContentCatalogProvider_Provide_m342DAE0BBCE88808163642D8D0A36005B8D4FC30,
	InternalOp_Start_m32B3A0491CEA49C08B83AD65526A9FCDA63130E4,
	InternalOp_Release_m6A48F23B0BF67A54472E72A19C6B6ABF6B52D2DD,
	InternalOp_LoadCatalog_mC080EB46BDACBCEA41F2F7C668AAAB1376DCAE0B,
	InternalOp_GetTransformedInternalId_mFA529AEC6097418B89A9647A6A308B48B58D3EF8,
	InternalOp_DetermineIdToLoad_mCE7D096E09A898F73672207445CBBED926B7EE73,
	InternalOp_OnCatalogLoaded_m32E0C229A2C69C41BCF6FB5E4759F54D1A7EA7AF,
	InternalOp__ctor_m00A19525A60DBF2EF367EF517DA10C4624018CA5,
	InternalOp_U3CLoadCatalogU3Eb__7_0_mE2044A89530400AB20A702228205F845B0909D17,
	InternalOp_U3CLoadCatalogU3Eb__7_1_mF11E9BFFB660F9158273D073CAB608F514333AD8,
	BundledCatalog_add_OnLoaded_m08D1DC44179DEA7AC6533698998B35D0A2F7E843,
	BundledCatalog_remove_OnLoaded_mEFC260E4F15C57F7E7FA144AC70A522DE1047B43,
	BundledCatalog_get_OpInProgress_m28D11B2D0119E8815165C869EE6F8C05E378332B,
	BundledCatalog_get_OpIsSuccess_mE7C858B1C1E424D63C6063B27E4A7A0D2FF7D1D4,
	BundledCatalog__ctor_m9818983D8EA29EBBDFC2BC7EB0D66DC93FE85078,
	BundledCatalog_Finalize_m998C5533738BE626BB99B3FC97E9DC5573DEA0A1,
	BundledCatalog_Unload_mBBF241B2FA8ED9B62552C0A8A93098611F4E9CF1,
	BundledCatalog_LoadCatalogFromBundleAsync_m87EC9B066E4E9145FA22DE481C354F69EF197F39,
	BundledCatalog_U3CLoadCatalogFromBundleAsyncU3Eb__16_0_m2B9D73C323D8EECD0F5A8E54F7FB5C72B521C032,
	BundledCatalog_U3CLoadCatalogFromBundleAsyncU3Eb__16_1_mB79EE7829A6B850788D4D24ED7A84E362DB168AE,
	ContentCatalogDataEntry_get_InternalId_mFB37FEB717C2B5D130971F8053BA9DA25B0C4368,
	ContentCatalogDataEntry_set_InternalId_m87C09E2098D5A89572421AA1538826C73CE9BDCB,
	ContentCatalogDataEntry_get_Provider_m2E5B4395011ED9C3BA10C5288B94F6A52347B522,
	ContentCatalogDataEntry_set_Provider_m57E6C75DB0E0AFDB10242312F2ED439C764DFB32,
	ContentCatalogDataEntry_get_Keys_mBEA0556C9890073F8B56DB307FFCD277421977E7,
	ContentCatalogDataEntry_set_Keys_m15E40907903F27A494730DBC44917B2F8E58B033,
	ContentCatalogDataEntry_get_Dependencies_m5C1F0348EADF4890D1FB030613E4C1B5183A720D,
	ContentCatalogDataEntry_set_Dependencies_m3FA6E60076D1F9B9851E24458567F6BAED1B2932,
	ContentCatalogDataEntry_get_Data_mC70E2483CD068C901C5FC29D4152818AC509C5DF,
	ContentCatalogDataEntry_set_Data_mFECE94F66B67ED56BF63B1AB0030344418917675,
	ContentCatalogDataEntry_get_ResourceType_m721C512ACB304B0B75C755EEFFB8D58655138E46,
	ContentCatalogDataEntry_set_ResourceType_m51903647B9C10D88AFB570267E2BD5383D2A2B83,
	ContentCatalogDataEntry__ctor_m35D853FE4BD8A796E2A36FF9C7D481DC746C75A9,
	ContentCatalogData_get_ProviderId_mD08675BA7BD77473D1406C154BDC3243608EE17C,
	ContentCatalogData_set_ProviderId_mFEBBA31FD47C3FF8226C5BEFF0CFB0C99581CEFA,
	ContentCatalogData_get_InstanceProviderData_m1C8F75FC83CA8BCAAD687DCC3DA7A03D27C1181C,
	ContentCatalogData_set_InstanceProviderData_m82A56B7162300C82F0007E3A8A09C7E168CE07E8,
	ContentCatalogData_get_SceneProviderData_m5BC5272A27857E6603B0EA4D2901BBE3BA6CEDBA,
	ContentCatalogData_set_SceneProviderData_m4F12291B570D89F56271B2985DA37A3887D08569,
	ContentCatalogData_get_ResourceProviderData_mB27A713ABE5105A00386E830FFAA63EF2A735611,
	ContentCatalogData_set_ResourceProviderData_m35399808222F501DC47652DD8A3E22EDC9A7E230,
	ContentCatalogData_get_ProviderIds_m6EF0BF07042821BE2211AFF7D002078C76140D76,
	ContentCatalogData_get_InternalIds_mEB3D0FAE03880663E7A1552FC62B8DFC68A65F1C,
	ContentCatalogData_CleanData_mAED0C34FF6ECC590082A97E2CE9027AE3DA58541,
	ContentCatalogData_CreateCustomLocator_m99BA0D17CC77D91BC5FB307AB5F6B0CBC7A11731,
	ContentCatalogData_CreateLocator_m14684F03CDD2C36D4294456F49D6E62EE2BCDB36,
	ContentCatalogData_ExpandInternalId_mE1B7E65B5BC2F697F3CD44315E8AAB8A2D53C919,
	ContentCatalogData__ctor_m6E6EBB5EB01F469B6C1DFAA00BBE3E418746E4CD,
	CompactLocation_get_InternalId_m924673579F96D30A92FC3F6D1412B04C34B46ADE,
	CompactLocation_get_ProviderId_m8984A92DD78F5178E404801F86B4E9861126A946,
	CompactLocation_get_Dependencies_m00D2B27CCF44BEE6756E4B494C1B5ABFC0B5EE2C,
	CompactLocation_get_HasDependencies_mBAFBB58126FAAAF5DC02B20061254967BA3B7650,
	CompactLocation_get_DependencyHashCode_mC7BA696649520BEE3BE793A0E4FBFC0908174216,
	CompactLocation_get_Data_m0AAE40A976FFFFE28B454CF6FA547F2C6C81343A,
	CompactLocation_get_PrimaryKey_m7619D46570D27544D9ADFFD67C71B76AF79FC0FD,
	CompactLocation_set_PrimaryKey_mAA96AF8389B0DD1BE342795386F4DBE8436C6363,
	CompactLocation_get_ResourceType_m3B3467CC3D01D5B81B75F56C2114818959B92B95,
	CompactLocation_ToString_m9C014084C5019F97F65E5D091BDB2AC991E962E3,
	CompactLocation_Hash_m8202FDA5FAFFB40D562CE01C8D6DEB51906F1401,
	CompactLocation__ctor_mECD42CDE7A9468E1205608E1DED10726E42094E1,
	NULL,
	NULL,
	NULL,
	LegacyResourcesLocator_Locate_mC0DE4EA48FD0727C27881E9505ED9042860012DA,
	LegacyResourcesLocator_get_Keys_m7EAECDF4CF0FA60E95E02590252A80BA8654411E,
	LegacyResourcesLocator_get_LocatorId_m9FF301DE2F149DBE70FA215322AE551F6787FC62,
	LegacyResourcesLocator__ctor_m57A60CBED17827A53BBDAC21801238BB7E524AB3,
	ResourceLocationData_get_Keys_m95E3FE87FAD4116ED7FCA22944798A556B56E072,
	ResourceLocationData_get_InternalId_m7B07A2EC474A0768E5AABEF5F5C0E791732E8027,
	ResourceLocationData_get_Provider_mAA09EA5D941C8C38A267FDE28153F3FD491A3CEC,
	ResourceLocationData_get_Dependencies_mB34D1E9CBA0A627B5425AD1EAF5A88C7B95B5BD5,
	ResourceLocationData_get_ResourceType_mFD9450F8EA61975402324BB8F2FF3D619F06CAD3,
	ResourceLocationData__ctor_m0E7FD144A514493E811C7164D5B584A461B191A9,
	ResourceLocationMap__ctor_mC813DB53B5E8FFE370668D269514DF3C560ECF6B,
	ResourceLocationMap_get_LocatorId_m4B732D9F2E7C5D28E56761A9D894DED62D9D5DEE,
	ResourceLocationMap_set_LocatorId_m9E78B67DDCEE65AADB7564874C0E3DF2BC2A16E5,
	ResourceLocationMap__ctor_m261636B722FC0BE9E8FDBAD4A82BAAA6E576E3DD,
	ResourceLocationMap_get_Locations_m053D3170A234FD827295742240A5AB4ED8C94C80,
	ResourceLocationMap_set_Locations_mF5479BA16A1818C48CB358E4584B503D25600431,
	ResourceLocationMap_get_Keys_m69A47B7B44545D92BCE439B17DA5EFF8CE65262E,
	ResourceLocationMap_Locate_mECDC782C947D84FAF8039E47163E941278DA2E60,
	ResourceLocationMap_Add_mADA3092718E8E26CC47C9D3BC4FF46355BBE662A,
	ResourceLocationMap_Add_mB9919BC6957DA163FB74329612D856FBA7473796,
	AddressablesRuntimeProperties_GetAssemblies_m09AECD629BFC348E1A6757DA45B4FAE49323A632,
	AddressablesRuntimeProperties_GetCachedValueCount_m2BD6E72F2379C60A57DC55C862047E3EABD1E6EB,
	AddressablesRuntimeProperties_SetPropertyValue_m29212BFCE259D325CC6F2CCDAC33E5D06F1FFA19,
	AddressablesRuntimeProperties_ClearCachedPropertyValues_m60331DB73383166637F6803E65EE4E9F92F0FEC3,
	AddressablesRuntimeProperties_EvaluateProperty_mD99449A4FDD26E48DC0F72D873D565E36DFF178E,
	AddressablesRuntimeProperties_EvaluateString_mDC0EDF6BE5F3D71A86D9368D65808341B33977F9,
	AddressablesRuntimeProperties_EvaluateString_mBE78D9C49C00AA51BAC9EFDDC2891D3896ED759B,
	AddressablesRuntimeProperties__cctor_m72344F3970961EE4B6E857FDBAF9FC9601D03249,
	CacheInitialization_Initialize_m84C4CA343614C58EA62339A112936B5D2D82F991,
	CacheInitialization_InitializeAsync_m88DBBF94C7F371BDDC6D83593561A55425121D75,
	CacheInitialization_get_RootPath_mCA040C83CD18A5FFEE86DB278DC27BF96AC4EFA2,
	CacheInitialization__ctor_m6D7F13CE6DF84327B832FE5DA423EB5FF3A4A68E,
	CacheInitOp_Init_m85BC46318C85BF88E32AE33E20EF6FBCC86B5A41,
	CacheInitOp_Update_m0DC3663F60875894337799B6788618E87E9F9FAF,
	CacheInitOp_Execute_m4C7CC0D4553AA5CE6B2BFA8CC69F1CD06E676EA4,
	CacheInitOp__ctor_m444BACF330047EFB701B77CB3D2B9B39560E0EDA,
	U3CU3Ec__DisplayClass1_0__ctor_mD5860E8EF9BE05A794379F42531D4079C8AACFAB,
	U3CU3Ec__DisplayClass1_0_U3CInitializeAsyncU3Eb__0_m2538DB099B7B6B28F096C7B427EE119B5B432EB9,
	CacheInitializationData_get_CompressionEnabled_m1A6D2EE22AC36E83DFD290029B49A0FA646A4E1B,
	CacheInitializationData_set_CompressionEnabled_m049A27E23EB72E7F8F417BDF761FC1B62B307780,
	CacheInitializationData_get_CacheDirectoryOverride_mE1EF452D155E7AA280A2DD8E39FC6CA5C2B95F07,
	CacheInitializationData_set_CacheDirectoryOverride_mBB16148369183D82249B0FFA31A15561379123FA,
	CacheInitializationData_get_ExpirationDelay_m47D69B354674B647094AE9D6350073891D084E2C,
	CacheInitializationData_set_ExpirationDelay_m128AB5CF5FBCC522C405CE9E06ED166012C420F2,
	CacheInitializationData_get_LimitCacheSize_m74556C6B8530AC825FA4F1F90480C6DF5BB0EDBA,
	CacheInitializationData_set_LimitCacheSize_mDF1410AD0211710BD6EEF0D1DFCBB45322AAA1A1,
	CacheInitializationData_get_MaximumCacheSize_mA1DF53979B2B8FB504BD7199E274DE47FDACDAA9,
	CacheInitializationData_set_MaximumCacheSize_mADE0EA2E6EE5D93C35FF33A1B799647646498BE7,
	CacheInitializationData__ctor_m1D05D1B29EF2177B68B7436A14ECE60482C5ED8C,
	InitializationOperation__ctor_m50D593F5AB884ECCA24A06110BC70AC6E18B4C0D,
	InitializationOperation_get_Progress_m19B5B699271709F8C49060D72BD3C4CCF9392D2B,
	InitializationOperation_get_DebugName_m30EF56B368268A0A53AB09DAFE7FE9E61C5148EE,
	InitializationOperation_CreateInitializationOperation_m0A06B94DC1C0E72AF6A76829341EAB8BA3B53475,
	InitializationOperation_Execute_m73765CDF2ABBF0C5B42B3EEEE27D478C6E120621,
	InitializationOperation_LoadProvider_mBD87B26C88D9C7A665915F4022EA3A40C6EB68A3,
	InitializationOperation_OnCatalogDataLoaded_m68939EADAC84D1E9B945D1334F093546CACE2C34,
	InitializationOperation_LoadContentCatalog_m73FE2779679B767FC83B5FCD809ECC8F9BE66375,
	InitializationOperation_LoadContentCatalog_mB6F8DBE2F391B2BC6DCC5F10B718B7AEF67F2AAF,
	InitializationOperation_LoadContentCatalogInternal_mB56B5D92CE721F32017E15E29A304AEE408AA543,
	U3CU3Ec__cctor_m79CEA4105B6CB03F698DEE3211F212C5340FAD32,
	U3CU3Ec__ctor_m16C874F7A1A6028AB4ADA575E33B175649B20220,
	U3CU3Ec_U3CExecuteU3Eb__11_0_m4447D36E6E5526706AFF1CCD1C5DCDD945AE4AEB,
	U3CU3Ec__DisplayClass14_0__ctor_mCD083B2540766C586BB16BDD77B124544584BAB2,
	U3CU3Ec__DisplayClass14_0_U3CLoadContentCatalogU3Eb__0_m71905AFDA419CB108FAABEBCDDFCC2AB18627959,
	U3CU3Ec__DisplayClass16_0__ctor_mD50EAF1D4DA9BBDC92F9727BE8F20C9D9680539E,
	U3CU3Ec__DisplayClass16_0_U3CLoadContentCatalogInternalU3Eb__0_mC5EFF2C227A1A0C6C68C4CE4B2E5AE8EE7A6046E,
	ResourceManagerRuntimeData_get_BuildTarget_m700564BBBBEE5DAE2784A7003B94BEF0F4E0E576,
	ResourceManagerRuntimeData_set_BuildTarget_m223B97D38F0ABF30350CC0D97AC1309C52B90D55,
	ResourceManagerRuntimeData_get_SettingsHash_m4C901062D59D872BFA86421F17B776DD80E6510B,
	ResourceManagerRuntimeData_set_SettingsHash_mFE24A6F9127F319C958B42302CB8533711C67963,
	ResourceManagerRuntimeData_get_CatalogLocations_mA1502016990897BC29281693E311DA7DDCD04F3D,
	ResourceManagerRuntimeData_get_ProfileEvents_m79993B4A71501B47BF3E582BE71379A01D553D56,
	ResourceManagerRuntimeData_set_ProfileEvents_m2864B9E3A67779664D86D4BCDC3D0D7D03A2FEA5,
	ResourceManagerRuntimeData_get_LogResourceManagerExceptions_m248B241CAE48E87A91C751C872E66F670F1CFC96,
	ResourceManagerRuntimeData_set_LogResourceManagerExceptions_mD3D941287A8566DC7D22D44F644607EA2523BD99,
	ResourceManagerRuntimeData_get_InitializationObjects_m66450D298C2EA3FC6937F1CDCA3FD45D1EC6419C,
	ResourceManagerRuntimeData_get_DisableCatalogUpdateOnStartup_m1C644ADE8967D7C6B084C9A7B0B093C089E153C0,
	ResourceManagerRuntimeData_set_DisableCatalogUpdateOnStartup_mBF675BA49554986D73793A129FABB219A810904D,
	ResourceManagerRuntimeData_get_IsLocalCatalogInBundle_m8E4E402623F2AA36C604F3612BB19F2AF70C36C4,
	ResourceManagerRuntimeData_set_IsLocalCatalogInBundle_m365491D5CDD96252F9F694DD4334160B6B096CE5,
	ResourceManagerRuntimeData_get_CertificateHandlerType_m197BBAAAA1560C291E4E3104D4E368AC292EAAF1,
	ResourceManagerRuntimeData_set_CertificateHandlerType_m14EA2E7F6F89B5642110A46719CCEF6EE9E14D03,
	ResourceManagerRuntimeData_get_AddressablesVersion_m3B27B471EA8E85668FC89FD378F5FDC666DBCE0F,
	ResourceManagerRuntimeData_set_AddressablesVersion_m715CCA401537DD86A16E29E65F0FE5B914BAB5F4,
	ResourceManagerRuntimeData_get_MaxConcurrentWebRequests_mAFA6F5BF14AB593C0B9E86296CA60B1F7CB98B20,
	ResourceManagerRuntimeData_set_MaxConcurrentWebRequests_m5E25EAB615AEEC0C14A2975321F2C197B7E05449,
	ResourceManagerRuntimeData__ctor_m9A7AE4DAC2751378AC656DA5A60ACC16230C3CD1,
};
extern void RuntimeBuildLog__ctor_m88375AA8CAFE39FD77175C963991221231AAE038_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000004, RuntimeBuildLog__ctor_m88375AA8CAFE39FD77175C963991221231AAE038_AdjustorThunk },
};
static const int32_t s_InvokerIndices[544] = 
{
	14,
	26,
	23,
	62,
	9,
	9,
	23,
	26,
	9,
	9,
	14,
	2542,
	14,
	9,
	23,
	23,
	2543,
	14,
	26,
	14,
	26,
	26,
	27,
	23,
	26,
	27,
	111,
	14,
	4,
	4,
	4,
	4,
	0,
	4,
	154,
	4,
	4,
	4,
	4,
	4,
	372,
	498,
	154,
	137,
	154,
	137,
	154,
	2544,
	137,
	2545,
	2545,
	2546,
	2546,
	2547,
	2545,
	-1,
	-1,
	-1,
	-1,
	2548,
	2548,
	2548,
	2549,
	2549,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2550,
	114,
	2551,
	2552,
	2553,
	2553,
	2553,
	2553,
	2553,
	2554,
	2555,
	2555,
	2556,
	2556,
	154,
	154,
	154,
	154,
	154,
	2557,
	2557,
	2557,
	2557,
	2557,
	2558,
	2559,
	2558,
	2559,
	2560,
	2560,
	2558,
	2559,
	2558,
	2559,
	2560,
	2560,
	2561,
	2561,
	2561,
	2561,
	2562,
	2563,
	2564,
	2562,
	2563,
	2564,
	2565,
	2566,
	186,
	154,
	3,
	3,
	14,
	26,
	14,
	10,
	10,
	26,
	23,
	14,
	26,
	2081,
	89,
	2080,
	14,
	14,
	14,
	14,
	26,
	27,
	26,
	27,
	26,
	2567,
	27,
	28,
	14,
	197,
	26,
	23,
	2568,
	2569,
	2570,
	2571,
	105,
	2572,
	2573,
	-1,
	2574,
	23,
	-1,
	-1,
	-1,
	2575,
	2576,
	2577,
	2578,
	-1,
	-1,
	-1,
	-1,
	-1,
	2074,
	2074,
	2074,
	-1,
	-1,
	2074,
	2579,
	2579,
	2580,
	2580,
	2581,
	0,
	2582,
	2581,
	2582,
	2583,
	2584,
	9,
	2585,
	2585,
	2585,
	2586,
	2587,
	2586,
	2587,
	2588,
	2589,
	2588,
	2589,
	9,
	2590,
	2591,
	2591,
	2592,
	2593,
	2594,
	2593,
	2594,
	2594,
	28,
	2595,
	28,
	14,
	2596,
	90,
	112,
	2597,
	2099,
	2598,
	2598,
	2598,
	2598,
	2598,
	2099,
	2074,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	197,
	14,
	89,
	197,
	14,
	197,
	23,
	23,
	14,
	115,
	23,
	23,
	23,
	2099,
	3,
	23,
	28,
	9,
	28,
	23,
	9,
	23,
	2599,
	2597,
	-1,
	-1,
	23,
	2600,
	23,
	2600,
	-1,
	-1,
	-1,
	-1,
	23,
	2601,
	23,
	2601,
	23,
	2602,
	2598,
	23,
	2602,
	2598,
	23,
	2602,
	2598,
	23,
	2603,
	23,
	2603,
	23,
	2603,
	23,
	2604,
	23,
	2604,
	23,
	2605,
	23,
	2605,
	23,
	2573,
	23,
	2606,
	2074,
	14,
	26,
	14,
	89,
	10,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	26,
	26,
	26,
	9,
	26,
	9,
	2081,
	14,
	14,
	14,
	26,
	14,
	89,
	89,
	23,
	26,
	14,
	14,
	-1,
	-1,
	2607,
	2608,
	2609,
	-1,
	2610,
	2607,
	2608,
	2609,
	89,
	23,
	26,
	9,
	9,
	14,
	89,
	26,
	2611,
	23,
	26,
	23,
	3,
	23,
	9,
	26,
	2612,
	23,
	26,
	23,
	3,
	23,
	9,
	14,
	14,
	26,
	2568,
	822,
	21,
	106,
	23,
	3,
	2613,
	23,
	26,
	2614,
	2614,
	2071,
	23,
	131,
	591,
	119,
	138,
	26,
	27,
	2093,
	2615,
	23,
	807,
	28,
	143,
	26,
	23,
	26,
	2616,
	26,
	26,
	89,
	89,
	26,
	23,
	23,
	23,
	26,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	971,
	14,
	26,
	2088,
	2617,
	2088,
	2617,
	14,
	26,
	14,
	14,
	23,
	105,
	28,
	1,
	23,
	14,
	14,
	14,
	89,
	10,
	14,
	14,
	26,
	14,
	14,
	112,
	1174,
	14,
	14,
	2568,
	2568,
	14,
	14,
	23,
	14,
	14,
	14,
	14,
	14,
	822,
	130,
	14,
	26,
	27,
	14,
	26,
	14,
	2568,
	27,
	27,
	4,
	106,
	137,
	3,
	0,
	0,
	2618,
	3,
	90,
	2087,
	4,
	23,
	26,
	335,
	23,
	23,
	23,
	89,
	89,
	31,
	14,
	26,
	10,
	32,
	89,
	31,
	172,
	200,
	23,
	26,
	727,
	14,
	2619,
	23,
	2620,
	2621,
	2619,
	2622,
	107,
	3,
	23,
	9,
	23,
	2623,
	23,
	2597,
	14,
	26,
	14,
	26,
	14,
	89,
	31,
	89,
	31,
	14,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	10,
	32,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[35] = 
{
	{ 0x02000012, { 58, 1 } },
	{ 0x02000015, { 59, 1 } },
	{ 0x02000016, { 60, 1 } },
	{ 0x02000026, { 61, 3 } },
	{ 0x06000038, { 0, 1 } },
	{ 0x06000039, { 1, 1 } },
	{ 0x0600003A, { 2, 1 } },
	{ 0x0600003B, { 3, 1 } },
	{ 0x06000041, { 4, 1 } },
	{ 0x06000042, { 5, 1 } },
	{ 0x06000043, { 6, 1 } },
	{ 0x06000044, { 7, 1 } },
	{ 0x06000045, { 8, 1 } },
	{ 0x06000046, { 9, 1 } },
	{ 0x06000047, { 10, 1 } },
	{ 0x06000048, { 11, 1 } },
	{ 0x06000049, { 12, 1 } },
	{ 0x0600004A, { 13, 1 } },
	{ 0x0600004B, { 14, 1 } },
	{ 0x0600004C, { 15, 1 } },
	{ 0x0600004D, { 16, 1 } },
	{ 0x060000A5, { 17, 1 } },
	{ 0x060000A8, { 18, 2 } },
	{ 0x060000A9, { 20, 6 } },
	{ 0x060000AA, { 26, 5 } },
	{ 0x060000AF, { 31, 2 } },
	{ 0x060000B0, { 33, 6 } },
	{ 0x060000B1, { 39, 5 } },
	{ 0x060000B2, { 44, 6 } },
	{ 0x060000B3, { 50, 5 } },
	{ 0x060000B7, { 55, 1 } },
	{ 0x060000B8, { 56, 2 } },
	{ 0x06000154, { 64, 1 } },
	{ 0x06000155, { 65, 1 } },
	{ 0x06000159, { 66, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[69] = 
{
	{ (Il2CppRGCTXDataType)3, 22094 },
	{ (Il2CppRGCTXDataType)3, 22095 },
	{ (Il2CppRGCTXDataType)3, 22096 },
	{ (Il2CppRGCTXDataType)3, 22097 },
	{ (Il2CppRGCTXDataType)3, 22098 },
	{ (Il2CppRGCTXDataType)3, 22099 },
	{ (Il2CppRGCTXDataType)3, 22100 },
	{ (Il2CppRGCTXDataType)3, 22101 },
	{ (Il2CppRGCTXDataType)3, 22102 },
	{ (Il2CppRGCTXDataType)3, 22103 },
	{ (Il2CppRGCTXDataType)3, 22104 },
	{ (Il2CppRGCTXDataType)3, 22105 },
	{ (Il2CppRGCTXDataType)3, 22106 },
	{ (Il2CppRGCTXDataType)3, 22107 },
	{ (Il2CppRGCTXDataType)3, 22108 },
	{ (Il2CppRGCTXDataType)3, 22109 },
	{ (Il2CppRGCTXDataType)3, 22110 },
	{ (Il2CppRGCTXDataType)3, 22111 },
	{ (Il2CppRGCTXDataType)3, 22112 },
	{ (Il2CppRGCTXDataType)3, 22113 },
	{ (Il2CppRGCTXDataType)2, 27987 },
	{ (Il2CppRGCTXDataType)3, 22114 },
	{ (Il2CppRGCTXDataType)3, 22115 },
	{ (Il2CppRGCTXDataType)2, 27988 },
	{ (Il2CppRGCTXDataType)3, 22116 },
	{ (Il2CppRGCTXDataType)3, 22117 },
	{ (Il2CppRGCTXDataType)3, 22118 },
	{ (Il2CppRGCTXDataType)3, 22119 },
	{ (Il2CppRGCTXDataType)1, 26235 },
	{ (Il2CppRGCTXDataType)3, 22120 },
	{ (Il2CppRGCTXDataType)3, 22121 },
	{ (Il2CppRGCTXDataType)3, 22122 },
	{ (Il2CppRGCTXDataType)3, 22123 },
	{ (Il2CppRGCTXDataType)2, 27989 },
	{ (Il2CppRGCTXDataType)3, 22124 },
	{ (Il2CppRGCTXDataType)3, 22125 },
	{ (Il2CppRGCTXDataType)2, 27990 },
	{ (Il2CppRGCTXDataType)3, 22126 },
	{ (Il2CppRGCTXDataType)3, 22127 },
	{ (Il2CppRGCTXDataType)3, 22128 },
	{ (Il2CppRGCTXDataType)3, 22129 },
	{ (Il2CppRGCTXDataType)1, 26245 },
	{ (Il2CppRGCTXDataType)3, 22130 },
	{ (Il2CppRGCTXDataType)3, 22131 },
	{ (Il2CppRGCTXDataType)2, 27991 },
	{ (Il2CppRGCTXDataType)3, 22132 },
	{ (Il2CppRGCTXDataType)3, 22133 },
	{ (Il2CppRGCTXDataType)2, 27992 },
	{ (Il2CppRGCTXDataType)3, 22134 },
	{ (Il2CppRGCTXDataType)3, 22135 },
	{ (Il2CppRGCTXDataType)3, 22136 },
	{ (Il2CppRGCTXDataType)3, 22137 },
	{ (Il2CppRGCTXDataType)1, 26253 },
	{ (Il2CppRGCTXDataType)3, 22138 },
	{ (Il2CppRGCTXDataType)3, 22139 },
	{ (Il2CppRGCTXDataType)2, 26256 },
	{ (Il2CppRGCTXDataType)3, 22140 },
	{ (Il2CppRGCTXDataType)2, 26257 },
	{ (Il2CppRGCTXDataType)3, 22141 },
	{ (Il2CppRGCTXDataType)3, 22142 },
	{ (Il2CppRGCTXDataType)3, 22143 },
	{ (Il2CppRGCTXDataType)3, 22144 },
	{ (Il2CppRGCTXDataType)3, 22145 },
	{ (Il2CppRGCTXDataType)1, 26350 },
	{ (Il2CppRGCTXDataType)3, 22146 },
	{ (Il2CppRGCTXDataType)3, 22147 },
	{ (Il2CppRGCTXDataType)3, 22148 },
	{ (Il2CppRGCTXDataType)3, 22149 },
	{ (Il2CppRGCTXDataType)2, 26368 },
};
extern const Il2CppCodeGenModule g_Unity_AddressablesCodeGenModule;
const Il2CppCodeGenModule g_Unity_AddressablesCodeGenModule = 
{
	"Unity.Addressables.dll",
	544,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	35,
	s_rgctxIndices,
	69,
	s_rgctxValues,
	NULL,
};
