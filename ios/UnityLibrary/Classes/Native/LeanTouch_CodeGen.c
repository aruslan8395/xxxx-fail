﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean PlacementObject::get_Selected()
extern void PlacementObject_get_Selected_m455279660122821B10F23CBD2E15BD4EF3DCB8B3 (void);
// 0x00000002 System.Void PlacementObject::set_Selected(System.Boolean)
extern void PlacementObject_set_Selected_mDDB172B6806809F8BE1119A2AE9FAD8F0C1D5CA1 (void);
// 0x00000003 System.Boolean PlacementObject::get_Moved()
extern void PlacementObject_get_Moved_m4DFE0D4AFFB85525800C184B93EB311C7AC3ED12 (void);
// 0x00000004 System.Void PlacementObject::set_Moved(System.Boolean)
extern void PlacementObject_set_Moved_m942BA026A031DDB8E21893D942A7299A36BD24B9 (void);
// 0x00000005 System.Void PlacementObject::.ctor()
extern void PlacementObject__ctor_mEAC2DD06D9139149C6983DBAC5A212060F415091 (void);
// 0x00000006 System.Void Lean.Touch.LeanInfoText::Display(System.String)
extern void LeanInfoText_Display_mC89B42B5079E21579121AD580789C28FF7235AF8 (void);
// 0x00000007 System.Void Lean.Touch.LeanInfoText::Display(System.String,System.String)
extern void LeanInfoText_Display_m873F45E5221D8754281AC6E843D440EBA54E6BA9 (void);
// 0x00000008 System.Void Lean.Touch.LeanInfoText::Display(System.Int32)
extern void LeanInfoText_Display_m8EA71728D9EBD19914BDBC1C8705DA47896FD064 (void);
// 0x00000009 System.Void Lean.Touch.LeanInfoText::Display(System.Single)
extern void LeanInfoText_Display_m36B74395502B3F97AAE87180F77140DBED5E36F4 (void);
// 0x0000000A System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector2)
extern void LeanInfoText_Display_mDBBD632A9C78FC7622CA415046269CA9DE1A05B8 (void);
// 0x0000000B System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector3)
extern void LeanInfoText_Display_mD435885000DDF1691E344CC619666DEEAC19EBEB (void);
// 0x0000000C System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector4)
extern void LeanInfoText_Display_m4379AD4A936839A5B6ECCCC254E77F16D862E4FE (void);
// 0x0000000D System.Void Lean.Touch.LeanInfoText::Display(System.Int32,System.Int32)
extern void LeanInfoText_Display_m1E63A97FA5A497B4307DBE823098A08B77FB9C2C (void);
// 0x0000000E System.Void Lean.Touch.LeanInfoText::.ctor()
extern void LeanInfoText__ctor_m8921A7F24A14E928B11C915243D040C8CAA97F45 (void);
// 0x0000000F System.Void Lean.Touch.LeanPulseScale::Update()
extern void LeanPulseScale_Update_mF09F52328885E2FA0D6FCA40C845B9891E3BFD8F (void);
// 0x00000010 System.Void Lean.Touch.LeanPulseScale::.ctor()
extern void LeanPulseScale__ctor_m20DEFFD37C09DA08DABFFFE102F74755EF1F5C2E (void);
// 0x00000011 System.Void Lean.Touch.LeanTouchEvents::OnEnable()
extern void LeanTouchEvents_OnEnable_mC96EFD5D7E9D5DA3241CA81FE7227962B324BC6C (void);
// 0x00000012 System.Void Lean.Touch.LeanTouchEvents::OnDisable()
extern void LeanTouchEvents_OnDisable_m85ECF46CFB1EC4C3BD316AF0CAB7DFE29CA7F13B (void);
// 0x00000013 System.Void Lean.Touch.LeanTouchEvents::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerDown_mA3D0F877BD6DC57E77FDE1C49CDBDBE4CEECE30D (void);
// 0x00000014 System.Void Lean.Touch.LeanTouchEvents::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUpdate_m8CC3E943326E5A66696F913EB481C4797E0B14B5 (void);
// 0x00000015 System.Void Lean.Touch.LeanTouchEvents::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUp_mBF83766DF3C28C1FE18FB02CCA86A4BF033D695E (void);
// 0x00000016 System.Void Lean.Touch.LeanTouchEvents::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerTap_m5C23A85C50C9B1ACB7DBB7E0AD237051B17ADF21 (void);
// 0x00000017 System.Void Lean.Touch.LeanTouchEvents::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerSwipe_m8AC15F1904BFC54B5C4271C6FED91A39A4EE91B2 (void);
// 0x00000018 System.Void Lean.Touch.LeanTouchEvents::HandleGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanTouchEvents_HandleGesture_m4413436E90CBA5D853DBAA3582996E9D0898876A (void);
// 0x00000019 System.Void Lean.Touch.LeanTouchEvents::.ctor()
extern void LeanTouchEvents__ctor_m4E0D5C623AC099DE68B77C93C31E031EDF2F117E (void);
// 0x0000001A System.Void Lean.Touch.LeanDragCamera::MoveToSelection()
extern void LeanDragCamera_MoveToSelection_m728D976D87EA012DCED4FFBA6DED868FB2DD26C1 (void);
// 0x0000001B System.Void Lean.Touch.LeanDragCamera::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragCamera_AddFinger_m6FE4EA9283A7E43B7A8922895E07F2E3C9903696 (void);
// 0x0000001C System.Void Lean.Touch.LeanDragCamera::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragCamera_RemoveFinger_m54D8A9307E55A339AE1585A2E54A209B5ED10679 (void);
// 0x0000001D System.Void Lean.Touch.LeanDragCamera::RemoveAllFingers()
extern void LeanDragCamera_RemoveAllFingers_m64128368181902BAB14710E7B2ADF1D453E6C271 (void);
// 0x0000001E System.Void Lean.Touch.LeanDragCamera::Awake()
extern void LeanDragCamera_Awake_m7B3706BDE4C1895DE21DBEED9032584681E45FAF (void);
// 0x0000001F System.Void Lean.Touch.LeanDragCamera::LateUpdate()
extern void LeanDragCamera_LateUpdate_m8728E32FB43B7CA19EF520C4C4C78BE92EAF8478 (void);
// 0x00000020 System.Void Lean.Touch.LeanDragCamera::.ctor()
extern void LeanDragCamera__ctor_mF87DBFC163061D8888517595CA0C65FB7FAB215C (void);
// 0x00000021 System.Void Lean.Touch.LeanDragTrail::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTrail_AddFinger_m4C98B01BAF07928B343A1F333934152EA50A5587 (void);
// 0x00000022 System.Void Lean.Touch.LeanDragTrail::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTrail_RemoveFinger_m082E75D7FCD1A0C44BE87E5182160E265A06A20A (void);
// 0x00000023 System.Void Lean.Touch.LeanDragTrail::RemoveAllFingers()
extern void LeanDragTrail_RemoveAllFingers_mF30DE6483ADF67991F0892CC6006D4A0035478A5 (void);
// 0x00000024 System.Void Lean.Touch.LeanDragTrail::Awake()
extern void LeanDragTrail_Awake_m0B4D21DA7885035CDFBE6D1A490EFD1496F5B9D6 (void);
// 0x00000025 System.Void Lean.Touch.LeanDragTrail::OnEnable()
extern void LeanDragTrail_OnEnable_mD0E0F8C3FE3FCA92A57C80620328262F7A1A0C68 (void);
// 0x00000026 System.Void Lean.Touch.LeanDragTrail::OnDisable()
extern void LeanDragTrail_OnDisable_mEBCA3EAE1A31028425B610AFF388E88C851F1C1E (void);
// 0x00000027 System.Void Lean.Touch.LeanDragTrail::Update()
extern void LeanDragTrail_Update_mC915D505899B9904FE5E54E190E56DE13AD3E07A (void);
// 0x00000028 System.Void Lean.Touch.LeanDragTrail::UpdateLine(Lean.Touch.LeanDragTrail/FingerData,Lean.Touch.LeanFinger,UnityEngine.LineRenderer)
extern void LeanDragTrail_UpdateLine_mFDBFF66AEE9C9112A54819BF6B6CD8DCE17434F6 (void);
// 0x00000029 System.Void Lean.Touch.LeanDragTrail::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanDragTrail_HandleFingerUp_mDD4968D9C46EFF834BABD5D0E1E9AA9F90B618C2 (void);
// 0x0000002A System.Void Lean.Touch.LeanDragTrail::.ctor()
extern void LeanDragTrail__ctor_m11B1E9B916902DC24128BD3915AD14CBF82B0A25 (void);
// 0x0000002B System.Void Lean.Touch.LeanDragTranslate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslate_AddFinger_m4CC7D46FE10E3FCCFF4BDF15FE850E62BF32DC41 (void);
// 0x0000002C System.Void Lean.Touch.LeanDragTranslate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslate_RemoveFinger_m182850F91D0B4D564FF7C412C18AD785AFBA9925 (void);
// 0x0000002D System.Void Lean.Touch.LeanDragTranslate::RemoveAllFingers()
extern void LeanDragTranslate_RemoveAllFingers_mF7F2EB545B6B66EBA950974F39C40ED4E640933E (void);
// 0x0000002E System.Void Lean.Touch.LeanDragTranslate::Awake()
extern void LeanDragTranslate_Awake_m84CC62EDC5669DC6ED9C28AB7D5D3686663F54C2 (void);
// 0x0000002F System.Void Lean.Touch.LeanDragTranslate::Update()
extern void LeanDragTranslate_Update_mBE0714E3ACFE28E012409462C476C7B2D59EFC7B (void);
// 0x00000030 System.Void Lean.Touch.LeanDragTranslate::TranslateUI(UnityEngine.Vector2)
extern void LeanDragTranslate_TranslateUI_m45EE318C09E821C1C43A95B416F6AAA7D342DBF3 (void);
// 0x00000031 System.Void Lean.Touch.LeanDragTranslate::Translate(UnityEngine.Vector2)
extern void LeanDragTranslate_Translate_m7BD8AC2BA77F507F8FBC1DF19C3108011A03E080 (void);
// 0x00000032 System.Void Lean.Touch.LeanDragTranslate::.ctor()
extern void LeanDragTranslate__ctor_mBC9000EFE1DCCCDB9B84F2CC265385963EC50A6D (void);
// 0x00000033 System.Int32 Lean.Touch.LeanFingerData::Count(System.Collections.Generic.List`1<T>)
// 0x00000034 System.Boolean Lean.Touch.LeanFingerData::Exists(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger)
// 0x00000035 System.Void Lean.Touch.LeanFingerData::Remove(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger,System.Collections.Generic.Stack`1<T>)
// 0x00000036 System.Void Lean.Touch.LeanFingerData::RemoveAll(System.Collections.Generic.List`1<T>,System.Collections.Generic.Stack`1<T>)
// 0x00000037 T Lean.Touch.LeanFingerData::Find(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger)
// 0x00000038 T Lean.Touch.LeanFingerData::FindOrCreate(System.Collections.Generic.List`1<T>&,Lean.Touch.LeanFinger)
// 0x00000039 System.Void Lean.Touch.LeanFingerData::.ctor()
extern void LeanFingerData__ctor_mAB102253306FDAE43FA8680B4D84059BFD831A18 (void);
// 0x0000003A Lean.Touch.LeanFingerDown/LeanFingerEvent Lean.Touch.LeanFingerDown::get_OnFinger()
extern void LeanFingerDown_get_OnFinger_m0BAB3394325C043DB48660D3334EBBED0CF5C2D7 (void);
// 0x0000003B Lean.Touch.LeanFingerDown/Vector3Event Lean.Touch.LeanFingerDown::get_OnWorld()
extern void LeanFingerDown_get_OnWorld_m61EB189436881FB0800230865BDDB24F4A92D858 (void);
// 0x0000003C Lean.Touch.LeanFingerDown/Vector2Event Lean.Touch.LeanFingerDown::get_OnScreen()
extern void LeanFingerDown_get_OnScreen_m3A7F4C0E825E449D718FC01E465200DDEFD399BD (void);
// 0x0000003D System.Void Lean.Touch.LeanFingerDown::Awake()
extern void LeanFingerDown_Awake_m027ADE644DFD5664ED9DF5DA89C5EF7E5BC448F5 (void);
// 0x0000003E System.Void Lean.Touch.LeanFingerDown::OnEnable()
extern void LeanFingerDown_OnEnable_m7F5CF06AC5361A7E5739DFDC2B1F02A9A5DB211C (void);
// 0x0000003F System.Void Lean.Touch.LeanFingerDown::OnDisable()
extern void LeanFingerDown_OnDisable_mB196EC8556D9A04BFBC9D47081B50512FBBD02AA (void);
// 0x00000040 System.Void Lean.Touch.LeanFingerDown::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerDown_HandleFingerDown_m67B3DD3FD8892A8627E3A9073F97A7A8A94AB60B (void);
// 0x00000041 System.Void Lean.Touch.LeanFingerDown::.ctor()
extern void LeanFingerDown__ctor_m74CCE40102A349C904F633B25B3158010F09C5F7 (void);
// 0x00000042 System.Void Lean.Touch.LeanFingerFilter::.ctor(System.Boolean)
extern void LeanFingerFilter__ctor_mE72DC02BFD6E51E07FF20A8BEF9F985347483AA5 (void);
// 0x00000043 System.Void Lean.Touch.LeanFingerFilter::.ctor(Lean.Touch.LeanFingerFilter/FilterType,System.Boolean,System.Int32,System.Int32,Lean.Touch.LeanSelectable)
extern void LeanFingerFilter__ctor_mFB253DF5673F23327658C66A9D4AA8C0F8725263 (void);
// 0x00000044 System.Void Lean.Touch.LeanFingerFilter::UpdateRequiredSelectable(UnityEngine.GameObject)
extern void LeanFingerFilter_UpdateRequiredSelectable_m3E2BEC9AB1DAA8AB70EC2E11B8D1227945031E7D (void);
// 0x00000045 System.Void Lean.Touch.LeanFingerFilter::AddFinger(Lean.Touch.LeanFinger)
extern void LeanFingerFilter_AddFinger_m74C4A364453ED4A22F8423749A505DF0BA5A92EE (void);
// 0x00000046 System.Void Lean.Touch.LeanFingerFilter::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanFingerFilter_RemoveFinger_mF840A547635068D3F879DF19F3E17BE362E926F7 (void);
// 0x00000047 System.Void Lean.Touch.LeanFingerFilter::RemoveAllFingers()
extern void LeanFingerFilter_RemoveAllFingers_mE802FAFCE7EBF3FE06ECDF1BA15F3644F85F3E9F (void);
// 0x00000048 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::GetFingers(System.Boolean)
extern void LeanFingerFilter_GetFingers_mB21B35B96FBDE7030A77F8C129ED467C6C85EDDE (void);
// 0x00000049 System.Boolean Lean.Touch.LeanFingerFilter::SimulatedFingersExist(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanFingerFilter_SimulatedFingersExist_m0D6895747216B91285CDDD5923490E2E4599061A (void);
// 0x0000004A Lean.Touch.LeanFingerOld/LeanFingerEvent Lean.Touch.LeanFingerOld::get_OnFinger()
extern void LeanFingerOld_get_OnFinger_m2BC7D6FCE3E19A3DCAC7D1CDE5A8D35F1BEB7A58 (void);
// 0x0000004B Lean.Touch.LeanFingerOld/Vector3Event Lean.Touch.LeanFingerOld::get_OnWorld()
extern void LeanFingerOld_get_OnWorld_mAFAD32A68D9580E9F73A0CDAC8928CF3B7946F9E (void);
// 0x0000004C Lean.Touch.LeanFingerOld/Vector2Event Lean.Touch.LeanFingerOld::get_OnScreen()
extern void LeanFingerOld_get_OnScreen_m72B61F60B8ED378D6D0DFB4C79136D6521FCD022 (void);
// 0x0000004D System.Void Lean.Touch.LeanFingerOld::Start()
extern void LeanFingerOld_Start_m7F77C11EA28F9587CB04AED703C17B17FE040BBE (void);
// 0x0000004E System.Void Lean.Touch.LeanFingerOld::OnEnable()
extern void LeanFingerOld_OnEnable_m88EB1D8CCEFDB54C3479B8DF1CD6561FEEF7AFAE (void);
// 0x0000004F System.Void Lean.Touch.LeanFingerOld::OnDisable()
extern void LeanFingerOld_OnDisable_mDABDB163715447F4C07533350B4A392495E17B8B (void);
// 0x00000050 System.Void Lean.Touch.LeanFingerOld::HandleFingerOld(Lean.Touch.LeanFinger)
extern void LeanFingerOld_HandleFingerOld_m48AFD28E9D1FC3B31F34E4A2CC2D224D82379E68 (void);
// 0x00000051 System.Void Lean.Touch.LeanFingerOld::.ctor()
extern void LeanFingerOld__ctor_mFA869EE1D8F3A2CEE0FE48E5ACD6DE994EA11788 (void);
// 0x00000052 System.Void Lean.Touch.LeanFingerSwipe::Awake()
extern void LeanFingerSwipe_Awake_m181DA196669AB41D1C30B42C175E88D28204D211 (void);
// 0x00000053 System.Void Lean.Touch.LeanFingerSwipe::OnEnable()
extern void LeanFingerSwipe_OnEnable_m991E730B1CEBD2F228D61BF25831C3F4DD78941C (void);
// 0x00000054 System.Void Lean.Touch.LeanFingerSwipe::OnDisable()
extern void LeanFingerSwipe_OnDisable_m799D8A4E4B5F351E64812073F7BFD6B526A26C32 (void);
// 0x00000055 System.Void Lean.Touch.LeanFingerSwipe::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanFingerSwipe_HandleFingerSwipe_m1BD172260F0DFBF5C928D69596BAFF359A9CB73B (void);
// 0x00000056 System.Void Lean.Touch.LeanFingerSwipe::.ctor()
extern void LeanFingerSwipe__ctor_mCDA967A7F4AB7F9BCB88B7DDE3B96D7B522BAB88 (void);
// 0x00000057 Lean.Touch.LeanFingerTap/LeanFingerEvent Lean.Touch.LeanFingerTap::get_OnFinger()
extern void LeanFingerTap_get_OnFinger_mD89F0648B98225A985D39D8A5EDA267E291E6455 (void);
// 0x00000058 Lean.Touch.LeanFingerTap/IntEvent Lean.Touch.LeanFingerTap::get_OnCount()
extern void LeanFingerTap_get_OnCount_mCA987B94870BE58577F047F81C4EF6405B8DA3BB (void);
// 0x00000059 Lean.Touch.LeanFingerTap/Vector3Event Lean.Touch.LeanFingerTap::get_OnWorld()
extern void LeanFingerTap_get_OnWorld_m15E0F71373F995CC9A51AA0E71C0DAA2803FEF8D (void);
// 0x0000005A Lean.Touch.LeanFingerTap/Vector2Event Lean.Touch.LeanFingerTap::get_OnScreen()
extern void LeanFingerTap_get_OnScreen_mEE11634BE7DCBF458BD79C48677A0EF151298297 (void);
// 0x0000005B System.Void Lean.Touch.LeanFingerTap::Start()
extern void LeanFingerTap_Start_m42FB925B43082A52181D40459B05D57D9A6BC330 (void);
// 0x0000005C System.Void Lean.Touch.LeanFingerTap::OnEnable()
extern void LeanFingerTap_OnEnable_m48D53E704DE813C41C4D47344B213C4F14E57B59 (void);
// 0x0000005D System.Void Lean.Touch.LeanFingerTap::OnDisable()
extern void LeanFingerTap_OnDisable_m32CEB4ED012CDA46405AE2F34CFDB1EDF40445FA (void);
// 0x0000005E System.Void Lean.Touch.LeanFingerTap::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanFingerTap_HandleFingerTap_mD5BA50BD353B8EB50A0C05D8EBB66314716CDCC1 (void);
// 0x0000005F System.Void Lean.Touch.LeanFingerTap::.ctor()
extern void LeanFingerTap__ctor_m1D1E504F1D1C1A68A68A66DAD66ACA2C2B6A875E (void);
// 0x00000060 Lean.Touch.LeanFingerUp/LeanFingerEvent Lean.Touch.LeanFingerUp::get_OnFinger()
extern void LeanFingerUp_get_OnFinger_mC8A52BA4BD078D1FC5930831B5C27D556E6D7018 (void);
// 0x00000061 Lean.Touch.LeanFingerUp/Vector3Event Lean.Touch.LeanFingerUp::get_OnWorld()
extern void LeanFingerUp_get_OnWorld_m402AD3A542D8673FAE8DC5C3B7B51A93CD0F9D7D (void);
// 0x00000062 Lean.Touch.LeanFingerUp/Vector2Event Lean.Touch.LeanFingerUp::get_OnScreen()
extern void LeanFingerUp_get_OnScreen_mB4A4AE71D19E07C6CD7771BC0040FD3F8A9FDDCC (void);
// 0x00000063 System.Void Lean.Touch.LeanFingerUp::Start()
extern void LeanFingerUp_Start_mA9ED0637901B6AB89B2B03333C64C8CFEB113469 (void);
// 0x00000064 System.Void Lean.Touch.LeanFingerUp::OnEnable()
extern void LeanFingerUp_OnEnable_m57D99AFBFE1EBC40A2ED497310266F3330DF80BA (void);
// 0x00000065 System.Void Lean.Touch.LeanFingerUp::OnDisable()
extern void LeanFingerUp_OnDisable_m3B088021E233E1458A2A06BC417020E2D33685AF (void);
// 0x00000066 System.Void Lean.Touch.LeanFingerUp::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerUp_HandleFingerUp_m239A03C8BFCC3DEE3B86E8E2A29FF838D9B4E97B (void);
// 0x00000067 System.Void Lean.Touch.LeanFingerUp::.ctor()
extern void LeanFingerUp__ctor_mEF8E87E6EA5F3096B05ABC337633864BF00D8FC0 (void);
// 0x00000068 Lean.Touch.LeanFingerUpdate/LeanFingerEvent Lean.Touch.LeanFingerUpdate::get_OnFinger()
extern void LeanFingerUpdate_get_OnFinger_m09B173C847C6A8760EB68891E5D644923C1095F3 (void);
// 0x00000069 Lean.Touch.LeanFingerUpdate/Vector2Event Lean.Touch.LeanFingerUpdate::get_OnDelta()
extern void LeanFingerUpdate_get_OnDelta_m55ECD79AC0B829C76273BACE0709CF32472BC861 (void);
// 0x0000006A Lean.Touch.LeanFingerUpdate/FloatEvent Lean.Touch.LeanFingerUpdate::get_OnDistance()
extern void LeanFingerUpdate_get_OnDistance_m381843B49142966FE9FCD0AC9C28151357FC0EB2 (void);
// 0x0000006B Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldFrom()
extern void LeanFingerUpdate_get_OnWorldFrom_m58399A52D0C9E5B7710EAC9E206BA5A68565BF12 (void);
// 0x0000006C Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldTo()
extern void LeanFingerUpdate_get_OnWorldTo_mE5B3AFBC4CF85834C13E649655A07E85CD66DCA1 (void);
// 0x0000006D Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldDelta()
extern void LeanFingerUpdate_get_OnWorldDelta_mBE545C2F5CAA0080B328564804B66F46648785A6 (void);
// 0x0000006E Lean.Touch.LeanFingerUpdate/Vector3Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldFromTo()
extern void LeanFingerUpdate_get_OnWorldFromTo_m7ACB67A531A8719C4EB4376FE9B6807056F955A0 (void);
// 0x0000006F System.Void Lean.Touch.LeanFingerUpdate::Awake()
extern void LeanFingerUpdate_Awake_mA2FA75D1D7F5C66DCCB56493AB5779E9A00D11F5 (void);
// 0x00000070 System.Void Lean.Touch.LeanFingerUpdate::OnEnable()
extern void LeanFingerUpdate_OnEnable_mC2AEE1A2604D91209618A698F3B9103A4E63A353 (void);
// 0x00000071 System.Void Lean.Touch.LeanFingerUpdate::OnDisable()
extern void LeanFingerUpdate_OnDisable_mACF860169F38FC215F48B873BA1297002B2BFBDC (void);
// 0x00000072 System.Void Lean.Touch.LeanFingerUpdate::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanFingerUpdate_HandleFingerUpdate_m31485A52BCAF37B13920B56176F4964803E5C58A (void);
// 0x00000073 System.Void Lean.Touch.LeanFingerUpdate::.ctor()
extern void LeanFingerUpdate__ctor_mB48F8C1BAA0286249DF857C371C791E061C0A53A (void);
// 0x00000074 System.Void Lean.Touch.LeanPinchScale::AddFinger(Lean.Touch.LeanFinger)
extern void LeanPinchScale_AddFinger_m742DED990437B51659621844167EB76BF2CE7717 (void);
// 0x00000075 System.Void Lean.Touch.LeanPinchScale::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanPinchScale_RemoveFinger_mE34778EB1D6CB110274CE077F081BECBAC14B404 (void);
// 0x00000076 System.Void Lean.Touch.LeanPinchScale::RemoveAllFingers()
extern void LeanPinchScale_RemoveAllFingers_m7AA07C179F616386BFAB3774FEBD60D479FAC440 (void);
// 0x00000077 System.Void Lean.Touch.LeanPinchScale::Awake()
extern void LeanPinchScale_Awake_m617CBABFC1E61A7DEB6195C8965F808CF521AA8C (void);
// 0x00000078 System.Void Lean.Touch.LeanPinchScale::Update()
extern void LeanPinchScale_Update_m273AA43206C30781E8C8B4D211AA8FE7B1B0ABF5 (void);
// 0x00000079 System.Void Lean.Touch.LeanPinchScale::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanPinchScale_TranslateUI_mFD2A8D66341DA9F9CA208E26844CE7DAD20D1384 (void);
// 0x0000007A System.Void Lean.Touch.LeanPinchScale::Translate(System.Single,UnityEngine.Vector2)
extern void LeanPinchScale_Translate_m3C7C5D94116565DEBC1124620B4CABABEA067712 (void);
// 0x0000007B System.Void Lean.Touch.LeanPinchScale::.ctor()
extern void LeanPinchScale__ctor_m314559A2E8548D25C9A815773EE047E99458E036 (void);
// 0x0000007C System.Void Lean.Touch.LeanScreenDepth::.ctor(Lean.Touch.LeanScreenDepth/ConversionType,System.Int32,System.Single)
extern void LeanScreenDepth__ctor_m09C2B43694D7A2B959469447627D3B25AB095FE9 (void);
// 0x0000007D UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::Convert(UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_Convert_m4CA8998FD975A5AC752171657968240E73225A49 (void);
// 0x0000007E UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::ConvertDelta(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_ConvertDelta_m2D9538AB685663572C4A7E9C7847CF643D453D0E (void);
// 0x0000007F System.Boolean Lean.Touch.LeanScreenDepth::TryConvert(UnityEngine.Vector3&,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_TryConvert_m58F0642957BDC300666C8799D1581F0AD8A2501F (void);
// 0x00000080 System.Boolean Lean.Touch.LeanScreenDepth::Exists(UnityEngine.GameObject,T&)
// 0x00000081 System.Boolean Lean.Touch.LeanScreenDepth::IsChildOf(UnityEngine.Transform,UnityEngine.Transform)
extern void LeanScreenDepth_IsChildOf_mC35631C16DEEE1F7EFC8ECBC21EDBCA27A528B80 (void);
// 0x00000082 System.Void Lean.Touch.LeanScreenDepth::.cctor()
extern void LeanScreenDepth__cctor_mCD3C54755FDADC247C7EBF078FD7CE7861D05903 (void);
// 0x00000083 System.Void Lean.Touch.LeanSelect::TrySelect(Lean.Touch.LeanFinger,UnityEngine.Component,UnityEngine.Vector3)
extern void LeanSelect_TrySelect_mC5BFAB821F418D52412960D61D87FCD19403D9CB (void);
// 0x00000084 System.Void Lean.Touch.LeanSelect::Select(Lean.Touch.LeanFinger,Lean.Touch.LeanSelectable)
extern void LeanSelect_Select_m9EB6EB9A8AF98B67048CC84B1CA0EE44AB048B38 (void);
// 0x00000085 System.Void Lean.Touch.LeanSelect::DeselectAll()
extern void LeanSelect_DeselectAll_mD7C138D0FE111FF4E6642CEF15CB6EABF535FE23 (void);
// 0x00000086 System.Void Lean.Touch.LeanSelect::OnEnable()
extern void LeanSelect_OnEnable_mFC12599311AB3491E9B76535F365627062D03F34 (void);
// 0x00000087 System.Void Lean.Touch.LeanSelect::OnDisable()
extern void LeanSelect_OnDisable_mB86CECE9D9104908072817644727581B86247AAB (void);
// 0x00000088 System.Void Lean.Touch.LeanSelect::.ctor()
extern void LeanSelect__ctor_m5AC05333119825B1410B5783C7CE24B9A82FE47F (void);
// 0x00000089 System.Void Lean.Touch.LeanSelect::.cctor()
extern void LeanSelect__cctor_m3552C61DEBD00681AAF40B42D96F6E23FD10FFE2 (void);
// 0x0000008A System.Void Lean.Touch.LeanSelectBase::SelectStartScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelectBase_SelectStartScreenPosition_m8E5D4B117C100257A5301B618E9BB267ADF13549 (void);
// 0x0000008B System.Void Lean.Touch.LeanSelectBase::SelectScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelectBase_SelectScreenPosition_m934789C260AD1664FDA85624DC2A76FE8C0326DC (void);
// 0x0000008C System.Void Lean.Touch.LeanSelectBase::SelectScreenPosition(Lean.Touch.LeanFinger,UnityEngine.Vector2)
extern void LeanSelectBase_SelectScreenPosition_m79BF224A6471B044145023A4D154745ABB3A022C (void);
// 0x0000008D System.Void Lean.Touch.LeanSelectBase::TrySelect(Lean.Touch.LeanFinger,UnityEngine.Component,UnityEngine.Vector3)
// 0x0000008E System.Void Lean.Touch.LeanSelectBase::TryGetComponent(Lean.Touch.LeanSelectBase/SelectType,UnityEngine.Vector2,UnityEngine.Component&,UnityEngine.Vector3&)
extern void LeanSelectBase_TryGetComponent_mE4890DFE413B04A3890E124326DD3B63087E1F88 (void);
// 0x0000008F System.Int32 Lean.Touch.LeanSelectBase::GetClosestRaycastHitsIndex(System.Int32)
extern void LeanSelectBase_GetClosestRaycastHitsIndex_mB062EBABDCA90A4FC8DD056011064213A92918A6 (void);
// 0x00000090 UnityEngine.Vector2 Lean.Touch.LeanSelectBase::GetScreenPoint(UnityEngine.Camera,UnityEngine.Transform)
extern void LeanSelectBase_GetScreenPoint_m1A5F3E511AD4F5DB65546390C0AB03A19DC9F815 (void);
// 0x00000091 System.Void Lean.Touch.LeanSelectBase::.ctor()
extern void LeanSelectBase__ctor_m21C9F84FD06B0204E845F8CBCC1030887CAD2366 (void);
// 0x00000092 System.Void Lean.Touch.LeanSelectBase::.cctor()
extern void LeanSelectBase__cctor_m89AC8B3785351C2E89C6B4B5874C33F3D3397F94 (void);
// 0x00000093 System.Void Lean.Touch.LeanSelectable::add_OnEnableGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_add_OnEnableGlobal_mA4AE6B61F64C69E38D3D4C08530615118074FE27 (void);
// 0x00000094 System.Void Lean.Touch.LeanSelectable::remove_OnEnableGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_remove_OnEnableGlobal_m071D22DA519C90595CAAE7FC2D250CEC46A94952 (void);
// 0x00000095 System.Void Lean.Touch.LeanSelectable::add_OnDisableGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_add_OnDisableGlobal_m310A3835E66C461E943E5F11F889AF2FC2D344EE (void);
// 0x00000096 System.Void Lean.Touch.LeanSelectable::remove_OnDisableGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_remove_OnDisableGlobal_m5A63CC00B20D746B1D1775CF837FFC39F93735C7 (void);
// 0x00000097 System.Void Lean.Touch.LeanSelectable::add_OnSelectGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectGlobal_m49B4735AEFC00D613C3463A5AB2CF4A229D70945 (void);
// 0x00000098 System.Void Lean.Touch.LeanSelectable::remove_OnSelectGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectGlobal_m3398464478E0766B8B6D913FED4356B41FF08032 (void);
// 0x00000099 System.Void Lean.Touch.LeanSelectable::add_OnSelectSetGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectSetGlobal_mD6E364EB80C79228D9F2A108E7B0272DAFD351AE (void);
// 0x0000009A System.Void Lean.Touch.LeanSelectable::remove_OnSelectSetGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectSetGlobal_m0C4118B0CA52F87594B9872C5BF1BFBB739B9EB9 (void);
// 0x0000009B System.Void Lean.Touch.LeanSelectable::add_OnSelectUpGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectUpGlobal_m217EB0AC8EA82F5C2AE056A75CBD9467DA467E11 (void);
// 0x0000009C System.Void Lean.Touch.LeanSelectable::remove_OnSelectUpGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectUpGlobal_mCD19683D78EAD04CCE9FB36ACFBC3B15336D4F3E (void);
// 0x0000009D System.Void Lean.Touch.LeanSelectable::add_OnDeselectGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_add_OnDeselectGlobal_m2E6DE2CFE77446917368463D19A500B5763476D8 (void);
// 0x0000009E System.Void Lean.Touch.LeanSelectable::remove_OnDeselectGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_remove_OnDeselectGlobal_mA530F49ACB9CA9F187F6DEFDEE334B6309965A0A (void);
// 0x0000009F Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelect()
extern void LeanSelectable_get_OnSelect_m458A1A56BE7BAF5A3694E2D4B6C442CA288F9BBA (void);
// 0x000000A0 Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelectUpdate()
extern void LeanSelectable_get_OnSelectUpdate_m2129D6C2B021FC842DCF9F3714982B43C6A9A637 (void);
// 0x000000A1 Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelectUp()
extern void LeanSelectable_get_OnSelectUp_m27EF71C22C5EE13936D03BFB0A46F4455F64798D (void);
// 0x000000A2 UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectable::get_OnDeselect()
extern void LeanSelectable_get_OnDeselect_m61CF88AD67D81724DA3E19D94C2756F5BC8A3CE5 (void);
// 0x000000A3 System.Void Lean.Touch.LeanSelectable::set_IsSelected(System.Boolean)
extern void LeanSelectable_set_IsSelected_mB5F64CA8927EDD0E171E144FC123D1D98A0525A2 (void);
// 0x000000A4 System.Boolean Lean.Touch.LeanSelectable::get_IsSelected()
extern void LeanSelectable_get_IsSelected_m45813D800C6C360758156A2087BEEF66DC708EB3 (void);
// 0x000000A5 System.Boolean Lean.Touch.LeanSelectable::get_IsSelectedRaw()
extern void LeanSelectable_get_IsSelectedRaw_m24B4AB93759291374E9599581F4F17390EDFA1D9 (void);
// 0x000000A6 System.Int32 Lean.Touch.LeanSelectable::get_IsSelectedCount()
extern void LeanSelectable_get_IsSelectedCount_mF084096474A93A1C4FB47B379702843A3CE64AC5 (void);
// 0x000000A7 System.Int32 Lean.Touch.LeanSelectable::get_IsSelectedRawCount()
extern void LeanSelectable_get_IsSelectedRawCount_m61D1D3250A5BAE849B237334868A543B054DFD79 (void);
// 0x000000A8 Lean.Touch.LeanFinger Lean.Touch.LeanSelectable::get_SelectingFinger()
extern void LeanSelectable_get_SelectingFinger_m1251D3B675B3FD2FFB0068D764D586C6D537D60B (void);
// 0x000000A9 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::get_SelectingFingers()
extern void LeanSelectable_get_SelectingFingers_mDB3588A7613F596580B8C9741754AFB1530C1779 (void);
// 0x000000AA System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::GetFingers(System.Boolean,System.Boolean,System.Int32,Lean.Touch.LeanSelectable)
extern void LeanSelectable_GetFingers_m821F744EF77C858C2A6038FAC5DEC77B0372F2F3 (void);
// 0x000000AB System.Void Lean.Touch.LeanSelectable::GetSelected(System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_GetSelected_m58CC9B9D0208F066889B4EE00837E09F3512E0A2 (void);
// 0x000000AC System.Void Lean.Touch.LeanSelectable::Cull(System.Int32)
extern void LeanSelectable_Cull_mF23406C07D76AFCD33F84AFCB3B2232CB2B5CA9D (void);
// 0x000000AD Lean.Touch.LeanSelectable Lean.Touch.LeanSelectable::FindSelectable(Lean.Touch.LeanFinger)
extern void LeanSelectable_FindSelectable_m2B24DCF9FE3D8DD9279209D352DFC9F794AC7BF5 (void);
// 0x000000AE System.Void Lean.Touch.LeanSelectable::ReplaceSelection(Lean.Touch.LeanFinger,System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_ReplaceSelection_mA9381B279E02A200DB4EAB10B7EBAB542EED95FC (void);
// 0x000000AF System.Boolean Lean.Touch.LeanSelectable::IsSelectedBy(Lean.Touch.LeanFinger)
extern void LeanSelectable_IsSelectedBy_mA2CE79FA7AFEF426918C16C2C58658C789B9F658 (void);
// 0x000000B0 System.Boolean Lean.Touch.LeanSelectable::GetIsSelected(System.Boolean)
extern void LeanSelectable_GetIsSelected_m1DBF0E54DBC45C3188F0C98DDE80D15BDC40E3C2 (void);
// 0x000000B1 System.Void Lean.Touch.LeanSelectable::Select()
extern void LeanSelectable_Select_m6C323F3F39CEFE669BDFAD2EF3768000BE73C9EC (void);
// 0x000000B2 System.Void Lean.Touch.LeanSelectable::Select(Lean.Touch.LeanFinger)
extern void LeanSelectable_Select_mFE2E8E0481A51153E75BFBC02192ABE0DF431601 (void);
// 0x000000B3 System.Void Lean.Touch.LeanSelectable::Deselect()
extern void LeanSelectable_Deselect_mC59C461CA95C8180800A3846FF2FBB12E1928316 (void);
// 0x000000B4 System.Void Lean.Touch.LeanSelectable::DeselectAll()
extern void LeanSelectable_DeselectAll_m374D612DC7A2A6EE9028ACC4672FAB5D6A5B2E73 (void);
// 0x000000B5 System.Void Lean.Touch.LeanSelectable::OnEnable()
extern void LeanSelectable_OnEnable_mBCAB65E5423DEEE77DA3D497C2C0900AD042975B (void);
// 0x000000B6 System.Void Lean.Touch.LeanSelectable::OnDisable()
extern void LeanSelectable_OnDisable_m815F9EFEE11B80BE17968A13442241971C26A2E0 (void);
// 0x000000B7 System.Void Lean.Touch.LeanSelectable::BuildTempSelectables(Lean.Touch.LeanFinger)
extern void LeanSelectable_BuildTempSelectables_m87DEB12929F673A78A75F0ED23B54CDFF004FA2E (void);
// 0x000000B8 System.Void Lean.Touch.LeanSelectable::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerUpdate_m6965B7DF88B37CC0F440F8B4E80B320F8152BF65 (void);
// 0x000000B9 System.Boolean Lean.Touch.LeanSelectable::get_AnyFingersSet()
extern void LeanSelectable_get_AnyFingersSet_mFBBED3F411BF5141C09AD054374865B89FA1573B (void);
// 0x000000BA System.Void Lean.Touch.LeanSelectable::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerUp_mEEBA33685A88C3245FD0B87F556EE575E796855E (void);
// 0x000000BB System.Void Lean.Touch.LeanSelectable::HandleFingerInactive(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerInactive_mE7E0D8E81F29695C64BB3B6CF732FFCB5D0DE5FC (void);
// 0x000000BC System.Void Lean.Touch.LeanSelectable::.ctor()
extern void LeanSelectable__ctor_mDB34A260912A9BA62B628E399C4C49CD8278434B (void);
// 0x000000BD System.Void Lean.Touch.LeanSelectable::.cctor()
extern void LeanSelectable__cctor_m10628DBF9367FA478BA466A70DEF89EFA16F8FF8 (void);
// 0x000000BE Lean.Touch.LeanSelectable Lean.Touch.LeanSelectableBehaviour::get_Selectable()
extern void LeanSelectableBehaviour_get_Selectable_m81BCDA355E315B389E391E2496C4F6B6940FC393 (void);
// 0x000000BF System.Void Lean.Touch.LeanSelectableBehaviour::Register()
extern void LeanSelectableBehaviour_Register_mAEFA703242375C095EDABBF5F2337B94C258F57C (void);
// 0x000000C0 System.Void Lean.Touch.LeanSelectableBehaviour::Register(Lean.Touch.LeanSelectable)
extern void LeanSelectableBehaviour_Register_m11E06AC483169D046681D12DA9041898EB46A099 (void);
// 0x000000C1 System.Void Lean.Touch.LeanSelectableBehaviour::Unregister()
extern void LeanSelectableBehaviour_Unregister_mA35B24DEA8F7FAEFB63F303CE126915DF31DAEDC (void);
// 0x000000C2 System.Void Lean.Touch.LeanSelectableBehaviour::OnEnable()
extern void LeanSelectableBehaviour_OnEnable_m9384AAAFF5046D077EFB3CA0DD5974930F53D05B (void);
// 0x000000C3 System.Void Lean.Touch.LeanSelectableBehaviour::Start()
extern void LeanSelectableBehaviour_Start_m9BBCA492A9EFEEE4405B21716586E3E4A7231E02 (void);
// 0x000000C4 System.Void Lean.Touch.LeanSelectableBehaviour::OnDisable()
extern void LeanSelectableBehaviour_OnDisable_mDCA81D89C7B9D0C200D3CF0293463D0D5B01FC8C (void);
// 0x000000C5 System.Void Lean.Touch.LeanSelectableBehaviour::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableBehaviour_OnSelect_m13039731E3B66DFD0029DD1EF5259A2CF0A4D9CA (void);
// 0x000000C6 System.Void Lean.Touch.LeanSelectableBehaviour::OnSelectUp(Lean.Touch.LeanFinger)
extern void LeanSelectableBehaviour_OnSelectUp_mB4BB14046FBEA3B4955C1CAC48A13C2050AF6A0A (void);
// 0x000000C7 System.Void Lean.Touch.LeanSelectableBehaviour::OnDeselect()
extern void LeanSelectableBehaviour_OnDeselect_m5B7AFA42C14EB79671B8C506F5F408C838C04552 (void);
// 0x000000C8 System.Void Lean.Touch.LeanSelectableBehaviour::.ctor()
extern void LeanSelectableBehaviour__ctor_mAD30ECF856963F95ADE2F5CFFD88D04ED9B28477 (void);
// 0x000000C9 System.Void Lean.Touch.LeanSelectableGraphicColor::Awake()
extern void LeanSelectableGraphicColor_Awake_m08DE4F0B054BF6E88ED70F605E16DF25B5BCADAB (void);
// 0x000000CA System.Void Lean.Touch.LeanSelectableGraphicColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableGraphicColor_OnSelect_m06AD85DF68BC5A6F3AC578B46B6FDC8FC8664231 (void);
// 0x000000CB System.Void Lean.Touch.LeanSelectableGraphicColor::OnDeselect()
extern void LeanSelectableGraphicColor_OnDeselect_m0D451FC3CEAF872AB61F5AE4B48A57394910D3F2 (void);
// 0x000000CC System.Void Lean.Touch.LeanSelectableGraphicColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableGraphicColor_ChangeColor_m0B02F0C1FAAF3D557B937D7D447C7CA7D1CE5A74 (void);
// 0x000000CD System.Void Lean.Touch.LeanSelectableGraphicColor::.ctor()
extern void LeanSelectableGraphicColor__ctor_mB1A829691CDB1A982043A0397C0BBB225282E86D (void);
// 0x000000CE System.Void Lean.Touch.LeanSelectableRendererColor::Awake()
extern void LeanSelectableRendererColor_Awake_mF5A1124D509C2A60F88A6B26068B0BE62E9C0852 (void);
// 0x000000CF System.Void Lean.Touch.LeanSelectableRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableRendererColor_OnSelect_m1970E0D894D9C099306D4B83671240601AD16A3E (void);
// 0x000000D0 System.Void Lean.Touch.LeanSelectableRendererColor::OnDeselect()
extern void LeanSelectableRendererColor_OnDeselect_m51F70C3B3A052D38DDF089A2CDD730F9A0AB99C8 (void);
// 0x000000D1 System.Void Lean.Touch.LeanSelectableRendererColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableRendererColor_ChangeColor_m40097FFBCAE29F63A697EFE48F40DD32FF3F56F6 (void);
// 0x000000D2 System.Void Lean.Touch.LeanSelectableRendererColor::.ctor()
extern void LeanSelectableRendererColor__ctor_m846D28FA84523E5202F59826EE1637E6A96C56C6 (void);
// 0x000000D3 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::Awake()
extern void LeanSelectableSpriteRendererColor_Awake_mB0CE0D8C5EE03B5E5ACDF8112571BA1E0A5B58AD (void);
// 0x000000D4 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableSpriteRendererColor_OnSelect_mD4593ABB988E6D34D50BF50746FC7DE154EE9BBE (void);
// 0x000000D5 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnDeselect()
extern void LeanSelectableSpriteRendererColor_OnDeselect_m9C83E0E59BE4ACBD19042078824290DBC6C8D655 (void);
// 0x000000D6 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableSpriteRendererColor_ChangeColor_m03E4A9F088936E02CAF3C902CC3514789F1DEA0E (void);
// 0x000000D7 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::.ctor()
extern void LeanSelectableSpriteRendererColor__ctor_m86FC5F91F62E0B5ED8FA1F570133FD05F1E581DC (void);
// 0x000000D8 Lean.Touch.LeanSwipeBase/LeanFingerEvent Lean.Touch.LeanSwipeBase::get_OnFinger()
extern void LeanSwipeBase_get_OnFinger_m089DA3BF574B349393275C5E0680FA7AD0A941A7 (void);
// 0x000000D9 Lean.Touch.LeanSwipeBase/Vector2Event Lean.Touch.LeanSwipeBase::get_OnDelta()
extern void LeanSwipeBase_get_OnDelta_m34CF75E4F7C48424B8DE7022C3034DE6AE2D9993 (void);
// 0x000000DA Lean.Touch.LeanSwipeBase/FloatEvent Lean.Touch.LeanSwipeBase::get_OnDistance()
extern void LeanSwipeBase_get_OnDistance_m893B0BF23AB8C1CEFAEB74831AFE568464735F82 (void);
// 0x000000DB Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldFrom()
extern void LeanSwipeBase_get_OnWorldFrom_mF79B90B70DF37167092DEB3E266F1E18AAA831CA (void);
// 0x000000DC Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldTo()
extern void LeanSwipeBase_get_OnWorldTo_mAFEF7B305825D6CD79E9CF6AF2D038F9EFFF485D (void);
// 0x000000DD Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldDelta()
extern void LeanSwipeBase_get_OnWorldDelta_m5795E00F837569385378684B733423B113BC26D1 (void);
// 0x000000DE Lean.Touch.LeanSwipeBase/Vector3Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldFromTo()
extern void LeanSwipeBase_get_OnWorldFromTo_m4FC410EB76914CE5E8F9792AEADC8B44691C98B0 (void);
// 0x000000DF System.Boolean Lean.Touch.LeanSwipeBase::AngleIsValid(UnityEngine.Vector2)
extern void LeanSwipeBase_AngleIsValid_mB681B627ED3D397118C65AD2EE92C830F30CD546 (void);
// 0x000000E0 System.Void Lean.Touch.LeanSwipeBase::HandleFingerSwipe(Lean.Touch.LeanFinger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanSwipeBase_HandleFingerSwipe_mEF2CEB93D880577C0FB85659345BDBA59E5DBF4E (void);
// 0x000000E1 System.Void Lean.Touch.LeanSwipeBase::.ctor()
extern void LeanSwipeBase__ctor_m0E088D22B3C151DD0C03DF38801364D812C66B75 (void);
// 0x000000E2 System.Void Lean.Touch.LeanTwistRotate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotate_AddFinger_m06198BA0BE59F93B3F239D56053A8EE8A4F794EF (void);
// 0x000000E3 System.Void Lean.Touch.LeanTwistRotate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotate_RemoveFinger_mC099EC7A6D775872D495E8D3605092D2F9186246 (void);
// 0x000000E4 System.Void Lean.Touch.LeanTwistRotate::RemoveAllFingers()
extern void LeanTwistRotate_RemoveAllFingers_m94278F25735B8FBB27C1CE815D061D2CB4A77BC3 (void);
// 0x000000E5 System.Void Lean.Touch.LeanTwistRotate::Awake()
extern void LeanTwistRotate_Awake_m343EF7B18DE6A844AF854CBF0725BF0BE4284B50 (void);
// 0x000000E6 System.Void Lean.Touch.LeanTwistRotate::Update()
extern void LeanTwistRotate_Update_mD9917297B2AE6FD68EA8F07C2649E95C5185F76B (void);
// 0x000000E7 System.Void Lean.Touch.LeanTwistRotate::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanTwistRotate_TranslateUI_mC1DC5900812C204BE8812CA8E1782BF1E2A6CF9F (void);
// 0x000000E8 System.Void Lean.Touch.LeanTwistRotate::Translate(System.Single,UnityEngine.Vector2)
extern void LeanTwistRotate_Translate_m9F62FEB003B1E95FE3F4F1066005365C9A17E018 (void);
// 0x000000E9 System.Void Lean.Touch.LeanTwistRotate::RotateUI(System.Single)
extern void LeanTwistRotate_RotateUI_m2A8ADFB4A8376D9150FE673CE66812CF9C1E5104 (void);
// 0x000000EA System.Void Lean.Touch.LeanTwistRotate::Rotate(System.Single)
extern void LeanTwistRotate_Rotate_mC211DFAB5207340FA76A74E00EE79443ED09A9E3 (void);
// 0x000000EB System.Void Lean.Touch.LeanTwistRotate::.ctor()
extern void LeanTwistRotate__ctor_m7ED74A8ADB3616000C647E21F79331B6D2D0ECAB (void);
// 0x000000EC System.Void Lean.Touch.LeanTwistRotateAxis::Start()
extern void LeanTwistRotateAxis_Start_m44B782C5209DD47386C4458A2EBB521787E91BBB (void);
// 0x000000ED System.Void Lean.Touch.LeanTwistRotateAxis::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotateAxis_AddFinger_m8D4B6FBE644BCBDB22345CF8C7B33C975108C394 (void);
// 0x000000EE System.Void Lean.Touch.LeanTwistRotateAxis::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotateAxis_RemoveFinger_mEECE240BA420BFBC19EC9F1BA20CECA8ED203139 (void);
// 0x000000EF System.Void Lean.Touch.LeanTwistRotateAxis::RemoveAllFingers()
extern void LeanTwistRotateAxis_RemoveAllFingers_m04B2EF72EC3DA09B5AA808A9605534FE40B886B6 (void);
// 0x000000F0 System.Void Lean.Touch.LeanTwistRotateAxis::Awake()
extern void LeanTwistRotateAxis_Awake_m4B4B8940E9CB4E7376B8E81C86337025E962CE97 (void);
// 0x000000F1 System.Void Lean.Touch.LeanTwistRotateAxis::Update()
extern void LeanTwistRotateAxis_Update_m0B1314061FBFAC43E3B803E49576FA923016529B (void);
// 0x000000F2 System.Void Lean.Touch.LeanTwistRotateAxis::.ctor()
extern void LeanTwistRotateAxis__ctor_m1D5C67A5228B40CAD97D2BEA27A72D225AB70AAB (void);
// 0x000000F3 System.Boolean Lean.Touch.LeanFinger::get_IsActive()
extern void LeanFinger_get_IsActive_m7B5C0C70E780459E6CFAD6EA7F1EDC329B4F301F (void);
// 0x000000F4 System.Single Lean.Touch.LeanFinger::get_SnapshotDuration()
extern void LeanFinger_get_SnapshotDuration_m00EA679A6256D6E6C3188B5C87547B21DFE69E6F (void);
// 0x000000F5 System.Boolean Lean.Touch.LeanFinger::get_IsOverGui()
extern void LeanFinger_get_IsOverGui_m4FADA1E47BCEF005D81B24DD96514D05F0D6EA4E (void);
// 0x000000F6 System.Boolean Lean.Touch.LeanFinger::get_Down()
extern void LeanFinger_get_Down_m63337A38DF2CB3F638839BB772E39F9F37ED197F (void);
// 0x000000F7 System.Boolean Lean.Touch.LeanFinger::get_Up()
extern void LeanFinger_get_Up_mBA17A3A5510FB43E81D0FE93C8DB139C2A5B204C (void);
// 0x000000F8 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScreenDelta()
extern void LeanFinger_get_LastSnapshotScreenDelta_m3DD9E1CCC4ABB919ABC24C9DEC1D017672EB27A7 (void);
// 0x000000F9 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScaledDelta()
extern void LeanFinger_get_LastSnapshotScaledDelta_mA7BF830AB9635286F4520D88271D93F42A1750C1 (void);
// 0x000000FA UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScreenDelta()
extern void LeanFinger_get_ScreenDelta_m805424DD0B14A8B6D85319A7EB62F776A9CE7422 (void);
// 0x000000FB UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScaledDelta()
extern void LeanFinger_get_ScaledDelta_mEDB3129AAC50FC83726BE26746E532E3CDCE094F (void);
// 0x000000FC UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScreenDelta()
extern void LeanFinger_get_SwipeScreenDelta_mD96829E19F10C3325E4010B314C658E2B5FE6E36 (void);
// 0x000000FD UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScaledDelta()
extern void LeanFinger_get_SwipeScaledDelta_mFFCF0BEE998EA39F128C137E029C6E078A3D84FB (void);
// 0x000000FE UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSmoothScreenPosition(System.Single)
extern void LeanFinger_GetSmoothScreenPosition_m0DCD6CA9CE517AB8BD15E3C269A21AB3DF89E159 (void);
// 0x000000FF System.Single Lean.Touch.LeanFinger::get_SmoothScreenPositionDelta()
extern void LeanFinger_get_SmoothScreenPositionDelta_mA7EB0B2947DE1781B20112D58F0283DB152B2641 (void);
// 0x00000100 UnityEngine.Ray Lean.Touch.LeanFinger::GetRay(UnityEngine.Camera)
extern void LeanFinger_GetRay_m055289FE695582FF57C9A381C28DF0F7580C1DD5 (void);
// 0x00000101 UnityEngine.Ray Lean.Touch.LeanFinger::GetStartRay(UnityEngine.Camera)
extern void LeanFinger_GetStartRay_mD8077059062A44A7186809C5DD592F42947F8600 (void);
// 0x00000102 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenDelta(System.Single)
extern void LeanFinger_GetSnapshotScreenDelta_mBE8E710D7A01EBA901D03B03475F2AE3E9A535F1 (void);
// 0x00000103 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScaledDelta(System.Single)
extern void LeanFinger_GetSnapshotScaledDelta_m167B50111800864822CE441DEAB1F0955954402A (void);
// 0x00000104 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenPosition(System.Single)
extern void LeanFinger_GetSnapshotScreenPosition_mBB2F679EAB8649CEF17151548C03CEED1D4FB4EC (void);
// 0x00000105 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetSnapshotWorldPosition(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetSnapshotWorldPosition_mF8EFFCC1E0F0AF62150EDA2A2DCD0EA65590C974 (void);
// 0x00000106 System.Single Lean.Touch.LeanFinger::GetRadians(UnityEngine.Vector2)
extern void LeanFinger_GetRadians_mE40D50BCC0D96C679C5680EC06C5566C3898BA44 (void);
// 0x00000107 System.Single Lean.Touch.LeanFinger::GetDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDegrees_m5D305CBC7A07C80A8903EF796CBD01F8AC128946 (void);
// 0x00000108 System.Single Lean.Touch.LeanFinger::GetLastRadians(UnityEngine.Vector2)
extern void LeanFinger_GetLastRadians_mD38E79A7B6288220CFAA7863467FF247D7697E4C (void);
// 0x00000109 System.Single Lean.Touch.LeanFinger::GetLastDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetLastDegrees_m0E4A0CC2BC6AB1F7BEF175E97903931A0D0DFCE5 (void);
// 0x0000010A System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_mB3EAF87D832EBC7EEC8527CA909CD14EA4C238C1 (void);
// 0x0000010B System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_m8AD39852759E6C5C402FCD01D84184A35234104C (void);
// 0x0000010C System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_m5551EEF44F06CD804C11E2BE884F551937555E86 (void);
// 0x0000010D System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_m6FA7E89E772A66357B476FAA02D1EE42ED84669C (void);
// 0x0000010E System.Single Lean.Touch.LeanFinger::GetScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScreenDistance_m8AFF803D658761C26B638FFB8BB364A606D17737 (void);
// 0x0000010F System.Single Lean.Touch.LeanFinger::GetScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScaledDistance_mD0C88B654FAA52B3B6326A606600D84C6359E466 (void);
// 0x00000110 System.Single Lean.Touch.LeanFinger::GetLastScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScreenDistance_mF740435B176EDD5F9A8E320AB0961A33C0F31BBF (void);
// 0x00000111 System.Single Lean.Touch.LeanFinger::GetLastScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScaledDistance_m38FA4AAC461BBA58A278F77BEFA30DE4E087279F (void);
// 0x00000112 System.Single Lean.Touch.LeanFinger::GetStartScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScreenDistance_mC7EF17E383354CE99243B99312FA3491BB8FCB23 (void);
// 0x00000113 System.Single Lean.Touch.LeanFinger::GetStartScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScaledDistance_mD2FB957F6AD12683A287709EDB07E269798CFAC7 (void);
// 0x00000114 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetStartWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetStartWorldPosition_mCECBD81DD0353BFA05BE95B0E5CBA636AAF18C6F (void);
// 0x00000115 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetLastWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetLastWorldPosition_mB0310D234AF766B2469EDD3DD6E1F173F0BE8617 (void);
// 0x00000116 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldPosition_m7E26ADEC7D2AE0F5370585A39B4BE4DB85BCC237 (void);
// 0x00000117 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_m56C5F5A4D8043F495EFE939B0AA66399F200FEF8 (void);
// 0x00000118 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_m2DB5442571023BBA2DE0D5D295EF341BC01B14C2 (void);
// 0x00000119 System.Void Lean.Touch.LeanFinger::ClearSnapshots(System.Int32)
extern void LeanFinger_ClearSnapshots_m3D66E683C8F566BD9BFB6A7025E2A8230568BB18 (void);
// 0x0000011A System.Void Lean.Touch.LeanFinger::RecordSnapshot()
extern void LeanFinger_RecordSnapshot_m77E4E23BA5DD226853726EAD61D42CEF434DBDA1 (void);
// 0x0000011B System.Void Lean.Touch.LeanFinger::.ctor()
extern void LeanFinger__ctor_mA0EBF0E1F7705147ED0B962A7E4A7CEEF3B0EDDA (void);
// 0x0000011C UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter()
extern void LeanGesture_GetScreenCenter_mBEF0476CED5FE8BB64F877F4ABF3A67A8F9F1224 (void);
// 0x0000011D UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenCenter_m1DC38D53DB783DF953F11C628DBB526785F8B842 (void);
// 0x0000011E System.Boolean Lean.Touch.LeanGesture::TryGetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenCenter_m6B05B5DD79C77423120ABC8F91EE7BF9962F5265 (void);
// 0x0000011F UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter()
extern void LeanGesture_GetLastScreenCenter_m4E14B6ABA30F7814115D7A9EC1288BC793BF5A78 (void);
// 0x00000120 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenCenter_m5D0898BCD34B34A933D19442B755DF036BDCA895 (void);
// 0x00000121 System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetLastScreenCenter_mA28E2EE3F4731065261811B5A49019EAF0E4720E (void);
// 0x00000122 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter()
extern void LeanGesture_GetStartScreenCenter_m01AECC0B403D0BCFD5F3BE2FC582F9F24A8782C1 (void);
// 0x00000123 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenCenter_m107DFC9CF385B8222188B5E7ECC37C83F104D2DA (void);
// 0x00000124 System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetStartScreenCenter_m4EBFF200C9069D6FCC51C32B5E34412907E4FB3F (void);
// 0x00000125 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta()
extern void LeanGesture_GetScreenDelta_mACDB404102BCA59C31544F8CA56A67D9DDD31208 (void);
// 0x00000126 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDelta_m39E329359BE8F6FEFBCC6D1E9D7511B3A4C98E01 (void);
// 0x00000127 System.Boolean Lean.Touch.LeanGesture::TryGetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenDelta_mB36A7BED7FDBC73303285DB8FEC6D7692FB4903C (void);
// 0x00000128 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta()
extern void LeanGesture_GetScaledDelta_mEF6CE2F578CD9941AB2A59D3A5FBD6B1E905054E (void);
// 0x00000129 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDelta_m839EDC79670B093766CA8FFEE2B818C4ADDFD8D7 (void);
// 0x0000012A System.Boolean Lean.Touch.LeanGesture::TryGetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScaledDelta_mA7CD4F6CAF40334EC0CA95551ECCDBB3E08BF777 (void);
// 0x0000012B UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_m8C098B852420EE97FD5E45D150499D3D9A4CE818 (void);
// 0x0000012C UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_mB2D118B56CE437465850B4ECFD61C46BD79596BD (void);
// 0x0000012D System.Boolean Lean.Touch.LeanGesture::TryGetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Vector3&,UnityEngine.Camera)
extern void LeanGesture_TryGetWorldDelta_m3E3F139CEA1BE81FF41E6ED9A814C8751F272236 (void);
// 0x0000012E System.Single Lean.Touch.LeanGesture::GetScreenDistance()
extern void LeanGesture_GetScreenDistance_m6C92A4B160314A7321B56FF87F5679217426763B (void);
// 0x0000012F System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDistance_m610A8A328FC1EB8D57D5A17E0BAA6DF5028270C3 (void);
// 0x00000130 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScreenDistance_m52B34E72ED87957E64AC2AED4A1F7573025522D9 (void);
// 0x00000131 System.Boolean Lean.Touch.LeanGesture::TryGetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScreenDistance_m23863CEA30992F714E375888644D1821734A6FA6 (void);
// 0x00000132 System.Single Lean.Touch.LeanGesture::GetScaledDistance()
extern void LeanGesture_GetScaledDistance_m98B93AF6E44C1447E77A6EEEE1CC0BADCDFFD26E (void);
// 0x00000133 System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDistance_mF7A95B44E7803F4A7B0F788543A6844E6C9D1486 (void);
// 0x00000134 System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScaledDistance_mDF74EACB2D5AC58470A92D6DBC3A43A753F72BD9 (void);
// 0x00000135 System.Boolean Lean.Touch.LeanGesture::TryGetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScaledDistance_mC664C23A094DE14529A29912704FA4DFB959C2C8 (void);
// 0x00000136 System.Single Lean.Touch.LeanGesture::GetLastScreenDistance()
extern void LeanGesture_GetLastScreenDistance_mA8A4D8ED507B74A967409948A7E714186D6837BD (void);
// 0x00000137 System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenDistance_mDE8490CC6D2376181647664FCAA77E02563823E0 (void);
// 0x00000138 System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScreenDistance_m1453B1FF8B2217B8419A06B9ADB11BBE36A58971 (void);
// 0x00000139 System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScreenDistance_m17A3C6F0CEBE3377C970F458399BB7FF79A65D04 (void);
// 0x0000013A System.Single Lean.Touch.LeanGesture::GetLastScaledDistance()
extern void LeanGesture_GetLastScaledDistance_m4AAA8EDC82C58600D3F1B10CD5150883B8107E2D (void);
// 0x0000013B System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScaledDistance_m5B75BCA7A0612F095A8F97DB298BEC3361C2A18F (void);
// 0x0000013C System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScaledDistance_m9A507CA3FED364435C5356B76A7A1F40147272ED (void);
// 0x0000013D System.Boolean Lean.Touch.LeanGesture::TryGetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScaledDistance_m9F9E2260F51FB3F26A032CCC397642F3DAB218C6 (void);
// 0x0000013E System.Single Lean.Touch.LeanGesture::GetStartScreenDistance()
extern void LeanGesture_GetStartScreenDistance_mB2CA2653DB9CA9BE250348AAFC6701589530F6AF (void);
// 0x0000013F System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenDistance_mC0EA85608522FF162FD6E9F312C79ECFCA19AC98 (void);
// 0x00000140 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScreenDistance_mDD29D9ACE3D03F7318060204EB05D7F43CE1BD30 (void);
// 0x00000141 System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScreenDistance_m7894CF4766FC28B51D1BEBEB02C5521732A8539B (void);
// 0x00000142 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance()
extern void LeanGesture_GetStartScaledDistance_mBE1FF8BB4188B541E2B42B039CEA8FE76EECA2F9 (void);
// 0x00000143 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScaledDistance_m43AAB7E46D2833EDE932B93DAE960BC46F485DFE (void);
// 0x00000144 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScaledDistance_mFD7F591A3F5B3A308A557EBBDD94F46A0D13C62C (void);
// 0x00000145 System.Boolean Lean.Touch.LeanGesture::TryGetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScaledDistance_m8405F3CFF4E15AB0EEC647500BB4431D38DC3555 (void);
// 0x00000146 System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Single)
extern void LeanGesture_GetPinchScale_mD6168FFE37DC887B5803DD979C5DDE0E93E1606B (void);
// 0x00000147 System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchScale_mBFF1DC81C5B25C59831945FFF2DDE0909F907753 (void);
// 0x00000148 System.Boolean Lean.Touch.LeanGesture::TryGetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchScale_m432860AFE690B9E49C26E56B381C4BBAABD977FD (void);
// 0x00000149 System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Single)
extern void LeanGesture_GetPinchRatio_mDA8ED0D67FB5C4CBFC56A1C665551A6E0F1F9B2F (void);
// 0x0000014A System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchRatio_m9FEA853F6A3F9AF930361BF20310C153D2828F5F (void);
// 0x0000014B System.Boolean Lean.Touch.LeanGesture::TryGetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchRatio_mAAF982BC35CD0CE9C0FC97238151C2FD7DD82807 (void);
// 0x0000014C System.Single Lean.Touch.LeanGesture::GetTwistDegrees()
extern void LeanGesture_GetTwistDegrees_m18FB7974DA7A7721E2824AD3193112C213726765 (void);
// 0x0000014D System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistDegrees_m5A181D7665683D6BDD079393230F57215808092E (void);
// 0x0000014E System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistDegrees_mA3780F085953F752018F41F92E88A09E8165334F (void);
// 0x0000014F System.Boolean Lean.Touch.LeanGesture::TryGetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistDegrees_m3D406A35E046BAE35811D9358348942391429264 (void);
// 0x00000150 System.Single Lean.Touch.LeanGesture::GetTwistRadians()
extern void LeanGesture_GetTwistRadians_m72BA39E88552A9306F7C2C6F0069AF6153957758 (void);
// 0x00000151 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistRadians_m8A14814AF562024C9A965AB8649692527E200523 (void);
// 0x00000152 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistRadians_m2223C606D6D6ED508B716E624A79242D8D457FF6 (void);
// 0x00000153 System.Boolean Lean.Touch.LeanGesture::TryGetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistRadians_mC0F3EFA57BFECDEFE2E2B602D6CCC5CF7FB0E2B0 (void);
// 0x00000154 UnityEngine.Vector3 Lean.Touch.LeanSnapshot::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanSnapshot_GetWorldPosition_mE02A4A0650D7E14F97CD824EB557B7BC953E8FE6 (void);
// 0x00000155 Lean.Touch.LeanSnapshot Lean.Touch.LeanSnapshot::Pop()
extern void LeanSnapshot_Pop_m6E8E3B015721A7277ABEAF88B3D9092FABEFE989 (void);
// 0x00000156 System.Boolean Lean.Touch.LeanSnapshot::TryGetScreenPosition(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetScreenPosition_m76796289A21DB0DEA1327510B5DC21B3B4D2FE62 (void);
// 0x00000157 System.Boolean Lean.Touch.LeanSnapshot::TryGetSnapshot(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Int32,System.Single&,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetSnapshot_mDAA5E437929A25F41639BE1EC5135465B5299498 (void);
// 0x00000158 System.Int32 Lean.Touch.LeanSnapshot::GetLowerIndex(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single)
extern void LeanSnapshot_GetLowerIndex_mF5B21BC32FC91F245DDFB277ACBF412EE3A02EBC (void);
// 0x00000159 System.Void Lean.Touch.LeanSnapshot::.ctor()
extern void LeanSnapshot__ctor_m13D33A7A17AFD90CB14AF79835C772694E82DE4B (void);
// 0x0000015A System.Void Lean.Touch.LeanSnapshot::.cctor()
extern void LeanSnapshot__cctor_mDAA2C4CEE81A5011E3ADC52219AD43F188F575F6 (void);
// 0x0000015B System.Void Lean.Touch.LeanTouch::add_OnFingerDown(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerDown_m2446320CDA24F4B2FFD2B51578AE4F75A4D1AF6A (void);
// 0x0000015C System.Void Lean.Touch.LeanTouch::remove_OnFingerDown(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerDown_mBF0212170B21C64B9CBA9370B6B801E03C949072 (void);
// 0x0000015D System.Void Lean.Touch.LeanTouch::add_OnFingerUpdate(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerUpdate_mE45099FD601D90608AD9751259D19903E10D5D44 (void);
// 0x0000015E System.Void Lean.Touch.LeanTouch::remove_OnFingerUpdate(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerUpdate_m7D9EF807B234ACDDEE861D5796A576759FF27815 (void);
// 0x0000015F System.Void Lean.Touch.LeanTouch::add_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerUp_mEF2096577CDA9B4D199625D0F8C60609612D4FE4 (void);
// 0x00000160 System.Void Lean.Touch.LeanTouch::remove_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerUp_mA4A5415C54D5C2E49F9A423940E92D1C2D9A4DF6 (void);
// 0x00000161 System.Void Lean.Touch.LeanTouch::add_OnFingerOld(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerOld_mC7FBE9821BC1BA0B142E1326C2E1EE89BF9EC708 (void);
// 0x00000162 System.Void Lean.Touch.LeanTouch::remove_OnFingerOld(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerOld_mAB896E689C7D034BD745175D2C5417802D2AC2B6 (void);
// 0x00000163 System.Void Lean.Touch.LeanTouch::add_OnFingerTap(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerTap_mED8640F02B94B9F08E1A1D2F62B8B41904F9BC2A (void);
// 0x00000164 System.Void Lean.Touch.LeanTouch::remove_OnFingerTap(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerTap_m444AEA0604B710058DE2690AF9C72ED75D0E0D20 (void);
// 0x00000165 System.Void Lean.Touch.LeanTouch::add_OnFingerSwipe(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerSwipe_mF3672F6EA7557D61064EE33F8A1E3222097D7423 (void);
// 0x00000166 System.Void Lean.Touch.LeanTouch::remove_OnFingerSwipe(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerSwipe_m8535FB791A56F635E7C84F48F5BA0D8536F0B7C1 (void);
// 0x00000167 System.Void Lean.Touch.LeanTouch::add_OnGesture(System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>)
extern void LeanTouch_add_OnGesture_m174D3E742EBAB9BE94DF90D51AD91E13EB8BD0B0 (void);
// 0x00000168 System.Void Lean.Touch.LeanTouch::remove_OnGesture(System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>)
extern void LeanTouch_remove_OnGesture_mF6BEF7D30585383DA48137F701F2869D4D6E2E6F (void);
// 0x00000169 System.Void Lean.Touch.LeanTouch::add_OnFingerExpired(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerExpired_m90CFBD7BF361027A4550FB815129A8F2C980BF4B (void);
// 0x0000016A System.Void Lean.Touch.LeanTouch::remove_OnFingerExpired(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerExpired_m88B3683F171D539E80029A8FA978F0CCFA23A7FC (void);
// 0x0000016B System.Void Lean.Touch.LeanTouch::add_OnFingerInactive(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerInactive_m3929851860A96503E1DD7D99F1DDCE3A2586598E (void);
// 0x0000016C System.Void Lean.Touch.LeanTouch::remove_OnFingerInactive(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerInactive_m1DC16F3BACD8E0611B3E2EE67F3B5981566B0479 (void);
// 0x0000016D System.Single Lean.Touch.LeanTouch::get_CurrentTapThreshold()
extern void LeanTouch_get_CurrentTapThreshold_mFAC0500D58A166B0E8F6E61DD412CD893872249C (void);
// 0x0000016E System.Single Lean.Touch.LeanTouch::get_CurrentSwipeThreshold()
extern void LeanTouch_get_CurrentSwipeThreshold_mE095832573A6DBAB2F50686AF1BD416126ABD753 (void);
// 0x0000016F System.Int32 Lean.Touch.LeanTouch::get_CurrentReferenceDpi()
extern void LeanTouch_get_CurrentReferenceDpi_m94CF91557AE597F7AD9976B64285EDA51B67A1F3 (void);
// 0x00000170 UnityEngine.LayerMask Lean.Touch.LeanTouch::get_CurrentGuiLayers()
extern void LeanTouch_get_CurrentGuiLayers_m5BD1673C5183C16B067C6508EBAB21BAF8F38857 (void);
// 0x00000171 Lean.Touch.LeanTouch Lean.Touch.LeanTouch::get_Instance()
extern void LeanTouch_get_Instance_mD14E64FF0BDAC5B20B90AF0A934F69C3BE31FCB4 (void);
// 0x00000172 System.Single Lean.Touch.LeanTouch::get_ScalingFactor()
extern void LeanTouch_get_ScalingFactor_m617EED84247952F5B8E628DF758059459DC4420D (void);
// 0x00000173 System.Single Lean.Touch.LeanTouch::get_ScreenFactor()
extern void LeanTouch_get_ScreenFactor_mBA745E8A83787B715B721BEDCE05D1B8456E933D (void);
// 0x00000174 System.Boolean Lean.Touch.LeanTouch::get_GuiInUse()
extern void LeanTouch_get_GuiInUse_mEFD8587451F7ABA0795C92330A4C012A9174B848 (void);
// 0x00000175 System.Boolean Lean.Touch.LeanTouch::PointOverGui(UnityEngine.Vector2)
extern void LeanTouch_PointOverGui_m9BC719836CB96EBD1A2882138DEB7EF0603C2674 (void);
// 0x00000176 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2)
extern void LeanTouch_RaycastGui_m5A1F511E931D590797E344306DE07FB70CEB0A49 (void);
// 0x00000177 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2,UnityEngine.LayerMask)
extern void LeanTouch_RaycastGui_m26CCF03838DCD8C65042816D7DED38652E8CA1EA (void);
// 0x00000178 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::GetFingers(System.Boolean,System.Boolean,System.Int32)
extern void LeanTouch_GetFingers_mE345FF052E046CB3163610E73E33B1B4FED4DDC6 (void);
// 0x00000179 System.Void Lean.Touch.LeanTouch::SimulateTap(UnityEngine.Vector2,System.Single,System.Int32)
extern void LeanTouch_SimulateTap_m97E1150049F42F64CCB66385AF1FBDD6C46DE6FF (void);
// 0x0000017A System.Void Lean.Touch.LeanTouch::Clear()
extern void LeanTouch_Clear_mC455580A4FE48381BC7F0C3D90162724A159FEE9 (void);
// 0x0000017B System.Void Lean.Touch.LeanTouch::OnEnable()
extern void LeanTouch_OnEnable_mFF5A315C0F8CC8F46AA856DBED2D4D236866F4D5 (void);
// 0x0000017C System.Void Lean.Touch.LeanTouch::OnDisable()
extern void LeanTouch_OnDisable_m98BBABF0D6B9094F498276C20434B754F1876C14 (void);
// 0x0000017D System.Void Lean.Touch.LeanTouch::Update()
extern void LeanTouch_Update_mDA216CEE3AD7B68A0F96E15C1DEC3222FA99598E (void);
// 0x0000017E System.Void Lean.Touch.LeanTouch::UpdateFingers(System.Single,System.Boolean)
extern void LeanTouch_UpdateFingers_mD27614293027684A532ABAA8983AE4FDE9B10260 (void);
// 0x0000017F System.Void Lean.Touch.LeanTouch::OnGUI()
extern void LeanTouch_OnGUI_mE1E41559E4E8D0FC1ADEEE07B881A657BE12AE9A (void);
// 0x00000180 System.Void Lean.Touch.LeanTouch::BeginFingers(System.Single)
extern void LeanTouch_BeginFingers_mDC83A558CD0C0B21BCECAECF16B0A84456ACB093 (void);
// 0x00000181 System.Void Lean.Touch.LeanTouch::EndFingers(System.Single)
extern void LeanTouch_EndFingers_m49185B4B0485735E75536B74B18A3983D6E7CCB0 (void);
// 0x00000182 System.Void Lean.Touch.LeanTouch::PollFingers()
extern void LeanTouch_PollFingers_mF96BB27C6F0E65D36E6F013DF15A95F7AC352D6E (void);
// 0x00000183 System.Void Lean.Touch.LeanTouch::UpdateEvents()
extern void LeanTouch_UpdateEvents_mB7C1BB2A9C4F3DC5B65C8AE84AB180A7F2DF98D6 (void);
// 0x00000184 Lean.Touch.LeanFinger Lean.Touch.LeanTouch::AddFinger(System.Int32,UnityEngine.Vector2,System.Single,System.Boolean)
extern void LeanTouch_AddFinger_mEB34D91C5A4B8CE613638480E44E1A3E954972D1 (void);
// 0x00000185 Lean.Touch.LeanFinger Lean.Touch.LeanTouch::FindFinger(System.Int32)
extern void LeanTouch_FindFinger_m434440076CE45DBC0B30A91043830C20CDD5A005 (void);
// 0x00000186 System.Int32 Lean.Touch.LeanTouch::FindInactiveFingerIndex(System.Int32)
extern void LeanTouch_FindInactiveFingerIndex_m31FFBE7A878005B9578137401C9917B6DEB33CE0 (void);
// 0x00000187 System.Void Lean.Touch.LeanTouch::.ctor()
extern void LeanTouch__ctor_m9CA35BE5D76383B904B9DCB0BC7A3BC704DB85D1 (void);
// 0x00000188 System.Void Lean.Touch.LeanTouch::.cctor()
extern void LeanTouch__cctor_mFFF3DED1B3FE607086AF611640FA18DB23CBC03E (void);
// 0x00000189 System.Void Lean.Touch.LeanDragTrail/FingerData::.ctor()
extern void FingerData__ctor_mE9FB63BC10400B284B1C562FCC9BB52221208274 (void);
// 0x0000018A System.Void Lean.Touch.LeanFingerDown/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mC05EC48CAFC7550B3DC95C0D558FCA0ADF9863AF (void);
// 0x0000018B System.Void Lean.Touch.LeanFingerDown/Vector3Event::.ctor()
extern void Vector3Event__ctor_m60E3B84BF9CCF9209FB30A04F67D0FF9772EE6DB (void);
// 0x0000018C System.Void Lean.Touch.LeanFingerDown/Vector2Event::.ctor()
extern void Vector2Event__ctor_mB54AA670057292037FC6D18596B00CDC19EE67E0 (void);
// 0x0000018D System.Void Lean.Touch.LeanFingerOld/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m71A73B91CF98FFAC3D84C742489975CAFF58558B (void);
// 0x0000018E System.Void Lean.Touch.LeanFingerOld/Vector3Event::.ctor()
extern void Vector3Event__ctor_mC2EFC8A1743CFF95D10B3B016F7C7F6BA8FB3E73 (void);
// 0x0000018F System.Void Lean.Touch.LeanFingerOld/Vector2Event::.ctor()
extern void Vector2Event__ctor_mA415760F47AB7C1A922D959D65CB91C353BFD4BA (void);
// 0x00000190 System.Void Lean.Touch.LeanFingerTap/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mD612A446C5123142E226DE533375C90340318B51 (void);
// 0x00000191 System.Void Lean.Touch.LeanFingerTap/Vector3Event::.ctor()
extern void Vector3Event__ctor_m620F393B70ED40F6EC234631FA237906A51B024D (void);
// 0x00000192 System.Void Lean.Touch.LeanFingerTap/Vector2Event::.ctor()
extern void Vector2Event__ctor_mC02FBAC35F16AC5C93E96F1E4803628523D91F27 (void);
// 0x00000193 System.Void Lean.Touch.LeanFingerTap/IntEvent::.ctor()
extern void IntEvent__ctor_m99681557C151A665D5FDC8FA5239105FAA279EC3 (void);
// 0x00000194 System.Void Lean.Touch.LeanFingerUp/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mEA623E90D605563BB01DEA534A134BA43A063B94 (void);
// 0x00000195 System.Void Lean.Touch.LeanFingerUp/Vector3Event::.ctor()
extern void Vector3Event__ctor_m0ACA0A4790B94E511CABA0F738EB2F579B57F3D1 (void);
// 0x00000196 System.Void Lean.Touch.LeanFingerUp/Vector2Event::.ctor()
extern void Vector2Event__ctor_m5826B47957619CDF4660B82B0BA843766387F666 (void);
// 0x00000197 System.Void Lean.Touch.LeanFingerUpdate/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m4FB05496B752552B0DFBB32D70A1C123DCF420FD (void);
// 0x00000198 System.Void Lean.Touch.LeanFingerUpdate/FloatEvent::.ctor()
extern void FloatEvent__ctor_m54BFD81526CD29222C541DFF086D12634BEBEBCF (void);
// 0x00000199 System.Void Lean.Touch.LeanFingerUpdate/Vector2Event::.ctor()
extern void Vector2Event__ctor_mF15CC2F3E21447CB7E0271235D60B89B20D4104A (void);
// 0x0000019A System.Void Lean.Touch.LeanFingerUpdate/Vector3Event::.ctor()
extern void Vector3Event__ctor_mA2DDB696B16841CBE6F604BF36DB1328EBDE079B (void);
// 0x0000019B System.Void Lean.Touch.LeanFingerUpdate/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_m6959F1F3ACDF38D8CB51C96B5A463D1AE8DFCB53 (void);
// 0x0000019C System.Void Lean.Touch.LeanSelectable/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m9E3C79E663456779F7EDF5D8CB62F109743DDEF1 (void);
// 0x0000019D System.Void Lean.Touch.LeanSwipeBase/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m282B85DFC7872BC0910F3185B36229F93271602B (void);
// 0x0000019E System.Void Lean.Touch.LeanSwipeBase/FloatEvent::.ctor()
extern void FloatEvent__ctor_mEF280836B4F0B3C0DB5351D38CE3BCB176AAD4EB (void);
// 0x0000019F System.Void Lean.Touch.LeanSwipeBase/Vector2Event::.ctor()
extern void Vector2Event__ctor_mD00DCED0754F1B5079750F905A79C2C4A1D3190F (void);
// 0x000001A0 System.Void Lean.Touch.LeanSwipeBase/Vector3Event::.ctor()
extern void Vector3Event__ctor_mD07BBC817B6AAA4F64B85812CEECBDD0529EB214 (void);
// 0x000001A1 System.Void Lean.Touch.LeanSwipeBase/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_m371C03513F418FFBCF53AE4E02127EF6BC66C70A (void);
static Il2CppMethodPointer s_methodPointers[417] = 
{
	PlacementObject_get_Selected_m455279660122821B10F23CBD2E15BD4EF3DCB8B3,
	PlacementObject_set_Selected_mDDB172B6806809F8BE1119A2AE9FAD8F0C1D5CA1,
	PlacementObject_get_Moved_m4DFE0D4AFFB85525800C184B93EB311C7AC3ED12,
	PlacementObject_set_Moved_m942BA026A031DDB8E21893D942A7299A36BD24B9,
	PlacementObject__ctor_mEAC2DD06D9139149C6983DBAC5A212060F415091,
	LeanInfoText_Display_mC89B42B5079E21579121AD580789C28FF7235AF8,
	LeanInfoText_Display_m873F45E5221D8754281AC6E843D440EBA54E6BA9,
	LeanInfoText_Display_m8EA71728D9EBD19914BDBC1C8705DA47896FD064,
	LeanInfoText_Display_m36B74395502B3F97AAE87180F77140DBED5E36F4,
	LeanInfoText_Display_mDBBD632A9C78FC7622CA415046269CA9DE1A05B8,
	LeanInfoText_Display_mD435885000DDF1691E344CC619666DEEAC19EBEB,
	LeanInfoText_Display_m4379AD4A936839A5B6ECCCC254E77F16D862E4FE,
	LeanInfoText_Display_m1E63A97FA5A497B4307DBE823098A08B77FB9C2C,
	LeanInfoText__ctor_m8921A7F24A14E928B11C915243D040C8CAA97F45,
	LeanPulseScale_Update_mF09F52328885E2FA0D6FCA40C845B9891E3BFD8F,
	LeanPulseScale__ctor_m20DEFFD37C09DA08DABFFFE102F74755EF1F5C2E,
	LeanTouchEvents_OnEnable_mC96EFD5D7E9D5DA3241CA81FE7227962B324BC6C,
	LeanTouchEvents_OnDisable_m85ECF46CFB1EC4C3BD316AF0CAB7DFE29CA7F13B,
	LeanTouchEvents_HandleFingerDown_mA3D0F877BD6DC57E77FDE1C49CDBDBE4CEECE30D,
	LeanTouchEvents_HandleFingerUpdate_m8CC3E943326E5A66696F913EB481C4797E0B14B5,
	LeanTouchEvents_HandleFingerUp_mBF83766DF3C28C1FE18FB02CCA86A4BF033D695E,
	LeanTouchEvents_HandleFingerTap_m5C23A85C50C9B1ACB7DBB7E0AD237051B17ADF21,
	LeanTouchEvents_HandleFingerSwipe_m8AC15F1904BFC54B5C4271C6FED91A39A4EE91B2,
	LeanTouchEvents_HandleGesture_m4413436E90CBA5D853DBAA3582996E9D0898876A,
	LeanTouchEvents__ctor_m4E0D5C623AC099DE68B77C93C31E031EDF2F117E,
	LeanDragCamera_MoveToSelection_m728D976D87EA012DCED4FFBA6DED868FB2DD26C1,
	LeanDragCamera_AddFinger_m6FE4EA9283A7E43B7A8922895E07F2E3C9903696,
	LeanDragCamera_RemoveFinger_m54D8A9307E55A339AE1585A2E54A209B5ED10679,
	LeanDragCamera_RemoveAllFingers_m64128368181902BAB14710E7B2ADF1D453E6C271,
	LeanDragCamera_Awake_m7B3706BDE4C1895DE21DBEED9032584681E45FAF,
	LeanDragCamera_LateUpdate_m8728E32FB43B7CA19EF520C4C4C78BE92EAF8478,
	LeanDragCamera__ctor_mF87DBFC163061D8888517595CA0C65FB7FAB215C,
	LeanDragTrail_AddFinger_m4C98B01BAF07928B343A1F333934152EA50A5587,
	LeanDragTrail_RemoveFinger_m082E75D7FCD1A0C44BE87E5182160E265A06A20A,
	LeanDragTrail_RemoveAllFingers_mF30DE6483ADF67991F0892CC6006D4A0035478A5,
	LeanDragTrail_Awake_m0B4D21DA7885035CDFBE6D1A490EFD1496F5B9D6,
	LeanDragTrail_OnEnable_mD0E0F8C3FE3FCA92A57C80620328262F7A1A0C68,
	LeanDragTrail_OnDisable_mEBCA3EAE1A31028425B610AFF388E88C851F1C1E,
	LeanDragTrail_Update_mC915D505899B9904FE5E54E190E56DE13AD3E07A,
	LeanDragTrail_UpdateLine_mFDBFF66AEE9C9112A54819BF6B6CD8DCE17434F6,
	LeanDragTrail_HandleFingerUp_mDD4968D9C46EFF834BABD5D0E1E9AA9F90B618C2,
	LeanDragTrail__ctor_m11B1E9B916902DC24128BD3915AD14CBF82B0A25,
	LeanDragTranslate_AddFinger_m4CC7D46FE10E3FCCFF4BDF15FE850E62BF32DC41,
	LeanDragTranslate_RemoveFinger_m182850F91D0B4D564FF7C412C18AD785AFBA9925,
	LeanDragTranslate_RemoveAllFingers_mF7F2EB545B6B66EBA950974F39C40ED4E640933E,
	LeanDragTranslate_Awake_m84CC62EDC5669DC6ED9C28AB7D5D3686663F54C2,
	LeanDragTranslate_Update_mBE0714E3ACFE28E012409462C476C7B2D59EFC7B,
	LeanDragTranslate_TranslateUI_m45EE318C09E821C1C43A95B416F6AAA7D342DBF3,
	LeanDragTranslate_Translate_m7BD8AC2BA77F507F8FBC1DF19C3108011A03E080,
	LeanDragTranslate__ctor_mBC9000EFE1DCCCDB9B84F2CC265385963EC50A6D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LeanFingerData__ctor_mAB102253306FDAE43FA8680B4D84059BFD831A18,
	LeanFingerDown_get_OnFinger_m0BAB3394325C043DB48660D3334EBBED0CF5C2D7,
	LeanFingerDown_get_OnWorld_m61EB189436881FB0800230865BDDB24F4A92D858,
	LeanFingerDown_get_OnScreen_m3A7F4C0E825E449D718FC01E465200DDEFD399BD,
	LeanFingerDown_Awake_m027ADE644DFD5664ED9DF5DA89C5EF7E5BC448F5,
	LeanFingerDown_OnEnable_m7F5CF06AC5361A7E5739DFDC2B1F02A9A5DB211C,
	LeanFingerDown_OnDisable_mB196EC8556D9A04BFBC9D47081B50512FBBD02AA,
	LeanFingerDown_HandleFingerDown_m67B3DD3FD8892A8627E3A9073F97A7A8A94AB60B,
	LeanFingerDown__ctor_m74CCE40102A349C904F633B25B3158010F09C5F7,
	LeanFingerFilter__ctor_mE72DC02BFD6E51E07FF20A8BEF9F985347483AA5,
	LeanFingerFilter__ctor_mFB253DF5673F23327658C66A9D4AA8C0F8725263,
	LeanFingerFilter_UpdateRequiredSelectable_m3E2BEC9AB1DAA8AB70EC2E11B8D1227945031E7D,
	LeanFingerFilter_AddFinger_m74C4A364453ED4A22F8423749A505DF0BA5A92EE,
	LeanFingerFilter_RemoveFinger_mF840A547635068D3F879DF19F3E17BE362E926F7,
	LeanFingerFilter_RemoveAllFingers_mE802FAFCE7EBF3FE06ECDF1BA15F3644F85F3E9F,
	LeanFingerFilter_GetFingers_mB21B35B96FBDE7030A77F8C129ED467C6C85EDDE,
	LeanFingerFilter_SimulatedFingersExist_m0D6895747216B91285CDDD5923490E2E4599061A,
	LeanFingerOld_get_OnFinger_m2BC7D6FCE3E19A3DCAC7D1CDE5A8D35F1BEB7A58,
	LeanFingerOld_get_OnWorld_mAFAD32A68D9580E9F73A0CDAC8928CF3B7946F9E,
	LeanFingerOld_get_OnScreen_m72B61F60B8ED378D6D0DFB4C79136D6521FCD022,
	LeanFingerOld_Start_m7F77C11EA28F9587CB04AED703C17B17FE040BBE,
	LeanFingerOld_OnEnable_m88EB1D8CCEFDB54C3479B8DF1CD6561FEEF7AFAE,
	LeanFingerOld_OnDisable_mDABDB163715447F4C07533350B4A392495E17B8B,
	LeanFingerOld_HandleFingerOld_m48AFD28E9D1FC3B31F34E4A2CC2D224D82379E68,
	LeanFingerOld__ctor_mFA869EE1D8F3A2CEE0FE48E5ACD6DE994EA11788,
	LeanFingerSwipe_Awake_m181DA196669AB41D1C30B42C175E88D28204D211,
	LeanFingerSwipe_OnEnable_m991E730B1CEBD2F228D61BF25831C3F4DD78941C,
	LeanFingerSwipe_OnDisable_m799D8A4E4B5F351E64812073F7BFD6B526A26C32,
	LeanFingerSwipe_HandleFingerSwipe_m1BD172260F0DFBF5C928D69596BAFF359A9CB73B,
	LeanFingerSwipe__ctor_mCDA967A7F4AB7F9BCB88B7DDE3B96D7B522BAB88,
	LeanFingerTap_get_OnFinger_mD89F0648B98225A985D39D8A5EDA267E291E6455,
	LeanFingerTap_get_OnCount_mCA987B94870BE58577F047F81C4EF6405B8DA3BB,
	LeanFingerTap_get_OnWorld_m15E0F71373F995CC9A51AA0E71C0DAA2803FEF8D,
	LeanFingerTap_get_OnScreen_mEE11634BE7DCBF458BD79C48677A0EF151298297,
	LeanFingerTap_Start_m42FB925B43082A52181D40459B05D57D9A6BC330,
	LeanFingerTap_OnEnable_m48D53E704DE813C41C4D47344B213C4F14E57B59,
	LeanFingerTap_OnDisable_m32CEB4ED012CDA46405AE2F34CFDB1EDF40445FA,
	LeanFingerTap_HandleFingerTap_mD5BA50BD353B8EB50A0C05D8EBB66314716CDCC1,
	LeanFingerTap__ctor_m1D1E504F1D1C1A68A68A66DAD66ACA2C2B6A875E,
	LeanFingerUp_get_OnFinger_mC8A52BA4BD078D1FC5930831B5C27D556E6D7018,
	LeanFingerUp_get_OnWorld_m402AD3A542D8673FAE8DC5C3B7B51A93CD0F9D7D,
	LeanFingerUp_get_OnScreen_mB4A4AE71D19E07C6CD7771BC0040FD3F8A9FDDCC,
	LeanFingerUp_Start_mA9ED0637901B6AB89B2B03333C64C8CFEB113469,
	LeanFingerUp_OnEnable_m57D99AFBFE1EBC40A2ED497310266F3330DF80BA,
	LeanFingerUp_OnDisable_m3B088021E233E1458A2A06BC417020E2D33685AF,
	LeanFingerUp_HandleFingerUp_m239A03C8BFCC3DEE3B86E8E2A29FF838D9B4E97B,
	LeanFingerUp__ctor_mEF8E87E6EA5F3096B05ABC337633864BF00D8FC0,
	LeanFingerUpdate_get_OnFinger_m09B173C847C6A8760EB68891E5D644923C1095F3,
	LeanFingerUpdate_get_OnDelta_m55ECD79AC0B829C76273BACE0709CF32472BC861,
	LeanFingerUpdate_get_OnDistance_m381843B49142966FE9FCD0AC9C28151357FC0EB2,
	LeanFingerUpdate_get_OnWorldFrom_m58399A52D0C9E5B7710EAC9E206BA5A68565BF12,
	LeanFingerUpdate_get_OnWorldTo_mE5B3AFBC4CF85834C13E649655A07E85CD66DCA1,
	LeanFingerUpdate_get_OnWorldDelta_mBE545C2F5CAA0080B328564804B66F46648785A6,
	LeanFingerUpdate_get_OnWorldFromTo_m7ACB67A531A8719C4EB4376FE9B6807056F955A0,
	LeanFingerUpdate_Awake_mA2FA75D1D7F5C66DCCB56493AB5779E9A00D11F5,
	LeanFingerUpdate_OnEnable_mC2AEE1A2604D91209618A698F3B9103A4E63A353,
	LeanFingerUpdate_OnDisable_mACF860169F38FC215F48B873BA1297002B2BFBDC,
	LeanFingerUpdate_HandleFingerUpdate_m31485A52BCAF37B13920B56176F4964803E5C58A,
	LeanFingerUpdate__ctor_mB48F8C1BAA0286249DF857C371C791E061C0A53A,
	LeanPinchScale_AddFinger_m742DED990437B51659621844167EB76BF2CE7717,
	LeanPinchScale_RemoveFinger_mE34778EB1D6CB110274CE077F081BECBAC14B404,
	LeanPinchScale_RemoveAllFingers_m7AA07C179F616386BFAB3774FEBD60D479FAC440,
	LeanPinchScale_Awake_m617CBABFC1E61A7DEB6195C8965F808CF521AA8C,
	LeanPinchScale_Update_m273AA43206C30781E8C8B4D211AA8FE7B1B0ABF5,
	LeanPinchScale_TranslateUI_mFD2A8D66341DA9F9CA208E26844CE7DAD20D1384,
	LeanPinchScale_Translate_m3C7C5D94116565DEBC1124620B4CABABEA067712,
	LeanPinchScale__ctor_m314559A2E8548D25C9A815773EE047E99458E036,
	LeanScreenDepth__ctor_m09C2B43694D7A2B959469447627D3B25AB095FE9,
	LeanScreenDepth_Convert_m4CA8998FD975A5AC752171657968240E73225A49,
	LeanScreenDepth_ConvertDelta_m2D9538AB685663572C4A7E9C7847CF643D453D0E,
	LeanScreenDepth_TryConvert_m58F0642957BDC300666C8799D1581F0AD8A2501F,
	NULL,
	LeanScreenDepth_IsChildOf_mC35631C16DEEE1F7EFC8ECBC21EDBCA27A528B80,
	LeanScreenDepth__cctor_mCD3C54755FDADC247C7EBF078FD7CE7861D05903,
	LeanSelect_TrySelect_mC5BFAB821F418D52412960D61D87FCD19403D9CB,
	LeanSelect_Select_m9EB6EB9A8AF98B67048CC84B1CA0EE44AB048B38,
	LeanSelect_DeselectAll_mD7C138D0FE111FF4E6642CEF15CB6EABF535FE23,
	LeanSelect_OnEnable_mFC12599311AB3491E9B76535F365627062D03F34,
	LeanSelect_OnDisable_mB86CECE9D9104908072817644727581B86247AAB,
	LeanSelect__ctor_m5AC05333119825B1410B5783C7CE24B9A82FE47F,
	LeanSelect__cctor_m3552C61DEBD00681AAF40B42D96F6E23FD10FFE2,
	LeanSelectBase_SelectStartScreenPosition_m8E5D4B117C100257A5301B618E9BB267ADF13549,
	LeanSelectBase_SelectScreenPosition_m934789C260AD1664FDA85624DC2A76FE8C0326DC,
	LeanSelectBase_SelectScreenPosition_m79BF224A6471B044145023A4D154745ABB3A022C,
	NULL,
	LeanSelectBase_TryGetComponent_mE4890DFE413B04A3890E124326DD3B63087E1F88,
	LeanSelectBase_GetClosestRaycastHitsIndex_mB062EBABDCA90A4FC8DD056011064213A92918A6,
	LeanSelectBase_GetScreenPoint_m1A5F3E511AD4F5DB65546390C0AB03A19DC9F815,
	LeanSelectBase__ctor_m21C9F84FD06B0204E845F8CBCC1030887CAD2366,
	LeanSelectBase__cctor_m89AC8B3785351C2E89C6B4B5874C33F3D3397F94,
	LeanSelectable_add_OnEnableGlobal_mA4AE6B61F64C69E38D3D4C08530615118074FE27,
	LeanSelectable_remove_OnEnableGlobal_m071D22DA519C90595CAAE7FC2D250CEC46A94952,
	LeanSelectable_add_OnDisableGlobal_m310A3835E66C461E943E5F11F889AF2FC2D344EE,
	LeanSelectable_remove_OnDisableGlobal_m5A63CC00B20D746B1D1775CF837FFC39F93735C7,
	LeanSelectable_add_OnSelectGlobal_m49B4735AEFC00D613C3463A5AB2CF4A229D70945,
	LeanSelectable_remove_OnSelectGlobal_m3398464478E0766B8B6D913FED4356B41FF08032,
	LeanSelectable_add_OnSelectSetGlobal_mD6E364EB80C79228D9F2A108E7B0272DAFD351AE,
	LeanSelectable_remove_OnSelectSetGlobal_m0C4118B0CA52F87594B9872C5BF1BFBB739B9EB9,
	LeanSelectable_add_OnSelectUpGlobal_m217EB0AC8EA82F5C2AE056A75CBD9467DA467E11,
	LeanSelectable_remove_OnSelectUpGlobal_mCD19683D78EAD04CCE9FB36ACFBC3B15336D4F3E,
	LeanSelectable_add_OnDeselectGlobal_m2E6DE2CFE77446917368463D19A500B5763476D8,
	LeanSelectable_remove_OnDeselectGlobal_mA530F49ACB9CA9F187F6DEFDEE334B6309965A0A,
	LeanSelectable_get_OnSelect_m458A1A56BE7BAF5A3694E2D4B6C442CA288F9BBA,
	LeanSelectable_get_OnSelectUpdate_m2129D6C2B021FC842DCF9F3714982B43C6A9A637,
	LeanSelectable_get_OnSelectUp_m27EF71C22C5EE13936D03BFB0A46F4455F64798D,
	LeanSelectable_get_OnDeselect_m61CF88AD67D81724DA3E19D94C2756F5BC8A3CE5,
	LeanSelectable_set_IsSelected_mB5F64CA8927EDD0E171E144FC123D1D98A0525A2,
	LeanSelectable_get_IsSelected_m45813D800C6C360758156A2087BEEF66DC708EB3,
	LeanSelectable_get_IsSelectedRaw_m24B4AB93759291374E9599581F4F17390EDFA1D9,
	LeanSelectable_get_IsSelectedCount_mF084096474A93A1C4FB47B379702843A3CE64AC5,
	LeanSelectable_get_IsSelectedRawCount_m61D1D3250A5BAE849B237334868A543B054DFD79,
	LeanSelectable_get_SelectingFinger_m1251D3B675B3FD2FFB0068D764D586C6D537D60B,
	LeanSelectable_get_SelectingFingers_mDB3588A7613F596580B8C9741754AFB1530C1779,
	LeanSelectable_GetFingers_m821F744EF77C858C2A6038FAC5DEC77B0372F2F3,
	LeanSelectable_GetSelected_m58CC9B9D0208F066889B4EE00837E09F3512E0A2,
	LeanSelectable_Cull_mF23406C07D76AFCD33F84AFCB3B2232CB2B5CA9D,
	LeanSelectable_FindSelectable_m2B24DCF9FE3D8DD9279209D352DFC9F794AC7BF5,
	LeanSelectable_ReplaceSelection_mA9381B279E02A200DB4EAB10B7EBAB542EED95FC,
	LeanSelectable_IsSelectedBy_mA2CE79FA7AFEF426918C16C2C58658C789B9F658,
	LeanSelectable_GetIsSelected_m1DBF0E54DBC45C3188F0C98DDE80D15BDC40E3C2,
	LeanSelectable_Select_m6C323F3F39CEFE669BDFAD2EF3768000BE73C9EC,
	LeanSelectable_Select_mFE2E8E0481A51153E75BFBC02192ABE0DF431601,
	LeanSelectable_Deselect_mC59C461CA95C8180800A3846FF2FBB12E1928316,
	LeanSelectable_DeselectAll_m374D612DC7A2A6EE9028ACC4672FAB5D6A5B2E73,
	LeanSelectable_OnEnable_mBCAB65E5423DEEE77DA3D497C2C0900AD042975B,
	LeanSelectable_OnDisable_m815F9EFEE11B80BE17968A13442241971C26A2E0,
	LeanSelectable_BuildTempSelectables_m87DEB12929F673A78A75F0ED23B54CDFF004FA2E,
	LeanSelectable_HandleFingerUpdate_m6965B7DF88B37CC0F440F8B4E80B320F8152BF65,
	LeanSelectable_get_AnyFingersSet_mFBBED3F411BF5141C09AD054374865B89FA1573B,
	LeanSelectable_HandleFingerUp_mEEBA33685A88C3245FD0B87F556EE575E796855E,
	LeanSelectable_HandleFingerInactive_mE7E0D8E81F29695C64BB3B6CF732FFCB5D0DE5FC,
	LeanSelectable__ctor_mDB34A260912A9BA62B628E399C4C49CD8278434B,
	LeanSelectable__cctor_m10628DBF9367FA478BA466A70DEF89EFA16F8FF8,
	LeanSelectableBehaviour_get_Selectable_m81BCDA355E315B389E391E2496C4F6B6940FC393,
	LeanSelectableBehaviour_Register_mAEFA703242375C095EDABBF5F2337B94C258F57C,
	LeanSelectableBehaviour_Register_m11E06AC483169D046681D12DA9041898EB46A099,
	LeanSelectableBehaviour_Unregister_mA35B24DEA8F7FAEFB63F303CE126915DF31DAEDC,
	LeanSelectableBehaviour_OnEnable_m9384AAAFF5046D077EFB3CA0DD5974930F53D05B,
	LeanSelectableBehaviour_Start_m9BBCA492A9EFEEE4405B21716586E3E4A7231E02,
	LeanSelectableBehaviour_OnDisable_mDCA81D89C7B9D0C200D3CF0293463D0D5B01FC8C,
	LeanSelectableBehaviour_OnSelect_m13039731E3B66DFD0029DD1EF5259A2CF0A4D9CA,
	LeanSelectableBehaviour_OnSelectUp_mB4BB14046FBEA3B4955C1CAC48A13C2050AF6A0A,
	LeanSelectableBehaviour_OnDeselect_m5B7AFA42C14EB79671B8C506F5F408C838C04552,
	LeanSelectableBehaviour__ctor_mAD30ECF856963F95ADE2F5CFFD88D04ED9B28477,
	LeanSelectableGraphicColor_Awake_m08DE4F0B054BF6E88ED70F605E16DF25B5BCADAB,
	LeanSelectableGraphicColor_OnSelect_m06AD85DF68BC5A6F3AC578B46B6FDC8FC8664231,
	LeanSelectableGraphicColor_OnDeselect_m0D451FC3CEAF872AB61F5AE4B48A57394910D3F2,
	LeanSelectableGraphicColor_ChangeColor_m0B02F0C1FAAF3D557B937D7D447C7CA7D1CE5A74,
	LeanSelectableGraphicColor__ctor_mB1A829691CDB1A982043A0397C0BBB225282E86D,
	LeanSelectableRendererColor_Awake_mF5A1124D509C2A60F88A6B26068B0BE62E9C0852,
	LeanSelectableRendererColor_OnSelect_m1970E0D894D9C099306D4B83671240601AD16A3E,
	LeanSelectableRendererColor_OnDeselect_m51F70C3B3A052D38DDF089A2CDD730F9A0AB99C8,
	LeanSelectableRendererColor_ChangeColor_m40097FFBCAE29F63A697EFE48F40DD32FF3F56F6,
	LeanSelectableRendererColor__ctor_m846D28FA84523E5202F59826EE1637E6A96C56C6,
	LeanSelectableSpriteRendererColor_Awake_mB0CE0D8C5EE03B5E5ACDF8112571BA1E0A5B58AD,
	LeanSelectableSpriteRendererColor_OnSelect_mD4593ABB988E6D34D50BF50746FC7DE154EE9BBE,
	LeanSelectableSpriteRendererColor_OnDeselect_m9C83E0E59BE4ACBD19042078824290DBC6C8D655,
	LeanSelectableSpriteRendererColor_ChangeColor_m03E4A9F088936E02CAF3C902CC3514789F1DEA0E,
	LeanSelectableSpriteRendererColor__ctor_m86FC5F91F62E0B5ED8FA1F570133FD05F1E581DC,
	LeanSwipeBase_get_OnFinger_m089DA3BF574B349393275C5E0680FA7AD0A941A7,
	LeanSwipeBase_get_OnDelta_m34CF75E4F7C48424B8DE7022C3034DE6AE2D9993,
	LeanSwipeBase_get_OnDistance_m893B0BF23AB8C1CEFAEB74831AFE568464735F82,
	LeanSwipeBase_get_OnWorldFrom_mF79B90B70DF37167092DEB3E266F1E18AAA831CA,
	LeanSwipeBase_get_OnWorldTo_mAFEF7B305825D6CD79E9CF6AF2D038F9EFFF485D,
	LeanSwipeBase_get_OnWorldDelta_m5795E00F837569385378684B733423B113BC26D1,
	LeanSwipeBase_get_OnWorldFromTo_m4FC410EB76914CE5E8F9792AEADC8B44691C98B0,
	LeanSwipeBase_AngleIsValid_mB681B627ED3D397118C65AD2EE92C830F30CD546,
	LeanSwipeBase_HandleFingerSwipe_mEF2CEB93D880577C0FB85659345BDBA59E5DBF4E,
	LeanSwipeBase__ctor_m0E088D22B3C151DD0C03DF38801364D812C66B75,
	LeanTwistRotate_AddFinger_m06198BA0BE59F93B3F239D56053A8EE8A4F794EF,
	LeanTwistRotate_RemoveFinger_mC099EC7A6D775872D495E8D3605092D2F9186246,
	LeanTwistRotate_RemoveAllFingers_m94278F25735B8FBB27C1CE815D061D2CB4A77BC3,
	LeanTwistRotate_Awake_m343EF7B18DE6A844AF854CBF0725BF0BE4284B50,
	LeanTwistRotate_Update_mD9917297B2AE6FD68EA8F07C2649E95C5185F76B,
	LeanTwistRotate_TranslateUI_mC1DC5900812C204BE8812CA8E1782BF1E2A6CF9F,
	LeanTwistRotate_Translate_m9F62FEB003B1E95FE3F4F1066005365C9A17E018,
	LeanTwistRotate_RotateUI_m2A8ADFB4A8376D9150FE673CE66812CF9C1E5104,
	LeanTwistRotate_Rotate_mC211DFAB5207340FA76A74E00EE79443ED09A9E3,
	LeanTwistRotate__ctor_m7ED74A8ADB3616000C647E21F79331B6D2D0ECAB,
	LeanTwistRotateAxis_Start_m44B782C5209DD47386C4458A2EBB521787E91BBB,
	LeanTwistRotateAxis_AddFinger_m8D4B6FBE644BCBDB22345CF8C7B33C975108C394,
	LeanTwistRotateAxis_RemoveFinger_mEECE240BA420BFBC19EC9F1BA20CECA8ED203139,
	LeanTwistRotateAxis_RemoveAllFingers_m04B2EF72EC3DA09B5AA808A9605534FE40B886B6,
	LeanTwistRotateAxis_Awake_m4B4B8940E9CB4E7376B8E81C86337025E962CE97,
	LeanTwistRotateAxis_Update_m0B1314061FBFAC43E3B803E49576FA923016529B,
	LeanTwistRotateAxis__ctor_m1D5C67A5228B40CAD97D2BEA27A72D225AB70AAB,
	LeanFinger_get_IsActive_m7B5C0C70E780459E6CFAD6EA7F1EDC329B4F301F,
	LeanFinger_get_SnapshotDuration_m00EA679A6256D6E6C3188B5C87547B21DFE69E6F,
	LeanFinger_get_IsOverGui_m4FADA1E47BCEF005D81B24DD96514D05F0D6EA4E,
	LeanFinger_get_Down_m63337A38DF2CB3F638839BB772E39F9F37ED197F,
	LeanFinger_get_Up_mBA17A3A5510FB43E81D0FE93C8DB139C2A5B204C,
	LeanFinger_get_LastSnapshotScreenDelta_m3DD9E1CCC4ABB919ABC24C9DEC1D017672EB27A7,
	LeanFinger_get_LastSnapshotScaledDelta_mA7BF830AB9635286F4520D88271D93F42A1750C1,
	LeanFinger_get_ScreenDelta_m805424DD0B14A8B6D85319A7EB62F776A9CE7422,
	LeanFinger_get_ScaledDelta_mEDB3129AAC50FC83726BE26746E532E3CDCE094F,
	LeanFinger_get_SwipeScreenDelta_mD96829E19F10C3325E4010B314C658E2B5FE6E36,
	LeanFinger_get_SwipeScaledDelta_mFFCF0BEE998EA39F128C137E029C6E078A3D84FB,
	LeanFinger_GetSmoothScreenPosition_m0DCD6CA9CE517AB8BD15E3C269A21AB3DF89E159,
	LeanFinger_get_SmoothScreenPositionDelta_mA7EB0B2947DE1781B20112D58F0283DB152B2641,
	LeanFinger_GetRay_m055289FE695582FF57C9A381C28DF0F7580C1DD5,
	LeanFinger_GetStartRay_mD8077059062A44A7186809C5DD592F42947F8600,
	LeanFinger_GetSnapshotScreenDelta_mBE8E710D7A01EBA901D03B03475F2AE3E9A535F1,
	LeanFinger_GetSnapshotScaledDelta_m167B50111800864822CE441DEAB1F0955954402A,
	LeanFinger_GetSnapshotScreenPosition_mBB2F679EAB8649CEF17151548C03CEED1D4FB4EC,
	LeanFinger_GetSnapshotWorldPosition_mF8EFFCC1E0F0AF62150EDA2A2DCD0EA65590C974,
	LeanFinger_GetRadians_mE40D50BCC0D96C679C5680EC06C5566C3898BA44,
	LeanFinger_GetDegrees_m5D305CBC7A07C80A8903EF796CBD01F8AC128946,
	LeanFinger_GetLastRadians_mD38E79A7B6288220CFAA7863467FF247D7697E4C,
	LeanFinger_GetLastDegrees_m0E4A0CC2BC6AB1F7BEF175E97903931A0D0DFCE5,
	LeanFinger_GetDeltaRadians_mB3EAF87D832EBC7EEC8527CA909CD14EA4C238C1,
	LeanFinger_GetDeltaRadians_m8AD39852759E6C5C402FCD01D84184A35234104C,
	LeanFinger_GetDeltaDegrees_m5551EEF44F06CD804C11E2BE884F551937555E86,
	LeanFinger_GetDeltaDegrees_m6FA7E89E772A66357B476FAA02D1EE42ED84669C,
	LeanFinger_GetScreenDistance_m8AFF803D658761C26B638FFB8BB364A606D17737,
	LeanFinger_GetScaledDistance_mD0C88B654FAA52B3B6326A606600D84C6359E466,
	LeanFinger_GetLastScreenDistance_mF740435B176EDD5F9A8E320AB0961A33C0F31BBF,
	LeanFinger_GetLastScaledDistance_m38FA4AAC461BBA58A278F77BEFA30DE4E087279F,
	LeanFinger_GetStartScreenDistance_mC7EF17E383354CE99243B99312FA3491BB8FCB23,
	LeanFinger_GetStartScaledDistance_mD2FB957F6AD12683A287709EDB07E269798CFAC7,
	LeanFinger_GetStartWorldPosition_mCECBD81DD0353BFA05BE95B0E5CBA636AAF18C6F,
	LeanFinger_GetLastWorldPosition_mB0310D234AF766B2469EDD3DD6E1F173F0BE8617,
	LeanFinger_GetWorldPosition_m7E26ADEC7D2AE0F5370585A39B4BE4DB85BCC237,
	LeanFinger_GetWorldDelta_m56C5F5A4D8043F495EFE939B0AA66399F200FEF8,
	LeanFinger_GetWorldDelta_m2DB5442571023BBA2DE0D5D295EF341BC01B14C2,
	LeanFinger_ClearSnapshots_m3D66E683C8F566BD9BFB6A7025E2A8230568BB18,
	LeanFinger_RecordSnapshot_m77E4E23BA5DD226853726EAD61D42CEF434DBDA1,
	LeanFinger__ctor_mA0EBF0E1F7705147ED0B962A7E4A7CEEF3B0EDDA,
	LeanGesture_GetScreenCenter_mBEF0476CED5FE8BB64F877F4ABF3A67A8F9F1224,
	LeanGesture_GetScreenCenter_m1DC38D53DB783DF953F11C628DBB526785F8B842,
	LeanGesture_TryGetScreenCenter_m6B05B5DD79C77423120ABC8F91EE7BF9962F5265,
	LeanGesture_GetLastScreenCenter_m4E14B6ABA30F7814115D7A9EC1288BC793BF5A78,
	LeanGesture_GetLastScreenCenter_m5D0898BCD34B34A933D19442B755DF036BDCA895,
	LeanGesture_TryGetLastScreenCenter_mA28E2EE3F4731065261811B5A49019EAF0E4720E,
	LeanGesture_GetStartScreenCenter_m01AECC0B403D0BCFD5F3BE2FC582F9F24A8782C1,
	LeanGesture_GetStartScreenCenter_m107DFC9CF385B8222188B5E7ECC37C83F104D2DA,
	LeanGesture_TryGetStartScreenCenter_m4EBFF200C9069D6FCC51C32B5E34412907E4FB3F,
	LeanGesture_GetScreenDelta_mACDB404102BCA59C31544F8CA56A67D9DDD31208,
	LeanGesture_GetScreenDelta_m39E329359BE8F6FEFBCC6D1E9D7511B3A4C98E01,
	LeanGesture_TryGetScreenDelta_mB36A7BED7FDBC73303285DB8FEC6D7692FB4903C,
	LeanGesture_GetScaledDelta_mEF6CE2F578CD9941AB2A59D3A5FBD6B1E905054E,
	LeanGesture_GetScaledDelta_m839EDC79670B093766CA8FFEE2B818C4ADDFD8D7,
	LeanGesture_TryGetScaledDelta_mA7CD4F6CAF40334EC0CA95551ECCDBB3E08BF777,
	LeanGesture_GetWorldDelta_m8C098B852420EE97FD5E45D150499D3D9A4CE818,
	LeanGesture_GetWorldDelta_mB2D118B56CE437465850B4ECFD61C46BD79596BD,
	LeanGesture_TryGetWorldDelta_m3E3F139CEA1BE81FF41E6ED9A814C8751F272236,
	LeanGesture_GetScreenDistance_m6C92A4B160314A7321B56FF87F5679217426763B,
	LeanGesture_GetScreenDistance_m610A8A328FC1EB8D57D5A17E0BAA6DF5028270C3,
	LeanGesture_GetScreenDistance_m52B34E72ED87957E64AC2AED4A1F7573025522D9,
	LeanGesture_TryGetScreenDistance_m23863CEA30992F714E375888644D1821734A6FA6,
	LeanGesture_GetScaledDistance_m98B93AF6E44C1447E77A6EEEE1CC0BADCDFFD26E,
	LeanGesture_GetScaledDistance_mF7A95B44E7803F4A7B0F788543A6844E6C9D1486,
	LeanGesture_GetScaledDistance_mDF74EACB2D5AC58470A92D6DBC3A43A753F72BD9,
	LeanGesture_TryGetScaledDistance_mC664C23A094DE14529A29912704FA4DFB959C2C8,
	LeanGesture_GetLastScreenDistance_mA8A4D8ED507B74A967409948A7E714186D6837BD,
	LeanGesture_GetLastScreenDistance_mDE8490CC6D2376181647664FCAA77E02563823E0,
	LeanGesture_GetLastScreenDistance_m1453B1FF8B2217B8419A06B9ADB11BBE36A58971,
	LeanGesture_TryGetLastScreenDistance_m17A3C6F0CEBE3377C970F458399BB7FF79A65D04,
	LeanGesture_GetLastScaledDistance_m4AAA8EDC82C58600D3F1B10CD5150883B8107E2D,
	LeanGesture_GetLastScaledDistance_m5B75BCA7A0612F095A8F97DB298BEC3361C2A18F,
	LeanGesture_GetLastScaledDistance_m9A507CA3FED364435C5356B76A7A1F40147272ED,
	LeanGesture_TryGetLastScaledDistance_m9F9E2260F51FB3F26A032CCC397642F3DAB218C6,
	LeanGesture_GetStartScreenDistance_mB2CA2653DB9CA9BE250348AAFC6701589530F6AF,
	LeanGesture_GetStartScreenDistance_mC0EA85608522FF162FD6E9F312C79ECFCA19AC98,
	LeanGesture_GetStartScreenDistance_mDD29D9ACE3D03F7318060204EB05D7F43CE1BD30,
	LeanGesture_TryGetStartScreenDistance_m7894CF4766FC28B51D1BEBEB02C5521732A8539B,
	LeanGesture_GetStartScaledDistance_mBE1FF8BB4188B541E2B42B039CEA8FE76EECA2F9,
	LeanGesture_GetStartScaledDistance_m43AAB7E46D2833EDE932B93DAE960BC46F485DFE,
	LeanGesture_GetStartScaledDistance_mFD7F591A3F5B3A308A557EBBDD94F46A0D13C62C,
	LeanGesture_TryGetStartScaledDistance_m8405F3CFF4E15AB0EEC647500BB4431D38DC3555,
	LeanGesture_GetPinchScale_mD6168FFE37DC887B5803DD979C5DDE0E93E1606B,
	LeanGesture_GetPinchScale_mBFF1DC81C5B25C59831945FFF2DDE0909F907753,
	LeanGesture_TryGetPinchScale_m432860AFE690B9E49C26E56B381C4BBAABD977FD,
	LeanGesture_GetPinchRatio_mDA8ED0D67FB5C4CBFC56A1C665551A6E0F1F9B2F,
	LeanGesture_GetPinchRatio_m9FEA853F6A3F9AF930361BF20310C153D2828F5F,
	LeanGesture_TryGetPinchRatio_mAAF982BC35CD0CE9C0FC97238151C2FD7DD82807,
	LeanGesture_GetTwistDegrees_m18FB7974DA7A7721E2824AD3193112C213726765,
	LeanGesture_GetTwistDegrees_m5A181D7665683D6BDD079393230F57215808092E,
	LeanGesture_GetTwistDegrees_mA3780F085953F752018F41F92E88A09E8165334F,
	LeanGesture_TryGetTwistDegrees_m3D406A35E046BAE35811D9358348942391429264,
	LeanGesture_GetTwistRadians_m72BA39E88552A9306F7C2C6F0069AF6153957758,
	LeanGesture_GetTwistRadians_m8A14814AF562024C9A965AB8649692527E200523,
	LeanGesture_GetTwistRadians_m2223C606D6D6ED508B716E624A79242D8D457FF6,
	LeanGesture_TryGetTwistRadians_mC0F3EFA57BFECDEFE2E2B602D6CCC5CF7FB0E2B0,
	LeanSnapshot_GetWorldPosition_mE02A4A0650D7E14F97CD824EB557B7BC953E8FE6,
	LeanSnapshot_Pop_m6E8E3B015721A7277ABEAF88B3D9092FABEFE989,
	LeanSnapshot_TryGetScreenPosition_m76796289A21DB0DEA1327510B5DC21B3B4D2FE62,
	LeanSnapshot_TryGetSnapshot_mDAA5E437929A25F41639BE1EC5135465B5299498,
	LeanSnapshot_GetLowerIndex_mF5B21BC32FC91F245DDFB277ACBF412EE3A02EBC,
	LeanSnapshot__ctor_m13D33A7A17AFD90CB14AF79835C772694E82DE4B,
	LeanSnapshot__cctor_mDAA2C4CEE81A5011E3ADC52219AD43F188F575F6,
	LeanTouch_add_OnFingerDown_m2446320CDA24F4B2FFD2B51578AE4F75A4D1AF6A,
	LeanTouch_remove_OnFingerDown_mBF0212170B21C64B9CBA9370B6B801E03C949072,
	LeanTouch_add_OnFingerUpdate_mE45099FD601D90608AD9751259D19903E10D5D44,
	LeanTouch_remove_OnFingerUpdate_m7D9EF807B234ACDDEE861D5796A576759FF27815,
	LeanTouch_add_OnFingerUp_mEF2096577CDA9B4D199625D0F8C60609612D4FE4,
	LeanTouch_remove_OnFingerUp_mA4A5415C54D5C2E49F9A423940E92D1C2D9A4DF6,
	LeanTouch_add_OnFingerOld_mC7FBE9821BC1BA0B142E1326C2E1EE89BF9EC708,
	LeanTouch_remove_OnFingerOld_mAB896E689C7D034BD745175D2C5417802D2AC2B6,
	LeanTouch_add_OnFingerTap_mED8640F02B94B9F08E1A1D2F62B8B41904F9BC2A,
	LeanTouch_remove_OnFingerTap_m444AEA0604B710058DE2690AF9C72ED75D0E0D20,
	LeanTouch_add_OnFingerSwipe_mF3672F6EA7557D61064EE33F8A1E3222097D7423,
	LeanTouch_remove_OnFingerSwipe_m8535FB791A56F635E7C84F48F5BA0D8536F0B7C1,
	LeanTouch_add_OnGesture_m174D3E742EBAB9BE94DF90D51AD91E13EB8BD0B0,
	LeanTouch_remove_OnGesture_mF6BEF7D30585383DA48137F701F2869D4D6E2E6F,
	LeanTouch_add_OnFingerExpired_m90CFBD7BF361027A4550FB815129A8F2C980BF4B,
	LeanTouch_remove_OnFingerExpired_m88B3683F171D539E80029A8FA978F0CCFA23A7FC,
	LeanTouch_add_OnFingerInactive_m3929851860A96503E1DD7D99F1DDCE3A2586598E,
	LeanTouch_remove_OnFingerInactive_m1DC16F3BACD8E0611B3E2EE67F3B5981566B0479,
	LeanTouch_get_CurrentTapThreshold_mFAC0500D58A166B0E8F6E61DD412CD893872249C,
	LeanTouch_get_CurrentSwipeThreshold_mE095832573A6DBAB2F50686AF1BD416126ABD753,
	LeanTouch_get_CurrentReferenceDpi_m94CF91557AE597F7AD9976B64285EDA51B67A1F3,
	LeanTouch_get_CurrentGuiLayers_m5BD1673C5183C16B067C6508EBAB21BAF8F38857,
	LeanTouch_get_Instance_mD14E64FF0BDAC5B20B90AF0A934F69C3BE31FCB4,
	LeanTouch_get_ScalingFactor_m617EED84247952F5B8E628DF758059459DC4420D,
	LeanTouch_get_ScreenFactor_mBA745E8A83787B715B721BEDCE05D1B8456E933D,
	LeanTouch_get_GuiInUse_mEFD8587451F7ABA0795C92330A4C012A9174B848,
	LeanTouch_PointOverGui_m9BC719836CB96EBD1A2882138DEB7EF0603C2674,
	LeanTouch_RaycastGui_m5A1F511E931D590797E344306DE07FB70CEB0A49,
	LeanTouch_RaycastGui_m26CCF03838DCD8C65042816D7DED38652E8CA1EA,
	LeanTouch_GetFingers_mE345FF052E046CB3163610E73E33B1B4FED4DDC6,
	LeanTouch_SimulateTap_m97E1150049F42F64CCB66385AF1FBDD6C46DE6FF,
	LeanTouch_Clear_mC455580A4FE48381BC7F0C3D90162724A159FEE9,
	LeanTouch_OnEnable_mFF5A315C0F8CC8F46AA856DBED2D4D236866F4D5,
	LeanTouch_OnDisable_m98BBABF0D6B9094F498276C20434B754F1876C14,
	LeanTouch_Update_mDA216CEE3AD7B68A0F96E15C1DEC3222FA99598E,
	LeanTouch_UpdateFingers_mD27614293027684A532ABAA8983AE4FDE9B10260,
	LeanTouch_OnGUI_mE1E41559E4E8D0FC1ADEEE07B881A657BE12AE9A,
	LeanTouch_BeginFingers_mDC83A558CD0C0B21BCECAECF16B0A84456ACB093,
	LeanTouch_EndFingers_m49185B4B0485735E75536B74B18A3983D6E7CCB0,
	LeanTouch_PollFingers_mF96BB27C6F0E65D36E6F013DF15A95F7AC352D6E,
	LeanTouch_UpdateEvents_mB7C1BB2A9C4F3DC5B65C8AE84AB180A7F2DF98D6,
	LeanTouch_AddFinger_mEB34D91C5A4B8CE613638480E44E1A3E954972D1,
	LeanTouch_FindFinger_m434440076CE45DBC0B30A91043830C20CDD5A005,
	LeanTouch_FindInactiveFingerIndex_m31FFBE7A878005B9578137401C9917B6DEB33CE0,
	LeanTouch__ctor_m9CA35BE5D76383B904B9DCB0BC7A3BC704DB85D1,
	LeanTouch__cctor_mFFF3DED1B3FE607086AF611640FA18DB23CBC03E,
	FingerData__ctor_mE9FB63BC10400B284B1C562FCC9BB52221208274,
	LeanFingerEvent__ctor_mC05EC48CAFC7550B3DC95C0D558FCA0ADF9863AF,
	Vector3Event__ctor_m60E3B84BF9CCF9209FB30A04F67D0FF9772EE6DB,
	Vector2Event__ctor_mB54AA670057292037FC6D18596B00CDC19EE67E0,
	LeanFingerEvent__ctor_m71A73B91CF98FFAC3D84C742489975CAFF58558B,
	Vector3Event__ctor_mC2EFC8A1743CFF95D10B3B016F7C7F6BA8FB3E73,
	Vector2Event__ctor_mA415760F47AB7C1A922D959D65CB91C353BFD4BA,
	LeanFingerEvent__ctor_mD612A446C5123142E226DE533375C90340318B51,
	Vector3Event__ctor_m620F393B70ED40F6EC234631FA237906A51B024D,
	Vector2Event__ctor_mC02FBAC35F16AC5C93E96F1E4803628523D91F27,
	IntEvent__ctor_m99681557C151A665D5FDC8FA5239105FAA279EC3,
	LeanFingerEvent__ctor_mEA623E90D605563BB01DEA534A134BA43A063B94,
	Vector3Event__ctor_m0ACA0A4790B94E511CABA0F738EB2F579B57F3D1,
	Vector2Event__ctor_m5826B47957619CDF4660B82B0BA843766387F666,
	LeanFingerEvent__ctor_m4FB05496B752552B0DFBB32D70A1C123DCF420FD,
	FloatEvent__ctor_m54BFD81526CD29222C541DFF086D12634BEBEBCF,
	Vector2Event__ctor_mF15CC2F3E21447CB7E0271235D60B89B20D4104A,
	Vector3Event__ctor_mA2DDB696B16841CBE6F604BF36DB1328EBDE079B,
	Vector3Vector3Event__ctor_m6959F1F3ACDF38D8CB51C96B5A463D1AE8DFCB53,
	LeanFingerEvent__ctor_m9E3C79E663456779F7EDF5D8CB62F109743DDEF1,
	LeanFingerEvent__ctor_m282B85DFC7872BC0910F3185B36229F93271602B,
	FloatEvent__ctor_mEF280836B4F0B3C0DB5351D38CE3BCB176AAD4EB,
	Vector2Event__ctor_mD00DCED0754F1B5079750F905A79C2C4A1D3190F,
	Vector3Event__ctor_mD07BBC817B6AAA4F64B85812CEECBDD0529EB214,
	Vector3Vector3Event__ctor_m371C03513F418FFBCF53AE4E02127EF6BC66C70A,
};
extern void LeanFingerFilter__ctor_mE72DC02BFD6E51E07FF20A8BEF9F985347483AA5_AdjustorThunk (void);
extern void LeanFingerFilter__ctor_mFB253DF5673F23327658C66A9D4AA8C0F8725263_AdjustorThunk (void);
extern void LeanFingerFilter_UpdateRequiredSelectable_m3E2BEC9AB1DAA8AB70EC2E11B8D1227945031E7D_AdjustorThunk (void);
extern void LeanFingerFilter_AddFinger_m74C4A364453ED4A22F8423749A505DF0BA5A92EE_AdjustorThunk (void);
extern void LeanFingerFilter_RemoveFinger_mF840A547635068D3F879DF19F3E17BE362E926F7_AdjustorThunk (void);
extern void LeanFingerFilter_RemoveAllFingers_mE802FAFCE7EBF3FE06ECDF1BA15F3644F85F3E9F_AdjustorThunk (void);
extern void LeanFingerFilter_GetFingers_mB21B35B96FBDE7030A77F8C129ED467C6C85EDDE_AdjustorThunk (void);
extern void LeanScreenDepth__ctor_m09C2B43694D7A2B959469447627D3B25AB095FE9_AdjustorThunk (void);
extern void LeanScreenDepth_Convert_m4CA8998FD975A5AC752171657968240E73225A49_AdjustorThunk (void);
extern void LeanScreenDepth_ConvertDelta_m2D9538AB685663572C4A7E9C7847CF643D453D0E_AdjustorThunk (void);
extern void LeanScreenDepth_TryConvert_m58F0642957BDC300666C8799D1581F0AD8A2501F_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[11] = 
{
	{ 0x06000042, LeanFingerFilter__ctor_mE72DC02BFD6E51E07FF20A8BEF9F985347483AA5_AdjustorThunk },
	{ 0x06000043, LeanFingerFilter__ctor_mFB253DF5673F23327658C66A9D4AA8C0F8725263_AdjustorThunk },
	{ 0x06000044, LeanFingerFilter_UpdateRequiredSelectable_m3E2BEC9AB1DAA8AB70EC2E11B8D1227945031E7D_AdjustorThunk },
	{ 0x06000045, LeanFingerFilter_AddFinger_m74C4A364453ED4A22F8423749A505DF0BA5A92EE_AdjustorThunk },
	{ 0x06000046, LeanFingerFilter_RemoveFinger_mF840A547635068D3F879DF19F3E17BE362E926F7_AdjustorThunk },
	{ 0x06000047, LeanFingerFilter_RemoveAllFingers_mE802FAFCE7EBF3FE06ECDF1BA15F3644F85F3E9F_AdjustorThunk },
	{ 0x06000048, LeanFingerFilter_GetFingers_mB21B35B96FBDE7030A77F8C129ED467C6C85EDDE_AdjustorThunk },
	{ 0x0600007C, LeanScreenDepth__ctor_m09C2B43694D7A2B959469447627D3B25AB095FE9_AdjustorThunk },
	{ 0x0600007D, LeanScreenDepth_Convert_m4CA8998FD975A5AC752171657968240E73225A49_AdjustorThunk },
	{ 0x0600007E, LeanScreenDepth_ConvertDelta_m2D9538AB685663572C4A7E9C7847CF643D453D0E_AdjustorThunk },
	{ 0x0600007F, LeanScreenDepth_TryConvert_m58F0642957BDC300666C8799D1581F0AD8A2501F_AdjustorThunk },
};
static const int32_t s_InvokerIndices[417] = 
{
	89,
	31,
	89,
	31,
	23,
	26,
	27,
	32,
	335,
	1435,
	1424,
	1968,
	129,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	197,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	1435,
	1435,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	31,
	2377,
	26,
	26,
	26,
	23,
	123,
	114,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	2378,
	2378,
	23,
	2379,
	2380,
	2381,
	2382,
	-1,
	135,
	3,
	2383,
	27,
	23,
	23,
	23,
	23,
	3,
	26,
	26,
	1489,
	2383,
	2384,
	21,
	2385,
	23,
	3,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	14,
	14,
	14,
	14,
	31,
	89,
	89,
	106,
	106,
	14,
	14,
	2386,
	154,
	164,
	0,
	137,
	9,
	214,
	23,
	26,
	23,
	3,
	23,
	23,
	154,
	154,
	89,
	154,
	154,
	23,
	3,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	26,
	23,
	1450,
	23,
	23,
	26,
	23,
	1450,
	23,
	23,
	26,
	23,
	1450,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	1436,
	2387,
	23,
	26,
	26,
	23,
	23,
	23,
	2378,
	2378,
	335,
	335,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	89,
	727,
	89,
	89,
	89,
	1434,
	1434,
	1434,
	1434,
	1434,
	1434,
	2388,
	727,
	2389,
	2389,
	2388,
	2388,
	2388,
	2390,
	2391,
	2391,
	2391,
	2391,
	2391,
	2392,
	2391,
	2392,
	2391,
	2391,
	2391,
	2391,
	2391,
	2391,
	2393,
	2393,
	2393,
	2393,
	2390,
	32,
	23,
	23,
	1631,
	1692,
	216,
	1631,
	1692,
	216,
	1631,
	1692,
	216,
	1631,
	1692,
	216,
	1631,
	1692,
	216,
	2394,
	2395,
	2396,
	1449,
	490,
	2397,
	2398,
	1449,
	490,
	2397,
	2398,
	1449,
	490,
	2397,
	2398,
	1449,
	490,
	2397,
	2398,
	1449,
	490,
	2397,
	2398,
	1449,
	490,
	2397,
	2398,
	441,
	2399,
	2400,
	441,
	2399,
	2400,
	1449,
	490,
	2401,
	2402,
	1449,
	490,
	2401,
	2402,
	2393,
	4,
	2403,
	2404,
	2405,
	23,
	3,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	1449,
	1449,
	106,
	2406,
	4,
	1449,
	1449,
	49,
	2407,
	2408,
	2409,
	2410,
	2411,
	23,
	23,
	23,
	23,
	1976,
	23,
	335,
	335,
	23,
	23,
	2412,
	34,
	37,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x06000033, { 0, 3 } },
	{ 0x06000034, { 3, 3 } },
	{ 0x06000035, { 6, 5 } },
	{ 0x06000036, { 11, 4 } },
	{ 0x06000037, { 15, 3 } },
	{ 0x06000038, { 18, 7 } },
	{ 0x06000080, { 25, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[28] = 
{
	{ (Il2CppRGCTXDataType)3, 21972 },
	{ (Il2CppRGCTXDataType)3, 21973 },
	{ (Il2CppRGCTXDataType)2, 24973 },
	{ (Il2CppRGCTXDataType)3, 21974 },
	{ (Il2CppRGCTXDataType)3, 21975 },
	{ (Il2CppRGCTXDataType)2, 24975 },
	{ (Il2CppRGCTXDataType)3, 21976 },
	{ (Il2CppRGCTXDataType)3, 21977 },
	{ (Il2CppRGCTXDataType)2, 24977 },
	{ (Il2CppRGCTXDataType)3, 21978 },
	{ (Il2CppRGCTXDataType)3, 21979 },
	{ (Il2CppRGCTXDataType)3, 21980 },
	{ (Il2CppRGCTXDataType)3, 21981 },
	{ (Il2CppRGCTXDataType)3, 21982 },
	{ (Il2CppRGCTXDataType)3, 21983 },
	{ (Il2CppRGCTXDataType)3, 21984 },
	{ (Il2CppRGCTXDataType)3, 21985 },
	{ (Il2CppRGCTXDataType)2, 24985 },
	{ (Il2CppRGCTXDataType)2, 24987 },
	{ (Il2CppRGCTXDataType)3, 21986 },
	{ (Il2CppRGCTXDataType)3, 21987 },
	{ (Il2CppRGCTXDataType)3, 21988 },
	{ (Il2CppRGCTXDataType)2, 24988 },
	{ (Il2CppRGCTXDataType)3, 21989 },
	{ (Il2CppRGCTXDataType)3, 21990 },
	{ (Il2CppRGCTXDataType)2, 25076 },
	{ (Il2CppRGCTXDataType)3, 21991 },
	{ (Il2CppRGCTXDataType)3, 21992 },
};
extern const Il2CppCodeGenModule g_LeanTouchCodeGenModule;
const Il2CppCodeGenModule g_LeanTouchCodeGenModule = 
{
	"LeanTouch.dll",
	417,
	s_methodPointers,
	11,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	28,
	s_rgctxValues,
	NULL,
};
