﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 Lean.Touch.LeanDeselected/LeanSelectableEvent Lean.Touch.LeanDeselected::get_OnSelectable()
extern void LeanDeselected_get_OnSelectable_mBBC8ED3614C88FC9B1F1BF94F75CD95A9B69922C (void);
// 0x00000002 System.Void Lean.Touch.LeanDeselected::OnEnable()
extern void LeanDeselected_OnEnable_m6EFA53672541D1DC3410A58590812586CD73465C (void);
// 0x00000003 System.Void Lean.Touch.LeanDeselected::OnDisable()
extern void LeanDeselected_OnDisable_mC057DFAD3F6B52623632A2E18FA83920BB16A28D (void);
// 0x00000004 System.Void Lean.Touch.LeanDeselected::HandleDeselectGlobal(Lean.Touch.LeanSelectable)
extern void LeanDeselected_HandleDeselectGlobal_m47293B3799168F828BBF30BD2D70F13E2514AC71 (void);
// 0x00000005 System.Void Lean.Touch.LeanDeselected::.ctor()
extern void LeanDeselected__ctor_mB123DE39A76B75A9A34AA744A167CAB61AF6339E (void);
// 0x00000006 System.Void Lean.Touch.LeanFormatText::set_Format(System.String)
extern void LeanFormatText_set_Format_mCD099F93A9ECA47B4B1427D52B4450FB45F13B46 (void);
// 0x00000007 System.String Lean.Touch.LeanFormatText::get_Format()
extern void LeanFormatText_get_Format_m3665531805101578ADFFAFC148C54B11270727C9 (void);
// 0x00000008 Lean.Touch.LeanFormatText/StringEvent Lean.Touch.LeanFormatText::get_OnFormatted()
extern void LeanFormatText_get_OnFormatted_m3E136D990EC866F847C8C21B99A2CDBF02F6E3D6 (void);
// 0x00000009 System.Void Lean.Touch.LeanFormatText::FormatFloat(System.Single)
extern void LeanFormatText_FormatFloat_m9898AE90E9AE87F94666598911A727E085C03D16 (void);
// 0x0000000A System.Void Lean.Touch.LeanFormatText::FormatInt(System.Int32)
extern void LeanFormatText_FormatInt_m9B0C9B5AF5B72E689CD6A91232B0C90E321FDB80 (void);
// 0x0000000B System.Void Lean.Touch.LeanFormatText::FormatString(System.String)
extern void LeanFormatText_FormatString_m2FEE59329B4CD3B98208FC04E58C1BCC67BDD13B (void);
// 0x0000000C System.Void Lean.Touch.LeanFormatText::.ctor()
extern void LeanFormatText__ctor_m91A0B64E76165217A0E43B5474D941F1F335ACDA (void);
// 0x0000000D System.Void Lean.Touch.LeanModifyFloat::set_Offset(System.Single)
extern void LeanModifyFloat_set_Offset_mFB1D84CD1E6D68BF37F66D2D3F45E095EAAD1852 (void);
// 0x0000000E System.Single Lean.Touch.LeanModifyFloat::get_Offset()
extern void LeanModifyFloat_get_Offset_m794E960CB258FD182ACF3081AE2EE8B795F253E4 (void);
// 0x0000000F System.Void Lean.Touch.LeanModifyFloat::set_ScaleByTime(System.Boolean)
extern void LeanModifyFloat_set_ScaleByTime_m1F88265F18FCA698AB2AFC604E82FECCB4324F6D (void);
// 0x00000010 System.Boolean Lean.Touch.LeanModifyFloat::get_ScaleByTime()
extern void LeanModifyFloat_get_ScaleByTime_mAA9A20B331CCA39237E8C8747DCB5A1C37D35589 (void);
// 0x00000011 System.Void Lean.Touch.LeanModifyFloat::set_Multiplier(System.Single)
extern void LeanModifyFloat_set_Multiplier_m96298FFE7887B6B8DEE33E3CE3505108C2978FC6 (void);
// 0x00000012 System.Single Lean.Touch.LeanModifyFloat::get_Multiplier()
extern void LeanModifyFloat_get_Multiplier_mE1EE7CCB4F86F16CA526CF0AEC2B3A462FD3D915 (void);
// 0x00000013 Lean.Touch.LeanModifyFloat/FloatEvent Lean.Touch.LeanModifyFloat::get_OnModified()
extern void LeanModifyFloat_get_OnModified_m5E3F35328A4AEF4E52430D70930EB44722F9337D (void);
// 0x00000014 System.Void Lean.Touch.LeanModifyFloat::ModifyValue(System.Single)
extern void LeanModifyFloat_ModifyValue_m29AB63FF7770CFE01D5DA00EBE381C657AA9D34D (void);
// 0x00000015 System.Void Lean.Touch.LeanModifyFloat::.ctor()
extern void LeanModifyFloat__ctor_mB2D7ED4CAFCA5E632C10E11012A3C7E9140EE094 (void);
// 0x00000016 System.Void Lean.Touch.LeanModifyVector2::set_Modify(Lean.Touch.LeanModifyVector2/ModifyType)
extern void LeanModifyVector2_set_Modify_m4EB2E7AE74C37C082F45AFB9F091F7679683EB66 (void);
// 0x00000017 Lean.Touch.LeanModifyVector2/ModifyType Lean.Touch.LeanModifyVector2::get_Modify()
extern void LeanModifyVector2_get_Modify_mEC70D4B4645B540916C488693314AFF5C857FD6E (void);
// 0x00000018 System.Void Lean.Touch.LeanModifyVector2::set_ScaleByTime(System.Boolean)
extern void LeanModifyVector2_set_ScaleByTime_m269969AD02DFAC1C3775182E4D51CB0AAE56EC5A (void);
// 0x00000019 System.Boolean Lean.Touch.LeanModifyVector2::get_ScaleByTime()
extern void LeanModifyVector2_get_ScaleByTime_m9C2E4A31B247554DD9B2D9991588E402C74DE1DB (void);
// 0x0000001A System.Void Lean.Touch.LeanModifyVector2::set_Multiplier(System.Single)
extern void LeanModifyVector2_set_Multiplier_m73891D92D5203B6D1F31D88BDD66AFBFF4B8CCC3 (void);
// 0x0000001B System.Single Lean.Touch.LeanModifyVector2::get_Multiplier()
extern void LeanModifyVector2_get_Multiplier_m915284ABDC9EB0E99BDD4A9FAB305186A85702C1 (void);
// 0x0000001C Lean.Touch.LeanModifyVector2/Vector2Event Lean.Touch.LeanModifyVector2::get_OnModified()
extern void LeanModifyVector2_get_OnModified_mBDFFF7D410FBBD6304147DDEB5C17102BC0EAECE (void);
// 0x0000001D System.Void Lean.Touch.LeanModifyVector2::ModifyValue(UnityEngine.Vector2)
extern void LeanModifyVector2_ModifyValue_m90F0F623194ECF750A0EF42356137927AA97400B (void);
// 0x0000001E System.Void Lean.Touch.LeanModifyVector2::.ctor()
extern void LeanModifyVector2__ctor_m38E0BEE9134590784D0BFB5E8BC7AA8314507432 (void);
// 0x0000001F System.Collections.Generic.List`1<UnityEngine.Events.UnityEvent> Lean.Touch.LeanRandomEvents::get_Events()
extern void LeanRandomEvents_get_Events_mEC215056EC54E1A7F19BB8CEF6FE08211D4842D8 (void);
// 0x00000020 System.Void Lean.Touch.LeanRandomEvents::Invoke()
extern void LeanRandomEvents_Invoke_m120D8D4D505ACE82B2184A9C9E9F4951CA037628 (void);
// 0x00000021 System.Void Lean.Touch.LeanRandomEvents::.ctor()
extern void LeanRandomEvents__ctor_m32EAC92FACFDCD0F27C6C7062178A67810336426 (void);
// 0x00000022 Lean.Touch.LeanSelected/LeanSelectableEvent Lean.Touch.LeanSelected::get_OnSelectable()
extern void LeanSelected_get_OnSelectable_m0446BB76A759FBE666F755D12DBD83B1348F79A6 (void);
// 0x00000023 System.Void Lean.Touch.LeanSelected::OnEnable()
extern void LeanSelected_OnEnable_m9111C1BFCE816A5CB9FDE989F71E8CB3A4DEB63F (void);
// 0x00000024 System.Void Lean.Touch.LeanSelected::OnDisable()
extern void LeanSelected_OnDisable_m7BADF6D38E7AE6B7680DA1EBD596BEA69C9B4EC7 (void);
// 0x00000025 System.Void Lean.Touch.LeanSelected::HandleSelectGlobal(Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger)
extern void LeanSelected_HandleSelectGlobal_m499475218995145579719D2C1CFCC377F3D0AD1D (void);
// 0x00000026 System.Void Lean.Touch.LeanSelected::.ctor()
extern void LeanSelected__ctor_m34395F9EB121959A824660D7ABDF073663BE156E (void);
// 0x00000027 Lean.Touch.LeanSelectedCount/IntEvent Lean.Touch.LeanSelectedCount::get_OnCount()
extern void LeanSelectedCount_get_OnCount_m081EE009FB095739FA79A524D60ED85B1F1E1235 (void);
// 0x00000028 UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectedCount::get_OnMatch()
extern void LeanSelectedCount_get_OnMatch_mD6F422082D39C4D778371F684DBA96ECB27083E8 (void);
// 0x00000029 UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectedCount::get_OnUnmatch()
extern void LeanSelectedCount_get_OnUnmatch_mDABE242D06C0AEAF2DA4BE411099889ABBE9D279 (void);
// 0x0000002A System.Void Lean.Touch.LeanSelectedCount::OnEnable()
extern void LeanSelectedCount_OnEnable_mA5216CE96A138EF751AD56E42515EEBAA82F7E76 (void);
// 0x0000002B System.Void Lean.Touch.LeanSelectedCount::OnDisable()
extern void LeanSelectedCount_OnDisable_mF3AB73E3FE659AA02FC69EA79A3D8043FE75537F (void);
// 0x0000002C System.Void Lean.Touch.LeanSelectedCount::HandleSelectGlobal(Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger)
extern void LeanSelectedCount_HandleSelectGlobal_m1712D4DAB1995DE14725ECD58AA7DC7C7D291CD6 (void);
// 0x0000002D System.Void Lean.Touch.LeanSelectedCount::HandleDeselectGlobal(Lean.Touch.LeanSelectable)
extern void LeanSelectedCount_HandleDeselectGlobal_mE3AE1658DAF14C370FD5B0A6A81FB38187F68C23 (void);
// 0x0000002E System.Void Lean.Touch.LeanSelectedCount::HandleDisableGlobal(Lean.Touch.LeanSelectable)
extern void LeanSelectedCount_HandleDisableGlobal_mFD93F54404A997F74DD243E0E23E5DB96B92323C (void);
// 0x0000002F System.Void Lean.Touch.LeanSelectedCount::UpdateState()
extern void LeanSelectedCount_UpdateState_m0B9554B781D3AC9B735F07AA7F49A2868BB62185 (void);
// 0x00000030 System.Void Lean.Touch.LeanSelectedCount::.ctor()
extern void LeanSelectedCount__ctor_mA7324B403FF50B71F96B566FE685D9A4237B312B (void);
// 0x00000031 System.Void Lean.Touch.LeanSelectedRatio::set_Inverse(System.Boolean)
extern void LeanSelectedRatio_set_Inverse_m6157CD08A4755539234F6387519DA5F8A74165BC (void);
// 0x00000032 System.Boolean Lean.Touch.LeanSelectedRatio::get_Inverse()
extern void LeanSelectedRatio_get_Inverse_m832568BD4D78C3AECCE980E1058D949F2182314D (void);
// 0x00000033 Lean.Touch.LeanSelectedRatio/FloatEvent Lean.Touch.LeanSelectedRatio::get_OnRatio()
extern void LeanSelectedRatio_get_OnRatio_m57917D225C4CA3D10C53F1911ED16B1541779974 (void);
// 0x00000034 System.Void Lean.Touch.LeanSelectedRatio::UpdateNow()
extern void LeanSelectedRatio_UpdateNow_m7ABF23AFCD0DB1E91B3A3D2E5E47F6E898433285 (void);
// 0x00000035 System.Void Lean.Touch.LeanSelectedRatio::Update()
extern void LeanSelectedRatio_Update_mDBA880AB965F7D9728AFB94E720AA0D925C4F5A2 (void);
// 0x00000036 System.Void Lean.Touch.LeanSelectedRatio::.ctor()
extern void LeanSelectedRatio__ctor_m482D65D74CD44DBBBC07128C87CAB47EF80003DA (void);
// 0x00000037 System.Void Lean.Touch.LeanSelectedText::set_Format(System.String)
extern void LeanSelectedText_set_Format_mCBBFEB61C389F7183D347893228A8B42AB0D726B (void);
// 0x00000038 System.String Lean.Touch.LeanSelectedText::get_Format()
extern void LeanSelectedText_get_Format_mA42E0A15A6E6003C0217DDF0DDC40E36F67EEBED (void);
// 0x00000039 Lean.Touch.LeanSelectedText/StringEvent Lean.Touch.LeanSelectedText::get_OnText()
extern void LeanSelectedText_get_OnText_mE5586339BFE70BE0162F5F72589834356EA54A04 (void);
// 0x0000003A System.Void Lean.Touch.LeanSelectedText::UpdateNow()
extern void LeanSelectedText_UpdateNow_mB7D5873EB5120646E32BD6843312FE2205FDDF7C (void);
// 0x0000003B System.Void Lean.Touch.LeanSelectedText::Update()
extern void LeanSelectedText_Update_m2D17E8C30C6A0199D50DBBE7A3324DF1019C8D81 (void);
// 0x0000003C System.Void Lean.Touch.LeanSelectedText::.ctor()
extern void LeanSelectedText__ctor_m908B99182B38CC6590E31D15C8409384C4C973D2 (void);
// 0x0000003D System.Void Lean.Touch.IDropHandler::HandleDrop(UnityEngine.GameObject,Lean.Touch.LeanFinger)
// 0x0000003E System.Void Lean.Touch.LeanDragColorMesh::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragColorMesh_AddFinger_mAA31A77D516EE9A91FE49CE573BE7B8F4389576B (void);
// 0x0000003F System.Void Lean.Touch.LeanDragColorMesh::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragColorMesh_RemoveFinger_m565A9789FD8BA6F682EE6FB2596AD59FB7A634EF (void);
// 0x00000040 System.Void Lean.Touch.LeanDragColorMesh::RemoveAllFingers()
extern void LeanDragColorMesh_RemoveAllFingers_mA3FC04C8F109D79FDB6D5B4B401F74FDAE619716 (void);
// 0x00000041 System.Void Lean.Touch.LeanDragColorMesh::Awake()
extern void LeanDragColorMesh_Awake_m70173B1F697EADD6F3C68032E72A494E44914650 (void);
// 0x00000042 System.Void Lean.Touch.LeanDragColorMesh::Update()
extern void LeanDragColorMesh_Update_m12D669F3656A2A6F3375A472A6380ABB24C6D3A9 (void);
// 0x00000043 System.Void Lean.Touch.LeanDragColorMesh::Paint(Lean.Touch.LeanFinger)
extern void LeanDragColorMesh_Paint_m5063EB9F76ED4A93E62AF6E73A66C13BB5C68179 (void);
// 0x00000044 System.Void Lean.Touch.LeanDragColorMesh::.ctor()
extern void LeanDragColorMesh__ctor_m58B0BE5F2B14AC9175CA153E5CABBB9E78521794 (void);
// 0x00000045 System.Void Lean.Touch.LeanDragDeformMesh::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragDeformMesh_AddFinger_m1753C45299BFA593F01A903BF566EAE2E5AF5C5E (void);
// 0x00000046 System.Void Lean.Touch.LeanDragDeformMesh::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragDeformMesh_RemoveFinger_mD84842835527A0A697DDF8645B59A84DA0D5492A (void);
// 0x00000047 System.Void Lean.Touch.LeanDragDeformMesh::RemoveAllFingers()
extern void LeanDragDeformMesh_RemoveAllFingers_mD27D7158169F4CDE4F1F0110A7BEEC9E8655CE77 (void);
// 0x00000048 System.Void Lean.Touch.LeanDragDeformMesh::Awake()
extern void LeanDragDeformMesh_Awake_m32F8B543962C7A21A698D5AA06DF6A64FFECF91C (void);
// 0x00000049 System.Void Lean.Touch.LeanDragDeformMesh::Update()
extern void LeanDragDeformMesh_Update_mA09DFD7193FE6D26B84511B22B787776486BFAC4 (void);
// 0x0000004A System.Void Lean.Touch.LeanDragDeformMesh::.ctor()
extern void LeanDragDeformMesh__ctor_m0DB9FE34BB84BCB2692CE760F0826FD36A3F0364 (void);
// 0x0000004B Lean.Touch.LeanDragLine/Vector3Event Lean.Touch.LeanDragLine::get_OnReleasedFrom()
extern void LeanDragLine_get_OnReleasedFrom_m75A60DDEF8546F74165ADEDBEF5B8D86F4C9A2BC (void);
// 0x0000004C Lean.Touch.LeanDragLine/Vector3Event Lean.Touch.LeanDragLine::get_OnReleasedTo()
extern void LeanDragLine_get_OnReleasedTo_m9D75DFF603C9250A5A2E5297EB8C5F092EB61873 (void);
// 0x0000004D Lean.Touch.LeanDragLine/Vector3Event Lean.Touch.LeanDragLine::get_OnReleasedDelta()
extern void LeanDragLine_get_OnReleasedDelta_mF64F77A3A624BBBB87BFDE9BE4B862FFDF99D80E (void);
// 0x0000004E Lean.Touch.LeanDragLine/Vector3Vector3Event Lean.Touch.LeanDragLine::get_OnReleasedFromTo()
extern void LeanDragLine_get_OnReleasedFromTo_mDF14664AB53C5C6B11FFCE49091E949E5A9703A9 (void);
// 0x0000004F System.Void Lean.Touch.LeanDragLine::UpdateLine(Lean.Touch.LeanDragTrail/FingerData,Lean.Touch.LeanFinger,UnityEngine.LineRenderer)
extern void LeanDragLine_UpdateLine_mE65074D211E7D9B18D9D4254F657AA9AFAEB205D (void);
// 0x00000050 System.Void Lean.Touch.LeanDragLine::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanDragLine_HandleFingerUp_m2076F2E2DF0CABEE385198FD482BCFB564B36D6C (void);
// 0x00000051 System.Void Lean.Touch.LeanDragLine::.ctor()
extern void LeanDragLine__ctor_mB74185BD95139D8731C8E012A5E43550D47A5C52 (void);
// 0x00000052 System.Void Lean.Touch.LeanDragSelect::OnEnable()
extern void LeanDragSelect_OnEnable_m17586657791075CBEF620B9EE198C0A6839F69B4 (void);
// 0x00000053 System.Void Lean.Touch.LeanDragSelect::OnDisable()
extern void LeanDragSelect_OnDisable_m77C9299E07283E24D18EF3C8F74A295612728DB7 (void);
// 0x00000054 System.Void Lean.Touch.LeanDragSelect::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanDragSelect_HandleFingerDown_mA3075A4CAA06736E1A05DDDA33B3A52A0864F540 (void);
// 0x00000055 System.Void Lean.Touch.LeanDragSelect::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanDragSelect_HandleFingerUpdate_mE5BAD1D9476DC1EB17E6751015F7AAFB0ACA2E28 (void);
// 0x00000056 System.Void Lean.Touch.LeanDragSelect::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanDragSelect_HandleFingerUp_m59FF4291EBDF6B719F3C3140F38B52B118DEB04B (void);
// 0x00000057 System.Void Lean.Touch.LeanDragSelect::SelectGlobal(Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger)
extern void LeanDragSelect_SelectGlobal_m8A9A67FAE6D71BB677032B22CDBC89B017CDEA4C (void);
// 0x00000058 System.Void Lean.Touch.LeanDragSelect::.ctor()
extern void LeanDragSelect__ctor_m33FE42249ECBD1132A4C1692DE7785DF11A49A94 (void);
// 0x00000059 System.Void Lean.Touch.LeanDragTranslateAlong::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateAlong_AddFinger_m12F5E6DB8BB38CEB3B238EBE6F6C53D56EAA16B9 (void);
// 0x0000005A System.Void Lean.Touch.LeanDragTranslateAlong::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateAlong_RemoveFinger_mF12258AF2DF2154A1D75274E9887A42A78911A7D (void);
// 0x0000005B System.Void Lean.Touch.LeanDragTranslateAlong::RemoveAllFingers()
extern void LeanDragTranslateAlong_RemoveAllFingers_m8D5F3C1428810191F4C7648D7A6434BB41BD14C6 (void);
// 0x0000005C System.Void Lean.Touch.LeanDragTranslateAlong::Awake()
extern void LeanDragTranslateAlong_Awake_m6CBDFFCAD8CE631D949FFF270E4A18AE83D231A8 (void);
// 0x0000005D System.Void Lean.Touch.LeanDragTranslateAlong::Update()
extern void LeanDragTranslateAlong_Update_m21327B445D9CFEE8DD3037F907AC5CAE0E0FB435 (void);
// 0x0000005E System.Void Lean.Touch.LeanDragTranslateAlong::UpdateTranslation()
extern void LeanDragTranslateAlong_UpdateTranslation_m901C7729B4551D0484F1F70FC2B52C1C9AB12A17 (void);
// 0x0000005F System.Void Lean.Touch.LeanDragTranslateAlong::.ctor()
extern void LeanDragTranslateAlong__ctor_m84289624AE3ECCB1FDDB7311A74EAFE6F249845D (void);
// 0x00000060 System.Void Lean.Touch.LeanDragTranslateRigidbody::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateRigidbody_AddFinger_m52B365A4F4FBF7F12FA0FD1BD48897EDBB7F2A7B (void);
// 0x00000061 System.Void Lean.Touch.LeanDragTranslateRigidbody::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateRigidbody_RemoveFinger_mA4EBE8F6205AF90C483630DAE07173A1181D5F0B (void);
// 0x00000062 System.Void Lean.Touch.LeanDragTranslateRigidbody::RemoveAllFingers()
extern void LeanDragTranslateRigidbody_RemoveAllFingers_m8DE8894BAE016AFCB18F6B56B77AF851AD2608BD (void);
// 0x00000063 System.Void Lean.Touch.LeanDragTranslateRigidbody::Awake()
extern void LeanDragTranslateRigidbody_Awake_mF5424B27DDA5F7D3F1BB5E9692134AB7575ACA13 (void);
// 0x00000064 System.Void Lean.Touch.LeanDragTranslateRigidbody::OnEnable()
extern void LeanDragTranslateRigidbody_OnEnable_mFED69C61D3D26A1FAA16BDA57A6AEAC7FE635116 (void);
// 0x00000065 System.Void Lean.Touch.LeanDragTranslateRigidbody::FixedUpdate()
extern void LeanDragTranslateRigidbody_FixedUpdate_mD8B21D3F84F63D5352DC46CB1BDAA81634559CD9 (void);
// 0x00000066 System.Void Lean.Touch.LeanDragTranslateRigidbody::Update()
extern void LeanDragTranslateRigidbody_Update_m517332A3B20728A168960F26DBE4E7FAE069E767 (void);
// 0x00000067 System.Void Lean.Touch.LeanDragTranslateRigidbody::.ctor()
extern void LeanDragTranslateRigidbody__ctor_m23D71792689FE93441C3F1C34BD0B7F2ADF8499E (void);
// 0x00000068 System.Void Lean.Touch.LeanDragTranslateRigidbody2D::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateRigidbody2D_AddFinger_m787D030DBF41744145DEEE2C224EE0EB67D600B7 (void);
// 0x00000069 System.Void Lean.Touch.LeanDragTranslateRigidbody2D::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateRigidbody2D_RemoveFinger_m429398E67FCFA21D72BE0E7A5236B61FD4926A9E (void);
// 0x0000006A System.Void Lean.Touch.LeanDragTranslateRigidbody2D::RemoveAllFingers()
extern void LeanDragTranslateRigidbody2D_RemoveAllFingers_m766453F61892D9F0D16A763D55B9CDD20294A800 (void);
// 0x0000006B System.Void Lean.Touch.LeanDragTranslateRigidbody2D::Awake()
extern void LeanDragTranslateRigidbody2D_Awake_mFB0CD83B7D193493568DCF5DE3323C4BB90B6649 (void);
// 0x0000006C System.Void Lean.Touch.LeanDragTranslateRigidbody2D::OnEnable()
extern void LeanDragTranslateRigidbody2D_OnEnable_mCE3835AC590C34FDC029A21DFBA4736BAF03E0E5 (void);
// 0x0000006D System.Void Lean.Touch.LeanDragTranslateRigidbody2D::FixedUpdate()
extern void LeanDragTranslateRigidbody2D_FixedUpdate_m590B2B155ABA1835AC0015E8C64D15AE325E6C9A (void);
// 0x0000006E System.Void Lean.Touch.LeanDragTranslateRigidbody2D::Update()
extern void LeanDragTranslateRigidbody2D_Update_mE086A9B1CE7E0C55FA9F63F56854623B7517C371 (void);
// 0x0000006F System.Void Lean.Touch.LeanDragTranslateRigidbody2D::.ctor()
extern void LeanDragTranslateRigidbody2D__ctor_m25ECC1B8C8E56696748E67455343A552EC93EE58 (void);
// 0x00000070 Lean.Touch.LeanDrop/GameObjectLeanFingerEvent Lean.Touch.LeanDrop::get_OnDropped()
extern void LeanDrop_get_OnDropped_m09853A653C1709DE436B48B56B338CA353C53F73 (void);
// 0x00000071 System.Void Lean.Touch.LeanDrop::HandleDrop(UnityEngine.GameObject,Lean.Touch.LeanFinger)
extern void LeanDrop_HandleDrop_m48B7252A705C3E93D0DE201A5794FE93D883927E (void);
// 0x00000072 System.Void Lean.Touch.LeanDrop::.ctor()
extern void LeanDrop__ctor_m6D91A825CAEA68E7854C44C8EEF79EFB55B8B560 (void);
// 0x00000073 System.Void Lean.Touch.LeanDropCount::HandleDrop(UnityEngine.GameObject,Lean.Touch.LeanFinger)
extern void LeanDropCount_HandleDrop_m7BEEF850A044C7C9A7C3767E335261C773DCA10A (void);
// 0x00000074 System.Void Lean.Touch.LeanDropCount::.ctor()
extern void LeanDropCount__ctor_m6E3A1EB3C0F3BFA1016826CEB4A37CA7CC3C82A2 (void);
// 0x00000075 Lean.Touch.LeanFingerDownCanvas/LeanFingerEvent Lean.Touch.LeanFingerDownCanvas::get_OnFinger()
extern void LeanFingerDownCanvas_get_OnFinger_m678B70EE8F058D542CC89B3E86CB2E98D187BE8F (void);
// 0x00000076 Lean.Touch.LeanFingerDownCanvas/Vector3Event Lean.Touch.LeanFingerDownCanvas::get_OnWorld()
extern void LeanFingerDownCanvas_get_OnWorld_mC92EB9CD5F667B0B99439EE8D6945B94F539CA6B (void);
// 0x00000077 System.Boolean Lean.Touch.LeanFingerDownCanvas::ElementOverlapped(Lean.Touch.LeanFinger)
extern void LeanFingerDownCanvas_ElementOverlapped_m6720ECF08D80802EB9427BD1DAC5CC267159BCF6 (void);
// 0x00000078 System.Void Lean.Touch.LeanFingerDownCanvas::OnEnable()
extern void LeanFingerDownCanvas_OnEnable_m8CDE230E8911101A27B7E40BC3465107C1A3710F (void);
// 0x00000079 System.Void Lean.Touch.LeanFingerDownCanvas::OnDisable()
extern void LeanFingerDownCanvas_OnDisable_m44CF2C5D4E0ABB14274FE9679D981A23F8FECE27 (void);
// 0x0000007A System.Void Lean.Touch.LeanFingerDownCanvas::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerDownCanvas_HandleFingerDown_mA58F32E01A85D2DCA7574FF2F04F68EB4B5C8D85 (void);
// 0x0000007B System.Void Lean.Touch.LeanFingerDownCanvas::.ctor()
extern void LeanFingerDownCanvas__ctor_m149FBA10E592252B5D2A31E5C6A2BAE7F922DAFF (void);
// 0x0000007C System.Void Lean.Touch.LeanFingerFlick::Awake()
extern void LeanFingerFlick_Awake_m3B7858BC02E7BDDD1F91C26CC0F5F635E68EC555 (void);
// 0x0000007D System.Void Lean.Touch.LeanFingerFlick::OnEnable()
extern void LeanFingerFlick_OnEnable_m24D845ECFFB4553DB478468AE022C0F8D0DED1EC (void);
// 0x0000007E System.Void Lean.Touch.LeanFingerFlick::OnDisable()
extern void LeanFingerFlick_OnDisable_m6E5219EAD7B87910CD1308D0F20126726A7BADB5 (void);
// 0x0000007F System.Void Lean.Touch.LeanFingerFlick::Update()
extern void LeanFingerFlick_Update_mAE0AA6BBFE695EFA3F88B20936702E9168EF9772 (void);
// 0x00000080 System.Void Lean.Touch.LeanFingerFlick::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerFlick_HandleFingerDown_m55AEDA533194F4BC2E2DF9B2032F007F1BFCFD31 (void);
// 0x00000081 System.Void Lean.Touch.LeanFingerFlick::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerFlick_HandleFingerUp_mABA074A17FA3E27994012A424BB3A0CFE2A97E08 (void);
// 0x00000082 System.Boolean Lean.Touch.LeanFingerFlick::TestFinger(Lean.Touch.LeanFinger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFingerFlick_TestFinger_m33EF58EFDDECD6ABF54CA3C581105498286783AD (void);
// 0x00000083 System.Void Lean.Touch.LeanFingerFlick::.ctor()
extern void LeanFingerFlick__ctor_m5FA1984949DE8BD8E9A389F020DEB2E195ECC996 (void);
// 0x00000084 Lean.Touch.LeanFingerHeld/LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnFingerDown()
extern void LeanFingerHeld_get_OnFingerDown_m79EB9C4ACCA785136D57B77C9AC60CF65D561FAE (void);
// 0x00000085 Lean.Touch.LeanFingerHeld/LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnFingerUpdate()
extern void LeanFingerHeld_get_OnFingerUpdate_mC5A70CDEB0D96EB5DEE35C3F34B5D5DEA4A33C27 (void);
// 0x00000086 Lean.Touch.LeanFingerHeld/LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnFingerUp()
extern void LeanFingerHeld_get_OnFingerUp_mC4F63AB681E8F4D69EDAEEB0D4A1C230D268B3DF (void);
// 0x00000087 Lean.Touch.LeanFingerHeld/Vector3Event Lean.Touch.LeanFingerHeld::get_OnWorldDown()
extern void LeanFingerHeld_get_OnWorldDown_m79C6D7EE44AD5779893E0A58268B1B3130FF92F1 (void);
// 0x00000088 Lean.Touch.LeanFingerHeld/Vector3Event Lean.Touch.LeanFingerHeld::get_OnWorldUpdate()
extern void LeanFingerHeld_get_OnWorldUpdate_mB80A455536E61F22C36373497CAC94CC419FE492 (void);
// 0x00000089 Lean.Touch.LeanFingerHeld/Vector3Event Lean.Touch.LeanFingerHeld::get_OnWorldUp()
extern void LeanFingerHeld_get_OnWorldUp_m7C2F59FADC82C45CBEB74AB20CC91957569EC1E5 (void);
// 0x0000008A System.Void Lean.Touch.LeanFingerHeld::Awake()
extern void LeanFingerHeld_Awake_m9A7957846DAFA4055949FDA97106B3DE4111431C (void);
// 0x0000008B System.Void Lean.Touch.LeanFingerHeld::OnEnable()
extern void LeanFingerHeld_OnEnable_m9227B0083545C31FB74DA23351B0807167A3D59D (void);
// 0x0000008C System.Void Lean.Touch.LeanFingerHeld::OnDisable()
extern void LeanFingerHeld_OnDisable_mF437C683F72A4740C826BD12B20F710EC45E2F89 (void);
// 0x0000008D System.Void Lean.Touch.LeanFingerHeld::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_HandleFingerDown_m7BF90A4D7EF9A3AD0261E7DA69808846CD180C94 (void);
// 0x0000008E System.Void Lean.Touch.LeanFingerHeld::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_HandleFingerUpdate_m7B8274F0079F573933638C94EF5DA2FD537767AB (void);
// 0x0000008F System.Boolean Lean.Touch.LeanFingerHeld::IsHeld(Lean.Touch.LeanFinger,Lean.Touch.LeanFingerHeld/FingerData)
extern void LeanFingerHeld_IsHeld_m65456832C636AA898846DD0474494A122B4BD8A4 (void);
// 0x00000090 System.Void Lean.Touch.LeanFingerHeld::InvokeDown(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_InvokeDown_mE8F862E18888CDE79F05CEBB6D71C3DA7EF4AF92 (void);
// 0x00000091 System.Void Lean.Touch.LeanFingerHeld::InvokeUpdate(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_InvokeUpdate_m9AC7EE952EA0AE9BEEC5B6F1AE0A354A6FDB9F78 (void);
// 0x00000092 System.Void Lean.Touch.LeanFingerHeld::InvokeUp(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_InvokeUp_m38EAC4835A2B182E8402B86944513BA6E83EA6D8 (void);
// 0x00000093 System.Void Lean.Touch.LeanFingerHeld::.ctor()
extern void LeanFingerHeld__ctor_m07177F0765A8D943720126178AE15075ABCEC63F (void);
// 0x00000094 Lean.Touch.LeanFingerTapExpired/LeanFingerEvent Lean.Touch.LeanFingerTapExpired::get_OnFinger()
extern void LeanFingerTapExpired_get_OnFinger_m7999B4D9DC9270222CF9DE0994E4F29ED347E755 (void);
// 0x00000095 Lean.Touch.LeanFingerTapExpired/IntEvent Lean.Touch.LeanFingerTapExpired::get_OnCount()
extern void LeanFingerTapExpired_get_OnCount_mB3C8D62347E4F37B0D5376E1CEA55CC6EF88C282 (void);
// 0x00000096 Lean.Touch.LeanFingerTapExpired/Vector3Event Lean.Touch.LeanFingerTapExpired::get_OnWorld()
extern void LeanFingerTapExpired_get_OnWorld_mB5F5DA2673F390E4157DACD707B8AE400FE92B33 (void);
// 0x00000097 System.Void Lean.Touch.LeanFingerTapExpired::Awake()
extern void LeanFingerTapExpired_Awake_m850DD44586845A239552821AD97C07B99DCE0752 (void);
// 0x00000098 System.Void Lean.Touch.LeanFingerTapExpired::OnEnable()
extern void LeanFingerTapExpired_OnEnable_m92E9966FE5070213A330C8E305C331B9D8A3CEBD (void);
// 0x00000099 System.Void Lean.Touch.LeanFingerTapExpired::OnDisable()
extern void LeanFingerTapExpired_OnDisable_m5369D4A7CEBE862D0ACF120C39E0724C348723A0 (void);
// 0x0000009A System.Void Lean.Touch.LeanFingerTapExpired::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanFingerTapExpired_HandleFingerTap_mFF7DAA7CCD04A6D0F5865EF15C6BE99093AE399F (void);
// 0x0000009B System.Void Lean.Touch.LeanFingerTapExpired::HandleFingerExpired(Lean.Touch.LeanFinger)
extern void LeanFingerTapExpired_HandleFingerExpired_mBD1B29208E2D914E661D723EB301AD9F83FC9DD5 (void);
// 0x0000009C System.Void Lean.Touch.LeanFingerTapExpired::.ctor()
extern void LeanFingerTapExpired__ctor_m96DFC8EC42B5F2DB488A7CB78484F144A8E91D45 (void);
// 0x0000009D Lean.Touch.LeanFingerTapQuick/LeanFingerEvent Lean.Touch.LeanFingerTapQuick::get_OnFinger()
extern void LeanFingerTapQuick_get_OnFinger_m525C6ADC90552B037DD68D5763D9BEBB38A94EF6 (void);
// 0x0000009E Lean.Touch.LeanFingerTapQuick/Vector3Event Lean.Touch.LeanFingerTapQuick::get_OnWorld()
extern void LeanFingerTapQuick_get_OnWorld_m6C770A2C9480A39342B2FB12BA2496E7508D7C69 (void);
// 0x0000009F Lean.Touch.LeanFingerTapQuick/Vector2Event Lean.Touch.LeanFingerTapQuick::get_OnScreen()
extern void LeanFingerTapQuick_get_OnScreen_m3AD2A85FE7356175DCE8F8DA3C678570FADECE73 (void);
// 0x000000A0 System.Void Lean.Touch.LeanFingerTapQuick::OnEnable()
extern void LeanFingerTapQuick_OnEnable_m03BEC242EC2178763C72A215C20C73F53EDBAE17 (void);
// 0x000000A1 System.Void Lean.Touch.LeanFingerTapQuick::OnDisable()
extern void LeanFingerTapQuick_OnDisable_mB1A97BE3969A6A85C4273B8642C44FB2742AA092 (void);
// 0x000000A2 System.Void Lean.Touch.LeanFingerTapQuick::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerTapQuick_HandleFingerDown_m694A92A1F01E1B0B39986437649B648E7209C8A2 (void);
// 0x000000A3 System.Void Lean.Touch.LeanFingerTapQuick::.ctor()
extern void LeanFingerTapQuick__ctor_m54BD66B02B2CD1F7223DFE0DD76266E4D8D0BA4B (void);
// 0x000000A4 Lean.Touch.LeanFirstDown/LeanFingerEvent Lean.Touch.LeanFirstDown::get_OnFinger()
extern void LeanFirstDown_get_OnFinger_m1FE7FC3977DEF3021F78D4D625CA7610AEC191E6 (void);
// 0x000000A5 Lean.Touch.LeanFirstDown/Vector3Event Lean.Touch.LeanFirstDown::get_OnWorld()
extern void LeanFirstDown_get_OnWorld_m886B45CF87E8176C1FDFDAEF5E273D04F696F6F7 (void);
// 0x000000A6 Lean.Touch.LeanFirstDown/Vector2Event Lean.Touch.LeanFirstDown::get_OnScreen()
extern void LeanFirstDown_get_OnScreen_mF084F6B5171C2F46F629A28B8D9496616DE54C50 (void);
// 0x000000A7 System.Void Lean.Touch.LeanFirstDown::Awake()
extern void LeanFirstDown_Awake_m535AFEA7BF1C5EECC51380451B77A811135FC55C (void);
// 0x000000A8 System.Void Lean.Touch.LeanFirstDown::OnEnable()
extern void LeanFirstDown_OnEnable_m29471B3AF8A92E48A9CBA2A76D4AB85E2486B612 (void);
// 0x000000A9 System.Void Lean.Touch.LeanFirstDown::OnDisable()
extern void LeanFirstDown_OnDisable_mBCA025D386FB08F859E9FF12668BBCA1DEEF533C (void);
// 0x000000AA System.Void Lean.Touch.LeanFirstDown::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFirstDown_HandleFingerDown_m90770E92F34E73A5D46BF30993479E473C2ABB6A (void);
// 0x000000AB System.Void Lean.Touch.LeanFirstDown::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanFirstDown_HandleFingerUp_m953D1C46F10AD5535449D0CF10F9A2704C529FD7 (void);
// 0x000000AC System.Void Lean.Touch.LeanFirstDown::.ctor()
extern void LeanFirstDown__ctor_mEAE74CAE165F067CC4813CD18BFE7CFFFFA4CF6F (void);
// 0x000000AD System.Boolean Lean.Touch.LeanFirstDownCanvas::ElementOverlapped(Lean.Touch.LeanFinger)
extern void LeanFirstDownCanvas_ElementOverlapped_m8240F4FF9FCB12CDEEC5BA4C4B927DE8791F9992 (void);
// 0x000000AE System.Void Lean.Touch.LeanFirstDownCanvas::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFirstDownCanvas_HandleFingerDown_mE20CA7BAF01DDB96268EC5495BE596A85489CD8F (void);
// 0x000000AF System.Void Lean.Touch.LeanFirstDownCanvas::.ctor()
extern void LeanFirstDownCanvas__ctor_mEF86EE90E702DD59E90B1ACB1B505392D2DC5C25 (void);
// 0x000000B0 System.Void Lean.Touch.LeanGestureToggle::AddFinger(Lean.Touch.LeanFinger)
extern void LeanGestureToggle_AddFinger_mFDF7EA736828FEDE292AFAC9B7D31085829AB6CB (void);
// 0x000000B1 System.Void Lean.Touch.LeanGestureToggle::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanGestureToggle_RemoveFinger_m6B8564C8C77B6797235F4AEF89BDE0441B692733 (void);
// 0x000000B2 System.Void Lean.Touch.LeanGestureToggle::RemoveAllFingers()
extern void LeanGestureToggle_RemoveAllFingers_mF4CF750D8761BF53A6A6FB0B88E23059E46C328E (void);
// 0x000000B3 System.Void Lean.Touch.LeanGestureToggle::Awake()
extern void LeanGestureToggle_Awake_mDB66E4B2C2675D70561FDEAD520174B6F66755C1 (void);
// 0x000000B4 System.Void Lean.Touch.LeanGestureToggle::Update()
extern void LeanGestureToggle_Update_m4870F63BE93114F393C4F747A82D5BA2AB0CB9B5 (void);
// 0x000000B5 System.Void Lean.Touch.LeanGestureToggle::.ctor()
extern void LeanGestureToggle__ctor_m0B839BE60E35AA96F88F5275E88767D4B44865A3 (void);
// 0x000000B6 Lean.Touch.LeanLastUp/LeanFingerEvent Lean.Touch.LeanLastUp::get_OnFinger()
extern void LeanLastUp_get_OnFinger_m25113CB96F6C3BF18DB93A6F6C752E311B98A3ED (void);
// 0x000000B7 Lean.Touch.LeanLastUp/Vector3Event Lean.Touch.LeanLastUp::get_OnWorld()
extern void LeanLastUp_get_OnWorld_mFE92AEA52A597C4887580568D70637F27D5E3CE7 (void);
// 0x000000B8 Lean.Touch.LeanLastUp/Vector2Event Lean.Touch.LeanLastUp::get_OnScreen()
extern void LeanLastUp_get_OnScreen_m536C8AF95F4BB181023C71F3D54128170E2FFB06 (void);
// 0x000000B9 System.Void Lean.Touch.LeanLastUp::Awake()
extern void LeanLastUp_Awake_m32D35FAE90A881EBF9CC1B04744F93B8BA549C3D (void);
// 0x000000BA System.Void Lean.Touch.LeanLastUp::OnEnable()
extern void LeanLastUp_OnEnable_m025D87AC5854BD3CEA0B8C33DA11E1845757FA7A (void);
// 0x000000BB System.Void Lean.Touch.LeanLastUp::OnDisable()
extern void LeanLastUp_OnDisable_m60ED249555211C27A76563CE5AFCB93688689DEF (void);
// 0x000000BC System.Void Lean.Touch.LeanLastUp::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanLastUp_HandleFingerDown_mBC5BEB36346B7E400F206E92511E7571FFBE33DB (void);
// 0x000000BD System.Void Lean.Touch.LeanLastUp::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanLastUp_HandleFingerUp_mF58BA6247A4712CC60BAFBB1902AB3D60FEA2CC6 (void);
// 0x000000BE System.Void Lean.Touch.LeanLastUp::.ctor()
extern void LeanLastUp__ctor_m994DE50833BDB1043266E70BF8B1C8AF9BF915D3 (void);
// 0x000000BF System.Boolean Lean.Touch.LeanLastUpCanvas::ElementOverlapped(Lean.Touch.LeanFinger)
extern void LeanLastUpCanvas_ElementOverlapped_m2DCB427A43213EF8F991E6C4AFCB68256AB631F3 (void);
// 0x000000C0 System.Void Lean.Touch.LeanLastUpCanvas::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanLastUpCanvas_HandleFingerDown_m5DF8ECC8618C64E29C6F38BEB48F06E2E570E925 (void);
// 0x000000C1 System.Void Lean.Touch.LeanLastUpCanvas::.ctor()
extern void LeanLastUpCanvas__ctor_m57DB0E68A5B3502F6992029CE42772BA56C247FC (void);
// 0x000000C2 System.Void Lean.Touch.LeanManualFlick::AddFinger(Lean.Touch.LeanFinger)
extern void LeanManualFlick_AddFinger_m077D7ABB500F3FB421B388163EDAB2D5029DA8D5 (void);
// 0x000000C3 System.Void Lean.Touch.LeanManualFlick::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanManualFlick_RemoveFinger_m26133E58D8B2E27A75D690C34410408B67057B2C (void);
// 0x000000C4 System.Void Lean.Touch.LeanManualFlick::OnEnable()
extern void LeanManualFlick_OnEnable_mB9A36783AE1499268434ECC2E4F7A46859D45BC8 (void);
// 0x000000C5 System.Void Lean.Touch.LeanManualFlick::OnDisable()
extern void LeanManualFlick_OnDisable_m3148E44874EA2CD6B4C460B59CB897160CC77853 (void);
// 0x000000C6 System.Void Lean.Touch.LeanManualFlick::Update()
extern void LeanManualFlick_Update_mAED17C2912FBC504E71ED76CE8D58B6E92F7F8EC (void);
// 0x000000C7 System.Void Lean.Touch.LeanManualFlick::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanManualFlick_HandleFingerUp_mC75191648217EDA4EB1ED65CF12FA0998C7AEC21 (void);
// 0x000000C8 System.Boolean Lean.Touch.LeanManualFlick::TestFinger(Lean.Touch.LeanFinger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanManualFlick_TestFinger_m148EF2C1BD25703ECEBAF13FFA3F258B725A9743 (void);
// 0x000000C9 System.Void Lean.Touch.LeanManualFlick::.ctor()
extern void LeanManualFlick__ctor_m1FFC26087CA00D44C009EA7BA3E1318C3F02CCA3 (void);
// 0x000000CA System.Void Lean.Touch.LeanManualSwipe::AddFinger(Lean.Touch.LeanFinger)
extern void LeanManualSwipe_AddFinger_mB5A8EF0978104262B2478BED6EC60196125B6D1B (void);
// 0x000000CB System.Void Lean.Touch.LeanManualSwipe::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanManualSwipe_RemoveFinger_mBBEBB8E9B68EB9F636F2F4B4E8A33FCFDA74DA4B (void);
// 0x000000CC System.Void Lean.Touch.LeanManualSwipe::OnEnable()
extern void LeanManualSwipe_OnEnable_m3135555688923BB5B46D5604A97B6BAD8F304EB1 (void);
// 0x000000CD System.Void Lean.Touch.LeanManualSwipe::OnDisable()
extern void LeanManualSwipe_OnDisable_m872CE80703513A7D19034C1F67DF3D4B90CD600A (void);
// 0x000000CE System.Void Lean.Touch.LeanManualSwipe::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanManualSwipe_HandleFingerSwipe_mD38779743B0A86B65B3EA28C7A3D8A152F35823F (void);
// 0x000000CF System.Void Lean.Touch.LeanManualSwipe::.ctor()
extern void LeanManualSwipe__ctor_m87BDDD5999945CE66E9F3AD9DC7579C54880523C (void);
// 0x000000D0 System.Void Lean.Touch.LeanManualVelocity::AddForceA(System.Single)
extern void LeanManualVelocity_AddForceA_mA43CDB288F08C3070054101FD8E86860F56E4070 (void);
// 0x000000D1 System.Void Lean.Touch.LeanManualVelocity::AddForceB(System.Single)
extern void LeanManualVelocity_AddForceB_m70948A5A10B7C232B5DF5E53B6C8432BAB7BF81E (void);
// 0x000000D2 System.Void Lean.Touch.LeanManualVelocity::AddForceAB(UnityEngine.Vector2)
extern void LeanManualVelocity_AddForceAB_mBCFC154ADBA10761B5855103D0373E9C82E6CE1F (void);
// 0x000000D3 System.Void Lean.Touch.LeanManualVelocity::AddForceFromTo(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanManualVelocity_AddForceFromTo_mEB54553E53C9D5D23D313A2DC78ACDADF6FD6FB5 (void);
// 0x000000D4 System.Void Lean.Touch.LeanManualVelocity::AddForce(UnityEngine.Vector3)
extern void LeanManualVelocity_AddForce_m8342200418823647F4363F2EAB9E0167D12A14FA (void);
// 0x000000D5 System.Void Lean.Touch.LeanManualVelocity::.ctor()
extern void LeanManualVelocity__ctor_mB8CCF5D1EA699E1FFF0E210A3330BEC760561171 (void);
// 0x000000D6 System.Void Lean.Touch.LeanManualVelocity2D::AddForceA(System.Single)
extern void LeanManualVelocity2D_AddForceA_m8B9ADA336B5A5C8662817B6B056A1E8E4F62AA8E (void);
// 0x000000D7 System.Void Lean.Touch.LeanManualVelocity2D::AddForceB(System.Single)
extern void LeanManualVelocity2D_AddForceB_mEBCA4D3966EDD1E2F6B3A95AEA26DB1F097E4549 (void);
// 0x000000D8 System.Void Lean.Touch.LeanManualVelocity2D::AddForceAB(UnityEngine.Vector2)
extern void LeanManualVelocity2D_AddForceAB_m052DA77A35D98B2BE13B18EC2A8EEC4211D51DBF (void);
// 0x000000D9 System.Void Lean.Touch.LeanManualVelocity2D::AddForceFromTo(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanManualVelocity2D_AddForceFromTo_m2730575216A624D29A28B6E0F78394BE57495680 (void);
// 0x000000DA System.Void Lean.Touch.LeanManualVelocity2D::AddForce(UnityEngine.Vector3)
extern void LeanManualVelocity2D_AddForce_m11AD0748A38728059C99C06441D97218F9471079 (void);
// 0x000000DB System.Void Lean.Touch.LeanManualVelocity2D::.ctor()
extern void LeanManualVelocity2D__ctor_mD51F5EFBA38B9B993132A1C7697EDAD3686F4CD2 (void);
// 0x000000DC Lean.Touch.LeanMouseWheel/FloatEvent Lean.Touch.LeanMouseWheel::get_OnDelta()
extern void LeanMouseWheel_get_OnDelta_m341B415B09ADAB5056247422F2F2DCAA1A4316AC (void);
// 0x000000DD System.Void Lean.Touch.LeanMouseWheel::Awake()
extern void LeanMouseWheel_Awake_m4D10DFFAC0CDA879A5AA0BBC43EEE2CA8E62FB09 (void);
// 0x000000DE System.Void Lean.Touch.LeanMouseWheel::Update()
extern void LeanMouseWheel_Update_mD826061A9DA35F9669B08E2E320D08C80B310566 (void);
// 0x000000DF System.Void Lean.Touch.LeanMouseWheel::.ctor()
extern void LeanMouseWheel__ctor_m6D2465CD3873C1B1D01A86EBB0C038ED984DB7C3 (void);
// 0x000000E0 Lean.Touch.LeanMultiDirection/FloatEvent Lean.Touch.LeanMultiDirection::get_OnDelta()
extern void LeanMultiDirection_get_OnDelta_mE5F82A09D22D0DF3EBC4998B69D05A428EB34BB3 (void);
// 0x000000E1 System.Void Lean.Touch.LeanMultiDirection::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiDirection_AddFinger_m4F3B19C379B96BC0A717DB76E07F4910639AB8ED (void);
// 0x000000E2 System.Void Lean.Touch.LeanMultiDirection::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiDirection_RemoveFinger_m7B75CF197A711E8CC48C6A17437CA3A2B73E84E3 (void);
// 0x000000E3 System.Void Lean.Touch.LeanMultiDirection::RemoveAllFingers()
extern void LeanMultiDirection_RemoveAllFingers_m1FB651497D44492265DA9FD947AC818A5EB62DA8 (void);
// 0x000000E4 System.Void Lean.Touch.LeanMultiDirection::Awake()
extern void LeanMultiDirection_Awake_m76993C0402967DAB9EBD7524A072BC5BD605DEFC (void);
// 0x000000E5 System.Void Lean.Touch.LeanMultiDirection::Update()
extern void LeanMultiDirection_Update_mA761AD1F6E9F9BCB9B81652CDC3808AF11EB81B9 (void);
// 0x000000E6 System.Void Lean.Touch.LeanMultiDirection::.ctor()
extern void LeanMultiDirection__ctor_mDD6B74D3D1D9666BCB0DA876E92EF6705B4723CD (void);
// 0x000000E7 Lean.Touch.LeanMultiDown/LeanFingerListEvent Lean.Touch.LeanMultiDown::get_OnFingers()
extern void LeanMultiDown_get_OnFingers_mDB5A12F325E6207F665CCB043A6352392503CE88 (void);
// 0x000000E8 Lean.Touch.LeanMultiDown/Vector3Event Lean.Touch.LeanMultiDown::get_OnWorld()
extern void LeanMultiDown_get_OnWorld_m766FAE90A2DB5F24EA3D508B113055325E5EB2C4 (void);
// 0x000000E9 Lean.Touch.LeanMultiDown/Vector2Event Lean.Touch.LeanMultiDown::get_OnScreen()
extern void LeanMultiDown_get_OnScreen_m37A802B50BB88A361A837B6E6BA75B68D2A1A1F1 (void);
// 0x000000EA System.Void Lean.Touch.LeanMultiDown::Awake()
extern void LeanMultiDown_Awake_m2D8E40D945D3BAA82C263C6487BA6517E119672B (void);
// 0x000000EB System.Void Lean.Touch.LeanMultiDown::OnEnable()
extern void LeanMultiDown_OnEnable_m3528F1CAA971D9CC39F35C14FEBA837AEF9DFFE9 (void);
// 0x000000EC System.Void Lean.Touch.LeanMultiDown::OnDisable()
extern void LeanMultiDown_OnDisable_m162724039139649916BAC99508524DFB6C3BF908 (void);
// 0x000000ED System.Void Lean.Touch.LeanMultiDown::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanMultiDown_HandleFingerDown_m9C59CA9E801BD1B74F121600E9ADA95F022C4CAA (void);
// 0x000000EE System.Void Lean.Touch.LeanMultiDown::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanMultiDown_HandleFingerUp_mA4824B771A67DC68A66C37C631A92F2BCDF3DB13 (void);
// 0x000000EF System.Void Lean.Touch.LeanMultiDown::.ctor()
extern void LeanMultiDown__ctor_m71CA134BC733105487374E2B274ABFA905BE91BF (void);
// 0x000000F0 Lean.Touch.LeanMultiHeld/LeanFingerListEvent Lean.Touch.LeanMultiHeld::get_OnFingersDown()
extern void LeanMultiHeld_get_OnFingersDown_mAACCABC3276E016FF24D01DBCFA8FB3579FD1F1F (void);
// 0x000000F1 Lean.Touch.LeanMultiHeld/LeanFingerListEvent Lean.Touch.LeanMultiHeld::get_OnFingersUpdate()
extern void LeanMultiHeld_get_OnFingersUpdate_m228A71E3D1831FC263CF86BF11C24F56C578357B (void);
// 0x000000F2 Lean.Touch.LeanMultiHeld/LeanFingerListEvent Lean.Touch.LeanMultiHeld::get_OnFingersUp()
extern void LeanMultiHeld_get_OnFingersUp_m444E1DCF67FAA3732840A1A03B71ACF9ED2E1702 (void);
// 0x000000F3 Lean.Touch.LeanMultiHeld/Vector3Event Lean.Touch.LeanMultiHeld::get_OnWorldDown()
extern void LeanMultiHeld_get_OnWorldDown_m8726293C7727710DE6C166F333E8352EE59BD3D8 (void);
// 0x000000F4 Lean.Touch.LeanMultiHeld/Vector3Event Lean.Touch.LeanMultiHeld::get_OnWorldUpdate()
extern void LeanMultiHeld_get_OnWorldUpdate_m8774DBEBFAB022DF774387E9510156E3479C6D35 (void);
// 0x000000F5 Lean.Touch.LeanMultiHeld/Vector3Event Lean.Touch.LeanMultiHeld::get_OnWorldUp()
extern void LeanMultiHeld_get_OnWorldUp_m703CFB5F7E86871F0C2A10D4574483CADB75AD9E (void);
// 0x000000F6 Lean.Touch.LeanMultiHeld/Vector2Event Lean.Touch.LeanMultiHeld::get_OnScreenDown()
extern void LeanMultiHeld_get_OnScreenDown_mC9F914E072F7204438C3E93E8B704F57AF87BDD3 (void);
// 0x000000F7 Lean.Touch.LeanMultiHeld/Vector2Event Lean.Touch.LeanMultiHeld::get_OnScreenUpdate()
extern void LeanMultiHeld_get_OnScreenUpdate_m9D6DA1878ECE3BBBD4A7FC138966507DD37B7D6D (void);
// 0x000000F8 Lean.Touch.LeanMultiHeld/Vector2Event Lean.Touch.LeanMultiHeld::get_OnScreenUp()
extern void LeanMultiHeld_get_OnScreenUp_m59C64D3207252509586223BE619086BCBFE8A3CA (void);
// 0x000000F9 System.Void Lean.Touch.LeanMultiHeld::Awake()
extern void LeanMultiHeld_Awake_mC1060B4C6759BD31CB9F502D2242478B654860EF (void);
// 0x000000FA System.Void Lean.Touch.LeanMultiHeld::OnEnable()
extern void LeanMultiHeld_OnEnable_m496194383710F1AC5E6324FD3505E17622E7F691 (void);
// 0x000000FB System.Void Lean.Touch.LeanMultiHeld::OnDisable()
extern void LeanMultiHeld_OnDisable_mFE6985DFAE1536E32C2D33F44955885E2039E147 (void);
// 0x000000FC System.Void Lean.Touch.LeanMultiHeld::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanMultiHeld_HandleFingerDown_m5A32DFCF4044B2B154A3B6BFAEF3DC942C559C6F (void);
// 0x000000FD System.Void Lean.Touch.LeanMultiHeld::HandleGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanMultiHeld_HandleGesture_mC11F78F4DA05D9C0610C56EAFB874A783A4819E0 (void);
// 0x000000FE System.Boolean Lean.Touch.LeanMultiHeld::get_IsHeld()
extern void LeanMultiHeld_get_IsHeld_m153085BBE8597DC01BC67F54A181792875496F5E (void);
// 0x000000FF System.Void Lean.Touch.LeanMultiHeld::InvokeDown()
extern void LeanMultiHeld_InvokeDown_mA8A3C5E4072B8ABDFEE9BD20D6A577CC028B78B9 (void);
// 0x00000100 System.Void Lean.Touch.LeanMultiHeld::InvokeUpdate()
extern void LeanMultiHeld_InvokeUpdate_mAB9072A8E9F38B62003A747E02D44DECC13844EF (void);
// 0x00000101 System.Void Lean.Touch.LeanMultiHeld::InvokeUp()
extern void LeanMultiHeld_InvokeUp_m938BC0C3FB9A237F7754CB016A23C30748854C68 (void);
// 0x00000102 System.Void Lean.Touch.LeanMultiHeld::.ctor()
extern void LeanMultiHeld__ctor_m24902EBB016ABB7C482410D5433A91E45DB623C0 (void);
// 0x00000103 Lean.Touch.LeanMultiPinch/FloatEvent Lean.Touch.LeanMultiPinch::get_OnPinch()
extern void LeanMultiPinch_get_OnPinch_mAB5E4FE481D517A3BE39EA3F5B3D519698104D3B (void);
// 0x00000104 System.Void Lean.Touch.LeanMultiPinch::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiPinch_AddFinger_m854D688573EDBC17658E102ADE01DD4B04EAA5F6 (void);
// 0x00000105 System.Void Lean.Touch.LeanMultiPinch::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiPinch_RemoveFinger_m32FF2FBC05E43A44D605EAB3AF8513F62EE97FE2 (void);
// 0x00000106 System.Void Lean.Touch.LeanMultiPinch::RemoveAllFingers()
extern void LeanMultiPinch_RemoveAllFingers_m30E8C1E4AC7910D17FA7167EFB7F5896FB8BAA3E (void);
// 0x00000107 System.Void Lean.Touch.LeanMultiPinch::Awake()
extern void LeanMultiPinch_Awake_mDD1E37628C6FA709C00F62091828F115A83EDA0A (void);
// 0x00000108 System.Void Lean.Touch.LeanMultiPinch::Update()
extern void LeanMultiPinch_Update_m317AA0A46D8F04304C62616BF406CCFB036A0D23 (void);
// 0x00000109 System.Void Lean.Touch.LeanMultiPinch::.ctor()
extern void LeanMultiPinch__ctor_mFE28BCE52C8E2413CA73B464BFDCD5787D1CF28E (void);
// 0x0000010A Lean.Touch.LeanMultiPull/Vector2Event Lean.Touch.LeanMultiPull::get_OnVector()
extern void LeanMultiPull_get_OnVector_m6B4B245A99AA7B93FE992F09A810D47E5E82CC7A (void);
// 0x0000010B Lean.Touch.LeanMultiPull/FloatEvent Lean.Touch.LeanMultiPull::get_OnDistance()
extern void LeanMultiPull_get_OnDistance_m49FAF940F9D1F3C9A513D3EAFC9ED67D41DD2790 (void);
// 0x0000010C Lean.Touch.LeanMultiPull/Vector3Event Lean.Touch.LeanMultiPull::get_OnWorldFrom()
extern void LeanMultiPull_get_OnWorldFrom_mB49A2A19B18FD64C992BBA34FCCD8D19D3769EBA (void);
// 0x0000010D Lean.Touch.LeanMultiPull/Vector3Event Lean.Touch.LeanMultiPull::get_OnWorldTo()
extern void LeanMultiPull_get_OnWorldTo_mFC6EC793FB4E98A590B1D1BC46012D26EE348A34 (void);
// 0x0000010E Lean.Touch.LeanMultiPull/Vector3Event Lean.Touch.LeanMultiPull::get_OnWorldDelta()
extern void LeanMultiPull_get_OnWorldDelta_mED6B6288BF768152C4DA7BC7A4E1700FD422EB0E (void);
// 0x0000010F Lean.Touch.LeanMultiPull/Vector3Vector3Event Lean.Touch.LeanMultiPull::get_OnWorldFromTo()
extern void LeanMultiPull_get_OnWorldFromTo_m52EAFAAB8A46870FA1C78AF20F8FD2EA977AD890 (void);
// 0x00000110 System.Void Lean.Touch.LeanMultiPull::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiPull_AddFinger_mEB80146FFEB9A5DF88D093F4A41708E0B3F45208 (void);
// 0x00000111 System.Void Lean.Touch.LeanMultiPull::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiPull_RemoveFinger_mA3C0B2BDF9C5B3DFAA4CEF41814D109D89814C5B (void);
// 0x00000112 System.Void Lean.Touch.LeanMultiPull::RemoveAllFingers()
extern void LeanMultiPull_RemoveAllFingers_mFD2D8A0ACB9D06CED7BAF247EBCD74F3A639A39E (void);
// 0x00000113 System.Void Lean.Touch.LeanMultiPull::Awake()
extern void LeanMultiPull_Awake_mD81DCFD20BAEFADE73E77DF6655452DED221DBC1 (void);
// 0x00000114 System.Void Lean.Touch.LeanMultiPull::Update()
extern void LeanMultiPull_Update_m7D2955AE45E4EF3F940238FA002A899C4C06C11A (void);
// 0x00000115 System.Void Lean.Touch.LeanMultiPull::.ctor()
extern void LeanMultiPull__ctor_mD7CF17AF16D7B3B13011888D9CC68DE0B95B6D2F (void);
// 0x00000116 Lean.Touch.LeanMultiSwipe/FingerListEvent Lean.Touch.LeanMultiSwipe::get_OnFingers()
extern void LeanMultiSwipe_get_OnFingers_mD3A8460AA714F7212F65EAAE36D34DFC1E8C1440 (void);
// 0x00000117 Lean.Touch.LeanMultiSwipe/Vector2Event Lean.Touch.LeanMultiSwipe::get_OnSwipeParallel()
extern void LeanMultiSwipe_get_OnSwipeParallel_m468B2C33FDBC9A10042ACED36EB16D681B7BACBD (void);
// 0x00000118 Lean.Touch.LeanMultiSwipe/FloatEvent Lean.Touch.LeanMultiSwipe::get_OnSwipeIn()
extern void LeanMultiSwipe_get_OnSwipeIn_mACCE0B743E307FF6C9FF896630FBA613E9E81230 (void);
// 0x00000119 Lean.Touch.LeanMultiSwipe/FloatEvent Lean.Touch.LeanMultiSwipe::get_OnSwipeOut()
extern void LeanMultiSwipe_get_OnSwipeOut_m9D483F96F7A2509A17BBD284C37C69D291E61D6E (void);
// 0x0000011A System.Void Lean.Touch.LeanMultiSwipe::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiSwipe_AddFinger_m3DF826ADAD310A1236AC68F565085C958B3F6FBE (void);
// 0x0000011B System.Void Lean.Touch.LeanMultiSwipe::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiSwipe_RemoveFinger_m8CC2A796411C1CA5120201FD455158153ADD479E (void);
// 0x0000011C System.Void Lean.Touch.LeanMultiSwipe::RemoveAllFingers()
extern void LeanMultiSwipe_RemoveAllFingers_m41AF9C74E838B0C26A9198F09AD65AA2EBA221C5 (void);
// 0x0000011D System.Void Lean.Touch.LeanMultiSwipe::Awake()
extern void LeanMultiSwipe_Awake_mE2567AC460AC9D3B561195CC573659510DCFFC57 (void);
// 0x0000011E System.Void Lean.Touch.LeanMultiSwipe::Update()
extern void LeanMultiSwipe_Update_mC7D90F94E30317B923FA14A6ED3F64EE5662245D (void);
// 0x0000011F System.Void Lean.Touch.LeanMultiSwipe::FingerSwipe(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,Lean.Touch.LeanFinger)
extern void LeanMultiSwipe_FingerSwipe_mA6A441B068CACE99EF446A117ACF8129FB238035 (void);
// 0x00000120 System.Void Lean.Touch.LeanMultiSwipe::.ctor()
extern void LeanMultiSwipe__ctor_m0E8B787AD0734BA41815C725D96173B915014AA2 (void);
// 0x00000121 UnityEngine.Events.UnityEvent Lean.Touch.LeanMultiTap::get_OnTap()
extern void LeanMultiTap_get_OnTap_m855DE7F347D6779013E6BEAC792051BEB949FBAD (void);
// 0x00000122 Lean.Touch.LeanMultiTap/IntEvent Lean.Touch.LeanMultiTap::get_OnCount()
extern void LeanMultiTap_get_OnCount_mF2AECE8FB1340AEF8445E6B9E9A2F94113A10B5C (void);
// 0x00000123 Lean.Touch.LeanMultiTap/IntEvent Lean.Touch.LeanMultiTap::get_OnHighest()
extern void LeanMultiTap_get_OnHighest_m600B8A65DF0AEA7D8A410AEA8EA6EC6D8CA1A3A3 (void);
// 0x00000124 Lean.Touch.LeanMultiTap/IntIntEvent Lean.Touch.LeanMultiTap::get_OnCountHighest()
extern void LeanMultiTap_get_OnCountHighest_mBC4ABE7C4FEFC8C3FFA92B56CA7B6FE60CE30811 (void);
// 0x00000125 System.Void Lean.Touch.LeanMultiTap::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiTap_AddFinger_m6F2F458B9E424B3FB4508873EBA9CDAD5AED8B06 (void);
// 0x00000126 System.Void Lean.Touch.LeanMultiTap::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiTap_RemoveFinger_m96C66924DAA73713B54F408723EA2E338CE08F23 (void);
// 0x00000127 System.Void Lean.Touch.LeanMultiTap::RemoveAllFingers()
extern void LeanMultiTap_RemoveAllFingers_m9C4D65A65FD4CF7DF0E3A9BC24D8B9E5E75325D7 (void);
// 0x00000128 System.Void Lean.Touch.LeanMultiTap::Awake()
extern void LeanMultiTap_Awake_m4F53FDF463C08E201153EF685B2B4BA2F2C921B9 (void);
// 0x00000129 System.Void Lean.Touch.LeanMultiTap::Update()
extern void LeanMultiTap_Update_m6CD1CF211EDE5EAB4003B1F5793CDED8BC14DD1B (void);
// 0x0000012A System.Int32 Lean.Touch.LeanMultiTap::GetFingerCount(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanMultiTap_GetFingerCount_mE9FA30268672D01B076AA3CDD0882241893AF434 (void);
// 0x0000012B System.Void Lean.Touch.LeanMultiTap::.ctor()
extern void LeanMultiTap__ctor_m3EFF45F27190EB48047EEBC12463EA106BE92C76 (void);
// 0x0000012C Lean.Touch.LeanMultiTwist/FloatEvent Lean.Touch.LeanMultiTwist::get_OnTwistDegrees()
extern void LeanMultiTwist_get_OnTwistDegrees_m77FB259224F6F135CBD956F7DFAFBD43CC3A4C28 (void);
// 0x0000012D System.Void Lean.Touch.LeanMultiTwist::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiTwist_AddFinger_m27B539FBE3993FD11576EA2438100F0FC7B6C458 (void);
// 0x0000012E System.Void Lean.Touch.LeanMultiTwist::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiTwist_RemoveFinger_m363CE97E97FBF93711C5B622CB34C811AC4E3734 (void);
// 0x0000012F System.Void Lean.Touch.LeanMultiTwist::RemoveAllFingers()
extern void LeanMultiTwist_RemoveAllFingers_m3A06351D5ACC3F128A59194D52AC3F3C85426189 (void);
// 0x00000130 System.Void Lean.Touch.LeanMultiTwist::Awake()
extern void LeanMultiTwist_Awake_m6CBB23BB6557D1309419A572F81CAC8C03DF72EA (void);
// 0x00000131 System.Void Lean.Touch.LeanMultiTwist::Update()
extern void LeanMultiTwist_Update_m9EABA65B62CC0F03C0D32B25A5C2E857898D21E8 (void);
// 0x00000132 System.Void Lean.Touch.LeanMultiTwist::.ctor()
extern void LeanMultiTwist__ctor_m236BD80D423A8F01009FCE703CBE3D6F4FC2E6B6 (void);
// 0x00000133 Lean.Touch.LeanMultiUp/LeanFingerListEvent Lean.Touch.LeanMultiUp::get_OnFingers()
extern void LeanMultiUp_get_OnFingers_m66A8D4101F60F7286C0A28FFC8319A4D7A001022 (void);
// 0x00000134 Lean.Touch.LeanMultiUp/Vector3Event Lean.Touch.LeanMultiUp::get_OnWorld()
extern void LeanMultiUp_get_OnWorld_m5B7105A18A42546F53BEC64D4EBFDE1EC2F58A9D (void);
// 0x00000135 Lean.Touch.LeanMultiUp/Vector2Event Lean.Touch.LeanMultiUp::get_OnScreen()
extern void LeanMultiUp_get_OnScreen_mA6BC77DD7E7701D480EEE663A7DFF4A1D9F39A0F (void);
// 0x00000136 System.Void Lean.Touch.LeanMultiUp::Awake()
extern void LeanMultiUp_Awake_m1B5D04C5B14C44ED3842EB16F2BA196DAAA2C924 (void);
// 0x00000137 System.Void Lean.Touch.LeanMultiUp::OnEnable()
extern void LeanMultiUp_OnEnable_m4F69C4114F338691392F52B1F46595E5D0C35AF2 (void);
// 0x00000138 System.Void Lean.Touch.LeanMultiUp::OnDisable()
extern void LeanMultiUp_OnDisable_m68EE1E431B3A8B8C16D2E203276E373CC0710EF7 (void);
// 0x00000139 System.Void Lean.Touch.LeanMultiUp::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanMultiUp_HandleFingerDown_m2BF6DC3276ED40C35936B9082F5C122CFE044493 (void);
// 0x0000013A System.Void Lean.Touch.LeanMultiUp::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanMultiUp_HandleFingerUp_m330E63BE63762613985AAA787269BF1DEC4331FA (void);
// 0x0000013B System.Void Lean.Touch.LeanMultiUp::.ctor()
extern void LeanMultiUp__ctor_m9F202CBC7F2AEE80BBA87EECD2846830ABEFFC4F (void);
// 0x0000013C Lean.Touch.LeanMultiUpdate/LeanFingerListEvent Lean.Touch.LeanMultiUpdate::get_OnFingers()
extern void LeanMultiUpdate_get_OnFingers_m362E92A29B4BD12518930056D0DC93A340C496E8 (void);
// 0x0000013D Lean.Touch.LeanMultiUpdate/Vector2Event Lean.Touch.LeanMultiUpdate::get_OnDelta()
extern void LeanMultiUpdate_get_OnDelta_mE2AA3138255583145DCF3FDB239A03374E8417D5 (void);
// 0x0000013E Lean.Touch.LeanMultiUpdate/FloatEvent Lean.Touch.LeanMultiUpdate::get_OnDistance()
extern void LeanMultiUpdate_get_OnDistance_m4C77437185EB840F92587AAAD43AA2D8EA202D42 (void);
// 0x0000013F Lean.Touch.LeanMultiUpdate/Vector3Event Lean.Touch.LeanMultiUpdate::get_OnWorldFrom()
extern void LeanMultiUpdate_get_OnWorldFrom_m7C0CF9F98EAD169CB3B364550E8FDDECD9A1EE4F (void);
// 0x00000140 Lean.Touch.LeanMultiUpdate/Vector3Event Lean.Touch.LeanMultiUpdate::get_OnWorldTo()
extern void LeanMultiUpdate_get_OnWorldTo_mE928782C4AD81F39BEF2C7CBE42A15D667416430 (void);
// 0x00000141 Lean.Touch.LeanMultiUpdate/Vector3Event Lean.Touch.LeanMultiUpdate::get_OnWorldDelta()
extern void LeanMultiUpdate_get_OnWorldDelta_m3D3715941032AD5A376F62996E66EFDCE7515E66 (void);
// 0x00000142 Lean.Touch.LeanMultiUpdate/Vector3Vector3Event Lean.Touch.LeanMultiUpdate::get_OnWorldFromTo()
extern void LeanMultiUpdate_get_OnWorldFromTo_m40EE02FCF12BB80465D2DC630D2094A927639661 (void);
// 0x00000143 System.Void Lean.Touch.LeanMultiUpdate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUpdate_AddFinger_m406BCD8E091B05807586409246E7E8C7B0CF195A (void);
// 0x00000144 System.Void Lean.Touch.LeanMultiUpdate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUpdate_RemoveFinger_m178A1E406CAFFC29ECF04CC7B7029DD20ED7E93B (void);
// 0x00000145 System.Void Lean.Touch.LeanMultiUpdate::RemoveAllFingers()
extern void LeanMultiUpdate_RemoveAllFingers_m9EFA346DD7E928DDCED02ADBC9E61CBA83FA62E1 (void);
// 0x00000146 System.Void Lean.Touch.LeanMultiUpdate::Awake()
extern void LeanMultiUpdate_Awake_m828094AB66F491370637B4127CE0835B4CE4480F (void);
// 0x00000147 System.Void Lean.Touch.LeanMultiUpdate::Update()
extern void LeanMultiUpdate_Update_m4A5B44C35C96FA5A9BDA6D21DFF6088B45498173 (void);
// 0x00000148 System.Void Lean.Touch.LeanMultiUpdate::.ctor()
extern void LeanMultiUpdate__ctor_m01A38DB61E98C216A0330A62F3CFE104F2317FA4 (void);
// 0x00000149 Lean.Touch.LeanMultiUpdateCanvas/LeanFingerListEvent Lean.Touch.LeanMultiUpdateCanvas::get_OnFingers()
extern void LeanMultiUpdateCanvas_get_OnFingers_mA549C79722C2A7E20A56B56FDC43A9E0F9EE1B9E (void);
// 0x0000014A Lean.Touch.LeanMultiUpdateCanvas/Vector3Event Lean.Touch.LeanMultiUpdateCanvas::get_OnWorld()
extern void LeanMultiUpdateCanvas_get_OnWorld_m7D58F904D9990D65473A4FE2305FF3FD8C837461 (void);
// 0x0000014B System.Void Lean.Touch.LeanMultiUpdateCanvas::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_AddFinger_mF6A748EDF3E6572893264EC789888E1E5E3A27EA (void);
// 0x0000014C System.Void Lean.Touch.LeanMultiUpdateCanvas::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_RemoveFinger_m4C725C2C258B1C5FB68CF50421FAC5D90E6E56EA (void);
// 0x0000014D System.Void Lean.Touch.LeanMultiUpdateCanvas::RemoveAllFingers()
extern void LeanMultiUpdateCanvas_RemoveAllFingers_mA1CB852ADF3433814FCB7CD069297911499C9E70 (void);
// 0x0000014E System.Boolean Lean.Touch.LeanMultiUpdateCanvas::ElementOverlapped(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_ElementOverlapped_mE31D2CD60522160D7B2ACA8CC1009AAA721415B4 (void);
// 0x0000014F System.Void Lean.Touch.LeanMultiUpdateCanvas::Awake()
extern void LeanMultiUpdateCanvas_Awake_m34F0EA13A8B92799A58B417102E4A39C34D01DE0 (void);
// 0x00000150 System.Void Lean.Touch.LeanMultiUpdateCanvas::OnEnable()
extern void LeanMultiUpdateCanvas_OnEnable_mAE3BEE6E50A445A216C47EBB6A1A5AA02C80821D (void);
// 0x00000151 System.Void Lean.Touch.LeanMultiUpdateCanvas::OnDisable()
extern void LeanMultiUpdateCanvas_OnDisable_m32E45AD294643FFF798572D1606C50F2E7BBFF5A (void);
// 0x00000152 System.Void Lean.Touch.LeanMultiUpdateCanvas::Update()
extern void LeanMultiUpdateCanvas_Update_m2C89464C4263ED518AC26D8AE62108E8B96E9153 (void);
// 0x00000153 System.Void Lean.Touch.LeanMultiUpdateCanvas::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_HandleFingerDown_mF0D2F81CF1D1F7D788C0D1C172388EF8A2D92BD2 (void);
// 0x00000154 System.Void Lean.Touch.LeanMultiUpdateCanvas::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_HandleFingerUp_m2114CD840BA76AC2EFF439BE45983558AA81C032 (void);
// 0x00000155 System.Void Lean.Touch.LeanMultiUpdateCanvas::.ctor()
extern void LeanMultiUpdateCanvas__ctor_mEB71ECAB087CCE3E51A547E1EE4613430632FF5A (void);
// 0x00000156 System.Void Lean.Touch.LeanPinchCamera::set_Zoom(System.Single)
extern void LeanPinchCamera_set_Zoom_m993396FB6905F8667AD5FDC04695B94282362BBD (void);
// 0x00000157 System.Single Lean.Touch.LeanPinchCamera::get_Zoom()
extern void LeanPinchCamera_get_Zoom_mCEEA6CA18A8E00EABD30A5FADB29E7BD658A2F77 (void);
// 0x00000158 System.Void Lean.Touch.LeanPinchCamera::ContinuouslyZoom(System.Single)
extern void LeanPinchCamera_ContinuouslyZoom_m2BE0B2752EFB7984AA69164931744E132FD3D73D (void);
// 0x00000159 System.Void Lean.Touch.LeanPinchCamera::MultiplyZoom(System.Single)
extern void LeanPinchCamera_MultiplyZoom_mD5A59EAA94D69DE5C813DB4BFFD740DB0EABB510 (void);
// 0x0000015A System.Void Lean.Touch.LeanPinchCamera::IncrementZoom(System.Single)
extern void LeanPinchCamera_IncrementZoom_m63AD75FB4743748B91C29AFC57073BDAE670D379 (void);
// 0x0000015B System.Void Lean.Touch.LeanPinchCamera::AddFinger(Lean.Touch.LeanFinger)
extern void LeanPinchCamera_AddFinger_mF8AC437A32108C5CDE208D391A30A5399C5C7490 (void);
// 0x0000015C System.Void Lean.Touch.LeanPinchCamera::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanPinchCamera_RemoveFinger_m0D9C5A61FAEA37A0321EA6C2BC6BBC2D27539C13 (void);
// 0x0000015D System.Void Lean.Touch.LeanPinchCamera::RemoveAllFingers()
extern void LeanPinchCamera_RemoveAllFingers_m482E81B91CEBC6282926A4CFBCC8061B530A3300 (void);
// 0x0000015E System.Void Lean.Touch.LeanPinchCamera::Awake()
extern void LeanPinchCamera_Awake_mE724996AFEDBD475AF85B1F3C2F56DAD9E399F79 (void);
// 0x0000015F System.Void Lean.Touch.LeanPinchCamera::Start()
extern void LeanPinchCamera_Start_m0997AF9855BA5742100A8115948C1DBF4AE41D8C (void);
// 0x00000160 System.Void Lean.Touch.LeanPinchCamera::LateUpdate()
extern void LeanPinchCamera_LateUpdate_m3DD4B89BD8575F03EFE48842B96141B32F823E50 (void);
// 0x00000161 System.Void Lean.Touch.LeanPinchCamera::SetZoom(System.Single)
extern void LeanPinchCamera_SetZoom_m83B8549BB2598B44B36C36465717824215CC01BF (void);
// 0x00000162 System.Single Lean.Touch.LeanPinchCamera::TryClamp(System.Single)
extern void LeanPinchCamera_TryClamp_m897B1E966EEC41C9E475D82F35DD25FEE51FD1F8 (void);
// 0x00000163 System.Void Lean.Touch.LeanPinchCamera::.ctor()
extern void LeanPinchCamera__ctor_mAA431E2C7323AD8F2D8D1D93FA072F9B793A91DD (void);
// 0x00000164 Lean.Touch.LeanFingerDownTap/LeanFingerEvent Lean.Touch.LeanFingerDownTap::get_OnFinger()
extern void LeanFingerDownTap_get_OnFinger_m0CB430C353F6D5F1815315B3DF55EE0B0AA6C02E (void);
// 0x00000165 Lean.Touch.LeanFingerDownTap/Vector3Event Lean.Touch.LeanFingerDownTap::get_OnWorld()
extern void LeanFingerDownTap_get_OnWorld_m9A935234A5906011FD59461D48FFED8AE3666A82 (void);
// 0x00000166 Lean.Touch.LeanFingerDownTap/Vector2Event Lean.Touch.LeanFingerDownTap::get_OnScreen()
extern void LeanFingerDownTap_get_OnScreen_m2666F4AC9D7EF0CC0B64FB8AB9BFEB0C97C78CEE (void);
// 0x00000167 System.Void Lean.Touch.LeanFingerDownTap::OnEnable()
extern void LeanFingerDownTap_OnEnable_m9E0BB5A06F4643A8D12C09926A2E93D3CF1343F8 (void);
// 0x00000168 System.Void Lean.Touch.LeanFingerDownTap::OnDisable()
extern void LeanFingerDownTap_OnDisable_m7F0D856FFCFC9BE2959059970985F092BB65AAFF (void);
// 0x00000169 System.Void Lean.Touch.LeanFingerDownTap::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerDownTap_HandleFingerDown_mD32CE3096C55DA4EFC87E03154F94D942AF58314 (void);
// 0x0000016A System.Void Lean.Touch.LeanFingerDownTap::.ctor()
extern void LeanFingerDownTap__ctor_mDC1884F7BF9DD6E7765FC844DE1985B53795061A (void);
// 0x0000016B System.Void Lean.Touch.LeanReplayFinger::Replay()
extern void LeanReplayFinger_Replay_m9492F34737BF38EA14F2B1A9821A01696F7FE3C7 (void);
// 0x0000016C System.Void Lean.Touch.LeanReplayFinger::StopReplay()
extern void LeanReplayFinger_StopReplay_m1490CEB676F49AB909B75060E312885B34CA9E72 (void);
// 0x0000016D System.Void Lean.Touch.LeanReplayFinger::OnEnable()
extern void LeanReplayFinger_OnEnable_mE7EC7A7401A9F706744E19D927396381E8D33B83 (void);
// 0x0000016E System.Void Lean.Touch.LeanReplayFinger::OnDisable()
extern void LeanReplayFinger_OnDisable_m792FBA952CF0D90A226D70830C9EE99596141ADD (void);
// 0x0000016F System.Void Lean.Touch.LeanReplayFinger::Update()
extern void LeanReplayFinger_Update_m3DF85FF69BD92E41C366725C7F81384B85B879E7 (void);
// 0x00000170 System.Void Lean.Touch.LeanReplayFinger::HandleFingerSet(Lean.Touch.LeanFinger)
extern void LeanReplayFinger_HandleFingerSet_mA7775E0A49C79ECAC39169EB850815FC8802C442 (void);
// 0x00000171 System.Void Lean.Touch.LeanReplayFinger::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanReplayFinger_HandleFingerUp_m0FCE193517AC0E2BEAF8F87FDE9DBEE4CF95153B (void);
// 0x00000172 System.Void Lean.Touch.LeanReplayFinger::CopySnapshots(Lean.Touch.LeanFinger)
extern void LeanReplayFinger_CopySnapshots_m4CFFB081D9A871E68F247CC6E126C359EDD75447 (void);
// 0x00000173 System.Void Lean.Touch.LeanReplayFinger::.ctor()
extern void LeanReplayFinger__ctor_m4ED2A4FF3647FEA28E6D33D5F884EB882F6BFF37 (void);
// 0x00000174 Lean.Touch.LeanSelectSelf/LeanFingerEvent Lean.Touch.LeanSelectSelf::get_OnFinger()
extern void LeanSelectSelf_get_OnFinger_m2939EA3835B88F47E40B8ACEF020C458D2F1C781 (void);
// 0x00000175 Lean.Touch.LeanSelectSelf/Vector3Event Lean.Touch.LeanSelectSelf::get_OnWorld()
extern void LeanSelectSelf_get_OnWorld_mCE6F329B203EEC13E8179DFB5AAB4D985239F874 (void);
// 0x00000176 System.Void Lean.Touch.LeanSelectSelf::TrySelect(Lean.Touch.LeanFinger,UnityEngine.Component,UnityEngine.Vector3)
extern void LeanSelectSelf_TrySelect_m08F3F8C232A502B8DC09FF7B9ECD80CD0BBC5907 (void);
// 0x00000177 System.Boolean Lean.Touch.LeanSelectSelf::TryFindInParent(UnityEngine.Transform,UnityEngine.Transform)
extern void LeanSelectSelf_TryFindInParent_mDA555964E22ABAC180681B3F533A7AED6CEC2A77 (void);
// 0x00000178 System.Boolean Lean.Touch.LeanSelectSelf::TryFindInChildren(UnityEngine.Transform,UnityEngine.Transform)
extern void LeanSelectSelf_TryFindInChildren_m3FC2108AE19138213C40BEB1C5A087696A1F17D8 (void);
// 0x00000179 System.Void Lean.Touch.LeanSelectSelf::.ctor()
extern void LeanSelectSelf__ctor_m0137ED5636A9594A0AD527E78F3760B6DF7275F2 (void);
// 0x0000017A Lean.Touch.LeanSelectableBlock Lean.Touch.LeanSelectableBlock::FindBlock(System.Int32,System.Int32)
extern void LeanSelectableBlock_FindBlock_m43AABA7BF5A60FAECA749CD345B11FB5BEB39023 (void);
// 0x0000017B System.Void Lean.Touch.LeanSelectableBlock::OnEnable()
extern void LeanSelectableBlock_OnEnable_m4861048ED88884161DFFEDA3992BC1CC50749D27 (void);
// 0x0000017C System.Void Lean.Touch.LeanSelectableBlock::OnDisable()
extern void LeanSelectableBlock_OnDisable_mF0D563E01355C635063A0931F8A01B5C9E56D20C (void);
// 0x0000017D System.Void Lean.Touch.LeanSelectableBlock::Swap(Lean.Touch.LeanSelectableBlock,Lean.Touch.LeanSelectableBlock)
extern void LeanSelectableBlock_Swap_mC9C1ABEFA598BE5A23180A9A4BAC4053BB9B6CB6 (void);
// 0x0000017E System.Void Lean.Touch.LeanSelectableBlock::Update()
extern void LeanSelectableBlock_Update_m16A904FD6C44C97FCC0FC37F7C323A4BA25BDCC9 (void);
// 0x0000017F System.Void Lean.Touch.LeanSelectableBlock::.ctor()
extern void LeanSelectableBlock__ctor_m51766876AC9433DCF8A82DA1260831808C767222 (void);
// 0x00000180 System.Void Lean.Touch.LeanSelectableBlock::.cctor()
extern void LeanSelectableBlock__cctor_m14C2F5625E95F7BEFF3A50255DF8727F5F218564 (void);
// 0x00000181 Lean.Touch.LeanSelectableCenter/Vector3Event Lean.Touch.LeanSelectableCenter::get_OnPosition()
extern void LeanSelectableCenter_get_OnPosition_m61DB7CE23B694EAC54EBDF46DC5CDC69A204259B (void);
// 0x00000182 System.Void Lean.Touch.LeanSelectableCenter::Calculate()
extern void LeanSelectableCenter_Calculate_mF82731FBDF1C5092EFA150657A3A1D128B35DDD9 (void);
// 0x00000183 System.Void Lean.Touch.LeanSelectableCenter::.ctor()
extern void LeanSelectableCenter__ctor_m2A81F05026EEF8F8DA098B9CB4ECE79A95ADB55B (void);
// 0x00000184 System.Void Lean.Touch.LeanSelectableCount::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableCount_OnSelect_mBFA8833173A2F98AFE431576BB0A5FD7215305BB (void);
// 0x00000185 System.Void Lean.Touch.LeanSelectableCount::OnDeselect()
extern void LeanSelectableCount_OnDeselect_mDDCF2B7635FCC948CFE9BED9CD4C0BDB9B439EA3 (void);
// 0x00000186 System.Void Lean.Touch.LeanSelectableCount::.ctor()
extern void LeanSelectableCount__ctor_m0D2FF060F82DF651957068150007A45C5346DCC4 (void);
// 0x00000187 System.Void Lean.Touch.LeanSelectableDial::set_Angle(System.Single)
extern void LeanSelectableDial_set_Angle_mE06BAA98649481330409CAE8B4F7417E6F2DAE0E (void);
// 0x00000188 System.Single Lean.Touch.LeanSelectableDial::get_Angle()
extern void LeanSelectableDial_get_Angle_mB927A9D38E298764C43588B0B738595FF12DDED4 (void);
// 0x00000189 Lean.Touch.LeanSelectableDial/FloatEvent Lean.Touch.LeanSelectableDial::get_OnAngleChanged()
extern void LeanSelectableDial_get_OnAngleChanged_m373B30AD619F0D3E6431C9BC591503E54B5F4D2F (void);
// 0x0000018A System.Void Lean.Touch.LeanSelectableDial::IncrementAngle(System.Single)
extern void LeanSelectableDial_IncrementAngle_m55E0ACEBE03A393E003665F09DAE63939799EDF3 (void);
// 0x0000018B System.Void Lean.Touch.LeanSelectableDial::Update()
extern void LeanSelectableDial_Update_mC105787164C2807EC167D667BF40CA8E532DD7A6 (void);
// 0x0000018C UnityEngine.Vector2 Lean.Touch.LeanSelectableDial::GetPoint(UnityEngine.Vector2)
extern void LeanSelectableDial_GetPoint_mE8879E4F2EA8A54843EAE9F5BACA65BF38BC7A2A (void);
// 0x0000018D System.Void Lean.Touch.LeanSelectableDial::.ctor()
extern void LeanSelectableDial__ctor_m8D12EF89926693F56E4BF888ADA113B778D9D23E (void);
// 0x0000018E System.Void Lean.Touch.LeanSelectableDragTorque::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableDragTorque_OnSelect_m086CD5846F30C5424372E216D0F4FF8FF8FD2B36 (void);
// 0x0000018F System.Void Lean.Touch.LeanSelectableDragTorque::Update()
extern void LeanSelectableDragTorque_Update_mE7A709514601A5A8C30EB60C98E7ECCA58CC903E (void);
// 0x00000190 System.Void Lean.Touch.LeanSelectableDragTorque::.ctor()
extern void LeanSelectableDragTorque__ctor_m08C13532F1A6DCFE15E90097392B6328FD01EF72 (void);
// 0x00000191 Lean.Touch.LeanSelectableDrop/GameObjectEvent Lean.Touch.LeanSelectableDrop::get_OnGameObject()
extern void LeanSelectableDrop_get_OnGameObject_mEAA7D49FBAB510966EF231E7222847292004A38A (void);
// 0x00000192 Lean.Touch.LeanSelectableDrop/IDropHandlerEvent Lean.Touch.LeanSelectableDrop::get_OnDropHandler()
extern void LeanSelectableDrop_get_OnDropHandler_m2224CFA16F39E38529583CF1564F14CCDCC03766 (void);
// 0x00000193 System.Void Lean.Touch.LeanSelectableDrop::OnSelectUp(Lean.Touch.LeanFinger)
extern void LeanSelectableDrop_OnSelectUp_mD734D34B292C41D290BCECEF4B2BA798E51C85BC (void);
// 0x00000194 System.Void Lean.Touch.LeanSelectableDrop::Drop(Lean.Touch.LeanFinger,UnityEngine.Component)
extern void LeanSelectableDrop_Drop_mF7D2568FFF200C6C1CAA5582D927256B74998781 (void);
// 0x00000195 System.Void Lean.Touch.LeanSelectableDrop::.ctor()
extern void LeanSelectableDrop__ctor_mF4440E259815836F692EE411A54314F8F3BD8351 (void);
// 0x00000196 System.Void Lean.Touch.LeanSelectableDrop::.cctor()
extern void LeanSelectableDrop__cctor_m193BC06AB9A6C8643D1A93E0DAC2C3D716941E05 (void);
// 0x00000197 System.Void Lean.Touch.LeanSelectablePressureScale::Update()
extern void LeanSelectablePressureScale_Update_m022E64DF49BC6E44BDB4BC11CF96DF5A191F35DD (void);
// 0x00000198 System.Void Lean.Touch.LeanSelectablePressureScale::.ctor()
extern void LeanSelectablePressureScale__ctor_m2455EEECB6B4FED6C1DB713B70F9E45769D48B5C (void);
// 0x00000199 Lean.Touch.LeanSelectableSelected/SelectableEvent Lean.Touch.LeanSelectableSelected::get_OnSelectableDown()
extern void LeanSelectableSelected_get_OnSelectableDown_m3EE3635149265E3D94774DD591EA5076C1163889 (void);
// 0x0000019A Lean.Touch.LeanSelectableSelected/SelectableEvent Lean.Touch.LeanSelectableSelected::get_OnSelectableUpdate()
extern void LeanSelectableSelected_get_OnSelectableUpdate_mEC2725751CB467A0C2B2AD5A81BDFC8BBB36806F (void);
// 0x0000019B Lean.Touch.LeanSelectableSelected/SelectableEvent Lean.Touch.LeanSelectableSelected::get_OnSelectableUp()
extern void LeanSelectableSelected_get_OnSelectableUp_m264752965CAD228DD4EBF4CEE9614DFE1D9E0D56 (void);
// 0x0000019C System.Void Lean.Touch.LeanSelectableSelected::Update()
extern void LeanSelectableSelected_Update_mA6BA7DD12376A4A94454F0F8197C642D3CCC86CF (void);
// 0x0000019D System.Void Lean.Touch.LeanSelectableSelected::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableSelected_OnSelect_m158D5CBCAF5B779EE6F901713BC30D2F38526406 (void);
// 0x0000019E System.Void Lean.Touch.LeanSelectableSelected::OnDeselect()
extern void LeanSelectableSelected_OnDeselect_m3964FE1A6D9CDACFF11631A1114E1101ACDA5B0C (void);
// 0x0000019F System.Void Lean.Touch.LeanSelectableSelected::.ctor()
extern void LeanSelectableSelected__ctor_m79FE7291EEDEE8D4811B72640BF75D368725B9A8 (void);
// 0x000001A0 System.Void Lean.Touch.LeanSelectableTime::Update()
extern void LeanSelectableTime_Update_m7A22E3C31E1E3EA34A03D98C4E39E9B535A2BD58 (void);
// 0x000001A1 System.Void Lean.Touch.LeanSelectableTime::.ctor()
extern void LeanSelectableTime__ctor_mF02706155831F49954869F3126987694C14E5E17 (void);
// 0x000001A2 System.Void Lean.Touch.LeanSelectionBox::OnEnable()
extern void LeanSelectionBox_OnEnable_mB7A1002C21794EC8F6F1A726E53A1D2D3A6F2485 (void);
// 0x000001A3 System.Void Lean.Touch.LeanSelectionBox::OnDisable()
extern void LeanSelectionBox_OnDisable_m4047421E5ADCF817B017DB466067A236C1487AB3 (void);
// 0x000001A4 System.Void Lean.Touch.LeanSelectionBox::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanSelectionBox_HandleFingerDown_m33EFB56D2D04683616C4E548699243B8A4DA1F8C (void);
// 0x000001A5 System.Void Lean.Touch.LeanSelectionBox::HandleFingerSet(Lean.Touch.LeanFinger)
extern void LeanSelectionBox_HandleFingerSet_m82D528E541DB91240DF08CB4560507C95E0B10B5 (void);
// 0x000001A6 System.Void Lean.Touch.LeanSelectionBox::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanSelectionBox_HandleFingerUp_mD789EA053D7AC7ACB9FAA039C186E311BA1EF656 (void);
// 0x000001A7 System.Void Lean.Touch.LeanSelectionBox::WriteTransform(UnityEngine.RectTransform,Lean.Touch.LeanFinger)
extern void LeanSelectionBox_WriteTransform_m95BAA7B9E57939F73AC0CC63AA40C143FF5575A4 (void);
// 0x000001A8 System.Void Lean.Touch.LeanSelectionBox::.ctor()
extern void LeanSelectionBox__ctor_mF714BE99D11D9F27F46FC75AE21D5DB509705D23 (void);
// 0x000001A9 System.Void Lean.Touch.LeanSelectionBox::.cctor()
extern void LeanSelectionBox__cctor_m98F2960AD1DE8736C5CBA88FFC51EB7D42147104 (void);
// 0x000001AA System.Int32 Lean.Touch.LeanShape::Mod(System.Int32,System.Int32)
extern void LeanShape_Mod_mA9C6A3DF40F6E9A9EB7EC086CAE16304B10AE5CA (void);
// 0x000001AB UnityEngine.Vector2 Lean.Touch.LeanShape::GetPoint(System.Int32,System.Boolean)
extern void LeanShape_GetPoint_m5A0923120ED9002B6F918401EC1614247C2636E8 (void);
// 0x000001AC System.Void Lean.Touch.LeanShape::UpdateVisual()
extern void LeanShape_UpdateVisual_m9A985EA9E1D52D1EE3C9C040FB40DE665BB32EA8 (void);
// 0x000001AD System.Void Lean.Touch.LeanShape::.ctor()
extern void LeanShape__ctor_mB13EEB87B7A3C044B924CAEB9F39608923114881 (void);
// 0x000001AE Lean.Touch.LeanShapeDetector/LeanFingerEvent Lean.Touch.LeanShapeDetector::get_OnDetected()
extern void LeanShapeDetector_get_OnDetected_m88AA64695CBFAB216DFAF76C696DCA49C449A7AE (void);
// 0x000001AF System.Void Lean.Touch.LeanShapeDetector::AddFinger(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_AddFinger_m8A63CDBB6F7CBC79F4F58D42C8BA0CADD727E9E6 (void);
// 0x000001B0 System.Void Lean.Touch.LeanShapeDetector::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_RemoveFinger_m731652375B3749266122517E4E5EF5ECF4C8E1EF (void);
// 0x000001B1 System.Void Lean.Touch.LeanShapeDetector::RemoveAllFingers()
extern void LeanShapeDetector_RemoveAllFingers_mDFA61F8413E9CF424110707BF4AD8AA5439D0B6F (void);
// 0x000001B2 System.Void Lean.Touch.LeanShapeDetector::OnEnable()
extern void LeanShapeDetector_OnEnable_mA8D1D253605A03AB1BACF69F21386B600FB75648 (void);
// 0x000001B3 System.Void Lean.Touch.LeanShapeDetector::OnDisable()
extern void LeanShapeDetector_OnDisable_m0519E2D2FE3ED5F4B5344F92394969D7518D37CF (void);
// 0x000001B4 System.Void Lean.Touch.LeanShapeDetector::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_HandleFingerDown_m6C41D86D09A3EFDC170B14B90224374FACE83283 (void);
// 0x000001B5 System.Void Lean.Touch.LeanShapeDetector::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_HandleFingerUpdate_m7FAE0408CA7DEE0E7F85FCD57C892A7CCA87766A (void);
// 0x000001B6 System.Void Lean.Touch.LeanShapeDetector::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_HandleFingerUp_mE35CDAA113A499C475B9EE7A5BE754C3106BB210 (void);
// 0x000001B7 System.Void Lean.Touch.LeanShapeDetector::AddRange(System.Int32,System.Int32)
extern void LeanShapeDetector_AddRange_mD0AC26145E785B2380D95EEDFA914A42CEF01C74 (void);
// 0x000001B8 System.Boolean Lean.Touch.LeanShapeDetector::CalculateMatch(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Single,System.Single,System.Int32,System.Int32)
extern void LeanShapeDetector_CalculateMatch_mEFB57B24AAD0F268622EDFC3F6036EAC269B2D0A (void);
// 0x000001B9 System.Void Lean.Touch.LeanShapeDetector::FitShape(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void LeanShapeDetector_FitShape_mC52E202BE46D78F12F8FF3D99F1BC0B4D5FBA878 (void);
// 0x000001BA System.Void Lean.Touch.LeanShapeDetector::ConvertPoints(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Int32,System.Int32)
extern void LeanShapeDetector_ConvertPoints_m3C2A6D83CB1EF362966A2F0A9275BB359D41BD09 (void);
// 0x000001BB UnityEngine.Vector2 Lean.Touch.LeanShapeDetector::Read(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Int32)
extern void LeanShapeDetector_Read_m134C4151F4A1EBE286F4851847B842E34D960502 (void);
// 0x000001BC UnityEngine.Rect Lean.Touch.LeanShapeDetector::GetRect(System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void LeanShapeDetector_GetRect_m49678BF1AA98E5CBCB12CC6D973EDCEF63A89013 (void);
// 0x000001BD System.Void Lean.Touch.LeanShapeDetector::.ctor()
extern void LeanShapeDetector__ctor_mA0578DD384033F9FAC1B1BE1A99FE81A956F3CFA (void);
// 0x000001BE System.Void Lean.Touch.LeanShapeDetector::.cctor()
extern void LeanShapeDetector__cctor_m02BBD424BF13865C4493C0DBF1B7C79B0CD0C5CD (void);
// 0x000001BF System.Void Lean.Touch.LeanSpawnWithFinger::Spawn(Lean.Touch.LeanFinger)
extern void LeanSpawnWithFinger_Spawn_m7B88A5BE93DB3947D35F3FFD191A24225ED1E5FF (void);
// 0x000001C0 System.Void Lean.Touch.LeanSpawnWithFinger::OnEnable()
extern void LeanSpawnWithFinger_OnEnable_m2A88FFA343CA960D22744332B7EAE4B5BEFF95F7 (void);
// 0x000001C1 System.Void Lean.Touch.LeanSpawnWithFinger::OnDisable()
extern void LeanSpawnWithFinger_OnDisable_m8633E4CBD95D607BC247EF69E8CE5FC71CF4A356 (void);
// 0x000001C2 System.Void Lean.Touch.LeanSpawnWithFinger::Update()
extern void LeanSpawnWithFinger_Update_m05192F6CFBD8FF8DBB8FFAD5748FC7CF0C324E7A (void);
// 0x000001C3 System.Void Lean.Touch.LeanSpawnWithFinger::UpdateSpawnedTransform(Lean.Touch.LeanFinger,UnityEngine.Transform)
extern void LeanSpawnWithFinger_UpdateSpawnedTransform_mDE1F0B78EB0DF131E334929DB5492F5DF275AEF1 (void);
// 0x000001C4 System.Void Lean.Touch.LeanSpawnWithFinger::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanSpawnWithFinger_HandleFingerUp_mA7E3129AE8CB8D932D7414C9437C99B5ACA991C8 (void);
// 0x000001C5 System.Void Lean.Touch.LeanSpawnWithFinger::.ctor()
extern void LeanSpawnWithFinger__ctor_mE018A6379FCEA059BD6851C90D4AEAA0B8305D8D (void);
// 0x000001C6 System.Void Lean.Touch.LeanSpawnWithFinger::.cctor()
extern void LeanSpawnWithFinger__cctor_m97C86DD6F6E5BE488C1CC580B00889AC3168D5B7 (void);
// 0x000001C7 UnityEngine.Events.UnityEvent Lean.Touch.LeanSwipeEdge::get_OnEdge()
extern void LeanSwipeEdge_get_OnEdge_m1BD57424A4B69E661270AEF52822A2558F2488B5 (void);
// 0x000001C8 System.Void Lean.Touch.LeanSwipeEdge::CheckBetween(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanSwipeEdge_CheckBetween_m3A2293756FD47E3B0DD85AE3E84D27DD172327F4 (void);
// 0x000001C9 System.Void Lean.Touch.LeanSwipeEdge::AddFinger(Lean.Touch.LeanFinger)
extern void LeanSwipeEdge_AddFinger_m7A30AAFCB3F0E0B10AD5135488CFFDD9F1979C37 (void);
// 0x000001CA System.Void Lean.Touch.LeanSwipeEdge::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanSwipeEdge_RemoveFinger_m6782D8A94B0D240EAB75A96216D14653DA3E4096 (void);
// 0x000001CB System.Void Lean.Touch.LeanSwipeEdge::RemoveAllFingers()
extern void LeanSwipeEdge_RemoveAllFingers_m4B5A1A9396C074BC4050D5E174D80A4814BCD886 (void);
// 0x000001CC System.Void Lean.Touch.LeanSwipeEdge::Awake()
extern void LeanSwipeEdge_Awake_mF5E03FDCF760BA9F933EE63C853E10E3E85BB959 (void);
// 0x000001CD System.Void Lean.Touch.LeanSwipeEdge::Update()
extern void LeanSwipeEdge_Update_mE12B77020D9BA077D050696B5B83113C522386B5 (void);
// 0x000001CE System.Void Lean.Touch.LeanSwipeEdge::InvokeEdge()
extern void LeanSwipeEdge_InvokeEdge_m57B9682317B2D34649E43C9135D0B934E9DEF37C (void);
// 0x000001CF System.Boolean Lean.Touch.LeanSwipeEdge::CheckAngle(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3 (void);
// 0x000001D0 System.Boolean Lean.Touch.LeanSwipeEdge::CheckEdge(System.Single)
extern void LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B (void);
// 0x000001D1 System.Void Lean.Touch.LeanSwipeEdge::.ctor()
extern void LeanSwipeEdge__ctor_mBABEA72C9A8D796264627C7EAD9D7EECECD79A53 (void);
// 0x000001D2 System.Void Lean.Touch.LeanTwistCamera::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistCamera_AddFinger_mDFBE1DF25C6AEBE831E491F4B1B9A61B507A3ECD (void);
// 0x000001D3 System.Void Lean.Touch.LeanTwistCamera::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistCamera_RemoveFinger_m490128386DCFEF3597E463F42BC7AE86D461B31D (void);
// 0x000001D4 System.Void Lean.Touch.LeanTwistCamera::RemoveAllFingers()
extern void LeanTwistCamera_RemoveAllFingers_mE74F8D9E7FA9CB73D2F7E8C946812D4F0E2ADE68 (void);
// 0x000001D5 System.Void Lean.Touch.LeanTwistCamera::Awake()
extern void LeanTwistCamera_Awake_m73538D6D435E1778292BA73FE291A32D59216847 (void);
// 0x000001D6 System.Void Lean.Touch.LeanTwistCamera::Update()
extern void LeanTwistCamera_Update_mF932C5BA25C5AFDC87D121E8AFCB3382083BD43B (void);
// 0x000001D7 System.Void Lean.Touch.LeanTwistCamera::.ctor()
extern void LeanTwistCamera__ctor_m99D0DB9BB0A2ED9D47AB4C18DB879D2A01412FC1 (void);
// 0x000001D8 System.Void Lean.Touch.LeanDeselected/LeanSelectableEvent::.ctor()
extern void LeanSelectableEvent__ctor_m7D938195530F4F8E01C85954427C839CEFF708F3 (void);
// 0x000001D9 System.Void Lean.Touch.LeanFormatText/StringEvent::.ctor()
extern void StringEvent__ctor_mCEE80E5500799ECBCD10F1B7698F4D9B4DE3507F (void);
// 0x000001DA System.Void Lean.Touch.LeanModifyFloat/FloatEvent::.ctor()
extern void FloatEvent__ctor_mD6FBA9BA39F8FDC8160A19DBF41B9030D0D81FB1 (void);
// 0x000001DB System.Void Lean.Touch.LeanModifyVector2/Vector2Event::.ctor()
extern void Vector2Event__ctor_m3ECD31702F0BB9FA127F067F50326642E0D8D875 (void);
// 0x000001DC System.Void Lean.Touch.LeanSelected/LeanSelectableEvent::.ctor()
extern void LeanSelectableEvent__ctor_m59A2A0568E376230D85EB8287FC3758506447F44 (void);
// 0x000001DD System.Void Lean.Touch.LeanSelectedCount/IntEvent::.ctor()
extern void IntEvent__ctor_mE049A4466C8D289ED96E94A8ED064243DFDE2AC5 (void);
// 0x000001DE System.Void Lean.Touch.LeanSelectedRatio/FloatEvent::.ctor()
extern void FloatEvent__ctor_m09663BFE2BD6832D99977F38BB45555D40A18222 (void);
// 0x000001DF System.Void Lean.Touch.LeanSelectedText/StringEvent::.ctor()
extern void StringEvent__ctor_m73A679E22F8FACE8C2A059B5AD9B436289CAF333 (void);
// 0x000001E0 System.Void Lean.Touch.LeanDragLine/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_m691D33414AE9D57E683ECF23179442AB9AAFF841 (void);
// 0x000001E1 System.Void Lean.Touch.LeanDragLine/Vector3Event::.ctor()
extern void Vector3Event__ctor_mDE688E466E2968E2DB215B751FB8518E365D93C0 (void);
// 0x000001E2 System.Void Lean.Touch.LeanDragSelect/FingerData::.ctor()
extern void FingerData__ctor_mF0049F1969B8C99D5992411510CF19E9D9EB7CF6 (void);
// 0x000001E3 System.Void Lean.Touch.LeanDrop/GameObjectLeanFingerEvent::.ctor()
extern void GameObjectLeanFingerEvent__ctor_m2C621D50BB193D1232FB782198AF06A0FF555362 (void);
// 0x000001E4 System.Void Lean.Touch.LeanFingerDownCanvas/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mD743EE12670F1C5DCD1046FBCE8886086E1C23EC (void);
// 0x000001E5 System.Void Lean.Touch.LeanFingerDownCanvas/Vector3Event::.ctor()
extern void Vector3Event__ctor_m1EADF61F42BB689C70CCA3645A6592D93820EA28 (void);
// 0x000001E6 System.Void Lean.Touch.LeanFingerFlick/FingerData::.ctor()
extern void FingerData__ctor_mAC702941384E0EC7985FFF4F5CAB47F563C3005E (void);
// 0x000001E7 System.Void Lean.Touch.LeanFingerHeld/FingerData::.ctor()
extern void FingerData__ctor_mEB2F6F15840F2B943ADA9DA47A3D656B6D3C5A3C (void);
// 0x000001E8 System.Void Lean.Touch.LeanFingerHeld/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m0867972AA5E1DE436FADD810EABB276E378D7604 (void);
// 0x000001E9 System.Void Lean.Touch.LeanFingerHeld/Vector3Event::.ctor()
extern void Vector3Event__ctor_m803DA834A2C8660841277CC583360D4AC1803514 (void);
// 0x000001EA System.Void Lean.Touch.LeanFingerTapExpired/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m0546ED0C822C04DEFC27E570373A96BC55C15AA4 (void);
// 0x000001EB System.Void Lean.Touch.LeanFingerTapExpired/Vector3Event::.ctor()
extern void Vector3Event__ctor_m7259E50792081D01CEC7805BF5E3D464E3DFC65F (void);
// 0x000001EC System.Void Lean.Touch.LeanFingerTapExpired/IntEvent::.ctor()
extern void IntEvent__ctor_m74686344DFBE90F8E7E2CC541A76C31898CB33AE (void);
// 0x000001ED System.Void Lean.Touch.LeanFingerTapQuick/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m47D4AB1E18BEB62CA7C845A2639221216FF9848B (void);
// 0x000001EE System.Void Lean.Touch.LeanFingerTapQuick/Vector3Event::.ctor()
extern void Vector3Event__ctor_mCD7FCFB8F38E0FA2F7B5400EAAB55319E5790AA3 (void);
// 0x000001EF System.Void Lean.Touch.LeanFingerTapQuick/Vector2Event::.ctor()
extern void Vector2Event__ctor_mFA66087B66F278C6F5105F17833DAF064D0183C5 (void);
// 0x000001F0 System.Void Lean.Touch.LeanFirstDown/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m6BB3FB399B13E920BF15FA1E28AE970755E24521 (void);
// 0x000001F1 System.Void Lean.Touch.LeanFirstDown/Vector3Event::.ctor()
extern void Vector3Event__ctor_m4A3FB1FBE7349933E1C910C4B6FDE9A1CF460354 (void);
// 0x000001F2 System.Void Lean.Touch.LeanFirstDown/Vector2Event::.ctor()
extern void Vector2Event__ctor_m56F1F71EA3973D20BFCDD2FA49071C7BE9354A1D (void);
// 0x000001F3 System.Void Lean.Touch.LeanLastUp/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mEE8FF1C21C51C2FC5A97DBCFB7BDBD025495F8A2 (void);
// 0x000001F4 System.Void Lean.Touch.LeanLastUp/Vector3Event::.ctor()
extern void Vector3Event__ctor_mDF252FA6C68AE09DA137452B3CE5F9F7AEEA03D7 (void);
// 0x000001F5 System.Void Lean.Touch.LeanLastUp/Vector2Event::.ctor()
extern void Vector2Event__ctor_mF9AD102DA199AE45F76F9086A8360B71B957A722 (void);
// 0x000001F6 System.Void Lean.Touch.LeanManualFlick/FingerData::.ctor()
extern void FingerData__ctor_m9944B2C76679158C780530AE95B0303FF790AB11 (void);
// 0x000001F7 System.Void Lean.Touch.LeanMouseWheel/FloatEvent::.ctor()
extern void FloatEvent__ctor_mE350F7FD4CC34D241BB2AE160E87B9F957A2896E (void);
// 0x000001F8 System.Void Lean.Touch.LeanMultiDirection/FloatEvent::.ctor()
extern void FloatEvent__ctor_mB407361C76B347F9995D9805C375C04D6255D90D (void);
// 0x000001F9 System.Void Lean.Touch.LeanMultiDown/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_m7835CF2BADC0E2BCB2FA8D3FE42C38F6CF01F1D0 (void);
// 0x000001FA System.Void Lean.Touch.LeanMultiDown/Vector3Event::.ctor()
extern void Vector3Event__ctor_m87F8FA35C2F5CAF72D3D8CF1A551E0AC009DBE72 (void);
// 0x000001FB System.Void Lean.Touch.LeanMultiDown/Vector2Event::.ctor()
extern void Vector2Event__ctor_m27112CEB04B70931AC322705A43090E63C1A0371 (void);
// 0x000001FC System.Void Lean.Touch.LeanMultiHeld/FingerData::.ctor()
extern void FingerData__ctor_m2258140B9F6C3FB9A8528C9B802A7BDDE796EC5A (void);
// 0x000001FD System.Void Lean.Touch.LeanMultiHeld/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_m61E1D4EE66582756185CC5A28B571D513B0F9461 (void);
// 0x000001FE System.Void Lean.Touch.LeanMultiHeld/Vector3Event::.ctor()
extern void Vector3Event__ctor_m33A9FF532A551C43A3E78BA8DDF5ACF0E72BA0C0 (void);
// 0x000001FF System.Void Lean.Touch.LeanMultiHeld/Vector2Event::.ctor()
extern void Vector2Event__ctor_mD809696839EF461E22513F7865C042AE8B0B4FFB (void);
// 0x00000200 System.Void Lean.Touch.LeanMultiPinch/FloatEvent::.ctor()
extern void FloatEvent__ctor_m9DFBF58C3A43E306F7AF9859A08FFDBD7C58231B (void);
// 0x00000201 System.Void Lean.Touch.LeanMultiPull/FloatEvent::.ctor()
extern void FloatEvent__ctor_mD7D8001ACB970A2D364D072C8E05E23886E292BE (void);
// 0x00000202 System.Void Lean.Touch.LeanMultiPull/Vector2Event::.ctor()
extern void Vector2Event__ctor_mC91A14FBAEAD59D7BCE67582A064B729ADEAA640 (void);
// 0x00000203 System.Void Lean.Touch.LeanMultiPull/Vector3Event::.ctor()
extern void Vector3Event__ctor_m345295CB46802D01E410625A43A6901B8D023668 (void);
// 0x00000204 System.Void Lean.Touch.LeanMultiPull/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_mCEB315ECFA0AFC9ED7910A80C5BF081300466EAC (void);
// 0x00000205 System.Void Lean.Touch.LeanMultiSwipe/FingerListEvent::.ctor()
extern void FingerListEvent__ctor_mA6E37846A0647E5A780D557656F07DCF84455404 (void);
// 0x00000206 System.Void Lean.Touch.LeanMultiSwipe/Vector2Event::.ctor()
extern void Vector2Event__ctor_mD398D4D8E2E75992A800E9599A591437F45E320A (void);
// 0x00000207 System.Void Lean.Touch.LeanMultiSwipe/FloatEvent::.ctor()
extern void FloatEvent__ctor_m1186CB9C3C251D23F687A27F05DD48079F789FED (void);
// 0x00000208 System.Void Lean.Touch.LeanMultiTap/IntEvent::.ctor()
extern void IntEvent__ctor_mD4107307271F17C8B2AB137C53FB135F03D59F80 (void);
// 0x00000209 System.Void Lean.Touch.LeanMultiTap/IntIntEvent::.ctor()
extern void IntIntEvent__ctor_mCD04EA47196B7AC046DDDD9F0C320FC1A28405D8 (void);
// 0x0000020A System.Void Lean.Touch.LeanMultiTwist/FloatEvent::.ctor()
extern void FloatEvent__ctor_m525AFDB173BC6C05CCDCBE4FE31B1306F26C3502 (void);
// 0x0000020B System.Void Lean.Touch.LeanMultiUp/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_m3A6BCE982E413DEC60867B0E78F3C4F30BE51204 (void);
// 0x0000020C System.Void Lean.Touch.LeanMultiUp/Vector3Event::.ctor()
extern void Vector3Event__ctor_m8250C087CED02087AE20725306BD3D2B475E440E (void);
// 0x0000020D System.Void Lean.Touch.LeanMultiUp/Vector2Event::.ctor()
extern void Vector2Event__ctor_m18684A1BE151BD6623D7C83B015BFD0710C2C12C (void);
// 0x0000020E System.Void Lean.Touch.LeanMultiUpdate/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_mB5D07F6BD1A58AD93C5392ED4DAD741A087E4364 (void);
// 0x0000020F System.Void Lean.Touch.LeanMultiUpdate/FloatEvent::.ctor()
extern void FloatEvent__ctor_m48AE499081261D9F8DBFB027C29799C251858EDF (void);
// 0x00000210 System.Void Lean.Touch.LeanMultiUpdate/Vector2Event::.ctor()
extern void Vector2Event__ctor_m66D127CECA90862F8A7FD05D71903FDA4DD6D811 (void);
// 0x00000211 System.Void Lean.Touch.LeanMultiUpdate/Vector3Event::.ctor()
extern void Vector3Event__ctor_m1ED8A8C66C5D1D9141DA31AE6E644D86F2DE848C (void);
// 0x00000212 System.Void Lean.Touch.LeanMultiUpdate/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_mB7FC65C3DD9839C6C79E6603E1C945AC8F3B5211 (void);
// 0x00000213 System.Void Lean.Touch.LeanMultiUpdateCanvas/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_mD1C3A83A1060CBE408A8F2E7E88920A19075AEAC (void);
// 0x00000214 System.Void Lean.Touch.LeanMultiUpdateCanvas/Vector3Event::.ctor()
extern void Vector3Event__ctor_mF5B0D217606611606BD2E96BEB9A0D9A7A3C5876 (void);
// 0x00000215 System.Void Lean.Touch.LeanFingerDownTap/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mD4082E4660FEC0F6D764810B8A30DF7E9333D488 (void);
// 0x00000216 System.Void Lean.Touch.LeanFingerDownTap/Vector3Event::.ctor()
extern void Vector3Event__ctor_m4962C522BA459FB51EB99B30CF15CDB360B6546B (void);
// 0x00000217 System.Void Lean.Touch.LeanFingerDownTap/Vector2Event::.ctor()
extern void Vector2Event__ctor_mA075F7B8AC8CA77EDC8051FF4F9DEB8DF84C537F (void);
// 0x00000218 System.Void Lean.Touch.LeanSelectSelf/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mBA41BA4376A87C0F9DE83D9E1CDB593AB0900AFB (void);
// 0x00000219 System.Void Lean.Touch.LeanSelectSelf/Vector3Event::.ctor()
extern void Vector3Event__ctor_m9EE118D141ED428AFCF99C6F2F0CEC188DA68B7E (void);
// 0x0000021A System.Void Lean.Touch.LeanSelectableCenter/Vector3Event::.ctor()
extern void Vector3Event__ctor_m14674F105A9E7821F566B42129BA816FA9D061F9 (void);
// 0x0000021B UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableDial/Trigger::get_OnEnter()
extern void Trigger_get_OnEnter_mE815F4FED24015A59629B44C805107463F57881F (void);
// 0x0000021C UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableDial/Trigger::get_OnExit()
extern void Trigger_get_OnExit_mEC9D2517E3CE558E581E77B6764E3B9154CDF12F (void);
// 0x0000021D System.Boolean Lean.Touch.LeanSelectableDial/Trigger::IsInside(System.Single,System.Boolean)
extern void Trigger_IsInside_mE8C333A6F8FF4089BBEEF525BED6B9D2C60B8B24 (void);
// 0x0000021E System.Void Lean.Touch.LeanSelectableDial/Trigger::.ctor()
extern void Trigger__ctor_m50C4DD6341DC0066BEE7D5172191FBD7D8D99357 (void);
// 0x0000021F System.Void Lean.Touch.LeanSelectableDial/FloatEvent::.ctor()
extern void FloatEvent__ctor_mF59800CE762E3CAE9EF8D6AE90670E398106B7FE (void);
// 0x00000220 System.Void Lean.Touch.LeanSelectableDrop/GameObjectEvent::.ctor()
extern void GameObjectEvent__ctor_m3E371051FEF017C9437F6D933B5CA547798F9DC7 (void);
// 0x00000221 System.Void Lean.Touch.LeanSelectableDrop/IDropHandlerEvent::.ctor()
extern void IDropHandlerEvent__ctor_m466E64F22C333B064CD94B8D849E7BC8DCCBBB5A (void);
// 0x00000222 System.Void Lean.Touch.LeanSelectableSelected/SelectableEvent::.ctor()
extern void SelectableEvent__ctor_m09AFFDFD8EA1379DFA067DA43990530D84EAF402 (void);
// 0x00000223 System.Void Lean.Touch.LeanSelectionBox/FingerData::.ctor()
extern void FingerData__ctor_m748C97AF5997A53C8E89D8457C7F8B971EF9651E (void);
// 0x00000224 System.Void Lean.Touch.LeanShapeDetector/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m2661A1F5658A39B05226E2D7AD710A60EE608BF1 (void);
// 0x00000225 UnityEngine.Vector2 Lean.Touch.LeanShapeDetector/FingerData::get_EndPoint()
extern void FingerData_get_EndPoint_m67160CC61B111D4DFACA6F7AB1948B721CFE4681 (void);
// 0x00000226 System.Void Lean.Touch.LeanShapeDetector/FingerData::.ctor()
extern void FingerData__ctor_m6606D6B3FCB5F6089BA5C2A51AE35F99A9FA3071 (void);
// 0x00000227 System.Single Lean.Touch.LeanShapeDetector/Line::GetFirstDistance(UnityEngine.Vector2)
extern void Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4 (void);
// 0x00000228 System.Single Lean.Touch.LeanShapeDetector/Line::GetDistance(UnityEngine.Vector2)
extern void Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433 (void);
// 0x00000229 System.Void Lean.Touch.LeanSpawnWithFinger/FingerData::.ctor()
extern void FingerData__ctor_m8BEC4556B84D540B9331DA7E702E7861FE686509 (void);
static Il2CppMethodPointer s_methodPointers[553] = 
{
	LeanDeselected_get_OnSelectable_mBBC8ED3614C88FC9B1F1BF94F75CD95A9B69922C,
	LeanDeselected_OnEnable_m6EFA53672541D1DC3410A58590812586CD73465C,
	LeanDeselected_OnDisable_mC057DFAD3F6B52623632A2E18FA83920BB16A28D,
	LeanDeselected_HandleDeselectGlobal_m47293B3799168F828BBF30BD2D70F13E2514AC71,
	LeanDeselected__ctor_mB123DE39A76B75A9A34AA744A167CAB61AF6339E,
	LeanFormatText_set_Format_mCD099F93A9ECA47B4B1427D52B4450FB45F13B46,
	LeanFormatText_get_Format_m3665531805101578ADFFAFC148C54B11270727C9,
	LeanFormatText_get_OnFormatted_m3E136D990EC866F847C8C21B99A2CDBF02F6E3D6,
	LeanFormatText_FormatFloat_m9898AE90E9AE87F94666598911A727E085C03D16,
	LeanFormatText_FormatInt_m9B0C9B5AF5B72E689CD6A91232B0C90E321FDB80,
	LeanFormatText_FormatString_m2FEE59329B4CD3B98208FC04E58C1BCC67BDD13B,
	LeanFormatText__ctor_m91A0B64E76165217A0E43B5474D941F1F335ACDA,
	LeanModifyFloat_set_Offset_mFB1D84CD1E6D68BF37F66D2D3F45E095EAAD1852,
	LeanModifyFloat_get_Offset_m794E960CB258FD182ACF3081AE2EE8B795F253E4,
	LeanModifyFloat_set_ScaleByTime_m1F88265F18FCA698AB2AFC604E82FECCB4324F6D,
	LeanModifyFloat_get_ScaleByTime_mAA9A20B331CCA39237E8C8747DCB5A1C37D35589,
	LeanModifyFloat_set_Multiplier_m96298FFE7887B6B8DEE33E3CE3505108C2978FC6,
	LeanModifyFloat_get_Multiplier_mE1EE7CCB4F86F16CA526CF0AEC2B3A462FD3D915,
	LeanModifyFloat_get_OnModified_m5E3F35328A4AEF4E52430D70930EB44722F9337D,
	LeanModifyFloat_ModifyValue_m29AB63FF7770CFE01D5DA00EBE381C657AA9D34D,
	LeanModifyFloat__ctor_mB2D7ED4CAFCA5E632C10E11012A3C7E9140EE094,
	LeanModifyVector2_set_Modify_m4EB2E7AE74C37C082F45AFB9F091F7679683EB66,
	LeanModifyVector2_get_Modify_mEC70D4B4645B540916C488693314AFF5C857FD6E,
	LeanModifyVector2_set_ScaleByTime_m269969AD02DFAC1C3775182E4D51CB0AAE56EC5A,
	LeanModifyVector2_get_ScaleByTime_m9C2E4A31B247554DD9B2D9991588E402C74DE1DB,
	LeanModifyVector2_set_Multiplier_m73891D92D5203B6D1F31D88BDD66AFBFF4B8CCC3,
	LeanModifyVector2_get_Multiplier_m915284ABDC9EB0E99BDD4A9FAB305186A85702C1,
	LeanModifyVector2_get_OnModified_mBDFFF7D410FBBD6304147DDEB5C17102BC0EAECE,
	LeanModifyVector2_ModifyValue_m90F0F623194ECF750A0EF42356137927AA97400B,
	LeanModifyVector2__ctor_m38E0BEE9134590784D0BFB5E8BC7AA8314507432,
	LeanRandomEvents_get_Events_mEC215056EC54E1A7F19BB8CEF6FE08211D4842D8,
	LeanRandomEvents_Invoke_m120D8D4D505ACE82B2184A9C9E9F4951CA037628,
	LeanRandomEvents__ctor_m32EAC92FACFDCD0F27C6C7062178A67810336426,
	LeanSelected_get_OnSelectable_m0446BB76A759FBE666F755D12DBD83B1348F79A6,
	LeanSelected_OnEnable_m9111C1BFCE816A5CB9FDE989F71E8CB3A4DEB63F,
	LeanSelected_OnDisable_m7BADF6D38E7AE6B7680DA1EBD596BEA69C9B4EC7,
	LeanSelected_HandleSelectGlobal_m499475218995145579719D2C1CFCC377F3D0AD1D,
	LeanSelected__ctor_m34395F9EB121959A824660D7ABDF073663BE156E,
	LeanSelectedCount_get_OnCount_m081EE009FB095739FA79A524D60ED85B1F1E1235,
	LeanSelectedCount_get_OnMatch_mD6F422082D39C4D778371F684DBA96ECB27083E8,
	LeanSelectedCount_get_OnUnmatch_mDABE242D06C0AEAF2DA4BE411099889ABBE9D279,
	LeanSelectedCount_OnEnable_mA5216CE96A138EF751AD56E42515EEBAA82F7E76,
	LeanSelectedCount_OnDisable_mF3AB73E3FE659AA02FC69EA79A3D8043FE75537F,
	LeanSelectedCount_HandleSelectGlobal_m1712D4DAB1995DE14725ECD58AA7DC7C7D291CD6,
	LeanSelectedCount_HandleDeselectGlobal_mE3AE1658DAF14C370FD5B0A6A81FB38187F68C23,
	LeanSelectedCount_HandleDisableGlobal_mFD93F54404A997F74DD243E0E23E5DB96B92323C,
	LeanSelectedCount_UpdateState_m0B9554B781D3AC9B735F07AA7F49A2868BB62185,
	LeanSelectedCount__ctor_mA7324B403FF50B71F96B566FE685D9A4237B312B,
	LeanSelectedRatio_set_Inverse_m6157CD08A4755539234F6387519DA5F8A74165BC,
	LeanSelectedRatio_get_Inverse_m832568BD4D78C3AECCE980E1058D949F2182314D,
	LeanSelectedRatio_get_OnRatio_m57917D225C4CA3D10C53F1911ED16B1541779974,
	LeanSelectedRatio_UpdateNow_m7ABF23AFCD0DB1E91B3A3D2E5E47F6E898433285,
	LeanSelectedRatio_Update_mDBA880AB965F7D9728AFB94E720AA0D925C4F5A2,
	LeanSelectedRatio__ctor_m482D65D74CD44DBBBC07128C87CAB47EF80003DA,
	LeanSelectedText_set_Format_mCBBFEB61C389F7183D347893228A8B42AB0D726B,
	LeanSelectedText_get_Format_mA42E0A15A6E6003C0217DDF0DDC40E36F67EEBED,
	LeanSelectedText_get_OnText_mE5586339BFE70BE0162F5F72589834356EA54A04,
	LeanSelectedText_UpdateNow_mB7D5873EB5120646E32BD6843312FE2205FDDF7C,
	LeanSelectedText_Update_m2D17E8C30C6A0199D50DBBE7A3324DF1019C8D81,
	LeanSelectedText__ctor_m908B99182B38CC6590E31D15C8409384C4C973D2,
	NULL,
	LeanDragColorMesh_AddFinger_mAA31A77D516EE9A91FE49CE573BE7B8F4389576B,
	LeanDragColorMesh_RemoveFinger_m565A9789FD8BA6F682EE6FB2596AD59FB7A634EF,
	LeanDragColorMesh_RemoveAllFingers_mA3FC04C8F109D79FDB6D5B4B401F74FDAE619716,
	LeanDragColorMesh_Awake_m70173B1F697EADD6F3C68032E72A494E44914650,
	LeanDragColorMesh_Update_m12D669F3656A2A6F3375A472A6380ABB24C6D3A9,
	LeanDragColorMesh_Paint_m5063EB9F76ED4A93E62AF6E73A66C13BB5C68179,
	LeanDragColorMesh__ctor_m58B0BE5F2B14AC9175CA153E5CABBB9E78521794,
	LeanDragDeformMesh_AddFinger_m1753C45299BFA593F01A903BF566EAE2E5AF5C5E,
	LeanDragDeformMesh_RemoveFinger_mD84842835527A0A697DDF8645B59A84DA0D5492A,
	LeanDragDeformMesh_RemoveAllFingers_mD27D7158169F4CDE4F1F0110A7BEEC9E8655CE77,
	LeanDragDeformMesh_Awake_m32F8B543962C7A21A698D5AA06DF6A64FFECF91C,
	LeanDragDeformMesh_Update_mA09DFD7193FE6D26B84511B22B787776486BFAC4,
	LeanDragDeformMesh__ctor_m0DB9FE34BB84BCB2692CE760F0826FD36A3F0364,
	LeanDragLine_get_OnReleasedFrom_m75A60DDEF8546F74165ADEDBEF5B8D86F4C9A2BC,
	LeanDragLine_get_OnReleasedTo_m9D75DFF603C9250A5A2E5297EB8C5F092EB61873,
	LeanDragLine_get_OnReleasedDelta_mF64F77A3A624BBBB87BFDE9BE4B862FFDF99D80E,
	LeanDragLine_get_OnReleasedFromTo_mDF14664AB53C5C6B11FFCE49091E949E5A9703A9,
	LeanDragLine_UpdateLine_mE65074D211E7D9B18D9D4254F657AA9AFAEB205D,
	LeanDragLine_HandleFingerUp_m2076F2E2DF0CABEE385198FD482BCFB564B36D6C,
	LeanDragLine__ctor_mB74185BD95139D8731C8E012A5E43550D47A5C52,
	LeanDragSelect_OnEnable_m17586657791075CBEF620B9EE198C0A6839F69B4,
	LeanDragSelect_OnDisable_m77C9299E07283E24D18EF3C8F74A295612728DB7,
	LeanDragSelect_HandleFingerDown_mA3075A4CAA06736E1A05DDDA33B3A52A0864F540,
	LeanDragSelect_HandleFingerUpdate_mE5BAD1D9476DC1EB17E6751015F7AAFB0ACA2E28,
	LeanDragSelect_HandleFingerUp_m59FF4291EBDF6B719F3C3140F38B52B118DEB04B,
	LeanDragSelect_SelectGlobal_m8A9A67FAE6D71BB677032B22CDBC89B017CDEA4C,
	LeanDragSelect__ctor_m33FE42249ECBD1132A4C1692DE7785DF11A49A94,
	LeanDragTranslateAlong_AddFinger_m12F5E6DB8BB38CEB3B238EBE6F6C53D56EAA16B9,
	LeanDragTranslateAlong_RemoveFinger_mF12258AF2DF2154A1D75274E9887A42A78911A7D,
	LeanDragTranslateAlong_RemoveAllFingers_m8D5F3C1428810191F4C7648D7A6434BB41BD14C6,
	LeanDragTranslateAlong_Awake_m6CBDFFCAD8CE631D949FFF270E4A18AE83D231A8,
	LeanDragTranslateAlong_Update_m21327B445D9CFEE8DD3037F907AC5CAE0E0FB435,
	LeanDragTranslateAlong_UpdateTranslation_m901C7729B4551D0484F1F70FC2B52C1C9AB12A17,
	LeanDragTranslateAlong__ctor_m84289624AE3ECCB1FDDB7311A74EAFE6F249845D,
	LeanDragTranslateRigidbody_AddFinger_m52B365A4F4FBF7F12FA0FD1BD48897EDBB7F2A7B,
	LeanDragTranslateRigidbody_RemoveFinger_mA4EBE8F6205AF90C483630DAE07173A1181D5F0B,
	LeanDragTranslateRigidbody_RemoveAllFingers_m8DE8894BAE016AFCB18F6B56B77AF851AD2608BD,
	LeanDragTranslateRigidbody_Awake_mF5424B27DDA5F7D3F1BB5E9692134AB7575ACA13,
	LeanDragTranslateRigidbody_OnEnable_mFED69C61D3D26A1FAA16BDA57A6AEAC7FE635116,
	LeanDragTranslateRigidbody_FixedUpdate_mD8B21D3F84F63D5352DC46CB1BDAA81634559CD9,
	LeanDragTranslateRigidbody_Update_m517332A3B20728A168960F26DBE4E7FAE069E767,
	LeanDragTranslateRigidbody__ctor_m23D71792689FE93441C3F1C34BD0B7F2ADF8499E,
	LeanDragTranslateRigidbody2D_AddFinger_m787D030DBF41744145DEEE2C224EE0EB67D600B7,
	LeanDragTranslateRigidbody2D_RemoveFinger_m429398E67FCFA21D72BE0E7A5236B61FD4926A9E,
	LeanDragTranslateRigidbody2D_RemoveAllFingers_m766453F61892D9F0D16A763D55B9CDD20294A800,
	LeanDragTranslateRigidbody2D_Awake_mFB0CD83B7D193493568DCF5DE3323C4BB90B6649,
	LeanDragTranslateRigidbody2D_OnEnable_mCE3835AC590C34FDC029A21DFBA4736BAF03E0E5,
	LeanDragTranslateRigidbody2D_FixedUpdate_m590B2B155ABA1835AC0015E8C64D15AE325E6C9A,
	LeanDragTranslateRigidbody2D_Update_mE086A9B1CE7E0C55FA9F63F56854623B7517C371,
	LeanDragTranslateRigidbody2D__ctor_m25ECC1B8C8E56696748E67455343A552EC93EE58,
	LeanDrop_get_OnDropped_m09853A653C1709DE436B48B56B338CA353C53F73,
	LeanDrop_HandleDrop_m48B7252A705C3E93D0DE201A5794FE93D883927E,
	LeanDrop__ctor_m6D91A825CAEA68E7854C44C8EEF79EFB55B8B560,
	LeanDropCount_HandleDrop_m7BEEF850A044C7C9A7C3767E335261C773DCA10A,
	LeanDropCount__ctor_m6E3A1EB3C0F3BFA1016826CEB4A37CA7CC3C82A2,
	LeanFingerDownCanvas_get_OnFinger_m678B70EE8F058D542CC89B3E86CB2E98D187BE8F,
	LeanFingerDownCanvas_get_OnWorld_mC92EB9CD5F667B0B99439EE8D6945B94F539CA6B,
	LeanFingerDownCanvas_ElementOverlapped_m6720ECF08D80802EB9427BD1DAC5CC267159BCF6,
	LeanFingerDownCanvas_OnEnable_m8CDE230E8911101A27B7E40BC3465107C1A3710F,
	LeanFingerDownCanvas_OnDisable_m44CF2C5D4E0ABB14274FE9679D981A23F8FECE27,
	LeanFingerDownCanvas_HandleFingerDown_mA58F32E01A85D2DCA7574FF2F04F68EB4B5C8D85,
	LeanFingerDownCanvas__ctor_m149FBA10E592252B5D2A31E5C6A2BAE7F922DAFF,
	LeanFingerFlick_Awake_m3B7858BC02E7BDDD1F91C26CC0F5F635E68EC555,
	LeanFingerFlick_OnEnable_m24D845ECFFB4553DB478468AE022C0F8D0DED1EC,
	LeanFingerFlick_OnDisable_m6E5219EAD7B87910CD1308D0F20126726A7BADB5,
	LeanFingerFlick_Update_mAE0AA6BBFE695EFA3F88B20936702E9168EF9772,
	LeanFingerFlick_HandleFingerDown_m55AEDA533194F4BC2E2DF9B2032F007F1BFCFD31,
	LeanFingerFlick_HandleFingerUp_mABA074A17FA3E27994012A424BB3A0CFE2A97E08,
	LeanFingerFlick_TestFinger_m33EF58EFDDECD6ABF54CA3C581105498286783AD,
	LeanFingerFlick__ctor_m5FA1984949DE8BD8E9A389F020DEB2E195ECC996,
	LeanFingerHeld_get_OnFingerDown_m79EB9C4ACCA785136D57B77C9AC60CF65D561FAE,
	LeanFingerHeld_get_OnFingerUpdate_mC5A70CDEB0D96EB5DEE35C3F34B5D5DEA4A33C27,
	LeanFingerHeld_get_OnFingerUp_mC4F63AB681E8F4D69EDAEEB0D4A1C230D268B3DF,
	LeanFingerHeld_get_OnWorldDown_m79C6D7EE44AD5779893E0A58268B1B3130FF92F1,
	LeanFingerHeld_get_OnWorldUpdate_mB80A455536E61F22C36373497CAC94CC419FE492,
	LeanFingerHeld_get_OnWorldUp_m7C2F59FADC82C45CBEB74AB20CC91957569EC1E5,
	LeanFingerHeld_Awake_m9A7957846DAFA4055949FDA97106B3DE4111431C,
	LeanFingerHeld_OnEnable_m9227B0083545C31FB74DA23351B0807167A3D59D,
	LeanFingerHeld_OnDisable_mF437C683F72A4740C826BD12B20F710EC45E2F89,
	LeanFingerHeld_HandleFingerDown_m7BF90A4D7EF9A3AD0261E7DA69808846CD180C94,
	LeanFingerHeld_HandleFingerUpdate_m7B8274F0079F573933638C94EF5DA2FD537767AB,
	LeanFingerHeld_IsHeld_m65456832C636AA898846DD0474494A122B4BD8A4,
	LeanFingerHeld_InvokeDown_mE8F862E18888CDE79F05CEBB6D71C3DA7EF4AF92,
	LeanFingerHeld_InvokeUpdate_m9AC7EE952EA0AE9BEEC5B6F1AE0A354A6FDB9F78,
	LeanFingerHeld_InvokeUp_m38EAC4835A2B182E8402B86944513BA6E83EA6D8,
	LeanFingerHeld__ctor_m07177F0765A8D943720126178AE15075ABCEC63F,
	LeanFingerTapExpired_get_OnFinger_m7999B4D9DC9270222CF9DE0994E4F29ED347E755,
	LeanFingerTapExpired_get_OnCount_mB3C8D62347E4F37B0D5376E1CEA55CC6EF88C282,
	LeanFingerTapExpired_get_OnWorld_mB5F5DA2673F390E4157DACD707B8AE400FE92B33,
	LeanFingerTapExpired_Awake_m850DD44586845A239552821AD97C07B99DCE0752,
	LeanFingerTapExpired_OnEnable_m92E9966FE5070213A330C8E305C331B9D8A3CEBD,
	LeanFingerTapExpired_OnDisable_m5369D4A7CEBE862D0ACF120C39E0724C348723A0,
	LeanFingerTapExpired_HandleFingerTap_mFF7DAA7CCD04A6D0F5865EF15C6BE99093AE399F,
	LeanFingerTapExpired_HandleFingerExpired_mBD1B29208E2D914E661D723EB301AD9F83FC9DD5,
	LeanFingerTapExpired__ctor_m96DFC8EC42B5F2DB488A7CB78484F144A8E91D45,
	LeanFingerTapQuick_get_OnFinger_m525C6ADC90552B037DD68D5763D9BEBB38A94EF6,
	LeanFingerTapQuick_get_OnWorld_m6C770A2C9480A39342B2FB12BA2496E7508D7C69,
	LeanFingerTapQuick_get_OnScreen_m3AD2A85FE7356175DCE8F8DA3C678570FADECE73,
	LeanFingerTapQuick_OnEnable_m03BEC242EC2178763C72A215C20C73F53EDBAE17,
	LeanFingerTapQuick_OnDisable_mB1A97BE3969A6A85C4273B8642C44FB2742AA092,
	LeanFingerTapQuick_HandleFingerDown_m694A92A1F01E1B0B39986437649B648E7209C8A2,
	LeanFingerTapQuick__ctor_m54BD66B02B2CD1F7223DFE0DD76266E4D8D0BA4B,
	LeanFirstDown_get_OnFinger_m1FE7FC3977DEF3021F78D4D625CA7610AEC191E6,
	LeanFirstDown_get_OnWorld_m886B45CF87E8176C1FDFDAEF5E273D04F696F6F7,
	LeanFirstDown_get_OnScreen_mF084F6B5171C2F46F629A28B8D9496616DE54C50,
	LeanFirstDown_Awake_m535AFEA7BF1C5EECC51380451B77A811135FC55C,
	LeanFirstDown_OnEnable_m29471B3AF8A92E48A9CBA2A76D4AB85E2486B612,
	LeanFirstDown_OnDisable_mBCA025D386FB08F859E9FF12668BBCA1DEEF533C,
	LeanFirstDown_HandleFingerDown_m90770E92F34E73A5D46BF30993479E473C2ABB6A,
	LeanFirstDown_HandleFingerUp_m953D1C46F10AD5535449D0CF10F9A2704C529FD7,
	LeanFirstDown__ctor_mEAE74CAE165F067CC4813CD18BFE7CFFFFA4CF6F,
	LeanFirstDownCanvas_ElementOverlapped_m8240F4FF9FCB12CDEEC5BA4C4B927DE8791F9992,
	LeanFirstDownCanvas_HandleFingerDown_mE20CA7BAF01DDB96268EC5495BE596A85489CD8F,
	LeanFirstDownCanvas__ctor_mEF86EE90E702DD59E90B1ACB1B505392D2DC5C25,
	LeanGestureToggle_AddFinger_mFDF7EA736828FEDE292AFAC9B7D31085829AB6CB,
	LeanGestureToggle_RemoveFinger_m6B8564C8C77B6797235F4AEF89BDE0441B692733,
	LeanGestureToggle_RemoveAllFingers_mF4CF750D8761BF53A6A6FB0B88E23059E46C328E,
	LeanGestureToggle_Awake_mDB66E4B2C2675D70561FDEAD520174B6F66755C1,
	LeanGestureToggle_Update_m4870F63BE93114F393C4F747A82D5BA2AB0CB9B5,
	LeanGestureToggle__ctor_m0B839BE60E35AA96F88F5275E88767D4B44865A3,
	LeanLastUp_get_OnFinger_m25113CB96F6C3BF18DB93A6F6C752E311B98A3ED,
	LeanLastUp_get_OnWorld_mFE92AEA52A597C4887580568D70637F27D5E3CE7,
	LeanLastUp_get_OnScreen_m536C8AF95F4BB181023C71F3D54128170E2FFB06,
	LeanLastUp_Awake_m32D35FAE90A881EBF9CC1B04744F93B8BA549C3D,
	LeanLastUp_OnEnable_m025D87AC5854BD3CEA0B8C33DA11E1845757FA7A,
	LeanLastUp_OnDisable_m60ED249555211C27A76563CE5AFCB93688689DEF,
	LeanLastUp_HandleFingerDown_mBC5BEB36346B7E400F206E92511E7571FFBE33DB,
	LeanLastUp_HandleFingerUp_mF58BA6247A4712CC60BAFBB1902AB3D60FEA2CC6,
	LeanLastUp__ctor_m994DE50833BDB1043266E70BF8B1C8AF9BF915D3,
	LeanLastUpCanvas_ElementOverlapped_m2DCB427A43213EF8F991E6C4AFCB68256AB631F3,
	LeanLastUpCanvas_HandleFingerDown_m5DF8ECC8618C64E29C6F38BEB48F06E2E570E925,
	LeanLastUpCanvas__ctor_m57DB0E68A5B3502F6992029CE42772BA56C247FC,
	LeanManualFlick_AddFinger_m077D7ABB500F3FB421B388163EDAB2D5029DA8D5,
	LeanManualFlick_RemoveFinger_m26133E58D8B2E27A75D690C34410408B67057B2C,
	LeanManualFlick_OnEnable_mB9A36783AE1499268434ECC2E4F7A46859D45BC8,
	LeanManualFlick_OnDisable_m3148E44874EA2CD6B4C460B59CB897160CC77853,
	LeanManualFlick_Update_mAED17C2912FBC504E71ED76CE8D58B6E92F7F8EC,
	LeanManualFlick_HandleFingerUp_mC75191648217EDA4EB1ED65CF12FA0998C7AEC21,
	LeanManualFlick_TestFinger_m148EF2C1BD25703ECEBAF13FFA3F258B725A9743,
	LeanManualFlick__ctor_m1FFC26087CA00D44C009EA7BA3E1318C3F02CCA3,
	LeanManualSwipe_AddFinger_mB5A8EF0978104262B2478BED6EC60196125B6D1B,
	LeanManualSwipe_RemoveFinger_mBBEBB8E9B68EB9F636F2F4B4E8A33FCFDA74DA4B,
	LeanManualSwipe_OnEnable_m3135555688923BB5B46D5604A97B6BAD8F304EB1,
	LeanManualSwipe_OnDisable_m872CE80703513A7D19034C1F67DF3D4B90CD600A,
	LeanManualSwipe_HandleFingerSwipe_mD38779743B0A86B65B3EA28C7A3D8A152F35823F,
	LeanManualSwipe__ctor_m87BDDD5999945CE66E9F3AD9DC7579C54880523C,
	LeanManualVelocity_AddForceA_mA43CDB288F08C3070054101FD8E86860F56E4070,
	LeanManualVelocity_AddForceB_m70948A5A10B7C232B5DF5E53B6C8432BAB7BF81E,
	LeanManualVelocity_AddForceAB_mBCFC154ADBA10761B5855103D0373E9C82E6CE1F,
	LeanManualVelocity_AddForceFromTo_mEB54553E53C9D5D23D313A2DC78ACDADF6FD6FB5,
	LeanManualVelocity_AddForce_m8342200418823647F4363F2EAB9E0167D12A14FA,
	LeanManualVelocity__ctor_mB8CCF5D1EA699E1FFF0E210A3330BEC760561171,
	LeanManualVelocity2D_AddForceA_m8B9ADA336B5A5C8662817B6B056A1E8E4F62AA8E,
	LeanManualVelocity2D_AddForceB_mEBCA4D3966EDD1E2F6B3A95AEA26DB1F097E4549,
	LeanManualVelocity2D_AddForceAB_m052DA77A35D98B2BE13B18EC2A8EEC4211D51DBF,
	LeanManualVelocity2D_AddForceFromTo_m2730575216A624D29A28B6E0F78394BE57495680,
	LeanManualVelocity2D_AddForce_m11AD0748A38728059C99C06441D97218F9471079,
	LeanManualVelocity2D__ctor_mD51F5EFBA38B9B993132A1C7697EDAD3686F4CD2,
	LeanMouseWheel_get_OnDelta_m341B415B09ADAB5056247422F2F2DCAA1A4316AC,
	LeanMouseWheel_Awake_m4D10DFFAC0CDA879A5AA0BBC43EEE2CA8E62FB09,
	LeanMouseWheel_Update_mD826061A9DA35F9669B08E2E320D08C80B310566,
	LeanMouseWheel__ctor_m6D2465CD3873C1B1D01A86EBB0C038ED984DB7C3,
	LeanMultiDirection_get_OnDelta_mE5F82A09D22D0DF3EBC4998B69D05A428EB34BB3,
	LeanMultiDirection_AddFinger_m4F3B19C379B96BC0A717DB76E07F4910639AB8ED,
	LeanMultiDirection_RemoveFinger_m7B75CF197A711E8CC48C6A17437CA3A2B73E84E3,
	LeanMultiDirection_RemoveAllFingers_m1FB651497D44492265DA9FD947AC818A5EB62DA8,
	LeanMultiDirection_Awake_m76993C0402967DAB9EBD7524A072BC5BD605DEFC,
	LeanMultiDirection_Update_mA761AD1F6E9F9BCB9B81652CDC3808AF11EB81B9,
	LeanMultiDirection__ctor_mDD6B74D3D1D9666BCB0DA876E92EF6705B4723CD,
	LeanMultiDown_get_OnFingers_mDB5A12F325E6207F665CCB043A6352392503CE88,
	LeanMultiDown_get_OnWorld_m766FAE90A2DB5F24EA3D508B113055325E5EB2C4,
	LeanMultiDown_get_OnScreen_m37A802B50BB88A361A837B6E6BA75B68D2A1A1F1,
	LeanMultiDown_Awake_m2D8E40D945D3BAA82C263C6487BA6517E119672B,
	LeanMultiDown_OnEnable_m3528F1CAA971D9CC39F35C14FEBA837AEF9DFFE9,
	LeanMultiDown_OnDisable_m162724039139649916BAC99508524DFB6C3BF908,
	LeanMultiDown_HandleFingerDown_m9C59CA9E801BD1B74F121600E9ADA95F022C4CAA,
	LeanMultiDown_HandleFingerUp_mA4824B771A67DC68A66C37C631A92F2BCDF3DB13,
	LeanMultiDown__ctor_m71CA134BC733105487374E2B274ABFA905BE91BF,
	LeanMultiHeld_get_OnFingersDown_mAACCABC3276E016FF24D01DBCFA8FB3579FD1F1F,
	LeanMultiHeld_get_OnFingersUpdate_m228A71E3D1831FC263CF86BF11C24F56C578357B,
	LeanMultiHeld_get_OnFingersUp_m444E1DCF67FAA3732840A1A03B71ACF9ED2E1702,
	LeanMultiHeld_get_OnWorldDown_m8726293C7727710DE6C166F333E8352EE59BD3D8,
	LeanMultiHeld_get_OnWorldUpdate_m8774DBEBFAB022DF774387E9510156E3479C6D35,
	LeanMultiHeld_get_OnWorldUp_m703CFB5F7E86871F0C2A10D4574483CADB75AD9E,
	LeanMultiHeld_get_OnScreenDown_mC9F914E072F7204438C3E93E8B704F57AF87BDD3,
	LeanMultiHeld_get_OnScreenUpdate_m9D6DA1878ECE3BBBD4A7FC138966507DD37B7D6D,
	LeanMultiHeld_get_OnScreenUp_m59C64D3207252509586223BE619086BCBFE8A3CA,
	LeanMultiHeld_Awake_mC1060B4C6759BD31CB9F502D2242478B654860EF,
	LeanMultiHeld_OnEnable_m496194383710F1AC5E6324FD3505E17622E7F691,
	LeanMultiHeld_OnDisable_mFE6985DFAE1536E32C2D33F44955885E2039E147,
	LeanMultiHeld_HandleFingerDown_m5A32DFCF4044B2B154A3B6BFAEF3DC942C559C6F,
	LeanMultiHeld_HandleGesture_mC11F78F4DA05D9C0610C56EAFB874A783A4819E0,
	LeanMultiHeld_get_IsHeld_m153085BBE8597DC01BC67F54A181792875496F5E,
	LeanMultiHeld_InvokeDown_mA8A3C5E4072B8ABDFEE9BD20D6A577CC028B78B9,
	LeanMultiHeld_InvokeUpdate_mAB9072A8E9F38B62003A747E02D44DECC13844EF,
	LeanMultiHeld_InvokeUp_m938BC0C3FB9A237F7754CB016A23C30748854C68,
	LeanMultiHeld__ctor_m24902EBB016ABB7C482410D5433A91E45DB623C0,
	LeanMultiPinch_get_OnPinch_mAB5E4FE481D517A3BE39EA3F5B3D519698104D3B,
	LeanMultiPinch_AddFinger_m854D688573EDBC17658E102ADE01DD4B04EAA5F6,
	LeanMultiPinch_RemoveFinger_m32FF2FBC05E43A44D605EAB3AF8513F62EE97FE2,
	LeanMultiPinch_RemoveAllFingers_m30E8C1E4AC7910D17FA7167EFB7F5896FB8BAA3E,
	LeanMultiPinch_Awake_mDD1E37628C6FA709C00F62091828F115A83EDA0A,
	LeanMultiPinch_Update_m317AA0A46D8F04304C62616BF406CCFB036A0D23,
	LeanMultiPinch__ctor_mFE28BCE52C8E2413CA73B464BFDCD5787D1CF28E,
	LeanMultiPull_get_OnVector_m6B4B245A99AA7B93FE992F09A810D47E5E82CC7A,
	LeanMultiPull_get_OnDistance_m49FAF940F9D1F3C9A513D3EAFC9ED67D41DD2790,
	LeanMultiPull_get_OnWorldFrom_mB49A2A19B18FD64C992BBA34FCCD8D19D3769EBA,
	LeanMultiPull_get_OnWorldTo_mFC6EC793FB4E98A590B1D1BC46012D26EE348A34,
	LeanMultiPull_get_OnWorldDelta_mED6B6288BF768152C4DA7BC7A4E1700FD422EB0E,
	LeanMultiPull_get_OnWorldFromTo_m52EAFAAB8A46870FA1C78AF20F8FD2EA977AD890,
	LeanMultiPull_AddFinger_mEB80146FFEB9A5DF88D093F4A41708E0B3F45208,
	LeanMultiPull_RemoveFinger_mA3C0B2BDF9C5B3DFAA4CEF41814D109D89814C5B,
	LeanMultiPull_RemoveAllFingers_mFD2D8A0ACB9D06CED7BAF247EBCD74F3A639A39E,
	LeanMultiPull_Awake_mD81DCFD20BAEFADE73E77DF6655452DED221DBC1,
	LeanMultiPull_Update_m7D2955AE45E4EF3F940238FA002A899C4C06C11A,
	LeanMultiPull__ctor_mD7CF17AF16D7B3B13011888D9CC68DE0B95B6D2F,
	LeanMultiSwipe_get_OnFingers_mD3A8460AA714F7212F65EAAE36D34DFC1E8C1440,
	LeanMultiSwipe_get_OnSwipeParallel_m468B2C33FDBC9A10042ACED36EB16D681B7BACBD,
	LeanMultiSwipe_get_OnSwipeIn_mACCE0B743E307FF6C9FF896630FBA613E9E81230,
	LeanMultiSwipe_get_OnSwipeOut_m9D483F96F7A2509A17BBD284C37C69D291E61D6E,
	LeanMultiSwipe_AddFinger_m3DF826ADAD310A1236AC68F565085C958B3F6FBE,
	LeanMultiSwipe_RemoveFinger_m8CC2A796411C1CA5120201FD455158153ADD479E,
	LeanMultiSwipe_RemoveAllFingers_m41AF9C74E838B0C26A9198F09AD65AA2EBA221C5,
	LeanMultiSwipe_Awake_mE2567AC460AC9D3B561195CC573659510DCFFC57,
	LeanMultiSwipe_Update_mC7D90F94E30317B923FA14A6ED3F64EE5662245D,
	LeanMultiSwipe_FingerSwipe_mA6A441B068CACE99EF446A117ACF8129FB238035,
	LeanMultiSwipe__ctor_m0E8B787AD0734BA41815C725D96173B915014AA2,
	LeanMultiTap_get_OnTap_m855DE7F347D6779013E6BEAC792051BEB949FBAD,
	LeanMultiTap_get_OnCount_mF2AECE8FB1340AEF8445E6B9E9A2F94113A10B5C,
	LeanMultiTap_get_OnHighest_m600B8A65DF0AEA7D8A410AEA8EA6EC6D8CA1A3A3,
	LeanMultiTap_get_OnCountHighest_mBC4ABE7C4FEFC8C3FFA92B56CA7B6FE60CE30811,
	LeanMultiTap_AddFinger_m6F2F458B9E424B3FB4508873EBA9CDAD5AED8B06,
	LeanMultiTap_RemoveFinger_m96C66924DAA73713B54F408723EA2E338CE08F23,
	LeanMultiTap_RemoveAllFingers_m9C4D65A65FD4CF7DF0E3A9BC24D8B9E5E75325D7,
	LeanMultiTap_Awake_m4F53FDF463C08E201153EF685B2B4BA2F2C921B9,
	LeanMultiTap_Update_m6CD1CF211EDE5EAB4003B1F5793CDED8BC14DD1B,
	LeanMultiTap_GetFingerCount_mE9FA30268672D01B076AA3CDD0882241893AF434,
	LeanMultiTap__ctor_m3EFF45F27190EB48047EEBC12463EA106BE92C76,
	LeanMultiTwist_get_OnTwistDegrees_m77FB259224F6F135CBD956F7DFAFBD43CC3A4C28,
	LeanMultiTwist_AddFinger_m27B539FBE3993FD11576EA2438100F0FC7B6C458,
	LeanMultiTwist_RemoveFinger_m363CE97E97FBF93711C5B622CB34C811AC4E3734,
	LeanMultiTwist_RemoveAllFingers_m3A06351D5ACC3F128A59194D52AC3F3C85426189,
	LeanMultiTwist_Awake_m6CBB23BB6557D1309419A572F81CAC8C03DF72EA,
	LeanMultiTwist_Update_m9EABA65B62CC0F03C0D32B25A5C2E857898D21E8,
	LeanMultiTwist__ctor_m236BD80D423A8F01009FCE703CBE3D6F4FC2E6B6,
	LeanMultiUp_get_OnFingers_m66A8D4101F60F7286C0A28FFC8319A4D7A001022,
	LeanMultiUp_get_OnWorld_m5B7105A18A42546F53BEC64D4EBFDE1EC2F58A9D,
	LeanMultiUp_get_OnScreen_mA6BC77DD7E7701D480EEE663A7DFF4A1D9F39A0F,
	LeanMultiUp_Awake_m1B5D04C5B14C44ED3842EB16F2BA196DAAA2C924,
	LeanMultiUp_OnEnable_m4F69C4114F338691392F52B1F46595E5D0C35AF2,
	LeanMultiUp_OnDisable_m68EE1E431B3A8B8C16D2E203276E373CC0710EF7,
	LeanMultiUp_HandleFingerDown_m2BF6DC3276ED40C35936B9082F5C122CFE044493,
	LeanMultiUp_HandleFingerUp_m330E63BE63762613985AAA787269BF1DEC4331FA,
	LeanMultiUp__ctor_m9F202CBC7F2AEE80BBA87EECD2846830ABEFFC4F,
	LeanMultiUpdate_get_OnFingers_m362E92A29B4BD12518930056D0DC93A340C496E8,
	LeanMultiUpdate_get_OnDelta_mE2AA3138255583145DCF3FDB239A03374E8417D5,
	LeanMultiUpdate_get_OnDistance_m4C77437185EB840F92587AAAD43AA2D8EA202D42,
	LeanMultiUpdate_get_OnWorldFrom_m7C0CF9F98EAD169CB3B364550E8FDDECD9A1EE4F,
	LeanMultiUpdate_get_OnWorldTo_mE928782C4AD81F39BEF2C7CBE42A15D667416430,
	LeanMultiUpdate_get_OnWorldDelta_m3D3715941032AD5A376F62996E66EFDCE7515E66,
	LeanMultiUpdate_get_OnWorldFromTo_m40EE02FCF12BB80465D2DC630D2094A927639661,
	LeanMultiUpdate_AddFinger_m406BCD8E091B05807586409246E7E8C7B0CF195A,
	LeanMultiUpdate_RemoveFinger_m178A1E406CAFFC29ECF04CC7B7029DD20ED7E93B,
	LeanMultiUpdate_RemoveAllFingers_m9EFA346DD7E928DDCED02ADBC9E61CBA83FA62E1,
	LeanMultiUpdate_Awake_m828094AB66F491370637B4127CE0835B4CE4480F,
	LeanMultiUpdate_Update_m4A5B44C35C96FA5A9BDA6D21DFF6088B45498173,
	LeanMultiUpdate__ctor_m01A38DB61E98C216A0330A62F3CFE104F2317FA4,
	LeanMultiUpdateCanvas_get_OnFingers_mA549C79722C2A7E20A56B56FDC43A9E0F9EE1B9E,
	LeanMultiUpdateCanvas_get_OnWorld_m7D58F904D9990D65473A4FE2305FF3FD8C837461,
	LeanMultiUpdateCanvas_AddFinger_mF6A748EDF3E6572893264EC789888E1E5E3A27EA,
	LeanMultiUpdateCanvas_RemoveFinger_m4C725C2C258B1C5FB68CF50421FAC5D90E6E56EA,
	LeanMultiUpdateCanvas_RemoveAllFingers_mA1CB852ADF3433814FCB7CD069297911499C9E70,
	LeanMultiUpdateCanvas_ElementOverlapped_mE31D2CD60522160D7B2ACA8CC1009AAA721415B4,
	LeanMultiUpdateCanvas_Awake_m34F0EA13A8B92799A58B417102E4A39C34D01DE0,
	LeanMultiUpdateCanvas_OnEnable_mAE3BEE6E50A445A216C47EBB6A1A5AA02C80821D,
	LeanMultiUpdateCanvas_OnDisable_m32E45AD294643FFF798572D1606C50F2E7BBFF5A,
	LeanMultiUpdateCanvas_Update_m2C89464C4263ED518AC26D8AE62108E8B96E9153,
	LeanMultiUpdateCanvas_HandleFingerDown_mF0D2F81CF1D1F7D788C0D1C172388EF8A2D92BD2,
	LeanMultiUpdateCanvas_HandleFingerUp_m2114CD840BA76AC2EFF439BE45983558AA81C032,
	LeanMultiUpdateCanvas__ctor_mEB71ECAB087CCE3E51A547E1EE4613430632FF5A,
	LeanPinchCamera_set_Zoom_m993396FB6905F8667AD5FDC04695B94282362BBD,
	LeanPinchCamera_get_Zoom_mCEEA6CA18A8E00EABD30A5FADB29E7BD658A2F77,
	LeanPinchCamera_ContinuouslyZoom_m2BE0B2752EFB7984AA69164931744E132FD3D73D,
	LeanPinchCamera_MultiplyZoom_mD5A59EAA94D69DE5C813DB4BFFD740DB0EABB510,
	LeanPinchCamera_IncrementZoom_m63AD75FB4743748B91C29AFC57073BDAE670D379,
	LeanPinchCamera_AddFinger_mF8AC437A32108C5CDE208D391A30A5399C5C7490,
	LeanPinchCamera_RemoveFinger_m0D9C5A61FAEA37A0321EA6C2BC6BBC2D27539C13,
	LeanPinchCamera_RemoveAllFingers_m482E81B91CEBC6282926A4CFBCC8061B530A3300,
	LeanPinchCamera_Awake_mE724996AFEDBD475AF85B1F3C2F56DAD9E399F79,
	LeanPinchCamera_Start_m0997AF9855BA5742100A8115948C1DBF4AE41D8C,
	LeanPinchCamera_LateUpdate_m3DD4B89BD8575F03EFE48842B96141B32F823E50,
	LeanPinchCamera_SetZoom_m83B8549BB2598B44B36C36465717824215CC01BF,
	LeanPinchCamera_TryClamp_m897B1E966EEC41C9E475D82F35DD25FEE51FD1F8,
	LeanPinchCamera__ctor_mAA431E2C7323AD8F2D8D1D93FA072F9B793A91DD,
	LeanFingerDownTap_get_OnFinger_m0CB430C353F6D5F1815315B3DF55EE0B0AA6C02E,
	LeanFingerDownTap_get_OnWorld_m9A935234A5906011FD59461D48FFED8AE3666A82,
	LeanFingerDownTap_get_OnScreen_m2666F4AC9D7EF0CC0B64FB8AB9BFEB0C97C78CEE,
	LeanFingerDownTap_OnEnable_m9E0BB5A06F4643A8D12C09926A2E93D3CF1343F8,
	LeanFingerDownTap_OnDisable_m7F0D856FFCFC9BE2959059970985F092BB65AAFF,
	LeanFingerDownTap_HandleFingerDown_mD32CE3096C55DA4EFC87E03154F94D942AF58314,
	LeanFingerDownTap__ctor_mDC1884F7BF9DD6E7765FC844DE1985B53795061A,
	LeanReplayFinger_Replay_m9492F34737BF38EA14F2B1A9821A01696F7FE3C7,
	LeanReplayFinger_StopReplay_m1490CEB676F49AB909B75060E312885B34CA9E72,
	LeanReplayFinger_OnEnable_mE7EC7A7401A9F706744E19D927396381E8D33B83,
	LeanReplayFinger_OnDisable_m792FBA952CF0D90A226D70830C9EE99596141ADD,
	LeanReplayFinger_Update_m3DF85FF69BD92E41C366725C7F81384B85B879E7,
	LeanReplayFinger_HandleFingerSet_mA7775E0A49C79ECAC39169EB850815FC8802C442,
	LeanReplayFinger_HandleFingerUp_m0FCE193517AC0E2BEAF8F87FDE9DBEE4CF95153B,
	LeanReplayFinger_CopySnapshots_m4CFFB081D9A871E68F247CC6E126C359EDD75447,
	LeanReplayFinger__ctor_m4ED2A4FF3647FEA28E6D33D5F884EB882F6BFF37,
	LeanSelectSelf_get_OnFinger_m2939EA3835B88F47E40B8ACEF020C458D2F1C781,
	LeanSelectSelf_get_OnWorld_mCE6F329B203EEC13E8179DFB5AAB4D985239F874,
	LeanSelectSelf_TrySelect_m08F3F8C232A502B8DC09FF7B9ECD80CD0BBC5907,
	LeanSelectSelf_TryFindInParent_mDA555964E22ABAC180681B3F533A7AED6CEC2A77,
	LeanSelectSelf_TryFindInChildren_m3FC2108AE19138213C40BEB1C5A087696A1F17D8,
	LeanSelectSelf__ctor_m0137ED5636A9594A0AD527E78F3760B6DF7275F2,
	LeanSelectableBlock_FindBlock_m43AABA7BF5A60FAECA749CD345B11FB5BEB39023,
	LeanSelectableBlock_OnEnable_m4861048ED88884161DFFEDA3992BC1CC50749D27,
	LeanSelectableBlock_OnDisable_mF0D563E01355C635063A0931F8A01B5C9E56D20C,
	LeanSelectableBlock_Swap_mC9C1ABEFA598BE5A23180A9A4BAC4053BB9B6CB6,
	LeanSelectableBlock_Update_m16A904FD6C44C97FCC0FC37F7C323A4BA25BDCC9,
	LeanSelectableBlock__ctor_m51766876AC9433DCF8A82DA1260831808C767222,
	LeanSelectableBlock__cctor_m14C2F5625E95F7BEFF3A50255DF8727F5F218564,
	LeanSelectableCenter_get_OnPosition_m61DB7CE23B694EAC54EBDF46DC5CDC69A204259B,
	LeanSelectableCenter_Calculate_mF82731FBDF1C5092EFA150657A3A1D128B35DDD9,
	LeanSelectableCenter__ctor_m2A81F05026EEF8F8DA098B9CB4ECE79A95ADB55B,
	LeanSelectableCount_OnSelect_mBFA8833173A2F98AFE431576BB0A5FD7215305BB,
	LeanSelectableCount_OnDeselect_mDDCF2B7635FCC948CFE9BED9CD4C0BDB9B439EA3,
	LeanSelectableCount__ctor_m0D2FF060F82DF651957068150007A45C5346DCC4,
	LeanSelectableDial_set_Angle_mE06BAA98649481330409CAE8B4F7417E6F2DAE0E,
	LeanSelectableDial_get_Angle_mB927A9D38E298764C43588B0B738595FF12DDED4,
	LeanSelectableDial_get_OnAngleChanged_m373B30AD619F0D3E6431C9BC591503E54B5F4D2F,
	LeanSelectableDial_IncrementAngle_m55E0ACEBE03A393E003665F09DAE63939799EDF3,
	LeanSelectableDial_Update_mC105787164C2807EC167D667BF40CA8E532DD7A6,
	LeanSelectableDial_GetPoint_mE8879E4F2EA8A54843EAE9F5BACA65BF38BC7A2A,
	LeanSelectableDial__ctor_m8D12EF89926693F56E4BF888ADA113B778D9D23E,
	LeanSelectableDragTorque_OnSelect_m086CD5846F30C5424372E216D0F4FF8FF8FD2B36,
	LeanSelectableDragTorque_Update_mE7A709514601A5A8C30EB60C98E7ECCA58CC903E,
	LeanSelectableDragTorque__ctor_m08C13532F1A6DCFE15E90097392B6328FD01EF72,
	LeanSelectableDrop_get_OnGameObject_mEAA7D49FBAB510966EF231E7222847292004A38A,
	LeanSelectableDrop_get_OnDropHandler_m2224CFA16F39E38529583CF1564F14CCDCC03766,
	LeanSelectableDrop_OnSelectUp_mD734D34B292C41D290BCECEF4B2BA798E51C85BC,
	LeanSelectableDrop_Drop_mF7D2568FFF200C6C1CAA5582D927256B74998781,
	LeanSelectableDrop__ctor_mF4440E259815836F692EE411A54314F8F3BD8351,
	LeanSelectableDrop__cctor_m193BC06AB9A6C8643D1A93E0DAC2C3D716941E05,
	LeanSelectablePressureScale_Update_m022E64DF49BC6E44BDB4BC11CF96DF5A191F35DD,
	LeanSelectablePressureScale__ctor_m2455EEECB6B4FED6C1DB713B70F9E45769D48B5C,
	LeanSelectableSelected_get_OnSelectableDown_m3EE3635149265E3D94774DD591EA5076C1163889,
	LeanSelectableSelected_get_OnSelectableUpdate_mEC2725751CB467A0C2B2AD5A81BDFC8BBB36806F,
	LeanSelectableSelected_get_OnSelectableUp_m264752965CAD228DD4EBF4CEE9614DFE1D9E0D56,
	LeanSelectableSelected_Update_mA6BA7DD12376A4A94454F0F8197C642D3CCC86CF,
	LeanSelectableSelected_OnSelect_m158D5CBCAF5B779EE6F901713BC30D2F38526406,
	LeanSelectableSelected_OnDeselect_m3964FE1A6D9CDACFF11631A1114E1101ACDA5B0C,
	LeanSelectableSelected__ctor_m79FE7291EEDEE8D4811B72640BF75D368725B9A8,
	LeanSelectableTime_Update_m7A22E3C31E1E3EA34A03D98C4E39E9B535A2BD58,
	LeanSelectableTime__ctor_mF02706155831F49954869F3126987694C14E5E17,
	LeanSelectionBox_OnEnable_mB7A1002C21794EC8F6F1A726E53A1D2D3A6F2485,
	LeanSelectionBox_OnDisable_m4047421E5ADCF817B017DB466067A236C1487AB3,
	LeanSelectionBox_HandleFingerDown_m33EFB56D2D04683616C4E548699243B8A4DA1F8C,
	LeanSelectionBox_HandleFingerSet_m82D528E541DB91240DF08CB4560507C95E0B10B5,
	LeanSelectionBox_HandleFingerUp_mD789EA053D7AC7ACB9FAA039C186E311BA1EF656,
	LeanSelectionBox_WriteTransform_m95BAA7B9E57939F73AC0CC63AA40C143FF5575A4,
	LeanSelectionBox__ctor_mF714BE99D11D9F27F46FC75AE21D5DB509705D23,
	LeanSelectionBox__cctor_m98F2960AD1DE8736C5CBA88FFC51EB7D42147104,
	LeanShape_Mod_mA9C6A3DF40F6E9A9EB7EC086CAE16304B10AE5CA,
	LeanShape_GetPoint_m5A0923120ED9002B6F918401EC1614247C2636E8,
	LeanShape_UpdateVisual_m9A985EA9E1D52D1EE3C9C040FB40DE665BB32EA8,
	LeanShape__ctor_mB13EEB87B7A3C044B924CAEB9F39608923114881,
	LeanShapeDetector_get_OnDetected_m88AA64695CBFAB216DFAF76C696DCA49C449A7AE,
	LeanShapeDetector_AddFinger_m8A63CDBB6F7CBC79F4F58D42C8BA0CADD727E9E6,
	LeanShapeDetector_RemoveFinger_m731652375B3749266122517E4E5EF5ECF4C8E1EF,
	LeanShapeDetector_RemoveAllFingers_mDFA61F8413E9CF424110707BF4AD8AA5439D0B6F,
	LeanShapeDetector_OnEnable_mA8D1D253605A03AB1BACF69F21386B600FB75648,
	LeanShapeDetector_OnDisable_m0519E2D2FE3ED5F4B5344F92394969D7518D37CF,
	LeanShapeDetector_HandleFingerDown_m6C41D86D09A3EFDC170B14B90224374FACE83283,
	LeanShapeDetector_HandleFingerUpdate_m7FAE0408CA7DEE0E7F85FCD57C892A7CCA87766A,
	LeanShapeDetector_HandleFingerUp_mE35CDAA113A499C475B9EE7A5BE754C3106BB210,
	LeanShapeDetector_AddRange_mD0AC26145E785B2380D95EEDFA914A42CEF01C74,
	LeanShapeDetector_CalculateMatch_mEFB57B24AAD0F268622EDFC3F6036EAC269B2D0A,
	LeanShapeDetector_FitShape_mC52E202BE46D78F12F8FF3D99F1BC0B4D5FBA878,
	LeanShapeDetector_ConvertPoints_m3C2A6D83CB1EF362966A2F0A9275BB359D41BD09,
	LeanShapeDetector_Read_m134C4151F4A1EBE286F4851847B842E34D960502,
	LeanShapeDetector_GetRect_m49678BF1AA98E5CBCB12CC6D973EDCEF63A89013,
	LeanShapeDetector__ctor_mA0578DD384033F9FAC1B1BE1A99FE81A956F3CFA,
	LeanShapeDetector__cctor_m02BBD424BF13865C4493C0DBF1B7C79B0CD0C5CD,
	LeanSpawnWithFinger_Spawn_m7B88A5BE93DB3947D35F3FFD191A24225ED1E5FF,
	LeanSpawnWithFinger_OnEnable_m2A88FFA343CA960D22744332B7EAE4B5BEFF95F7,
	LeanSpawnWithFinger_OnDisable_m8633E4CBD95D607BC247EF69E8CE5FC71CF4A356,
	LeanSpawnWithFinger_Update_m05192F6CFBD8FF8DBB8FFAD5748FC7CF0C324E7A,
	LeanSpawnWithFinger_UpdateSpawnedTransform_mDE1F0B78EB0DF131E334929DB5492F5DF275AEF1,
	LeanSpawnWithFinger_HandleFingerUp_mA7E3129AE8CB8D932D7414C9437C99B5ACA991C8,
	LeanSpawnWithFinger__ctor_mE018A6379FCEA059BD6851C90D4AEAA0B8305D8D,
	LeanSpawnWithFinger__cctor_m97C86DD6F6E5BE488C1CC580B00889AC3168D5B7,
	LeanSwipeEdge_get_OnEdge_m1BD57424A4B69E661270AEF52822A2558F2488B5,
	LeanSwipeEdge_CheckBetween_m3A2293756FD47E3B0DD85AE3E84D27DD172327F4,
	LeanSwipeEdge_AddFinger_m7A30AAFCB3F0E0B10AD5135488CFFDD9F1979C37,
	LeanSwipeEdge_RemoveFinger_m6782D8A94B0D240EAB75A96216D14653DA3E4096,
	LeanSwipeEdge_RemoveAllFingers_m4B5A1A9396C074BC4050D5E174D80A4814BCD886,
	LeanSwipeEdge_Awake_mF5E03FDCF760BA9F933EE63C853E10E3E85BB959,
	LeanSwipeEdge_Update_mE12B77020D9BA077D050696B5B83113C522386B5,
	LeanSwipeEdge_InvokeEdge_m57B9682317B2D34649E43C9135D0B934E9DEF37C,
	LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3,
	LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B,
	LeanSwipeEdge__ctor_mBABEA72C9A8D796264627C7EAD9D7EECECD79A53,
	LeanTwistCamera_AddFinger_mDFBE1DF25C6AEBE831E491F4B1B9A61B507A3ECD,
	LeanTwistCamera_RemoveFinger_m490128386DCFEF3597E463F42BC7AE86D461B31D,
	LeanTwistCamera_RemoveAllFingers_mE74F8D9E7FA9CB73D2F7E8C946812D4F0E2ADE68,
	LeanTwistCamera_Awake_m73538D6D435E1778292BA73FE291A32D59216847,
	LeanTwistCamera_Update_mF932C5BA25C5AFDC87D121E8AFCB3382083BD43B,
	LeanTwistCamera__ctor_m99D0DB9BB0A2ED9D47AB4C18DB879D2A01412FC1,
	LeanSelectableEvent__ctor_m7D938195530F4F8E01C85954427C839CEFF708F3,
	StringEvent__ctor_mCEE80E5500799ECBCD10F1B7698F4D9B4DE3507F,
	FloatEvent__ctor_mD6FBA9BA39F8FDC8160A19DBF41B9030D0D81FB1,
	Vector2Event__ctor_m3ECD31702F0BB9FA127F067F50326642E0D8D875,
	LeanSelectableEvent__ctor_m59A2A0568E376230D85EB8287FC3758506447F44,
	IntEvent__ctor_mE049A4466C8D289ED96E94A8ED064243DFDE2AC5,
	FloatEvent__ctor_m09663BFE2BD6832D99977F38BB45555D40A18222,
	StringEvent__ctor_m73A679E22F8FACE8C2A059B5AD9B436289CAF333,
	Vector3Vector3Event__ctor_m691D33414AE9D57E683ECF23179442AB9AAFF841,
	Vector3Event__ctor_mDE688E466E2968E2DB215B751FB8518E365D93C0,
	FingerData__ctor_mF0049F1969B8C99D5992411510CF19E9D9EB7CF6,
	GameObjectLeanFingerEvent__ctor_m2C621D50BB193D1232FB782198AF06A0FF555362,
	LeanFingerEvent__ctor_mD743EE12670F1C5DCD1046FBCE8886086E1C23EC,
	Vector3Event__ctor_m1EADF61F42BB689C70CCA3645A6592D93820EA28,
	FingerData__ctor_mAC702941384E0EC7985FFF4F5CAB47F563C3005E,
	FingerData__ctor_mEB2F6F15840F2B943ADA9DA47A3D656B6D3C5A3C,
	LeanFingerEvent__ctor_m0867972AA5E1DE436FADD810EABB276E378D7604,
	Vector3Event__ctor_m803DA834A2C8660841277CC583360D4AC1803514,
	LeanFingerEvent__ctor_m0546ED0C822C04DEFC27E570373A96BC55C15AA4,
	Vector3Event__ctor_m7259E50792081D01CEC7805BF5E3D464E3DFC65F,
	IntEvent__ctor_m74686344DFBE90F8E7E2CC541A76C31898CB33AE,
	LeanFingerEvent__ctor_m47D4AB1E18BEB62CA7C845A2639221216FF9848B,
	Vector3Event__ctor_mCD7FCFB8F38E0FA2F7B5400EAAB55319E5790AA3,
	Vector2Event__ctor_mFA66087B66F278C6F5105F17833DAF064D0183C5,
	LeanFingerEvent__ctor_m6BB3FB399B13E920BF15FA1E28AE970755E24521,
	Vector3Event__ctor_m4A3FB1FBE7349933E1C910C4B6FDE9A1CF460354,
	Vector2Event__ctor_m56F1F71EA3973D20BFCDD2FA49071C7BE9354A1D,
	LeanFingerEvent__ctor_mEE8FF1C21C51C2FC5A97DBCFB7BDBD025495F8A2,
	Vector3Event__ctor_mDF252FA6C68AE09DA137452B3CE5F9F7AEEA03D7,
	Vector2Event__ctor_mF9AD102DA199AE45F76F9086A8360B71B957A722,
	FingerData__ctor_m9944B2C76679158C780530AE95B0303FF790AB11,
	FloatEvent__ctor_mE350F7FD4CC34D241BB2AE160E87B9F957A2896E,
	FloatEvent__ctor_mB407361C76B347F9995D9805C375C04D6255D90D,
	LeanFingerListEvent__ctor_m7835CF2BADC0E2BCB2FA8D3FE42C38F6CF01F1D0,
	Vector3Event__ctor_m87F8FA35C2F5CAF72D3D8CF1A551E0AC009DBE72,
	Vector2Event__ctor_m27112CEB04B70931AC322705A43090E63C1A0371,
	FingerData__ctor_m2258140B9F6C3FB9A8528C9B802A7BDDE796EC5A,
	LeanFingerListEvent__ctor_m61E1D4EE66582756185CC5A28B571D513B0F9461,
	Vector3Event__ctor_m33A9FF532A551C43A3E78BA8DDF5ACF0E72BA0C0,
	Vector2Event__ctor_mD809696839EF461E22513F7865C042AE8B0B4FFB,
	FloatEvent__ctor_m9DFBF58C3A43E306F7AF9859A08FFDBD7C58231B,
	FloatEvent__ctor_mD7D8001ACB970A2D364D072C8E05E23886E292BE,
	Vector2Event__ctor_mC91A14FBAEAD59D7BCE67582A064B729ADEAA640,
	Vector3Event__ctor_m345295CB46802D01E410625A43A6901B8D023668,
	Vector3Vector3Event__ctor_mCEB315ECFA0AFC9ED7910A80C5BF081300466EAC,
	FingerListEvent__ctor_mA6E37846A0647E5A780D557656F07DCF84455404,
	Vector2Event__ctor_mD398D4D8E2E75992A800E9599A591437F45E320A,
	FloatEvent__ctor_m1186CB9C3C251D23F687A27F05DD48079F789FED,
	IntEvent__ctor_mD4107307271F17C8B2AB137C53FB135F03D59F80,
	IntIntEvent__ctor_mCD04EA47196B7AC046DDDD9F0C320FC1A28405D8,
	FloatEvent__ctor_m525AFDB173BC6C05CCDCBE4FE31B1306F26C3502,
	LeanFingerListEvent__ctor_m3A6BCE982E413DEC60867B0E78F3C4F30BE51204,
	Vector3Event__ctor_m8250C087CED02087AE20725306BD3D2B475E440E,
	Vector2Event__ctor_m18684A1BE151BD6623D7C83B015BFD0710C2C12C,
	LeanFingerListEvent__ctor_mB5D07F6BD1A58AD93C5392ED4DAD741A087E4364,
	FloatEvent__ctor_m48AE499081261D9F8DBFB027C29799C251858EDF,
	Vector2Event__ctor_m66D127CECA90862F8A7FD05D71903FDA4DD6D811,
	Vector3Event__ctor_m1ED8A8C66C5D1D9141DA31AE6E644D86F2DE848C,
	Vector3Vector3Event__ctor_mB7FC65C3DD9839C6C79E6603E1C945AC8F3B5211,
	LeanFingerListEvent__ctor_mD1C3A83A1060CBE408A8F2E7E88920A19075AEAC,
	Vector3Event__ctor_mF5B0D217606611606BD2E96BEB9A0D9A7A3C5876,
	LeanFingerEvent__ctor_mD4082E4660FEC0F6D764810B8A30DF7E9333D488,
	Vector3Event__ctor_m4962C522BA459FB51EB99B30CF15CDB360B6546B,
	Vector2Event__ctor_mA075F7B8AC8CA77EDC8051FF4F9DEB8DF84C537F,
	LeanFingerEvent__ctor_mBA41BA4376A87C0F9DE83D9E1CDB593AB0900AFB,
	Vector3Event__ctor_m9EE118D141ED428AFCF99C6F2F0CEC188DA68B7E,
	Vector3Event__ctor_m14674F105A9E7821F566B42129BA816FA9D061F9,
	Trigger_get_OnEnter_mE815F4FED24015A59629B44C805107463F57881F,
	Trigger_get_OnExit_mEC9D2517E3CE558E581E77B6764E3B9154CDF12F,
	Trigger_IsInside_mE8C333A6F8FF4089BBEEF525BED6B9D2C60B8B24,
	Trigger__ctor_m50C4DD6341DC0066BEE7D5172191FBD7D8D99357,
	FloatEvent__ctor_mF59800CE762E3CAE9EF8D6AE90670E398106B7FE,
	GameObjectEvent__ctor_m3E371051FEF017C9437F6D933B5CA547798F9DC7,
	IDropHandlerEvent__ctor_m466E64F22C333B064CD94B8D849E7BC8DCCBBB5A,
	SelectableEvent__ctor_m09AFFDFD8EA1379DFA067DA43990530D84EAF402,
	FingerData__ctor_m748C97AF5997A53C8E89D8457C7F8B971EF9651E,
	LeanFingerEvent__ctor_m2661A1F5658A39B05226E2D7AD710A60EE608BF1,
	FingerData_get_EndPoint_m67160CC61B111D4DFACA6F7AB1948B721CFE4681,
	FingerData__ctor_m6606D6B3FCB5F6089BA5C2A51AE35F99A9FA3071,
	Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4,
	Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433,
	FingerData__ctor_m8BEC4556B84D540B9331DA7E702E7861FE686509,
};
extern void Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4_AdjustorThunk (void);
extern void Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x06000227, Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4_AdjustorThunk },
	{ 0x06000228, Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433_AdjustorThunk },
};
static const int32_t s_InvokerIndices[553] = 
{
	14,
	23,
	23,
	26,
	23,
	26,
	14,
	14,
	335,
	32,
	26,
	23,
	335,
	727,
	31,
	89,
	335,
	727,
	14,
	335,
	23,
	32,
	10,
	31,
	89,
	335,
	727,
	14,
	1435,
	23,
	14,
	23,
	23,
	14,
	23,
	23,
	27,
	23,
	14,
	14,
	14,
	23,
	23,
	27,
	26,
	26,
	23,
	23,
	31,
	89,
	14,
	23,
	23,
	23,
	26,
	14,
	14,
	23,
	23,
	23,
	27,
	26,
	26,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	197,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	27,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	27,
	23,
	27,
	23,
	14,
	14,
	9,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	2735,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	26,
	90,
	26,
	26,
	26,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	26,
	23,
	14,
	14,
	14,
	23,
	23,
	26,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	26,
	23,
	9,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	26,
	23,
	9,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	26,
	2735,
	23,
	26,
	26,
	23,
	23,
	26,
	23,
	335,
	335,
	1435,
	1426,
	1424,
	23,
	335,
	335,
	1435,
	1426,
	1424,
	23,
	14,
	23,
	23,
	23,
	14,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	26,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	26,
	89,
	23,
	23,
	23,
	23,
	14,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	26,
	26,
	23,
	23,
	23,
	27,
	23,
	14,
	14,
	14,
	14,
	26,
	26,
	23,
	23,
	23,
	112,
	23,
	14,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	26,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	14,
	26,
	26,
	23,
	9,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	335,
	727,
	335,
	335,
	335,
	26,
	26,
	23,
	23,
	23,
	23,
	335,
	1989,
	23,
	14,
	14,
	14,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	14,
	14,
	2383,
	90,
	90,
	23,
	590,
	23,
	23,
	137,
	23,
	23,
	3,
	14,
	23,
	23,
	26,
	23,
	23,
	335,
	727,
	14,
	335,
	23,
	1940,
	23,
	26,
	23,
	23,
	14,
	14,
	26,
	27,
	23,
	3,
	23,
	23,
	14,
	14,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	27,
	23,
	3,
	168,
	2737,
	23,
	23,
	14,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	129,
	2738,
	137,
	182,
	2739,
	2740,
	23,
	3,
	26,
	23,
	23,
	23,
	27,
	26,
	23,
	3,
	14,
	1432,
	26,
	26,
	23,
	23,
	23,
	23,
	1765,
	489,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	2736,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1434,
	23,
	2391,
	2391,
	23,
};
extern const Il2CppCodeGenModule g_LeanTouchPlusCodeGenModule;
const Il2CppCodeGenModule g_LeanTouchPlusCodeGenModule = 
{
	"LeanTouchPlus.dll",
	553,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
