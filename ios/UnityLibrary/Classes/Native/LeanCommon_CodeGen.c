﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Lean.Common.LeanDestroy::Update()
extern void LeanDestroy_Update_m707102390BCFF85D1076614DAFAE08EC88A79809 (void);
// 0x00000002 System.Void Lean.Common.LeanDestroy::DestroyNow()
extern void LeanDestroy_DestroyNow_mA36CF042E32091CC87502E7083DBDCD60125BF6E (void);
// 0x00000003 System.Void Lean.Common.LeanDestroy::.ctor()
extern void LeanDestroy__ctor_m6A7FEE6F09E95AB897517B55D1D511BD837BAB86 (void);
// 0x00000004 System.Int32 Lean.Common.LeanPath::get_PointCount()
extern void LeanPath_get_PointCount_m1F2A4108C85A6FDD88714C9AC7C1215BEB96F46E (void);
// 0x00000005 System.Int32 Lean.Common.LeanPath::GetPointCount(System.Int32)
extern void LeanPath_GetPointCount_m7B275FF5BD1ECFEE2A6D422CC8789EF37EB2EF83 (void);
// 0x00000006 UnityEngine.Vector3 Lean.Common.LeanPath::GetSmoothedPoint(System.Single)
extern void LeanPath_GetSmoothedPoint_m9BFEABAD86458A03A551E4ED946C92CD2951FE8D (void);
// 0x00000007 UnityEngine.Vector3 Lean.Common.LeanPath::GetPoint(System.Int32,System.Int32)
extern void LeanPath_GetPoint_mBB74D5F909B33F2B0AA75C5747497F6872BFDE96 (void);
// 0x00000008 UnityEngine.Vector3 Lean.Common.LeanPath::GetPointRaw(System.Int32,System.Int32)
extern void LeanPath_GetPointRaw_mCDCA909AFBC56F2BFCE0F73EE6F3D9F45AC373DC (void);
// 0x00000009 System.Void Lean.Common.LeanPath::SetLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_SetLine_m8353B0C03DF983CF742D6053F573D4340A569087 (void);
// 0x0000000A System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_m8CE444A6FAA81D7C1CA1C8E076C2A586FD6AC4F4 (void);
// 0x0000000B System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_m522C7E9CAA3A3572F9D8622EC1815DA29BC0026C (void);
// 0x0000000C System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_m5DD4F03D5C42DC23AC86DCB45D36AFB080E7B823 (void);
// 0x0000000D System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_m674346FA3F2AAE1DCB71271C57E056DED9570F1B (void);
// 0x0000000E System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32,System.Single)
extern void LeanPath_TryGetClosest_m3B152CCCDBD861184DF4E6400E2A77643B70D82C (void);
// 0x0000000F UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_mEBEE16114D59404DF10CF75E5D121376887B2E48 (void);
// 0x00000010 UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Ray,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_m24B13DFD725ECDC0E2A9FB8657788218A32F4774 (void);
// 0x00000011 System.Single Lean.Common.LeanPath::GetClosestDistance(UnityEngine.Ray,UnityEngine.Vector3)
extern void LeanPath_GetClosestDistance_mCC3C20818C7FB71904AF322ABED2DA92F9CFD07D (void);
// 0x00000012 System.Int32 Lean.Common.LeanPath::Mod(System.Int32,System.Int32)
extern void LeanPath_Mod_m22A5B9C0614A452EB0425726BE6ED7DA5F5915B4 (void);
// 0x00000013 System.Single Lean.Common.LeanPath::CubicInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanPath_CubicInterpolate_m479A58CB17EAD73E5C9BD7551DC1AC16FCBD1384 (void);
// 0x00000014 System.Void Lean.Common.LeanPath::UpdateVisual()
extern void LeanPath_UpdateVisual_mC2538CAECF67C057C3E63D711C9F3FCCF0BEFEB7 (void);
// 0x00000015 System.Void Lean.Common.LeanPath::Update()
extern void LeanPath_Update_m5FBC3F4AD177C2DEB70403ED8A634548BB51A98D (void);
// 0x00000016 System.Void Lean.Common.LeanPath::.ctor()
extern void LeanPath__ctor_m22AAE4CE011E924B6E177365E66BCF3EA65B0C0F (void);
// 0x00000017 System.Void Lean.Common.LeanPath::.cctor()
extern void LeanPath__cctor_m6E069862EC063374F8E6EDCC6FFF47DD1243A094 (void);
// 0x00000018 UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Vector3,System.Single)
extern void LeanPlane_GetClosest_mF1FD4FC1680FC7D097E8A69A02462F3DCD6D0AD3 (void);
// 0x00000019 System.Boolean Lean.Common.LeanPlane::TryRaycast(UnityEngine.Ray,UnityEngine.Vector3&,System.Single,System.Boolean)
extern void LeanPlane_TryRaycast_mA06C6F3D6ED97BB41A2EFB189B61628730AE374E (void);
// 0x0000001A UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Ray,System.Single)
extern void LeanPlane_GetClosest_m9AD009BC6123E44680461561A01A1D8DEF4400FF (void);
// 0x0000001B System.Boolean Lean.Common.LeanPlane::RayToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Ray,System.Single&)
extern void LeanPlane_RayToPlane_mF2226373B89F46445B2DA5BA72172AB7910AA417 (void);
// 0x0000001C System.Void Lean.Common.LeanPlane::.ctor()
extern void LeanPlane__ctor_mBBE7FE0AB0C4EC882224F154EFC1432B1CE07A2E (void);
// 0x0000001D System.Void Lean.Common.LeanRoll::IncrementAngle(System.Single)
extern void LeanRoll_IncrementAngle_m8A93D17ACB0D437501196E5B1FC2E2F79F420D5D (void);
// 0x0000001E System.Void Lean.Common.LeanRoll::DecrementAngle(System.Single)
extern void LeanRoll_DecrementAngle_m3C95A15761B6EE1421CCCCADE7F51B631E143D8D (void);
// 0x0000001F System.Void Lean.Common.LeanRoll::RotateToDelta(UnityEngine.Vector2)
extern void LeanRoll_RotateToDelta_m94748E038BA0C64728C9E900DAD5953B3452A51D (void);
// 0x00000020 System.Void Lean.Common.LeanRoll::SnapToTarget()
extern void LeanRoll_SnapToTarget_m4B30673462D0C47154D2E7978BD820E40E7967A9 (void);
// 0x00000021 System.Void Lean.Common.LeanRoll::Start()
extern void LeanRoll_Start_m3491245DF88E8CDA9F21C412F44EC47069AA110C (void);
// 0x00000022 System.Void Lean.Common.LeanRoll::Update()
extern void LeanRoll_Update_mD3D02C5FD7B876CBEF6B4807BE56B02D36688A06 (void);
// 0x00000023 System.Void Lean.Common.LeanRoll::.ctor()
extern void LeanRoll__ctor_mD9EE4D575752BD0002EE249DF4F32E4AC4B8349F (void);
// 0x00000024 System.Void Lean.Common.LeanSpawn::Spawn()
extern void LeanSpawn_Spawn_m3C9A195396DEF7E950D61E0DC3AC0EFD5C4266E3 (void);
// 0x00000025 System.Void Lean.Common.LeanSpawn::Spawn(UnityEngine.Vector3)
extern void LeanSpawn_Spawn_m631348EDD1CD95BBB3AAC62FDEB142654AA122B2 (void);
// 0x00000026 System.Void Lean.Common.LeanSpawn::.ctor()
extern void LeanSpawn__ctor_mFE1F9E6427AD5099751DC7ABE5BA96B3C992B3AC (void);
// 0x00000027 T Lean.Common.LeanHelper::CreateElement(UnityEngine.Transform)
// 0x00000028 System.Single Lean.Common.LeanHelper::GetDampenFactor(System.Single,System.Single)
extern void LeanHelper_GetDampenFactor_mDCE92ADEFA6CD274B18AFD9C541529DD3DBA4642 (void);
// 0x00000029 T Lean.Common.LeanHelper::Destroy(T)
// 0x0000002A UnityEngine.Camera Lean.Common.LeanHelper::GetCamera(UnityEngine.Camera,UnityEngine.GameObject)
extern void LeanHelper_GetCamera_m36B3A0AE813922D60EB7E218A95D9DEB7C87F130 (void);
// 0x0000002B UnityEngine.Vector2 Lean.Common.LeanHelper::Hermite(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanHelper_Hermite_m35A2F34689850BA2589424A73D7D85DB9439BA07 (void);
// 0x0000002C System.Single Lean.Common.LeanHelper::HermiteInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanHelper_HermiteInterpolate_mF2AE41410718AC18E55CB19A54BD0DCBE561E2E5 (void);
// 0x0000002D System.Int32 Lean.Common.LeanInput::GetTouchCount()
extern void LeanInput_GetTouchCount_m743D94DD8A026F81487144ABC8F375015AE0F091 (void);
// 0x0000002E System.Void Lean.Common.LeanInput::GetTouch(System.Int32,System.Int32&,UnityEngine.Vector2&,System.Single&,System.Boolean&)
extern void LeanInput_GetTouch_m364DE97BB5C7954B8B34C5E9331BFAA9D72C09A5 (void);
// 0x0000002F UnityEngine.Vector2 Lean.Common.LeanInput::GetMousePosition()
extern void LeanInput_GetMousePosition_mF3EF9EF9D99349CA88667DF4BB9EACED4F34A2A1 (void);
// 0x00000030 System.Boolean Lean.Common.LeanInput::GetDown(UnityEngine.KeyCode)
extern void LeanInput_GetDown_m1B52B3952558F4B1CC05336879F18A40C8D78B87 (void);
// 0x00000031 System.Boolean Lean.Common.LeanInput::GetPressed(UnityEngine.KeyCode)
extern void LeanInput_GetPressed_m4ABB38639517EA80AC0CA60309DF253EE2B0A5C5 (void);
// 0x00000032 System.Boolean Lean.Common.LeanInput::GetUp(UnityEngine.KeyCode)
extern void LeanInput_GetUp_m7B05ADFCF13FCC16C0E599DE0CFC5A343B2B962A (void);
// 0x00000033 System.Boolean Lean.Common.LeanInput::GetMouseDown(System.Int32)
extern void LeanInput_GetMouseDown_m8161E4CE4739BD23193B840FF0B66FD9F8590C0A (void);
// 0x00000034 System.Boolean Lean.Common.LeanInput::GetMousePressed(System.Int32)
extern void LeanInput_GetMousePressed_mE8D78DF920BFC9AB30B4B43C0BF08D6FCC29B8FA (void);
// 0x00000035 System.Boolean Lean.Common.LeanInput::GetMouseUp(System.Int32)
extern void LeanInput_GetMouseUp_m8EA0949707BB8F058BCF4AD007092426BEC47A37 (void);
// 0x00000036 System.Single Lean.Common.LeanInput::GetMouseWheelDelta()
extern void LeanInput_GetMouseWheelDelta_m2934275461C131FF46469D81AA0A4FEF52EF7E41 (void);
// 0x00000037 System.Boolean Lean.Common.LeanInput::GetMouseExists()
extern void LeanInput_GetMouseExists_m99A1F8CB4034816DA7BA21E80E4B9AEC9E073FDC (void);
// 0x00000038 System.Boolean Lean.Common.LeanInput::GetKeyboardExists()
extern void LeanInput_GetKeyboardExists_mB7CF38C72DF798E02BA4A513545B958301626332 (void);
// 0x00000039 UnityEngine.Texture2D Lean.Common.Examples.LeanGuide::get_Icon()
extern void LeanGuide_get_Icon_m680AAEA366808134A4589CB715966342DB361FF0 (void);
// 0x0000003A System.String Lean.Common.Examples.LeanGuide::get_Version()
extern void LeanGuide_get_Version_m41A126EFCD85E72863A32EC17135DA32FFAE55F2 (void);
// 0x0000003B System.Void Lean.Common.Examples.LeanGuide::.ctor()
extern void LeanGuide__ctor_m2A8BFCB853FD105F4954991E4A823CF0ED41DECA (void);
// 0x0000003C System.Void Lean.Common.Examples.LeanLinkTo::set_Link(Lean.Common.Examples.LeanLinkTo/LinkType)
extern void LeanLinkTo_set_Link_mD08163285B0754B3EBE24F3CD661AC87D22774BD (void);
// 0x0000003D Lean.Common.Examples.LeanLinkTo/LinkType Lean.Common.Examples.LeanLinkTo::get_Link()
extern void LeanLinkTo_get_Link_m6C153A64C1F1B6B610477DC951F05E0BB473DAEE (void);
// 0x0000003E System.Void Lean.Common.Examples.LeanLinkTo::Update()
extern void LeanLinkTo_Update_m65D9FF85D61F5F7E3A4AAB52567A2C31B8DD1E1E (void);
// 0x0000003F System.Void Lean.Common.Examples.LeanLinkTo::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LeanLinkTo_OnPointerClick_mCE3075979F3721650C5CE5B574578C4225A71881 (void);
// 0x00000040 System.Int32 Lean.Common.Examples.LeanLinkTo::GetCurrentLevel()
extern void LeanLinkTo_GetCurrentLevel_m1D365D688EE573BC58128D9B49F795A71CE41523 (void);
// 0x00000041 System.Int32 Lean.Common.Examples.LeanLinkTo::GetLevelCount()
extern void LeanLinkTo_GetLevelCount_m7E99F5206E716B18DADE56CFA6B7AE6624633B67 (void);
// 0x00000042 System.Void Lean.Common.Examples.LeanLinkTo::LoadLevel(System.Int32)
extern void LeanLinkTo_LoadLevel_m62C55C0442228C95BAAB043631BDB8FDD5BA7626 (void);
// 0x00000043 System.Void Lean.Common.Examples.LeanLinkTo::.ctor()
extern void LeanLinkTo__ctor_m8D6B0032479F4C40EC6EA88A8CB586E8A588DAC6 (void);
// 0x00000044 System.Void Lean.Common.Examples.LeanUpgradeEventSystem::.ctor()
extern void LeanUpgradeEventSystem__ctor_m14DC4D653F96605CC675980F58EB2F50C77A8DEB (void);
static Il2CppMethodPointer s_methodPointers[68] = 
{
	LeanDestroy_Update_m707102390BCFF85D1076614DAFAE08EC88A79809,
	LeanDestroy_DestroyNow_mA36CF042E32091CC87502E7083DBDCD60125BF6E,
	LeanDestroy__ctor_m6A7FEE6F09E95AB897517B55D1D511BD837BAB86,
	LeanPath_get_PointCount_m1F2A4108C85A6FDD88714C9AC7C1215BEB96F46E,
	LeanPath_GetPointCount_m7B275FF5BD1ECFEE2A6D422CC8789EF37EB2EF83,
	LeanPath_GetSmoothedPoint_m9BFEABAD86458A03A551E4ED946C92CD2951FE8D,
	LeanPath_GetPoint_mBB74D5F909B33F2B0AA75C5747497F6872BFDE96,
	LeanPath_GetPointRaw_mCDCA909AFBC56F2BFCE0F73EE6F3D9F45AC373DC,
	LeanPath_SetLine_m8353B0C03DF983CF742D6053F573D4340A569087,
	LeanPath_TryGetClosest_m8CE444A6FAA81D7C1CA1C8E076C2A586FD6AC4F4,
	LeanPath_TryGetClosest_m522C7E9CAA3A3572F9D8622EC1815DA29BC0026C,
	LeanPath_TryGetClosest_m5DD4F03D5C42DC23AC86DCB45D36AFB080E7B823,
	LeanPath_TryGetClosest_m674346FA3F2AAE1DCB71271C57E056DED9570F1B,
	LeanPath_TryGetClosest_m3B152CCCDBD861184DF4E6400E2A77643B70D82C,
	LeanPath_GetClosestPoint_mEBEE16114D59404DF10CF75E5D121376887B2E48,
	LeanPath_GetClosestPoint_m24B13DFD725ECDC0E2A9FB8657788218A32F4774,
	LeanPath_GetClosestDistance_mCC3C20818C7FB71904AF322ABED2DA92F9CFD07D,
	LeanPath_Mod_m22A5B9C0614A452EB0425726BE6ED7DA5F5915B4,
	LeanPath_CubicInterpolate_m479A58CB17EAD73E5C9BD7551DC1AC16FCBD1384,
	LeanPath_UpdateVisual_mC2538CAECF67C057C3E63D711C9F3FCCF0BEFEB7,
	LeanPath_Update_m5FBC3F4AD177C2DEB70403ED8A634548BB51A98D,
	LeanPath__ctor_m22AAE4CE011E924B6E177365E66BCF3EA65B0C0F,
	LeanPath__cctor_m6E069862EC063374F8E6EDCC6FFF47DD1243A094,
	LeanPlane_GetClosest_mF1FD4FC1680FC7D097E8A69A02462F3DCD6D0AD3,
	LeanPlane_TryRaycast_mA06C6F3D6ED97BB41A2EFB189B61628730AE374E,
	LeanPlane_GetClosest_m9AD009BC6123E44680461561A01A1D8DEF4400FF,
	LeanPlane_RayToPlane_mF2226373B89F46445B2DA5BA72172AB7910AA417,
	LeanPlane__ctor_mBBE7FE0AB0C4EC882224F154EFC1432B1CE07A2E,
	LeanRoll_IncrementAngle_m8A93D17ACB0D437501196E5B1FC2E2F79F420D5D,
	LeanRoll_DecrementAngle_m3C95A15761B6EE1421CCCCADE7F51B631E143D8D,
	LeanRoll_RotateToDelta_m94748E038BA0C64728C9E900DAD5953B3452A51D,
	LeanRoll_SnapToTarget_m4B30673462D0C47154D2E7978BD820E40E7967A9,
	LeanRoll_Start_m3491245DF88E8CDA9F21C412F44EC47069AA110C,
	LeanRoll_Update_mD3D02C5FD7B876CBEF6B4807BE56B02D36688A06,
	LeanRoll__ctor_mD9EE4D575752BD0002EE249DF4F32E4AC4B8349F,
	LeanSpawn_Spawn_m3C9A195396DEF7E950D61E0DC3AC0EFD5C4266E3,
	LeanSpawn_Spawn_m631348EDD1CD95BBB3AAC62FDEB142654AA122B2,
	LeanSpawn__ctor_mFE1F9E6427AD5099751DC7ABE5BA96B3C992B3AC,
	NULL,
	LeanHelper_GetDampenFactor_mDCE92ADEFA6CD274B18AFD9C541529DD3DBA4642,
	NULL,
	LeanHelper_GetCamera_m36B3A0AE813922D60EB7E218A95D9DEB7C87F130,
	LeanHelper_Hermite_m35A2F34689850BA2589424A73D7D85DB9439BA07,
	LeanHelper_HermiteInterpolate_mF2AE41410718AC18E55CB19A54BD0DCBE561E2E5,
	LeanInput_GetTouchCount_m743D94DD8A026F81487144ABC8F375015AE0F091,
	LeanInput_GetTouch_m364DE97BB5C7954B8B34C5E9331BFAA9D72C09A5,
	LeanInput_GetMousePosition_mF3EF9EF9D99349CA88667DF4BB9EACED4F34A2A1,
	LeanInput_GetDown_m1B52B3952558F4B1CC05336879F18A40C8D78B87,
	LeanInput_GetPressed_m4ABB38639517EA80AC0CA60309DF253EE2B0A5C5,
	LeanInput_GetUp_m7B05ADFCF13FCC16C0E599DE0CFC5A343B2B962A,
	LeanInput_GetMouseDown_m8161E4CE4739BD23193B840FF0B66FD9F8590C0A,
	LeanInput_GetMousePressed_mE8D78DF920BFC9AB30B4B43C0BF08D6FCC29B8FA,
	LeanInput_GetMouseUp_m8EA0949707BB8F058BCF4AD007092426BEC47A37,
	LeanInput_GetMouseWheelDelta_m2934275461C131FF46469D81AA0A4FEF52EF7E41,
	LeanInput_GetMouseExists_m99A1F8CB4034816DA7BA21E80E4B9AEC9E073FDC,
	LeanInput_GetKeyboardExists_mB7CF38C72DF798E02BA4A513545B958301626332,
	LeanGuide_get_Icon_m680AAEA366808134A4589CB715966342DB361FF0,
	LeanGuide_get_Version_m41A126EFCD85E72863A32EC17135DA32FFAE55F2,
	LeanGuide__ctor_m2A8BFCB853FD105F4954991E4A823CF0ED41DECA,
	LeanLinkTo_set_Link_mD08163285B0754B3EBE24F3CD661AC87D22774BD,
	LeanLinkTo_get_Link_m6C153A64C1F1B6B610477DC951F05E0BB473DAEE,
	LeanLinkTo_Update_m65D9FF85D61F5F7E3A4AAB52567A2C31B8DD1E1E,
	LeanLinkTo_OnPointerClick_mCE3075979F3721650C5CE5B574578C4225A71881,
	LeanLinkTo_GetCurrentLevel_m1D365D688EE573BC58128D9B49F795A71CE41523,
	LeanLinkTo_GetLevelCount_m7E99F5206E716B18DADE56CFA6B7AE6624633B67,
	LeanLinkTo_LoadLevel_m62C55C0442228C95BAAB043631BDB8FDD5BA7626,
	LeanLinkTo__ctor_m8D6B0032479F4C40EC6EA88A8CB586E8A588DAC6,
	LeanUpgradeEventSystem__ctor_m14DC4D653F96605CC675980F58EB2F50C77A8DEB,
};
static const int32_t s_InvokerIndices[68] = 
{
	23,
	23,
	23,
	10,
	37,
	1430,
	2054,
	2054,
	1426,
	2055,
	2056,
	2057,
	2058,
	2059,
	2060,
	2061,
	2062,
	56,
	2063,
	23,
	23,
	23,
	3,
	2064,
	2065,
	2066,
	2067,
	23,
	335,
	335,
	1435,
	23,
	23,
	23,
	23,
	23,
	1424,
	23,
	-1,
	442,
	-1,
	1,
	2068,
	2069,
	106,
	2070,
	1631,
	46,
	46,
	46,
	46,
	46,
	46,
	1449,
	49,
	49,
	14,
	14,
	23,
	32,
	10,
	23,
	26,
	106,
	106,
	164,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000027, { 0, 3 } },
	{ 0x06000029, { 3, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)1, 24173 },
	{ (Il2CppRGCTXDataType)3, 21686 },
	{ (Il2CppRGCTXDataType)2, 24173 },
	{ (Il2CppRGCTXDataType)2, 24174 },
};
extern const Il2CppCodeGenModule g_LeanCommonCodeGenModule;
const Il2CppCodeGenModule g_LeanCommonCodeGenModule = 
{
	"LeanCommon.dll",
	68,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
};
