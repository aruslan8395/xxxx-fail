﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::GetHandle()
extern void VideoClipPlayable_GetHandle_m5B2EFD8CFE93DB4D2EE29A600E598233471DF242 (void);
// 0x00000002 System.Boolean UnityEngine.Experimental.Video.VideoClipPlayable::Equals(UnityEngine.Experimental.Video.VideoClipPlayable)
extern void VideoClipPlayable_Equals_m2F07E6EBA96043274F2384F18E43B66035058BC2 (void);
// 0x00000003 System.Void UnityEngine.Video.VideoClip::.ctor()
extern void VideoClip__ctor_m9BB7ECD808FCBD3DA63594134BEEC12B17852117 (void);
// 0x00000004 UnityEngine.Video.VideoSource UnityEngine.Video.VideoPlayer::get_source()
extern void VideoPlayer_get_source_m0B59B963847A0C4F206697FB61C618D9621949E4 (void);
// 0x00000005 System.Void UnityEngine.Video.VideoPlayer::set_source(UnityEngine.Video.VideoSource)
extern void VideoPlayer_set_source_mE27652C749468B4A44FE3557BA809C503C849ABE (void);
// 0x00000006 System.String UnityEngine.Video.VideoPlayer::get_url()
extern void VideoPlayer_get_url_mC8F4EC64F3938721095E741897A83F4A35741203 (void);
// 0x00000007 System.Void UnityEngine.Video.VideoPlayer::set_url(System.String)
extern void VideoPlayer_set_url_m076BC425E754574E8DFCDFA7AC2A37A3EFD5AF21 (void);
// 0x00000008 UnityEngine.Video.VideoClip UnityEngine.Video.VideoPlayer::get_clip()
extern void VideoPlayer_get_clip_mA2C3AC016BB7B09855C56A11843AE60EB4D36B76 (void);
// 0x00000009 System.Void UnityEngine.Video.VideoPlayer::set_clip(UnityEngine.Video.VideoClip)
extern void VideoPlayer_set_clip_mD8D5AA8642010DFACD0B88F38FAF7CBBB8DD4E64 (void);
// 0x0000000A UnityEngine.Video.VideoRenderMode UnityEngine.Video.VideoPlayer::get_renderMode()
extern void VideoPlayer_get_renderMode_mFBFD12B8E2C366663AA5F55A6087A4BB137D447E (void);
// 0x0000000B System.Void UnityEngine.Video.VideoPlayer::set_renderMode(UnityEngine.Video.VideoRenderMode)
extern void VideoPlayer_set_renderMode_m9DC3CFC4B99F66FA6E6116379D8C7F64D798CB63 (void);
// 0x0000000C UnityEngine.Camera UnityEngine.Video.VideoPlayer::get_targetCamera()
extern void VideoPlayer_get_targetCamera_mFDF3BD6E156461CC2C6BB44878886EDE4C995687 (void);
// 0x0000000D System.Void UnityEngine.Video.VideoPlayer::set_targetCamera(UnityEngine.Camera)
extern void VideoPlayer_set_targetCamera_mAD892B74820C093D33A8808C6CA4BFF7935773C4 (void);
// 0x0000000E UnityEngine.RenderTexture UnityEngine.Video.VideoPlayer::get_targetTexture()
extern void VideoPlayer_get_targetTexture_mDFF8E4B60BDFE26F5535E5D15CAAA8546DB939E3 (void);
// 0x0000000F System.Void UnityEngine.Video.VideoPlayer::set_targetTexture(UnityEngine.RenderTexture)
extern void VideoPlayer_set_targetTexture_m4088B6058A001D48C911DC71023AC723BD3C8252 (void);
// 0x00000010 UnityEngine.Renderer UnityEngine.Video.VideoPlayer::get_targetMaterialRenderer()
extern void VideoPlayer_get_targetMaterialRenderer_m7006B5CB698247A457EAE8325709C0A09883CFEC (void);
// 0x00000011 System.Void UnityEngine.Video.VideoPlayer::set_targetMaterialRenderer(UnityEngine.Renderer)
extern void VideoPlayer_set_targetMaterialRenderer_mA5BDE908C845D6023DEEA3013C78E357020D106D (void);
// 0x00000012 System.String UnityEngine.Video.VideoPlayer::get_targetMaterialProperty()
extern void VideoPlayer_get_targetMaterialProperty_mB8798BF77416FFAA2CA67BB0B2664C0DD6E072FB (void);
// 0x00000013 System.Void UnityEngine.Video.VideoPlayer::set_targetMaterialProperty(System.String)
extern void VideoPlayer_set_targetMaterialProperty_m8BF9B84E27C0C3090FA0231FC2C449B2BED9435F (void);
// 0x00000014 UnityEngine.Video.VideoAspectRatio UnityEngine.Video.VideoPlayer::get_aspectRatio()
extern void VideoPlayer_get_aspectRatio_mF5A5478D9C1E0FDE8A530CEE17B043A4C57C5E07 (void);
// 0x00000015 System.Void UnityEngine.Video.VideoPlayer::set_aspectRatio(UnityEngine.Video.VideoAspectRatio)
extern void VideoPlayer_set_aspectRatio_mEA5D88F04CF46AE08D472146D0A0274DA5E97065 (void);
// 0x00000016 System.Single UnityEngine.Video.VideoPlayer::get_targetCameraAlpha()
extern void VideoPlayer_get_targetCameraAlpha_mD196E8A77E2D28FF9F90842D18320843F52D6184 (void);
// 0x00000017 System.Void UnityEngine.Video.VideoPlayer::set_targetCameraAlpha(System.Single)
extern void VideoPlayer_set_targetCameraAlpha_m3326BD7558151BF416178CCE7C26ACC0C55D7660 (void);
// 0x00000018 UnityEngine.Video.Video3DLayout UnityEngine.Video.VideoPlayer::get_targetCamera3DLayout()
extern void VideoPlayer_get_targetCamera3DLayout_m05C57CC8E9F25B637711613D064B357633F40629 (void);
// 0x00000019 System.Void UnityEngine.Video.VideoPlayer::set_targetCamera3DLayout(UnityEngine.Video.Video3DLayout)
extern void VideoPlayer_set_targetCamera3DLayout_m9D29143E9BCEE303E21287DD4B254A042CAC076A (void);
// 0x0000001A UnityEngine.Texture UnityEngine.Video.VideoPlayer::get_texture()
extern void VideoPlayer_get_texture_m7FEBFE07CDB830FCAD4B5B2D7EF63D12E8C034CC (void);
// 0x0000001B System.Void UnityEngine.Video.VideoPlayer::Prepare()
extern void VideoPlayer_Prepare_m32AB43745A92A6762D570E60975AD69DB8FFF566 (void);
// 0x0000001C System.Boolean UnityEngine.Video.VideoPlayer::get_isPrepared()
extern void VideoPlayer_get_isPrepared_mF0DC157BD0B5E8FF26A27EA7ABE4BEDCBE963694 (void);
// 0x0000001D System.Boolean UnityEngine.Video.VideoPlayer::get_waitForFirstFrame()
extern void VideoPlayer_get_waitForFirstFrame_m0A34CAAA86DC05A4EC680FB127D6B9B015A0E67E (void);
// 0x0000001E System.Void UnityEngine.Video.VideoPlayer::set_waitForFirstFrame(System.Boolean)
extern void VideoPlayer_set_waitForFirstFrame_m8E11586971ADCEBC6535C065ABB23BC5E4C49875 (void);
// 0x0000001F System.Boolean UnityEngine.Video.VideoPlayer::get_playOnAwake()
extern void VideoPlayer_get_playOnAwake_m3F510732AC70AF08A33907810B3AD0D3672EDB68 (void);
// 0x00000020 System.Void UnityEngine.Video.VideoPlayer::set_playOnAwake(System.Boolean)
extern void VideoPlayer_set_playOnAwake_m3109BDD49A2981518F062FACBF0ECF1DB8BFE207 (void);
// 0x00000021 System.Void UnityEngine.Video.VideoPlayer::Play()
extern void VideoPlayer_Play_m2BCD775F42A36AC291C7B32D9E4D934EF0B91257 (void);
// 0x00000022 System.Void UnityEngine.Video.VideoPlayer::Pause()
extern void VideoPlayer_Pause_m62EE5660CFA287D78BB3FE815CA5649242509B93 (void);
// 0x00000023 System.Void UnityEngine.Video.VideoPlayer::Stop()
extern void VideoPlayer_Stop_mBEDCB786A42A95603F9D8B7763DEC3BD97565852 (void);
// 0x00000024 System.Boolean UnityEngine.Video.VideoPlayer::get_isPlaying()
extern void VideoPlayer_get_isPlaying_mC7CFE17762C14F2AFB1D73500317B9D25A7395DF (void);
// 0x00000025 System.Boolean UnityEngine.Video.VideoPlayer::get_isPaused()
extern void VideoPlayer_get_isPaused_m84665EE1EB3F31A1C30D9D3E17515802E446E849 (void);
// 0x00000026 System.Boolean UnityEngine.Video.VideoPlayer::get_canSetTime()
extern void VideoPlayer_get_canSetTime_m875362EEC923AAAAA882CC775CC9816AE974603C (void);
// 0x00000027 System.Double UnityEngine.Video.VideoPlayer::get_time()
extern void VideoPlayer_get_time_mF842FC1E9A1FD2333C7C9D13338D3B178665564A (void);
// 0x00000028 System.Void UnityEngine.Video.VideoPlayer::set_time(System.Double)
extern void VideoPlayer_set_time_m474F47BCE704FFCB3C6FFAAE6414A9ED89678514 (void);
// 0x00000029 System.Int64 UnityEngine.Video.VideoPlayer::get_frame()
extern void VideoPlayer_get_frame_mB7F5972A74C2D4039855454F552AD08BF12F30A0 (void);
// 0x0000002A System.Void UnityEngine.Video.VideoPlayer::set_frame(System.Int64)
extern void VideoPlayer_set_frame_m41D0EAE134C1D62C1766817C652419495C17E4C4 (void);
// 0x0000002B System.Double UnityEngine.Video.VideoPlayer::get_clockTime()
extern void VideoPlayer_get_clockTime_mF8CA7C444B3CA3DBC479EF8FDABFD484C9A9BD41 (void);
// 0x0000002C System.Boolean UnityEngine.Video.VideoPlayer::get_canStep()
extern void VideoPlayer_get_canStep_mCCE3965DAF4B2805F0451BAACA41864F8309872D (void);
// 0x0000002D System.Void UnityEngine.Video.VideoPlayer::StepForward()
extern void VideoPlayer_StepForward_m8B9A90273A3FA36EDE431773EF97E15F9151A899 (void);
// 0x0000002E System.Boolean UnityEngine.Video.VideoPlayer::get_canSetPlaybackSpeed()
extern void VideoPlayer_get_canSetPlaybackSpeed_m06A2D6459930861D61847ED28A29DAD6C668A788 (void);
// 0x0000002F System.Single UnityEngine.Video.VideoPlayer::get_playbackSpeed()
extern void VideoPlayer_get_playbackSpeed_m6A0A48C222262FADDA28565AD3D36C45B92D31A6 (void);
// 0x00000030 System.Void UnityEngine.Video.VideoPlayer::set_playbackSpeed(System.Single)
extern void VideoPlayer_set_playbackSpeed_m76CDBC2141AE66474AFCB707A6E0EE77454628A6 (void);
// 0x00000031 System.Boolean UnityEngine.Video.VideoPlayer::get_isLooping()
extern void VideoPlayer_get_isLooping_m096B276AD270A2C03C517262D4DA100DEC1A2014 (void);
// 0x00000032 System.Void UnityEngine.Video.VideoPlayer::set_isLooping(System.Boolean)
extern void VideoPlayer_set_isLooping_m0FA87AE5A8F06A545349EAA8F0C64158E29700C7 (void);
// 0x00000033 System.Boolean UnityEngine.Video.VideoPlayer::get_canSetTimeSource()
extern void VideoPlayer_get_canSetTimeSource_m7B87AE87F7F18588A51241D68F599C0FDD3B6853 (void);
// 0x00000034 UnityEngine.Video.VideoTimeSource UnityEngine.Video.VideoPlayer::get_timeSource()
extern void VideoPlayer_get_timeSource_m57C09418AD61A6C849ABBBDF7F1469F66EEA2656 (void);
// 0x00000035 System.Void UnityEngine.Video.VideoPlayer::set_timeSource(UnityEngine.Video.VideoTimeSource)
extern void VideoPlayer_set_timeSource_mBCED110DD2B0822D2F719CEBD1D103E9E7D3F2EB (void);
// 0x00000036 UnityEngine.Video.VideoTimeReference UnityEngine.Video.VideoPlayer::get_timeReference()
extern void VideoPlayer_get_timeReference_m34BC38D0539B1EAB554230892E5173E923D4257C (void);
// 0x00000037 System.Void UnityEngine.Video.VideoPlayer::set_timeReference(UnityEngine.Video.VideoTimeReference)
extern void VideoPlayer_set_timeReference_m7EBE45F2FCF7326831EB92B2C125A6D7A4EBFCB4 (void);
// 0x00000038 System.Double UnityEngine.Video.VideoPlayer::get_externalReferenceTime()
extern void VideoPlayer_get_externalReferenceTime_mCFCD9B4ED9C4DA648BF0F3C454208BFB66C72744 (void);
// 0x00000039 System.Void UnityEngine.Video.VideoPlayer::set_externalReferenceTime(System.Double)
extern void VideoPlayer_set_externalReferenceTime_mB900C62E1E95E5200B3065DF8FD5C8863128631F (void);
// 0x0000003A System.Boolean UnityEngine.Video.VideoPlayer::get_canSetSkipOnDrop()
extern void VideoPlayer_get_canSetSkipOnDrop_mA3D1E789DDCF90416DDD15D95DD32B73135D8210 (void);
// 0x0000003B System.Boolean UnityEngine.Video.VideoPlayer::get_skipOnDrop()
extern void VideoPlayer_get_skipOnDrop_m2690695BAFD5C39A6A2E65153609D8257D820362 (void);
// 0x0000003C System.Void UnityEngine.Video.VideoPlayer::set_skipOnDrop(System.Boolean)
extern void VideoPlayer_set_skipOnDrop_mBAA06EE0F8E6E41AA3CC43F0791E76BF2C14C0C8 (void);
// 0x0000003D System.UInt64 UnityEngine.Video.VideoPlayer::get_frameCount()
extern void VideoPlayer_get_frameCount_m89C61BE7B88F1A573FA42C2A7564230A2234F709 (void);
// 0x0000003E System.Single UnityEngine.Video.VideoPlayer::get_frameRate()
extern void VideoPlayer_get_frameRate_mEC5D740D0A22EBB929373F80343AB4675D6F05DE (void);
// 0x0000003F System.Double UnityEngine.Video.VideoPlayer::get_length()
extern void VideoPlayer_get_length_m593F8FA891C7B1D76BFAF6F1DFFAE2BB85FEDDC3 (void);
// 0x00000040 System.UInt32 UnityEngine.Video.VideoPlayer::get_width()
extern void VideoPlayer_get_width_mA6C581E0855C11ED7557394B6E40D96A1EC51218 (void);
// 0x00000041 System.UInt32 UnityEngine.Video.VideoPlayer::get_height()
extern void VideoPlayer_get_height_mF23A572FDF45854EE609FCFBD8655B137ECC1EC7 (void);
// 0x00000042 System.UInt32 UnityEngine.Video.VideoPlayer::get_pixelAspectRatioNumerator()
extern void VideoPlayer_get_pixelAspectRatioNumerator_mF75E3B2FED1EFB5351417D711BF8D9D3037A1D26 (void);
// 0x00000043 System.UInt32 UnityEngine.Video.VideoPlayer::get_pixelAspectRatioDenominator()
extern void VideoPlayer_get_pixelAspectRatioDenominator_mB80E2E7F6DB301F45A4B180CB54F3DAEE8D76DD2 (void);
// 0x00000044 System.UInt16 UnityEngine.Video.VideoPlayer::get_audioTrackCount()
extern void VideoPlayer_get_audioTrackCount_m9B51B6DCE2D782A177500AC4C5DC645813FE7C44 (void);
// 0x00000045 System.String UnityEngine.Video.VideoPlayer::GetAudioLanguageCode(System.UInt16)
extern void VideoPlayer_GetAudioLanguageCode_m49BB753D26C2B5B091579D5515C2DC6AF5D07050 (void);
// 0x00000046 System.UInt16 UnityEngine.Video.VideoPlayer::GetAudioChannelCount(System.UInt16)
extern void VideoPlayer_GetAudioChannelCount_m0C900E5923939CF03D8DA4F0843E8C5C31DB140C (void);
// 0x00000047 System.UInt32 UnityEngine.Video.VideoPlayer::GetAudioSampleRate(System.UInt16)
extern void VideoPlayer_GetAudioSampleRate_m5EB7367A99A3F48D86F4CE96B9241D2F6A865C30 (void);
// 0x00000048 System.UInt16 UnityEngine.Video.VideoPlayer::get_controlledAudioTrackMaxCount()
extern void VideoPlayer_get_controlledAudioTrackMaxCount_m5CD07FAE01ADA002F88F9E7B3432993AB52DAAF1 (void);
// 0x00000049 System.UInt16 UnityEngine.Video.VideoPlayer::get_controlledAudioTrackCount()
extern void VideoPlayer_get_controlledAudioTrackCount_mF9B0DA53C041F8B8B3809860C3CFA11C9FF6D188 (void);
// 0x0000004A System.Void UnityEngine.Video.VideoPlayer::set_controlledAudioTrackCount(System.UInt16)
extern void VideoPlayer_set_controlledAudioTrackCount_mE67D147B78D4DB81593E6E29631D475D98E7DA3B (void);
// 0x0000004B System.UInt16 UnityEngine.Video.VideoPlayer::GetControlledAudioTrackCount()
extern void VideoPlayer_GetControlledAudioTrackCount_m255D8594D8339E4175F9084E4D008C8D3F071AA7 (void);
// 0x0000004C System.Void UnityEngine.Video.VideoPlayer::SetControlledAudioTrackCount(System.UInt16)
extern void VideoPlayer_SetControlledAudioTrackCount_m4E37B42737D9CA58DD795F30DC42FF48CB0E1069 (void);
// 0x0000004D System.Void UnityEngine.Video.VideoPlayer::EnableAudioTrack(System.UInt16,System.Boolean)
extern void VideoPlayer_EnableAudioTrack_m30901BF68B20BB8397E246FDDE699E5D513AAA5B (void);
// 0x0000004E System.Boolean UnityEngine.Video.VideoPlayer::IsAudioTrackEnabled(System.UInt16)
extern void VideoPlayer_IsAudioTrackEnabled_mD2D5ED119462509DA30BF826B116A6BBAA7632DE (void);
// 0x0000004F UnityEngine.Video.VideoAudioOutputMode UnityEngine.Video.VideoPlayer::get_audioOutputMode()
extern void VideoPlayer_get_audioOutputMode_m9EF734E8234CE0E7C5F50370FC03703E4C57F09D (void);
// 0x00000050 System.Void UnityEngine.Video.VideoPlayer::set_audioOutputMode(UnityEngine.Video.VideoAudioOutputMode)
extern void VideoPlayer_set_audioOutputMode_m95A8791FE87A490EAEF1959E7597E0CC1659227F (void);
// 0x00000051 System.Boolean UnityEngine.Video.VideoPlayer::get_canSetDirectAudioVolume()
extern void VideoPlayer_get_canSetDirectAudioVolume_mAB106A790AF2874B4EF6A606D134A5A9A9A516F8 (void);
// 0x00000052 System.Single UnityEngine.Video.VideoPlayer::GetDirectAudioVolume(System.UInt16)
extern void VideoPlayer_GetDirectAudioVolume_mF496A8462B39AE0886690785BFA622079D14F60A (void);
// 0x00000053 System.Void UnityEngine.Video.VideoPlayer::SetDirectAudioVolume(System.UInt16,System.Single)
extern void VideoPlayer_SetDirectAudioVolume_m83860F5807607EC178AE9A8CEF354414731869C4 (void);
// 0x00000054 System.Boolean UnityEngine.Video.VideoPlayer::GetDirectAudioMute(System.UInt16)
extern void VideoPlayer_GetDirectAudioMute_m5E7BBBCF8F4C46DEDF3EF3D19CD10B5B1D6F8E94 (void);
// 0x00000055 System.Void UnityEngine.Video.VideoPlayer::SetDirectAudioMute(System.UInt16,System.Boolean)
extern void VideoPlayer_SetDirectAudioMute_m1EC2D7F907937D08DCBA9CB058B91E78150A4477 (void);
// 0x00000056 UnityEngine.AudioSource UnityEngine.Video.VideoPlayer::GetTargetAudioSource(System.UInt16)
extern void VideoPlayer_GetTargetAudioSource_m3D0D953E21725ADEDB001F9C63A5281608D18CCE (void);
// 0x00000057 System.Void UnityEngine.Video.VideoPlayer::SetTargetAudioSource(System.UInt16,UnityEngine.AudioSource)
extern void VideoPlayer_SetTargetAudioSource_m98880F0925C0E7DDFCB946563A23613CFFC494FE (void);
// 0x00000058 System.Void UnityEngine.Video.VideoPlayer::add_prepareCompleted(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_add_prepareCompleted_mA09867482AAB164B8AA6820FFE57E3F391CB8FE4 (void);
// 0x00000059 System.Void UnityEngine.Video.VideoPlayer::remove_prepareCompleted(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_remove_prepareCompleted_m6BBFD6B47A7627DA597319ADA50403B8C0F02186 (void);
// 0x0000005A System.Void UnityEngine.Video.VideoPlayer::add_loopPointReached(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_add_loopPointReached_m67619E2B83DC46D8DA3DF0CFAC24399FA0A2D932 (void);
// 0x0000005B System.Void UnityEngine.Video.VideoPlayer::remove_loopPointReached(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_remove_loopPointReached_m1E4A0D894AA6C597E9E7EC9F389069BF6E9E2F0B (void);
// 0x0000005C System.Void UnityEngine.Video.VideoPlayer::add_started(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_add_started_m97B60BBA14F176FAC93E2713AB9B0B1D1E094207 (void);
// 0x0000005D System.Void UnityEngine.Video.VideoPlayer::remove_started(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_remove_started_m7F677227C0FE32FBFCC830D2D0507A455210FB1F (void);
// 0x0000005E System.Void UnityEngine.Video.VideoPlayer::add_frameDropped(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_add_frameDropped_m3814AD28574DF3BE40B7FCBC2D6CB7F8760E6A06 (void);
// 0x0000005F System.Void UnityEngine.Video.VideoPlayer::remove_frameDropped(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_remove_frameDropped_m3BB8D079C760AEBB05E16970B9EA0B2054AA26C6 (void);
// 0x00000060 System.Void UnityEngine.Video.VideoPlayer::add_errorReceived(UnityEngine.Video.VideoPlayer/ErrorEventHandler)
extern void VideoPlayer_add_errorReceived_mABB9E416B6E5F505A4F408CB041BE9FE1597FC5D (void);
// 0x00000061 System.Void UnityEngine.Video.VideoPlayer::remove_errorReceived(UnityEngine.Video.VideoPlayer/ErrorEventHandler)
extern void VideoPlayer_remove_errorReceived_m94B7BA2DE9A008839683C9A3311A01A36CEDAB88 (void);
// 0x00000062 System.Void UnityEngine.Video.VideoPlayer::add_seekCompleted(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_add_seekCompleted_m6884EFA42474E5E146989B4DB9E624EF10FE270C (void);
// 0x00000063 System.Void UnityEngine.Video.VideoPlayer::remove_seekCompleted(UnityEngine.Video.VideoPlayer/EventHandler)
extern void VideoPlayer_remove_seekCompleted_mF6B1BD552840E9ACA57E5FA3275F564EF83D5F95 (void);
// 0x00000064 System.Void UnityEngine.Video.VideoPlayer::add_clockResyncOccurred(UnityEngine.Video.VideoPlayer/TimeEventHandler)
extern void VideoPlayer_add_clockResyncOccurred_m545CEFD4E8959807BB649197E8436B62B4A17B42 (void);
// 0x00000065 System.Void UnityEngine.Video.VideoPlayer::remove_clockResyncOccurred(UnityEngine.Video.VideoPlayer/TimeEventHandler)
extern void VideoPlayer_remove_clockResyncOccurred_mFDC8A5376EB47A3CF21536632FF62264E0F8C465 (void);
// 0x00000066 System.Boolean UnityEngine.Video.VideoPlayer::get_sendFrameReadyEvents()
extern void VideoPlayer_get_sendFrameReadyEvents_m89D1724E521FDBC0B76953D5351EB0782B7BF6B5 (void);
// 0x00000067 System.Void UnityEngine.Video.VideoPlayer::set_sendFrameReadyEvents(System.Boolean)
extern void VideoPlayer_set_sendFrameReadyEvents_m9ABC64BBC1AD7D7E457C92BE975997F571637BAD (void);
// 0x00000068 System.Void UnityEngine.Video.VideoPlayer::add_frameReady(UnityEngine.Video.VideoPlayer/FrameReadyEventHandler)
extern void VideoPlayer_add_frameReady_mB7641568597E988CE68BC3E3BEF068F558DBCF6F (void);
// 0x00000069 System.Void UnityEngine.Video.VideoPlayer::remove_frameReady(UnityEngine.Video.VideoPlayer/FrameReadyEventHandler)
extern void VideoPlayer_remove_frameReady_m0232BE22F0F4E3FA43A9D30C5176892DB1DC30D8 (void);
// 0x0000006A System.Void UnityEngine.Video.VideoPlayer::InvokePrepareCompletedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokePrepareCompletedCallback_Internal_m4CFD7054C97BE95CAC055CF18466E90D060E9B53 (void);
// 0x0000006B System.Void UnityEngine.Video.VideoPlayer::InvokeFrameReadyCallback_Internal(UnityEngine.Video.VideoPlayer,System.Int64)
extern void VideoPlayer_InvokeFrameReadyCallback_Internal_m4F62FC3695CFC72045E3C90503D541AE6E023EE8 (void);
// 0x0000006C System.Void UnityEngine.Video.VideoPlayer::InvokeLoopPointReachedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeLoopPointReachedCallback_Internal_m1A07EB382FD3CE673FF171A11047BEC54A6BB9AB (void);
// 0x0000006D System.Void UnityEngine.Video.VideoPlayer::InvokeStartedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeStartedCallback_Internal_m28D5BF153FC37959C225292BE859135FF778C8BB (void);
// 0x0000006E System.Void UnityEngine.Video.VideoPlayer::InvokeFrameDroppedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeFrameDroppedCallback_Internal_m669EAEF06893B53351EE81C3858BD62661228C58 (void);
// 0x0000006F System.Void UnityEngine.Video.VideoPlayer::InvokeErrorReceivedCallback_Internal(UnityEngine.Video.VideoPlayer,System.String)
extern void VideoPlayer_InvokeErrorReceivedCallback_Internal_mF7849030756F4A2B5226437C165ECDFE6E52E385 (void);
// 0x00000070 System.Void UnityEngine.Video.VideoPlayer::InvokeSeekCompletedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeSeekCompletedCallback_Internal_mFFB686F5BF044F61495CCF2EFB3517857F98C078 (void);
// 0x00000071 System.Void UnityEngine.Video.VideoPlayer::InvokeClockResyncOccurredCallback_Internal(UnityEngine.Video.VideoPlayer,System.Double)
extern void VideoPlayer_InvokeClockResyncOccurredCallback_Internal_m21D2B32DDE5ED48F6D6F2ECA9B7A0A2724AF9D6F (void);
// 0x00000072 System.Void UnityEngine.Video.VideoPlayer::.ctor()
extern void VideoPlayer__ctor_m73BC1386A16CF92E51355F0D06DC22FC0EC7B29F (void);
// 0x00000073 System.Void UnityEngine.Video.VideoPlayer/EventHandler::.ctor(System.Object,System.IntPtr)
extern void EventHandler__ctor_mA31DCA369A8B7C473F6CE19F6B53D6F3FAF7D6A7 (void);
// 0x00000074 System.Void UnityEngine.Video.VideoPlayer/EventHandler::Invoke(UnityEngine.Video.VideoPlayer)
extern void EventHandler_Invoke_m137A7D976F198147AD939AEF51E157107A3B1FBC (void);
// 0x00000075 System.IAsyncResult UnityEngine.Video.VideoPlayer/EventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.AsyncCallback,System.Object)
extern void EventHandler_BeginInvoke_mCA1B5193B15F3D56BCB40A8DDBA703724040F348 (void);
// 0x00000076 System.Void UnityEngine.Video.VideoPlayer/EventHandler::EndInvoke(System.IAsyncResult)
extern void EventHandler_EndInvoke_m95A975D9455F92F836E7E19BDB85538B1EBF4067 (void);
// 0x00000077 System.Void UnityEngine.Video.VideoPlayer/ErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern void ErrorEventHandler__ctor_m9E9B3A7A439858703258976491E29057CB17F534 (void);
// 0x00000078 System.Void UnityEngine.Video.VideoPlayer/ErrorEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.String)
extern void ErrorEventHandler_Invoke_m0A812811B673439792D99C125EE4FFE5E358EF6C (void);
// 0x00000079 System.IAsyncResult UnityEngine.Video.VideoPlayer/ErrorEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.String,System.AsyncCallback,System.Object)
extern void ErrorEventHandler_BeginInvoke_mD4C6F60629C221D7702E40D460818B90032FBAA7 (void);
// 0x0000007A System.Void UnityEngine.Video.VideoPlayer/ErrorEventHandler::EndInvoke(System.IAsyncResult)
extern void ErrorEventHandler_EndInvoke_m7ABF3F8E15D2EF4AE6961324B66208A7FD127295 (void);
// 0x0000007B System.Void UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::.ctor(System.Object,System.IntPtr)
extern void FrameReadyEventHandler__ctor_m7DFDBF9203E8F9FC1093E1655C5E2695623D7E3D (void);
// 0x0000007C System.Void UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.Int64)
extern void FrameReadyEventHandler_Invoke_m88D0AC1BED08D66B6CFA18DA23C58D10795DDA70 (void);
// 0x0000007D System.IAsyncResult UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.Int64,System.AsyncCallback,System.Object)
extern void FrameReadyEventHandler_BeginInvoke_m5DA99DFE61C78E158FF79535447F7649FC09E5F1 (void);
// 0x0000007E System.Void UnityEngine.Video.VideoPlayer/FrameReadyEventHandler::EndInvoke(System.IAsyncResult)
extern void FrameReadyEventHandler_EndInvoke_mC54DCEDB2F8CB30CBC6CD1590E2C08150E3E0CFF (void);
// 0x0000007F System.Void UnityEngine.Video.VideoPlayer/TimeEventHandler::.ctor(System.Object,System.IntPtr)
extern void TimeEventHandler__ctor_mF41715E69B793B1C7DCA3A619CFB05097466523F (void);
// 0x00000080 System.Void UnityEngine.Video.VideoPlayer/TimeEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.Double)
extern void TimeEventHandler_Invoke_m278E51F2838EC435606BE1CB3AD0E881505FAE10 (void);
// 0x00000081 System.IAsyncResult UnityEngine.Video.VideoPlayer/TimeEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.Double,System.AsyncCallback,System.Object)
extern void TimeEventHandler_BeginInvoke_m184CF1FDB1D643F00FE3C60982ED62EC4888F21D (void);
// 0x00000082 System.Void UnityEngine.Video.VideoPlayer/TimeEventHandler::EndInvoke(System.IAsyncResult)
extern void TimeEventHandler_EndInvoke_m2759449ABAAE5711D31E414D5906AB042725F7AA (void);
static Il2CppMethodPointer s_methodPointers[130] = 
{
	VideoClipPlayable_GetHandle_m5B2EFD8CFE93DB4D2EE29A600E598233471DF242,
	VideoClipPlayable_Equals_m2F07E6EBA96043274F2384F18E43B66035058BC2,
	VideoClip__ctor_m9BB7ECD808FCBD3DA63594134BEEC12B17852117,
	VideoPlayer_get_source_m0B59B963847A0C4F206697FB61C618D9621949E4,
	VideoPlayer_set_source_mE27652C749468B4A44FE3557BA809C503C849ABE,
	VideoPlayer_get_url_mC8F4EC64F3938721095E741897A83F4A35741203,
	VideoPlayer_set_url_m076BC425E754574E8DFCDFA7AC2A37A3EFD5AF21,
	VideoPlayer_get_clip_mA2C3AC016BB7B09855C56A11843AE60EB4D36B76,
	VideoPlayer_set_clip_mD8D5AA8642010DFACD0B88F38FAF7CBBB8DD4E64,
	VideoPlayer_get_renderMode_mFBFD12B8E2C366663AA5F55A6087A4BB137D447E,
	VideoPlayer_set_renderMode_m9DC3CFC4B99F66FA6E6116379D8C7F64D798CB63,
	VideoPlayer_get_targetCamera_mFDF3BD6E156461CC2C6BB44878886EDE4C995687,
	VideoPlayer_set_targetCamera_mAD892B74820C093D33A8808C6CA4BFF7935773C4,
	VideoPlayer_get_targetTexture_mDFF8E4B60BDFE26F5535E5D15CAAA8546DB939E3,
	VideoPlayer_set_targetTexture_m4088B6058A001D48C911DC71023AC723BD3C8252,
	VideoPlayer_get_targetMaterialRenderer_m7006B5CB698247A457EAE8325709C0A09883CFEC,
	VideoPlayer_set_targetMaterialRenderer_mA5BDE908C845D6023DEEA3013C78E357020D106D,
	VideoPlayer_get_targetMaterialProperty_mB8798BF77416FFAA2CA67BB0B2664C0DD6E072FB,
	VideoPlayer_set_targetMaterialProperty_m8BF9B84E27C0C3090FA0231FC2C449B2BED9435F,
	VideoPlayer_get_aspectRatio_mF5A5478D9C1E0FDE8A530CEE17B043A4C57C5E07,
	VideoPlayer_set_aspectRatio_mEA5D88F04CF46AE08D472146D0A0274DA5E97065,
	VideoPlayer_get_targetCameraAlpha_mD196E8A77E2D28FF9F90842D18320843F52D6184,
	VideoPlayer_set_targetCameraAlpha_m3326BD7558151BF416178CCE7C26ACC0C55D7660,
	VideoPlayer_get_targetCamera3DLayout_m05C57CC8E9F25B637711613D064B357633F40629,
	VideoPlayer_set_targetCamera3DLayout_m9D29143E9BCEE303E21287DD4B254A042CAC076A,
	VideoPlayer_get_texture_m7FEBFE07CDB830FCAD4B5B2D7EF63D12E8C034CC,
	VideoPlayer_Prepare_m32AB43745A92A6762D570E60975AD69DB8FFF566,
	VideoPlayer_get_isPrepared_mF0DC157BD0B5E8FF26A27EA7ABE4BEDCBE963694,
	VideoPlayer_get_waitForFirstFrame_m0A34CAAA86DC05A4EC680FB127D6B9B015A0E67E,
	VideoPlayer_set_waitForFirstFrame_m8E11586971ADCEBC6535C065ABB23BC5E4C49875,
	VideoPlayer_get_playOnAwake_m3F510732AC70AF08A33907810B3AD0D3672EDB68,
	VideoPlayer_set_playOnAwake_m3109BDD49A2981518F062FACBF0ECF1DB8BFE207,
	VideoPlayer_Play_m2BCD775F42A36AC291C7B32D9E4D934EF0B91257,
	VideoPlayer_Pause_m62EE5660CFA287D78BB3FE815CA5649242509B93,
	VideoPlayer_Stop_mBEDCB786A42A95603F9D8B7763DEC3BD97565852,
	VideoPlayer_get_isPlaying_mC7CFE17762C14F2AFB1D73500317B9D25A7395DF,
	VideoPlayer_get_isPaused_m84665EE1EB3F31A1C30D9D3E17515802E446E849,
	VideoPlayer_get_canSetTime_m875362EEC923AAAAA882CC775CC9816AE974603C,
	VideoPlayer_get_time_mF842FC1E9A1FD2333C7C9D13338D3B178665564A,
	VideoPlayer_set_time_m474F47BCE704FFCB3C6FFAAE6414A9ED89678514,
	VideoPlayer_get_frame_mB7F5972A74C2D4039855454F552AD08BF12F30A0,
	VideoPlayer_set_frame_m41D0EAE134C1D62C1766817C652419495C17E4C4,
	VideoPlayer_get_clockTime_mF8CA7C444B3CA3DBC479EF8FDABFD484C9A9BD41,
	VideoPlayer_get_canStep_mCCE3965DAF4B2805F0451BAACA41864F8309872D,
	VideoPlayer_StepForward_m8B9A90273A3FA36EDE431773EF97E15F9151A899,
	VideoPlayer_get_canSetPlaybackSpeed_m06A2D6459930861D61847ED28A29DAD6C668A788,
	VideoPlayer_get_playbackSpeed_m6A0A48C222262FADDA28565AD3D36C45B92D31A6,
	VideoPlayer_set_playbackSpeed_m76CDBC2141AE66474AFCB707A6E0EE77454628A6,
	VideoPlayer_get_isLooping_m096B276AD270A2C03C517262D4DA100DEC1A2014,
	VideoPlayer_set_isLooping_m0FA87AE5A8F06A545349EAA8F0C64158E29700C7,
	VideoPlayer_get_canSetTimeSource_m7B87AE87F7F18588A51241D68F599C0FDD3B6853,
	VideoPlayer_get_timeSource_m57C09418AD61A6C849ABBBDF7F1469F66EEA2656,
	VideoPlayer_set_timeSource_mBCED110DD2B0822D2F719CEBD1D103E9E7D3F2EB,
	VideoPlayer_get_timeReference_m34BC38D0539B1EAB554230892E5173E923D4257C,
	VideoPlayer_set_timeReference_m7EBE45F2FCF7326831EB92B2C125A6D7A4EBFCB4,
	VideoPlayer_get_externalReferenceTime_mCFCD9B4ED9C4DA648BF0F3C454208BFB66C72744,
	VideoPlayer_set_externalReferenceTime_mB900C62E1E95E5200B3065DF8FD5C8863128631F,
	VideoPlayer_get_canSetSkipOnDrop_mA3D1E789DDCF90416DDD15D95DD32B73135D8210,
	VideoPlayer_get_skipOnDrop_m2690695BAFD5C39A6A2E65153609D8257D820362,
	VideoPlayer_set_skipOnDrop_mBAA06EE0F8E6E41AA3CC43F0791E76BF2C14C0C8,
	VideoPlayer_get_frameCount_m89C61BE7B88F1A573FA42C2A7564230A2234F709,
	VideoPlayer_get_frameRate_mEC5D740D0A22EBB929373F80343AB4675D6F05DE,
	VideoPlayer_get_length_m593F8FA891C7B1D76BFAF6F1DFFAE2BB85FEDDC3,
	VideoPlayer_get_width_mA6C581E0855C11ED7557394B6E40D96A1EC51218,
	VideoPlayer_get_height_mF23A572FDF45854EE609FCFBD8655B137ECC1EC7,
	VideoPlayer_get_pixelAspectRatioNumerator_mF75E3B2FED1EFB5351417D711BF8D9D3037A1D26,
	VideoPlayer_get_pixelAspectRatioDenominator_mB80E2E7F6DB301F45A4B180CB54F3DAEE8D76DD2,
	VideoPlayer_get_audioTrackCount_m9B51B6DCE2D782A177500AC4C5DC645813FE7C44,
	VideoPlayer_GetAudioLanguageCode_m49BB753D26C2B5B091579D5515C2DC6AF5D07050,
	VideoPlayer_GetAudioChannelCount_m0C900E5923939CF03D8DA4F0843E8C5C31DB140C,
	VideoPlayer_GetAudioSampleRate_m5EB7367A99A3F48D86F4CE96B9241D2F6A865C30,
	VideoPlayer_get_controlledAudioTrackMaxCount_m5CD07FAE01ADA002F88F9E7B3432993AB52DAAF1,
	VideoPlayer_get_controlledAudioTrackCount_mF9B0DA53C041F8B8B3809860C3CFA11C9FF6D188,
	VideoPlayer_set_controlledAudioTrackCount_mE67D147B78D4DB81593E6E29631D475D98E7DA3B,
	VideoPlayer_GetControlledAudioTrackCount_m255D8594D8339E4175F9084E4D008C8D3F071AA7,
	VideoPlayer_SetControlledAudioTrackCount_m4E37B42737D9CA58DD795F30DC42FF48CB0E1069,
	VideoPlayer_EnableAudioTrack_m30901BF68B20BB8397E246FDDE699E5D513AAA5B,
	VideoPlayer_IsAudioTrackEnabled_mD2D5ED119462509DA30BF826B116A6BBAA7632DE,
	VideoPlayer_get_audioOutputMode_m9EF734E8234CE0E7C5F50370FC03703E4C57F09D,
	VideoPlayer_set_audioOutputMode_m95A8791FE87A490EAEF1959E7597E0CC1659227F,
	VideoPlayer_get_canSetDirectAudioVolume_mAB106A790AF2874B4EF6A606D134A5A9A9A516F8,
	VideoPlayer_GetDirectAudioVolume_mF496A8462B39AE0886690785BFA622079D14F60A,
	VideoPlayer_SetDirectAudioVolume_m83860F5807607EC178AE9A8CEF354414731869C4,
	VideoPlayer_GetDirectAudioMute_m5E7BBBCF8F4C46DEDF3EF3D19CD10B5B1D6F8E94,
	VideoPlayer_SetDirectAudioMute_m1EC2D7F907937D08DCBA9CB058B91E78150A4477,
	VideoPlayer_GetTargetAudioSource_m3D0D953E21725ADEDB001F9C63A5281608D18CCE,
	VideoPlayer_SetTargetAudioSource_m98880F0925C0E7DDFCB946563A23613CFFC494FE,
	VideoPlayer_add_prepareCompleted_mA09867482AAB164B8AA6820FFE57E3F391CB8FE4,
	VideoPlayer_remove_prepareCompleted_m6BBFD6B47A7627DA597319ADA50403B8C0F02186,
	VideoPlayer_add_loopPointReached_m67619E2B83DC46D8DA3DF0CFAC24399FA0A2D932,
	VideoPlayer_remove_loopPointReached_m1E4A0D894AA6C597E9E7EC9F389069BF6E9E2F0B,
	VideoPlayer_add_started_m97B60BBA14F176FAC93E2713AB9B0B1D1E094207,
	VideoPlayer_remove_started_m7F677227C0FE32FBFCC830D2D0507A455210FB1F,
	VideoPlayer_add_frameDropped_m3814AD28574DF3BE40B7FCBC2D6CB7F8760E6A06,
	VideoPlayer_remove_frameDropped_m3BB8D079C760AEBB05E16970B9EA0B2054AA26C6,
	VideoPlayer_add_errorReceived_mABB9E416B6E5F505A4F408CB041BE9FE1597FC5D,
	VideoPlayer_remove_errorReceived_m94B7BA2DE9A008839683C9A3311A01A36CEDAB88,
	VideoPlayer_add_seekCompleted_m6884EFA42474E5E146989B4DB9E624EF10FE270C,
	VideoPlayer_remove_seekCompleted_mF6B1BD552840E9ACA57E5FA3275F564EF83D5F95,
	VideoPlayer_add_clockResyncOccurred_m545CEFD4E8959807BB649197E8436B62B4A17B42,
	VideoPlayer_remove_clockResyncOccurred_mFDC8A5376EB47A3CF21536632FF62264E0F8C465,
	VideoPlayer_get_sendFrameReadyEvents_m89D1724E521FDBC0B76953D5351EB0782B7BF6B5,
	VideoPlayer_set_sendFrameReadyEvents_m9ABC64BBC1AD7D7E457C92BE975997F571637BAD,
	VideoPlayer_add_frameReady_mB7641568597E988CE68BC3E3BEF068F558DBCF6F,
	VideoPlayer_remove_frameReady_m0232BE22F0F4E3FA43A9D30C5176892DB1DC30D8,
	VideoPlayer_InvokePrepareCompletedCallback_Internal_m4CFD7054C97BE95CAC055CF18466E90D060E9B53,
	VideoPlayer_InvokeFrameReadyCallback_Internal_m4F62FC3695CFC72045E3C90503D541AE6E023EE8,
	VideoPlayer_InvokeLoopPointReachedCallback_Internal_m1A07EB382FD3CE673FF171A11047BEC54A6BB9AB,
	VideoPlayer_InvokeStartedCallback_Internal_m28D5BF153FC37959C225292BE859135FF778C8BB,
	VideoPlayer_InvokeFrameDroppedCallback_Internal_m669EAEF06893B53351EE81C3858BD62661228C58,
	VideoPlayer_InvokeErrorReceivedCallback_Internal_mF7849030756F4A2B5226437C165ECDFE6E52E385,
	VideoPlayer_InvokeSeekCompletedCallback_Internal_mFFB686F5BF044F61495CCF2EFB3517857F98C078,
	VideoPlayer_InvokeClockResyncOccurredCallback_Internal_m21D2B32DDE5ED48F6D6F2ECA9B7A0A2724AF9D6F,
	VideoPlayer__ctor_m73BC1386A16CF92E51355F0D06DC22FC0EC7B29F,
	EventHandler__ctor_mA31DCA369A8B7C473F6CE19F6B53D6F3FAF7D6A7,
	EventHandler_Invoke_m137A7D976F198147AD939AEF51E157107A3B1FBC,
	EventHandler_BeginInvoke_mCA1B5193B15F3D56BCB40A8DDBA703724040F348,
	EventHandler_EndInvoke_m95A975D9455F92F836E7E19BDB85538B1EBF4067,
	ErrorEventHandler__ctor_m9E9B3A7A439858703258976491E29057CB17F534,
	ErrorEventHandler_Invoke_m0A812811B673439792D99C125EE4FFE5E358EF6C,
	ErrorEventHandler_BeginInvoke_mD4C6F60629C221D7702E40D460818B90032FBAA7,
	ErrorEventHandler_EndInvoke_m7ABF3F8E15D2EF4AE6961324B66208A7FD127295,
	FrameReadyEventHandler__ctor_m7DFDBF9203E8F9FC1093E1655C5E2695623D7E3D,
	FrameReadyEventHandler_Invoke_m88D0AC1BED08D66B6CFA18DA23C58D10795DDA70,
	FrameReadyEventHandler_BeginInvoke_m5DA99DFE61C78E158FF79535447F7649FC09E5F1,
	FrameReadyEventHandler_EndInvoke_mC54DCEDB2F8CB30CBC6CD1590E2C08150E3E0CFF,
	TimeEventHandler__ctor_mF41715E69B793B1C7DCA3A619CFB05097466523F,
	TimeEventHandler_Invoke_m278E51F2838EC435606BE1CB3AD0E881505FAE10,
	TimeEventHandler_BeginInvoke_m184CF1FDB1D643F00FE3C60982ED62EC4888F21D,
	TimeEventHandler_EndInvoke_m2759449ABAAE5711D31E414D5906AB042725F7AA,
};
extern void VideoClipPlayable_GetHandle_m5B2EFD8CFE93DB4D2EE29A600E598233471DF242_AdjustorThunk (void);
extern void VideoClipPlayable_Equals_m2F07E6EBA96043274F2384F18E43B66035058BC2_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x06000001, VideoClipPlayable_GetHandle_m5B2EFD8CFE93DB4D2EE29A600E598233471DF242_AdjustorThunk },
	{ 0x06000002, VideoClipPlayable_Equals_m2F07E6EBA96043274F2384F18E43B66035058BC2_AdjustorThunk },
};
static const int32_t s_InvokerIndices[130] = 
{
	1740,
	2030,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	727,
	335,
	10,
	32,
	14,
	23,
	89,
	89,
	31,
	89,
	31,
	23,
	23,
	23,
	89,
	89,
	89,
	460,
	336,
	172,
	200,
	460,
	89,
	23,
	89,
	727,
	335,
	89,
	31,
	89,
	10,
	32,
	10,
	32,
	460,
	336,
	89,
	89,
	31,
	172,
	727,
	460,
	10,
	10,
	10,
	10,
	238,
	671,
	661,
	232,
	764,
	238,
	613,
	238,
	613,
	1304,
	231,
	10,
	32,
	89,
	2031,
	2032,
	231,
	1304,
	671,
	2033,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	89,
	31,
	26,
	26,
	154,
	152,
	154,
	154,
	154,
	137,
	154,
	2034,
	23,
	124,
	26,
	205,
	26,
	124,
	27,
	125,
	26,
	124,
	171,
	2035,
	26,
	124,
	1355,
	2036,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_VideoModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VideoModuleCodeGenModule = 
{
	"UnityEngine.VideoModule.dll",
	130,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
