﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <id>j__TPar <>f__AnonymousType0`4::get_id()
// 0x00000002 <seq>j__TPar <>f__AnonymousType0`4::get_seq()
// 0x00000003 <name>j__TPar <>f__AnonymousType0`4::get_name()
// 0x00000004 <data>j__TPar <>f__AnonymousType0`4::get_data()
// 0x00000005 System.Void <>f__AnonymousType0`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x00000006 System.Boolean <>f__AnonymousType0`4::Equals(System.Object)
// 0x00000007 System.Int32 <>f__AnonymousType0`4::GetHashCode()
// 0x00000008 System.String <>f__AnonymousType0`4::ToString()
// 0x00000009 System.Void GameManager::Start()
extern void GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E (void);
// 0x0000000A System.Void GameManager::Update()
extern void GameManager_Update_m07DC32583BF09EB71183725B7B95FA7B4716988A (void);
// 0x0000000B System.Void GameManager::.ctor()
extern void GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693 (void);
// 0x0000000C System.Void Rotate::Start()
extern void Rotate_Start_m0C36379106B8F7DD309CAC5E6D720D811129FA07 (void);
// 0x0000000D System.Void Rotate::Update()
extern void Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330 (void);
// 0x0000000E System.Void Rotate::SetRotationSpeed(System.String)
extern void Rotate_SetRotationSpeed_mD1F6C05B18C3D792AB9AF055FDBCA8128873F461 (void);
// 0x0000000F System.Void Rotate::.ctor()
extern void Rotate__ctor_mC70374AA84BC2CA04CA039D1B56F5AF1B95D42CF (void);
// 0x00000010 System.Void SceneLoader::Start()
extern void SceneLoader_Start_m2553B08838ABEA27EA661BC8B9CF7D8A14353D7B (void);
// 0x00000011 System.Void SceneLoader::Update()
extern void SceneLoader_Update_mD6559C8F6F858FA7413175425CFE0658DAF89707 (void);
// 0x00000012 System.Void SceneLoader::LoadScene(System.Int32)
extern void SceneLoader_LoadScene_mC2BE257125A724B3F3213988D312F0767A97FFD5 (void);
// 0x00000013 System.Void SceneLoader::MessengerFlutter()
extern void SceneLoader_MessengerFlutter_mA6DC0EA56132DB935C38E4B00C0C4B9CEA99F3CC (void);
// 0x00000014 System.Void SceneLoader::SwitchNative()
extern void SceneLoader_SwitchNative_m347537365D6F4F48B69DBC3C3E224E0BA244CE11 (void);
// 0x00000015 System.Void SceneLoader::UnloadNative()
extern void SceneLoader_UnloadNative_m115CF3FA476F18B72E4777201B1E2EA039509345 (void);
// 0x00000016 System.Void SceneLoader::QuitNative()
extern void SceneLoader_QuitNative_mE0C578D9505964BF7D24448CFDBB46C41BC6264D (void);
// 0x00000017 System.Void SceneLoader::.ctor()
extern void SceneLoader__ctor_mFE1E7D8E1BB135653D2C6FDE4D40F4FC38115009 (void);
// 0x00000018 System.Void NativeAPI::OnUnityMessage(System.String)
extern void NativeAPI_OnUnityMessage_mEB20277B0A0AF2ECDFB0DA7E49113BEB51493550 (void);
// 0x00000019 System.Void NativeAPI::OnUnitySceneLoaded(System.String,System.Int32,System.Boolean,System.Boolean)
extern void NativeAPI_OnUnitySceneLoaded_mB0837ED51EE99E965D15D2BA23B6287C0793D1DF (void);
// 0x0000001A System.Void NativeAPI::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NativeAPI_OnSceneLoaded_m5EC95403676F639015AE4A974CB0350752E92287 (void);
// 0x0000001B System.Void NativeAPI::SendMessageToFlutter(System.String)
extern void NativeAPI_SendMessageToFlutter_m31EBCD5E058F0179878B70B9F67D3C4CCF51B07D (void);
// 0x0000001C System.Void NativeAPI::ShowHostMainWindow()
extern void NativeAPI_ShowHostMainWindow_mB9A7879F05EC5BF0B39BED1AD8EA0B20B5887749 (void);
// 0x0000001D System.Void NativeAPI::UnloadMainWindow()
extern void NativeAPI_UnloadMainWindow_mEAC1F4610D0B6720F47ED79CB32964585462DAEF (void);
// 0x0000001E System.Void NativeAPI::QuitUnityWindow()
extern void NativeAPI_QuitUnityWindow_m11F2D4AC622EF6FEB1D1B0C7BCD73CC3C444D739 (void);
// 0x0000001F System.Void NativeAPI::.ctor()
extern void NativeAPI__ctor_m8A9AEF251DEAC1AD4CAF201F962E5DADBA8F47C5 (void);
// 0x00000020 T SingletonMonoBehaviour`1::get_Instance()
// 0x00000021 T SingletonMonoBehaviour`1::CreateSingleton()
// 0x00000022 System.Void SingletonMonoBehaviour`1::.ctor()
// 0x00000023 System.Void SingletonMonoBehaviour`1::.cctor()
// 0x00000024 MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE (void);
// 0x00000025 T MessageHandler::getData()
// 0x00000026 System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_mFD041017CFD2E7BE9B9CDB629921C6D155BF2D11 (void);
// 0x00000027 System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E (void);
// 0x00000028 System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45 (void);
// 0x00000029 System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89 (void);
// 0x0000002A System.Void UnityMessageManager::add_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344 (void);
// 0x0000002B System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4 (void);
// 0x0000002C System.Void UnityMessageManager::add_OnFlutterMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_add_OnFlutterMessage_m702ECE2711E59769765096D023C48B7C40F493B3 (void);
// 0x0000002D System.Void UnityMessageManager::remove_OnFlutterMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnFlutterMessage_m6EE57FCB3B88653B284550F2168F322B043F3366 (void);
// 0x0000002E System.Void UnityMessageManager::Start()
extern void UnityMessageManager_Start_m062DACF27948F5A0161368FE841569ACA6E92CC7 (void);
// 0x0000002F System.Void UnityMessageManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void UnityMessageManager_OnSceneLoaded_mCF1EB0D8D0511F19F1BD8403808AC73B7E58E594 (void);
// 0x00000030 System.Void UnityMessageManager::ShowHostMainWindow()
extern void UnityMessageManager_ShowHostMainWindow_m3A9A30AF899337888D541FA6021BD4BDA21F482A (void);
// 0x00000031 System.Void UnityMessageManager::UnloadMainWindow()
extern void UnityMessageManager_UnloadMainWindow_m64E4F629EEF9EC8DD64C36E0FBEAE1B091547CE0 (void);
// 0x00000032 System.Void UnityMessageManager::QuitUnityWindow()
extern void UnityMessageManager_QuitUnityWindow_m5B3DBA2E810AF58BD8C4489E9BB2B2A52A6BC022 (void);
// 0x00000033 System.Void UnityMessageManager::SendMessageToFlutter(System.String)
extern void UnityMessageManager_SendMessageToFlutter_m0ADC2E517A85054AE5BA1D3328255060B20A1570 (void);
// 0x00000034 System.Void UnityMessageManager::SendMessageToFlutter(UnityMessage)
extern void UnityMessageManager_SendMessageToFlutter_m390277F4683DC0C0F3437F5C69A4BD8FAD6DA70E (void);
// 0x00000035 System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_mD1704644A461EFD311809BB58F82D36898713485 (void);
// 0x00000036 System.Void UnityMessageManager::onFlutterMessage(System.String)
extern void UnityMessageManager_onFlutterMessage_m5BEF69E2E3423B8F8ACCB39BBF8286128B4C2B5A (void);
// 0x00000037 System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7 (void);
// 0x00000038 System.Void UnityMessageManager::.cctor()
extern void UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B (void);
// 0x00000039 System.Void ButtonControl::CloseMenu()
extern void ButtonControl_CloseMenu_mBB9A3DA4AF99C660C0A6D7DB1F093708F4354CC4 (void);
// 0x0000003A System.Void ButtonControl::OpenMenu()
extern void ButtonControl_OpenMenu_mC78ACCE33065096EBA2D021C3E85FA1E1EB0170E (void);
// 0x0000003B System.Void ButtonControl::GoToWebPage()
extern void ButtonControl_GoToWebPage_mA20CBD30CEBB4FAF6240A3B0230D0FE3134C5CC4 (void);
// 0x0000003C System.Void ButtonControl::CloseHelpPanel()
extern void ButtonControl_CloseHelpPanel_mE89EA04B86EE834870E925A4607155316C541B8D (void);
// 0x0000003D System.Void ButtonControl::OpenHelpPanel()
extern void ButtonControl_OpenHelpPanel_mF4D55701A3A1FBD8C05964E68A17E131CDC98037 (void);
// 0x0000003E System.Void ButtonControl::RemoveObject()
extern void ButtonControl_RemoveObject_mD376A17553D9293FD41E7C78A0D34106C3128A97 (void);
// 0x0000003F System.Void ButtonControl::GetARObjectName()
extern void ButtonControl_GetARObjectName_m6BF16A32D689DE43F9C6C63A972EAC723026EAFF (void);
// 0x00000040 System.Void ButtonControl::ChangeToScanScene()
extern void ButtonControl_ChangeToScanScene_mAB05F226EBE0B199D97619598777E845BC137238 (void);
// 0x00000041 System.Void ButtonControl::ChangeToVisualizeScene()
extern void ButtonControl_ChangeToVisualizeScene_mDA2CF2D364564EDCE4A8F0135AB91D489AC1FE55 (void);
// 0x00000042 System.Void ButtonControl::OpenFeatureMenu()
extern void ButtonControl_OpenFeatureMenu_m7D1E6BB13004B8C4490D0B23823A930B9182BC26 (void);
// 0x00000043 System.Void ButtonControl::CloseFeatureMenu()
extern void ButtonControl_CloseFeatureMenu_m51DEDCBD71A4926BD1461F90020F462F5EB319D7 (void);
// 0x00000044 System.Void ButtonControl::.ctor()
extern void ButtonControl__ctor_mD4415647A7E956D92750D2E75F6DB76707014075 (void);
// 0x00000045 System.Void ProductList::.ctor()
extern void ProductList__ctor_mB2765DD1BEF4E7167F95FA849194AB150BC6ED10 (void);
// 0x00000046 System.Void ProgressBar::Awake()
extern void ProgressBar_Awake_m66AE9E4239EAD84F71C2D09339ACF4239BB5FC82 (void);
// 0x00000047 System.Void ProgressBar::IncrementProgress(System.Single)
extern void ProgressBar_IncrementProgress_mCB2D724AB00BDED0053D300DC69B20E2BCDDBF3D (void);
// 0x00000048 System.Void ProgressBar::.ctor()
extern void ProgressBar__ctor_mD2838AA6298018734186C1DEFF927E917DEBF05D (void);
// 0x00000049 System.Void imageScanSpawn::Start()
extern void imageScanSpawn_Start_m81C763BD2B5DC5CC92F00AD71E8386BE41AB41B0 (void);
// 0x0000004A System.Void imageScanSpawn::Update()
extern void imageScanSpawn_Update_mEF4D4E6FA56570D8A5514450384A18AD3A2E01A7 (void);
// 0x0000004B System.Void imageScanSpawn::OnEnable()
extern void imageScanSpawn_OnEnable_m740A63518DB41DCE9E74DF55934A83AA5261543C (void);
// 0x0000004C System.Void imageScanSpawn::OnDisable()
extern void imageScanSpawn_OnDisable_m18E5468F7946CFF9A45C5E6047124DD19C33BDE7 (void);
// 0x0000004D System.Void imageScanSpawn::OnTrackedImageChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void imageScanSpawn_OnTrackedImageChanged_m106AA9A84CE67C1C63EDA206E94B12DD5F16DF23 (void);
// 0x0000004E System.Collections.IEnumerator imageScanSpawn::Get(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void imageScanSpawn_Get_mBC44E280DE5031F2C39E2DC734CD4DCB1C9A69D7 (void);
// 0x0000004F System.Void imageScanSpawn::UpdateTrackingGameObject(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void imageScanSpawn_UpdateTrackingGameObject_mBC3263936A57673DBB68F284B1BDEB7FCC6538B1 (void);
// 0x00000050 System.Void imageScanSpawn::UpdateLimitedGameObject(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void imageScanSpawn_UpdateLimitedGameObject_m919EDD2E65B57D445ABC0D8ED7540DD881604CBD (void);
// 0x00000051 System.Void imageScanSpawn::UpdateNoneGameObject(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void imageScanSpawn_UpdateNoneGameObject_mE4A12165C38C766DE1C75D66D7C48AE518C92E30 (void);
// 0x00000052 System.Void imageScanSpawn::DestroyGameObject(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void imageScanSpawn_DestroyGameObject_mB1C2229BE57949DBC343AAFDB30995F66050450C (void);
// 0x00000053 System.Void imageScanSpawn::DisableUI()
extern void imageScanSpawn_DisableUI_mFD5F956C13B53C957C7B03D964CD5F0ED24207A2 (void);
// 0x00000054 System.Void imageScanSpawn::EnableUI()
extern void imageScanSpawn_EnableUI_mF86517531D7751374189C3BCD24DCB78E508FA95 (void);
// 0x00000055 System.Void imageScanSpawn::RetryDownload()
extern void imageScanSpawn_RetryDownload_m1185FD24C92C113BC9FFD8ACFDD4745E9E4218BC (void);
// 0x00000056 System.Void imageScanSpawn::ErrorListener()
extern void imageScanSpawn_ErrorListener_m8403630699E696B3C38B23DC5872EF227449641A (void);
// 0x00000057 System.Void imageScanSpawn::.ctor()
extern void imageScanSpawn__ctor_mC77E6A9248BB312F756D13FFF63C2A50E75EBC8E (void);
// 0x00000058 System.Void taptoplace::Start()
extern void taptoplace_Start_m6C7B3A5B3A6F2298B741CCCA354DD87FCD7975C3 (void);
// 0x00000059 System.Void taptoplace::Update()
extern void taptoplace_Update_mC002185C31B8F7E00B094332F0CD68336DC27C5B (void);
// 0x0000005A System.Void taptoplace::CrosshairCalculation()
extern void taptoplace_CrosshairCalculation_m62D3DD9C8FB94D38322B613532104604E9A555C6 (void);
// 0x0000005B System.Void taptoplace::UpdatePlacementIndicator()
extern void taptoplace_UpdatePlacementIndicator_m69A41BACF68F532E921DE0B6AEF218C15A98C316 (void);
// 0x0000005C System.Void taptoplace::LookForPlaceChecker()
extern void taptoplace_LookForPlaceChecker_m09A3281D7ABC595DF20D0E1E06C7AA97549EB144 (void);
// 0x0000005D System.Void taptoplace::TapToPlace()
extern void taptoplace_TapToPlace_m933E53BD9507A349E51B7F8D91EBFCA30D66B952 (void);
// 0x0000005E System.Void taptoplace::SelectObject()
extern void taptoplace_SelectObject_mA6361F0B26D22144AEE6156A5FAB14776ED2D513 (void);
// 0x0000005F System.Void taptoplace::DeselectObject()
extern void taptoplace_DeselectObject_m07A868DED131B21FAA5A833F02F5E86707003A63 (void);
// 0x00000060 System.Void taptoplace::MoveObject()
extern void taptoplace_MoveObject_mC196A799BA0304772CE5CF10AF1D85E900AF96BE (void);
// 0x00000061 System.Boolean taptoplace::isPointerOverUI(UnityEngine.Touch)
extern void taptoplace_isPointerOverUI_mF4C8BD68817732F8D4C8276187187014311AE3D0 (void);
// 0x00000062 System.Collections.IEnumerator taptoplace::Get(System.String,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void taptoplace_Get_m4B6449F9BF0BDE863D0447148B6D531C64E432A8 (void);
// 0x00000063 System.Void taptoplace::ButtonChecker()
extern void taptoplace_ButtonChecker_m70A95C8040D46BAB8FC404AA754E243EABEFB41F (void);
// 0x00000064 System.Void taptoplace::OpenGallery()
extern void taptoplace_OpenGallery_m7EBE55EE6DC842B345F6CEF558F5E8AAFB3AFA9C (void);
// 0x00000065 System.Void taptoplace::TapAnywhereButton()
extern void taptoplace_TapAnywhereButton_m1F20F58C05C33399FB3F0D3BE4E5EB70A441EB0B (void);
// 0x00000066 System.Void taptoplace::ErrorListener()
extern void taptoplace_ErrorListener_m637B5014972EA8CA81AD600E754A7AD3DB2B8A1C (void);
// 0x00000067 System.Void taptoplace::TapToPlaceListener()
extern void taptoplace_TapToPlaceListener_mEDE7372929CA790EC2A5D456ECF06DF0672E0ED9 (void);
// 0x00000068 System.Void taptoplace::RetryDownload()
extern void taptoplace_RetryDownload_m5C29F4FB25A4DE4AE88971299363978E72089E06 (void);
// 0x00000069 System.Void taptoplace::SetAllPlanesActive(System.Boolean)
extern void taptoplace_SetAllPlanesActive_mF4CA01213F6550E90DD1585A6AC841E81DFF17D9 (void);
// 0x0000006A System.Void taptoplace::.ctor()
extern void taptoplace__ctor_mBC2B5681361A8D87CA7DCBA2ABBB9F72E9397B81 (void);
// 0x0000006B System.Void ThankYouCardController::Start()
extern void ThankYouCardController_Start_mAB40A113A0A3CF6C6730AB4106F905CD10C50A5B (void);
// 0x0000006C System.Void ThankYouCardController::Update()
extern void ThankYouCardController_Update_m312D079A55AC101E9B5D82647087C572F3EF4502 (void);
// 0x0000006D System.Void ThankYouCardController::exit_video()
extern void ThankYouCardController_exit_video_m6FABF747250AC287A6696CC95262EA6139558B92 (void);
// 0x0000006E System.Void ThankYouCardController::watch_video(System.String)
extern void ThankYouCardController_watch_video_mEA6DDD1186E526262C9803043A8964CF05EB3D33 (void);
// 0x0000006F System.Void ThankYouCardController::.ctor()
extern void ThankYouCardController__ctor_m2E3436D35F48687724299033DFF847741775C98D (void);
// 0x00000070 System.Void FlexibleGridLayout::CalculateLayoutInputVertical()
extern void FlexibleGridLayout_CalculateLayoutInputVertical_mFDB2A382522AC0143815D85B4C71A88043830EF0 (void);
// 0x00000071 System.Void FlexibleGridLayout::SetLayoutHorizontal()
extern void FlexibleGridLayout_SetLayoutHorizontal_m62E00DC5A73CB7738E0922794BC533236B99FEBB (void);
// 0x00000072 System.Void FlexibleGridLayout::SetLayoutVertical()
extern void FlexibleGridLayout_SetLayoutVertical_m3D461C345F094FE39C61BCB1A490144B8C2A0DB6 (void);
// 0x00000073 System.Void FlexibleGridLayout::.ctor()
extern void FlexibleGridLayout__ctor_mB90B3D4E55E5A00B12DE61EC3347F50EF7B0BFA1 (void);
// 0x00000074 System.Void TabButton::Start()
extern void TabButton_Start_m878CEC14619508594E283605C7070F230958BADE (void);
// 0x00000075 System.Void TabButton::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TabButton_OnPointerClick_m1955C94C6CF39E00025C038D730D7E605F483EAC (void);
// 0x00000076 System.Void TabButton::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TabButton_OnPointerEnter_mBF3284FD3B9782E86D8502E12FB9CCC2858361E3 (void);
// 0x00000077 System.Void TabButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TabButton_OnPointerExit_mB802AA341D932C3DB5408BF2E35AC73576B305CB (void);
// 0x00000078 System.Void TabButton::Select()
extern void TabButton_Select_m3C1C109A11B4FCF5B9B44F17A14833A916F29537 (void);
// 0x00000079 System.Void TabButton::Deselect()
extern void TabButton_Deselect_m2E77CE910058846A767449A9752DF6030DE5BF84 (void);
// 0x0000007A System.Void TabButton::.ctor()
extern void TabButton__ctor_m39324870B977EE891A120D004EBC2FFDE037BFBE (void);
// 0x0000007B System.Void TabGroup::Subscribe(TabButton)
extern void TabGroup_Subscribe_mB02E684DB07DFB7168308727E39A800BBBBF3980 (void);
// 0x0000007C System.Void TabGroup::OnTabEnter(TabButton)
extern void TabGroup_OnTabEnter_m97F5336C56400C4A66931E589F37897C8592C700 (void);
// 0x0000007D System.Void TabGroup::OnTabExit(TabButton)
extern void TabGroup_OnTabExit_m7600D6444B122AB6AB90295DD388437EDCC865C5 (void);
// 0x0000007E System.Void TabGroup::OnTabSelected(TabButton)
extern void TabGroup_OnTabSelected_m6BDBDF12235AF081AE085E6D6F4C3A267C652ED7 (void);
// 0x0000007F System.Void TabGroup::ResetTabs()
extern void TabGroup_ResetTabs_m77201A65B95563B1881DF895F06595ECA862A8C5 (void);
// 0x00000080 System.Void TabGroup::.ctor()
extern void TabGroup__ctor_m19FB9010DF250DF75569470723C72A08A4A7808E (void);
// 0x00000081 System.Single UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_mB352439A090F139AD9D4F0EC37748FEC249CA71C (void);
// 0x00000082 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m0F090422A1DC75EDC4AD4F3B1EA3CD2FFB623254 (void);
// 0x00000083 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_mA8BFD89026EB45887653EB6B996B891D8F443BF2 (void);
// 0x00000084 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_m6452460189A1D984E3D0E8CC2EB2145B1D57D7B2 (void);
// 0x00000085 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_mFBA38A4750A52D96065D115C7A2DC2831DE6AF6B (void);
// 0x00000086 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_mFCE27D08AA1DCE080182D97A6D79AF36EA2184CE (void);
// 0x00000087 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_m1931839FD5F7C272E2EFAFD33CE678B6FAB2E0B6 (void);
// 0x00000088 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_m4C948C4B8C166FE99E949F298D52C3ABCDE53AA7 (void);
// 0x00000089 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_m65F2ECDC4A345E3F6FEB3562FC61E4C518E4F941 (void);
// 0x0000008A System.Void UnityMessageManager/MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_m3EA48B06536871149F1A43C905528166D49F48C8 (void);
// 0x0000008B System.Void UnityMessageManager/MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801 (void);
// 0x0000008C System.IAsyncResult UnityMessageManager/MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_m89C256246D24A40498952C82CDD615B3507E4BA4 (void);
// 0x0000008D System.Void UnityMessageManager/MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m778326112BD3D26CB9DE0FFC335E9A1A9353BC35 (void);
// 0x0000008E System.Void UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA (void);
// 0x0000008F System.Void UnityMessageManager/MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43 (void);
// 0x00000090 System.IAsyncResult UnityMessageManager/MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_mFDBB4C951CAC28A4F0E08666050BB3F266FA927E (void);
// 0x00000091 System.Void UnityMessageManager/MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_m9BC3850452E21DEBBF15C7598D811D4D744633A6 (void);
// 0x00000092 System.Void imageScanSpawn/<Get>d__16::.ctor(System.Int32)
extern void U3CGetU3Ed__16__ctor_m7FC82C9FB47A7543AFDED56BCE4EFD8E0C7BEEA9 (void);
// 0x00000093 System.Void imageScanSpawn/<Get>d__16::System.IDisposable.Dispose()
extern void U3CGetU3Ed__16_System_IDisposable_Dispose_m2344228DAB2E864669A4582CAFB9C9AF073ACB99 (void);
// 0x00000094 System.Boolean imageScanSpawn/<Get>d__16::MoveNext()
extern void U3CGetU3Ed__16_MoveNext_m4027ADCD29F127CDA3CA0D4EDB53D3E2FF8E985F (void);
// 0x00000095 System.Object imageScanSpawn/<Get>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3DF863423120FC19E2EF51ED86714BACBC18F4FD (void);
// 0x00000096 System.Void imageScanSpawn/<Get>d__16::System.Collections.IEnumerator.Reset()
extern void U3CGetU3Ed__16_System_Collections_IEnumerator_Reset_m64389E12A92722AFA70972B3EC2C07807FACFFD3 (void);
// 0x00000097 System.Object imageScanSpawn/<Get>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CGetU3Ed__16_System_Collections_IEnumerator_get_Current_m7BAED2F20D547192FEA70F0AE690A91EC836AF65 (void);
// 0x00000098 System.Void taptoplace/<Get>d__34::.ctor(System.Int32)
extern void U3CGetU3Ed__34__ctor_mFA5E55E9D9E390F01EC64385D19AC3A04410C1FE (void);
// 0x00000099 System.Void taptoplace/<Get>d__34::System.IDisposable.Dispose()
extern void U3CGetU3Ed__34_System_IDisposable_Dispose_m486D5586E3836BC07FEA28C5E5826155024DC11C (void);
// 0x0000009A System.Boolean taptoplace/<Get>d__34::MoveNext()
extern void U3CGetU3Ed__34_MoveNext_m078C5768C542C3DD89B4D59D83DC15563764E0A1 (void);
// 0x0000009B System.Object taptoplace/<Get>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0FBF865175EAAFF0C22BD9A32B97A472EEE613EF (void);
// 0x0000009C System.Void taptoplace/<Get>d__34::System.Collections.IEnumerator.Reset()
extern void U3CGetU3Ed__34_System_Collections_IEnumerator_Reset_mBD017717050BAB6D079A3ECCD91FA85E031BCAB1 (void);
// 0x0000009D System.Object taptoplace/<Get>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CGetU3Ed__34_System_Collections_IEnumerator_get_Current_mE80FC2F534B207CED12121D7AC07B40A791D5E3C (void);
static Il2CppMethodPointer s_methodPointers[157] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E,
	GameManager_Update_m07DC32583BF09EB71183725B7B95FA7B4716988A,
	GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693,
	Rotate_Start_m0C36379106B8F7DD309CAC5E6D720D811129FA07,
	Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330,
	Rotate_SetRotationSpeed_mD1F6C05B18C3D792AB9AF055FDBCA8128873F461,
	Rotate__ctor_mC70374AA84BC2CA04CA039D1B56F5AF1B95D42CF,
	SceneLoader_Start_m2553B08838ABEA27EA661BC8B9CF7D8A14353D7B,
	SceneLoader_Update_mD6559C8F6F858FA7413175425CFE0658DAF89707,
	SceneLoader_LoadScene_mC2BE257125A724B3F3213988D312F0767A97FFD5,
	SceneLoader_MessengerFlutter_mA6DC0EA56132DB935C38E4B00C0C4B9CEA99F3CC,
	SceneLoader_SwitchNative_m347537365D6F4F48B69DBC3C3E224E0BA244CE11,
	SceneLoader_UnloadNative_m115CF3FA476F18B72E4777201B1E2EA039509345,
	SceneLoader_QuitNative_mE0C578D9505964BF7D24448CFDBB46C41BC6264D,
	SceneLoader__ctor_mFE1E7D8E1BB135653D2C6FDE4D40F4FC38115009,
	NativeAPI_OnUnityMessage_mEB20277B0A0AF2ECDFB0DA7E49113BEB51493550,
	NativeAPI_OnUnitySceneLoaded_mB0837ED51EE99E965D15D2BA23B6287C0793D1DF,
	NativeAPI_OnSceneLoaded_m5EC95403676F639015AE4A974CB0350752E92287,
	NativeAPI_SendMessageToFlutter_m31EBCD5E058F0179878B70B9F67D3C4CCF51B07D,
	NativeAPI_ShowHostMainWindow_mB9A7879F05EC5BF0B39BED1AD8EA0B20B5887749,
	NativeAPI_UnloadMainWindow_mEAC1F4610D0B6720F47ED79CB32964585462DAEF,
	NativeAPI_QuitUnityWindow_m11F2D4AC622EF6FEB1D1B0C7BCD73CC3C444D739,
	NativeAPI__ctor_m8A9AEF251DEAC1AD4CAF201F962E5DADBA8F47C5,
	NULL,
	NULL,
	NULL,
	NULL,
	MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE,
	NULL,
	MessageHandler__ctor_mFD041017CFD2E7BE9B9CDB629921C6D155BF2D11,
	MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E,
	UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45,
	UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89,
	UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344,
	UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4,
	UnityMessageManager_add_OnFlutterMessage_m702ECE2711E59769765096D023C48B7C40F493B3,
	UnityMessageManager_remove_OnFlutterMessage_m6EE57FCB3B88653B284550F2168F322B043F3366,
	UnityMessageManager_Start_m062DACF27948F5A0161368FE841569ACA6E92CC7,
	UnityMessageManager_OnSceneLoaded_mCF1EB0D8D0511F19F1BD8403808AC73B7E58E594,
	UnityMessageManager_ShowHostMainWindow_m3A9A30AF899337888D541FA6021BD4BDA21F482A,
	UnityMessageManager_UnloadMainWindow_m64E4F629EEF9EC8DD64C36E0FBEAE1B091547CE0,
	UnityMessageManager_QuitUnityWindow_m5B3DBA2E810AF58BD8C4489E9BB2B2A52A6BC022,
	UnityMessageManager_SendMessageToFlutter_m0ADC2E517A85054AE5BA1D3328255060B20A1570,
	UnityMessageManager_SendMessageToFlutter_m390277F4683DC0C0F3437F5C69A4BD8FAD6DA70E,
	UnityMessageManager_onMessage_mD1704644A461EFD311809BB58F82D36898713485,
	UnityMessageManager_onFlutterMessage_m5BEF69E2E3423B8F8ACCB39BBF8286128B4C2B5A,
	UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7,
	UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B,
	ButtonControl_CloseMenu_mBB9A3DA4AF99C660C0A6D7DB1F093708F4354CC4,
	ButtonControl_OpenMenu_mC78ACCE33065096EBA2D021C3E85FA1E1EB0170E,
	ButtonControl_GoToWebPage_mA20CBD30CEBB4FAF6240A3B0230D0FE3134C5CC4,
	ButtonControl_CloseHelpPanel_mE89EA04B86EE834870E925A4607155316C541B8D,
	ButtonControl_OpenHelpPanel_mF4D55701A3A1FBD8C05964E68A17E131CDC98037,
	ButtonControl_RemoveObject_mD376A17553D9293FD41E7C78A0D34106C3128A97,
	ButtonControl_GetARObjectName_m6BF16A32D689DE43F9C6C63A972EAC723026EAFF,
	ButtonControl_ChangeToScanScene_mAB05F226EBE0B199D97619598777E845BC137238,
	ButtonControl_ChangeToVisualizeScene_mDA2CF2D364564EDCE4A8F0135AB91D489AC1FE55,
	ButtonControl_OpenFeatureMenu_m7D1E6BB13004B8C4490D0B23823A930B9182BC26,
	ButtonControl_CloseFeatureMenu_m51DEDCBD71A4926BD1461F90020F462F5EB319D7,
	ButtonControl__ctor_mD4415647A7E956D92750D2E75F6DB76707014075,
	ProductList__ctor_mB2765DD1BEF4E7167F95FA849194AB150BC6ED10,
	ProgressBar_Awake_m66AE9E4239EAD84F71C2D09339ACF4239BB5FC82,
	ProgressBar_IncrementProgress_mCB2D724AB00BDED0053D300DC69B20E2BCDDBF3D,
	ProgressBar__ctor_mD2838AA6298018734186C1DEFF927E917DEBF05D,
	imageScanSpawn_Start_m81C763BD2B5DC5CC92F00AD71E8386BE41AB41B0,
	imageScanSpawn_Update_mEF4D4E6FA56570D8A5514450384A18AD3A2E01A7,
	imageScanSpawn_OnEnable_m740A63518DB41DCE9E74DF55934A83AA5261543C,
	imageScanSpawn_OnDisable_m18E5468F7946CFF9A45C5E6047124DD19C33BDE7,
	imageScanSpawn_OnTrackedImageChanged_m106AA9A84CE67C1C63EDA206E94B12DD5F16DF23,
	imageScanSpawn_Get_mBC44E280DE5031F2C39E2DC734CD4DCB1C9A69D7,
	imageScanSpawn_UpdateTrackingGameObject_mBC3263936A57673DBB68F284B1BDEB7FCC6538B1,
	imageScanSpawn_UpdateLimitedGameObject_m919EDD2E65B57D445ABC0D8ED7540DD881604CBD,
	imageScanSpawn_UpdateNoneGameObject_mE4A12165C38C766DE1C75D66D7C48AE518C92E30,
	imageScanSpawn_DestroyGameObject_mB1C2229BE57949DBC343AAFDB30995F66050450C,
	imageScanSpawn_DisableUI_mFD5F956C13B53C957C7B03D964CD5F0ED24207A2,
	imageScanSpawn_EnableUI_mF86517531D7751374189C3BCD24DCB78E508FA95,
	imageScanSpawn_RetryDownload_m1185FD24C92C113BC9FFD8ACFDD4745E9E4218BC,
	imageScanSpawn_ErrorListener_m8403630699E696B3C38B23DC5872EF227449641A,
	imageScanSpawn__ctor_mC77E6A9248BB312F756D13FFF63C2A50E75EBC8E,
	taptoplace_Start_m6C7B3A5B3A6F2298B741CCCA354DD87FCD7975C3,
	taptoplace_Update_mC002185C31B8F7E00B094332F0CD68336DC27C5B,
	taptoplace_CrosshairCalculation_m62D3DD9C8FB94D38322B613532104604E9A555C6,
	taptoplace_UpdatePlacementIndicator_m69A41BACF68F532E921DE0B6AEF218C15A98C316,
	taptoplace_LookForPlaceChecker_m09A3281D7ABC595DF20D0E1E06C7AA97549EB144,
	taptoplace_TapToPlace_m933E53BD9507A349E51B7F8D91EBFCA30D66B952,
	taptoplace_SelectObject_mA6361F0B26D22144AEE6156A5FAB14776ED2D513,
	taptoplace_DeselectObject_m07A868DED131B21FAA5A833F02F5E86707003A63,
	taptoplace_MoveObject_mC196A799BA0304772CE5CF10AF1D85E900AF96BE,
	taptoplace_isPointerOverUI_mF4C8BD68817732F8D4C8276187187014311AE3D0,
	taptoplace_Get_m4B6449F9BF0BDE863D0447148B6D531C64E432A8,
	taptoplace_ButtonChecker_m70A95C8040D46BAB8FC404AA754E243EABEFB41F,
	taptoplace_OpenGallery_m7EBE55EE6DC842B345F6CEF558F5E8AAFB3AFA9C,
	taptoplace_TapAnywhereButton_m1F20F58C05C33399FB3F0D3BE4E5EB70A441EB0B,
	taptoplace_ErrorListener_m637B5014972EA8CA81AD600E754A7AD3DB2B8A1C,
	taptoplace_TapToPlaceListener_mEDE7372929CA790EC2A5D456ECF06DF0672E0ED9,
	taptoplace_RetryDownload_m5C29F4FB25A4DE4AE88971299363978E72089E06,
	taptoplace_SetAllPlanesActive_mF4CA01213F6550E90DD1585A6AC841E81DFF17D9,
	taptoplace__ctor_mBC2B5681361A8D87CA7DCBA2ABBB9F72E9397B81,
	ThankYouCardController_Start_mAB40A113A0A3CF6C6730AB4106F905CD10C50A5B,
	ThankYouCardController_Update_m312D079A55AC101E9B5D82647087C572F3EF4502,
	ThankYouCardController_exit_video_m6FABF747250AC287A6696CC95262EA6139558B92,
	ThankYouCardController_watch_video_mEA6DDD1186E526262C9803043A8964CF05EB3D33,
	ThankYouCardController__ctor_m2E3436D35F48687724299033DFF847741775C98D,
	FlexibleGridLayout_CalculateLayoutInputVertical_mFDB2A382522AC0143815D85B4C71A88043830EF0,
	FlexibleGridLayout_SetLayoutHorizontal_m62E00DC5A73CB7738E0922794BC533236B99FEBB,
	FlexibleGridLayout_SetLayoutVertical_m3D461C345F094FE39C61BCB1A490144B8C2A0DB6,
	FlexibleGridLayout__ctor_mB90B3D4E55E5A00B12DE61EC3347F50EF7B0BFA1,
	TabButton_Start_m878CEC14619508594E283605C7070F230958BADE,
	TabButton_OnPointerClick_m1955C94C6CF39E00025C038D730D7E605F483EAC,
	TabButton_OnPointerEnter_mBF3284FD3B9782E86D8502E12FB9CCC2858361E3,
	TabButton_OnPointerExit_mB802AA341D932C3DB5408BF2E35AC73576B305CB,
	TabButton_Select_m3C1C109A11B4FCF5B9B44F17A14833A916F29537,
	TabButton_Deselect_m2E77CE910058846A767449A9752DF6030DE5BF84,
	TabButton__ctor_m39324870B977EE891A120D004EBC2FFDE037BFBE,
	TabGroup_Subscribe_mB02E684DB07DFB7168308727E39A800BBBBF3980,
	TabGroup_OnTabEnter_m97F5336C56400C4A66931E589F37897C8592C700,
	TabGroup_OnTabExit_m7600D6444B122AB6AB90295DD388437EDCC865C5,
	TabGroup_OnTabSelected_m6BDBDF12235AF081AE085E6D6F4C3A267C652ED7,
	TabGroup_ResetTabs_m77201A65B95563B1881DF895F06595ECA862A8C5,
	TabGroup__ctor_m19FB9010DF250DF75569470723C72A08A4A7808E,
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_mB352439A090F139AD9D4F0EC37748FEC249CA71C,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m0F090422A1DC75EDC4AD4F3B1EA3CD2FFB623254,
	ARFeatheredPlaneMeshVisualizer_Awake_mA8BFD89026EB45887653EB6B996B891D8F443BF2,
	ARFeatheredPlaneMeshVisualizer_OnEnable_m6452460189A1D984E3D0E8CC2EB2145B1D57D7B2,
	ARFeatheredPlaneMeshVisualizer_OnDisable_mFBA38A4750A52D96065D115C7A2DC2831DE6AF6B,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_mFCE27D08AA1DCE080182D97A6D79AF36EA2184CE,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_m1931839FD5F7C272E2EFAFD33CE678B6FAB2E0B6,
	ARFeatheredPlaneMeshVisualizer__ctor_m4C948C4B8C166FE99E949F298D52C3ABCDE53AA7,
	ARFeatheredPlaneMeshVisualizer__cctor_m65F2ECDC4A345E3F6FEB3562FC61E4C518E4F941,
	MessageDelegate__ctor_m3EA48B06536871149F1A43C905528166D49F48C8,
	MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801,
	MessageDelegate_BeginInvoke_m89C256246D24A40498952C82CDD615B3507E4BA4,
	MessageDelegate_EndInvoke_m778326112BD3D26CB9DE0FFC335E9A1A9353BC35,
	MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA,
	MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43,
	MessageHandlerDelegate_BeginInvoke_mFDBB4C951CAC28A4F0E08666050BB3F266FA927E,
	MessageHandlerDelegate_EndInvoke_m9BC3850452E21DEBBF15C7598D811D4D744633A6,
	U3CGetU3Ed__16__ctor_m7FC82C9FB47A7543AFDED56BCE4EFD8E0C7BEEA9,
	U3CGetU3Ed__16_System_IDisposable_Dispose_m2344228DAB2E864669A4582CAFB9C9AF073ACB99,
	U3CGetU3Ed__16_MoveNext_m4027ADCD29F127CDA3CA0D4EDB53D3E2FF8E985F,
	U3CGetU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3DF863423120FC19E2EF51ED86714BACBC18F4FD,
	U3CGetU3Ed__16_System_Collections_IEnumerator_Reset_m64389E12A92722AFA70972B3EC2C07807FACFFD3,
	U3CGetU3Ed__16_System_Collections_IEnumerator_get_Current_m7BAED2F20D547192FEA70F0AE690A91EC836AF65,
	U3CGetU3Ed__34__ctor_mFA5E55E9D9E390F01EC64385D19AC3A04410C1FE,
	U3CGetU3Ed__34_System_IDisposable_Dispose_m486D5586E3836BC07FEA28C5E5826155024DC11C,
	U3CGetU3Ed__34_MoveNext_m078C5768C542C3DD89B4D59D83DC15563764E0A1,
	U3CGetU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0FBF865175EAAFF0C22BD9A32B97A472EEE613EF,
	U3CGetU3Ed__34_System_Collections_IEnumerator_Reset_mBD017717050BAB6D079A3ECCD91FA85E031BCAB1,
	U3CGetU3Ed__34_System_Collections_IEnumerator_get_Current_mE80FC2F534B207CED12121D7AC07B40A791D5E3C,
};
static const int32_t s_InvokerIndices[157] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	154,
	1906,
	1705,
	154,
	3,
	3,
	3,
	23,
	-1,
	-1,
	-1,
	-1,
	0,
	-1,
	40,
	26,
	23,
	106,
	26,
	26,
	26,
	26,
	23,
	2832,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	335,
	23,
	23,
	23,
	23,
	23,
	2833,
	28,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2834,
	2835,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	727,
	335,
	23,
	23,
	23,
	2680,
	26,
	23,
	3,
	124,
	26,
	205,
	26,
	124,
	26,
	205,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000002, { 0, 21 } },
	{ 0x02000007, { 21, 9 } },
	{ 0x06000025, { 30, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[31] = 
{
	{ (Il2CppRGCTXDataType)2, 28001 },
	{ (Il2CppRGCTXDataType)3, 22231 },
	{ (Il2CppRGCTXDataType)2, 28002 },
	{ (Il2CppRGCTXDataType)3, 22232 },
	{ (Il2CppRGCTXDataType)3, 22233 },
	{ (Il2CppRGCTXDataType)2, 28003 },
	{ (Il2CppRGCTXDataType)3, 22234 },
	{ (Il2CppRGCTXDataType)3, 22235 },
	{ (Il2CppRGCTXDataType)2, 28004 },
	{ (Il2CppRGCTXDataType)3, 22236 },
	{ (Il2CppRGCTXDataType)3, 22237 },
	{ (Il2CppRGCTXDataType)2, 28005 },
	{ (Il2CppRGCTXDataType)3, 22238 },
	{ (Il2CppRGCTXDataType)3, 22239 },
	{ (Il2CppRGCTXDataType)3, 22240 },
	{ (Il2CppRGCTXDataType)3, 22241 },
	{ (Il2CppRGCTXDataType)3, 22242 },
	{ (Il2CppRGCTXDataType)2, 27509 },
	{ (Il2CppRGCTXDataType)2, 27510 },
	{ (Il2CppRGCTXDataType)2, 27511 },
	{ (Il2CppRGCTXDataType)2, 27512 },
	{ (Il2CppRGCTXDataType)2, 28006 },
	{ (Il2CppRGCTXDataType)3, 22243 },
	{ (Il2CppRGCTXDataType)1, 27527 },
	{ (Il2CppRGCTXDataType)3, 22244 },
	{ (Il2CppRGCTXDataType)3, 22245 },
	{ (Il2CppRGCTXDataType)2, 28007 },
	{ (Il2CppRGCTXDataType)3, 22246 },
	{ (Il2CppRGCTXDataType)2, 28008 },
	{ (Il2CppRGCTXDataType)3, 22247 },
	{ (Il2CppRGCTXDataType)3, 22248 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	157,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	31,
	s_rgctxValues,
	NULL,
};
