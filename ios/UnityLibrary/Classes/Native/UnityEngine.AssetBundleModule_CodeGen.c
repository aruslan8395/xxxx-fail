﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4 (void);
// 0x00000002 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync_Internal(System.String,System.UInt32,System.UInt64)
extern void AssetBundle_LoadFromFileAsync_Internal_m3F153468428A31347039E733D86A6AA6470385FD (void);
// 0x00000003 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String)
extern void AssetBundle_LoadFromFileAsync_m9B68068A09AD5E39904C23E69F03B53D24024C75 (void);
// 0x00000004 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String,System.UInt32)
extern void AssetBundle_LoadFromFileAsync_mE752E328810CB5390D38B81BB2C4FCE2267C41B5 (void);
// 0x00000005 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_m53B5A725689D7A0D5F8C002E2F2F693AE6E99AB5 (void);
// 0x00000006 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssetsAsync_mC73675FD84475804F33F140C675EBA814B8C0050 (void);
// 0x00000007 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync()
// 0x00000008 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync(System.Type)
extern void AssetBundle_LoadAllAssetsAsync_mAA52E6B1A7B29D97604108AF80CEE54B4954C984 (void);
// 0x00000009 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_Internal_m2EF88B897D614B01D8DDBACC25C3FEA7E754B075 (void);
// 0x0000000A System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3 (void);
// 0x0000000B UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m9E9879B2CD95EAD2142B96E1CDAB0F167E766CD9 (void);
// 0x0000000C UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern void AssetBundleCreateRequest_get_assetBundle_mFBACEFA632A41C43D4394F6770059DDE0E35BA11 (void);
// 0x0000000D System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern void AssetBundleCreateRequest__ctor_m25AE702BB10868BE4521FBBF1481C36AF51501EE (void);
// 0x0000000E UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern void AssetBundleRequest_get_asset_mCB977DEC51A59B1333AEEFC2AA189D2E5BF4FCF1 (void);
// 0x0000000F UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern void AssetBundleRequest_get_allAssets_mF68D1A1901B4F435D675982523CFEC98A96A6EAA (void);
// 0x00000010 System.Void UnityEngine.AssetBundleRequest::.ctor()
extern void AssetBundleRequest__ctor_m752C12271B44EED163182E197EE783D18E36836B (void);
static Il2CppMethodPointer s_methodPointers[16] = 
{
	AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4,
	AssetBundle_LoadFromFileAsync_Internal_m3F153468428A31347039E733D86A6AA6470385FD,
	AssetBundle_LoadFromFileAsync_m9B68068A09AD5E39904C23E69F03B53D24024C75,
	AssetBundle_LoadFromFileAsync_mE752E328810CB5390D38B81BB2C4FCE2267C41B5,
	AssetBundle_LoadAssetAsync_m53B5A725689D7A0D5F8C002E2F2F693AE6E99AB5,
	AssetBundle_LoadAssetWithSubAssetsAsync_mC73675FD84475804F33F140C675EBA814B8C0050,
	NULL,
	AssetBundle_LoadAllAssetsAsync_mAA52E6B1A7B29D97604108AF80CEE54B4954C984,
	AssetBundle_LoadAssetAsync_Internal_m2EF88B897D614B01D8DDBACC25C3FEA7E754B075,
	AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3,
	AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m9E9879B2CD95EAD2142B96E1CDAB0F167E766CD9,
	AssetBundleCreateRequest_get_assetBundle_mFBACEFA632A41C43D4394F6770059DDE0E35BA11,
	AssetBundleCreateRequest__ctor_m25AE702BB10868BE4521FBBF1481C36AF51501EE,
	AssetBundleRequest_get_asset_mCB977DEC51A59B1333AEEFC2AA189D2E5BF4FCF1,
	AssetBundleRequest_get_allAssets_mF68D1A1901B4F435D675982523CFEC98A96A6EAA,
	AssetBundleRequest__ctor_m752C12271B44EED163182E197EE783D18E36836B,
};
static const int32_t s_InvokerIndices[16] = 
{
	23,
	1787,
	0,
	119,
	105,
	105,
	-1,
	28,
	105,
	31,
	105,
	14,
	23,
	14,
	14,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000007, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)1, 27892 },
};
extern const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	16,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
};
