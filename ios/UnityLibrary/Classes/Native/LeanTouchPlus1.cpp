﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Lean.Touch.LeanFinger
struct LeanFinger_t1528CFE253E230129315FF802274B185655954CE;
// Lean.Touch.LeanFingerData
struct LeanFingerData_t12A7D2A2B984684FE69F1E5FB494BFBD6067B110;
// Lean.Touch.LeanFinger[]
struct LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41;
// Lean.Touch.LeanSelectable
struct LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79;
// Lean.Touch.LeanSelectable/LeanFingerEvent
struct LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282;
// Lean.Touch.LeanShapeDetector/FingerData
struct FingerData_tCFC35530CA4ABA18B2873B33BA5EB3DC7A3E1B8A;
// Lean.Touch.LeanShapeDetector/LeanFingerEvent
struct LeanFingerEvent_t0379EE36FAA2EC98182439F7626D9D8A16E5B9E9;
// Lean.Touch.LeanSpawnWithFinger
struct LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667;
// Lean.Touch.LeanSpawnWithFinger/FingerData
struct FingerData_t8756EDE1698B51920E54644B779698551904C8E5;
// Lean.Touch.LeanSpawnWithFinger/FingerData[]
struct FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7;
// Lean.Touch.LeanSwipeEdge
struct LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE;
// Lean.Touch.LeanTwistCamera
struct LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC;
// System.Action`1<Lean.Touch.LeanFinger>
struct Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99;
// System.Action`1<Lean.Touch.LeanSelectable>
struct Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>
struct Action_2_t660EA53697133D390D7238C3C0293C36652598CB;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.LinkedListNode`1<Lean.Touch.LeanSelectable>
struct LinkedListNode_1_tAEE63C6BA7B266F5425B2CBDBD8097FAE2D35232;
// System.Collections.Generic.LinkedList`1<Lean.Touch.LeanSelectable>
struct LinkedList_1_t4E3BCFB46FFEA1E9B0EAE493EC367C837291D101;
// System.Collections.Generic.List`1<Lean.Touch.LeanFinger>
struct List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B;
// System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>
struct List_1_t4134708539201512B20A18EACBD14D5F90B7B245;
// System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>
struct List_1_t90FFA2AB8560016783049867F226877AB47A1191;
// System.Collections.Generic.List`1<Lean.Touch.LeanSpawnWithFinger/FingerData>
struct List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.Stack`1<Lean.Touch.LeanSpawnWithFinger/FingerData>
struct Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>
struct UnityEvent_1_tD1E06FF088673F5F7209C2A772D1DBA04953C000;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t9E897A46A46C78F7104A831E63BB081546EFFF0D;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanTouch_tECC655E5BCC1A8DD00E84C0A32FEA8EAAAAFD22D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m1DE067369478B25813790543A9C8A9C097D1D552_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisLeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_m3038CB3801530355AA0F3C9BB2C5DEE08BC318D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LeanFingerData_FindOrCreate_TisFingerData_t8756EDE1698B51920E54644B779698551904C8E5_mCFFC1F97189676F54D02C533194FC65F21E5C294_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LeanFingerData_Remove_TisFingerData_t8756EDE1698B51920E54644B779698551904C8E5_mF7E71281B78FF383B11C95F791C570436BEAD284_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LeanSpawnWithFinger_HandleFingerUp_mA7E3129AE8CB8D932D7414C9437C99B5ACA991C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8301CAD35F9D130F39671FA9940FBEDED41A8FD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m8AF72779CDFBF0B3C04712DC2A82BEB60574E971_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mB7DBB69CCDC0F6F5965168A41C7E61BCCDA3E116_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mBF7D336A87CDF3BE4D94C60FDEB087CA861E8F3E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m25A520345C0C68F6162996DAA85DA2282D656A12_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m5A4EE1F195799CBF32D8CBF91CF1F7D3060600F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mCE09C7230D53F5DDE30062F890BEE99AF6FE8347_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_mB2B41F94C0264EE75558454A3CA3AB02E1C681F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1__ctor_m3F7C6FE33FAA71F303CB89570F8792B20371D121_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m6D0FC6F7D956ED8EFFDB8B6DD572A7355A9700F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t FingerData__ctor_m6606D6B3FCB5F6089BA5C2A51AE35F99A9FA3071_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FingerData_get_EndPoint_m67160CC61B111D4DFACA6F7AB1948B721CFE4681_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanFingerEvent__ctor_m2661A1F5658A39B05226E2D7AD710A60EE608BF1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSpawnWithFinger_HandleFingerUp_mA7E3129AE8CB8D932D7414C9437C99B5ACA991C8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSpawnWithFinger_OnDisable_m8633E4CBD95D607BC247EF69E8CE5FC71CF4A356_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSpawnWithFinger_OnEnable_m2A88FFA343CA960D22744332B7EAE4B5BEFF95F7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSpawnWithFinger_Spawn_m7B88A5BE93DB3947D35F3FFD191A24225ED1E5FF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSpawnWithFinger_UpdateSpawnedTransform_mDE1F0B78EB0DF131E334929DB5492F5DF275AEF1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSpawnWithFinger_Update_m05192F6CFBD8FF8DBB8FFAD5748FC7CF0C324E7A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSpawnWithFinger__cctor_m97C86DD6F6E5BE488C1CC580B00889AC3168D5B7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSwipeEdge_CheckBetween_m3A2293756FD47E3B0DD85AE3E84D27DD172327F4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSwipeEdge_Update_mE12B77020D9BA077D050696B5B83113C522386B5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanSwipeEdge_get_OnEdge_m1BD57424A4B69E661270AEF52822A2558F2488B5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanTwistCamera_Update_mF932C5BA25C5AFDC87D121E8AFCB3382083BD43B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LeanTwistCamera__ctor_m99D0DB9BB0A2ED9D47AB4C18DB879D2A01412FC1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// Lean.Touch.LeanFingerData
struct LeanFingerData_t12A7D2A2B984684FE69F1E5FB494BFBD6067B110  : public RuntimeObject
{
public:
	// Lean.Touch.LeanFinger Lean.Touch.LeanFingerData::Finger
	LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___Finger_0;

public:
	inline static int32_t get_offset_of_Finger_0() { return static_cast<int32_t>(offsetof(LeanFingerData_t12A7D2A2B984684FE69F1E5FB494BFBD6067B110, ___Finger_0)); }
	inline LeanFinger_t1528CFE253E230129315FF802274B185655954CE * get_Finger_0() const { return ___Finger_0; }
	inline LeanFinger_t1528CFE253E230129315FF802274B185655954CE ** get_address_of_Finger_0() { return &___Finger_0; }
	inline void set_Finger_0(LeanFinger_t1528CFE253E230129315FF802274B185655954CE * value)
	{
		___Finger_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Finger_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<Lean.Touch.LeanFinger>
struct List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B, ____items_1)); }
	inline LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41* get__items_1() const { return ____items_1; }
	inline LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B_StaticFields, ____emptyArray_5)); }
	inline LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41* get__emptyArray_5() const { return ____emptyArray_5; }
	inline LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(LeanFingerU5BU5D_t73C432E5F550C45897FABE8285AA7F15F32A5D41* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Lean.Touch.LeanSpawnWithFinger/FingerData>
struct List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E, ____items_1)); }
	inline FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* get__items_1() const { return ____items_1; }
	inline FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E_StaticFields, ____emptyArray_5)); }
	inline FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* get__emptyArray_5() const { return ____emptyArray_5; }
	inline FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____items_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Stack`1<Lean.Touch.LeanSpawnWithFinger/FingerData>
struct Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294, ____array_0)); }
	inline FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* get__array_0() const { return ____array_0; }
	inline FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(FingerDataU5BU5D_t1CB768E85ED4F741AD3CF3A523C13553F8EDCBB7* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}
};


// System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// Lean.Touch.LeanShapeDetector/FingerData
struct FingerData_tCFC35530CA4ABA18B2873B33BA5EB3DC7A3E1B8A  : public LeanFingerData_t12A7D2A2B984684FE69F1E5FB494BFBD6067B110
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Lean.Touch.LeanShapeDetector/FingerData::Points
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___Points_1;

public:
	inline static int32_t get_offset_of_Points_1() { return static_cast<int32_t>(offsetof(FingerData_tCFC35530CA4ABA18B2873B33BA5EB3DC7A3E1B8A, ___Points_1)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_Points_1() const { return ___Points_1; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_Points_1() { return &___Points_1; }
	inline void set_Points_1(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___Points_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Points_1), (void*)value);
	}
};


// Lean.Touch.LeanSpawnWithFinger/FingerData
struct FingerData_t8756EDE1698B51920E54644B779698551904C8E5  : public LeanFingerData_t12A7D2A2B984684FE69F1E5FB494BFBD6067B110
{
public:
	// UnityEngine.Transform Lean.Touch.LeanSpawnWithFinger/FingerData::Clone
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Clone_1;

public:
	inline static int32_t get_offset_of_Clone_1() { return static_cast<int32_t>(offsetof(FingerData_t8756EDE1698B51920E54644B779698551904C8E5, ___Clone_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Clone_1() const { return ___Clone_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Clone_1() { return &___Clone_1; }
	inline void set_Clone_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Clone_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Clone_1), (void*)value);
	}
};


// System.Boolean
struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>
struct UnityEvent_1_tD1E06FF088673F5F7209C2A772D1DBA04953C000  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tD1E06FF088673F5F7209C2A772D1DBA04953C000, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.LayerMask
struct LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// Lean.Touch.LeanFinger
struct LeanFinger_t1528CFE253E230129315FF802274B185655954CE  : public RuntimeObject
{
public:
	// System.Int32 Lean.Touch.LeanFinger::Index
	int32_t ___Index_0;
	// System.Single Lean.Touch.LeanFinger::Age
	float ___Age_1;
	// System.Boolean Lean.Touch.LeanFinger::Set
	bool ___Set_2;
	// System.Boolean Lean.Touch.LeanFinger::LastSet
	bool ___LastSet_3;
	// System.Boolean Lean.Touch.LeanFinger::Tap
	bool ___Tap_4;
	// System.Int32 Lean.Touch.LeanFinger::TapCount
	int32_t ___TapCount_5;
	// System.Boolean Lean.Touch.LeanFinger::Swipe
	bool ___Swipe_6;
	// System.Boolean Lean.Touch.LeanFinger::Old
	bool ___Old_7;
	// System.Boolean Lean.Touch.LeanFinger::Expired
	bool ___Expired_8;
	// System.Single Lean.Touch.LeanFinger::LastPressure
	float ___LastPressure_9;
	// System.Single Lean.Touch.LeanFinger::Pressure
	float ___Pressure_10;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::StartScreenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___StartScreenPosition_11;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::LastScreenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___LastScreenPosition_12;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::ScreenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___ScreenPosition_13;
	// System.Boolean Lean.Touch.LeanFinger::StartedOverGui
	bool ___StartedOverGui_14;
	// System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot> Lean.Touch.LeanFinger::Snapshots
	List_1_t90FFA2AB8560016783049867F226877AB47A1191 * ___Snapshots_15;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Index_0)); }
	inline int32_t get_Index_0() const { return ___Index_0; }
	inline int32_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(int32_t value)
	{
		___Index_0 = value;
	}

	inline static int32_t get_offset_of_Age_1() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Age_1)); }
	inline float get_Age_1() const { return ___Age_1; }
	inline float* get_address_of_Age_1() { return &___Age_1; }
	inline void set_Age_1(float value)
	{
		___Age_1 = value;
	}

	inline static int32_t get_offset_of_Set_2() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Set_2)); }
	inline bool get_Set_2() const { return ___Set_2; }
	inline bool* get_address_of_Set_2() { return &___Set_2; }
	inline void set_Set_2(bool value)
	{
		___Set_2 = value;
	}

	inline static int32_t get_offset_of_LastSet_3() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___LastSet_3)); }
	inline bool get_LastSet_3() const { return ___LastSet_3; }
	inline bool* get_address_of_LastSet_3() { return &___LastSet_3; }
	inline void set_LastSet_3(bool value)
	{
		___LastSet_3 = value;
	}

	inline static int32_t get_offset_of_Tap_4() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Tap_4)); }
	inline bool get_Tap_4() const { return ___Tap_4; }
	inline bool* get_address_of_Tap_4() { return &___Tap_4; }
	inline void set_Tap_4(bool value)
	{
		___Tap_4 = value;
	}

	inline static int32_t get_offset_of_TapCount_5() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___TapCount_5)); }
	inline int32_t get_TapCount_5() const { return ___TapCount_5; }
	inline int32_t* get_address_of_TapCount_5() { return &___TapCount_5; }
	inline void set_TapCount_5(int32_t value)
	{
		___TapCount_5 = value;
	}

	inline static int32_t get_offset_of_Swipe_6() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Swipe_6)); }
	inline bool get_Swipe_6() const { return ___Swipe_6; }
	inline bool* get_address_of_Swipe_6() { return &___Swipe_6; }
	inline void set_Swipe_6(bool value)
	{
		___Swipe_6 = value;
	}

	inline static int32_t get_offset_of_Old_7() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Old_7)); }
	inline bool get_Old_7() const { return ___Old_7; }
	inline bool* get_address_of_Old_7() { return &___Old_7; }
	inline void set_Old_7(bool value)
	{
		___Old_7 = value;
	}

	inline static int32_t get_offset_of_Expired_8() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Expired_8)); }
	inline bool get_Expired_8() const { return ___Expired_8; }
	inline bool* get_address_of_Expired_8() { return &___Expired_8; }
	inline void set_Expired_8(bool value)
	{
		___Expired_8 = value;
	}

	inline static int32_t get_offset_of_LastPressure_9() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___LastPressure_9)); }
	inline float get_LastPressure_9() const { return ___LastPressure_9; }
	inline float* get_address_of_LastPressure_9() { return &___LastPressure_9; }
	inline void set_LastPressure_9(float value)
	{
		___LastPressure_9 = value;
	}

	inline static int32_t get_offset_of_Pressure_10() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Pressure_10)); }
	inline float get_Pressure_10() const { return ___Pressure_10; }
	inline float* get_address_of_Pressure_10() { return &___Pressure_10; }
	inline void set_Pressure_10(float value)
	{
		___Pressure_10 = value;
	}

	inline static int32_t get_offset_of_StartScreenPosition_11() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___StartScreenPosition_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_StartScreenPosition_11() const { return ___StartScreenPosition_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_StartScreenPosition_11() { return &___StartScreenPosition_11; }
	inline void set_StartScreenPosition_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___StartScreenPosition_11 = value;
	}

	inline static int32_t get_offset_of_LastScreenPosition_12() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___LastScreenPosition_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_LastScreenPosition_12() const { return ___LastScreenPosition_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_LastScreenPosition_12() { return &___LastScreenPosition_12; }
	inline void set_LastScreenPosition_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___LastScreenPosition_12 = value;
	}

	inline static int32_t get_offset_of_ScreenPosition_13() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___ScreenPosition_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_ScreenPosition_13() const { return ___ScreenPosition_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_ScreenPosition_13() { return &___ScreenPosition_13; }
	inline void set_ScreenPosition_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___ScreenPosition_13 = value;
	}

	inline static int32_t get_offset_of_StartedOverGui_14() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___StartedOverGui_14)); }
	inline bool get_StartedOverGui_14() const { return ___StartedOverGui_14; }
	inline bool* get_address_of_StartedOverGui_14() { return &___StartedOverGui_14; }
	inline void set_StartedOverGui_14(bool value)
	{
		___StartedOverGui_14 = value;
	}

	inline static int32_t get_offset_of_Snapshots_15() { return static_cast<int32_t>(offsetof(LeanFinger_t1528CFE253E230129315FF802274B185655954CE, ___Snapshots_15)); }
	inline List_1_t90FFA2AB8560016783049867F226877AB47A1191 * get_Snapshots_15() const { return ___Snapshots_15; }
	inline List_1_t90FFA2AB8560016783049867F226877AB47A1191 ** get_address_of_Snapshots_15() { return &___Snapshots_15; }
	inline void set_Snapshots_15(List_1_t90FFA2AB8560016783049867F226877AB47A1191 * value)
	{
		___Snapshots_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Snapshots_15), (void*)value);
	}
};


// Lean.Touch.LeanFingerFilter/FilterType
struct FilterType_t32F20D56F57CD04F6621448E2AD180BCA3B7C94A 
{
public:
	// System.Int32 Lean.Touch.LeanFingerFilter/FilterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterType_t32F20D56F57CD04F6621448E2AD180BCA3B7C94A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanScreenDepth/ConversionType
struct ConversionType_tA35455B74CB9E08F7A91FCA3095F5219124BAB66 
{
public:
	// System.Int32 Lean.Touch.LeanScreenDepth/ConversionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConversionType_tA35455B74CB9E08F7A91FCA3095F5219124BAB66, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanShapeDetector/DirectionType
struct DirectionType_t427D8131E8A84CE82C1B129E810513C834A8F754 
{
public:
	// System.Int32 Lean.Touch.LeanShapeDetector/DirectionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectionType_t427D8131E8A84CE82C1B129E810513C834A8F754, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Lean.Touch.LeanShapeDetector/LeanFingerEvent
struct LeanFingerEvent_t0379EE36FAA2EC98182439F7626D9D8A16E5B9E9  : public UnityEvent_1_tD1E06FF088673F5F7209C2A772D1DBA04953C000
{
public:

public:
};


// Lean.Touch.LeanShapeDetector/Line
struct Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 
{
public:
	// UnityEngine.Vector2 Lean.Touch.LeanShapeDetector/Line::A
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___A_0;
	// UnityEngine.Vector2 Lean.Touch.LeanShapeDetector/Line::B
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___B_1;

public:
	inline static int32_t get_offset_of_A_0() { return static_cast<int32_t>(offsetof(Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22, ___A_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_A_0() const { return ___A_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_A_0() { return &___A_0; }
	inline void set_A_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___A_0 = value;
	}

	inline static int32_t get_offset_of_B_1() { return static_cast<int32_t>(offsetof(Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22, ___B_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_B_1() const { return ___B_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_B_1() { return &___B_1; }
	inline void set_B_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___B_1 = value;
	}
};


// Lean.Touch.LeanSpawnWithFinger/RotateType
struct RotateType_tF1405C6128C96B92B9048328C4EC81823EC5E8AD 
{
public:
	// System.Int32 Lean.Touch.LeanSpawnWithFinger/RotateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotateType_tF1405C6128C96B92B9048328C4EC81823EC5E8AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Lean.Touch.LeanFingerFilter
struct LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE 
{
public:
	// Lean.Touch.LeanFingerFilter/FilterType Lean.Touch.LeanFingerFilter::Filter
	int32_t ___Filter_0;
	// System.Boolean Lean.Touch.LeanFingerFilter::IgnoreStartedOverGui
	bool ___IgnoreStartedOverGui_1;
	// System.Int32 Lean.Touch.LeanFingerFilter::RequiredFingerCount
	int32_t ___RequiredFingerCount_2;
	// System.Int32 Lean.Touch.LeanFingerFilter::RequiredMouseButtons
	int32_t ___RequiredMouseButtons_3;
	// Lean.Touch.LeanSelectable Lean.Touch.LeanFingerFilter::RequiredSelectable
	LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * ___RequiredSelectable_4;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::fingers
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___fingers_5;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::filteredFingers
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___filteredFingers_6;

public:
	inline static int32_t get_offset_of_Filter_0() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE, ___Filter_0)); }
	inline int32_t get_Filter_0() const { return ___Filter_0; }
	inline int32_t* get_address_of_Filter_0() { return &___Filter_0; }
	inline void set_Filter_0(int32_t value)
	{
		___Filter_0 = value;
	}

	inline static int32_t get_offset_of_IgnoreStartedOverGui_1() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE, ___IgnoreStartedOverGui_1)); }
	inline bool get_IgnoreStartedOverGui_1() const { return ___IgnoreStartedOverGui_1; }
	inline bool* get_address_of_IgnoreStartedOverGui_1() { return &___IgnoreStartedOverGui_1; }
	inline void set_IgnoreStartedOverGui_1(bool value)
	{
		___IgnoreStartedOverGui_1 = value;
	}

	inline static int32_t get_offset_of_RequiredFingerCount_2() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE, ___RequiredFingerCount_2)); }
	inline int32_t get_RequiredFingerCount_2() const { return ___RequiredFingerCount_2; }
	inline int32_t* get_address_of_RequiredFingerCount_2() { return &___RequiredFingerCount_2; }
	inline void set_RequiredFingerCount_2(int32_t value)
	{
		___RequiredFingerCount_2 = value;
	}

	inline static int32_t get_offset_of_RequiredMouseButtons_3() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE, ___RequiredMouseButtons_3)); }
	inline int32_t get_RequiredMouseButtons_3() const { return ___RequiredMouseButtons_3; }
	inline int32_t* get_address_of_RequiredMouseButtons_3() { return &___RequiredMouseButtons_3; }
	inline void set_RequiredMouseButtons_3(int32_t value)
	{
		___RequiredMouseButtons_3 = value;
	}

	inline static int32_t get_offset_of_RequiredSelectable_4() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE, ___RequiredSelectable_4)); }
	inline LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * get_RequiredSelectable_4() const { return ___RequiredSelectable_4; }
	inline LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 ** get_address_of_RequiredSelectable_4() { return &___RequiredSelectable_4; }
	inline void set_RequiredSelectable_4(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * value)
	{
		___RequiredSelectable_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RequiredSelectable_4), (void*)value);
	}

	inline static int32_t get_offset_of_fingers_5() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE, ___fingers_5)); }
	inline List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * get_fingers_5() const { return ___fingers_5; }
	inline List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B ** get_address_of_fingers_5() { return &___fingers_5; }
	inline void set_fingers_5(List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * value)
	{
		___fingers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fingers_5), (void*)value);
	}

	inline static int32_t get_offset_of_filteredFingers_6() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE, ___filteredFingers_6)); }
	inline List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * get_filteredFingers_6() const { return ___filteredFingers_6; }
	inline List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B ** get_address_of_filteredFingers_6() { return &___filteredFingers_6; }
	inline void set_filteredFingers_6(List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * value)
	{
		___filteredFingers_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filteredFingers_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Lean.Touch.LeanFingerFilter
struct LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE_marshaled_pinvoke
{
	int32_t ___Filter_0;
	int32_t ___IgnoreStartedOverGui_1;
	int32_t ___RequiredFingerCount_2;
	int32_t ___RequiredMouseButtons_3;
	LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * ___RequiredSelectable_4;
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___fingers_5;
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___filteredFingers_6;
};
// Native definition for COM marshalling of Lean.Touch.LeanFingerFilter
struct LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE_marshaled_com
{
	int32_t ___Filter_0;
	int32_t ___IgnoreStartedOverGui_1;
	int32_t ___RequiredFingerCount_2;
	int32_t ___RequiredMouseButtons_3;
	LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * ___RequiredSelectable_4;
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___fingers_5;
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___filteredFingers_6;
};

// Lean.Touch.LeanScreenDepth
struct LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D 
{
public:
	// Lean.Touch.LeanScreenDepth/ConversionType Lean.Touch.LeanScreenDepth::Conversion
	int32_t ___Conversion_0;
	// UnityEngine.Camera Lean.Touch.LeanScreenDepth::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_1;
	// UnityEngine.Object Lean.Touch.LeanScreenDepth::Object
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___Object_2;
	// UnityEngine.LayerMask Lean.Touch.LeanScreenDepth::Layers
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___Layers_3;
	// System.Single Lean.Touch.LeanScreenDepth::Distance
	float ___Distance_4;

public:
	inline static int32_t get_offset_of_Conversion_0() { return static_cast<int32_t>(offsetof(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D, ___Conversion_0)); }
	inline int32_t get_Conversion_0() const { return ___Conversion_0; }
	inline int32_t* get_address_of_Conversion_0() { return &___Conversion_0; }
	inline void set_Conversion_0(int32_t value)
	{
		___Conversion_0 = value;
	}

	inline static int32_t get_offset_of_Camera_1() { return static_cast<int32_t>(offsetof(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D, ___Camera_1)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_1() const { return ___Camera_1; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_1() { return &___Camera_1; }
	inline void set_Camera_1(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Camera_1), (void*)value);
	}

	inline static int32_t get_offset_of_Object_2() { return static_cast<int32_t>(offsetof(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D, ___Object_2)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_Object_2() const { return ___Object_2; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_Object_2() { return &___Object_2; }
	inline void set_Object_2(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___Object_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Object_2), (void*)value);
	}

	inline static int32_t get_offset_of_Layers_3() { return static_cast<int32_t>(offsetof(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D, ___Layers_3)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_Layers_3() const { return ___Layers_3; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_Layers_3() { return &___Layers_3; }
	inline void set_Layers_3(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___Layers_3 = value;
	}

	inline static int32_t get_offset_of_Distance_4() { return static_cast<int32_t>(offsetof(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D, ___Distance_4)); }
	inline float get_Distance_4() const { return ___Distance_4; }
	inline float* get_address_of_Distance_4() { return &___Distance_4; }
	inline void set_Distance_4(float value)
	{
		___Distance_4 = value;
	}
};

struct LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_StaticFields
{
public:
	// UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::LastWorldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___LastWorldNormal_5;
	// UnityEngine.RaycastHit[] Lean.Touch.LeanScreenDepth::hits
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___hits_6;

public:
	inline static int32_t get_offset_of_LastWorldNormal_5() { return static_cast<int32_t>(offsetof(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_StaticFields, ___LastWorldNormal_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_LastWorldNormal_5() const { return ___LastWorldNormal_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_LastWorldNormal_5() { return &___LastWorldNormal_5; }
	inline void set_LastWorldNormal_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___LastWorldNormal_5 = value;
	}

	inline static int32_t get_offset_of_hits_6() { return static_cast<int32_t>(offsetof(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_StaticFields, ___hits_6)); }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* get_hits_6() const { return ___hits_6; }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57** get_address_of_hits_6() { return &___hits_6; }
	inline void set_hits_6(RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* value)
	{
		___hits_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hits_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Lean.Touch.LeanScreenDepth
struct LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_marshaled_pinvoke
{
	int32_t ___Conversion_0;
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_1;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke ___Object_2;
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___Layers_3;
	float ___Distance_4;
};
// Native definition for COM marshalling of Lean.Touch.LeanScreenDepth
struct LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_marshaled_com
{
	int32_t ___Conversion_0;
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_1;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com* ___Object_2;
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___Layers_3;
	float ___Distance_4;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// System.Action`1<Lean.Touch.LeanFinger>
struct Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::preWillRenderCanvases
	WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * ___preWillRenderCanvases_4;
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * ___willRenderCanvases_5;

public:
	inline static int32_t get_offset_of_preWillRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields, ___preWillRenderCanvases_4)); }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * get_preWillRenderCanvases_4() const { return ___preWillRenderCanvases_4; }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE ** get_address_of_preWillRenderCanvases_4() { return &___preWillRenderCanvases_4; }
	inline void set_preWillRenderCanvases_4(WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * value)
	{
		___preWillRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___preWillRenderCanvases_4), (void*)value);
	}

	inline static int32_t get_offset_of_willRenderCanvases_5() { return static_cast<int32_t>(offsetof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields, ___willRenderCanvases_5)); }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * get_willRenderCanvases_5() const { return ___willRenderCanvases_5; }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE ** get_address_of_willRenderCanvases_5() { return &___willRenderCanvases_5; }
	inline void set_willRenderCanvases_5(WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * value)
	{
		___willRenderCanvases_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___willRenderCanvases_5), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// Lean.Touch.LeanSelectable
struct LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanSelectable::DeselectOnUp
	bool ___DeselectOnUp_11;
	// System.Boolean Lean.Touch.LeanSelectable::HideWithFinger
	bool ___HideWithFinger_12;
	// System.Boolean Lean.Touch.LeanSelectable::IsolateSelectingFingers
	bool ___IsolateSelectingFingers_13;
	// Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::onSelect
	LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * ___onSelect_14;
	// Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::onSelectUpdate
	LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * ___onSelectUpdate_15;
	// Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::onSelectUp
	LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * ___onSelectUp_16;
	// UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectable::onDeselect
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onDeselect_17;
	// System.Boolean Lean.Touch.LeanSelectable::isSelected
	bool ___isSelected_18;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::selectingFingers
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___selectingFingers_19;
	// System.Collections.Generic.LinkedListNode`1<Lean.Touch.LeanSelectable> Lean.Touch.LeanSelectable::node
	LinkedListNode_1_tAEE63C6BA7B266F5425B2CBDBD8097FAE2D35232 * ___node_20;

public:
	inline static int32_t get_offset_of_DeselectOnUp_11() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___DeselectOnUp_11)); }
	inline bool get_DeselectOnUp_11() const { return ___DeselectOnUp_11; }
	inline bool* get_address_of_DeselectOnUp_11() { return &___DeselectOnUp_11; }
	inline void set_DeselectOnUp_11(bool value)
	{
		___DeselectOnUp_11 = value;
	}

	inline static int32_t get_offset_of_HideWithFinger_12() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___HideWithFinger_12)); }
	inline bool get_HideWithFinger_12() const { return ___HideWithFinger_12; }
	inline bool* get_address_of_HideWithFinger_12() { return &___HideWithFinger_12; }
	inline void set_HideWithFinger_12(bool value)
	{
		___HideWithFinger_12 = value;
	}

	inline static int32_t get_offset_of_IsolateSelectingFingers_13() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___IsolateSelectingFingers_13)); }
	inline bool get_IsolateSelectingFingers_13() const { return ___IsolateSelectingFingers_13; }
	inline bool* get_address_of_IsolateSelectingFingers_13() { return &___IsolateSelectingFingers_13; }
	inline void set_IsolateSelectingFingers_13(bool value)
	{
		___IsolateSelectingFingers_13 = value;
	}

	inline static int32_t get_offset_of_onSelect_14() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___onSelect_14)); }
	inline LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * get_onSelect_14() const { return ___onSelect_14; }
	inline LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 ** get_address_of_onSelect_14() { return &___onSelect_14; }
	inline void set_onSelect_14(LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * value)
	{
		___onSelect_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onSelect_14), (void*)value);
	}

	inline static int32_t get_offset_of_onSelectUpdate_15() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___onSelectUpdate_15)); }
	inline LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * get_onSelectUpdate_15() const { return ___onSelectUpdate_15; }
	inline LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 ** get_address_of_onSelectUpdate_15() { return &___onSelectUpdate_15; }
	inline void set_onSelectUpdate_15(LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * value)
	{
		___onSelectUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onSelectUpdate_15), (void*)value);
	}

	inline static int32_t get_offset_of_onSelectUp_16() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___onSelectUp_16)); }
	inline LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * get_onSelectUp_16() const { return ___onSelectUp_16; }
	inline LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 ** get_address_of_onSelectUp_16() { return &___onSelectUp_16; }
	inline void set_onSelectUp_16(LeanFingerEvent_tA4784DD367D9B8DD44B1FA078479A2B6EDD4F282 * value)
	{
		___onSelectUp_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onSelectUp_16), (void*)value);
	}

	inline static int32_t get_offset_of_onDeselect_17() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___onDeselect_17)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onDeselect_17() const { return ___onDeselect_17; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onDeselect_17() { return &___onDeselect_17; }
	inline void set_onDeselect_17(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onDeselect_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onDeselect_17), (void*)value);
	}

	inline static int32_t get_offset_of_isSelected_18() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___isSelected_18)); }
	inline bool get_isSelected_18() const { return ___isSelected_18; }
	inline bool* get_address_of_isSelected_18() { return &___isSelected_18; }
	inline void set_isSelected_18(bool value)
	{
		___isSelected_18 = value;
	}

	inline static int32_t get_offset_of_selectingFingers_19() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___selectingFingers_19)); }
	inline List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * get_selectingFingers_19() const { return ___selectingFingers_19; }
	inline List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B ** get_address_of_selectingFingers_19() { return &___selectingFingers_19; }
	inline void set_selectingFingers_19(List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * value)
	{
		___selectingFingers_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectingFingers_19), (void*)value);
	}

	inline static int32_t get_offset_of_node_20() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79, ___node_20)); }
	inline LinkedListNode_1_tAEE63C6BA7B266F5425B2CBDBD8097FAE2D35232 * get_node_20() const { return ___node_20; }
	inline LinkedListNode_1_tAEE63C6BA7B266F5425B2CBDBD8097FAE2D35232 ** get_address_of_node_20() { return &___node_20; }
	inline void set_node_20(LinkedListNode_1_tAEE63C6BA7B266F5425B2CBDBD8097FAE2D35232 * value)
	{
		___node_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___node_20), (void*)value);
	}
};

struct LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields
{
public:
	// System.Collections.Generic.LinkedList`1<Lean.Touch.LeanSelectable> Lean.Touch.LeanSelectable::Instances
	LinkedList_1_t4E3BCFB46FFEA1E9B0EAE493EC367C837291D101 * ___Instances_4;
	// System.Action`1<Lean.Touch.LeanSelectable> Lean.Touch.LeanSelectable::OnEnableGlobal
	Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * ___OnEnableGlobal_5;
	// System.Action`1<Lean.Touch.LeanSelectable> Lean.Touch.LeanSelectable::OnDisableGlobal
	Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * ___OnDisableGlobal_6;
	// System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::OnSelectGlobal
	Action_2_t660EA53697133D390D7238C3C0293C36652598CB * ___OnSelectGlobal_7;
	// System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::OnSelectSetGlobal
	Action_2_t660EA53697133D390D7238C3C0293C36652598CB * ___OnSelectSetGlobal_8;
	// System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::OnSelectUpGlobal
	Action_2_t660EA53697133D390D7238C3C0293C36652598CB * ___OnSelectUpGlobal_9;
	// System.Action`1<Lean.Touch.LeanSelectable> Lean.Touch.LeanSelectable::OnDeselectGlobal
	Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * ___OnDeselectGlobal_10;
	// System.Collections.Generic.List`1<Lean.Touch.LeanSelectable> Lean.Touch.LeanSelectable::tempSelectables
	List_1_t4134708539201512B20A18EACBD14D5F90B7B245 * ___tempSelectables_21;

public:
	inline static int32_t get_offset_of_Instances_4() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields, ___Instances_4)); }
	inline LinkedList_1_t4E3BCFB46FFEA1E9B0EAE493EC367C837291D101 * get_Instances_4() const { return ___Instances_4; }
	inline LinkedList_1_t4E3BCFB46FFEA1E9B0EAE493EC367C837291D101 ** get_address_of_Instances_4() { return &___Instances_4; }
	inline void set_Instances_4(LinkedList_1_t4E3BCFB46FFEA1E9B0EAE493EC367C837291D101 * value)
	{
		___Instances_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instances_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnEnableGlobal_5() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields, ___OnEnableGlobal_5)); }
	inline Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * get_OnEnableGlobal_5() const { return ___OnEnableGlobal_5; }
	inline Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 ** get_address_of_OnEnableGlobal_5() { return &___OnEnableGlobal_5; }
	inline void set_OnEnableGlobal_5(Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * value)
	{
		___OnEnableGlobal_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEnableGlobal_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnDisableGlobal_6() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields, ___OnDisableGlobal_6)); }
	inline Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * get_OnDisableGlobal_6() const { return ___OnDisableGlobal_6; }
	inline Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 ** get_address_of_OnDisableGlobal_6() { return &___OnDisableGlobal_6; }
	inline void set_OnDisableGlobal_6(Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * value)
	{
		___OnDisableGlobal_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDisableGlobal_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnSelectGlobal_7() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields, ___OnSelectGlobal_7)); }
	inline Action_2_t660EA53697133D390D7238C3C0293C36652598CB * get_OnSelectGlobal_7() const { return ___OnSelectGlobal_7; }
	inline Action_2_t660EA53697133D390D7238C3C0293C36652598CB ** get_address_of_OnSelectGlobal_7() { return &___OnSelectGlobal_7; }
	inline void set_OnSelectGlobal_7(Action_2_t660EA53697133D390D7238C3C0293C36652598CB * value)
	{
		___OnSelectGlobal_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSelectGlobal_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnSelectSetGlobal_8() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields, ___OnSelectSetGlobal_8)); }
	inline Action_2_t660EA53697133D390D7238C3C0293C36652598CB * get_OnSelectSetGlobal_8() const { return ___OnSelectSetGlobal_8; }
	inline Action_2_t660EA53697133D390D7238C3C0293C36652598CB ** get_address_of_OnSelectSetGlobal_8() { return &___OnSelectSetGlobal_8; }
	inline void set_OnSelectSetGlobal_8(Action_2_t660EA53697133D390D7238C3C0293C36652598CB * value)
	{
		___OnSelectSetGlobal_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSelectSetGlobal_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnSelectUpGlobal_9() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields, ___OnSelectUpGlobal_9)); }
	inline Action_2_t660EA53697133D390D7238C3C0293C36652598CB * get_OnSelectUpGlobal_9() const { return ___OnSelectUpGlobal_9; }
	inline Action_2_t660EA53697133D390D7238C3C0293C36652598CB ** get_address_of_OnSelectUpGlobal_9() { return &___OnSelectUpGlobal_9; }
	inline void set_OnSelectUpGlobal_9(Action_2_t660EA53697133D390D7238C3C0293C36652598CB * value)
	{
		___OnSelectUpGlobal_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSelectUpGlobal_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnDeselectGlobal_10() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields, ___OnDeselectGlobal_10)); }
	inline Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * get_OnDeselectGlobal_10() const { return ___OnDeselectGlobal_10; }
	inline Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 ** get_address_of_OnDeselectGlobal_10() { return &___OnDeselectGlobal_10; }
	inline void set_OnDeselectGlobal_10(Action_1_t71EB143010A73E285DEE973C6DF03BB4E6CE1CE7 * value)
	{
		___OnDeselectGlobal_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDeselectGlobal_10), (void*)value);
	}

	inline static int32_t get_offset_of_tempSelectables_21() { return static_cast<int32_t>(offsetof(LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_StaticFields, ___tempSelectables_21)); }
	inline List_1_t4134708539201512B20A18EACBD14D5F90B7B245 * get_tempSelectables_21() const { return ___tempSelectables_21; }
	inline List_1_t4134708539201512B20A18EACBD14D5F90B7B245 ** get_address_of_tempSelectables_21() { return &___tempSelectables_21; }
	inline void set_tempSelectables_21(List_1_t4134708539201512B20A18EACBD14D5F90B7B245 * value)
	{
		___tempSelectables_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tempSelectables_21), (void*)value);
	}
};


// Lean.Touch.LeanSpawnWithFinger
struct LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform Lean.Touch.LeanSpawnWithFinger::Prefab
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Prefab_4;
	// Lean.Touch.LeanSpawnWithFinger/RotateType Lean.Touch.LeanSpawnWithFinger::RotateTo
	int32_t ___RotateTo_5;
	// System.Boolean Lean.Touch.LeanSpawnWithFinger::DragAfterSpawn
	bool ___DragAfterSpawn_6;
	// Lean.Touch.LeanScreenDepth Lean.Touch.LeanSpawnWithFinger::ScreenDepth
	LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D  ___ScreenDepth_7;
	// UnityEngine.Vector2 Lean.Touch.LeanSpawnWithFinger::PixelOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___PixelOffset_8;
	// UnityEngine.Canvas Lean.Touch.LeanSpawnWithFinger::PixelScale
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___PixelScale_9;
	// UnityEngine.Vector3 Lean.Touch.LeanSpawnWithFinger::WorldOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___WorldOffset_10;
	// UnityEngine.Transform Lean.Touch.LeanSpawnWithFinger::WorldRelativeTo
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___WorldRelativeTo_11;
	// System.Collections.Generic.List`1<Lean.Touch.LeanSpawnWithFinger/FingerData> Lean.Touch.LeanSpawnWithFinger::fingerDatas
	List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * ___fingerDatas_12;

public:
	inline static int32_t get_offset_of_Prefab_4() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___Prefab_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Prefab_4() const { return ___Prefab_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Prefab_4() { return &___Prefab_4; }
	inline void set_Prefab_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Prefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Prefab_4), (void*)value);
	}

	inline static int32_t get_offset_of_RotateTo_5() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___RotateTo_5)); }
	inline int32_t get_RotateTo_5() const { return ___RotateTo_5; }
	inline int32_t* get_address_of_RotateTo_5() { return &___RotateTo_5; }
	inline void set_RotateTo_5(int32_t value)
	{
		___RotateTo_5 = value;
	}

	inline static int32_t get_offset_of_DragAfterSpawn_6() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___DragAfterSpawn_6)); }
	inline bool get_DragAfterSpawn_6() const { return ___DragAfterSpawn_6; }
	inline bool* get_address_of_DragAfterSpawn_6() { return &___DragAfterSpawn_6; }
	inline void set_DragAfterSpawn_6(bool value)
	{
		___DragAfterSpawn_6 = value;
	}

	inline static int32_t get_offset_of_ScreenDepth_7() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___ScreenDepth_7)); }
	inline LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D  get_ScreenDepth_7() const { return ___ScreenDepth_7; }
	inline LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D * get_address_of_ScreenDepth_7() { return &___ScreenDepth_7; }
	inline void set_ScreenDepth_7(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D  value)
	{
		___ScreenDepth_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___ScreenDepth_7))->___Camera_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___ScreenDepth_7))->___Object_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_PixelOffset_8() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___PixelOffset_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_PixelOffset_8() const { return ___PixelOffset_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_PixelOffset_8() { return &___PixelOffset_8; }
	inline void set_PixelOffset_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___PixelOffset_8 = value;
	}

	inline static int32_t get_offset_of_PixelScale_9() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___PixelScale_9)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_PixelScale_9() const { return ___PixelScale_9; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_PixelScale_9() { return &___PixelScale_9; }
	inline void set_PixelScale_9(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___PixelScale_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PixelScale_9), (void*)value);
	}

	inline static int32_t get_offset_of_WorldOffset_10() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___WorldOffset_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_WorldOffset_10() const { return ___WorldOffset_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_WorldOffset_10() { return &___WorldOffset_10; }
	inline void set_WorldOffset_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___WorldOffset_10 = value;
	}

	inline static int32_t get_offset_of_WorldRelativeTo_11() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___WorldRelativeTo_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_WorldRelativeTo_11() const { return ___WorldRelativeTo_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_WorldRelativeTo_11() { return &___WorldRelativeTo_11; }
	inline void set_WorldRelativeTo_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___WorldRelativeTo_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WorldRelativeTo_11), (void*)value);
	}

	inline static int32_t get_offset_of_fingerDatas_12() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667, ___fingerDatas_12)); }
	inline List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * get_fingerDatas_12() const { return ___fingerDatas_12; }
	inline List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E ** get_address_of_fingerDatas_12() { return &___fingerDatas_12; }
	inline void set_fingerDatas_12(List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * value)
	{
		___fingerDatas_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fingerDatas_12), (void*)value);
	}
};

struct LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<Lean.Touch.LeanSpawnWithFinger/FingerData> Lean.Touch.LeanSpawnWithFinger::fingerDataPool
	Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 * ___fingerDataPool_13;

public:
	inline static int32_t get_offset_of_fingerDataPool_13() { return static_cast<int32_t>(offsetof(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667_StaticFields, ___fingerDataPool_13)); }
	inline Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 * get_fingerDataPool_13() const { return ___fingerDataPool_13; }
	inline Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 ** get_address_of_fingerDataPool_13() { return &___fingerDataPool_13; }
	inline void set_fingerDataPool_13(Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 * value)
	{
		___fingerDataPool_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fingerDataPool_13), (void*)value);
	}
};


// Lean.Touch.LeanSwipeEdge
struct LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Lean.Touch.LeanFingerFilter Lean.Touch.LeanSwipeEdge::Use
	LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE  ___Use_4;
	// System.Boolean Lean.Touch.LeanSwipeEdge::Left
	bool ___Left_5;
	// System.Boolean Lean.Touch.LeanSwipeEdge::Right
	bool ___Right_6;
	// System.Boolean Lean.Touch.LeanSwipeEdge::Bottom
	bool ___Bottom_7;
	// System.Boolean Lean.Touch.LeanSwipeEdge::Top
	bool ___Top_8;
	// System.Single Lean.Touch.LeanSwipeEdge::AngleThreshold
	float ___AngleThreshold_9;
	// System.Single Lean.Touch.LeanSwipeEdge::EdgeThreshold
	float ___EdgeThreshold_10;
	// UnityEngine.Events.UnityEvent Lean.Touch.LeanSwipeEdge::onEdge
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onEdge_11;

public:
	inline static int32_t get_offset_of_Use_4() { return static_cast<int32_t>(offsetof(LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE, ___Use_4)); }
	inline LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE  get_Use_4() const { return ___Use_4; }
	inline LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * get_address_of_Use_4() { return &___Use_4; }
	inline void set_Use_4(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE  value)
	{
		___Use_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___RequiredSelectable_4), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___fingers_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___filteredFingers_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_Left_5() { return static_cast<int32_t>(offsetof(LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE, ___Left_5)); }
	inline bool get_Left_5() const { return ___Left_5; }
	inline bool* get_address_of_Left_5() { return &___Left_5; }
	inline void set_Left_5(bool value)
	{
		___Left_5 = value;
	}

	inline static int32_t get_offset_of_Right_6() { return static_cast<int32_t>(offsetof(LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE, ___Right_6)); }
	inline bool get_Right_6() const { return ___Right_6; }
	inline bool* get_address_of_Right_6() { return &___Right_6; }
	inline void set_Right_6(bool value)
	{
		___Right_6 = value;
	}

	inline static int32_t get_offset_of_Bottom_7() { return static_cast<int32_t>(offsetof(LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE, ___Bottom_7)); }
	inline bool get_Bottom_7() const { return ___Bottom_7; }
	inline bool* get_address_of_Bottom_7() { return &___Bottom_7; }
	inline void set_Bottom_7(bool value)
	{
		___Bottom_7 = value;
	}

	inline static int32_t get_offset_of_Top_8() { return static_cast<int32_t>(offsetof(LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE, ___Top_8)); }
	inline bool get_Top_8() const { return ___Top_8; }
	inline bool* get_address_of_Top_8() { return &___Top_8; }
	inline void set_Top_8(bool value)
	{
		___Top_8 = value;
	}

	inline static int32_t get_offset_of_AngleThreshold_9() { return static_cast<int32_t>(offsetof(LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE, ___AngleThreshold_9)); }
	inline float get_AngleThreshold_9() const { return ___AngleThreshold_9; }
	inline float* get_address_of_AngleThreshold_9() { return &___AngleThreshold_9; }
	inline void set_AngleThreshold_9(float value)
	{
		___AngleThreshold_9 = value;
	}

	inline static int32_t get_offset_of_EdgeThreshold_10() { return static_cast<int32_t>(offsetof(LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE, ___EdgeThreshold_10)); }
	inline float get_EdgeThreshold_10() const { return ___EdgeThreshold_10; }
	inline float* get_address_of_EdgeThreshold_10() { return &___EdgeThreshold_10; }
	inline void set_EdgeThreshold_10(float value)
	{
		___EdgeThreshold_10 = value;
	}

	inline static int32_t get_offset_of_onEdge_11() { return static_cast<int32_t>(offsetof(LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE, ___onEdge_11)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onEdge_11() const { return ___onEdge_11; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onEdge_11() { return &___onEdge_11; }
	inline void set_onEdge_11(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onEdge_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onEdge_11), (void*)value);
	}
};


// Lean.Touch.LeanTwistCamera
struct LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Lean.Touch.LeanFingerFilter Lean.Touch.LeanTwistCamera::Use
	LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE  ___Use_4;
	// System.Single Lean.Touch.LeanTwistCamera::Damping
	float ___Damping_5;
	// System.Boolean Lean.Touch.LeanTwistCamera::Relative
	bool ___Relative_6;
	// Lean.Touch.LeanScreenDepth Lean.Touch.LeanTwistCamera::ScreenDepth
	LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D  ___ScreenDepth_7;
	// UnityEngine.Vector3 Lean.Touch.LeanTwistCamera::remainingTranslation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___remainingTranslation_8;
	// UnityEngine.Quaternion Lean.Touch.LeanTwistCamera::remainingRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___remainingRotation_9;

public:
	inline static int32_t get_offset_of_Use_4() { return static_cast<int32_t>(offsetof(LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC, ___Use_4)); }
	inline LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE  get_Use_4() const { return ___Use_4; }
	inline LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * get_address_of_Use_4() { return &___Use_4; }
	inline void set_Use_4(LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE  value)
	{
		___Use_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___RequiredSelectable_4), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___fingers_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___filteredFingers_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_Damping_5() { return static_cast<int32_t>(offsetof(LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC, ___Damping_5)); }
	inline float get_Damping_5() const { return ___Damping_5; }
	inline float* get_address_of_Damping_5() { return &___Damping_5; }
	inline void set_Damping_5(float value)
	{
		___Damping_5 = value;
	}

	inline static int32_t get_offset_of_Relative_6() { return static_cast<int32_t>(offsetof(LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC, ___Relative_6)); }
	inline bool get_Relative_6() const { return ___Relative_6; }
	inline bool* get_address_of_Relative_6() { return &___Relative_6; }
	inline void set_Relative_6(bool value)
	{
		___Relative_6 = value;
	}

	inline static int32_t get_offset_of_ScreenDepth_7() { return static_cast<int32_t>(offsetof(LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC, ___ScreenDepth_7)); }
	inline LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D  get_ScreenDepth_7() const { return ___ScreenDepth_7; }
	inline LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D * get_address_of_ScreenDepth_7() { return &___ScreenDepth_7; }
	inline void set_ScreenDepth_7(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D  value)
	{
		___ScreenDepth_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___ScreenDepth_7))->___Camera_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___ScreenDepth_7))->___Object_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_remainingTranslation_8() { return static_cast<int32_t>(offsetof(LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC, ___remainingTranslation_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_remainingTranslation_8() const { return ___remainingTranslation_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_remainingTranslation_8() { return &___remainingTranslation_8; }
	inline void set_remainingTranslation_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___remainingTranslation_8 = value;
	}

	inline static int32_t get_offset_of_remainingRotation_9() { return static_cast<int32_t>(offsetof(LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC, ___remainingRotation_9)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_remainingRotation_9() const { return ___remainingRotation_9; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_remainingRotation_9() { return &___remainingRotation_9; }
	inline void set_remainingRotation_9(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___remainingRotation_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  m_Items[1];

public:
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mB7DBB69CCDC0F6F5965168A41C7E61BCCDA3E116_gshared_inline (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  List_1_get_Item_m25A520345C0C68F6162996DAA85DA2282D656A12_gshared_inline (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m8301CAD35F9D130F39671FA9940FBEDED41A8FD7_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_m38CD236F782AA440F6DDB5D90B4C9DA24CDBA3A7_gshared (UnityEvent_1_t9E897A46A46C78F7104A831E63BB081546EFFF0D * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mFBEB99FAFA7C81868D18C2A17C94AF512CD872EC_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// !!0 Lean.Touch.LeanFingerData::FindOrCreate<System.Object>(System.Collections.Generic.List`1<!!0>&,Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * LeanFingerData_FindOrCreate_TisRuntimeObject_m37A7D0CB0BCD892F721F538A5968B4BB58BE2712_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** ___fingerDatas0, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Lean.Touch.LeanFingerData::Remove<System.Object>(System.Collections.Generic.List`1<!!0>,Lean.Touch.LeanFinger,System.Collections.Generic.Stack`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerData_Remove_TisRuntimeObject_m9453F46A5E8DD3D1E59DCE3274F683223A3BA6C8_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___fingerDatas0, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger1, Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * ___pool2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stack_1__ctor_m54114F5D347F44F2C0FD45AF09974A5B55EC5373_gshared (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * __this, const RuntimeMethod* method);

// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
inline int32_t List_1_get_Count_mB7DBB69CCDC0F6F5965168A41C7E61BCCDA3E116_inline (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))List_1_get_Count_mB7DBB69CCDC0F6F5965168A41C7E61BCCDA3E116_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  List_1_get_Item_m25A520345C0C68F6162996DAA85DA2282D656A12_inline (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, int32_t, const RuntimeMethod*))List_1_get_Item_m25A520345C0C68F6162996DAA85DA2282D656A12_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
inline void List_1__ctor_m8301CAD35F9D130F39671FA9940FBEDED41A8FD7 (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))List_1__ctor_m8301CAD35F9D130F39671FA9940FBEDED41A8FD7_gshared)(__this, method);
}
// System.Void Lean.Touch.LeanFingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerData__ctor_mAB102253306FDAE43FA8680B4D84059BFD831A18 (LeanFingerData_t12A7D2A2B984684FE69F1E5FB494BFBD6067B110 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::.ctor()
inline void UnityEvent_1__ctor_m6D0FC6F7D956ED8EFFDB8B6DD572A7355A9700F9 (UnityEvent_1_tD1E06FF088673F5F7209C2A772D1DBA04953C000 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tD1E06FF088673F5F7209C2A772D1DBA04953C000 *, const RuntimeMethod*))UnityEvent_1__ctor_m38CD236F782AA440F6DDB5D90B4C9DA24CDBA3A7_gshared)(__this, method);
}
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.Single Lean.Touch.LeanShapeDetector/Line::GetFirstDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4 (Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m0E86E1B1038DDB8554A8A0D58729A7788D989588 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lhs0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_Dot_m34F6A75BE3FC6F728233811943AC4406C7D905BA (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lhs0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(System.Single,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38 (float ___d0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.Single Lean.Touch.LeanShapeDetector/Line::GetDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433 (Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Transform>(!!0)
inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_mB2B41F94C0264EE75558454A3CA3AB02E1C681F2 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___original0, const RuntimeMethod* method)
{
	return ((  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mFBEB99FAFA7C81868D18C2A17C94AF512CD872EC_gshared)(___original0, method);
}
// System.Void Lean.Touch.LeanSpawnWithFinger::UpdateSpawnedTransform(Lean.Touch.LeanFinger,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger_UpdateSpawnedTransform_mDE1F0B78EB0DF131E334929DB5492F5DF275AEF1 (LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667 * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___instance1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// !!0 Lean.Touch.LeanFingerData::FindOrCreate<Lean.Touch.LeanSpawnWithFinger/FingerData>(System.Collections.Generic.List`1<!!0>&,Lean.Touch.LeanFinger)
inline FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * LeanFingerData_FindOrCreate_TisFingerData_t8756EDE1698B51920E54644B779698551904C8E5_mCFFC1F97189676F54D02C533194FC65F21E5C294 (List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E ** ___fingerDatas0, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger1, const RuntimeMethod* method)
{
	return ((  FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * (*) (List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E **, LeanFinger_t1528CFE253E230129315FF802274B185655954CE *, const RuntimeMethod*))LeanFingerData_FindOrCreate_TisRuntimeObject_m37A7D0CB0BCD892F721F538A5968B4BB58BE2712_gshared)(___fingerDatas0, ___finger1, method);
}
// !!0 UnityEngine.Component::GetComponent<Lean.Touch.LeanSelectable>()
inline LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * Component_GetComponent_TisLeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_m3038CB3801530355AA0F3C9BB2C5DEE08BC318D7 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Void Lean.Touch.LeanSelectable::Select(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_Select_mFE2E8E0481A51153E75BFBC02192ABE0DF431601 (LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method);
// System.Void System.Action`1<Lean.Touch.LeanFinger>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m1DE067369478B25813790543A9C8A9C097D1D552 (Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Lean.Touch.LeanTouch::add_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanTouch_add_OnFingerUp_mEF2096577CDA9B4D199625D0F8C60609612D4FE4 (Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99 * ___value0, const RuntimeMethod* method);
// System.Void Lean.Touch.LeanTouch::remove_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanTouch_remove_OnFingerUp_mA4A5415C54D5C2E49F9A423940E92D1C2D9A4DF6 (Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99 * ___value0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Lean.Touch.LeanSpawnWithFinger/FingerData>::get_Count()
inline int32_t List_1_get_Count_m8AF72779CDFBF0B3C04712DC2A82BEB60574E971_inline (List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<Lean.Touch.LeanSpawnWithFinger/FingerData>::get_Item(System.Int32)
inline FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * List_1_get_Item_mCE09C7230D53F5DDE30062F890BEE99AF6FE8347_inline (List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * (*) (List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Canvas_get_scaleFactor_m0F6D59E75F7605ABD2AFF6AF32A1097226CE060A (Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::Convert(UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  LeanScreenDepth_Convert_m4CA8998FD975A5AC752171657968240E73225A49 (LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPoint0, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject1, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___ignore2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_TransformPoint_mA96DC2A20EE7F4F915F7509863A18D99F5DD76CB (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_up(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_up_m05A833821812AF78009A8EFB949C4EFC8087C46A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void Lean.Touch.LeanFingerData::Remove<Lean.Touch.LeanSpawnWithFinger/FingerData>(System.Collections.Generic.List`1<!!0>,Lean.Touch.LeanFinger,System.Collections.Generic.Stack`1<!!0>)
inline void LeanFingerData_Remove_TisFingerData_t8756EDE1698B51920E54644B779698551904C8E5_mF7E71281B78FF383B11C95F791C570436BEAD284 (List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * ___fingerDatas0, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger1, Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 * ___pool2, const RuntimeMethod* method)
{
	((  void (*) (List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E *, LeanFinger_t1528CFE253E230129315FF802274B185655954CE *, Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 *, const RuntimeMethod*))LeanFingerData_Remove_TisRuntimeObject_m9453F46A5E8DD3D1E59DCE3274F683223A3BA6C8_gshared)(___fingerDatas0, ___finger1, ___pool2, method);
}
// System.Void Lean.Touch.LeanScreenDepth::.ctor(Lean.Touch.LeanScreenDepth/ConversionType,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenDepth__ctor_m09C2B43694D7A2B959469447627D3B25AB095FE9 (LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D * __this, int32_t ___newConversion0, int32_t ___newLayers1, float ___newDistance2, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<Lean.Touch.LeanSpawnWithFinger/FingerData>::.ctor()
inline void Stack_1__ctor_m3F7C6FE33FAA71F303CB89570F8792B20371D121 (Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 * __this, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 *, const RuntimeMethod*))Stack_1__ctor_m54114F5D347F44F2C0FD45AF09974A5B55EC5373_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent__ctor_m2F8C02F28E289CA65598FF4FA8EAB84D955FF028 (UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_right_mB4BD67462D579461853F297C0DE85D81E07E911E (const RuntimeMethod* method);
// System.Boolean Lean.Touch.LeanSwipeEdge::CheckAngle(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_xMin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_xMin_mFDFA74F66595FD2B8CE360183D1A92B575F0A76E (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Boolean Lean.Touch.LeanSwipeEdge::CheckEdge(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, float ___distance0, const RuntimeMethod* method);
// System.Void Lean.Touch.LeanSwipeEdge::InvokeEdge()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_InvokeEdge_m57B9682317B2D34649E43C9135D0B934E9DEF37C (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_UnaryNegation_m3FA0AE2F9B031765EFA566B25F5453C3B001FF4D (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_xMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_xMax_mA16D7C3C2F30F8608719073ED79028C11CE90983 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441 (const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_yMin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_yMin_m31EDC3262BE39D2F6464B15397F882237E6158C3 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_yMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_yMax_m8AA5E92C322AF3FF571330F00579DA864F33341B (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Void Lean.Touch.LeanFingerFilter::AddFinger(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerFilter_AddFinger_m74C4A364453ED4A22F8423749A505DF0BA5A92EE (LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method);
// System.Void Lean.Touch.LeanFingerFilter::RemoveFinger(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerFilter_RemoveFinger_mF840A547635068D3F879DF19F3E17BE362E926F7 (LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method);
// System.Void Lean.Touch.LeanFingerFilter::RemoveAllFingers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerFilter_RemoveAllFingers_mE802FAFCE7EBF3FE06ECDF1BA15F3644F85F3E9F (LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * __this, const RuntimeMethod* method);
// System.Void Lean.Touch.LeanFingerFilter::UpdateRequiredSelectable(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerFilter_UpdateRequiredSelectable_m3E2BEC9AB1DAA8AB70EC2E11B8D1227945031E7D (LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * __this, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::GetFingers(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * LeanFingerFilter_GetFingers_mB21B35B96FBDE7030A77F8C129ED467C6C85EDDE (LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * __this, bool ___ignoreUpFingers0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Lean.Touch.LeanFinger>::get_Count()
inline int32_t List_1_get_Count_mBF7D336A87CDF3BE4D94C60FDEB087CA861E8F3E_inline (List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<Lean.Touch.LeanFinger>::get_Item(System.Int32)
inline LeanFinger_t1528CFE253E230129315FF802274B185655954CE * List_1_get_Item_m5A4EE1F195799CBF32D8CBF91CF1F7D3060600F2_inline (List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  LeanFinger_t1528CFE253E230129315FF802274B185655954CE * (*) (List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Void Lean.Touch.LeanSwipeEdge::CheckBetween(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_CheckBetween_m3A2293756FD47E3B0DD85AE3E84D27DD172327F4 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___from0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___to1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325 (UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_Angle_mC4A140B49B9E737C9FC6B52763468C5662A8B4AC (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___from0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___to1, const RuntimeMethod* method);
// System.Single Lean.Touch.LeanTouch::get_ScalingFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanTouch_get_ScalingFactor_m617EED84247952F5B8E628DF758059459DC4420D (const RuntimeMethod* method);
// System.Void Lean.Touch.LeanFingerFilter::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerFilter__ctor_mE72DC02BFD6E51E07FF20A8BEF9F985347483AA5 (LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * __this, bool ___newIgnoreStartedOverGui0, const RuntimeMethod* method);
// System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanGesture_GetTwistDegrees_m5A181D7665683D6BDD079393230F57215808092E (List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___fingers0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Boolean Lean.Touch.LeanGesture::TryGetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanGesture_TryGetScreenCenter_m6B05B5DD79C77423120ABC8F91EE7BF9962F5265 (List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * ___fingers0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___center1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_RotateAround_m433D292B2A38A5A4DEC7DCAE0A8BEAC5C3B2D1DD (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___axis1, float ___angle2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m6F7F297A2AF39309B4B8F537CCF49A143896CFBA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___axis0, float ___angle1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Inverse_mC3A78571A826F05CE179637E675BD25F8B203E0C (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___lhs0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rhs1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Single Lean.Common.LeanHelper::GetDampenFactor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanHelper_GetDampenFactor_mDCE92ADEFA6CD274B18AFD9C541529DD3DBA4642 (float ___damping0, float ___elapsed1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Slerp_m56DE173C3520C83DF3F1C6EDFA82FF88A2C9E756 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___a0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 Lean.Touch.LeanShapeDetector/FingerData::get_EndPoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  FingerData_get_EndPoint_m67160CC61B111D4DFACA6F7AB1948B721CFE4681 (FingerData_tCFC35530CA4ABA18B2873B33BA5EB3DC7A3E1B8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FingerData_get_EndPoint_m67160CC61B111D4DFACA6F7AB1948B721CFE4681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Points[Points.Count - 1];
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_0 = __this->get_Points_1();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_1 = __this->get_Points_1();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_mB7DBB69CCDC0F6F5965168A41C7E61BCCDA3E116_inline(L_1, /*hidden argument*/List_1_get_Count_mB7DBB69CCDC0F6F5965168A41C7E61BCCDA3E116_RuntimeMethod_var);
		NullCheck(L_0);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = List_1_get_Item_m25A520345C0C68F6162996DAA85DA2282D656A12_inline(L_0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)), /*hidden argument*/List_1_get_Item_m25A520345C0C68F6162996DAA85DA2282D656A12_RuntimeMethod_var);
		return L_3;
	}
}
// System.Void Lean.Touch.LeanShapeDetector/FingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FingerData__ctor_m6606D6B3FCB5F6089BA5C2A51AE35F99A9FA3071 (FingerData_tCFC35530CA4ABA18B2873B33BA5EB3DC7A3E1B8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FingerData__ctor_m6606D6B3FCB5F6089BA5C2A51AE35F99A9FA3071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Vector2> Points = new List<Vector2>(); // This stores the current shape this finger has drawn.
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_0 = (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)il2cpp_codegen_object_new(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var);
		List_1__ctor_m8301CAD35F9D130F39671FA9940FBEDED41A8FD7(L_0, /*hidden argument*/List_1__ctor_m8301CAD35F9D130F39671FA9940FBEDED41A8FD7_RuntimeMethod_var);
		__this->set_Points_1(L_0);
		LeanFingerData__ctor_mAB102253306FDAE43FA8680B4D84059BFD831A18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanShapeDetector/LeanFingerEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFingerEvent__ctor_m2661A1F5658A39B05226E2D7AD710A60EE608BF1 (LeanFingerEvent_t0379EE36FAA2EC98182439F7626D9D8A16E5B9E9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanFingerEvent__ctor_m2661A1F5658A39B05226E2D7AD710A60EE608BF1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m6D0FC6F7D956ED8EFFDB8B6DD572A7355A9700F9(__this, /*hidden argument*/UnityEvent_1__ctor_m6D0FC6F7D956ED8EFFDB8B6DD572A7355A9700F9_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Lean.Touch.LeanShapeDetector/Line::GetFirstDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4 (Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Vector2.Distance(point, A);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___point0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = __this->get_A_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		float L_2 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  float Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4_AdjustorThunk (RuntimeObject * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 * _thisAdjusted = reinterpret_cast<Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 *>(__this + _offset);
	return Line_GetFirstDistance_mF30BD9D5922F414642E85ED4E5E529CF084E4AD4(_thisAdjusted, ___point0, method);
}
// System.Single Lean.Touch.LeanShapeDetector/Line::GetDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433 (Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// if (A == B) return Vector2.Distance(point, A);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_A_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = __this->get_B_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		bool L_2 = Vector2_op_Equality_m0E86E1B1038DDB8554A8A0D58729A7788D989588(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		// if (A == B) return Vector2.Distance(point, A);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = ___point0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = __this->get_A_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		float L_5 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0020:
	{
		// var v  = B - A;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = __this->get_B_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = __this->get_A_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		// var w  = point - A;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = ___point0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = __this->get_A_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_9, L_10, /*hidden argument*/NULL);
		// var c1 = Vector2.Dot(w,v); if (c1 <= 0.0f) return Vector2.Distance(point, A);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = V_0;
		float L_13 = Vector2_Dot_m34F6A75BE3FC6F728233811943AC4406C7D905BA(L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		// var c1 = Vector2.Dot(w,v); if (c1 <= 0.0f) return Vector2.Distance(point, A);
		float L_14 = V_1;
		if ((!(((float)L_14) <= ((float)(0.0f)))))
		{
			goto IL_005a;
		}
	}
	{
		// var c1 = Vector2.Dot(w,v); if (c1 <= 0.0f) return Vector2.Distance(point, A);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_15 = ___point0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16 = __this->get_A_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		float L_17 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_005a:
	{
		// var c2 = Vector2.Dot(v,v); if (c2 <= c1) return Vector2.Distance(point, B);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_18 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		float L_20 = Vector2_Dot_m34F6A75BE3FC6F728233811943AC4406C7D905BA(L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		// var c2 = Vector2.Dot(v,v); if (c2 <= c1) return Vector2.Distance(point, B);
		float L_21 = V_2;
		float L_22 = V_1;
		if ((!(((float)L_21) <= ((float)L_22))))
		{
			goto IL_0073;
		}
	}
	{
		// var c2 = Vector2.Dot(v,v); if (c2 <= c1) return Vector2.Distance(point, B);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_23 = ___point0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_24 = __this->get_B_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		float L_25 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_23, L_24, /*hidden argument*/NULL);
		return L_25;
	}

IL_0073:
	{
		// var b  = c1 / c2;
		float L_26 = V_1;
		float L_27 = V_2;
		V_3 = ((float)((float)L_26/(float)L_27));
		// var Pb = A + b * v;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_28 = __this->get_A_0();
		float L_29 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_30 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_31 = Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38(L_29, L_30, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_28, L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		// return Vector2.Distance(point, Pb);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_33 = ___point0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_34 = V_4;
		float L_35 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_33, L_34, /*hidden argument*/NULL);
		return L_35;
	}
}
IL2CPP_EXTERN_C  float Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433_AdjustorThunk (RuntimeObject * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 * _thisAdjusted = reinterpret_cast<Line_tE02A2351AACC6BE101A702EF1004F8F816A83B22 *>(__this + _offset);
	return Line_GetDistance_m146F8EECCB8A448ACCEC85C655B8178688BBB433(_thisAdjusted, ___point0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSpawnWithFinger::Spawn(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger_Spawn_m7B88A5BE93DB3947D35F3FFD191A24225ED1E5FF (LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667 * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSpawnWithFinger_Spawn_m7B88A5BE93DB3947D35F3FFD191A24225ED1E5FF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_0 = NULL;
	LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * V_1 = NULL;
	{
		// if (Prefab != null && finger != null)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = __this->get_Prefab_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0062;
		}
	}
	{
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_2 = ___finger0;
		if (!L_2)
		{
			goto IL_0062;
		}
	}
	{
		// var clone = Instantiate(Prefab);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = __this->get_Prefab_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_mB2B41F94C0264EE75558454A3CA3AB02E1C681F2(L_3, /*hidden argument*/Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_mB2B41F94C0264EE75558454A3CA3AB02E1C681F2_RuntimeMethod_var);
		V_0 = L_4;
		// UpdateSpawnedTransform(finger, clone);
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_5 = ___finger0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = V_0;
		LeanSpawnWithFinger_UpdateSpawnedTransform_mDE1F0B78EB0DF131E334929DB5492F5DF275AEF1(__this, L_5, L_6, /*hidden argument*/NULL);
		// clone.gameObject.SetActive(true);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = V_0;
		NullCheck(L_7);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_8, (bool)1, /*hidden argument*/NULL);
		// if (DragAfterSpawn == true)
		bool L_9 = __this->get_DragAfterSpawn_6();
		if (!L_9)
		{
			goto IL_004b;
		}
	}
	{
		// var fingerData = LeanFingerData.FindOrCreate(ref fingerDatas, finger);
		List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E ** L_10 = __this->get_address_of_fingerDatas_12();
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_11 = ___finger0;
		FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * L_12 = LeanFingerData_FindOrCreate_TisFingerData_t8756EDE1698B51920E54644B779698551904C8E5_mCFFC1F97189676F54D02C533194FC65F21E5C294((List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E **)L_10, L_11, /*hidden argument*/LeanFingerData_FindOrCreate_TisFingerData_t8756EDE1698B51920E54644B779698551904C8E5_mCFFC1F97189676F54D02C533194FC65F21E5C294_RuntimeMethod_var);
		// fingerData.Clone = clone;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = V_0;
		NullCheck(L_12);
		L_12->set_Clone_1(L_13);
	}

IL_004b:
	{
		// var selectable = clone.GetComponent<LeanSelectable>();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = V_0;
		NullCheck(L_14);
		LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * L_15 = Component_GetComponent_TisLeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_m3038CB3801530355AA0F3C9BB2C5DEE08BC318D7(L_14, /*hidden argument*/Component_GetComponent_TisLeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79_m3038CB3801530355AA0F3C9BB2C5DEE08BC318D7_RuntimeMethod_var);
		V_1 = L_15;
		// if (selectable != null)
		LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_16, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0062;
		}
	}
	{
		// selectable.Select(finger);
		LeanSelectable_tB946CA1839F817FFA9523A6B918CA8FFEE468A79 * L_18 = V_1;
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_19 = ___finger0;
		NullCheck(L_18);
		LeanSelectable_Select_mFE2E8E0481A51153E75BFBC02192ABE0DF431601(L_18, L_19, /*hidden argument*/NULL);
	}

IL_0062:
	{
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSpawnWithFinger::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger_OnEnable_m2A88FFA343CA960D22744332B7EAE4B5BEFF95F7 (LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSpawnWithFinger_OnEnable_m2A88FFA343CA960D22744332B7EAE4B5BEFF95F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// LeanTouch.OnFingerUp += HandleFingerUp;
		Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99 * L_0 = (Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99 *)il2cpp_codegen_object_new(Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99_il2cpp_TypeInfo_var);
		Action_1__ctor_m1DE067369478B25813790543A9C8A9C097D1D552(L_0, __this, (intptr_t)((intptr_t)LeanSpawnWithFinger_HandleFingerUp_mA7E3129AE8CB8D932D7414C9437C99B5ACA991C8_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m1DE067369478B25813790543A9C8A9C097D1D552_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LeanTouch_tECC655E5BCC1A8DD00E84C0A32FEA8EAAAAFD22D_il2cpp_TypeInfo_var);
		LeanTouch_add_OnFingerUp_mEF2096577CDA9B4D199625D0F8C60609612D4FE4(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSpawnWithFinger::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger_OnDisable_m8633E4CBD95D607BC247EF69E8CE5FC71CF4A356 (LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSpawnWithFinger_OnDisable_m8633E4CBD95D607BC247EF69E8CE5FC71CF4A356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// LeanTouch.OnFingerUp -= HandleFingerUp;
		Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99 * L_0 = (Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99 *)il2cpp_codegen_object_new(Action_1_t2DB7F600B7017889EA41EB3F33F18AF1CC13EE99_il2cpp_TypeInfo_var);
		Action_1__ctor_m1DE067369478B25813790543A9C8A9C097D1D552(L_0, __this, (intptr_t)((intptr_t)LeanSpawnWithFinger_HandleFingerUp_mA7E3129AE8CB8D932D7414C9437C99B5ACA991C8_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m1DE067369478B25813790543A9C8A9C097D1D552_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LeanTouch_tECC655E5BCC1A8DD00E84C0A32FEA8EAAAAFD22D_il2cpp_TypeInfo_var);
		LeanTouch_remove_OnFingerUp_mA4A5415C54D5C2E49F9A423940E92D1C2D9A4DF6(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSpawnWithFinger::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger_Update_m05192F6CFBD8FF8DBB8FFAD5748FC7CF0C324E7A (LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSpawnWithFinger_Update_m05192F6CFBD8FF8DBB8FFAD5748FC7CF0C324E7A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * V_1 = NULL;
	{
		// for (var i = fingerDatas.Count - 1; i >= 0; i--)
		List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * L_0 = __this->get_fingerDatas_12();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m8AF72779CDFBF0B3C04712DC2A82BEB60574E971_inline(L_0, /*hidden argument*/List_1_get_Count_m8AF72779CDFBF0B3C04712DC2A82BEB60574E971_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		goto IL_0041;
	}

IL_0010:
	{
		// var fingerData = fingerDatas[i];
		List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * L_2 = __this->get_fingerDatas_12();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * L_4 = List_1_get_Item_mCE09C7230D53F5DDE30062F890BEE99AF6FE8347_inline(L_2, L_3, /*hidden argument*/List_1_get_Item_mCE09C7230D53F5DDE30062F890BEE99AF6FE8347_RuntimeMethod_var);
		V_1 = L_4;
		// if (fingerData.Clone != null)
		FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * L_5 = V_1;
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = L_5->get_Clone_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_6, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003d;
		}
	}
	{
		// UpdateSpawnedTransform(fingerData.Finger, fingerData.Clone);
		FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * L_8 = V_1;
		NullCheck(L_8);
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_9 = ((LeanFingerData_t12A7D2A2B984684FE69F1E5FB494BFBD6067B110 *)L_8)->get_Finger_0();
		FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * L_10 = V_1;
		NullCheck(L_10);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = L_10->get_Clone_1();
		LeanSpawnWithFinger_UpdateSpawnedTransform_mDE1F0B78EB0DF131E334929DB5492F5DF275AEF1(__this, L_9, L_11, /*hidden argument*/NULL);
	}

IL_003d:
	{
		// for (var i = fingerDatas.Count - 1; i >= 0; i--)
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
	}

IL_0041:
	{
		// for (var i = fingerDatas.Count - 1; i >= 0; i--)
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSpawnWithFinger::UpdateSpawnedTransform(Lean.Touch.LeanFinger,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger_UpdateSpawnedTransform_mDE1F0B78EB0DF131E334929DB5492F5DF275AEF1 (LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667 * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___instance1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSpawnWithFinger_UpdateSpawnedTransform_mDE1F0B78EB0DF131E334929DB5492F5DF275AEF1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		// var screenPoint = finger.ScreenPosition;
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_0 = ___finger0;
		NullCheck(L_0);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = L_0->get_ScreenPosition_13();
		V_0 = L_1;
		// if (PixelScale != null)
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_2 = __this->get_PixelScale_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		// screenPoint += PixelOffset * PixelScale.scaleFactor;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = __this->get_PixelOffset_8();
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_6 = __this->get_PixelScale_9();
		NullCheck(L_6);
		float L_7 = Canvas_get_scaleFactor_m0F6D59E75F7605ABD2AFF6AF32A1097226CE060A(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_5, L_7, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_4, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		// }
		goto IL_0041;
	}

IL_0034:
	{
		// screenPoint += PixelOffset;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11 = __this->get_PixelOffset_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0041:
	{
		// var worldPoint = ScreenDepth.Convert(screenPoint, gameObject, instance);
		LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D * L_13 = __this->get_address_of_ScreenDepth_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14 = V_0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = ___instance1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = LeanScreenDepth_Convert_m4CA8998FD975A5AC752171657968240E73225A49((LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D *)L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		// if (WorldRelativeTo != null)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = __this->get_WorldRelativeTo_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_18, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_007d;
		}
	}
	{
		// worldPoint += WorldRelativeTo.TransformPoint(WorldOffset);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = V_1;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = __this->get_WorldRelativeTo_11();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = __this->get_WorldOffset_10();
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Transform_TransformPoint_mA96DC2A20EE7F4F915F7509863A18D99F5DD76CB(L_21, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_20, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		// }
		goto IL_008a;
	}

IL_007d:
	{
		// worldPoint += WorldOffset;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = __this->get_WorldOffset_10();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_25, L_26, /*hidden argument*/NULL);
		V_1 = L_27;
	}

IL_008a:
	{
		// instance.position = worldPoint;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_28 = ___instance1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = V_1;
		NullCheck(L_28);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_28, L_29, /*hidden argument*/NULL);
		// switch (RotateTo)
		int32_t L_30 = __this->get_RotateTo_5();
		V_2 = L_30;
		int32_t L_31 = V_2;
		if (!L_31)
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_32 = V_2;
		if ((((int32_t)L_32) == ((int32_t)1)))
		{
			goto IL_00b2;
		}
	}
	{
		return;
	}

IL_00a0:
	{
		// instance.rotation = transform.rotation;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = ___instance1;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_34 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_35 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_33, L_35, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00b2:
	{
		// instance.up = LeanScreenDepth.LastWorldNormal;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_36 = ___instance1;
		IL2CPP_RUNTIME_CLASS_INIT(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = ((LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D_il2cpp_TypeInfo_var))->get_LastWorldNormal_5();
		NullCheck(L_36);
		Transform_set_up_m05A833821812AF78009A8EFB949C4EFC8087C46A(L_36, L_37, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSpawnWithFinger::HandleFingerUp(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger_HandleFingerUp_mA7E3129AE8CB8D932D7414C9437C99B5ACA991C8 (LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667 * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSpawnWithFinger_HandleFingerUp_mA7E3129AE8CB8D932D7414C9437C99B5ACA991C8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// LeanFingerData.Remove(fingerDatas, finger, fingerDataPool);
		List_1_t9E6ED9C0D8CB77A2B0CD0475209A723ADE3BAE3E * L_0 = __this->get_fingerDatas_12();
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_1 = ___finger0;
		IL2CPP_RUNTIME_CLASS_INIT(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667_il2cpp_TypeInfo_var);
		Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 * L_2 = ((LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667_StaticFields*)il2cpp_codegen_static_fields_for(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667_il2cpp_TypeInfo_var))->get_fingerDataPool_13();
		LeanFingerData_Remove_TisFingerData_t8756EDE1698B51920E54644B779698551904C8E5_mF7E71281B78FF383B11C95F791C570436BEAD284(L_0, L_1, L_2, /*hidden argument*/LeanFingerData_Remove_TisFingerData_t8756EDE1698B51920E54644B779698551904C8E5_mF7E71281B78FF383B11C95F791C570436BEAD284_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSpawnWithFinger::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger__ctor_mE018A6379FCEA059BD6851C90D4AEAA0B8305D8D (LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667 * __this, const RuntimeMethod* method)
{
	{
		// public LeanScreenDepth ScreenDepth = new LeanScreenDepth(LeanScreenDepth.ConversionType.FixedDistance, Physics.DefaultRaycastLayers, 10.0f);
		LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D  L_0;
		memset((&L_0), 0, sizeof(L_0));
		LeanScreenDepth__ctor_m09C2B43694D7A2B959469447627D3B25AB095FE9((&L_0), 0, ((int32_t)-5), (10.0f), /*hidden argument*/NULL);
		__this->set_ScreenDepth_7(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Lean.Touch.LeanSpawnWithFinger::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawnWithFinger__cctor_m97C86DD6F6E5BE488C1CC580B00889AC3168D5B7 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSpawnWithFinger__cctor_m97C86DD6F6E5BE488C1CC580B00889AC3168D5B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static Stack<FingerData> fingerDataPool = new Stack<FingerData>();
		Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 * L_0 = (Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294 *)il2cpp_codegen_object_new(Stack_1_t74A1F7584E55807291B7B18BDD27480B96BC8294_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3F7C6FE33FAA71F303CB89570F8792B20371D121(L_0, /*hidden argument*/Stack_1__ctor_m3F7C6FE33FAA71F303CB89570F8792B20371D121_RuntimeMethod_var);
		((LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667_StaticFields*)il2cpp_codegen_static_fields_for(LeanSpawnWithFinger_t6D6EA9D7603186B60E146AE2A4957125A0D1F667_il2cpp_TypeInfo_var))->set_fingerDataPool_13(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanSpawnWithFinger/FingerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FingerData__ctor_m8BEC4556B84D540B9331DA7E702E7861FE686509 (FingerData_t8756EDE1698B51920E54644B779698551904C8E5 * __this, const RuntimeMethod* method)
{
	{
		LeanFingerData__ctor_mAB102253306FDAE43FA8680B4D84059BFD831A18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Events.UnityEvent Lean.Touch.LeanSwipeEdge::get_OnEdge()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * LeanSwipeEdge_get_OnEdge_m1BD57424A4B69E661270AEF52822A2558F2488B5 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSwipeEdge_get_OnEdge_m1BD57424A4B69E661270AEF52822A2558F2488B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public UnityEvent OnEdge { get { if (onEdge == null) onEdge = new UnityEvent(); return onEdge; } } [SerializeField] private UnityEvent onEdge;
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_0 = __this->get_onEdge_11();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public UnityEvent OnEdge { get { if (onEdge == null) onEdge = new UnityEvent(); return onEdge; } } [SerializeField] private UnityEvent onEdge;
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_1 = (UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F *)il2cpp_codegen_object_new(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m2F8C02F28E289CA65598FF4FA8EAB84D955FF028(L_1, /*hidden argument*/NULL);
		__this->set_onEdge_11(L_1);
	}

IL_0013:
	{
		// public UnityEvent OnEdge { get { if (onEdge == null) onEdge = new UnityEvent(); return onEdge; } } [SerializeField] private UnityEvent onEdge;
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_2 = __this->get_onEdge_11();
		return L_2;
	}
}
// System.Void Lean.Touch.LeanSwipeEdge::CheckBetween(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_CheckBetween_m3A2293756FD47E3B0DD85AE3E84D27DD172327F4 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___from0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___to1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSwipeEdge_CheckBetween_m3A2293756FD47E3B0DD85AE3E84D27DD172327F4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// var rect   = new Rect(0, 0, Screen.width, Screen.height);
		int32_t L_0 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_0), (0.0f), (0.0f), (((float)((float)L_0))), (((float)((float)L_1))), /*hidden argument*/NULL);
		// var vector = (to - from).normalized;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___to1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = ___from0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_2), /*hidden argument*/NULL);
		V_1 = L_5;
		// if (Left == true && CheckAngle(vector, Vector2.right) == true && CheckEdge(from.x - rect.xMin) == true)
		bool L_6 = __this->get_Left_5();
		if (!L_6)
		{
			goto IL_0060;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Vector2_get_right_mB4BD67462D579461853F297C0DE85D81E07E911E(/*hidden argument*/NULL);
		bool L_9 = LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3(__this, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0060;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = ___from0;
		float L_11 = L_10.get_x_0();
		float L_12 = Rect_get_xMin_mFDFA74F66595FD2B8CE360183D1A92B575F0A76E((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_0), /*hidden argument*/NULL);
		bool L_13 = LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B(__this, ((float)il2cpp_codegen_subtract((float)L_11, (float)L_12)), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0060;
		}
	}
	{
		// InvokeEdge(); return;
		LeanSwipeEdge_InvokeEdge_m57B9682317B2D34649E43C9135D0B934E9DEF37C(__this, /*hidden argument*/NULL);
		// InvokeEdge(); return;
		return;
	}

IL_0060:
	{
		// else if (Right == true && CheckAngle(vector, -Vector2.right) == true && CheckEdge(from.x - rect.xMax) == true)
		bool L_14 = __this->get_Right_6();
		if (!L_14)
		{
			goto IL_0098;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16 = Vector2_get_right_mB4BD67462D579461853F297C0DE85D81E07E911E(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_17 = Vector2_op_UnaryNegation_m3FA0AE2F9B031765EFA566B25F5453C3B001FF4D(L_16, /*hidden argument*/NULL);
		bool L_18 = LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3(__this, L_15, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0098;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = ___from0;
		float L_20 = L_19.get_x_0();
		float L_21 = Rect_get_xMax_mA16D7C3C2F30F8608719073ED79028C11CE90983((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_0), /*hidden argument*/NULL);
		bool L_22 = LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B(__this, ((float)il2cpp_codegen_subtract((float)L_20, (float)L_21)), /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0098;
		}
	}
	{
		// InvokeEdge(); return;
		LeanSwipeEdge_InvokeEdge_m57B9682317B2D34649E43C9135D0B934E9DEF37C(__this, /*hidden argument*/NULL);
		// InvokeEdge(); return;
		return;
	}

IL_0098:
	{
		// else if (Bottom == true && CheckAngle(vector, Vector2.up) == true && CheckEdge(from.y - rect.yMin) == true)
		bool L_23 = __this->get_Bottom_7();
		if (!L_23)
		{
			goto IL_00cb;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_25 = Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441(/*hidden argument*/NULL);
		bool L_26 = LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3(__this, L_24, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00cb;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_27 = ___from0;
		float L_28 = L_27.get_y_1();
		float L_29 = Rect_get_yMin_m31EDC3262BE39D2F6464B15397F882237E6158C3((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_0), /*hidden argument*/NULL);
		bool L_30 = LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B(__this, ((float)il2cpp_codegen_subtract((float)L_28, (float)L_29)), /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00cb;
		}
	}
	{
		// InvokeEdge(); return;
		LeanSwipeEdge_InvokeEdge_m57B9682317B2D34649E43C9135D0B934E9DEF37C(__this, /*hidden argument*/NULL);
		// InvokeEdge(); return;
		return;
	}

IL_00cb:
	{
		// else if (Top == true && CheckAngle(vector, -Vector2.up) == true && CheckEdge(from.y - rect.yMax) == true)
		bool L_31 = __this->get_Top_8();
		if (!L_31)
		{
			goto IL_0103;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_33 = Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_34 = Vector2_op_UnaryNegation_m3FA0AE2F9B031765EFA566B25F5453C3B001FF4D(L_33, /*hidden argument*/NULL);
		bool L_35 = LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3(__this, L_32, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0103;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = ___from0;
		float L_37 = L_36.get_y_1();
		float L_38 = Rect_get_yMax_m8AA5E92C322AF3FF571330F00579DA864F33341B((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_0), /*hidden argument*/NULL);
		bool L_39 = LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B(__this, ((float)il2cpp_codegen_subtract((float)L_37, (float)L_38)), /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0103;
		}
	}
	{
		// InvokeEdge(); return;
		LeanSwipeEdge_InvokeEdge_m57B9682317B2D34649E43C9135D0B934E9DEF37C(__this, /*hidden argument*/NULL);
		// InvokeEdge(); return;
		return;
	}

IL_0103:
	{
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSwipeEdge::AddFinger(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_AddFinger_m7A30AAFCB3F0E0B10AD5135488CFFDD9F1979C37 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method)
{
	{
		// Use.AddFinger(finger);
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_1 = ___finger0;
		LeanFingerFilter_AddFinger_m74C4A364453ED4A22F8423749A505DF0BA5A92EE((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSwipeEdge::RemoveFinger(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_RemoveFinger_m6782D8A94B0D240EAB75A96216D14653DA3E4096 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method)
{
	{
		// Use.RemoveFinger(finger);
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_1 = ___finger0;
		LeanFingerFilter_RemoveFinger_mF840A547635068D3F879DF19F3E17BE362E926F7((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSwipeEdge::RemoveAllFingers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_RemoveAllFingers_m4B5A1A9396C074BC4050D5E174D80A4814BCD886 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, const RuntimeMethod* method)
{
	{
		// Use.RemoveAllFingers();
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		LeanFingerFilter_RemoveAllFingers_mE802FAFCE7EBF3FE06ECDF1BA15F3644F85F3E9F((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSwipeEdge::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_Awake_mF5E03FDCF760BA9F933EE63C853E10E3E85BB959 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, const RuntimeMethod* method)
{
	{
		// Use.UpdateRequiredSelectable(gameObject);
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		LeanFingerFilter_UpdateRequiredSelectable_m3E2BEC9AB1DAA8AB70EC2E11B8D1227945031E7D((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSwipeEdge::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_Update_mE12B77020D9BA077D050696B5B83113C522386B5 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSwipeEdge_Update_mE12B77020D9BA077D050696B5B83113C522386B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * V_0 = NULL;
	int32_t V_1 = 0;
	LeanFinger_t1528CFE253E230129315FF802274B185655954CE * V_2 = NULL;
	{
		// var fingers = Use.GetFingers();
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * L_1 = LeanFingerFilter_GetFingers_mB21B35B96FBDE7030A77F8C129ED467C6C85EDDE((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		// for (var i = fingers.Count - 1; i >= 0; i--)
		List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_mBF7D336A87CDF3BE4D94C60FDEB087CA861E8F3E_inline(L_2, /*hidden argument*/List_1_get_Count_mBF7D336A87CDF3BE4D94C60FDEB087CA861E8F3E_RuntimeMethod_var);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1));
		goto IL_003e;
	}

IL_0018:
	{
		// var finger = fingers[i];
		List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_6 = List_1_get_Item_m5A4EE1F195799CBF32D8CBF91CF1F7D3060600F2_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_m5A4EE1F195799CBF32D8CBF91CF1F7D3060600F2_RuntimeMethod_var);
		V_2 = L_6;
		// if (finger.Swipe == true)
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_7 = V_2;
		NullCheck(L_7);
		bool L_8 = L_7->get_Swipe_6();
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		// CheckBetween(finger.StartScreenPosition, finger.ScreenPosition);
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_9 = V_2;
		NullCheck(L_9);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = L_9->get_StartScreenPosition_11();
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_11 = V_2;
		NullCheck(L_11);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = L_11->get_ScreenPosition_13();
		LeanSwipeEdge_CheckBetween_m3A2293756FD47E3B0DD85AE3E84D27DD172327F4(__this, L_10, L_12, /*hidden argument*/NULL);
	}

IL_003a:
	{
		// for (var i = fingers.Count - 1; i >= 0; i--)
		int32_t L_13 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1));
	}

IL_003e:
	{
		// for (var i = fingers.Count - 1; i >= 0; i--)
		int32_t L_14 = V_1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanSwipeEdge::InvokeEdge()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge_InvokeEdge_m57B9682317B2D34649E43C9135D0B934E9DEF37C (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, const RuntimeMethod* method)
{
	{
		// if (onEdge != null)
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_0 = __this->get_onEdge_11();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// onEdge.Invoke();
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_1 = __this->get_onEdge_11();
		NullCheck(L_1);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Boolean Lean.Touch.LeanSwipeEdge::CheckAngle(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSwipeEdge_CheckAngle_mACF78E404CE5EB9122A0DF7F9CFB297BFF0082A3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Vector2.Angle(a, b) <= AngleThreshold;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___a0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = ___b1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		float L_2 = Vector2_Angle_mC4A140B49B9E737C9FC6B52763468C5662A8B4AC(L_0, L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_AngleThreshold_9();
		return (bool)((((int32_t)((!(((float)L_2) <= ((float)L_3)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Lean.Touch.LeanSwipeEdge::CheckEdge(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, float ___distance0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanSwipeEdge_CheckEdge_m0C831CD39D3F3AF5C911A9A2FC649493D089F34B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Mathf.Abs(distance * LeanTouch.ScalingFactor) < EdgeThreshold;
		float L_0 = ___distance0;
		IL2CPP_RUNTIME_CLASS_INIT(LeanTouch_tECC655E5BCC1A8DD00E84C0A32FEA8EAAAAFD22D_il2cpp_TypeInfo_var);
		float L_1 = LeanTouch_get_ScalingFactor_m617EED84247952F5B8E628DF758059459DC4420D(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)il2cpp_codegen_multiply((float)L_0, (float)L_1)));
		float L_3 = __this->get_EdgeThreshold_10();
		return (bool)((((float)L_2) < ((float)L_3))? 1 : 0);
	}
}
// System.Void Lean.Touch.LeanSwipeEdge::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSwipeEdge__ctor_mBABEA72C9A8D796264627C7EAD9D7EECECD79A53 (LeanSwipeEdge_t7964DB2BF486AFED294FAF45F509DBEB27B05AFE * __this, const RuntimeMethod* method)
{
	{
		// public LeanFingerFilter Use = new LeanFingerFilter(true);
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE  L_0;
		memset((&L_0), 0, sizeof(L_0));
		LeanFingerFilter__ctor_mE72DC02BFD6E51E07FF20A8BEF9F985347483AA5((&L_0), (bool)1, /*hidden argument*/NULL);
		__this->set_Use_4(L_0);
		// public bool Left = true;
		__this->set_Left_5((bool)1);
		// public bool Right = true;
		__this->set_Right_6((bool)1);
		// public bool Bottom = true;
		__this->set_Bottom_7((bool)1);
		// public bool Top = true;
		__this->set_Top_8((bool)1);
		// public float AngleThreshold = 10.0f;
		__this->set_AngleThreshold_9((10.0f));
		// public float EdgeThreshold = 10.0f;
		__this->set_EdgeThreshold_10((10.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Touch.LeanTwistCamera::AddFinger(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanTwistCamera_AddFinger_mDFBE1DF25C6AEBE831E491F4B1B9A61B507A3ECD (LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method)
{
	{
		// Use.AddFinger(finger);
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_1 = ___finger0;
		LeanFingerFilter_AddFinger_m74C4A364453ED4A22F8423749A505DF0BA5A92EE((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanTwistCamera::RemoveFinger(Lean.Touch.LeanFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanTwistCamera_RemoveFinger_m490128386DCFEF3597E463F42BC7AE86D461B31D (LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC * __this, LeanFinger_t1528CFE253E230129315FF802274B185655954CE * ___finger0, const RuntimeMethod* method)
{
	{
		// Use.RemoveFinger(finger);
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		LeanFinger_t1528CFE253E230129315FF802274B185655954CE * L_1 = ___finger0;
		LeanFingerFilter_RemoveFinger_mF840A547635068D3F879DF19F3E17BE362E926F7((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanTwistCamera::RemoveAllFingers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanTwistCamera_RemoveAllFingers_mE74F8D9E7FA9CB73D2F7E8C946812D4F0E2ADE68 (LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC * __this, const RuntimeMethod* method)
{
	{
		// Use.RemoveAllFingers();
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		LeanFingerFilter_RemoveAllFingers_mE802FAFCE7EBF3FE06ECDF1BA15F3644F85F3E9F((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanTwistCamera::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanTwistCamera_Awake_m73538D6D435E1778292BA73FE291A32D59216847 (LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC * __this, const RuntimeMethod* method)
{
	{
		// Use.UpdateRequiredSelectable(gameObject);
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		LeanFingerFilter_UpdateRequiredSelectable_m3E2BEC9AB1DAA8AB70EC2E11B8D1227945031E7D((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanTwistCamera::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanTwistCamera_Update_mF932C5BA25C5AFDC87D121E8AFCB3382083BD43B (LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanTwistCamera_Update_mF932C5BA25C5AFDC87D121E8AFCB3382083BD43B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * V_0 = NULL;
	float V_1 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_3;
	memset((&V_3), 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_8;
	memset((&V_8), 0, sizeof(V_8));
	{
		// var fingers = Use.GetFingers();
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE * L_0 = __this->get_address_of_Use_4();
		List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * L_1 = LeanFingerFilter_GetFingers_mB21B35B96FBDE7030A77F8C129ED467C6C85EDDE((LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE *)L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var twistDegrees = -LeanGesture.GetTwistDegrees(fingers);
		List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * L_2 = V_0;
		float L_3 = LeanGesture_GetTwistDegrees_m5A181D7665683D6BDD079393230F57215808092E(L_2, /*hidden argument*/NULL);
		V_1 = ((-L_3));
		// var oldPosition = transform.localPosition;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		// var oldRotation = transform.localRotation;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_7 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		// if (Relative == true)
		bool L_8 = __this->get_Relative_6();
		if (!L_8)
		{
			goto IL_0073;
		}
	}
	{
		// var screenPoint = default(Vector2);
		il2cpp_codegen_initobj((&V_7), sizeof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D ));
		// if (LeanGesture.TryGetScreenCenter(fingers, ref screenPoint) == true)
		List_1_t11606163DA52EEC526C0361FB5F3B04DDB6F202B * L_9 = V_0;
		bool L_10 = LeanGesture_TryGetScreenCenter_m6B05B5DD79C77423120ABC8F91EE7BF9962F5265(L_9, (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_7), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_008a;
		}
	}
	{
		// var worldPoint = ScreenDepth.Convert(screenPoint);
		LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D * L_11 = __this->get_address_of_ScreenDepth_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = V_7;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = LeanScreenDepth_Convert_m4CA8998FD975A5AC752171657968240E73225A49((LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D *)L_11, L_12, (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)NULL, (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)NULL, /*hidden argument*/NULL);
		V_8 = L_13;
		// transform.RotateAround(worldPoint, transform.forward, twistDegrees);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = V_8;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F(L_16, /*hidden argument*/NULL);
		float L_18 = V_1;
		NullCheck(L_14);
		Transform_RotateAround_m433D292B2A38A5A4DEC7DCAE0A8BEAC5C3B2D1DD(L_14, L_15, L_17, L_18, /*hidden argument*/NULL);
		// }
		goto IL_008a;
	}

IL_0073:
	{
		// transform.Rotate(transform.forward, twistDegrees);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F(L_20, /*hidden argument*/NULL);
		float L_22 = V_1;
		NullCheck(L_19);
		Transform_Rotate_m6F7F297A2AF39309B4B8F537CCF49A143896CFBA(L_19, L_21, L_22, /*hidden argument*/NULL);
	}

IL_008a:
	{
		// remainingTranslation += transform.localPosition - oldPosition;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = __this->get_remainingTranslation_8();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_24, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_25, L_26, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_23, L_27, /*hidden argument*/NULL);
		__this->set_remainingTranslation_8(L_28);
		// remainingRotation    *= Quaternion.Inverse(oldRotation) * transform.localRotation;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_29 = __this->get_remainingRotation_9();
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_30 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_31 = Quaternion_Inverse_mC3A78571A826F05CE179637E675BD25F8B203E0C(L_30, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_32 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_33 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_32, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_34 = Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790(L_31, L_33, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_35 = Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790(L_29, L_34, /*hidden argument*/NULL);
		__this->set_remainingRotation_9(L_35);
		// var factor = LeanHelper.GetDampenFactor(Damping, Time.deltaTime);
		float L_36 = __this->get_Damping_5();
		float L_37 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		float L_38 = LeanHelper_GetDampenFactor_mDCE92ADEFA6CD274B18AFD9C541529DD3DBA4642(L_36, L_37, /*hidden argument*/NULL);
		V_4 = L_38;
		// var newRemainingTranslation = Vector3.Lerp(remainingTranslation, Vector3.zero, factor);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_39 = __this->get_remainingTranslation_8();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		float L_41 = V_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42 = Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1(L_39, L_40, L_41, /*hidden argument*/NULL);
		V_5 = L_42;
		// var newRemainingRotation    = Quaternion.Slerp(remainingRotation, Quaternion.identity, factor);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_43 = __this->get_remainingRotation_9();
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_44 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		float L_45 = V_4;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_46 = Quaternion_Slerp_m56DE173C3520C83DF3F1C6EDFA82FF88A2C9E756(L_43, L_44, L_45, /*hidden argument*/NULL);
		V_6 = L_46;
		// transform.localPosition = oldPosition + remainingTranslation - newRemainingTranslation;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_47 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = __this->get_remainingTranslation_8();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_50 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_48, L_49, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_51 = V_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_52 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_50, L_51, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_47, L_52, /*hidden argument*/NULL);
		// transform.localRotation = oldRotation * Quaternion.Inverse(newRemainingRotation) * remainingRotation;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_53 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_54 = V_3;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_55 = V_6;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_56 = Quaternion_Inverse_mC3A78571A826F05CE179637E675BD25F8B203E0C(L_55, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_57 = Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790(L_54, L_56, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_58 = __this->get_remainingRotation_9();
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_59 = Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_53, L_59, /*hidden argument*/NULL);
		// remainingTranslation = newRemainingTranslation;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60 = V_5;
		__this->set_remainingTranslation_8(L_60);
		// remainingRotation    = newRemainingRotation;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_61 = V_6;
		__this->set_remainingRotation_9(L_61);
		// }
		return;
	}
}
// System.Void Lean.Touch.LeanTwistCamera::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanTwistCamera__ctor_m99D0DB9BB0A2ED9D47AB4C18DB879D2A01412FC1 (LeanTwistCamera_t34FB378D851B28FB54258EAC428B6CA38535E0BC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LeanTwistCamera__ctor_m99D0DB9BB0A2ED9D47AB4C18DB879D2A01412FC1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public LeanFingerFilter Use = new LeanFingerFilter(true);
		LeanFingerFilter_t4FC239CF4B54658AC2E2A26AB6B14661556D46DE  L_0;
		memset((&L_0), 0, sizeof(L_0));
		LeanFingerFilter__ctor_mE72DC02BFD6E51E07FF20A8BEF9F985347483AA5((&L_0), (bool)1, /*hidden argument*/NULL);
		__this->set_Use_4(L_0);
		// [FSA("Dampening")] public float Damping = -1.0f;
		__this->set_Damping_5((-1.0f));
		// public LeanScreenDepth ScreenDepth = new LeanScreenDepth(LeanScreenDepth.ConversionType.DepthIntercept);
		LeanScreenDepth_t76257D4ED7EA6D14304EE0C6F937A90011D30A1D  L_1;
		memset((&L_1), 0, sizeof(L_1));
		LeanScreenDepth__ctor_m09C2B43694D7A2B959469447627D3B25AB095FE9((&L_1), 1, ((int32_t)-5), (0.0f), /*hidden argument*/NULL);
		__this->set_ScreenDepth_7(L_1);
		// private Quaternion remainingRotation = Quaternion.identity;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_2 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		__this->set_remainingRotation_9(L_2);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mB7DBB69CCDC0F6F5965168A41C7E61BCCDA3E116_gshared_inline (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  List_1_get_Item_m25A520345C0C68F6162996DAA85DA2282D656A12_gshared_inline (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_2 = (Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)L_2, (int32_t)L_3);
		return (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_4;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
