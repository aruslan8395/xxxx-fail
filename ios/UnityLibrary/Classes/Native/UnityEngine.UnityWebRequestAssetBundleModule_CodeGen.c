﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAssetBundle::GetAssetBundle(System.String)
extern void UnityWebRequestAssetBundle_GetAssetBundle_m2532648FD093F0C4EAE63B3E419F12287646850B (void);
// 0x00000002 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAssetBundle::GetAssetBundle(System.String,System.UInt32)
extern void UnityWebRequestAssetBundle_GetAssetBundle_m7B84478DA8C074ACED6A71C3A8E434335E7D803C (void);
// 0x00000003 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAssetBundle::GetAssetBundle(System.String,UnityEngine.CachedAssetBundle,System.UInt32)
extern void UnityWebRequestAssetBundle_GetAssetBundle_mFD11F1D6E497F1C9547E46EF263D868149A58560 (void);
// 0x00000004 System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::Create(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.UInt32)
extern void DownloadHandlerAssetBundle_Create_mF2BE2489505217F7017337C5D8D1BB379C99E172 (void);
// 0x00000005 System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::CreateCached(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void DownloadHandlerAssetBundle_CreateCached_mDE651B99D7669D65060E098C31BA6141A1AD578D (void);
// 0x00000006 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::InternalCreateAssetBundle(System.String,System.UInt32)
extern void DownloadHandlerAssetBundle_InternalCreateAssetBundle_m4A8DF4FEFAA7A192CAA269AC3EC7D801FC949B4F (void);
// 0x00000007 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::InternalCreateAssetBundleCached(System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void DownloadHandlerAssetBundle_InternalCreateAssetBundleCached_m1C2B86BCE17D78778F176F55759E1D9AAEF94A9F (void);
// 0x00000008 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,System.UInt32)
extern void DownloadHandlerAssetBundle__ctor_m1B3026A98DBBB87D338779CE3FB78F0882D3F833 (void);
// 0x00000009 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,UnityEngine.CachedAssetBundle,System.UInt32)
extern void DownloadHandlerAssetBundle__ctor_mA8F9453267FD92804863CE2A1AC522E19BE84B57 (void);
// 0x0000000A System.Byte[] UnityEngine.Networking.DownloadHandlerAssetBundle::GetData()
extern void DownloadHandlerAssetBundle_GetData_mEE15F9F5235CA56D352A42360DBB97F4FC991F79 (void);
// 0x0000000B System.String UnityEngine.Networking.DownloadHandlerAssetBundle::GetText()
extern void DownloadHandlerAssetBundle_GetText_m0144E0707A55ADBBC66C19DC5D6221D3223F84A6 (void);
// 0x0000000C UnityEngine.AssetBundle UnityEngine.Networking.DownloadHandlerAssetBundle::get_assetBundle()
extern void DownloadHandlerAssetBundle_get_assetBundle_mC9081BC84B18ED108B8AA06C104985F62AFBD589 (void);
// 0x0000000D System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::CreateCached_Injected(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.String,UnityEngine.Hash128&,System.UInt32)
extern void DownloadHandlerAssetBundle_CreateCached_Injected_mD9AA1830E3F6FF54F0FC5C8C9FF2CABCB8AE027D (void);
static Il2CppMethodPointer s_methodPointers[13] = 
{
	UnityWebRequestAssetBundle_GetAssetBundle_m2532648FD093F0C4EAE63B3E419F12287646850B,
	UnityWebRequestAssetBundle_GetAssetBundle_m7B84478DA8C074ACED6A71C3A8E434335E7D803C,
	UnityWebRequestAssetBundle_GetAssetBundle_mFD11F1D6E497F1C9547E46EF263D868149A58560,
	DownloadHandlerAssetBundle_Create_mF2BE2489505217F7017337C5D8D1BB379C99E172,
	DownloadHandlerAssetBundle_CreateCached_mDE651B99D7669D65060E098C31BA6141A1AD578D,
	DownloadHandlerAssetBundle_InternalCreateAssetBundle_m4A8DF4FEFAA7A192CAA269AC3EC7D801FC949B4F,
	DownloadHandlerAssetBundle_InternalCreateAssetBundleCached_m1C2B86BCE17D78778F176F55759E1D9AAEF94A9F,
	DownloadHandlerAssetBundle__ctor_m1B3026A98DBBB87D338779CE3FB78F0882D3F833,
	DownloadHandlerAssetBundle__ctor_mA8F9453267FD92804863CE2A1AC522E19BE84B57,
	DownloadHandlerAssetBundle_GetData_mEE15F9F5235CA56D352A42360DBB97F4FC991F79,
	DownloadHandlerAssetBundle_GetText_m0144E0707A55ADBBC66C19DC5D6221D3223F84A6,
	DownloadHandlerAssetBundle_get_assetBundle_mC9081BC84B18ED108B8AA06C104985F62AFBD589,
	DownloadHandlerAssetBundle_CreateCached_Injected_mD9AA1830E3F6FF54F0FC5C8C9FF2CABCB8AE027D,
};
static const int32_t s_InvokerIndices[13] = 
{
	0,
	119,
	2024,
	2025,
	2026,
	130,
	2027,
	130,
	2028,
	14,
	14,
	14,
	2029,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestAssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestAssetBundleModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestAssetBundleModule.dll",
	13,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
