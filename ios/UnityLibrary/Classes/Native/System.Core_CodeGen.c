﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863 (void);
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000032 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000033 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D (void);
// 0x00000034 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000036 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000003A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000042 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000043 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000044 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000045 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000047 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000048 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000049 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004A TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004B TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004C TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004D TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004E TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000050 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000051 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000052 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000053 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000054 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000055 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000056 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000057 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000058 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000059 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x0000005A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005C System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000005D System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005E System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000005F System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000060 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x00000061 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000062 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x00000063 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000064 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000065 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000066 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000067 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x00000068 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000069 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006A System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000006B System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x0000006C System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x0000006D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006F System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000070 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x00000071 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000072 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000073 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000074 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000075 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000076 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000077 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x00000078 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000079 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007A System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000007B System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x0000007C System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x0000007D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007F System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x00000080 System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000081 System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000082 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000083 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x00000084 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000085 System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x00000086 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000087 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000088 TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000089 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x0000008A System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x0000008B System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000008C System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008D System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x0000008E System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x0000008F System.Boolean System.Linq.Enumerable/<ConcatIterator>d__59`1::MoveNext()
// 0x00000090 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally1()
// 0x00000091 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally2()
// 0x00000092 TSource System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000093 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x00000094 System.Object System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x00000095 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000096 System.Collections.IEnumerator System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000097 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000098 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000099 System.Boolean System.Linq.Enumerable/<DistinctIterator>d__68`1::MoveNext()
// 0x0000009A System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::<>m__Finally1()
// 0x0000009B TSource System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000009C System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000009D System.Object System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000009E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000009F System.Collections.IEnumerator System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A0 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x000000A1 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x000000A2 System.Boolean System.Linq.Enumerable/<UnionIterator>d__71`1::MoveNext()
// 0x000000A3 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally1()
// 0x000000A4 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally2()
// 0x000000A5 TSource System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000A6 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x000000A7 System.Object System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x000000A8 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A9 System.Collections.IEnumerator System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Void System.Linq.Enumerable/<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000AB System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000AC System.Boolean System.Linq.Enumerable/<CastIterator>d__99`1::MoveNext()
// 0x000000AD System.Void System.Linq.Enumerable/<CastIterator>d__99`1::<>m__Finally1()
// 0x000000AE TResult System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000AF System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000B0 System.Object System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000B1 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000B2 System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B3 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000B4 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x000000B5 System.Void System.Linq.IdentityFunction`1/<>c::.cctor()
// 0x000000B6 System.Void System.Linq.IdentityFunction`1/<>c::.ctor()
// 0x000000B7 TElement System.Linq.IdentityFunction`1/<>c::<get_Instance>b__1_0(TElement)
// 0x000000B8 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000B9 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000BA System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x000000BB System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BC System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x000000BD System.Linq.Lookup`2/Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x000000BE System.Void System.Linq.Lookup`2::Resize()
// 0x000000BF System.Void System.Linq.Lookup`2/Grouping::Add(TElement)
// 0x000000C0 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2/Grouping::GetEnumerator()
// 0x000000C1 System.Collections.IEnumerator System.Linq.Lookup`2/Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C2 System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x000000C3 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x000000C4 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x000000C5 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x000000C6 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x000000C7 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x000000C8 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x000000C9 System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x000000CA System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x000000CB System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000CC TElement System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000CD System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000CE System.Void System.Linq.Lookup`2/Grouping::.ctor()
// 0x000000CF System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000D0 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000D1 System.Boolean System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::MoveNext()
// 0x000000D2 TElement System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000D3 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000D4 System.Object System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000D5 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000D6 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000D7 System.Boolean System.Linq.Lookup`2/<GetEnumerator>d__12::MoveNext()
// 0x000000D8 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000D9 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000DA System.Object System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000DB System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000DC System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000DD System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000DE System.Void System.Linq.Set`1::Resize()
// 0x000000DF System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000E0 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000E1 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000E2 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E3 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000E4 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000E5 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E6 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000E7 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000E8 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000E9 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x000000EA TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000EB System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000EC System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000ED System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000EE System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000EF System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000F0 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000F1 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000F2 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000F3 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000F4 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000F5 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000F6 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000F7 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000F8 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000F9 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32*,System.Int32)
extern void BitHelper__ctor_m7D010B426005B48D8C03139EB9248E927293BF3F (void);
// 0x000000FA System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32[],System.Int32)
extern void BitHelper__ctor_m8F1FB12BE0C611E499E3E03A151A52FE02A468C5 (void);
// 0x000000FB System.Void System.Collections.Generic.BitHelper::MarkBit(System.Int32)
extern void BitHelper_MarkBit_mADF9FABA576B57C4BB5BD616ACAA28C613045802 (void);
// 0x000000FC System.Boolean System.Collections.Generic.BitHelper::IsMarked(System.Int32)
extern void BitHelper_IsMarked_m2DB877B8A728264DE8A02E20BDFA4C886CAB4345 (void);
// 0x000000FD System.Int32 System.Collections.Generic.BitHelper::ToIntArrayLength(System.Int32)
extern void BitHelper_ToIntArrayLength_mAFCF611F107D1DDA8FA965FA24A027715E1D3991 (void);
// 0x000000FE System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000FF System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000100 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000101 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000102 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000103 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000104 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000105 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000106 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000107 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000108 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000109 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000010A System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000010B System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000010C System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000010D System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000010E System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000010F System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000110 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000111 System.Void System.Collections.Generic.HashSet`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000112 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000113 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000114 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000115 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x00000116 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000117 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000118 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000119 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000011A System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x0000011B System.Void System.Collections.Generic.HashSet`1::IntersectWithHashSetWithSameEC(System.Collections.Generic.HashSet`1<T>)
// 0x0000011C System.Void System.Collections.Generic.HashSet`1::IntersectWithEnumerable(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000011D System.Int32 System.Collections.Generic.HashSet`1::InternalIndexOf(T)
// 0x0000011E System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x0000011F System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000120 System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000121 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x00000122 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x00000123 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x00000124 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000125 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[293] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BitHelper__ctor_m7D010B426005B48D8C03139EB9248E927293BF3F,
	BitHelper__ctor_m8F1FB12BE0C611E499E3E03A151A52FE02A468C5,
	BitHelper_MarkBit_mADF9FABA576B57C4BB5BD616ACAA28C613045802,
	BitHelper_IsMarked_m2DB877B8A728264DE8A02E20BDFA4C886CAB4345,
	BitHelper_ToIntArrayLength_mAFCF611F107D1DDA8FA965FA24A027715E1D3991,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[293] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	917,
	27,
	37,
	197,
	197,
	3,
	0,
	0,
	4,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	64,
	130,
	32,
	30,
	21,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[69] = 
{
	{ 0x02000008, { 100, 4 } },
	{ 0x02000009, { 104, 9 } },
	{ 0x0200000A, { 115, 7 } },
	{ 0x0200000B, { 124, 10 } },
	{ 0x0200000C, { 136, 11 } },
	{ 0x0200000D, { 150, 9 } },
	{ 0x0200000E, { 162, 12 } },
	{ 0x0200000F, { 177, 1 } },
	{ 0x02000010, { 178, 2 } },
	{ 0x02000011, { 180, 12 } },
	{ 0x02000012, { 192, 9 } },
	{ 0x02000013, { 201, 11 } },
	{ 0x02000014, { 212, 12 } },
	{ 0x02000015, { 224, 6 } },
	{ 0x02000016, { 230, 2 } },
	{ 0x02000017, { 232, 4 } },
	{ 0x02000018, { 236, 3 } },
	{ 0x0200001B, { 239, 17 } },
	{ 0x0200001C, { 260, 5 } },
	{ 0x0200001D, { 265, 1 } },
	{ 0x0200001F, { 266, 8 } },
	{ 0x02000021, { 274, 4 } },
	{ 0x02000022, { 278, 3 } },
	{ 0x02000023, { 281, 5 } },
	{ 0x02000024, { 286, 7 } },
	{ 0x02000025, { 293, 3 } },
	{ 0x02000026, { 296, 7 } },
	{ 0x02000027, { 303, 4 } },
	{ 0x02000029, { 307, 39 } },
	{ 0x0200002B, { 346, 2 } },
	{ 0x06000034, { 0, 10 } },
	{ 0x06000035, { 10, 10 } },
	{ 0x06000036, { 20, 5 } },
	{ 0x06000037, { 25, 5 } },
	{ 0x06000038, { 30, 1 } },
	{ 0x06000039, { 31, 2 } },
	{ 0x0600003A, { 33, 2 } },
	{ 0x0600003B, { 35, 4 } },
	{ 0x0600003C, { 39, 1 } },
	{ 0x0600003D, { 40, 2 } },
	{ 0x0600003E, { 42, 1 } },
	{ 0x0600003F, { 43, 2 } },
	{ 0x06000040, { 45, 1 } },
	{ 0x06000041, { 46, 2 } },
	{ 0x06000042, { 48, 3 } },
	{ 0x06000043, { 51, 2 } },
	{ 0x06000044, { 53, 1 } },
	{ 0x06000045, { 54, 7 } },
	{ 0x06000046, { 61, 2 } },
	{ 0x06000047, { 63, 2 } },
	{ 0x06000048, { 65, 4 } },
	{ 0x06000049, { 69, 4 } },
	{ 0x0600004A, { 73, 3 } },
	{ 0x0600004B, { 76, 3 } },
	{ 0x0600004C, { 79, 4 } },
	{ 0x0600004D, { 83, 4 } },
	{ 0x0600004E, { 87, 3 } },
	{ 0x0600004F, { 90, 1 } },
	{ 0x06000050, { 91, 1 } },
	{ 0x06000051, { 92, 3 } },
	{ 0x06000052, { 95, 3 } },
	{ 0x06000053, { 98, 2 } },
	{ 0x06000063, { 113, 2 } },
	{ 0x06000068, { 122, 2 } },
	{ 0x0600006D, { 134, 2 } },
	{ 0x06000073, { 147, 3 } },
	{ 0x06000078, { 159, 3 } },
	{ 0x0600007D, { 174, 3 } },
	{ 0x060000B8, { 256, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[348] = 
{
	{ (Il2CppRGCTXDataType)2, 27766 },
	{ (Il2CppRGCTXDataType)3, 21284 },
	{ (Il2CppRGCTXDataType)2, 27767 },
	{ (Il2CppRGCTXDataType)2, 27768 },
	{ (Il2CppRGCTXDataType)3, 21285 },
	{ (Il2CppRGCTXDataType)2, 27769 },
	{ (Il2CppRGCTXDataType)2, 27770 },
	{ (Il2CppRGCTXDataType)3, 21286 },
	{ (Il2CppRGCTXDataType)2, 27771 },
	{ (Il2CppRGCTXDataType)3, 21287 },
	{ (Il2CppRGCTXDataType)2, 27772 },
	{ (Il2CppRGCTXDataType)3, 21288 },
	{ (Il2CppRGCTXDataType)2, 27773 },
	{ (Il2CppRGCTXDataType)2, 27774 },
	{ (Il2CppRGCTXDataType)3, 21289 },
	{ (Il2CppRGCTXDataType)2, 27775 },
	{ (Il2CppRGCTXDataType)2, 27776 },
	{ (Il2CppRGCTXDataType)3, 21290 },
	{ (Il2CppRGCTXDataType)2, 27777 },
	{ (Il2CppRGCTXDataType)3, 21291 },
	{ (Il2CppRGCTXDataType)2, 27778 },
	{ (Il2CppRGCTXDataType)3, 21292 },
	{ (Il2CppRGCTXDataType)3, 21293 },
	{ (Il2CppRGCTXDataType)2, 19706 },
	{ (Il2CppRGCTXDataType)3, 21294 },
	{ (Il2CppRGCTXDataType)2, 27779 },
	{ (Il2CppRGCTXDataType)3, 21295 },
	{ (Il2CppRGCTXDataType)3, 21296 },
	{ (Il2CppRGCTXDataType)2, 19713 },
	{ (Il2CppRGCTXDataType)3, 21297 },
	{ (Il2CppRGCTXDataType)3, 21298 },
	{ (Il2CppRGCTXDataType)2, 27780 },
	{ (Il2CppRGCTXDataType)3, 21299 },
	{ (Il2CppRGCTXDataType)2, 27781 },
	{ (Il2CppRGCTXDataType)3, 21300 },
	{ (Il2CppRGCTXDataType)3, 21301 },
	{ (Il2CppRGCTXDataType)2, 27782 },
	{ (Il2CppRGCTXDataType)2, 27783 },
	{ (Il2CppRGCTXDataType)3, 21302 },
	{ (Il2CppRGCTXDataType)3, 21303 },
	{ (Il2CppRGCTXDataType)2, 27784 },
	{ (Il2CppRGCTXDataType)3, 21304 },
	{ (Il2CppRGCTXDataType)3, 21305 },
	{ (Il2CppRGCTXDataType)2, 27785 },
	{ (Il2CppRGCTXDataType)3, 21306 },
	{ (Il2CppRGCTXDataType)3, 21307 },
	{ (Il2CppRGCTXDataType)2, 27786 },
	{ (Il2CppRGCTXDataType)3, 21308 },
	{ (Il2CppRGCTXDataType)2, 27787 },
	{ (Il2CppRGCTXDataType)3, 21309 },
	{ (Il2CppRGCTXDataType)3, 21310 },
	{ (Il2CppRGCTXDataType)2, 19754 },
	{ (Il2CppRGCTXDataType)3, 21311 },
	{ (Il2CppRGCTXDataType)3, 21312 },
	{ (Il2CppRGCTXDataType)2, 19769 },
	{ (Il2CppRGCTXDataType)3, 21313 },
	{ (Il2CppRGCTXDataType)2, 19762 },
	{ (Il2CppRGCTXDataType)2, 27788 },
	{ (Il2CppRGCTXDataType)3, 21314 },
	{ (Il2CppRGCTXDataType)3, 21315 },
	{ (Il2CppRGCTXDataType)3, 21316 },
	{ (Il2CppRGCTXDataType)2, 19770 },
	{ (Il2CppRGCTXDataType)3, 21317 },
	{ (Il2CppRGCTXDataType)2, 27789 },
	{ (Il2CppRGCTXDataType)3, 21318 },
	{ (Il2CppRGCTXDataType)2, 27790 },
	{ (Il2CppRGCTXDataType)2, 27791 },
	{ (Il2CppRGCTXDataType)2, 19774 },
	{ (Il2CppRGCTXDataType)2, 27792 },
	{ (Il2CppRGCTXDataType)2, 27793 },
	{ (Il2CppRGCTXDataType)2, 27794 },
	{ (Il2CppRGCTXDataType)2, 19776 },
	{ (Il2CppRGCTXDataType)2, 27795 },
	{ (Il2CppRGCTXDataType)2, 19778 },
	{ (Il2CppRGCTXDataType)2, 27796 },
	{ (Il2CppRGCTXDataType)3, 21319 },
	{ (Il2CppRGCTXDataType)2, 19781 },
	{ (Il2CppRGCTXDataType)2, 27797 },
	{ (Il2CppRGCTXDataType)3, 21320 },
	{ (Il2CppRGCTXDataType)2, 27798 },
	{ (Il2CppRGCTXDataType)2, 27799 },
	{ (Il2CppRGCTXDataType)2, 19784 },
	{ (Il2CppRGCTXDataType)2, 27800 },
	{ (Il2CppRGCTXDataType)2, 27801 },
	{ (Il2CppRGCTXDataType)2, 27802 },
	{ (Il2CppRGCTXDataType)2, 19786 },
	{ (Il2CppRGCTXDataType)2, 27803 },
	{ (Il2CppRGCTXDataType)2, 19788 },
	{ (Il2CppRGCTXDataType)2, 27804 },
	{ (Il2CppRGCTXDataType)3, 21321 },
	{ (Il2CppRGCTXDataType)2, 27805 },
	{ (Il2CppRGCTXDataType)2, 19793 },
	{ (Il2CppRGCTXDataType)2, 19795 },
	{ (Il2CppRGCTXDataType)2, 27806 },
	{ (Il2CppRGCTXDataType)3, 21322 },
	{ (Il2CppRGCTXDataType)2, 19798 },
	{ (Il2CppRGCTXDataType)2, 27807 },
	{ (Il2CppRGCTXDataType)3, 21323 },
	{ (Il2CppRGCTXDataType)2, 27808 },
	{ (Il2CppRGCTXDataType)2, 19801 },
	{ (Il2CppRGCTXDataType)3, 21324 },
	{ (Il2CppRGCTXDataType)3, 21325 },
	{ (Il2CppRGCTXDataType)2, 19805 },
	{ (Il2CppRGCTXDataType)3, 21326 },
	{ (Il2CppRGCTXDataType)3, 21327 },
	{ (Il2CppRGCTXDataType)2, 19817 },
	{ (Il2CppRGCTXDataType)2, 27809 },
	{ (Il2CppRGCTXDataType)3, 21328 },
	{ (Il2CppRGCTXDataType)3, 21329 },
	{ (Il2CppRGCTXDataType)2, 19819 },
	{ (Il2CppRGCTXDataType)2, 27651 },
	{ (Il2CppRGCTXDataType)3, 21330 },
	{ (Il2CppRGCTXDataType)3, 21331 },
	{ (Il2CppRGCTXDataType)2, 27810 },
	{ (Il2CppRGCTXDataType)3, 21332 },
	{ (Il2CppRGCTXDataType)3, 21333 },
	{ (Il2CppRGCTXDataType)2, 19829 },
	{ (Il2CppRGCTXDataType)2, 27811 },
	{ (Il2CppRGCTXDataType)3, 21334 },
	{ (Il2CppRGCTXDataType)3, 21335 },
	{ (Il2CppRGCTXDataType)3, 20245 },
	{ (Il2CppRGCTXDataType)3, 21336 },
	{ (Il2CppRGCTXDataType)2, 27812 },
	{ (Il2CppRGCTXDataType)3, 21337 },
	{ (Il2CppRGCTXDataType)3, 21338 },
	{ (Il2CppRGCTXDataType)2, 19841 },
	{ (Il2CppRGCTXDataType)2, 27813 },
	{ (Il2CppRGCTXDataType)3, 21339 },
	{ (Il2CppRGCTXDataType)3, 21340 },
	{ (Il2CppRGCTXDataType)3, 21341 },
	{ (Il2CppRGCTXDataType)3, 21342 },
	{ (Il2CppRGCTXDataType)3, 21343 },
	{ (Il2CppRGCTXDataType)3, 20251 },
	{ (Il2CppRGCTXDataType)3, 21344 },
	{ (Il2CppRGCTXDataType)2, 27814 },
	{ (Il2CppRGCTXDataType)3, 21345 },
	{ (Il2CppRGCTXDataType)3, 21346 },
	{ (Il2CppRGCTXDataType)2, 19854 },
	{ (Il2CppRGCTXDataType)2, 27815 },
	{ (Il2CppRGCTXDataType)3, 21347 },
	{ (Il2CppRGCTXDataType)3, 21348 },
	{ (Il2CppRGCTXDataType)2, 19856 },
	{ (Il2CppRGCTXDataType)2, 27816 },
	{ (Il2CppRGCTXDataType)3, 21349 },
	{ (Il2CppRGCTXDataType)3, 21350 },
	{ (Il2CppRGCTXDataType)2, 27817 },
	{ (Il2CppRGCTXDataType)3, 21351 },
	{ (Il2CppRGCTXDataType)3, 21352 },
	{ (Il2CppRGCTXDataType)2, 27818 },
	{ (Il2CppRGCTXDataType)3, 21353 },
	{ (Il2CppRGCTXDataType)3, 21354 },
	{ (Il2CppRGCTXDataType)2, 19871 },
	{ (Il2CppRGCTXDataType)2, 27819 },
	{ (Il2CppRGCTXDataType)3, 21355 },
	{ (Il2CppRGCTXDataType)3, 21356 },
	{ (Il2CppRGCTXDataType)3, 21357 },
	{ (Il2CppRGCTXDataType)3, 20262 },
	{ (Il2CppRGCTXDataType)2, 27820 },
	{ (Il2CppRGCTXDataType)3, 21358 },
	{ (Il2CppRGCTXDataType)3, 21359 },
	{ (Il2CppRGCTXDataType)2, 27821 },
	{ (Il2CppRGCTXDataType)3, 21360 },
	{ (Il2CppRGCTXDataType)3, 21361 },
	{ (Il2CppRGCTXDataType)2, 19887 },
	{ (Il2CppRGCTXDataType)2, 27822 },
	{ (Il2CppRGCTXDataType)3, 21362 },
	{ (Il2CppRGCTXDataType)3, 21363 },
	{ (Il2CppRGCTXDataType)3, 21364 },
	{ (Il2CppRGCTXDataType)3, 21365 },
	{ (Il2CppRGCTXDataType)3, 21366 },
	{ (Il2CppRGCTXDataType)3, 21367 },
	{ (Il2CppRGCTXDataType)3, 20268 },
	{ (Il2CppRGCTXDataType)2, 27823 },
	{ (Il2CppRGCTXDataType)3, 21368 },
	{ (Il2CppRGCTXDataType)3, 21369 },
	{ (Il2CppRGCTXDataType)2, 27824 },
	{ (Il2CppRGCTXDataType)3, 21370 },
	{ (Il2CppRGCTXDataType)3, 21371 },
	{ (Il2CppRGCTXDataType)3, 21372 },
	{ (Il2CppRGCTXDataType)3, 21373 },
	{ (Il2CppRGCTXDataType)3, 21374 },
	{ (Il2CppRGCTXDataType)3, 21375 },
	{ (Il2CppRGCTXDataType)2, 27825 },
	{ (Il2CppRGCTXDataType)2, 27826 },
	{ (Il2CppRGCTXDataType)3, 21376 },
	{ (Il2CppRGCTXDataType)2, 19922 },
	{ (Il2CppRGCTXDataType)2, 19916 },
	{ (Il2CppRGCTXDataType)3, 21377 },
	{ (Il2CppRGCTXDataType)2, 19915 },
	{ (Il2CppRGCTXDataType)2, 27827 },
	{ (Il2CppRGCTXDataType)3, 21378 },
	{ (Il2CppRGCTXDataType)3, 21379 },
	{ (Il2CppRGCTXDataType)3, 21380 },
	{ (Il2CppRGCTXDataType)3, 21381 },
	{ (Il2CppRGCTXDataType)2, 19935 },
	{ (Il2CppRGCTXDataType)2, 19930 },
	{ (Il2CppRGCTXDataType)3, 21382 },
	{ (Il2CppRGCTXDataType)2, 19929 },
	{ (Il2CppRGCTXDataType)2, 27828 },
	{ (Il2CppRGCTXDataType)3, 21383 },
	{ (Il2CppRGCTXDataType)3, 21384 },
	{ (Il2CppRGCTXDataType)3, 21385 },
	{ (Il2CppRGCTXDataType)2, 27829 },
	{ (Il2CppRGCTXDataType)3, 21386 },
	{ (Il2CppRGCTXDataType)2, 19948 },
	{ (Il2CppRGCTXDataType)2, 19940 },
	{ (Il2CppRGCTXDataType)3, 21387 },
	{ (Il2CppRGCTXDataType)3, 21388 },
	{ (Il2CppRGCTXDataType)2, 19939 },
	{ (Il2CppRGCTXDataType)2, 27830 },
	{ (Il2CppRGCTXDataType)3, 21389 },
	{ (Il2CppRGCTXDataType)3, 21390 },
	{ (Il2CppRGCTXDataType)3, 21391 },
	{ (Il2CppRGCTXDataType)3, 21392 },
	{ (Il2CppRGCTXDataType)2, 27831 },
	{ (Il2CppRGCTXDataType)3, 21393 },
	{ (Il2CppRGCTXDataType)2, 19961 },
	{ (Il2CppRGCTXDataType)2, 19953 },
	{ (Il2CppRGCTXDataType)3, 21394 },
	{ (Il2CppRGCTXDataType)3, 21395 },
	{ (Il2CppRGCTXDataType)2, 19952 },
	{ (Il2CppRGCTXDataType)2, 27832 },
	{ (Il2CppRGCTXDataType)3, 21396 },
	{ (Il2CppRGCTXDataType)3, 21397 },
	{ (Il2CppRGCTXDataType)3, 21398 },
	{ (Il2CppRGCTXDataType)2, 19965 },
	{ (Il2CppRGCTXDataType)3, 21399 },
	{ (Il2CppRGCTXDataType)2, 27833 },
	{ (Il2CppRGCTXDataType)3, 21400 },
	{ (Il2CppRGCTXDataType)3, 21401 },
	{ (Il2CppRGCTXDataType)2, 27834 },
	{ (Il2CppRGCTXDataType)2, 27835 },
	{ (Il2CppRGCTXDataType)2, 27836 },
	{ (Il2CppRGCTXDataType)3, 21402 },
	{ (Il2CppRGCTXDataType)2, 19977 },
	{ (Il2CppRGCTXDataType)3, 21403 },
	{ (Il2CppRGCTXDataType)2, 27837 },
	{ (Il2CppRGCTXDataType)3, 21404 },
	{ (Il2CppRGCTXDataType)2, 27837 },
	{ (Il2CppRGCTXDataType)2, 20001 },
	{ (Il2CppRGCTXDataType)3, 21405 },
	{ (Il2CppRGCTXDataType)3, 21406 },
	{ (Il2CppRGCTXDataType)3, 21407 },
	{ (Il2CppRGCTXDataType)3, 21408 },
	{ (Il2CppRGCTXDataType)2, 27838 },
	{ (Il2CppRGCTXDataType)2, 27839 },
	{ (Il2CppRGCTXDataType)2, 27840 },
	{ (Il2CppRGCTXDataType)3, 21409 },
	{ (Il2CppRGCTXDataType)3, 21410 },
	{ (Il2CppRGCTXDataType)2, 19997 },
	{ (Il2CppRGCTXDataType)2, 20000 },
	{ (Il2CppRGCTXDataType)3, 21411 },
	{ (Il2CppRGCTXDataType)3, 21412 },
	{ (Il2CppRGCTXDataType)2, 20004 },
	{ (Il2CppRGCTXDataType)3, 21413 },
	{ (Il2CppRGCTXDataType)2, 27841 },
	{ (Il2CppRGCTXDataType)2, 19994 },
	{ (Il2CppRGCTXDataType)2, 27842 },
	{ (Il2CppRGCTXDataType)3, 21414 },
	{ (Il2CppRGCTXDataType)3, 21415 },
	{ (Il2CppRGCTXDataType)3, 21416 },
	{ (Il2CppRGCTXDataType)2, 27843 },
	{ (Il2CppRGCTXDataType)3, 21417 },
	{ (Il2CppRGCTXDataType)3, 21418 },
	{ (Il2CppRGCTXDataType)3, 21419 },
	{ (Il2CppRGCTXDataType)2, 20019 },
	{ (Il2CppRGCTXDataType)3, 21420 },
	{ (Il2CppRGCTXDataType)2, 27844 },
	{ (Il2CppRGCTXDataType)2, 27845 },
	{ (Il2CppRGCTXDataType)3, 21421 },
	{ (Il2CppRGCTXDataType)3, 21422 },
	{ (Il2CppRGCTXDataType)2, 20040 },
	{ (Il2CppRGCTXDataType)3, 21423 },
	{ (Il2CppRGCTXDataType)2, 20041 },
	{ (Il2CppRGCTXDataType)3, 21424 },
	{ (Il2CppRGCTXDataType)2, 27846 },
	{ (Il2CppRGCTXDataType)3, 21425 },
	{ (Il2CppRGCTXDataType)3, 21426 },
	{ (Il2CppRGCTXDataType)2, 27847 },
	{ (Il2CppRGCTXDataType)3, 21427 },
	{ (Il2CppRGCTXDataType)3, 21428 },
	{ (Il2CppRGCTXDataType)2, 27848 },
	{ (Il2CppRGCTXDataType)3, 21429 },
	{ (Il2CppRGCTXDataType)3, 21430 },
	{ (Il2CppRGCTXDataType)3, 21431 },
	{ (Il2CppRGCTXDataType)2, 20072 },
	{ (Il2CppRGCTXDataType)3, 21432 },
	{ (Il2CppRGCTXDataType)2, 20081 },
	{ (Il2CppRGCTXDataType)3, 21433 },
	{ (Il2CppRGCTXDataType)2, 27849 },
	{ (Il2CppRGCTXDataType)2, 27850 },
	{ (Il2CppRGCTXDataType)3, 21434 },
	{ (Il2CppRGCTXDataType)3, 21435 },
	{ (Il2CppRGCTXDataType)3, 21436 },
	{ (Il2CppRGCTXDataType)3, 21437 },
	{ (Il2CppRGCTXDataType)3, 21438 },
	{ (Il2CppRGCTXDataType)3, 21439 },
	{ (Il2CppRGCTXDataType)2, 20097 },
	{ (Il2CppRGCTXDataType)2, 27851 },
	{ (Il2CppRGCTXDataType)3, 21440 },
	{ (Il2CppRGCTXDataType)3, 21441 },
	{ (Il2CppRGCTXDataType)2, 20101 },
	{ (Il2CppRGCTXDataType)3, 21442 },
	{ (Il2CppRGCTXDataType)2, 27852 },
	{ (Il2CppRGCTXDataType)2, 20111 },
	{ (Il2CppRGCTXDataType)2, 20109 },
	{ (Il2CppRGCTXDataType)2, 27853 },
	{ (Il2CppRGCTXDataType)3, 21443 },
	{ (Il2CppRGCTXDataType)2, 27854 },
	{ (Il2CppRGCTXDataType)3, 21444 },
	{ (Il2CppRGCTXDataType)2, 20121 },
	{ (Il2CppRGCTXDataType)3, 21445 },
	{ (Il2CppRGCTXDataType)2, 20121 },
	{ (Il2CppRGCTXDataType)3, 21446 },
	{ (Il2CppRGCTXDataType)2, 20138 },
	{ (Il2CppRGCTXDataType)3, 21447 },
	{ (Il2CppRGCTXDataType)3, 21448 },
	{ (Il2CppRGCTXDataType)3, 21449 },
	{ (Il2CppRGCTXDataType)2, 27855 },
	{ (Il2CppRGCTXDataType)3, 21450 },
	{ (Il2CppRGCTXDataType)3, 21451 },
	{ (Il2CppRGCTXDataType)3, 21452 },
	{ (Il2CppRGCTXDataType)2, 20118 },
	{ (Il2CppRGCTXDataType)3, 21453 },
	{ (Il2CppRGCTXDataType)3, 21454 },
	{ (Il2CppRGCTXDataType)2, 20123 },
	{ (Il2CppRGCTXDataType)3, 21455 },
	{ (Il2CppRGCTXDataType)1, 27856 },
	{ (Il2CppRGCTXDataType)2, 20122 },
	{ (Il2CppRGCTXDataType)3, 21456 },
	{ (Il2CppRGCTXDataType)1, 20122 },
	{ (Il2CppRGCTXDataType)1, 20118 },
	{ (Il2CppRGCTXDataType)2, 27855 },
	{ (Il2CppRGCTXDataType)2, 20122 },
	{ (Il2CppRGCTXDataType)2, 20120 },
	{ (Il2CppRGCTXDataType)2, 20124 },
	{ (Il2CppRGCTXDataType)3, 21457 },
	{ (Il2CppRGCTXDataType)3, 21458 },
	{ (Il2CppRGCTXDataType)3, 21459 },
	{ (Il2CppRGCTXDataType)3, 21460 },
	{ (Il2CppRGCTXDataType)3, 21461 },
	{ (Il2CppRGCTXDataType)3, 21462 },
	{ (Il2CppRGCTXDataType)3, 21463 },
	{ (Il2CppRGCTXDataType)3, 21464 },
	{ (Il2CppRGCTXDataType)3, 21465 },
	{ (Il2CppRGCTXDataType)2, 20119 },
	{ (Il2CppRGCTXDataType)3, 21466 },
	{ (Il2CppRGCTXDataType)2, 20134 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	293,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	69,
	s_rgctxIndices,
	348,
	s_rgctxValues,
	NULL,
};
